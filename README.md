# Clone 1ra vez

Dentro de **/frontend** hacer `npm install`. Una vez haya finalizado.
Levantar el entorno con : `npm run dev`

> Para lo que es el backend:
> Dentro de **/portalprov** hacer `composer install`. Una vez haya finalizado.
> Se debe levantar apache (xampp)
> http://localhost/portalprov/web/index.php
> 



Para poder recargar el portal sin que salga el error 404 luego de hacer un npm run build ver [esta página](https://router.vuejs.org/guide/essentials/history-mode.html#example-server-configurations).

Si no funciona al estar en apache ver de que esté [instalado el mod_rewrite](https://stackoverflow.com/questions/7337724/how-to-check-whether-mod-rewrite-is-enable-on-server) y que estén [habilitados los .htaccess](https://www.keycdn.com/support/htaccess-not-working)




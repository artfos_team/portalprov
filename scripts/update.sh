cd iprove/
echo "pulling..."
git pull origin develop
echo "pulled"
cd frontend/
echo "building frontend..."
npm run build
echo "builded"
cd ../
echo "copying data..."
cp -TR app/controllers ../app/controllers
cp -TR app/models ../app/models
cp -TR app/views ../app/views
cp -TR app/commands ../app/commands
cp -TR frontend/dist/static ../static
cp -T frontend/dist/index.html ../index.html
cp -T frontend/dist/_redirects ../_redirects
echo "data copied"


ALTER TABLE `company_general_data`
	ADD COLUMN `modifDate` TIMESTAMP NULL DEFAULT current_timestamp() ON UPDATE current_timestamp() AFTER `modifDate`;

CREATE TABLE `log_company_general_data` (
	`idGeneralData` INT(11) NOT NULL AUTO_INCREMENT,
	`address` VARCHAR(100) NULL DEFAULT NULL COLLATE 'latin1_swedish_ci',
	`addressNumber` VARCHAR(200) NULL DEFAULT NULL COLLATE 'latin1_swedish_ci',
	`idProvince` INT(11) NULL DEFAULT NULL,
	`idCountry` INT(11) NULL DEFAULT NULL,
	`zipCode` VARCHAR(20) NULL DEFAULT NULL COLLATE 'latin1_swedish_ci',
	`city` VARCHAR(100) NULL DEFAULT NULL COLLATE 'latin1_swedish_ci',
	`phone` VARCHAR(20) NULL DEFAULT NULL COLLATE 'latin1_swedish_ci',
	`paymentEmail` VARCHAR(100) NULL DEFAULT NULL COLLATE 'latin1_swedish_ci',
	`receiptEmail` VARCHAR(100) NULL DEFAULT NULL COLLATE 'latin1_swedish_ci',
	`typeIVA` VARCHAR(20) NULL DEFAULT NULL COLLATE 'latin1_swedish_ci',
	`personType` VARCHAR(20) NULL DEFAULT NULL COLLATE 'latin1_swedish_ci',
	`personSubType` VARCHAR(10) NULL DEFAULT NULL COLLATE 'latin1_swedish_ci',
	`personSubTypeOther` VARCHAR(30) NULL DEFAULT NULL COLLATE 'latin1_swedish_ci',
	`pdfAfip` VARCHAR(200) NULL DEFAULT NULL COLLATE 'latin1_swedish_ci',
	`idCompanyDataStatus` INT(11) NULL DEFAULT '1' COMMENT 'Foreign Key (idCompanyDataStatus) references company_data_status(idCompanyDataStatus )',
	`observation` VARCHAR(400) NULL DEFAULT NULL COLLATE 'latin1_swedish_ci',
	`idUser` INT(11) NULL DEFAULT NULL,
	`namePrincipalContact` VARCHAR(100) NULL DEFAULT NULL COLLATE 'latin1_swedish_ci',
	`surnamePrincipalContact` VARCHAR(100) NULL DEFAULT NULL COLLATE 'latin1_swedish_ci',
	`phonePrincipalContact` VARCHAR(100) NULL DEFAULT NULL COLLATE 'latin1_swedish_ci',
	`emailPrincipalContact` VARCHAR(100) NULL DEFAULT NULL COLLATE 'latin1_swedish_ci',
	`nameCommercialContact` VARCHAR(100) NULL DEFAULT NULL COLLATE 'latin1_swedish_ci',
	`surnameCommercialContact` VARCHAR(100) NULL DEFAULT NULL COLLATE 'latin1_swedish_ci',
	`phoneCommercialContact` VARCHAR(100) NULL DEFAULT NULL COLLATE 'latin1_swedish_ci',
	`emailCommercialContact` VARCHAR(100) NULL DEFAULT NULL COLLATE 'latin1_swedish_ci',
	`idGroupCompany` INT(11) NULL DEFAULT NULL,
	`modifDate` TIMESTAMP NULL DEFAULT NULL,
	PRIMARY KEY (`idGeneralData`) USING BTREE,
	INDEX `company_general_data_FK_1` (`idProvince`) USING BTREE,
	INDEX `company_general_data_FK_2` (`idCompanyDataStatus`) USING BTREE,
	INDEX `idCountry` (`idCountry`) USING BTREE,
	INDEX `company_general_data_FK_3` (`idUser`) USING BTREE,
	INDEX `company_general_data_FK_4` (`idGroupCompany`) USING BTREE,
	CONSTRAINT `log_company_general_data_ibfk_1` FOREIGN KEY (`idCountry`) REFERENCES `iprove`.`country` (`idCountry`) ON UPDATE SET NULL ON DELETE SET NULL,
	CONSTRAINT `log_company_general_data_ibfk_2` FOREIGN KEY (`idProvince`) REFERENCES `iprove`.`province` (`idProvince`) ON UPDATE SET NULL ON DELETE SET NULL,
	CONSTRAINT `log_company_general_data_ibfk_3` FOREIGN KEY (`idCompanyDataStatus`) REFERENCES `iprove`.`company_data_status` (`idCompanyDataStatus`) ON UPDATE CASCADE ON DELETE CASCADE,
	CONSTRAINT `log_company_general_data_ibfk_4` FOREIGN KEY (`idUser`) REFERENCES `iprove`.`user` (`idUser`) ON UPDATE CASCADE ON DELETE SET NULL,
	CONSTRAINT `log_company_general_data_ibfk_5` FOREIGN KEY (`idGroupCompany`) REFERENCES `iprove`.`group_company` (`idGroupCompany`) ON UPDATE SET NULL ON DELETE SET NULL
)
COLLATE='latin1_swedish_ci'
ENGINE=InnoDB
ROW_FORMAT=DYNAMIC
AUTO_INCREMENT=40
;

DELIMITER ///

CREATE TRIGGER tracking_company_general_data AFTER UPDATE ON company_general_data
    FOR EACH ROW
    BEGIN
        IF NEW.modifDate <> OLD.modifDate THEN  
            DELETE FROM log_company_general_data WHERE idGeneralData = OLD.idGeneralData AND modifDate <> OLD.modifDate;
            INSERT INTO log_company_general_data (idGeneralData,address,addressNumber,idProvince,idCountry,zipCode,city,phone,paymentEmail,receiptEmail,typeIVA,personType,personSubType,personSubTypeOther,pdfAfip,idCompanyDataStatus,observation,idUser,namePrincipalContact,surnamePrincipalContact,phonePrincipalContact,emailPrincipalContact,nameCommercialContact,surnameCommercialContact,phoneCommercialContact,emailCommercialContact,idGroupCompany,modifDate) VALUES(OLD.idGeneralData, OLD.address, OLD.addressNumber, OLD.idProvince, OLD.idCountry, OLD.zipCode, OLD.city, OLD.phone, OLD.paymentEmail, OLD.receiptEmail, OLD.typeIVA, OLD.personType, OLD.personSubType, OLD.personSubTypeOther, OLD.pdfAfip, OLD.idCompanyDataStatus, OLD.observation, OLD.idUser, OLD.namePrincipalContact, OLD.surnamePrincipalContact, OLD.phonePrincipalContact, OLD.emailPrincipalContact, OLD.nameCommercialContact, OLD.surnameCommercialContact, OLD.phoneCommercialContact, OLD.emailCommercialContact, OLD.idGroupCompany, OLD.modifDate);
        END IF;
    END;
///

DELIMITER ;

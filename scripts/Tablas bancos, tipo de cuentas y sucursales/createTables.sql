CREATE TABLE `bank` (
	`idBank` INT(11) NOT NULL AUTO_INCREMENT,
	`bank` VARCHAR(50) NOT NULL DEFAULT '0',
	PRIMARY KEY (`idBank`)
)
ENGINE=InnoDB;

CREATE TABLE `bankBranch` (
	`idBankBranch` INT(11) NOT NULL AUTO_INCREMENT,
	`bankBranch` VARCHAR(200) NOT NULL DEFAULT '0',
	PRIMARY KEY (`idBankBranch`)
)
ENGINE=InnoDB;

CREATE TABLE `bankAccountType` (
	`idBankAccountType` INT(11) NOT NULL AUTO_INCREMENT,
	`bankAccountType` VARCHAR(100) NOT NULL DEFAULT '0',
	PRIMARY KEY (`idBankAccountType`)
)
ENGINE=InnoDB;
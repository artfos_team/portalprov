CREATE TABLE `company_stage` (
	`idStage` INT(11) NOT NULL AUTO_INCREMENT,
	`stage` VARCHAR(50) NULL DEFAULT '0' COLLATE 'utf8mb4_general_ci',
	PRIMARY KEY (`idStage`) USING BTREE
)
COLLATE='utf8mb4_general_ci'
ENGINE=InnoDB
AUTO_INCREMENT=4
;

INSERT INTO `company_stage` (`idStage`, `stage`) VALUES (1, 'Datos Generales');
INSERT INTO `company_stage` (`idStage`, `stage`) VALUES (2, 'Datos Bancarios');
INSERT INTO `company_stage` (`idStage`, `stage`) VALUES (3, 'Datos Impositivos');


ALTER TABLE `company`
	ADD COLUMN `idStage` INT NULL DEFAULT '1' AFTER `signUpStatus`,
	ADD CONSTRAINT `company_FK_4` FOREIGN KEY (`idStage`) REFERENCES `company_stage` (`idStage`) ON UPDATE CASCADE ON DELETE CASCADE;



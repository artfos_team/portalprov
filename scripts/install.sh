export MYSQL_PASS='Murcielag0_'
export MYSQL_HOST=localhost

git clone -b develop https://NicoRickert@bitbucket.org/artfos_team/contreras.git

cd contreras/

mysql -uroot -p$MYSQL_PASS -h$MYSQL_HOST < ./scripts/bd/contreras.sql

composer install

cd frontend/
npm install
npm run build


cd ../
mkdir web/assets/
mkdir web/upload/buyOrders
mkdir web/upload/payments
mkdir web/upload/documents
mkdir web/upload/licitacionesOfertas

cp -TR app/controllers ../app/controllers
cp -TR app/models ../app/models
cp -TR app/views ../app/views
cp -TR app/commands ../app/commands
cp -TR frontend/dist/static ../static
cp -T frontend/dist/index.html ../index.html
cp -T frontend/dist/_redirects ../_redirects

echo "data copied"

chmod 777 -R ./

rm -Rf contreras/


ALTER TABLE `company_general_data`
	ADD COLUMN `idGroupCompany` INT(11) NULL DEFAULT NULL AFTER `emailCommercialContact`,
	ADD CONSTRAINT `company_general_data_FK_4` FOREIGN KEY (`idGroupCompany`) REFERENCES `group_company` (`idGroupCompany`) ON UPDATE SET NULL ON DELETE SET NULL;
SELECT `DEFAULT_COLLATION_NAME` FROM `information_schema`.`SCHEMATA` WHERE `SCHEMA_NAME`='iprove';
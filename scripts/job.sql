/*
 Navicat Premium Data Transfer

 Source Server         : local
 Source Server Type    : MySQL
 Source Server Version : 50505
 Source Host           : localhost
 Source Database       : iprove

 Target Server Type    : MySQL
 Target Server Version : 50505
 File Encoding         : utf-8

 Date: 12/17/2019 13:50:51 PM
*/

SET NAMES utf8;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
--  Table structure for `job`
-- ----------------------------
DROP TABLE IF EXISTS `job`;
CREATE TABLE `job` (
  `idJob` int(11) NOT NULL AUTO_INCREMENT,
  `job` varchar(255) DEFAULT 'NULL',
  PRIMARY KEY (`idJob`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
--  Records of `job`
-- ----------------------------
BEGIN;
INSERT INTO `job` VALUES ('1', 'Comprador'), ('2', 'Jefe de Compras'), ('3', 'Jefe Finanzas');
COMMIT;

SET FOREIGN_KEY_CHECKS = 1;

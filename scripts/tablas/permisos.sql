INSERT INTO `iprove`.`roles` (`id`, `name`, `description`) VALUES (19, 'Proveedor', 'Proveedor con Cuit');
INSERT INTO `iprove`.`roles` (`id`, `name`, `description`) VALUES (20, 'Compras', 'Publica compulsas y califica proveedores');
INSERT INTO `iprove`.`roles` (`id`, `name`, `description`) VALUES (21, 'Cuentas a pagar', 'Toma facturas e informa el estado');
INSERT INTO `iprove`.`roles` (`id`, `name`, `description`) VALUES (22, 'Tesoreria', 'establece fecha de pago de cada factura y si será por cheque o banco');
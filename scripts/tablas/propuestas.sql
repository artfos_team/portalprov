CREATE TABLE `propuestas` (
	`idPropuesta` INT NOT NULL AUTO_INCREMENT,
	`idLicitacion` INT NULL,
	`idProvider` INT NULL,
	`monto` DECIMAL NULL,
	`status` INT NULL,
	`date` DATETIME NULL,
	PRIMARY KEY (`idPropuesta`)
)
COLLATE='latin1_swedish_ci'
;

CREATE TABLE `provider_document` (
	`idProviderDocument` INT NOT NULL AUTO_INCREMENT,
	`idProvider` INT NULL,
	`idDocument` INT NULL,
	`status` INT NULL DEFAULT '1',
	`date` DATE NULL,
	PRIMARY KEY (`idProviderDocument`)
)
COLLATE='latin1_swedish_ci'
;

﻿-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Versión del servidor:         10.1.32-MariaDB - mariadb.org binary distribution
-- SO del servidor:              Win32
-- HeidiSQL Versión:             9.5.0.5196
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Volcando estructura de base de datos para iprove
DROP DATABASE IF EXISTS `iprove`;
CREATE DATABASE IF NOT EXISTS `iprove` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `iprove`;

-- Volcando estructura para tabla iprove.city
DROP TABLE IF EXISTS `city`;
CREATE TABLE IF NOT EXISTS `city` (
  `idCity` int(11) NOT NULL AUTO_INCREMENT,
  `city` varchar(255) NOT NULL,
  `idCountry` int(11) NOT NULL,
  `idProvince` int(11) NOT NULL,
  PRIMARY KEY (`idCity`),
  KEY `fk_city_1` (`idCountry`) USING BTREE,
  KEY `fk_city_2` (`idProvince`) USING BTREE,
  CONSTRAINT `city_ibfk_1` FOREIGN KEY (`idCountry`) REFERENCES `country` (`idCountry`),
  CONSTRAINT `city_ibfk_2` FOREIGN KEY (`idProvince`) REFERENCES `province` (`idProvince`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla iprove.city: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `city` DISABLE KEYS */;
/*!40000 ALTER TABLE `city` ENABLE KEYS */;

-- Volcando estructura para tabla iprove.comments
DROP TABLE IF EXISTS `comments`;
CREATE TABLE IF NOT EXISTS `comments` (
  `idComments` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(500) DEFAULT NULL,
  `idCommentType` int(11) DEFAULT NULL,
  `idUser` int(11) NOT NULL DEFAULT '0',
  `dateComments` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `statusComments` int(11) NOT NULL DEFAULT '0',
  `idRefenceTo` int(11) DEFAULT '0',
  PRIMARY KEY (`idComments`),
  KEY `FK_invoicecomments_invoice_comments_status` (`statusComments`),
  KEY `FK_comments_comment_type` (`idCommentType`),
  KEY `FK_comments_user` (`idUser`),
  CONSTRAINT `FK_comments_comment_type` FOREIGN KEY (`idCommentType`) REFERENCES `comment_type` (`idCommentType`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_comments_user` FOREIGN KEY (`idUser`) REFERENCES `user` (`idUser`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_invoicecomments_invoice_comments_status` FOREIGN KEY (`statusComments`) REFERENCES `comments_status` (`idCommentsStatus`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Comentarios para las facturas';

-- Volcando datos para la tabla iprove.comments: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `comments` DISABLE KEYS */;
/*!40000 ALTER TABLE `comments` ENABLE KEYS */;

-- Volcando estructura para tabla iprove.comments_status
DROP TABLE IF EXISTS `comments_status`;
CREATE TABLE IF NOT EXISTS `comments_status` (
  `idCommentsStatus` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(50) NOT NULL DEFAULT '0',
  PRIMARY KEY (`idCommentsStatus`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1 COMMENT='Estatus de los comentarios';

-- Volcando datos para la tabla iprove.comments_status: ~3 rows (aproximadamente)
/*!40000 ALTER TABLE `comments_status` DISABLE KEYS */;
REPLACE INTO `comments_status` (`idCommentsStatus`, `description`) VALUES
	(1, 'Enviado '),
	(2, 'Sin Leer'),
	(3, 'Visto');
/*!40000 ALTER TABLE `comments_status` ENABLE KEYS */;

-- Volcando estructura para tabla iprove.comment_type
DROP TABLE IF EXISTS `comment_type`;
CREATE TABLE IF NOT EXISTS `comment_type` (
  `idCommentType` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(50) NOT NULL DEFAULT '0',
  PRIMARY KEY (`idCommentType`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla iprove.comment_type: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `comment_type` DISABLE KEYS */;
/*!40000 ALTER TABLE `comment_type` ENABLE KEYS */;
INSERT INTO `comment_type` VALUES (1, 'General');

-- Volcando estructura para tabla iprove.company
DROP TABLE IF EXISTS `company`;
CREATE TABLE IF NOT EXISTS `company` (
  `idCompany` int(11) NOT NULL AUTO_INCREMENT,
  `company` varchar(255) DEFAULT NULL,
  `companyName` varchar(255) DEFAULT NULL,
  `logo` text,
  `idCompanyCategory` int(11) DEFAULT NULL,
  `cuit` varchar(30) DEFAULT NULL,
  `requiresPurchaseOrder` tinyint(1) NOT NULL DEFAULT '0',
  `comments` text,
  `typeCompany` int(11) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `billingAddress` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`idCompany`),
  KEY `fk_company_1` (`idCompanyCategory`) USING BTREE,
  KEY `idCompany` (`idCompany`),
  CONSTRAINT `company_ibfk_1` FOREIGN KEY (`idCompanyCategory`) REFERENCES `company_category` (`idCompanyCategory`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla iprove.company: ~2 rows (aproximadamente)
/*!40000 ALTER TABLE `company` DISABLE KEYS */;
REPLACE INTO `company` (`idCompany`, `company`, `companyName`, `logo`, `idCompanyCategory`, `cuit`, `requiresPurchaseOrder`, `comments`, `typeCompany`, `address`, `billingAddress`) VALUES
	(1, 'Compañía 1', 'Compañía 1', NULL, NULL, '123456789\r\n', 0, NULL, NULL, 'Direccion 123', 'Direccion 456'),
	(2, 'Compañía 2', 'Compañía 2', NULL, NULL, '987654321\r\n', 0, NULL, NULL, 'Direccion 321', 'Direccion 654');
/*!40000 ALTER TABLE `company` ENABLE KEYS */;

-- Volcando estructura para tabla iprove.company_address
DROP TABLE IF EXISTS `company_address`;
CREATE TABLE IF NOT EXISTS `company_address` (
  `idCompanyAddress` int(11) NOT NULL AUTO_INCREMENT,
  `idCompany` int(11) NOT NULL,
  `address` varchar(255) NOT NULL,
  `idCity` int(11) NOT NULL,
  `zipCode` varchar(255) NOT NULL,
  `idProvince` int(11) NOT NULL,
  `idCountry` int(11) NOT NULL,
  `defaultLocation` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`idCompanyAddress`),
  KEY `fk_company_address_1` (`idCompany`) USING BTREE,
  KEY `fk_company_address_2` (`idCity`) USING BTREE,
  KEY `fk_company_address_3` (`idProvince`) USING BTREE,
  KEY `fk_company_address_4` (`idCountry`) USING BTREE,
  CONSTRAINT `company_address_ibfk_1` FOREIGN KEY (`idCompany`) REFERENCES `company` (`idCompany`),
  CONSTRAINT `company_address_ibfk_2` FOREIGN KEY (`idCity`) REFERENCES `city` (`idCity`),
  CONSTRAINT `company_address_ibfk_3` FOREIGN KEY (`idProvince`) REFERENCES `province` (`idProvince`),
  CONSTRAINT `company_address_ibfk_4` FOREIGN KEY (`idCountry`) REFERENCES `country` (`idCountry`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla iprove.company_address: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `company_address` DISABLE KEYS */;
/*!40000 ALTER TABLE `company_address` ENABLE KEYS */;

-- Volcando estructura para tabla iprove.company_category
DROP TABLE IF EXISTS `company_category`;
CREATE TABLE IF NOT EXISTS `company_category` (
  `idCompanyCategory` int(11) NOT NULL AUTO_INCREMENT,
  `companyCategory` varchar(255) NOT NULL,
  PRIMARY KEY (`idCompanyCategory`)
) ENGINE=InnoDB AUTO_INCREMENT=46 DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla iprove.company_category: ~2 rows (aproximadamente)
/*!40000 ALTER TABLE `company_category` DISABLE KEYS */;
REPLACE INTO `company_category` (`idCompanyCategory`, `companyCategory`) VALUES
	(12, 'Consumo Masivo'),
	(45, 'Telecomunicaciones');
/*!40000 ALTER TABLE `company_category` ENABLE KEYS */;

-- Volcando estructura para tabla iprove.company_contact
DROP TABLE IF EXISTS `company_contact`;
CREATE TABLE IF NOT EXISTS `company_contact` (
  `idCompanyContact` int(11) NOT NULL AUTO_INCREMENT,
  `idCompany` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `lastName` varchar(255) NOT NULL,
  `position` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `idCompanyAddress` int(11) NOT NULL,
  `telephone` varchar(255) DEFAULT NULL,
  `sex` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`idCompanyContact`),
  KEY `fk_company_contact_1` (`idCompany`) USING BTREE,
  KEY `fk_company_contact_2` (`idCompanyAddress`) USING BTREE,
  CONSTRAINT `company_contact_ibfk_1` FOREIGN KEY (`idCompany`) REFERENCES `company` (`idCompany`),
  CONSTRAINT `company_contact_ibfk_2` FOREIGN KEY (`idCompanyAddress`) REFERENCES `company_address` (`idCompanyAddress`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla iprove.company_contact: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `company_contact` DISABLE KEYS */;
/*!40000 ALTER TABLE `company_contact` ENABLE KEYS */;

-- Volcando estructura para tabla iprove.company_contact_phone
DROP TABLE IF EXISTS `company_contact_phone`;
CREATE TABLE IF NOT EXISTS `company_contact_phone` (
  `idCompanyContactPhone` int(11) NOT NULL AUTO_INCREMENT,
  `idCompanyContact` int(11) NOT NULL,
  `phoneType` int(11) NOT NULL,
  `phoneNumber` varchar(255) NOT NULL,
  `defaultPhone` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`idCompanyContactPhone`),
  KEY `fk_company_contact_phone_1` (`idCompanyContact`) USING BTREE,
  CONSTRAINT `company_contact_phone_ibfk_1` FOREIGN KEY (`idCompanyContact`) REFERENCES `company_contact` (`idCompanyContact`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla iprove.company_contact_phone: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `company_contact_phone` DISABLE KEYS */;
/*!40000 ALTER TABLE `company_contact_phone` ENABLE KEYS */;

-- Volcando estructura para tabla iprove.country
DROP TABLE IF EXISTS `country`;
CREATE TABLE IF NOT EXISTS `country` (
  `idCountry` int(11) NOT NULL AUTO_INCREMENT,
  `country` varchar(255) NOT NULL,
  PRIMARY KEY (`idCountry`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla iprove.country: ~1 rows (aproximadamente)
/*!40000 ALTER TABLE `country` DISABLE KEYS */;
REPLACE INTO `country` (`idCountry`, `country`) VALUES
	(1, 'Generico');
/*!40000 ALTER TABLE `country` ENABLE KEYS */;

-- Volcando estructura para tabla iprove.documentation
DROP TABLE IF EXISTS `documentation`;
CREATE TABLE IF NOT EXISTS `documentation` (
  `idDocumentation` int(11) NOT NULL AUTO_INCREMENT,
  `url` varchar(255) DEFAULT '0',
  `idDocumentationType` int(11) DEFAULT '0',
  `idProvider` int(11) DEFAULT '0',
  PRIMARY KEY (`idDocumentation`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla iprove.documentation: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `documentation` DISABLE KEYS */;
/*!40000 ALTER TABLE `documentation` ENABLE KEYS */;

-- Volcando estructura para tabla iprove.documentation_type
DROP TABLE IF EXISTS `documentation_type`;
CREATE TABLE IF NOT EXISTS `documentation_type` (
  `idDocumentationType` int(11) NOT NULL AUTO_INCREMENT,
  `documentationType` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`idDocumentationType`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla iprove.documentation_type: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `documentation_type` DISABLE KEYS */;
/*!40000 ALTER TABLE `documentation_type` ENABLE KEYS */;

-- Volcando estructura para tabla iprove.email_message
DROP TABLE IF EXISTS `email_message`;
CREATE TABLE IF NOT EXISTS `email_message` (
  `idEmailMessage` int(11) NOT NULL AUTO_INCREMENT,
  `from` text NOT NULL,
  `to` text NOT NULL,
  `cc` text,
  `bcc` text,
  `subject` varchar(255) NOT NULL,
  `body` text NOT NULL,
  `headers` text NOT NULL,
  `contentType` varchar(255) NOT NULL,
  `charset` varchar(255) NOT NULL,
  `created` datetime NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`idEmailMessage`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla iprove.email_message: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `email_message` DISABLE KEYS */;
/*!40000 ALTER TABLE `email_message` ENABLE KEYS */;

-- Volcando estructura para tabla iprove.email_parameters
DROP TABLE IF EXISTS `email_parameters`;
CREATE TABLE IF NOT EXISTS `email_parameters` (
  `idEmailParameters` int(11) NOT NULL AUTO_INCREMENT,
  `smtp` varchar(100) NOT NULL,
  `user` varchar(70) NOT NULL,
  `userPsw` varchar(50) NOT NULL,
  `fromName` varchar(50) NOT NULL,
  `hostForMails` varchar(200) NOT NULL,
  PRIMARY KEY (`idEmailParameters`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla iprove.email_parameters: ~1 rows (aproximadamente)
/*!40000 ALTER TABLE `email_parameters` DISABLE KEYS */;
REPLACE INTO `email_parameters` (`idEmailParameters`, `smtp`, `user`, `userPsw`, `fromName`, `hostForMails`) VALUES
	(1, 'smtp.gmail.com', 'prove@portal.com.ar', 'password', 'Info', 'http://iproveedores.com.ar');
/*!40000 ALTER TABLE `email_parameters` ENABLE KEYS */;

-- Volcando estructura para tabla iprove.import
DROP TABLE IF EXISTS `import`;
CREATE TABLE IF NOT EXISTS `import` (
  `idImport` int(11) NOT NULL AUTO_INCREMENT,
  `dateImport` date NOT NULL,
  `totalImport` int(11) DEFAULT '0',
  `updateImport` int(11) DEFAULT '0',
  `createImport` int(11) DEFAULT '0',
  `errorImport` int(11) DEFAULT '0',
  PRIMARY KEY (`idImport`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla iprove.import: ~1 rows (aproximadamente)
/*!40000 ALTER TABLE `import` DISABLE KEYS */;
REPLACE INTO `import` (`idImport`, `dateImport`, `totalImport`, `updateImport`, `createImport`, `errorImport`) VALUES
	(1, '2018-09-05', 1435, 1200, 233, 2);
/*!40000 ALTER TABLE `import` ENABLE KEYS */;

-- Volcando estructura para tabla iprove.import_error
DROP TABLE IF EXISTS `import_error`;
CREATE TABLE IF NOT EXISTS `import_error` (
  `idImportError` int(11) NOT NULL AUTO_INCREMENT,
  `idImport` int(11) NOT NULL,
  `description` varchar(300) DEFAULT NULL,
  `status` int(11) DEFAULT '0',
  PRIMARY KEY (`idImportError`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla iprove.import_error: ~2 rows (aproximadamente)
/*!40000 ALTER TABLE `import_error` DISABLE KEYS */;
REPLACE INTO `import_error` (`idImportError`, `idImport`, `description`, `status`) VALUES
	(1, 1, 'Falta el CUIT en la factura 34', 0),
	(2, 1, 'Falta el monto en la factura 93', 0);
/*!40000 ALTER TABLE `import_error` ENABLE KEYS */;

-- Volcando estructura para tabla iprove.invoice
DROP TABLE IF EXISTS `invoice`;
CREATE TABLE IF NOT EXISTS `invoice` (
  `idInvoice` int(11) NOT NULL,
  `idStatusInvoice` int(11) DEFAULT '0',
  `idCompanyProvider` int(11) DEFAULT '0',
  `numberInvoice` varchar(20) DEFAULT NULL,
  `dateEmission` date DEFAULT NULL,
  `datePayment` date DEFAULT NULL,
  `dateExpiration` date DEFAULT NULL,
  `invoiceTotalAmount` varchar(50) DEFAULT NULL,
  `idCompanyClient` int(11) DEFAULT '0',
  `idProvider` int(11) DEFAULT NULL,
  `idPaymentType` int(11) DEFAULT NULL,
  `retencion` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`idInvoice`),
  UNIQUE KEY `numberInvoice` (`numberInvoice`),
  KEY `FK_invoice_invoicestatus` (`idStatusInvoice`),
  KEY `FK_invoice_company` (`idCompanyProvider`),
  KEY `FK_invoice_company_2` (`idCompanyClient`),
  CONSTRAINT `FK_invoice_company` FOREIGN KEY (`idCompanyProvider`) REFERENCES `company` (`idCompany`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_invoice_company_2` FOREIGN KEY (`idCompanyClient`) REFERENCES `company` (`idCompany`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_invoice_invoicestatus` FOREIGN KEY (`idStatusInvoice`) REFERENCES `invoicestatus` (`idInvoiceStatus`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Tabla invoice para Komatzu';

-- Volcando datos para la tabla iprove.invoice: ~6 rows (aproximadamente)
/*!40000 ALTER TABLE `invoice` DISABLE KEYS */;
REPLACE INTO `invoice` (`idInvoice`, `idStatusInvoice`, `idCompanyProvider`, `numberInvoice`, `dateEmission`, `datePayment`, `dateExpiration`, `invoiceTotalAmount`, `idCompanyClient`, `idProvider`, `idPaymentType`, `retencion`) VALUES
	(1, 1, 1, '1', '2018-08-22', NULL, '2018-09-05', '10000', 2, NULL, NULL, NULL),
	(2, 1, 1, '2', '2018-08-23', NULL, '2018-09-23', '4500', 2, NULL, NULL, NULL),
	(3, 2, 1, '3', '2018-08-23', NULL, '2018-09-23', '7400', 2, NULL, NULL, NULL),
	(4, 3, 1, '4', '2018-08-17', NULL, '2018-09-17', '8560', 2, NULL, NULL, NULL),
	(5, 5, 1, '5', '2018-08-24', '2018-08-25', '2018-09-24', '1500', 2, NULL, NULL, NULL),
	(6, 4, 1, '6', '2018-08-21', NULL, '2018-09-21', '16000', 2, NULL, NULL, NULL);
/*!40000 ALTER TABLE `invoice` ENABLE KEYS */;

-- Volcando estructura para tabla iprove.invoicestatus
DROP TABLE IF EXISTS `invoicestatus`;
CREATE TABLE IF NOT EXISTS `invoicestatus` (
  `idInvoiceStatus` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`idInvoiceStatus`)
) ENGINE=InnoDB AUTO_INCREMENT=100 DEFAULT CHARSET=latin1 COMMENT='Estatus para las facturas';

-- Volcando datos para la tabla iprove.invoicestatus: ~4 rows (aproximadamente)
/*!40000 ALTER TABLE `invoicestatus` DISABLE KEYS */;
REPLACE INTO `invoicestatus` (`idInvoiceStatus`, `description`) VALUES
	(1, 'Recibida'),
	(2, 'Confirmada'),
	(3, 'Rechazada'),
	(4, 'Pendiente de Pago'),
	(5, 'Pagada');
/*!40000 ALTER TABLE `invoicestatus` ENABLE KEYS */;

-- Volcando estructura para tabla iprove.invoice_detail
DROP TABLE IF EXISTS `invoice_detail`;
CREATE TABLE IF NOT EXISTS `invoice_detail` (
  `idInvoiceDetail` int(11) NOT NULL AUTO_INCREMENT,
  `idInvoice` int(11) NOT NULL,
  `NroLinDet` int(11) DEFAULT NULL,
  `NmbItem` varchar(255) DEFAULT NULL,
  `QtyItem` varchar(255) DEFAULT NULL,
  `UnmdItem` varchar(255) DEFAULT NULL,
  `PrcItem` varchar(255) DEFAULT NULL,
  `MontoItem` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`idInvoiceDetail`),
  KEY `fk_invoice_detail` (`idInvoice`),
  CONSTRAINT `fk_invoice_detail` FOREIGN KEY (`idInvoice`) REFERENCES `invoice` (`idInvoice`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla iprove.invoice_detail: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `invoice_detail` DISABLE KEYS */;
/*!40000 ALTER TABLE `invoice_detail` ENABLE KEYS */;

-- Volcando estructura para tabla iprove.item
DROP TABLE IF EXISTS `item`;
CREATE TABLE IF NOT EXISTS `item` (
  `idOrderItem` int(11) NOT NULL AUTO_INCREMENT,
  `idOrder` int(11) NOT NULL,
  `product` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`idOrderItem`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla iprove.item: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `item` DISABLE KEYS */;
/*!40000 ALTER TABLE `item` ENABLE KEYS */;

-- Volcando estructura para tabla iprove.item_has_attachment
DROP TABLE IF EXISTS `item_has_attachment`;
CREATE TABLE IF NOT EXISTS `item_has_attachment` (
  `idItemHasAttachment` int(11) NOT NULL AUTO_INCREMENT,
  `idItem` int(11) NOT NULL,
  `idItemType` int(11) NOT NULL,
  `attachment` text NOT NULL,
  `title` varchar(255) NOT NULL,
  `publicDownload` tinyint(1) NOT NULL DEFAULT '0',
  `description` text,
  PRIMARY KEY (`idItemHasAttachment`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla iprove.item_has_attachment: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `item_has_attachment` DISABLE KEYS */;
/*!40000 ALTER TABLE `item_has_attachment` ENABLE KEYS */;

-- Volcando estructura para tabla iprove.logs
DROP TABLE IF EXISTS `logs`;
CREATE TABLE IF NOT EXISTS `logs` (
  `idLog` int(11) NOT NULL AUTO_INCREMENT,
  `idUser` int(11) DEFAULT NULL,
  `levelLog` int(11) NOT NULL DEFAULT '0',
  `dateLog` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `commentaryLog` varchar(250) NOT NULL,
  `source` varchar(50) NOT NULL,
  PRIMARY KEY (`idLog`),
  KEY `FK_logs_user` (`idUser`),
  CONSTRAINT `FK_logs_user` FOREIGN KEY (`idUser`) REFERENCES `user` (`idUser`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=103 DEFAULT CHARSET=latin1 COMMENT='Tabla para almacenar logs del sistema';

-- Volcando datos para la tabla iprove.logs: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `logs` DISABLE KEYS */;
/*!40000 ALTER TABLE `logs` ENABLE KEYS */;

-- Volcando estructura para tabla iprove.mailer_mailer
DROP TABLE IF EXISTS `mailer_mailer`;
CREATE TABLE IF NOT EXISTS `mailer_mailer` (
  `idmailer_mailer` int(11) NOT NULL AUTO_INCREMENT,
  `priority` int(11) NOT NULL DEFAULT '0',
  `send_at` int(11) DEFAULT NULL,
  `sent_at` int(11) DEFAULT NULL,
  `read_at` int(11) DEFAULT NULL,
  `created_at` int(11) NOT NULL,
  `subject` varchar(255) NOT NULL,
  `body` text,
  `html_body` text,
  `receiver_name` varchar(255) NOT NULL,
  `receiver_mail` varchar(255) NOT NULL,
  `sender_name` varchar(255) NOT NULL,
  `sender_mail` varchar(255) NOT NULL,
  `iditem_type` int(11) DEFAULT NULL,
  `iditem_container` int(11) DEFAULT NULL,
  `iditem_item` int(11) DEFAULT NULL,
  `unique_hash` varchar(100) DEFAULT NULL,
  `attachments` text,
  PRIMARY KEY (`idmailer_mailer`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla iprove.mailer_mailer: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `mailer_mailer` DISABLE KEYS */;
/*!40000 ALTER TABLE `mailer_mailer` ENABLE KEYS */;

-- Volcando estructura para tabla iprove.migration
DROP TABLE IF EXISTS `migration`;
CREATE TABLE IF NOT EXISTS `migration` (
  `version` varchar(255) NOT NULL,
  `apply_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`version`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla iprove.migration: 0 rows
/*!40000 ALTER TABLE `migration` DISABLE KEYS */;
/*!40000 ALTER TABLE `migration` ENABLE KEYS */;

-- Volcando estructura para tabla iprove.ncnd
DROP TABLE IF EXISTS `ncnd`;
CREATE TABLE IF NOT EXISTS `ncnd` (
  `idNCND` int(11) NOT NULL AUTO_INCREMENT,
  `idInvoice` int(11) DEFAULT NULL,
  `XMLfilename` varchar(255) NOT NULL,
  `IdDoc_TipoDTE` varchar(255) DEFAULT NULL,
  `IdDoc_Folio` varchar(255) DEFAULT NULL,
  `IdDoc_FchEmis` varchar(255) NOT NULL,
  `Emisor_RUTEmisor` varchar(255) NOT NULL,
  `Emisor_RznSoc` varchar(255) DEFAULT NULL,
  `Emisor_GiroEmis` varchar(255) NOT NULL,
  `Emisor_Acteco` varchar(255) DEFAULT NULL,
  `Emisor_DirOrigen` varchar(255) NOT NULL,
  `Emisor_CmnaOrigen` varchar(255) NOT NULL,
  `Emisor_CiudadOrigen` varchar(255) NOT NULL,
  `Receptor_RUTRecep` varchar(255) NOT NULL,
  `Receptor_RznSocRecep` varchar(255) NOT NULL,
  `Receptor_GiroRecep` varchar(255) NOT NULL,
  `Receptor_DirRecep` varchar(255) NOT NULL,
  `Receptor_CmnaRecep` varchar(255) NOT NULL,
  `Receptor_CiudadRecep` varchar(50) NOT NULL,
  `Totales_MntNeto` varchar(50) NOT NULL,
  `Totales_MntExe` varchar(50) NOT NULL,
  `Totales_TasaIVA` varchar(50) NOT NULL,
  `Totales_MntTotal` varchar(50) NOT NULL,
  `TIPO` int(11) NOT NULL,
  PRIMARY KEY (`idNCND`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla iprove.ncnd: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `ncnd` DISABLE KEYS */;
/*!40000 ALTER TABLE `ncnd` ENABLE KEYS */;

-- Volcando estructura para tabla iprove.ncnd_detail
DROP TABLE IF EXISTS `ncnd_detail`;
CREATE TABLE IF NOT EXISTS `ncnd_detail` (
  `idncndDetail` int(11) NOT NULL AUTO_INCREMENT,
  `idNCND` int(11) NOT NULL,
  `NroLinDet` int(11) DEFAULT NULL,
  `NmbItem` varchar(255) DEFAULT NULL,
  `QtyItem` varchar(255) DEFAULT NULL,
  `UnmdItem` varchar(255) DEFAULT NULL,
  `PrcItem` varchar(255) DEFAULT NULL,
  `MontoItem` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`idncndDetail`),
  KEY `fk_ncnd_detail` (`idNCND`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla iprove.ncnd_detail: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `ncnd_detail` DISABLE KEYS */;
/*!40000 ALTER TABLE `ncnd_detail` ENABLE KEYS */;

-- Volcando estructura para tabla iprove.order
DROP TABLE IF EXISTS `order`;
CREATE TABLE IF NOT EXISTS `order` (
  `idOrder` int(11) NOT NULL AUTO_INCREMENT,
  `order` int(255) NOT NULL,
  `image` text,
  `idOrderCategory` int(11) DEFAULT NULL,
  `admision_date` date DEFAULT NULL,
  `amount` double DEFAULT '0',
  `invoiced_amount` double NOT NULL DEFAULT '0',
  `oc` text,
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `idCompany` int(11) DEFAULT NULL,
  `order_pdf` varchar(255) DEFAULT NULL,
  `paymentMethod` int(5) DEFAULT NULL,
  `addressPayment` varchar(255) DEFAULT NULL,
  `dayPayment` date DEFAULT NULL,
  `observation` text,
  PRIMARY KEY (`idOrder`),
  KEY `fk_order_1` (`idOrderCategory`) USING BTREE,
  KEY `fk_order_2` (`idCompany`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla iprove.order: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `order` DISABLE KEYS */;
/*!40000 ALTER TABLE `order` ENABLE KEYS */;

-- Volcando estructura para tabla iprove.order_category
DROP TABLE IF EXISTS `order_category`;
CREATE TABLE IF NOT EXISTS `order_category` (
  `idOrderCategory` int(11) NOT NULL AUTO_INCREMENT,
  `orderCategory` varchar(255) NOT NULL,
  `statusCategory` int(11) NOT NULL,
  PRIMARY KEY (`idOrderCategory`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla iprove.order_category: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `order_category` DISABLE KEYS */;
/*!40000 ALTER TABLE `order_category` ENABLE KEYS */;

-- Volcando estructura para tabla iprove.order_item
DROP TABLE IF EXISTS `order_item`;
CREATE TABLE IF NOT EXISTS `order_item` (
  `idOrderItem` int(11) NOT NULL AUTO_INCREMENT,
  `product` text,
  `price` float DEFAULT NULL,
  `coin` varchar(15) NOT NULL DEFAULT 'Pesos',
  `dateRequirement` int(11) DEFAULT NULL,
  `quantityRequirement` int(11) DEFAULT NULL,
  `subtotal` float DEFAULT NULL,
  `dateConfirmed` int(11) DEFAULT NULL,
  `quantityConfirmed` int(11) DEFAULT '1',
  `dateDelivery` int(11) DEFAULT NULL,
  `quantityDelivery` int(11) DEFAULT '1',
  `idOrder` int(11) DEFAULT NULL,
  `material` varchar(200) NOT NULL DEFAULT '',
  `descripcion` varchar(200) NOT NULL DEFAULT '',
  PRIMARY KEY (`idOrderItem`),
  KEY `idOrder` (`idOrder`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla iprove.order_item: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `order_item` DISABLE KEYS */;
/*!40000 ALTER TABLE `order_item` ENABLE KEYS */;

-- Volcando estructura para tabla iprove.parameters
DROP TABLE IF EXISTS `parameters`;
CREATE TABLE IF NOT EXISTS `parameters` (
  `idParameter` varchar(50) NOT NULL,
  `value` tinyint(4) NOT NULL DEFAULT '0',
  `stringValue` varchar(100) NOT NULL,
  `statusParameter` bit(1) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `detailParameter` varchar(200) NOT NULL,
  PRIMARY KEY (`idParameter`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Parametros del sistema. Activar o Inactivar sectores del sistema';

-- Volcando datos para la tabla iprove.parameters: ~4 rows (aproximadamente)
/*!40000 ALTER TABLE `parameters` DISABLE KEYS */;
REPLACE INTO `parameters` (`idParameter`, `value`, `stringValue`, `statusParameter`, `date`, `detailParameter`) VALUES
	('CIF-001', 1, 'No se tiene acceso', b'0', '2017-12-12 08:50:23', 'Mostrar u ocultar el botón "Consulta Individual de factura"'),
	('FE-001', 1, 'La página está cerrada, inténtelo mas tarde', b'1', '2017-12-12 08:50:23', 'Habilitar o deshabilitar funciones de todo el frontend"'),
	('IFM-001', 1, 'No puede importar facturas desde un excel.', b'1', '2018-09-05 13:49:07', 'Mostrar u ocultar el boton de Importar facturas'),
	('SUF-001', 1, 'No se requieren las facturas anteriores.', b'1', '2018-09-05 13:49:07', 'iproveRequerir facturas anteriores en la solicitud de usuario'),
	('SUN-001', 1, 'No se puede solicitar un nuevo usuario', b'1', '2018-01-15 17:21:43', 'Mostrar u ocultar el botón "Solicitud Usuario Nuevo"');
/*!40000 ALTER TABLE `parameters` ENABLE KEYS */;

-- Volcando estructura para tabla iprove.paymentmethod
DROP TABLE IF EXISTS `paymentmethod`;
CREATE TABLE IF NOT EXISTS `paymentmethod` (
  `idPaymentMethod` int(5) NOT NULL AUTO_INCREMENT,
  `description` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`idPaymentMethod`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla iprove.paymentmethod: ~4 rows (aproximadamente)
/*!40000 ALTER TABLE `paymentmethod` DISABLE KEYS */;
REPLACE INTO `paymentmethod` (`idPaymentMethod`, `description`) VALUES
	(1, 'Cheque'),
	(2, 'Efectivo'),
	(5, 'Transferencia Bancaria'),
	(6, 'otro');
/*!40000 ALTER TABLE `paymentmethod` ENABLE KEYS */;

-- Volcando estructura para tabla iprove.payment_type
DROP TABLE IF EXISTS `payment_type`;
CREATE TABLE IF NOT EXISTS `payment_type` (
  `idPaymentType` int(11) NOT NULL AUTO_INCREMENT,
  `paymentType` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`idPaymentType`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla iprove.payment_type: ~1 rows (aproximadamente)
/*!40000 ALTER TABLE `payment_type` DISABLE KEYS */;
REPLACE INTO `payment_type` (`idPaymentType`, `paymentType`) VALUES
	(1, 'Cheque');
/*!40000 ALTER TABLE `payment_type` ENABLE KEYS */;

-- Volcando estructura para tabla iprove.permissions
DROP TABLE IF EXISTS `permissions`;
CREATE TABLE IF NOT EXISTS `permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `controller` varchar(255) NOT NULL,
  `action` varchar(255) NOT NULL,
  `bizrule` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=210 DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla iprove.permissions: ~77 rows (aproximadamente)
/*!40000 ALTER TABLE `permissions` DISABLE KEYS */;
REPLACE INTO `permissions` (`id`, `name`, `description`, `controller`, `action`, `bizrule`) VALUES
	(1, 'Administración de actividades', 'Administración de actividades', 'activity', '*', NULL),
	(2, 'Administración de carreras', 'Administración de carreras', 'carrerr', '*', NULL),
	(3, 'Administración de ciudades', 'Administración de ciudades', 'city', '*', NULL),
	(4, 'Administración de empresas', 'Administración de empresas', 'company', '*', NULL),
	(5, 'Administración de idiomas', 'Administración de idiomas', 'language', '*', NULL),
	(6, 'Administración de institutos', 'Administración de institutos', 'institute', '*', NULL),
	(7, 'Administración de participantes', 'Administración de participantes', 'participant', '*', NULL),
	(8, 'Administración de programas', 'Administración de programas', 'program', '*', NULL),
	(9, 'Administración de puestos', 'Administración de puestos', 'position', '*', NULL),
	(10, 'Administración de informes', 'Administración de informes', 'workshop', '*', NULL),
	(11, 'Administración de países', 'Administración de países', 'country', '*', NULL),
	(12, 'Administración de provincias', 'Administración de provincias', 'province', '*', NULL),
	(13, 'Administración de rubros', 'Administración de rubros', 'companyCategory', '*', NULL),
	(14, 'Administración de tipos de programa', 'Administración de tipos de programa', 'programType', '*', NULL),
	(15, 'Administración de tipos de empresas - direcciones', 'Administración de tipos de empresas - direcciones', 'companyAddress', '*', NULL),
	(16, 'Administración de tipos de empresas - contacto', 'Administración de tipos de empresas - contacto', 'companyContact', '*', NULL),
	(17, 'Acceso cliente', 'Permite ver el estado de avance de los participantes del cliente', 'client', '*', NULL),
	(20, 'Admin consultores', 'Administra consultores', 'consultant', '*', NULL),
	(21, 'Acceso consultores', 'Acceso consultores', 'consultantAccess', '*', NULL),
	(22, 'Admin reportes', 'Permite ver los reportes de participantes', 'participantProgram', '*', NULL),
	(23, 'Admin usuarios', 'Administra consultores', 'user', '*', NULL),
	(24, 'Administra programas en curso', 'Administra programas en curso', 'programManagement', '*', NULL),
	(25, 'Admin recepción', 'Administra acciones de recepción', 'reception', '*', NULL),
	(30, 'Admin unidades de negocio', 'Administra unidades de negocio', 'businessUnit', '*', NULL),
	(100, 'Admin programas en curso - index', '', 'programManagement', 'index', NULL),
	(101, 'Admin programas en curso - checklist', '', 'programManagement', 'checklist', NULL),
	(102, 'Admin programas en curso - actividades detalle', '', 'programManagement', 'viewActivityDetail', NULL),
	(103, 'Admin programas en curso - reportes detalle', '', 'programManagement', 'viewReportDetail', NULL),
	(104, 'Admin programas en curso - agregar participantes', '', 'programManagement', 'addParticipantActivity', NULL),
	(105, 'Admin empresas - index empresas', '', 'company', 'index', NULL),
	(106, 'Admin empresas - ver empresa', '', 'company', 'view', NULL),
	(107, 'Facturas', '', 'invoice', 'index', NULL),
	(108, 'Admin participantes - ver participante', '', 'participant', 'view', NULL),
	(109, 'Admin actividades - index', '', 'activity', 'index', NULL),
	(110, 'Admin ocurrencias - ver ocurrencia', '', 'reception', 'occurrenceView', NULL),
	(111, 'Admin acciones participantes (reportes) - Ver programas sin oferta inicial', '', 'participantProgram', 'showProgramsWithoutOffer', NULL),
	(112, 'Admin acciones participantes (reportes) - Enviar programas sin oferta inicial', '', 'participantProgram', 'sendOffer', NULL),
	(113, 'Admin acciones participantes (reportes) - Ver programas sin reporte inicial', '', 'participantProgram', 'showProgramsWithoutInitialReport', NULL),
	(114, 'Admin acciones participantes (reportes) - Enviar programas sin reporte inicial', '', 'participantProgram', 'showProgramsWithoutOffer', NULL),
	(115, 'Admin acciones participantes (reportes) - Edición atributos programa', '', 'participantProgram', 'changeAttributeParticipantProgram', NULL),
	(116, 'Admin acciones participantes (reportes) - Ver archivo de reporte', '', 'participantProgram', 'getReportFile', NULL),
	(117, 'Admin acciones participantes (reportes) - Ver reporte por estado', '', 'participantProgram', 'reportByStatus', NULL),
	(118, 'Admin acciones participantes (reportes) - Ver reportes pendientes', '', 'participantProgram', 'pendingReports', NULL),
	(119, 'Admin programas en curso - agregar actividades personalizadas', '', 'programManagement', 'addCustomActivities', NULL),
	(120, 'Admin programas en curso - agregar reportes personalizados', '', 'programManagement', 'addCustomReport', NULL),
	(121, 'Admin programas en curso - cambiar status', '', 'programManagement', 'changeStatus', NULL),
	(122, 'Admin programas en curso - editar atributos', '', 'programManagement', 'editableParticipantProgram', NULL),
	(123, 'Admin programas en curso - subir archivos para actividades', '', 'programManagement', 'updateFilesParticipantActivity', NULL),
	(124, 'Admin programas en curso - subir archivos para informes', '', 'programManagement', 'updateFilesParticipantReport', NULL),
	(125, 'Admin programas en curso - obtener archivos para reportes', '', 'programManagement', 'getReportFile', NULL),
	(126, 'Admin programas en curso - enviar reportes', '', 'programManagement', 'sendReport', NULL),
	(127, 'Admin ocurrencias - ver ocurrencia (detail)', '', 'reception', 'occurrenceDetails', NULL),
	(128, 'Admin programas en curso - mostrar bitacora', '', 'programManagement', 'showInformationPiece', NULL),
	(129, 'Admin programas en curso - mostrar información de proceso', '', 'programManagement', 'showProcessInfo', NULL),
	(130, 'Admin programas en curso - mostrar reportes/actividades sin finalizar', '', 'programManagement', 'showNonCompletedItems', NULL),
	(135, 'Admin razones de desvinculación', '', 'exitReason', '*', NULL),
	(145, 'Admin modos reinserción', '', 'reinsertionMode', '*', NULL),
	(150, 'Admin programas en curso - enviar informes iniciales', '', 'programManagement', 'sendInitialReport', NULL),
	(160, 'Visualización contactos empresa', '', 'company', 'contactView', NULL),
	(161, 'Visualización direcciones empresa', '', 'company', 'addressView', NULL),
	(162, 'Agregar idiomas participante', '', 'participant', 'viewLanguages', NULL),
	(165, 'Visualización y carga CV participantes', '', 'participant', 'curriculum', NULL),
	(166, 'Visualización y carga CV participantes', '', 'participant', 'deleteCurriculum', NULL),
	(170, 'Permiso para cambiar la fecha de fin de programa de un participante', '', 'programManagement', 'changeEndDate', NULL),
	(171, 'Permiso para editar datos personales de los participantes', '', 'participant', 'update', NULL),
	(175, 'Permiso para ver los informes pendientes descartados', '', 'participantProgram', 'pendingReportsDiscarded', NULL),
	(176, 'Permiso para ver archivos en actividades', '', 'programManagement', 'updateFilesParticipantActivity', NULL),
	(200, 'Visualización de Reportes', '', 'report', '*', NULL),
	(201, 'Ordenes', '', 'order', '*', NULL),
	(202, 'orderitems', 'order item', 'OrderItem', '*', NULL),
	(203, 'Pagos / Facturas', 'Pagos / Facturas', 'invoice', '*', ''),
	(204, 'Administrado de Metodo de Pago', 'Administrador de Metodos de Pago', 'paymentMethod', '*', NULL),
	(205, 'Administración de Estados', 'Administración de Estados', 'orderCategory', '*', NULL),
	(206, 'Administración de estados de facturas', 'Administra estados de las facturas', 'InvoiceStatus', '*', NULL),
	(207, 'Administrador de SAP', 'Administracion de los parametros de la conexion a SAP', 'SapParameters', '*', NULL),
	(208, 'Administrador de emails', 'Administracion de los parametros de la conexion de emails', 'EmailParameters', '*', NULL),
	(209, 'Administrador de importaciones', 'Ver los logs de las importaciones', 'Imports', '*', NULL);
/*!40000 ALTER TABLE `permissions` ENABLE KEYS */;

-- Volcando estructura para tabla iprove.provider
DROP TABLE IF EXISTS `provider`;
CREATE TABLE IF NOT EXISTS `provider` (
  `idProvider` int(11) NOT NULL AUTO_INCREMENT,
  `idCompany` int(11) NOT NULL DEFAULT '0',
  `idUser` int(11) unsigned NOT NULL DEFAULT '0',
  `idComments` int(11) NOT NULL DEFAULT '0',
  `idProviderStatus` int(11) NOT NULL DEFAULT '0',
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `providerDeleted` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`idProvider`),
  KEY `FK_provider_company` (`idCompany`),
  KEY `FK_provider_user` (`idUser`),
  KEY `FK_provider_comments` (`idComments`),
  KEY `FK_provider_provider_status` (`idProviderStatus`),
  CONSTRAINT `FK_provider_company` FOREIGN KEY (`idCompany`) REFERENCES `company` (`idCompany`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_provider_provider_status` FOREIGN KEY (`idProviderStatus`) REFERENCES `provider_status` (`idProviderStatus`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1 COMMENT='Tabla de proveedores';

-- Volcando datos para la tabla iprove.provider: ~2 rows (aproximadamente)
/*!40000 ALTER TABLE `provider` DISABLE KEYS */;
REPLACE INTO `provider` (`idProvider`, `idCompany`, `idUser`, `idComments`, `idProviderStatus`, `date`, `providerDeleted`) VALUES
	(1, 1, 1, 0, 2, '2018-09-05 14:21:40', 0),
	(2, 1, 2, 0, 2, '2018-09-05 14:39:04', 0);
/*!40000 ALTER TABLE `provider` ENABLE KEYS */;

-- Volcando estructura para tabla iprove.provider_status
DROP TABLE IF EXISTS `provider_status`;
CREATE TABLE IF NOT EXISTS `provider_status` (
  `idProviderStatus` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(50) NOT NULL DEFAULT '0',
  PRIMARY KEY (`idProviderStatus`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1 COMMENT='Tabla de estado de proveedores';

-- Volcando datos para la tabla iprove.provider_status: ~6 rows (aproximadamente)
/*!40000 ALTER TABLE `provider_status` DISABLE KEYS */;
REPLACE INTO `provider_status` (`idProviderStatus`, `description`) VALUES
	(1, 'Solicitado'),
	(2, 'Activo'),
	(3, 'Inactivo'),
	(4, 'Sin acción'),
	(5, 'Suspendido'),
	(6, 'Rechazado');
/*!40000 ALTER TABLE `provider_status` ENABLE KEYS */;

-- Volcando estructura para tabla iprove.province
DROP TABLE IF EXISTS `province`;
CREATE TABLE IF NOT EXISTS `province` (
  `idProvince` int(11) NOT NULL AUTO_INCREMENT,
  `province` varchar(255) NOT NULL,
  `idCountry` int(11) NOT NULL,
  PRIMARY KEY (`idProvince`),
  KEY `fk_province_1` (`idCountry`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla iprove.province: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `province` DISABLE KEYS */;
/*!40000 ALTER TABLE `province` ENABLE KEYS */;

-- Volcando estructura para tabla iprove.purchase_order
DROP TABLE IF EXISTS `purchase_order`;
CREATE TABLE IF NOT EXISTS `purchase_order` (
  `idPurchaseOrder` int(11) NOT NULL,
  `number` int(25) NOT NULL,
  `image` varchar(255) NOT NULL,
  `idStatePurchaseOrder` int(11) NOT NULL,
  `idDetailItems` int(11) NOT NULL,
  `date` datetime NOT NULL,
  PRIMARY KEY (`idPurchaseOrder`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla iprove.purchase_order: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `purchase_order` DISABLE KEYS */;
/*!40000 ALTER TABLE `purchase_order` ENABLE KEYS */;

-- Volcando estructura para tabla iprove.roles
DROP TABLE IF EXISTS `roles`;
CREATE TABLE IF NOT EXISTS `roles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `visible` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla iprove.roles: ~2 rows (aproximadamente)
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
REPLACE INTO `roles` (`id`, `name`, `description`, `visible`) VALUES
	(15, 'Administrador Portal', 'Administrador Portal', 1),
	(18, 'Administrador General', 'Administrador General', 1);
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;

-- Volcando estructura para tabla iprove.roles_has_permissions
DROP TABLE IF EXISTS `roles_has_permissions`;
CREATE TABLE IF NOT EXISTS `roles_has_permissions` (
  `rolesId` int(11) NOT NULL,
  `permissionsId` int(11) NOT NULL,
  PRIMARY KEY (`rolesId`,`permissionsId`),
  KEY `fk_roles_has_permissions_2` (`permissionsId`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla iprove.roles_has_permissions: ~160 rows (aproximadamente)
/*!40000 ALTER TABLE `roles_has_permissions` DISABLE KEYS */;
REPLACE INTO `roles_has_permissions` (`rolesId`, `permissionsId`) VALUES
	(15, 21),
	(15, 100),
	(15, 101),
	(15, 102),
	(15, 103),
	(15, 104),
	(15, 107),
	(15, 108),
	(15, 121),
	(15, 124),
	(15, 125),
	(15, 126),
	(15, 128),
	(15, 129),
	(15, 130),
	(15, 165),
	(15, 166),
	(15, 170),
	(15, 171),
	(15, 176),
	(16, 7),
	(16, 100),
	(16, 101),
	(16, 102),
	(16, 103),
	(16, 111),
	(16, 112),
	(16, 113),
	(16, 114),
	(16, 115),
	(16, 116),
	(16, 117),
	(16, 128),
	(16, 129),
	(16, 130),
	(16, 150),
	(17, 7),
	(17, 100),
	(17, 101),
	(17, 102),
	(17, 103),
	(17, 104),
	(17, 118),
	(17, 119),
	(17, 120),
	(17, 121),
	(17, 122),
	(17, 123),
	(17, 124),
	(17, 125),
	(17, 126),
	(17, 128),
	(17, 129),
	(17, 130),
	(18, 1),
	(18, 2),
	(18, 3),
	(18, 4),
	(18, 5),
	(18, 6),
	(18, 7),
	(18, 8),
	(18, 9),
	(18, 10),
	(18, 11),
	(18, 12),
	(18, 13),
	(18, 14),
	(18, 15),
	(18, 16),
	(18, 20),
	(18, 21),
	(18, 22),
	(18, 23),
	(18, 24),
	(18, 25),
	(18, 30),
	(18, 135),
	(18, 145),
	(18, 175),
	(18, 200),
	(18, 201),
	(18, 202),
	(18, 203),
	(18, 204),
	(18, 205),
	(18, 206),
	(18, 207),
	(18, 208),
	(18, 209),
	(19, 1),
	(19, 2),
	(19, 3),
	(19, 4),
	(19, 5),
	(19, 6),
	(19, 7),
	(19, 8),
	(19, 9),
	(19, 10),
	(19, 11),
	(19, 12),
	(19, 13),
	(19, 14),
	(19, 15),
	(19, 16),
	(19, 17),
	(20, 1),
	(20, 2),
	(20, 3),
	(20, 4),
	(20, 5),
	(20, 6),
	(20, 7),
	(20, 8),
	(20, 9),
	(20, 10),
	(20, 11),
	(20, 12),
	(20, 13),
	(20, 14),
	(20, 15),
	(20, 16),
	(21, 22),
	(22, 25),
	(22, 160),
	(22, 161),
	(22, 162),
	(22, 165),
	(22, 166),
	(50, 105),
	(50, 106),
	(50, 107),
	(50, 108),
	(50, 109),
	(50, 110),
	(50, 127),
	(50, 128),
	(50, 129),
	(50, 130),
	(50, 160),
	(50, 161),
	(50, 162),
	(50, 165),
	(50, 166),
	(51, 100),
	(51, 101),
	(51, 107),
	(51, 108),
	(55, 135),
	(60, 145),
	(61, 201),
	(61, 202),
	(61, 203),
	(62, 201),
	(62, 202),
	(62, 203),
	(63, 201),
	(63, 202),
	(63, 203);
/*!40000 ALTER TABLE `roles_has_permissions` ENABLE KEYS */;

-- Volcando estructura para tabla iprove.sap_parameters
DROP TABLE IF EXISTS `sap_parameters`;
CREATE TABLE IF NOT EXISTS `sap_parameters` (
  `idSapParameters` int(11) NOT NULL AUTO_INCREMENT,
  `ashost` varchar(20) NOT NULL,
  `sysnr` varchar(10) DEFAULT NULL,
  `client` varchar(10) DEFAULT NULL,
  `lang` varchar(10) DEFAULT NULL,
  `user` varchar(50) NOT NULL,
  `passwd` varchar(200) NOT NULL,
  `function` varchar(50) NOT NULL,
  PRIMARY KEY (`idSapParameters`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla iprove.sap_parameters: ~1 rows (aproximadamente)
/*!40000 ALTER TABLE `sap_parameters` DISABLE KEYS */;
REPLACE INTO `sap_parameters` (`idSapParameters`, `ashost`, `sysnr`, `client`, `lang`, `user`, `passwd`, `function`) VALUES
	(1, '0.0.0.0', '00', '100', 'es', 'user', 'psw', 'ZOPT_RFC_PORTAL_VIM');
/*!40000 ALTER TABLE `sap_parameters` ENABLE KEYS */;

-- Volcando estructura para tabla iprove.user
DROP TABLE IF EXISTS `user`;
CREATE TABLE IF NOT EXISTS `user` (
  `idUser` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `salt` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `passwordStrategy` varchar(255) DEFAULT NULL,
  `requiresNewPassword` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `lastLoginAt` datetime DEFAULT NULL,
  `lastActiveAt` datetime DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0',
  `signature` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `idCompany` int(11) DEFAULT NULL,
  `idConsultant` int(11) DEFAULT NULL,
  `picture` text,
  `sex` tinyint(1) NOT NULL DEFAULT '0',
  `token` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`idUser`),
  KEY `fk_user_company` (`idCompany`) USING BTREE,
  KEY `fk_user_consultant_1` (`idConsultant`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla iprove.user: ~2 rows (aproximadamente)
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
REPLACE INTO `user` (`idUser`, `name`, `salt`, `password`, `passwordStrategy`, `requiresNewPassword`, `lastLoginAt`, `lastActiveAt`, `status`, `signature`, `email`, `idCompany`, `idConsultant`, `picture`, `sex`, `token`) VALUES
	(1, 'Usuario 1', '$2a$12$yX/UyMwSmYh11t0qL/o/hA', '$2a$12$yX/UyMwSmYh11t0qL/o/h.me.e6HsfNkm67Hk0kUNbM2shF7R2YZ.', 'bcrypt', 0, '2018-09-05 16:04:50', '2018-09-05 16:04:28', 0, NULL, 'info@portal.com', NULL, NULL, NULL, 0, '8c595c554a3a3ecc4b2940a2da703706142b6a55');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;

-- Volcando estructura para tabla iprove.user_has_roles
DROP TABLE IF EXISTS `user_has_roles`;
CREATE TABLE IF NOT EXISTS `user_has_roles` (
  `idUser` int(11) NOT NULL,
  `rolesId` int(11) NOT NULL,
  PRIMARY KEY (`idUser`,`rolesId`),
  KEY `fk_user_has_permissions_1` (`rolesId`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla iprove.user_has_roles: ~13 rows (aproximadamente)
/*!40000 ALTER TABLE `user_has_roles` DISABLE KEYS */;
REPLACE INTO `user_has_roles` (`idUser`, `rolesId`) VALUES
	(1, 15),
	(1, 16),
	(1, 17),
	(1, 18),
	(1, 19),
	(1, 20),
	(1, 21),
	(1, 22),
	(1, 50),
	(1, 51),
	(1, 55),
	(1, 60),
	(2, 15);
/*!40000 ALTER TABLE `user_has_roles` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;

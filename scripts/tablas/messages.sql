CREATE TABLE `messages` (
	`idMessage` INT NOT NULL,
	`idEmisor` INT NULL,
	`idReceptor` INT NULL,
	`message` VARCHAR(255) NULL,
	`date` DATE NULL,
	`messageType` INT NULL,
	`status` INT NULL,
	PRIMARY KEY (`idMessage`)
)
COLLATE='latin1_swedish_ci'
;

CREATE TABLE `message_type` (
	`idMessagetype` INT NULL,
	`description` VARCHAR(255) NULL
)
COLLATE='latin1_swedish_ci'
;

INSERT INTO `iprove`.`message_type` (`idMessagetype`, `description`) VALUES ('1', 'privado');
INSERT INTO `iprove`.`message_type` (`idMessagetype`, `description`) VALUES ('2', 'difusion_general');

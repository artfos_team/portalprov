/*CREATE TABLE `prov_calificaciones` (
	`idCalif` INT NOT NULL AUTO_INCREMENT,
	`idProvider` INT NULL DEFAULT '0',
	`calification` INT NULL DEFAULT '0',
	`comment` VARCHAR(500) NULL DEFAULT NULL,
	`date` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
	PRIMARY KEY (`idCalif`)
)
COLLATE='latin1_swedish_ci'
;*/

ALTER TABLE `provider`
	ADD COLUMN `calification` INT(11) NOT NULL DEFAULT '0' AFTER `providerDeleted`;
	
	ALTER TABLE `provider`
	ADD COLUMN `reasonCalification` INT(11) NULL DEFAULT '0' AFTER `calification`;
	
	
	ALTER TABLE `company`
	ADD COLUMN `calification` INT NULL DEFAULT '0' AFTER `billingAddress`,
	ADD COLUMN `reasonCalification` VARCHAR(255) NULL DEFAULT NULL AFTER `calification`;
	
	
INSERT INTO `iprove`.`permissions` (`id`, `name`, `description`, `controller`, `action`) VALUES ('211', 'Modificacion de calificaciones', 'Modificar calificaciones a los proveedores', 'company', 'viewCalification');

INSERT INTO `iprove`.`roles_has_permissions` (`rolesId`, `permissionsId`) VALUES ('20', '211');	

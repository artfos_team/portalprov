CREATE TABLE `buyorder` (
	`nic` INT(11) NOT NULL DEFAULT '0',
	`monto` INT(11) NOT NULL DEFAULT '0',
	`fecha_emision` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
	`fecha_vencimiento` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
	`porcentajeAnticipo` INT(11) NOT NULL DEFAULT '0',
	`tieneAnticipo` TINYINT(4) NOT NULL DEFAULT '0',
	`pdf` VARCHAR(255) NOT NULL,
	`statusId` INT(11) NOT NULL,
	`idCompany` INT(11) NOT NULL,
	`view` INT(11) NOT NULL DEFAULT '0',
	PRIMARY KEY (`nic`)
)
COLLATE='latin1_swedish_ci'
ENGINE=InnoDB
;

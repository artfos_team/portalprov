CREATE TABLE `rubros` (
	`idRubro` INT NOT NULL AUTO_INCREMENT,
	`nombre` VARCHAR(50) NOT NULL,
	`status` VARCHAR(50) NOT NULL DEFAULT '1',
	PRIMARY KEY (`idRubro`)
)
COLLATE='latin1_swedish_ci'
;

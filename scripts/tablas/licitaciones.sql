CREATE TABLE `licitaciones` (
	`idLicitacion` INT(8) NOT NULL AUTO_INCREMENT,
	`nombre` VARCHAR(25) NULL,
	`descripcion` VARCHAR(1000) NULL,
	`fecha_inicio` DATETIME NULL,
	`fecha_fin` DATETIME NULL,
	`idRubro` INT NULL,
	PRIMARY KEY (`idLicitacion`)
)
COLLATE='latin1_swedish_ci'
;

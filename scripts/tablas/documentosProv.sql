CREATE TABLE `document` (
	`documentName` VARCHAR(255) NULL,
	`required` INT NULL,
	`active` INT NULL
)
COLLATE='latin1_swedish_ci'
;


ALTER TABLE `document`
	CHANGE COLUMN `documentName` `idDocument` INT NOT NULL AUTO_INCREMENT FIRST,
	ADD COLUMN `documentName` VARCHAR(255) NULL DEFAULT NULL AFTER `idDocument`,
	ADD PRIMARY KEY (`idDocument`);


INSERT INTO `iprove`.`permissions` (`name`, `description`, `controller`, `action`) VALUES ('Administracion de Documentos', 'Administrar los documentos de los proveedores', 'document', '*');
INSERT INTO `iprove`.`roles_has_permissions` (`rolesId`, `permissionsId`) VALUES ('18', '212');

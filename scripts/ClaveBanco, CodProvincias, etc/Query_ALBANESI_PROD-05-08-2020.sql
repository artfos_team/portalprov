UPDATE `iprove`.`province` SET `codRegion`='00' WHERE  `idProvince`=1;
UPDATE `iprove`.`province` SET `codRegion`='01' WHERE  `idProvince`=2;
UPDATE `iprove`.`province` SET `codRegion`='02' WHERE  `idProvince`=3;
UPDATE `iprove`.`province` SET `codRegion`='03' WHERE  `idProvince`=4;
UPDATE `iprove`.`province` SET `codRegion`='04' WHERE  `idProvince`=5;
UPDATE `iprove`.`province` SET `codRegion`='05' WHERE  `idProvince`=6;
UPDATE `iprove`.`province` SET `codRegion`='06' WHERE  `idProvince`=7;
UPDATE `iprove`.`province` SET `codRegion`='07' WHERE  `idProvince`=8;
UPDATE `iprove`.`province` SET `codRegion`='08' WHERE  `idProvince`=9;
UPDATE `iprove`.`province` SET `codRegion`='09' WHERE  `idProvince`=10;


ALTER TABLE `bank`
	ADD COLUMN `claveBanco` VARCHAR(50) NULL DEFAULT NULL AFTER `bank`;

UPDATE `iprove`.`bank` SET `claveBanco`='GALICIA' WHERE  `idBank`=81;
UPDATE `iprove`.`bank` SET `claveBanco`='BNA' WHERE  `idBank`=82;
UPDATE `iprove`.`bank` SET `claveBanco`='BAPRO' WHERE  `idBank`=83;
UPDATE `iprove`.`bank` SET `claveBanco`='BBVA' WHERE  `idBank`=86;
UPDATE `iprove`.`bank` SET `claveBanco`='BANCOR' WHERE  `idBank`=87;
UPDATE `iprove`.`bank` SET `claveBanco`='BMR' WHERE  `idBank`=93;
UPDATE `iprove`.`bank` SET `claveBanco`='BPNE' WHERE  `idBank`=99;
UPDATE `iprove`.`bank` SET `claveBanco`='BST' WHERE  `idBank`=132;
UPDATE `iprove`.`bank` SET `claveBanco`='CHUBUT' WHERE  `idBank`=95;
UPDATE `iprove`.`bank` SET `claveBanco`='CITIBANK' WHERE  `idBank`=85;
UPDATE `iprove`.`bank` SET `claveBanco`='CIUDAD' WHERE  `idBank`=89;
UPDATE `iprove`.`bank` SET `claveBanco`='CMF' WHERE  `idBank`=125;
UPDATE `iprove`.`bank` SET `claveBanco`='COINAG' WHERE  `idBank`=140;
UPDATE `iprove`.`bank` SET `claveBanco`='COLUMBIA' WHERE  `idBank`=138;
UPDATE `iprove`.`bank` SET `claveBanco`='COMAFI' WHERE  `idBank`=116;
UPDATE `iprove`.`bank` SET `claveBanco`='CREDICOOP' WHERE  `idBank`=104;
UPDATE `iprove`.`bank` SET `claveBanco`='HIPOTECARIO' WHERE  `idBank`=91;
UPDATE `iprove`.`bank` SET `claveBanco`='HSBC' WHERE  `idBank`=102;
UPDATE `iprove`.`bank` SET `claveBanco`='ICBC' WHERE  `idBank`=84;
UPDATE `iprove`.`bank` SET `claveBanco`='INDUSTRIAL' WHERE  `idBank`=127;
UPDATE `iprove`.`bank` SET `claveBanco`='INTERFINANZA' WHERE  `idBank`=101;
UPDATE `iprove`.`bank` SET `claveBanco`='ITAU' WHERE  `idBank`=108;
UPDATE `iprove`.`bank` SET `claveBanco`='JP MORGAN' WHERE  `idBank`=103;
UPDATE `iprove`.`bank` SET `claveBanco`='MACRO' WHERE  `idBank`=115;
UPDATE `iprove`.`bank` SET `claveBanco`='MARIVA' WHERE  `idBank`=107;
UPDATE `iprove`.`bank` SET `claveBanco`='MERIDIAN' WHERE  `idBank`=114;
UPDATE `iprove`.`bank` SET `claveBanco`='NBLR' WHERE  `idBank`=120;
UPDATE `iprove`.`bank` SET `claveBanco`='PAMPA' WHERE  `idBank`=97;
UPDATE `iprove`.`bank` SET `claveBanco`='PATAGONIA' WHERE  `idBank`=90;
UPDATE `iprove`.`bank` SET `claveBanco`='SANTA FE' WHERE  `idBank`=128;
UPDATE `iprove`.`bank` SET `claveBanco`='SANTANDER' WHERE  `idBank`=94;
UPDATE `iprove`.`bank` SET `claveBanco`='SUPERVIELLE' WHERE  `idBank`=88;
INSERT INTO `iprove`.`bank` (`bank`, `claveBanco`) VALUES ('BANCO FINANSUR S.A', 'FINANSUR');



ALTER TABLE `company_jurisdiction_data`
	CHANGE COLUMN `coefficient` `coefficient` VARCHAR(20) NULL DEFAULT NULL COLLATE 'latin1_swedish_ci' AFTER `idTaxData`;

INSERT INTO `iprove`.`rubros` (`nombre`) VALUES ('139900: Fabricación de productos textiles n.c.p.');
INSERT INTO `iprove`.`rubros` (`nombre`) VALUES ('201180: Fabricación de materias quíimicas inorgánicas básicas n.c.p.');
INSERT INTO `iprove`.`rubros` (`nombre`) VALUES ('201190: Fabricación de materias quí­micas orgánicas básicas n.c.p.');
INSERT INTO `iprove`.`rubros` (`nombre`) VALUES ('201409: Fabricación de materias plásticas en formas primarias n.c.p.');
INSERT INTO `iprove`.`rubros` (`nombre`) VALUES ('202908: Fabricación de productos qui­micos n.c.p.');
INSERT INTO `iprove`.`rubros` (`nombre`) VALUES ('210090: Fabricación de productos de laboratorio y productos botánicos de uso farmacéutico n.c.p.');
INSERT INTO `iprove`.`rubros` (`nombre`) VALUES ('221909: Fabricación  de productos de caucho n.c.p.');
INSERT INTO `iprove`.`rubros` (`nombre`) VALUES ('222090: Fabricación de productos plásticos en formas básicas y artí­culos de plástico n.c.p., excepto muebles');
INSERT INTO `iprove`.`rubros` (`nombre`) VALUES ('231090: Fabricación de productos de vidrio n.c.p.');
INSERT INTO `iprove`.`rubros` (`nombre`) VALUES ('239209: Fabricación de productos de arcilla y cerámica no refractaria para uso estructural n.c.p.');
INSERT INTO `iprove`.`rubros` (`nombre`) VALUES ('239399: Fabricación de artíiculos de cerámica no refractaria para uso no estructural n.c.p.');
INSERT INTO `iprove`.`rubros` (`nombre`) VALUES ('239900: Fabricación de productos minerales no metálicos n.c.p.');
INSERT INTO `iprove`.`rubros` (`nombre`) VALUES ('241009: Fabricación en industrias básicas de productos de hierro y acero n.c.p.');
INSERT INTO `iprove`.`rubros` (`nombre`) VALUES ('242090: Fabricación de productos primarios de metales preciosos y metales no ferrosos n.c.p. y sus semielaborados');
INSERT INTO `iprove`.`rubros` (`nombre`) VALUES ('259309: Fabricación de cerraduras, herrajes y artí­culos de ferreterí­a n.c.p.');
INSERT INTO `iprove`.`rubros` (`nombre`) VALUES ('259999: Fabricación de productos elaborados de metal n.c.p.');
INSERT INTO `iprove`.`rubros` (`nombre`) VALUES ('266090: Fabricación de equipo médico y quirúrgico y de aparatos ortopédicos n.c.p.');
INSERT INTO `iprove`.`rubros` (`nombre`) VALUES ('273190: Fabricación de hilos y cables aislados n.c.p.');
INSERT INTO `iprove`.`rubros` (`nombre`) VALUES ('275099: Fabricación de aparatos de uso doméstico n.c.p.');
INSERT INTO `iprove`.`rubros` (`nombre`) VALUES ('279000: Fabricación  de equipo eléctrico n.c.p.');
INSERT INTO `iprove`.`rubros` (`nombre`) VALUES ('281900: Fabricación de  maquinaria y equipo de uso general n.c.p.');
INSERT INTO `iprove`.`rubros` (`nombre`) VALUES ('282909: Fabricación de maquinaria y equipo de uso especial n.c.p.');
INSERT INTO `iprove`.`rubros` (`nombre`) VALUES ('293090: Fabricación de partes, piezas y accesorios para vehí­culos automotores y sus motores n.c.p.');
INSERT INTO `iprove`.`rubros` (`nombre`) VALUES ('309900: Fabricación de equipo de transporte n.c.p.');
INSERT INTO `iprove`.`rubros` (`nombre`) VALUES ('329090: Industrias manufactureras n.c.p.');
INSERT INTO `iprove`.`rubros` (`nombre`) VALUES ('331290: Reparación y mantenimiento de maquinaria de uso especial n.c.p.');
INSERT INTO `iprove`.`rubros` (`nombre`) VALUES ('331900: Reparación y mantenimiento de máquinas y equipo n.c.p.');
INSERT INTO `iprove`.`rubros` (`nombre`) VALUES ('351190: Generación de energí­a n.c.p.');
INSERT INTO `iprove`.`rubros` (`nombre`) VALUES ('429090: Construcción de obras de ingenierí­a civil n.c.p.');
INSERT INTO `iprove`.`rubros` (`nombre`) VALUES ('432190: Instalación, ejecución y mantenimiento de instalaciones eléctricas, electromecánicas y electrónicas n.c.p.');
INSERT INTO `iprove`.`rubros` (`nombre`) VALUES ('432990: Instalaciones para edificios y obras de ingenierí­a civil n.c.p.');
INSERT INTO `iprove`.`rubros` (`nombre`) VALUES ('433090: Terminación de edificios n.c.p.');
INSERT INTO `iprove`.`rubros` (`nombre`) VALUES ('439990: Actividades especializadas de construcción n.c.p.');
INSERT INTO `iprove`.`rubros` (`nombre`) VALUES ('451190: Venta de vehí­culos automotores nuevos n.c.p.');
INSERT INTO `iprove`.`rubros` (`nombre`) VALUES ('451290: Venta de vehí­culos automotores usados n.c.p.');
INSERT INTO `iprove`.`rubros` (`nombre`) VALUES ('452990: Mantenimiento y reparación del motor n.c.p., mecánica integral');
INSERT INTO `iprove`.`rubros` (`nombre`) VALUES ('453291: Venta al por menor de partes, piezas y accesorios nuevos n.c.p.');
INSERT INTO `iprove`.`rubros` (`nombre`) VALUES ('453292: Venta al por menor de partes, piezas y accesorios usados n.c.p.');
INSERT INTO `iprove`.`rubros` (`nombre`) VALUES ('461019: Venta al por mayor en comisión o consignación de productos agrí­colas n.c.p.');
INSERT INTO `iprove`.`rubros` (`nombre`) VALUES ('461029: Venta al por mayor en comisión o consignación de productos pecuarios n.c.p.');
INSERT INTO `iprove`.`rubros` (`nombre`) VALUES ('461039: Venta al por mayor en comisión o consignación de alimentos, bebidas y tabaco n.c.p.');
INSERT INTO `iprove`.`rubros` (`nombre`) VALUES ('461099: Venta al por mayor en comisión o consignación de  mercaderí­as n.c.p.');
INSERT INTO `iprove`.`rubros` (`nombre`) VALUES ('462190: Venta al por mayor de materias primas agrí­colas y de la silvicultura n.c.p.');
INSERT INTO `iprove`.`rubros` (`nombre`) VALUES ('462209: Venta al por mayor de materias primas pecuarias n.c.p. incluso animales vivos');
INSERT INTO `iprove`.`rubros` (`nombre`) VALUES ('463129: Venta al por mayor de aves, huevos y productos de granja y de la caza n.c.p.');
INSERT INTO `iprove`.`rubros` (`nombre`) VALUES ('463159: Venta al por mayor de productos y subproductos de molinerí­a n.c.p.');
INSERT INTO `iprove`.`rubros` (`nombre`) VALUES ('463160: Venta al por mayor de chocolates, golosinas y productos para kioscos y polirrubros n.c.p., excepto cigarrillos');
INSERT INTO `iprove`.`rubros` (`nombre`) VALUES ('463199: Venta al por mayor de productos alimenticios n.c.p.');
INSERT INTO `iprove`.`rubros` (`nombre`) VALUES ('463219: Venta al por mayor de bebidas alcohólicas n.c.p.');
INSERT INTO `iprove`.`rubros` (`nombre`) VALUES ('464119: Venta al por mayor de productos textiles n.c.p.');
INSERT INTO `iprove`.`rubros` (`nombre`) VALUES ('464129: Venta al por mayor de prendas y accesorios de vestir n.c.p., excepto uniformes y ropa de trabajo');
INSERT INTO `iprove`.`rubros` (`nombre`) VALUES ('464149: Venta al por mayor de artí­culos de marroquinerí­a,  paraguas y productos similares n.c.p.');
INSERT INTO `iprove`.`rubros` (`nombre`) VALUES ('464999: Venta al por mayor de artí­culos de uso doméstico o personal n.c.p');
INSERT INTO `iprove`.`rubros` (`nombre`) VALUES ('465390: Venta al por mayor de máquinas, equipos e implementos de uso especial n.c.p.');
INSERT INTO `iprove`.`rubros` (`nombre`) VALUES ('465690: Venta al por mayor de muebles e instalaciones para la industria, el comercio y los servicios n.c.p.');
INSERT INTO `iprove`.`rubros` (`nombre`) VALUES ('465930: Venta al por mayor de equipo profesional y cientí­fico e instrumentos de medida y de control n.c.p.');
INSERT INTO `iprove`.`rubros` (`nombre`) VALUES ('465990: Venta al por mayor de máquinas, equipo y materiales conexos n.c.p.');
INSERT INTO `iprove`.`rubros` (`nombre`) VALUES ('466399: Venta al por mayor de artí­culos para la construcción n.c.p.');
INSERT INTO `iprove`.`rubros` (`nombre`) VALUES ('466910: Venta al por mayor de productos intermedios n.c.p., desperdicios y desechos textiles');
INSERT INTO `iprove`.`rubros` (`nombre`) VALUES ('466920: Venta al por mayor de productos intermedios n.c.p., desperdicios y desechos de papel y cartón');
INSERT INTO `iprove`.`rubros` (`nombre`) VALUES ('466939: Venta al por mayor de productos intermedios, desperdicios y desechos de vidrio, caucho, goma y quí­micos n.c.p.');
INSERT INTO `iprove`.`rubros` (`nombre`) VALUES ('466940: Venta al por mayor de productos intermedios n.c.p., desperdicios y desechos metálicos');
INSERT INTO `iprove`.`rubros` (`nombre`) VALUES ('466990: Venta al por mayor de productos intermedios, desperdicios y desechos n.c.p.');
INSERT INTO `iprove`.`rubros` (`nombre`) VALUES ('469090: Venta al por mayor de mercancí­as n.c.p.');
INSERT INTO `iprove`.`rubros` (`nombre`) VALUES ('471190: Venta al por menor en kioscos, polirrubros y comercios no especializados n.c.p.');
INSERT INTO `iprove`.`rubros` (`nombre`) VALUES ('472190: Venta al por menor de productos alimenticios n.c.p., en comercios especializados');
INSERT INTO `iprove`.`rubros` (`nombre`) VALUES ('475190: Venta al por menor de artí­culos textiles n.c.p. excepto prendas de vestir');
INSERT INTO `iprove`.`rubros` (`nombre`) VALUES ('475290: Venta al por menor de materiales de construcción n.c.p.');
INSERT INTO `iprove`.`rubros` (`nombre`) VALUES ('475490: Venta al por menor de artí­culos para el hogar n.c.p.');
INSERT INTO `iprove`.`rubros` (`nombre`) VALUES ('477190: Venta al por menor de prendas y accesorios de vestir n.c.p.');
INSERT INTO `iprove`.`rubros` (`nombre`) VALUES ('477290: Venta al por menor de artí­culos de marroquinerí­a, paraguas y similares n.c.p.');
INSERT INTO `iprove`.`rubros` (`nombre`) VALUES ('477490: Venta al por menor de artí­culos nuevos n.c.p.');
INSERT INTO `iprove`.`rubros` (`nombre`) VALUES ('477890: Venta al por menor de artí­culos usados n.c.p. excepto+E1155 automotores y motocicletas');
INSERT INTO `iprove`.`rubros` (`nombre`) VALUES ('478090: Venta al por menor de productos n.c.p. en puestos móviles y mercados');
INSERT INTO `iprove`.`rubros` (`nombre`) VALUES ('479109: Venta al por menor por correo, televisión y otros medios de comunicación n.c.p.');
INSERT INTO `iprove`.`rubros` (`nombre`) VALUES ('479900: Venta al por menor no realizada en establecimientos  n.c.p.');
INSERT INTO `iprove`.`rubros` (`nombre`) VALUES ('492190: Servicio de transporte automotor de pasajeros n.c.p.');
INSERT INTO `iprove`.`rubros` (`nombre`) VALUES ('492229: Servicio de transporte automotor de mercaderí­as a granel n.c.p.');
INSERT INTO `iprove`.`rubros` (`nombre`) VALUES ('492280: Servicio de transporte automotor urbano de carga n.c.p.');
INSERT INTO `iprove`.`rubros` (`nombre`) VALUES ('492290: Servicio de transporte automotor de cargas n.c.p.');
INSERT INTO `iprove`.`rubros` (`nombre`) VALUES ('522099: Servicios de almacenamiento y depósito n.c.p.');
INSERT INTO `iprove`.`rubros` (`nombre`) VALUES ('523019: Servicios de gestión aduanera para el transporte de mercaderí­as n.c.p.');
INSERT INTO `iprove`.`rubros` (`nombre`) VALUES ('523039: Servicios de operadores logí­sticos n.c.p.');
INSERT INTO `iprove`.`rubros` (`nombre`) VALUES ('523090: Servicios de gestión y logí­stica para el transporte de mercaderí­as n.c.p.');
INSERT INTO `iprove`.`rubros` (`nombre`) VALUES ('524190: Servicios complementarios para el transporte terrestre n.c.p.');
INSERT INTO `iprove`.`rubros` (`nombre`) VALUES ('524290: Servicios complementarios para el transporte marí­timo n.c.p.');
INSERT INTO `iprove`.`rubros` (`nombre`) VALUES ('524390: Servicios complementarios para el transporte aéreo n.c.p.');
INSERT INTO `iprove`.`rubros` (`nombre`) VALUES ('551090: Servicios de hospedaje temporal n.c.p.');
INSERT INTO `iprove`.`rubros` (`nombre`) VALUES ('561019: Servicios de expendio de comidas y bebidas en establecimientos con servicio de mesa y/o en mostrador n.c.p.');
INSERT INTO `iprove`.`rubros` (`nombre`) VALUES ('562099: Servicios de comidas n.c.p.');
INSERT INTO `iprove`.`rubros` (`nombre`) VALUES ('581900: Edición n.c.p.');
INSERT INTO `iprove`.`rubros` (`nombre`) VALUES ('602900: Servicios de televisión n.c.p');
INSERT INTO `iprove`.`rubros` (`nombre`) VALUES ('614090: Servicios de telecomunicación ví­a internet n.c.p.');
INSERT INTO `iprove`.`rubros` (`nombre`) VALUES ('619000: Servicios de telecomunicaciones n.c.p.');
INSERT INTO `iprove`.`rubros` (`nombre`) VALUES ('620900: Servicios de informática n.c.p.');
INSERT INTO `iprove`.`rubros` (`nombre`) VALUES ('631190: Actividades conexas al procesamiento y hospedaje de datos n.c.p.');
INSERT INTO `iprove`.`rubros` (`nombre`) VALUES ('639900: Servicios de información n.c.p.');
INSERT INTO `iprove`.`rubros` (`nombre`) VALUES ('643009: Fondos y sociedades de inversión y entidades financieras similares n.c.p.');
INSERT INTO `iprove`.`rubros` (`nombre`) VALUES ('649290: Servicios de crédito n.c.p.');
INSERT INTO `iprove`.`rubros` (`nombre`) VALUES ('649999: Servicios de financiación y actividades financieras n.c.p.');
INSERT INTO `iprove`.`rubros` (`nombre`) VALUES ('661999: Servicios auxiliares a la intermediación financiera n.c.p.');
INSERT INTO `iprove`.`rubros` (`nombre`) VALUES ('662090: Servicios auxiliares a los servicios de seguros n.c.p.');
INSERT INTO `iprove`.`rubros` (`nombre`) VALUES ('681098: Servicios inmobiliarios realizados por cuenta propia, con bienes urbanos propios o arrendados n.c.p.');
INSERT INTO `iprove`.`rubros` (`nombre`) VALUES ('681099: Servicios inmobiliarios realizados por cuenta propia, con bienes rurales propios o arrendados n.c.p.');
INSERT INTO `iprove`.`rubros` (`nombre`) VALUES ('682099: Servicios inmobiliarios realizados a cambio de una retribución o por contrata n.c.p.');
INSERT INTO `iprove`.`rubros` (`nombre`) VALUES ('702099: Servicios de asesoramiento, dirección y gestión empresarial n.c.p.');
INSERT INTO `iprove`.`rubros` (`nombre`) VALUES ('711009: Servicios de arquitectura e ingenieria y servicios conexos de asesoramiento técnico n.c.p.');
INSERT INTO `iprove`.`rubros` (`nombre`) VALUES ('721090: Investigación y desarrollo experimental en el campo de las ciencias exactas y naturales n.c.p.');
INSERT INTO `iprove`.`rubros` (`nombre`) VALUES ('731009: Servicios de publicidad n.c.p.');
INSERT INTO `iprove`.`rubros` (`nombre`) VALUES ('749009: Actividades profesionales, cientificas y técnicas n.c.p.');
INSERT INTO `iprove`.`rubros` (`nombre`) VALUES ('771190: Alquiler de vehí­culos automotores n.c.p., sin conductor ni operarios');
INSERT INTO `iprove`.`rubros` (`nombre`) VALUES ('771290: Alquiler de equipo de transporte n.c.p. sin conductor ni operarios');
INSERT INTO `iprove`.`rubros` (`nombre`) VALUES ('772099: Alquiler de efectos personales y enseres domésticos n.c.p.');
INSERT INTO `iprove`.`rubros` (`nombre`) VALUES ('773090: Alquiler de maquinaria y equipo n.c.p., sin personal');
INSERT INTO `iprove`.`rubros` (`nombre`) VALUES ('791909: Servicios complementarios de apoyo turi­stico n.c.p.');
INSERT INTO `iprove`.`rubros` (`nombre`) VALUES ('801090: Servicios de seguridad e investigación n.c.p.');
INSERT INTO `iprove`.`rubros` (`nombre`) VALUES ('812090: Servicios de limpieza n.c.p.');
INSERT INTO `iprove`.`rubros` (`nombre`) VALUES ('829900: Servicios empresariales n.c.p.');
INSERT INTO `iprove`.`rubros` (`nombre`) VALUES ('854990: Servicios de enseñanza n.c.p.');
INSERT INTO `iprove`.`rubros` (`nombre`) VALUES ('869090: Servicios relacionados con la salud humana n.c.p.');
INSERT INTO `iprove`.`rubros` (`nombre`) VALUES ('870990: Servicios sociales con alojamiento n.c.p.');
INSERT INTO `iprove`.`rubros` (`nombre`) VALUES ('900091: Servicios de espectáculos arti­sticos n.c.p.');
INSERT INTO `iprove`.`rubros` (`nombre`) VALUES ('910900: Servicios culturales n.c.p.');
INSERT INTO `iprove`.`rubros` (`nombre`) VALUES ('920009: Servicios relacionados con juegos de azar y apuestas n.c.p.');
INSERT INTO `iprove`.`rubros` (`nombre`) VALUES ('939090: Servicios de entretenimiento n.c.p.');
INSERT INTO `iprove`.`rubros` (`nombre`) VALUES ('949990: Servicios de asociaciones n.c.p.');
INSERT INTO `iprove`.`rubros` (`nombre`) VALUES ('960990: Servicios personales n.c.p.');

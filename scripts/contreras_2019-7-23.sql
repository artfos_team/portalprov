-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Versión del servidor:         10.1.32-MariaDB - mariadb.org binary distribution
-- SO del servidor:              Win32
-- HeidiSQL Versión:             9.5.0.5196
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Volcando estructura de base de datos para iprove
DROP DATABASE IF EXISTS `iprove`;
CREATE DATABASE IF NOT EXISTS `iprove` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `iprove`;

-- Volcando estructura para tabla iprove.buy_order
CREATE TABLE IF NOT EXISTS `buy_order` (
  `idBuyOrder` int(11) NOT NULL AUTO_INCREMENT,
  `nic` int(11) NOT NULL DEFAULT '0',
  `monto` int(11) NOT NULL DEFAULT '0',
  `idCompany` int(11) NOT NULL DEFAULT '0',
  `fecha_emision` date DEFAULT NULL,
  `fecha_vencimiento` date DEFAULT NULL,
  `fecha_liberacion` date DEFAULT NULL,
  `porcentajeAnticipo` int(11) NOT NULL DEFAULT '0',
  `tieneAnticipo` tinyint(4) NOT NULL DEFAULT '0',
  `pdf` varchar(255) DEFAULT NULL,
  `statusId` int(11) NOT NULL,
  `view` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`idBuyOrder`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla iprove.buy_order: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `buy_order` DISABLE KEYS */;
/*!40000 ALTER TABLE `buy_order` ENABLE KEYS */;

-- Volcando estructura para tabla iprove.buy_order_document
CREATE TABLE IF NOT EXISTS `buy_order_document` (
  `idBuyOrderDocument` int(11) NOT NULL AUTO_INCREMENT,
  `idBuyOrder` int(11) DEFAULT NULL,
  `pdf` varchar(500) DEFAULT '0',
  `date` datetime DEFAULT CURRENT_TIMESTAMP,
  `documentType` int(11) DEFAULT '1',
  `visto` tinyint(4) DEFAULT '0',
  `status` tinyint(4) DEFAULT '0',
  `mensajeRechazo` varchar(400) DEFAULT NULL,
  PRIMARY KEY (`idBuyOrderDocument`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla iprove.buy_order_document: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `buy_order_document` DISABLE KEYS */;
/*!40000 ALTER TABLE `buy_order_document` ENABLE KEYS */;

-- Volcando estructura para tabla iprove.buy_order_document_type
CREATE TABLE IF NOT EXISTS `buy_order_document_type` (
  `idBuyOrderDocumentType` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`idBuyOrderDocumentType`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla iprove.buy_order_document_type: ~3 rows (aproximadamente)
/*!40000 ALTER TABLE `buy_order_document_type` DISABLE KEYS */;
INSERT IGNORE INTO `buy_order_document_type` (`idBuyOrderDocumentType`, `description`) VALUES
	(1, 'Factura'),
	(2, 'Nota de crédito'),
	(3, 'Nota de débito');
/*!40000 ALTER TABLE `buy_order_document_type` ENABLE KEYS */;

-- Volcando estructura para tabla iprove.buy_order_history
CREATE TABLE IF NOT EXISTS `buy_order_history` (
  `idBuyOrderHistory` int(11) NOT NULL AUTO_INCREMENT,
  `idBuyOrder` int(11) DEFAULT NULL,
  `detalle` varchar(255) DEFAULT NULL,
  `date` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`idBuyOrderHistory`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla iprove.buy_order_history: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `buy_order_history` DISABLE KEYS */;
/*!40000 ALTER TABLE `buy_order_history` ENABLE KEYS */;

-- Volcando estructura para tabla iprove.buy_order_status
CREATE TABLE IF NOT EXISTS `buy_order_status` (
  `statusId` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(100) DEFAULT '',
  `orden` int(11) DEFAULT NULL,
  PRIMARY KEY (`statusId`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla iprove.buy_order_status: ~8 rows (aproximadamente)
/*!40000 ALTER TABLE `buy_order_status` DISABLE KEYS */;
INSERT IGNORE INTO `buy_order_status` (`statusId`, `description`, `orden`) VALUES
	(1, 'Orden Compra (Liberada)', 1),
	(2, 'Orden de Compra (OC) ', 3),
	(3, 'OC a Facturar', 4),
	(4, 'Enviados', 5),
	(5, 'Aceptados', 6),
	(6, 'Gestión de pago', 8),
	(7, 'Orden de Compra (Rechazada)', 2),
	(8, 'Rechazados', 7);
/*!40000 ALTER TABLE `buy_order_status` ENABLE KEYS */;

-- Volcando estructura para tabla iprove.comments
CREATE TABLE IF NOT EXISTS `comments` (
  `idComments` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(500) DEFAULT NULL,
  `idCommentType` int(11) DEFAULT NULL,
  `idUser` int(11) NOT NULL DEFAULT '0',
  `dateComments` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `statusComments` int(11) NOT NULL DEFAULT '0',
  `idRefenceTo` int(11) DEFAULT '0',
  PRIMARY KEY (`idComments`),
  KEY `FK_invoicecomments_invoice_comments_status` (`statusComments`),
  KEY `FK_comments_comment_type` (`idCommentType`),
  KEY `FK_comments_user` (`idUser`),
  CONSTRAINT `FK_comments_comment_type` FOREIGN KEY (`idCommentType`) REFERENCES `comment_type` (`idCommentType`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_comments_user` FOREIGN KEY (`idUser`) REFERENCES `user` (`idUser`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_invoicecomments_invoice_comments_status` FOREIGN KEY (`statusComments`) REFERENCES `comments_status` (`idCommentsStatus`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Comentarios para las facturas';

-- Volcando datos para la tabla iprove.comments: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `comments` DISABLE KEYS */;
/*!40000 ALTER TABLE `comments` ENABLE KEYS */;

-- Volcando estructura para tabla iprove.company
CREATE TABLE IF NOT EXISTS `company` (
  `idCompany` int(11) NOT NULL AUTO_INCREMENT,
  `company` varchar(255) DEFAULT NULL,
  `companyName` varchar(255) DEFAULT NULL,
  `logo` text,
  `idCompanyCategory` int(11) DEFAULT NULL,
  `cuit` varchar(30) DEFAULT NULL,
  `requiresPurchaseOrder` tinyint(1) NOT NULL DEFAULT '0',
  `comments` text,
  `typeCompany` int(11) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `billingAddress` varchar(255) DEFAULT NULL,
  `calification` int(11) DEFAULT '0',
  `reasonCalification` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`idCompany`),
  KEY `fk_company_1` (`idCompanyCategory`) USING BTREE,
  KEY `idCompany` (`idCompany`),
  CONSTRAINT `company_ibfk_1` FOREIGN KEY (`idCompanyCategory`) REFERENCES `company_category` (`idCompanyCategory`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla iprove.company: ~2 rows (aproximadamente)
/*!40000 ALTER TABLE `company` DISABLE KEYS */;
INSERT IGNORE INTO `company` (`idCompany`, `company`, `companyName`, `logo`, `idCompanyCategory`, `cuit`, `requiresPurchaseOrder`, `comments`, `typeCompany`, `address`, `billingAddress`, `calification`, `reasonCalification`) VALUES
	(1, 'Compañía 1', 'Compañía 1', NULL, NULL, '123456789\r\n', 0, '', NULL, 'Direccion 1235', 'Direccion 456', 3, 'Buen tiempo de entrega'),
	(2, 'Compañía 2', 'Compañía 2', NULL, NULL, '987654321\r\n', 0, NULL, NULL, 'Direccion 321', 'Direccion 654', 4, 'Tarda mucho en contestar');
/*!40000 ALTER TABLE `company` ENABLE KEYS */;

-- Volcando estructura para tabla iprove.configuration
CREATE TABLE IF NOT EXISTS `configuration` (
  `idConfig` int(11) NOT NULL AUTO_INCREMENT,
  `attribute` varchar(20) DEFAULT NULL,
  `description` varchar(100) DEFAULT NULL,
  `value` varchar(3000) NOT NULL DEFAULT '0',
  PRIMARY KEY (`idConfig`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla iprove.configuration: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `configuration` DISABLE KEYS */;
INSERT IGNORE INTO `configuration` (`idConfig`, `attribute`, `description`, `value`) VALUES
	(1, 'URL-UPL', 'Url hacia la carpeta web/uploads/', 'http://localhost/contreras/web/');
/*!40000 ALTER TABLE `configuration` ENABLE KEYS */;

-- Volcando estructura para tabla iprove.document
CREATE TABLE IF NOT EXISTS `document` (
  `idDocument` int(11) NOT NULL AUTO_INCREMENT,
  `documentName` varchar(255) DEFAULT NULL,
  `required` int(11) DEFAULT NULL,
  `active` int(11) DEFAULT NULL,
  PRIMARY KEY (`idDocument`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla iprove.document: ~2 rows (aproximadamente)
/*!40000 ALTER TABLE `document` DISABLE KEYS */;
INSERT IGNORE INTO `document` (`idDocument`, `documentName`, `required`, `active`) VALUES
	(1, 'Balance', 0, 1),
	(2, 'Declaracion Jurada', 1, 1);
/*!40000 ALTER TABLE `document` ENABLE KEYS */;

-- Volcando estructura para tabla iprove.documentation
CREATE TABLE IF NOT EXISTS `documentation` (
  `idDocumentation` int(11) NOT NULL AUTO_INCREMENT,
  `url` varchar(255) DEFAULT '0',
  `idDocumentationType` int(11) DEFAULT '0',
  `idProvider` int(11) DEFAULT '0',
  PRIMARY KEY (`idDocumentation`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla iprove.documentation: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `documentation` DISABLE KEYS */;
/*!40000 ALTER TABLE `documentation` ENABLE KEYS */;

-- Volcando estructura para tabla iprove.email_parameters
CREATE TABLE IF NOT EXISTS `email_parameters` (
  `idEmailParameters` int(11) NOT NULL AUTO_INCREMENT,
  `smtp` varchar(100) NOT NULL,
  `user` varchar(70) NOT NULL,
  `userPsw` varchar(50) NOT NULL,
  `fromName` varchar(50) NOT NULL,
  `hostForMails` varchar(200) NOT NULL,
  PRIMARY KEY (`idEmailParameters`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla iprove.email_parameters: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `email_parameters` DISABLE KEYS */;
INSERT IGNORE INTO `email_parameters` (`idEmailParameters`, `smtp`, `user`, `userPsw`, `fromName`, `hostForMails`) VALUES
	(1, '127.0.0.1', 'test@portal.com.ar', 'portal', 'Info', 'http://localhost:8081');
/*!40000 ALTER TABLE `email_parameters` ENABLE KEYS */;

-- Volcando estructura para tabla iprove.images
CREATE TABLE IF NOT EXISTS `images` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(255) DEFAULT NULL,
  `archivo` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla iprove.images: ~5 rows (aproximadamente)
/*!40000 ALTER TABLE `images` DISABLE KEYS */;
INSERT IGNORE INTO `images` (`id`, `descripcion`, `archivo`) VALUES
	(1, 'logo', 'logo-small-2.png'),
	(2, 'Index Backend', 'fondo.jpg'),
	(3, 'Fondo login backend', 'fondo.jpg'),
	(4, 'Logo login backend', 'Logo.jpg'),
	(5, 'Fondo login frontend', 'KomatsuFront.jpg');
/*!40000 ALTER TABLE `images` ENABLE KEYS */;

-- Volcando estructura para tabla iprove.import
CREATE TABLE IF NOT EXISTS `import` (
  `idImport` int(11) NOT NULL AUTO_INCREMENT,
  `dateImport` date NOT NULL,
  `totalImport` int(11) DEFAULT '0',
  `updateImport` int(11) DEFAULT '0',
  `createImport` int(11) DEFAULT '0',
  `errorImport` int(11) DEFAULT '0',
  PRIMARY KEY (`idImport`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla iprove.import: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `import` DISABLE KEYS */;
/*!40000 ALTER TABLE `import` ENABLE KEYS */;

-- Volcando estructura para tabla iprove.import_error
CREATE TABLE IF NOT EXISTS `import_error` (
  `idImportError` int(11) NOT NULL AUTO_INCREMENT,
  `idImport` int(11) NOT NULL,
  `description` varchar(300) DEFAULT NULL,
  `status` int(11) DEFAULT '0',
  PRIMARY KEY (`idImportError`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla iprove.import_error: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `import_error` DISABLE KEYS */;
/*!40000 ALTER TABLE `import_error` ENABLE KEYS */;

-- Volcando estructura para tabla iprove.import_file_column
CREATE TABLE IF NOT EXISTS `import_file_column` (
  `idImportFileColumn` int(11) NOT NULL AUTO_INCREMENT,
  `idImportFileFormat` bigint(20) NOT NULL,
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`idImportFileColumn`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla iprove.import_file_column: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `import_file_column` DISABLE KEYS */;
/*!40000 ALTER TABLE `import_file_column` ENABLE KEYS */;

-- Volcando estructura para tabla iprove.import_file_format
CREATE TABLE IF NOT EXISTS `import_file_format` (
  `idImportFileFormat` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `url` varchar(255) NOT NULL,
  `type` varchar(2) NOT NULL,
  PRIMARY KEY (`idImportFileFormat`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla iprove.import_file_format: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `import_file_format` DISABLE KEYS */;
/*!40000 ALTER TABLE `import_file_format` ENABLE KEYS */;

-- Volcando estructura para tabla iprove.import_schedule
CREATE TABLE IF NOT EXISTS `import_schedule` (
  `idImportSchedule` int(11) NOT NULL AUTO_INCREMENT,
  `idImportFileFormat` bigint(20) NOT NULL,
  `time` varchar(5) NOT NULL,
  PRIMARY KEY (`idImportSchedule`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla iprove.import_schedule: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `import_schedule` DISABLE KEYS */;
/*!40000 ALTER TABLE `import_schedule` ENABLE KEYS */;

-- Volcando estructura para tabla iprove.invoice_detail
CREATE TABLE IF NOT EXISTS `invoice_detail` (
  `idInvoiceDetail` int(11) NOT NULL AUTO_INCREMENT,
  `idInvoice` int(11) NOT NULL,
  `NroLinDet` int(11) DEFAULT NULL,
  `NmbItem` varchar(255) DEFAULT NULL,
  `QtyItem` varchar(255) DEFAULT NULL,
  `UnmdItem` varchar(255) DEFAULT NULL,
  `PrcItem` varchar(255) DEFAULT NULL,
  `MontoItem` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`idInvoiceDetail`),
  KEY `fk_invoice_detail` (`idInvoice`),
  CONSTRAINT `fk_invoice_detail` FOREIGN KEY (`idInvoice`) REFERENCES `invoice` (`idInvoice`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla iprove.invoice_detail: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `invoice_detail` DISABLE KEYS */;
/*!40000 ALTER TABLE `invoice_detail` ENABLE KEYS */;

-- Volcando estructura para tabla iprove.licitaciones
CREATE TABLE IF NOT EXISTS `licitaciones` (
  `idLicitacion` int(8) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(25) DEFAULT NULL,
  `cuerpo` varchar(1000) DEFAULT NULL,
  `fecha_creacion` datetime DEFAULT CURRENT_TIMESTAMP,
  `fecha_inicio` datetime DEFAULT NULL,
  `fecha_fin` datetime DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`idLicitacion`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla iprove.licitaciones: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `licitaciones` DISABLE KEYS */;
/*!40000 ALTER TABLE `licitaciones` ENABLE KEYS */;

-- Volcando estructura para tabla iprove.licitaciones_consultas
CREATE TABLE IF NOT EXISTS `licitaciones_consultas` (
  `idMessage` int(11) NOT NULL AUTO_INCREMENT,
  `idEmisor` int(11) DEFAULT NULL,
  `idReceptor` int(11) DEFAULT NULL,
  `message` varchar(255) DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  `messageType` int(11) DEFAULT NULL,
  `replyTo` int(11) DEFAULT NULL,
  `idLicitacion` int(11) DEFAULT NULL,
  PRIMARY KEY (`idMessage`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla iprove.licitaciones_consultas: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `licitaciones_consultas` DISABLE KEYS */;
/*!40000 ALTER TABLE `licitaciones_consultas` ENABLE KEYS */;

-- Volcando estructura para tabla iprove.licitaciones_ofertas
CREATE TABLE IF NOT EXISTS `licitaciones_ofertas` (
  `idOferta` int(11) NOT NULL AUTO_INCREMENT,
  `idLicitacion` int(11) DEFAULT NULL,
  `idProvider` int(11) DEFAULT NULL,
  `oferta_tec` varchar(250) DEFAULT NULL,
  `oferta_eco` varchar(250) DEFAULT NULL,
  `fecha_carga` datetime DEFAULT CURRENT_TIMESTAMP,
  `fecha_visto` datetime DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  PRIMARY KEY (`idOferta`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla iprove.licitaciones_ofertas: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `licitaciones_ofertas` DISABLE KEYS */;
/*!40000 ALTER TABLE `licitaciones_ofertas` ENABLE KEYS */;

-- Volcando estructura para tabla iprove.licitaciones_status
CREATE TABLE IF NOT EXISTS `licitaciones_status` (
  `idLicitacionStatus` int(11) NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`idLicitacionStatus`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla iprove.licitaciones_status: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `licitaciones_status` DISABLE KEYS */;
/*!40000 ALTER TABLE `licitaciones_status` ENABLE KEYS */;

-- Volcando estructura para tabla iprove.licitaciones_x_proveedores
CREATE TABLE IF NOT EXISTS `licitaciones_x_proveedores` (
  `idLicitacionProvider` int(11) NOT NULL AUTO_INCREMENT,
  `idLicitacion` int(11) DEFAULT NULL,
  `idProvider` int(11) DEFAULT NULL,
  `fecha_visto` datetime DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  PRIMARY KEY (`idLicitacionProvider`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla iprove.licitaciones_x_proveedores: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `licitaciones_x_proveedores` DISABLE KEYS */;
/*!40000 ALTER TABLE `licitaciones_x_proveedores` ENABLE KEYS */;

-- Volcando estructura para tabla iprove.logs
CREATE TABLE IF NOT EXISTS `logs` (
  `idLog` int(11) NOT NULL AUTO_INCREMENT,
  `idUser` int(11) DEFAULT NULL,
  `levelLog` int(11) NOT NULL DEFAULT '0',
  `dateLog` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `commentaryLog` varchar(250) NOT NULL,
  `source` varchar(50) NOT NULL,
  PRIMARY KEY (`idLog`),
  KEY `FK_logs_user` (`idUser`),
  CONSTRAINT `FK_logs_user` FOREIGN KEY (`idUser`) REFERENCES `user` (`idUser`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Tabla para almacenar logs del sistema';

-- Volcando datos para la tabla iprove.logs: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `logs` DISABLE KEYS */;
/*!40000 ALTER TABLE `logs` ENABLE KEYS */;

-- Volcando estructura para tabla iprove.manual
CREATE TABLE IF NOT EXISTS `manual` (
  `idManual` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `description` varchar(100) DEFAULT NULL,
  `path` varchar(200) DEFAULT NULL,
  `status` int(11) DEFAULT '1',
  PRIMARY KEY (`idManual`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla iprove.manual: ~4 rows (aproximadamente)
/*!40000 ALTER TABLE `manual` DISABLE KEYS */;
INSERT IGNORE INTO `manual` (`idManual`, `name`, `description`, `path`, `status`) VALUES
	(1, 'Test', 'Hola', 'upload/invoices/1.pdf', 1),
	(2, 'Test', 'Hola', 'upload/invoices/1.pdf', 1),
	(4, 'Manual sobre los procesos administrativps para la', 'Hola', 'upload/invoices/1.pdf', 1);
/*!40000 ALTER TABLE `manual` ENABLE KEYS */;

-- Volcando estructura para tabla iprove.messages
CREATE TABLE IF NOT EXISTS `messages` (
  `idMessage` int(11) NOT NULL AUTO_INCREMENT,
  `idEmisor` int(11) DEFAULT NULL,
  `idReceptor` int(11) DEFAULT NULL,
  `message` varchar(255) DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  `messageType` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  PRIMARY KEY (`idMessage`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla iprove.messages: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `messages` DISABLE KEYS */;
/*!40000 ALTER TABLE `messages` ENABLE KEYS */;

-- Volcando estructura para tabla iprove.message_type
CREATE TABLE IF NOT EXISTS `message_type` (
  `idMessagetype` int(11) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla iprove.message_type: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `message_type` DISABLE KEYS */;
/*!40000 ALTER TABLE `message_type` ENABLE KEYS */;

-- Volcando estructura para tabla iprove.migration
CREATE TABLE IF NOT EXISTS `migration` (
  `version` varchar(255) NOT NULL,
  `apply_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`version`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla iprove.migration: 0 rows
/*!40000 ALTER TABLE `migration` DISABLE KEYS */;
/*!40000 ALTER TABLE `migration` ENABLE KEYS */;

-- Volcando estructura para tabla iprove.parameters
CREATE TABLE IF NOT EXISTS `parameters` (
  `idParameter` varchar(50) NOT NULL,
  `value` tinyint(4) NOT NULL DEFAULT '0',
  `stringValue` varchar(100) NOT NULL,
  `statusParameter` bit(1) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `detailParameter` varchar(200) NOT NULL,
  PRIMARY KEY (`idParameter`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Parametros del sistema. Activar o Inactivar sectores del sistema';

-- Volcando datos para la tabla iprove.parameters: ~5 rows (aproximadamente)
/*!40000 ALTER TABLE `parameters` DISABLE KEYS */;
INSERT IGNORE INTO `parameters` (`idParameter`, `value`, `stringValue`, `statusParameter`, `date`, `detailParameter`) VALUES
	('CIF-001', 1, 'No se tiene acceso', b'0', '2017-12-12 08:50:23', 'Mostrar u ocultar el botón "Consulta Individual de factura"'),
	('FE-001', 1, 'La página está cerrada, inténtelo mas tarde', b'1', '2017-12-12 08:50:23', 'Habilitar o deshabilitar funciones de todo el frontend"'),
	('IFM-001', 1, 'No puede importar facturas desde un excel.', b'1', '2018-09-05 13:49:07', 'Mostrar u ocultar el boton de Importar facturas'),
	('SUF-001', 0, 'No se requieren las facturas anteriores.', b'1', '2018-09-05 13:49:07', 'Requerir facturas anteriores en la solicitud de usuario'),
	('SUN-001', 1, 'No se puede solicitar un nuevo usuario', b'1', '2018-01-15 17:21:43', 'Mostrar u ocultar el botón "Solicitud Usuario Nuevo"');
/*!40000 ALTER TABLE `parameters` ENABLE KEYS */;

-- Volcando estructura para tabla iprove.permissions
CREATE TABLE IF NOT EXISTS `permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `controller` varchar(255) NOT NULL,
  `action` varchar(255) NOT NULL,
  `bizrule` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=216 DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla iprove.permissions: ~83 rows (aproximadamente)
/*!40000 ALTER TABLE `permissions` DISABLE KEYS */;
INSERT IGNORE INTO `permissions` (`id`, `name`, `description`, `controller`, `action`, `bizrule`) VALUES
	(1, 'Administración de actividades', 'Administración de actividades', 'activity', '*', NULL),
	(2, 'Administración de carreras', 'Administración de carreras', 'carrerr', '*', NULL),
	(3, 'Administración de ciudades', 'Administración de ciudades', 'city', '*', NULL),
	(4, 'Administración de empresas', 'Administración de empresas', 'company', '*', NULL),
	(5, 'Administración de idiomas', 'Administración de idiomas', 'language', '*', NULL),
	(6, 'Administración de institutos', 'Administración de institutos', 'institute', '*', NULL),
	(7, 'Administración de participantes', 'Administración de participantes', 'participant', '*', NULL),
	(8, 'Administración de programas', 'Administración de programas', 'program', '*', NULL),
	(9, 'Administración de puestos', 'Administración de puestos', 'position', '*', NULL),
	(10, 'Administración de informes', 'Administración de informes', 'workshop', '*', NULL),
	(11, 'Administración de países', 'Administración de países', 'country', '*', NULL),
	(12, 'Administración de provincias', 'Administración de provincias', 'province', '*', NULL),
	(13, 'Administración de rubros', 'Administración de rubros', 'companyCategory', '*', NULL),
	(14, 'Administración de tipos de programa', 'Administración de tipos de programa', 'programType', '*', NULL),
	(15, 'Administración de tipos de empresas - direcciones', 'Administración de tipos de empresas - direcciones', 'companyAddress', '*', NULL),
	(16, 'Administración de tipos de empresas - contacto', 'Administración de tipos de empresas - contacto', 'companyContact', '*', NULL),
	(17, 'Acceso cliente', 'Permite ver el estado de avance de los participantes del cliente', 'client', '*', NULL),
	(20, 'Admin consultores', 'Administra consultores', 'consultant', '*', NULL),
	(21, 'Acceso consultores', 'Acceso consultores', 'consultantAccess', '*', NULL),
	(22, 'Admin reportes', 'Permite ver los reportes de participantes', 'participantProgram', '*', NULL),
	(23, 'Admin usuarios', 'Administra consultores', 'user', '*', NULL),
	(24, 'Administra programas en curso', 'Administra programas en curso', 'programManagement', '*', NULL),
	(25, 'Admin recepción', 'Administra acciones de recepción', 'reception', '*', NULL),
	(30, 'Admin unidades de negocio', 'Administra unidades de negocio', 'businessUnit', '*', NULL),
	(100, 'Admin programas en curso - index', '', 'programManagement', 'index', NULL),
	(101, 'Admin programas en curso - checklist', '', 'programManagement', 'checklist', NULL),
	(102, 'Admin programas en curso - actividades detalle', '', 'programManagement', 'viewActivityDetail', NULL),
	(103, 'Admin programas en curso - reportes detalle', '', 'programManagement', 'viewReportDetail', NULL),
	(104, 'Admin programas en curso - agregar participantes', '', 'programManagement', 'addParticipantActivity', NULL),
	(105, 'Admin empresas - index empresas', '', 'company', 'index', NULL),
	(106, 'Admin empresas - ver empresa', '', 'company', 'view', NULL),
	(107, 'Facturas', '', 'invoice', 'index', NULL),
	(108, 'Admin participantes - ver participante', '', 'participant', 'view', NULL),
	(109, 'Admin actividades - index', '', 'activity', 'index', NULL),
	(110, 'Admin ocurrencias - ver ocurrencia', '', 'reception', 'occurrenceView', NULL),
	(111, 'Admin acciones participantes (reportes) - Ver programas sin oferta inicial', '', 'participantProgram', 'showProgramsWithoutOffer', NULL),
	(112, 'Admin acciones participantes (reportes) - Enviar programas sin oferta inicial', '', 'participantProgram', 'sendOffer', NULL),
	(113, 'Admin acciones participantes (reportes) - Ver programas sin reporte inicial', '', 'participantProgram', 'showProgramsWithoutInitialReport', NULL),
	(114, 'Admin acciones participantes (reportes) - Enviar programas sin reporte inicial', '', 'participantProgram', 'showProgramsWithoutOffer', NULL),
	(115, 'Admin acciones participantes (reportes) - Edición atributos programa', '', 'participantProgram', 'changeAttributeParticipantProgram', NULL),
	(116, 'Admin acciones participantes (reportes) - Ver archivo de reporte', '', 'participantProgram', 'getReportFile', NULL),
	(117, 'Admin acciones participantes (reportes) - Ver reporte por estado', '', 'participantProgram', 'reportByStatus', NULL),
	(118, 'Admin acciones participantes (reportes) - Ver reportes pendientes', '', 'participantProgram', 'pendingReports', NULL),
	(119, 'Admin programas en curso - agregar actividades personalizadas', '', 'programManagement', 'addCustomActivities', NULL),
	(120, 'Admin programas en curso - agregar reportes personalizados', '', 'programManagement', 'addCustomReport', NULL),
	(121, 'Admin programas en curso - cambiar status', '', 'programManagement', 'changeStatus', NULL),
	(122, 'Admin programas en curso - editar atributos', '', 'programManagement', 'editableParticipantProgram', NULL),
	(123, 'Admin programas en curso - subir archivos para actividades', '', 'programManagement', 'updateFilesParticipantActivity', NULL),
	(124, 'Admin programas en curso - subir archivos para informes', '', 'programManagement', 'updateFilesParticipantReport', NULL),
	(125, 'Admin programas en curso - obtener archivos para reportes', '', 'programManagement', 'getReportFile', NULL),
	(126, 'Admin programas en curso - enviar reportes', '', 'programManagement', 'sendReport', NULL),
	(127, 'Admin ocurrencias - ver ocurrencia (detail)', '', 'reception', 'occurrenceDetails', NULL),
	(128, 'Admin programas en curso - mostrar bitacora', '', 'programManagement', 'showInformationPiece', NULL),
	(129, 'Admin programas en curso - mostrar información de proceso', '', 'programManagement', 'showProcessInfo', NULL),
	(130, 'Admin programas en curso - mostrar reportes/actividades sin finalizar', '', 'programManagement', 'showNonCompletedItems', NULL),
	(135, 'Admin razones de desvinculación', '', 'exitReason', '*', NULL),
	(145, 'Admin modos reinserción', '', 'reinsertionMode', '*', NULL),
	(150, 'Admin programas en curso - enviar informes iniciales', '', 'programManagement', 'sendInitialReport', NULL),
	(160, 'Visualización contactos empresa', '', 'company', 'contactView', NULL),
	(161, 'Visualización direcciones empresa', '', 'company', 'addressView', NULL),
	(162, 'Agregar idiomas participante', '', 'participant', 'viewLanguages', NULL),
	(165, 'Visualización y carga CV participantes', '', 'participant', 'curriculum', NULL),
	(166, 'Visualización y carga CV participantes', '', 'participant', 'deleteCurriculum', NULL),
	(170, 'Permiso para cambiar la fecha de fin de programa de un participante', '', 'programManagement', 'changeEndDate', NULL),
	(171, 'Permiso para editar datos personales de los participantes', '', 'participant', 'update', NULL),
	(175, 'Permiso para ver los informes pendientes descartados', '', 'participantProgram', 'pendingReportsDiscarded', NULL),
	(176, 'Permiso para ver archivos en actividades', '', 'programManagement', 'updateFilesParticipantActivity', NULL),
	(200, 'Visualización de Reportes', '', 'report', '*', NULL),
	(201, 'Ordenes', '', 'order', '*', NULL),
	(202, 'orderitems', 'order item', 'OrderItem', '*', NULL),
	(203, 'Pagos / Facturas', 'Pagos / Facturas', 'invoice', '*', ''),
	(204, 'Administrado de Metodo de Pago', 'Administrador de Metodos de Pago', 'paymentMethod', '*', NULL),
	(205, 'Administración de Estados', 'Administración de Estados', 'orderCategory', '*', NULL),
	(206, 'Administración de estados de facturas', 'Administra estados de las facturas', 'InvoiceStatus', '*', NULL),
	(207, 'Administrador de SAP', 'Administracion de los parametros de la conexion a SAP', 'SapParameters', '*', NULL),
	(208, 'Administrador de emails', 'Administracion de los parametros de la conexion de emails', 'EmailParameters', '*', NULL),
	(209, 'Administrador de importaciones', 'Ver los logs de las importaciones', 'Imports', '*', NULL),
	(210, 'Administracion de imagenes', 'Administracion de imagenes del sitio', 'Images', '*', NULL),
	(211, 'Modificacion de calificaciones', 'Modificar calificaciones a los proveedores', 'company', 'viewCalification', NULL),
	(212, 'Administracion de Documentos', 'Administrar los documentos de los proveedores', 'document', '*', NULL),
	(213, 'Administrador de schedules', 'Administrador de schedules', 'schedule', '*', NULL),
	(214, 'Administrador de configuración', 'Configuracion', 'configuration', '*', NULL),
	(215, 'Administrador de manuales', 'Manuales', 'manual', '*', NULL);
/*!40000 ALTER TABLE `permissions` ENABLE KEYS */;

-- Volcando estructura para tabla iprove.provider
CREATE TABLE IF NOT EXISTS `provider` (
  `idProvider` int(11) NOT NULL AUTO_INCREMENT,
  `idCompany` int(11) NOT NULL DEFAULT '0',
  `idUser` int(11) unsigned NOT NULL DEFAULT '0',
  `idComments` int(11) NOT NULL DEFAULT '0',
  `idProviderStatus` int(11) NOT NULL DEFAULT '0',
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `providerDeleted` int(11) NOT NULL DEFAULT '0',
  `calification` int(11) DEFAULT '0',
  `reasonCalification` varchar(50) DEFAULT NULL,
  `aceptoTerminos` tinyint(4) DEFAULT '0',
  PRIMARY KEY (`idProvider`),
  KEY `FK_provider_company` (`idCompany`),
  KEY `FK_provider_user` (`idUser`),
  KEY `FK_provider_comments` (`idComments`),
  KEY `FK_provider_provider_status` (`idProviderStatus`),
  CONSTRAINT `FK_provider_company` FOREIGN KEY (`idCompany`) REFERENCES `company` (`idCompany`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_provider_provider_status` FOREIGN KEY (`idProviderStatus`) REFERENCES `provider_status` (`idProviderStatus`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=latin1 COMMENT='Tabla de proveedores';

-- Volcando datos para la tabla iprove.provider: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `provider` DISABLE KEYS */;
INSERT IGNORE INTO `provider` (`idProvider`, `idCompany`, `idUser`, `idComments`, `idProviderStatus`, `date`, `providerDeleted`, `calification`, `reasonCalification`, `aceptoTerminos`) VALUES
	(24, 1, 41, 0, 2, '2019-05-08 16:10:41', 0, 0, '0', 0);
/*!40000 ALTER TABLE `provider` ENABLE KEYS */;

-- Volcando estructura para tabla iprove.provider_document
CREATE TABLE IF NOT EXISTS `provider_document` (
  `idProviderDocument` int(11) NOT NULL AUTO_INCREMENT,
  `idProvider` int(11) DEFAULT NULL,
  `idDocument` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT '1',
  `date` date DEFAULT NULL,
  PRIMARY KEY (`idProviderDocument`)
) ENGINE=InnoDB AUTO_INCREMENT=98 DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla iprove.provider_document: ~5 rows (aproximadamente)
/*!40000 ALTER TABLE `provider_document` DISABLE KEYS */;
INSERT IGNORE INTO `provider_document` (`idProviderDocument`, `idProvider`, `idDocument`, `status`, `date`) VALUES
	(93, 1, 1, 1, '2019-07-10'),
	(94, 1, 2, 0, '2019-07-10'),
	(96, 2, 1, 0, '2019-02-26'),
	(97, 2, 2, 0, '2019-02-26');
/*!40000 ALTER TABLE `provider_document` ENABLE KEYS */;

-- Volcando estructura para tabla iprove.provider_status
CREATE TABLE IF NOT EXISTS `provider_status` (
  `idProviderStatus` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(50) NOT NULL DEFAULT '0',
  PRIMARY KEY (`idProviderStatus`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1 COMMENT='Tabla de estado de proveedores';

-- Volcando datos para la tabla iprove.provider_status: ~6 rows (aproximadamente)
/*!40000 ALTER TABLE `provider_status` DISABLE KEYS */;
INSERT IGNORE INTO `provider_status` (`idProviderStatus`, `description`) VALUES
	(1, 'Solicitado'),
	(2, 'Activo'),
	(3, 'Inactivo'),
	(4, 'Sin acción'),
	(5, 'Suspendido'),
	(6, 'Rechazado');
/*!40000 ALTER TABLE `provider_status` ENABLE KEYS */;

-- Volcando estructura para tabla iprove.roles
CREATE TABLE IF NOT EXISTS `roles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `visible` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla iprove.roles: ~6 rows (aproximadamente)
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT IGNORE INTO `roles` (`id`, `name`, `description`, `visible`) VALUES
	(15, 'Administrador Portal', 'Administrador Portal', 1),
	(18, 'Administrador General', 'Administrador General', 1),
	(19, 'Proveedor', 'Proveedor con Cuit', 1),
	(20, 'Compras', 'Publica compulsas y califica proveedores', 1),
	(21, 'Cuentas a pagar', 'Toma facturas e informa el estado', 1),
	(22, 'Tesoreria', 'establece fecha de pago de cada factura y si será por cheque o banco', 1);
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;

-- Volcando estructura para tabla iprove.roles_has_permissions
CREATE TABLE IF NOT EXISTS `roles_has_permissions` (
  `rolesId` int(11) NOT NULL,
  `permissionsId` int(11) NOT NULL,
  PRIMARY KEY (`rolesId`,`permissionsId`),
  KEY `fk_roles_has_permissions_2` (`permissionsId`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla iprove.roles_has_permissions: ~181 rows (aproximadamente)
/*!40000 ALTER TABLE `roles_has_permissions` DISABLE KEYS */;
INSERT IGNORE INTO `roles_has_permissions` (`rolesId`, `permissionsId`) VALUES
	(15, 21),
	(15, 100),
	(15, 101),
	(15, 102),
	(15, 103),
	(15, 104),
	(15, 107),
	(15, 108),
	(15, 121),
	(15, 124),
	(15, 125),
	(15, 126),
	(15, 128),
	(15, 129),
	(15, 130),
	(15, 165),
	(15, 166),
	(15, 170),
	(15, 171),
	(15, 176),
	(15, 210),
	(16, 7),
	(16, 100),
	(16, 101),
	(16, 102),
	(16, 103),
	(16, 111),
	(16, 112),
	(16, 113),
	(16, 114),
	(16, 115),
	(16, 116),
	(16, 117),
	(16, 128),
	(16, 129),
	(16, 130),
	(16, 150),
	(16, 210),
	(17, 7),
	(17, 100),
	(17, 101),
	(17, 102),
	(17, 103),
	(17, 104),
	(17, 118),
	(17, 119),
	(17, 120),
	(17, 121),
	(17, 122),
	(17, 123),
	(17, 124),
	(17, 125),
	(17, 126),
	(17, 128),
	(17, 129),
	(17, 130),
	(17, 210),
	(18, 1),
	(18, 2),
	(18, 3),
	(18, 4),
	(18, 5),
	(18, 6),
	(18, 7),
	(18, 8),
	(18, 9),
	(18, 10),
	(18, 11),
	(18, 12),
	(18, 13),
	(18, 14),
	(18, 15),
	(18, 16),
	(18, 20),
	(18, 21),
	(18, 22),
	(18, 23),
	(18, 24),
	(18, 25),
	(18, 30),
	(18, 135),
	(18, 145),
	(18, 175),
	(18, 200),
	(18, 201),
	(18, 202),
	(18, 203),
	(18, 204),
	(18, 205),
	(18, 206),
	(18, 207),
	(18, 208),
	(18, 209),
	(18, 210),
	(18, 211),
	(18, 212),
	(18, 213),
	(18, 214),
	(18, 215),
	(19, 1),
	(19, 2),
	(19, 3),
	(19, 4),
	(19, 5),
	(19, 6),
	(19, 7),
	(19, 8),
	(19, 9),
	(19, 10),
	(19, 11),
	(19, 12),
	(19, 13),
	(19, 14),
	(19, 15),
	(19, 16),
	(19, 17),
	(19, 210),
	(20, 1),
	(20, 2),
	(20, 3),
	(20, 4),
	(20, 5),
	(20, 6),
	(20, 7),
	(20, 8),
	(20, 9),
	(20, 10),
	(20, 11),
	(20, 12),
	(20, 13),
	(20, 14),
	(20, 15),
	(20, 16),
	(20, 210),
	(20, 211),
	(21, 22),
	(21, 210),
	(22, 25),
	(22, 160),
	(22, 161),
	(22, 162),
	(22, 165),
	(22, 166),
	(22, 210),
	(50, 105),
	(50, 106),
	(50, 107),
	(50, 108),
	(50, 109),
	(50, 110),
	(50, 127),
	(50, 128),
	(50, 129),
	(50, 130),
	(50, 160),
	(50, 161),
	(50, 162),
	(50, 165),
	(50, 166),
	(50, 210),
	(51, 100),
	(51, 101),
	(51, 107),
	(51, 108),
	(51, 210),
	(55, 135),
	(55, 210),
	(60, 145),
	(60, 210),
	(61, 201),
	(61, 202),
	(61, 203),
	(61, 210),
	(62, 201),
	(62, 202),
	(62, 203),
	(62, 210),
	(63, 201),
	(63, 202),
	(63, 203),
	(63, 210);
/*!40000 ALTER TABLE `roles_has_permissions` ENABLE KEYS */;

-- Volcando estructura para tabla iprove.rubros
CREATE TABLE IF NOT EXISTS `rubros` (
  `idRubro` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) NOT NULL,
  `status` varchar(50) NOT NULL DEFAULT '1',
  PRIMARY KEY (`idRubro`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla iprove.rubros: ~3 rows (aproximadamente)
/*!40000 ALTER TABLE `rubros` DISABLE KEYS */;
INSERT IGNORE INTO `rubros` (`idRubro`, `nombre`, `status`) VALUES
	(1, 'Electrónica', '1'),
	(2, 'Mecánica', '1'),
	(3, 'Textil', '1');
/*!40000 ALTER TABLE `rubros` ENABLE KEYS */;

-- Volcando estructura para tabla iprove.rubros_company
CREATE TABLE IF NOT EXISTS `rubros_company` (
  `idRubroCompany` int(11) NOT NULL AUTO_INCREMENT,
  `idRubro` int(11) NOT NULL DEFAULT '0',
  `idCompany` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`idRubroCompany`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla iprove.rubros_company: ~3 rows (aproximadamente)
/*!40000 ALTER TABLE `rubros_company` DISABLE KEYS */;
INSERT IGNORE INTO `rubros_company` (`idRubroCompany`, `idRubro`, `idCompany`) VALUES
	(1, 2, 1),
	(2, 3, 2),
	(3, 3, 1);
/*!40000 ALTER TABLE `rubros_company` ENABLE KEYS */;

-- Volcando estructura para tabla iprove.sap_parameters
CREATE TABLE IF NOT EXISTS `sap_parameters` (
  `idSapParameters` int(11) NOT NULL AUTO_INCREMENT,
  `ashost` varchar(20) NOT NULL,
  `sysnr` varchar(10) DEFAULT NULL,
  `client` varchar(10) DEFAULT NULL,
  `lang` varchar(10) DEFAULT NULL,
  `user` varchar(50) NOT NULL,
  `passwd` varchar(200) NOT NULL,
  `function` varchar(50) NOT NULL,
  PRIMARY KEY (`idSapParameters`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla iprove.sap_parameters: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `sap_parameters` DISABLE KEYS */;
INSERT IGNORE INTO `sap_parameters` (`idSapParameters`, `ashost`, `sysnr`, `client`, `lang`, `user`, `passwd`, `function`) VALUES
	(1, '0.0.0.0', '00', '100', 'es', 'user', 'psw', 'ZOPT_RFC_PORTAL_VIM');
/*!40000 ALTER TABLE `sap_parameters` ENABLE KEYS */;

-- Volcando estructura para tabla iprove.tbl_migration
CREATE TABLE IF NOT EXISTS `tbl_migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla iprove.tbl_migration: ~3 rows (aproximadamente)
/*!40000 ALTER TABLE `tbl_migration` DISABLE KEYS */;
INSERT IGNORE INTO `tbl_migration` (`version`, `apply_time`) VALUES
	('m000000_000000_base', 1555349488),
	('m190327_131752_import_file_format', 1555349497),
	('m190409_152748_import_schedule', 1555349498);
/*!40000 ALTER TABLE `tbl_migration` ENABLE KEYS */;

-- Volcando estructura para tabla iprove.user
CREATE TABLE IF NOT EXISTS `user` (
  `idUser` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `salt` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `passwordStrategy` varchar(255) DEFAULT NULL,
  `requiresNewPassword` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `lastLoginAt` datetime DEFAULT NULL,
  `lastActiveAt` datetime DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0',
  `signature` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `idCompany` int(11) DEFAULT NULL,
  `idConsultant` int(11) DEFAULT NULL,
  `picture` text,
  `sex` tinyint(1) NOT NULL DEFAULT '0',
  `token` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`idUser`),
  KEY `fk_user_company` (`idCompany`) USING BTREE,
  KEY `fk_user_consultant_1` (`idConsultant`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=42 DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla iprove.user: ~2 rows (aproximadamente)
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT IGNORE INTO `user` (`idUser`, `name`, `salt`, `password`, `passwordStrategy`, `requiresNewPassword`, `lastLoginAt`, `lastActiveAt`, `status`, `signature`, `email`, `idCompany`, `idConsultant`, `picture`, `sex`, `token`) VALUES
	(1, 'Usuario Compras 1', '$2a$12$yX/UyMwSmYh11t0qL/o/hA', '$2a$12$yX/UyMwSmYh11t0qL/o/h.me.e6HsfNkm67Hk0kUNbM2shF7R2YZ.', 'bcrypt', 0, '2019-07-23 16:17:25', '2019-07-23 16:38:05', 0, NULL, 'info@portal.com', NULL, NULL, NULL, 0, '9afb70931b3e99bb3d26557eccf2cf1671d0bdec'),
	(41, 'Proveedor 1', '$2a$12$W9tJe2VdsRL2ptokbxGHmg', '$2a$12$W9tJe2VdsRL2ptokbxGHmeE3nSn1OB/wQXnHUDKaY0jLO8VwI4dfW', NULL, 0, '2019-07-23 16:31:34', NULL, 0, NULL, 'proveedor@portal.com', 44, NULL, NULL, 0, '8f0b59155591903e5661f78be882e1783c131a5a');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;

-- Volcando estructura para tabla iprove.user_has_roles
CREATE TABLE IF NOT EXISTS `user_has_roles` (
  `idUser` int(11) NOT NULL,
  `rolesId` int(11) NOT NULL,
  PRIMARY KEY (`idUser`,`rolesId`),
  KEY `fk_user_has_permissions_1` (`rolesId`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla iprove.user_has_roles: ~14 rows (aproximadamente)
/*!40000 ALTER TABLE `user_has_roles` DISABLE KEYS */;
INSERT IGNORE INTO `user_has_roles` (`idUser`, `rolesId`) VALUES
	(1, 15),
	(1, 16),
	(1, 17),
	(1, 18),
	(1, 19),
	(1, 20),
	(1, 21),
	(1, 22),
	(1, 50),
	(1, 51),
	(1, 55),
	(1, 60),
	(2, 19),
	(41, 19);
/*!40000 ALTER TABLE `user_has_roles` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;

INSERT INTO `iprove`.`company_data_status` (`idCompanyDataStatus`, `description`) VALUES ('4', 'Modificado');

ALTER TABLE `company_payment_data`
	ADD COLUMN `modifDate` TIMESTAMP NULL DEFAULT current_timestamp() ON UPDATE current_timestamp() AFTER `idUser`;


CREATE TABLE `log_company_payment_data` (
	`idPaymentData` INT(11) NOT NULL AUTO_INCREMENT,
	`idBankBranch` INT(11) NULL DEFAULT NULL,
	`idBank` INT(11) NULL DEFAULT NULL,
	`accountNumber` VARCHAR(50) NULL DEFAULT NULL COLLATE 'latin1_swedish_ci',
	`idBankAccountType` INT(11) NULL DEFAULT NULL,
	`cbu` DECIMAL(22,0) NULL DEFAULT NULL,
	`pdfBank` VARCHAR(200) NULL DEFAULT NULL COLLATE 'latin1_swedish_ci',
	`idCompanyDataStatus` INT(11) NULL DEFAULT '1',
	`observation` VARCHAR(400) NULL DEFAULT NULL COLLATE 'latin1_swedish_ci',
	`idUser` INT(11) NULL DEFAULT NULL,
    `modifDate` TIMESTAMP NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
	PRIMARY KEY (`idPaymentData`) USING BTREE,
	INDEX `company_payment_data_FK` (`idBank`) USING BTREE,
	INDEX `company_payment_data_FK_1` (`idBankAccountType`) USING BTREE,
	INDEX `company_payment_data_FK_2` (`idBankBranch`) USING BTREE,
	INDEX `company_payment_data_FK_3` (`idCompanyDataStatus`) USING BTREE,
	INDEX `company_payment_data_FK_4` (`idUser`) USING BTREE,
	CONSTRAINT `log_company_payment_data_FK` FOREIGN KEY (`idBank`) REFERENCES `iprove`.`bank` (`idBank`) ON UPDATE CASCADE ON DELETE SET NULL,
	CONSTRAINT `log_company_payment_data_FK_1` FOREIGN KEY (`idBankAccountType`) REFERENCES `iprove`.`bankaccounttype` (`idBankAccountType`) ON UPDATE CASCADE ON DELETE SET NULL,
	CONSTRAINT `log_company_payment_data_FK_2` FOREIGN KEY (`idBankBranch`) REFERENCES `iprove`.`bankbranch` (`idBankBranch`) ON UPDATE CASCADE ON DELETE SET NULL,
	CONSTRAINT `log_company_payment_data_FK_3` FOREIGN KEY (`idCompanyDataStatus`) REFERENCES `iprove`.`company_data_status` (`idCompanyDataStatus`) ON UPDATE CASCADE ON DELETE CASCADE,
	CONSTRAINT `log_company_payment_data_FK_4` FOREIGN KEY (`idUser`) REFERENCES `iprove`.`user` (`idUser`) ON UPDATE CASCADE ON DELETE SET NULL
)
COLLATE='latin1_swedish_ci'
ENGINE=InnoDB
AUTO_INCREMENT=70
;





DELIMITER ///

CREATE TRIGGER tracking_company_payment_data AFTER UPDATE ON company_payment_data
    FOR EACH ROW
    BEGIN
        IF NEW.modifDate <> OLD.modifDate THEN  
            DELETE FROM log_company_payment_data WHERE idPaymentData = OLD.idPaymentData AND modifDate <> OLD.modifDate;
            INSERT INTO log_company_payment_data (idPaymentData,idBankBranch,idBank,accountNumber,idBankAccountType,cbu,pdfBank,idCompanyDataStatus,observation,idUser,modifDate) VALUES(OLD.idPaymentData, OLD.idBankBranch, OLD.idBank, OLD.accountNumber, OLD.idBankAccountType, OLD.cbu, OLD.pdfBank, OLD.idCompanyDataStatus, OLD.observation, OLD.idUser, OLD.modifDate);
        END IF;
    END;
///

DELIMITER ;



ALTER TABLE `company_tax_data`
	ADD COLUMN `modifDate` TIMESTAMP NULL DEFAULT current_timestamp() ON UPDATE current_timestamp() AFTER `idUser`;


CREATE TABLE `log_company_tax_data` (
	`idTaxData` INT(11) NOT NULL AUTO_INCREMENT,
	`pdfIb` VARCHAR(400) NULL DEFAULT NULL COLLATE 'latin1_swedish_ci',
	`ibType` VARCHAR(20) NULL DEFAULT NULL COLLATE 'latin1_swedish_ci',
	`idAgreementRegimeType` INT(11) NULL DEFAULT NULL,
	`pdfCM05` VARCHAR(400) NULL DEFAULT NULL COLLATE 'latin1_swedish_ci',
	`jurisdictionHeadquarters` INT(11) NULL DEFAULT NULL,
	`earningsRetentionType` VARCHAR(20) NULL DEFAULT NULL COLLATE 'latin1_swedish_ci',
	`retentionRegimen` INT(11) NULL DEFAULT NULL,
	`idEarningCategoryRetain` INT(11) NULL DEFAULT NULL,
	`earningsExclusionNumber` VARCHAR(30) NULL DEFAULT NULL COLLATE 'latin1_swedish_ci',
	`earningsExclusionFrom` DATE NULL DEFAULT NULL,
	`earningsExclusionTo` DATE NULL DEFAULT NULL,
	`earningsExclusionPdf` VARCHAR(400) NULL DEFAULT NULL COLLATE 'latin1_swedish_ci',
	`sussEmployeer` TINYINT(1) NULL DEFAULT NULL,
	`sussRetentionRegimen` TINYINT(1) NULL DEFAULT NULL,
	`sussExclusionNumber` VARCHAR(30) NULL DEFAULT NULL COLLATE 'latin1_swedish_ci',
	`sussExclusionFrom` DATE NULL DEFAULT NULL,
	`sussExclusionTo` DATE NULL DEFAULT NULL,
	`sussExclusionPdf` VARCHAR(400) NULL DEFAULT NULL COLLATE 'latin1_swedish_ci',
	`ivaRetentionAgent` TINYINT(1) NULL DEFAULT NULL,
	`ivaResolutionPdf` VARCHAR(400) NULL DEFAULT NULL COLLATE 'latin1_swedish_ci',
	`ivaIdRetentionRegime` INT(11) NULL DEFAULT NULL,
	`ivaIdCategoryRetain` INT(11) NULL DEFAULT NULL,
	`ivaExclusionNumber` VARCHAR(30) NULL DEFAULT NULL COLLATE 'latin1_swedish_ci',
	`ivaExclusionFrom` DATE NULL DEFAULT NULL,
	`ivaExclusionTo` DATE NULL DEFAULT NULL,
	`ivaExclusionPdf` VARCHAR(400) NULL DEFAULT NULL COLLATE 'latin1_swedish_ci',
	`idCompanyDataStatus` INT(11) NULL DEFAULT '1',
	`observation` VARCHAR(400) NULL DEFAULT NULL COLLATE 'latin1_swedish_ci',
	`idUser` INT(11) NULL DEFAULT NULL,
	`modifDate` TIMESTAMP NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
	PRIMARY KEY (`idTaxData`) USING BTREE,
	INDEX `company_tax_data_FK` (`idAgreementRegimeType`) USING BTREE,
	INDEX `company_tax_data_FK_1` (`idEarningCategoryRetain`) USING BTREE,
	INDEX `company_tax_data_FK_2` (`ivaIdCategoryRetain`) USING BTREE,
	INDEX `company_tax_data_FK_3` (`ivaIdRetentionRegime`) USING BTREE,
	INDEX `company_tax_data_FK_4` (`jurisdictionHeadquarters`) USING BTREE,
	INDEX `company_tax_data_FK_5` (`idCompanyDataStatus`) USING BTREE,
	INDEX `company_tax_data_FK_6` (`idUser`) USING BTREE,
	INDEX `company_tax_data_FK_7` (`retentionRegimen`) USING BTREE,
	CONSTRAINT `log_company_tax_data_FK` FOREIGN KEY (`idAgreementRegimeType`) REFERENCES `iprove`.`agreement_regime_type` (`idAgreementRegimeType`) ON UPDATE CASCADE ON DELETE SET NULL,
	CONSTRAINT `log_company_tax_data_FK_1` FOREIGN KEY (`idEarningCategoryRetain`) REFERENCES `iprove`.`category_retain` (`idCategoryRetain`) ON UPDATE CASCADE ON DELETE SET NULL,
	CONSTRAINT `log_company_tax_data_FK_2` FOREIGN KEY (`ivaIdCategoryRetain`) REFERENCES `iprove`.`category_retain` (`idCategoryRetain`) ON UPDATE CASCADE ON DELETE SET NULL,
	CONSTRAINT `log_company_tax_data_FK_3` FOREIGN KEY (`ivaIdRetentionRegime`) REFERENCES `iprove`.`retention_regime` (`idRetentionRegime`) ON UPDATE CASCADE ON DELETE SET NULL,
	CONSTRAINT `log_company_tax_data_FK_4` FOREIGN KEY (`jurisdictionHeadquarters`) REFERENCES `iprove`.`jurisdictions` (`idJurisdictions`) ON UPDATE CASCADE ON DELETE SET NULL,
	CONSTRAINT `log_company_tax_data_FK_5` FOREIGN KEY (`idCompanyDataStatus`) REFERENCES `iprove`.`company_data_status` (`idCompanyDataStatus`) ON UPDATE CASCADE ON DELETE CASCADE,
	CONSTRAINT `log_company_tax_data_FK_6` FOREIGN KEY (`idUser`) REFERENCES `iprove`.`user` (`idUser`) ON UPDATE CASCADE ON DELETE SET NULL,
	CONSTRAINT `log_company_tax_data_FK_7` FOREIGN KEY (`retentionRegimen`) REFERENCES `iprove`.`retention_regime` (`idRetentionRegime`) ON UPDATE CASCADE ON DELETE SET NULL
)
COLLATE='latin1_swedish_ci'
ENGINE=InnoDB
AUTO_INCREMENT=52
;



DELIMITER ///

CREATE TRIGGER tracking_company_tax_data AFTER UPDATE ON company_tax_data
    FOR EACH ROW
    BEGIN
        IF NEW.modifDate <> OLD.modifDate THEN  
            DELETE FROM log_company_tax_data WHERE idTaxData = OLD.idTaxData AND modifDate <> OLD.modifDate;
            INSERT INTO log_company_tax_data (idTaxData,pdfIb,ibType,idAgreementRegimeType,pdfCM05,jurisdictionHeadquarters,earningsRetentionType,retentionRegimen,idEarningCategoryRetain,earningsExclusionNumber,earningsExclusionFrom,earningsExclusionTo,earningsExclusionPdf,sussEmployeer,sussRetentionRegimen,sussExclusionNumber,sussExclusionFrom,sussExclusionTo,sussExclusionPdf,ivaRetentionAgent,ivaResolutionPdf,ivaIdRetentionRegime,ivaIdCategoryRetain,ivaExclusionNumber,ivaExclusionFrom,ivaExclusionTo,ivaExclusionPdf,idCompanyDataStatus,observation,idUser,modifDate) VALUES(OLD.idTaxData, OLD.pdfIb, OLD.ibType, OLD.idAgreementRegimeType, OLD.pdfCM05, OLD.jurisdictionHeadquarters, OLD.earningsRetentionType, OLD.retentionRegimen, OLD.idEarningCategoryRetain, OLD.earningsExclusionNumber, OLD.earningsExclusionFrom, OLD.earningsExclusionTo, OLD.earningsExclusionPdf, OLD.sussEmployeer, OLD.sussRetentionRegimen, OLD.sussExclusionNumber, OLD.sussExclusionFrom, OLD.sussExclusionTo, OLD.sussExclusionPdf, OLD.ivaRetentionAgent, OLD.ivaResolutionPdf, OLD.ivaIdRetentionRegime, OLD.ivaIdCategoryRetain, OLD.ivaExclusionNumber, OLD.ivaExclusionFrom, OLD.ivaExclusionTo, OLD.ivaExclusionPdf, OLD.idCompanyDataStatus, OLD.observation, OLD.idUser, OLD.modifDate);
        END IF;
    END;
///

DELIMITER ;



ALTER TABLE `company_jurisdiction_data`
	ADD COLUMN `modifDate` TIMESTAMP NULL DEFAULT current_timestamp() ON UPDATE current_timestamp() AFTER `idTaxData`;



CREATE TABLE `log_company_jurisdiction_data` (
	`idCompanyJurisdictionData` INT(11) NOT NULL AUTO_INCREMENT,
	`idJurisdiction` INT(11) NULL DEFAULT NULL,
	`coefficient` DOUBLE(22,0) NULL DEFAULT NULL,
	`aliquot` DOUBLE(22,0) NULL DEFAULT NULL,
	`certificateNumber` INT(11) NULL DEFAULT NULL,
	`certificateFrom` DATE NULL DEFAULT NULL,
	`certificateTo` DATE NULL DEFAULT NULL,
	`certificatePdf` VARCHAR(400) NULL DEFAULT NULL COLLATE 'latin1_swedish_ci',
	`retentionAgentHas` TINYINT(1) NULL DEFAULT NULL,
	`retentionAgentDate` DATE NULL DEFAULT NULL,
	`retentionAgentPdf` VARCHAR(400) NULL DEFAULT NULL COLLATE 'latin1_swedish_ci',
	`perceptionAgentHas` TINYINT(1) NULL DEFAULT NULL,
	`perceptionAgentDate` DATE NULL DEFAULT NULL,
	`perceptionAgentPdf` VARCHAR(400) NULL DEFAULT NULL COLLATE 'latin1_swedish_ci',
	`idTaxData` INT(11) NULL DEFAULT NULL,
	`modifDate` TIMESTAMP NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
	PRIMARY KEY (`idCompanyJurisdictionData`) USING BTREE,
	INDEX `company_jurisdiction_data_FK` (`idTaxData`) USING BTREE,
	INDEX `company_jurisdiction_data_FK_1` (`idJurisdiction`) USING BTREE,
	CONSTRAINT `log_company_jurisdiction_data_FK` FOREIGN KEY (`idTaxData`) REFERENCES `iprove`.`company_tax_data` (`idTaxData`) ON UPDATE CASCADE ON DELETE CASCADE,
	CONSTRAINT `log_company_jurisdiction_data_FK_1` FOREIGN KEY (`idJurisdiction`) REFERENCES `iprove`.`jurisdictions` (`idJurisdictions`) ON UPDATE CASCADE ON DELETE SET NULL
)
COLLATE='latin1_swedish_ci'
ENGINE=InnoDB
AUTO_INCREMENT=52
;


DELIMITER ///

CREATE TRIGGER tracking_company_jurisdiction_data AFTER UPDATE ON company_jurisdiction_data
    FOR EACH ROW
    BEGIN
        IF NEW.modifDate <> OLD.modifDate THEN  
            DELETE FROM log_company_jurisdiction_data WHERE idCompanyJurisdictionData = OLD.idCompanyJurisdictionData AND modifDate <> OLD.modifDate;
            INSERT INTO log_company_jurisdiction_data (idCompanyJurisdictionData,idJurisdiction,coefficient,aliquot,certificateNumber,certificateFrom,certificateTo,certificatePdf,retentionAgentHas,retentionAgentDate,retentionAgentPdf,perceptionAgentHas,perceptionAgentDate,perceptionAgentPdf,idTaxData,modifDate) VALUES(OLD.idCompanyJurisdictionData, OLD.idJurisdiction, OLD.coefficient, OLD.aliquot, OLD.certificateNumber, OLD.certificateFrom, OLD.certificateTo, OLD.certificatePdf, OLD.retentionAgentHas, OLD.retentionAgentDate, OLD.retentionAgentPdf, OLD.perceptionAgentHas, OLD.perceptionAgentDate, OLD.perceptionAgentPdf, OLD.idTaxData, OLD.modifDate);
        END IF;
    END;
///

DELIMITER ;

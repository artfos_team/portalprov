CREATE TABLE IF NOT EXISTS `country` (
	`idCountry` INT(111) NOT NULL AUTO_INCREMENT,
	`country` VARCHAR(255) NOT NULL DEFAULT '',
	PRIMARY KEY (`idCountry`)
)
COLLATE='utf8mb4_general_ci'
ENGINE=InnoDB
;

CREATE TABLE IF NOT EXISTS `province` (
	`idProvince` INT(111) NOT NULL AUTO_INCREMENT,
	`province` VARCHAR(255) NOT NULL DEFAULT '',
	`idCountry` INT(111) NOT NULL DEFAULT 0,
	PRIMARY KEY (`idProvince`)
)
COLLATE='utf8mb4_general_ci'
ENGINE=InnoDB
;

CREATE TABLE IF NOT EXISTS `agreement_regime_type` (
	`idAgreementRegimeType` INT(111) NOT NULL AUTO_INCREMENT,
	`agreementRegimeType` VARCHAR(255) NOT NULL DEFAULT '',
	PRIMARY KEY (`idAgreementRegimeType`)
)
COLLATE='utf8mb4_general_ci'
ENGINE=InnoDB
;

CREATE TABLE IF NOT EXISTS `jurisdictions` (
	`idJurisdictions` INT(111) NOT NULL AUTO_INCREMENT,
	`jurisdictions` VARCHAR(255) NOT NULL DEFAULT '',
	PRIMARY KEY (`idjurisdictions`)
)
COLLATE='utf8mb4_general_ci'
ENGINE=InnoDB
;

CREATE TABLE IF NOT EXISTS `retention_regime` (
	`idRetentionRegime` INT(111) NOT NULL AUTO_INCREMENT,
	`retentionRegime` VARCHAR(255) NOT NULL DEFAULT '',
	PRIMARY KEY (`idRetentionRegime`)
)
COLLATE='utf8mb4_general_ci'
ENGINE=InnoDB
;

CREATE TABLE IF NOT EXISTS `category_retain` (
	`idCategoryRetain` INT(111) NOT NULL AUTO_INCREMENT,
	`categoryRetain` VARCHAR(255) NOT NULL DEFAULT '',
	PRIMARY KEY (`idCategoryRetain`)
)
COLLATE='utf8mb4_general_ci'
ENGINE=InnoDB
;


INSERT INTO `iprove`.`permissions` (`id`, `name`, `description`, `controller`, `action`) VALUES ('31', 'Administración de tipo de régimen de convenio', 'Administración de tipo de régimen de convenio', 'agreementRegimeType', '*');
INSERT INTO `iprove`.`permissions` (`id`, `name`, `description`, `controller`, `action`) VALUES ('32', 'Administración de jurisdicciones', 'Administración de jurisdicciones', 'jurisdictions', '*');
INSERT INTO `iprove`.`permissions` (`id`, `name`, `description`, `controller`, `action`) VALUES ('33', 'Administración de régimen de retención', 'Administración de régimen de retención', 'retentionRegime', '*');
INSERT INTO `iprove`.`permissions` (`id`, `name`, `description`, `controller`, `action`) VALUES ('34', 'Administración de categoría a retener', 'Administración de categoría a retener', 'categoryRetain', '*');


INSERT INTO `iprove`.`roles_has_permissions` (`rolesId`, `permissionsId`) VALUES ('18', '31');
INSERT INTO `iprove`.`roles_has_permissions` (`rolesId`, `permissionsId`) VALUES ('18', '32');
INSERT INTO `iprove`.`roles_has_permissions` (`rolesId`, `permissionsId`) VALUES ('18', '33');
INSERT INTO `iprove`.`roles_has_permissions` (`rolesId`, `permissionsId`) VALUES ('18', '34');
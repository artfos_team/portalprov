CREATE TABLE `group_company` (
	`idGroupCompany` INT(111) NOT NULL AUTO_INCREMENT,
	`groupCompany` VARCHAR(255) NOT NULL DEFAULT '',
	PRIMARY KEY (`idGroupCompany`)
)
COLLATE='utf8mb4_general_ci'
ENGINE=InnoDB
AUTO_INCREMENT=1
;

INSERT INTO `iprove`.`permissions` (`id`, `name`, `description`, `controller`, `action`) VALUES ('40', 'Administración de Empresas del grupo Albanesi', 'Administración de Empresas del grupo Albanesi', 'groupCompanies', '*');
INSERT INTO `iprove`.`roles_has_permissions` (`rolesId`, `permissionsId`) VALUES ('18', '40');
ALTER TABLE `company_tax_data`
	ADD COLUMN `retentionRegimen` INT(11) NULL DEFAULT NULL AFTER `earningsRetentionType`,
	ADD CONSTRAINT `company_tax_data_FK_7` FOREIGN KEY (`retentionRegimen`) REFERENCES `retention_regime` (`idRetentionRegime`) ON UPDATE CASCADE ON DELETE SET NULL;

ALTER TABLE `company_tax_data`
	CHANGE COLUMN `ivaExclusionTo` `ivaExclusionTo` DATE NULL DEFAULT NULL COLLATE 'latin1_swedish_ci' AFTER `ivaExclusionFrom`;

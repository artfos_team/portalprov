CREATE TABLE `news` (
	`idNews` INT(11) NOT NULL AUTO_INCREMENT,
	`dateLine` TIMESTAMP NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
	`headLine` VARCHAR(250) NULL DEFAULT NULL COLLATE 'latin1_swedish_ci',
	`body` LONGTEXT NULL DEFAULT NULL COLLATE 'latin1_swedish_ci',
	PRIMARY KEY (`idNews`) USING BTREE
)
COLLATE='utf8mb4_general_ci'
ENGINE=InnoDB
;


INSERT INTO `iprove`.`permissions` (`id`, `name`, `description`, `controller`, `action`) VALUES ('41', 'Noticias', 'Noticias', 'news', '*');
INSERT INTO `iprove`.`roles_has_permissions` (`rolesId`, `permissionsId`) VALUES ('18', '41');
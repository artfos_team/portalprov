---Acordate que en la tabla company_tax_data poner todos en null el campo aliquot porque sino da conflicto con las relaciones en db

ALTER TABLE `category_retain`
	ADD COLUMN `indicadorRet` VARCHAR(2) NULL DEFAULT NULL AFTER `idRetentionRegime`;

UPDATE `iprove`.`category_retain` SET `indicadorRet`='01' WHERE  `idCategoryRetain`=17;
UPDATE `iprove`.`category_retain` SET `indicadorRet`='01' WHERE  `idCategoryRetain`=18;
UPDATE `iprove`.`category_retain` SET `indicadorRet`='02' WHERE  `idCategoryRetain`=19;
UPDATE `iprove`.`category_retain` SET `indicadorRet`='01' WHERE  `idCategoryRetain`=20;
UPDATE `iprove`.`category_retain` SET `indicadorRet`='02' WHERE  `idCategoryRetain`=21;
UPDATE `iprove`.`category_retain` SET `indicadorRet`='03' WHERE  `idCategoryRetain`=22;
UPDATE `iprove`.`category_retain` SET `indicadorRet`='04' WHERE  `idCategoryRetain`=23;
UPDATE `iprove`.`category_retain` SET `indicadorRet`='05' WHERE  `idCategoryRetain`=24;
UPDATE `iprove`.`category_retain` SET `indicadorRet`='06' WHERE  `idCategoryRetain`=25;
UPDATE `iprove`.`category_retain` SET `indicadorRet`='07' WHERE  `idCategoryRetain`=26;
UPDATE `iprove`.`category_retain` SET `indicadorRet`='08' WHERE  `idCategoryRetain`=27;
UPDATE `iprove`.`category_retain` SET `indicadorRet`='09' WHERE  `idCategoryRetain`=28;
UPDATE `iprove`.`category_retain` SET `indicadorRet`='01' WHERE  `idCategoryRetain`=29;
UPDATE `iprove`.`category_retain` SET `indicadorRet`='02' WHERE  `idCategoryRetain`=30;
UPDATE `iprove`.`category_retain` SET `indicadorRet`='03' WHERE  `idCategoryRetain`=31;
UPDATE `iprove`.`category_retain` SET `indicadorRet`='04' WHERE  `idCategoryRetain`=32;
UPDATE `iprove`.`category_retain` SET `indicadorRet`='05' WHERE  `idCategoryRetain`=33;
UPDATE `iprove`.`category_retain` SET `indicadorRet`='06' WHERE  `idCategoryRetain`=34;
UPDATE `iprove`.`category_retain` SET `indicadorRet`='07' WHERE  `idCategoryRetain`=35;
UPDATE `iprove`.`category_retain` SET `indicadorRet`='01' WHERE  `idCategoryRetain`=36;
UPDATE `iprove`.`category_retain` SET `indicadorRet`='01' WHERE  `idCategoryRetain`=39;
UPDATE `iprove`.`category_retain` SET `indicadorRet`='02' WHERE  `idCategoryRetain`=40;
UPDATE `iprove`.`category_retain` SET `indicadorRet`='01' WHERE  `idCategoryRetain`=41;
UPDATE `iprove`.`category_retain` SET `indicadorRet`='01' WHERE  `idCategoryRetain`=42;
UPDATE `iprove`.`category_retain` SET `indicadorRet`='02' WHERE  `idCategoryRetain`=43;
UPDATE `iprove`.`category_retain` SET `indicadorRet`='01' WHERE  `idCategoryRetain`=44;
UPDATE `iprove`.`category_retain` SET `indicadorRet`='01' WHERE  `idCategoryRetain`=37;
UPDATE `iprove`.`category_retain` SET `indicadorRet`='02' WHERE  `idCategoryRetain`=38;


--

ALTER TABLE `company_jurisdiction_data`
	CHANGE COLUMN `aliquot` `aliquot` INT(11) NULL DEFAULT NULL AFTER `coefficient`;

ALTER TABLE `company_jurisdiction_data`
	ADD CONSTRAINT `company_jurisdiction_data_FK_3` FOREIGN KEY (`aliquot`) REFERENCES `category_retain` (`idCategoryRetain`) ON UPDATE CASCADE ON DELETE SET NULL;

ALTER TABLE `category_retain`
	ADD COLUMN `idJurisdictions` INT(11) NULL DEFAULT NULL AFTER `idRetentionRegime`;

ALTER TABLE `category_retain`
	ADD COLUMN `ib` INT(1) NULL DEFAULT NULL AFTER `suss`;



---

INSERT INTO `iprove`.`jurisdictions` (`idJurisdictions`, `jurisdictions`, `codeERP`) VALUES ('25', 'Timbúes', 'RT');

---


---IC
INSERT INTO `iprove`.`category_retain` (`idCategoryRetain`, `categoryRetain`, `gain`, `suss`, `ib`, `vat`, `idJurisdictions`, `indicadorRet`) VALUES ('', 'Ret IIBB CABA 0%', '0', '0', '1', '0', '1', '02');
INSERT INTO `iprove`.`category_retain` (`idCategoryRetain`, `categoryRetain`, `gain`, `suss`, `ib`, `vat`, `idJurisdictions`, `indicadorRet`) VALUES ('', 'Ret IIBB CABA 0,10%', '0', '0', '1', '0', '1', '03');
INSERT INTO `iprove`.`category_retain` (`idCategoryRetain`, `categoryRetain`, `gain`, `suss`, `ib`, `vat`, `idJurisdictions`, `indicadorRet`) VALUES ('', 'Ret IIBB CABA 3%', '0', '0', '1', '0', '1', '04');
INSERT INTO `iprove`.`category_retain` (`idCategoryRetain`, `categoryRetain`, `gain`, `suss`, `ib`, `vat`, `idJurisdictions`, `indicadorRet`) VALUES ('', 'Ret IIBB CABA 4,5%', '0', '0', '1', '0', '1', '05');
INSERT INTO `iprove`.`category_retain` (`idCategoryRetain`, `categoryRetain`, `gain`, `suss`, `ib`, `vat`, `idJurisdictions`, `indicadorRet`) VALUES ('', 'Ret IIBB CABA 0,20%', '0', '0', '1', '0', '1', '06');
INSERT INTO `iprove`.`category_retain` (`idCategoryRetain`, `categoryRetain`, `gain`, `suss`, `ib`, `vat`, `idJurisdictions`, `indicadorRet`) VALUES ('', 'Ret IIBB CABA 0,50%', '0', '0', '1', '0', '1', '07');
INSERT INTO `iprove`.`category_retain` (`idCategoryRetain`, `categoryRetain`, `gain`, `suss`, `ib`, `vat`, `idJurisdictions`, `indicadorRet`) VALUES ('', 'Ret IIBB CABA 0,75%', '0', '0', '1', '0', '1', '08');
INSERT INTO `iprove`.`category_retain` (`idCategoryRetain`, `categoryRetain`, `gain`, `suss`, `ib`, `vat`, `idJurisdictions`, `indicadorRet`) VALUES ('', 'Ret IIBB CABA 0,90%', '0', '0', '1', '0', '1', '09');
INSERT INTO `iprove`.`category_retain` (`idCategoryRetain`, `categoryRetain`, `gain`, `suss`, `ib`, `vat`, `idJurisdictions`, `indicadorRet`) VALUES ('', 'Ret IIBB CABA 1,00%', '0', '0', '1', '0', '1', '10');
INSERT INTO `iprove`.`category_retain` (`idCategoryRetain`, `categoryRetain`, `gain`, `suss`, `ib`, `vat`, `idJurisdictions`, `indicadorRet`) VALUES ('', 'Ret IIBB CABA 1,25%', '0', '0', '1', '0', '1', '11');
INSERT INTO `iprove`.`category_retain` (`idCategoryRetain`, `categoryRetain`, `gain`, `suss`, `ib`, `vat`, `idJurisdictions`, `indicadorRet`) VALUES ('', 'Ret IIBB CABA 1,50%', '0', '0', '1', '0', '1', '12');
INSERT INTO `iprove`.`category_retain` (`idCategoryRetain`, `categoryRetain`, `gain`, `suss`, `ib`, `vat`, `idJurisdictions`, `indicadorRet`) VALUES ('', 'Ret IIBB CABA 1,75%', '0', '0', '1', '0', '1', '13');
INSERT INTO `iprove`.`category_retain` (`idCategoryRetain`, `categoryRetain`, `gain`, `suss`, `ib`, `vat`, `idJurisdictions`, `indicadorRet`) VALUES ('', 'Ret IIBB CABA 2,00%', '0', '0', '1', '0', '1', '14');
INSERT INTO `iprove`.`category_retain` (`idCategoryRetain`, `categoryRetain`, `gain`, `suss`, `ib`, `vat`, `idJurisdictions`, `indicadorRet`) VALUES ('', 'Ret IIBB CABA 2,50%', '0', '0', '1', '0', '1', '15');
INSERT INTO `iprove`.`category_retain` (`idCategoryRetain`, `categoryRetain`, `gain`, `suss`, `ib`, `vat`, `idJurisdictions`, `indicadorRet`) VALUES ('', 'Ret IIBB CABA 4,00%', '0', '0', '1', '0', '1', '16');

---IB
INSERT INTO `iprove`.`category_retain` (`idCategoryRetain`, `categoryRetain`, `gain`, `suss`, `ib`, `vat`, `idJurisdictions`, `indicadorRet`) VALUES ('', 'Ret IIBB BSAS 0,00%', '0', '0', '1', '0', '2', '01');
INSERT INTO `iprove`.`category_retain` (`idCategoryRetain`, `categoryRetain`, `gain`, `suss`, `ib`, `vat`, `idJurisdictions`, `indicadorRet`) VALUES ('', 'Ret IIBB BSAS 0,01%', '0', '0', '1', '0', '2', '02');
INSERT INTO `iprove`.`category_retain` (`idCategoryRetain`, `categoryRetain`, `gain`, `suss`, `ib`, `vat`, `idJurisdictions`, `indicadorRet`) VALUES ('', 'Ret IIBB BSAS 0,05%', '0', '0', '1', '0', '2', '03');
INSERT INTO `iprove`.`category_retain` (`idCategoryRetain`, `categoryRetain`, `gain`, `suss`, `ib`, `vat`, `idJurisdictions`, `indicadorRet`) VALUES ('', 'Ret IIBB BSAS 0,10%', '0', '0', '1', '0', '2', '04');
INSERT INTO `iprove`.`category_retain` (`idCategoryRetain`, `categoryRetain`, `gain`, `suss`, `ib`, `vat`, `idJurisdictions`, `indicadorRet`) VALUES ('', 'Ret IIBB BSAS 0,20%', '0', '0', '1', '0', '2', '05');
INSERT INTO `iprove`.`category_retain` (`idCategoryRetain`, `categoryRetain`, `gain`, `suss`, `ib`, `vat`, `idJurisdictions`, `indicadorRet`) VALUES ('', 'Ret IIBB BSAS 0,30%', '0', '0', '1', '0', '2', '06');
INSERT INTO `iprove`.`category_retain` (`idCategoryRetain`, `categoryRetain`, `gain`, `suss`, `ib`, `vat`, `idJurisdictions`, `indicadorRet`) VALUES ('', 'Ret IIBB BSAS 0,40%', '0', '0', '1', '0', '2', '07');
INSERT INTO `iprove`.`category_retain` (`idCategoryRetain`, `categoryRetain`, `gain`, `suss`, `ib`, `vat`, `idJurisdictions`, `indicadorRet`) VALUES ('', 'Ret IIBB BSAS 0,50%', '0', '0', '1', '0', '2', '08');
INSERT INTO `iprove`.`category_retain` (`idCategoryRetain`, `categoryRetain`, `gain`, `suss`, `ib`, `vat`, `idJurisdictions`, `indicadorRet`) VALUES ('', 'Ret IIBB BSAS 0,70%', '0', '0', '1', '0', '2', '09');
INSERT INTO `iprove`.`category_retain` (`idCategoryRetain`, `categoryRetain`, `gain`, `suss`, `ib`, `vat`, `idJurisdictions`, `indicadorRet`) VALUES ('', 'Ret IIBB BSAS 0,80%', '0', '0', '1', '0', '2', '10');
INSERT INTO `iprove`.`category_retain` (`idCategoryRetain`, `categoryRetain`, `gain`, `suss`, `ib`, `vat`, `idJurisdictions`, `indicadorRet`) VALUES ('', 'Ret IIBB BSAS 0,90%', '0', '0', '1', '0', '2', '11');
INSERT INTO `iprove`.`category_retain` (`idCategoryRetain`, `categoryRetain`, `gain`, `suss`, `ib`, `vat`, `idJurisdictions`, `indicadorRet`) VALUES ('', 'Ret IIBB BSAS 1,00%', '0', '0', '1', '0', '2', '12');
INSERT INTO `iprove`.`category_retain` (`idCategoryRetain`, `categoryRetain`, `gain`, `suss`, `ib`, `vat`, `idJurisdictions`, `indicadorRet`) VALUES ('', 'Ret IIBB BSAS 1,10%', '0', '0', '1', '0', '2', '13');
INSERT INTO `iprove`.`category_retain` (`idCategoryRetain`, `categoryRetain`, `gain`, `suss`, `ib`, `vat`, `idJurisdictions`, `indicadorRet`) VALUES ('', 'Ret IIBB BSAS 1,20%', '0', '0', '1', '0', '2', '14');
INSERT INTO `iprove`.`category_retain` (`idCategoryRetain`, `categoryRetain`, `gain`, `suss`, `ib`, `vat`, `idJurisdictions`, `indicadorRet`) VALUES ('', 'Ret IIBB BSAS 1,30%', '0', '0', '1', '0', '2', '15');
INSERT INTO `iprove`.`category_retain` (`idCategoryRetain`, `categoryRetain`, `gain`, `suss`, `ib`, `vat`, `idJurisdictions`, `indicadorRet`) VALUES ('', 'Ret IIBB BSAS 1,50%', '0', '0', '1', '0', '2', '16');
INSERT INTO `iprove`.`category_retain` (`idCategoryRetain`, `categoryRetain`, `gain`, `suss`, `ib`, `vat`, `idJurisdictions`, `indicadorRet`) VALUES ('', 'Ret IIBB BSAS 1,60%', '0', '0', '1', '0', '2', '17');
INSERT INTO `iprove`.`category_retain` (`idCategoryRetain`, `categoryRetain`, `gain`, `suss`, `ib`, `vat`, `idJurisdictions`, `indicadorRet`) VALUES ('', 'Ret IIBB BSAS 1,75%', '0', '0', '1', '0', '2', '18');
INSERT INTO `iprove`.`category_retain` (`idCategoryRetain`, `categoryRetain`, `gain`, `suss`, `ib`, `vat`, `idJurisdictions`, `indicadorRet`) VALUES ('', 'Ret IIBB BSAS 2,00%', '0', '0', '1', '0', '2', '19');
INSERT INTO `iprove`.`category_retain` (`idCategoryRetain`, `categoryRetain`, `gain`, `suss`, `ib`, `vat`, `idJurisdictions`, `indicadorRet`) VALUES ('', 'Ret IIBB BSAS 2,50%', '0', '0', '1', '0', '2', '20');
INSERT INTO `iprove`.`category_retain` (`idCategoryRetain`, `categoryRetain`, `gain`, `suss`, `ib`, `vat`, `idJurisdictions`, `indicadorRet`) VALUES ('', 'Ret IIBB BSAS 3,00%', '0', '0', '1', '0', '2', '21');
INSERT INTO `iprove`.`category_retain` (`idCategoryRetain`, `categoryRetain`, `gain`, `suss`, `ib`, `vat`, `idJurisdictions`, `indicadorRet`) VALUES ('', 'Ret IIBB BSAS 3,50%', '0', '0', '1', '0', '2', '22');
INSERT INTO `iprove`.`category_retain` (`idCategoryRetain`, `categoryRetain`, `gain`, `suss`, `ib`, `vat`, `idJurisdictions`, `indicadorRet`) VALUES ('', 'Ret IIBB BSAS 4,00%', '0', '0', '1', '0', '2', '23');
INSERT INTO `iprove`.`category_retain` (`idCategoryRetain`, `categoryRetain`, `gain`, `suss`, `ib`, `vat`, `idJurisdictions`, `indicadorRet`) VALUES ('', 'Ret IIBB BSAS 5,00%', '0', '0', '1', '0', '2', '24');
INSERT INTO `iprove`.`category_retain` (`idCategoryRetain`, `categoryRetain`, `gain`, `suss`, `ib`, `vat`, `idJurisdictions`, `indicadorRet`) VALUES ('', 'Ret IIBB BSAS 6,00%', '0', '0', '1', '0', '2', '25');
INSERT INTO `iprove`.`category_retain` (`idCategoryRetain`, `categoryRetain`, `gain`, `suss`, `ib`, `vat`, `idJurisdictions`, `indicadorRet`) VALUES ('', 'Ret IIBB BSAS 8,00%', '0', '0', '1', '0', '2', '26');


---IO
INSERT INTO `iprove`.`category_retain` (`idCategoryRetain`, `categoryRetain`, `gain`, `suss`, `ib`, `vat`, `idJurisdictions`, `indicadorRet`) VALUES ('', 'Ret IIBB Cba Contribuyente 0%', '0', '0', '1', '0', '4', '01');
INSERT INTO `iprove`.`category_retain` (`idCategoryRetain`, `categoryRetain`, `gain`, `suss`, `ib`, `vat`, `idJurisdictions`, `indicadorRet`) VALUES ('', 'Ret IIBB Cba No Inscript BI 100% Alic 7%', '0', '0', '1', '0', '4', '02');
INSERT INTO `iprove`.`category_retain` (`idCategoryRetain`, `categoryRetain`, `gain`, `suss`, `ib`, `vat`, `idJurisdictions`, `indicadorRet`) VALUES ('', 'Ret IIBB Cba Contribuyente CM 3,5%', '0', '0', '1', '0', '4', '03');
INSERT INTO `iprove`.`category_retain` (`idCategoryRetain`, `categoryRetain`, `gain`, `suss`, `ib`, `vat`, `idJurisdictions`, `indicadorRet`) VALUES ('', 'Ret IIBB Cba Locac. Inmueb/int/Com 3,5%', '0', '0', '1', '0', '4', '04');
INSERT INTO `iprove`.`category_retain` (`idCategoryRetain`, `categoryRetain`, `gain`, `suss`, `ib`, `vat`, `idJurisdictions`, `indicadorRet`) VALUES ('', 'Ret IIBB Cba Contribuyente 0,05%', '0', '0', '1', '0', '4', '05');
INSERT INTO `iprove`.`category_retain` (`idCategoryRetain`, `categoryRetain`, `gain`, `suss`, `ib`, `vat`, `idJurisdictions`, `indicadorRet`) VALUES ('', 'Ret IIBB Cba Contribuyente 0,1%', '0', '0', '1', '0', '4', '06');
INSERT INTO `iprove`.`category_retain` (`idCategoryRetain`, `categoryRetain`, `gain`, `suss`, `ib`, `vat`, `idJurisdictions`, `indicadorRet`) VALUES ('', 'Ret IIBB Cba Pad. Alto Riego Base 50% 5%', '0', '0', '1', '0', '4', '07');
INSERT INTO `iprove`.`category_retain` (`idCategoryRetain`, `categoryRetain`, `gain`, `suss`, `ib`, `vat`, `idJurisdictions`, `indicadorRet`) VALUES ('', 'Ret IIBB Cba Pad. Alto Riego Base 80% 5%', '0', '0', '1', '0', '4', '08');
INSERT INTO `iprove`.`category_retain` (`idCategoryRetain`, `categoryRetain`, `gain`, `suss`, `ib`, `vat`, `idJurisdictions`, `indicadorRet`) VALUES ('', 'Ret IIBB Cba Contribuyente 0,2%', '0', '0', '1', '0', '4', '09');
INSERT INTO `iprove`.`category_retain` (`idCategoryRetain`, `categoryRetain`, `gain`, `suss`, `ib`, `vat`, `idJurisdictions`, `indicadorRet`) VALUES ('', 'Ret IIBB Cba Contribuyente 0,3%', '0', '0', '1', '0', '4', '10');
INSERT INTO `iprove`.`category_retain` (`idCategoryRetain`, `categoryRetain`, `gain`, `suss`, `ib`, `vat`, `idJurisdictions`, `indicadorRet`) VALUES ('', 'Ret IIBB Cba Contribuyente 0,5%', '0', '0', '1', '0', '4', '11');
INSERT INTO `iprove`.`category_retain` (`idCategoryRetain`, `categoryRetain`, `gain`, `suss`, `ib`, `vat`, `idJurisdictions`, `indicadorRet`) VALUES ('', 'Ret IIBB Cba Contribuyente 0,75%', '0', '0', '1', '0', '4', '12');
INSERT INTO `iprove`.`category_retain` (`idCategoryRetain`, `categoryRetain`, `gain`, `suss`, `ib`, `vat`, `idJurisdictions`, `indicadorRet`) VALUES ('', 'Ret IIBB Cba Contribuyente 0,9%', '0', '0', '1', '0', '4', '13');
INSERT INTO `iprove`.`category_retain` (`idCategoryRetain`, `categoryRetain`, `gain`, `suss`, `ib`, `vat`, `idJurisdictions`, `indicadorRet`) VALUES ('', 'Ret IIBB Cba Contribuyente 1%', '0', '0', '1', '0', '4', '14');
INSERT INTO `iprove`.`category_retain` (`idCategoryRetain`, `categoryRetain`, `gain`, `suss`, `ib`, `vat`, `idJurisdictions`, `indicadorRet`) VALUES ('', 'Ret IIBB Cba Contribuyente 1,25%', '0', '0', '1', '0', '4', '15');
INSERT INTO `iprove`.`category_retain` (`idCategoryRetain`, `categoryRetain`, `gain`, `suss`, `ib`, `vat`, `idJurisdictions`, `indicadorRet`) VALUES ('', 'Ret IIBB Cba Contribuyente 1,50%', '0', '0', '1', '0', '4', '16');
INSERT INTO `iprove`.`category_retain` (`idCategoryRetain`, `categoryRetain`, `gain`, `suss`, `ib`, `vat`, `idJurisdictions`, `indicadorRet`) VALUES ('', 'Ret IIBB Cba Contribuyente 1,75%', '0', '0', '1', '0', '4', '17');
INSERT INTO `iprove`.`category_retain` (`idCategoryRetain`, `categoryRetain`, `gain`, `suss`, `ib`, `vat`, `idJurisdictions`, `indicadorRet`) VALUES ('', 'Ret IIBB Cba Contribuyente 2%', '0', '0', '1', '0', '4', '18');
INSERT INTO `iprove`.`category_retain` (`idCategoryRetain`, `categoryRetain`, `gain`, `suss`, `ib`, `vat`, `idJurisdictions`, `indicadorRet`) VALUES ('', 'Ret IIBB Cba Contribuyente 2,25%', '0', '0', '1', '0', '4', '19');
INSERT INTO `iprove`.`category_retain` (`idCategoryRetain`, `categoryRetain`, `gain`, `suss`, `ib`, `vat`, `idJurisdictions`, `indicadorRet`) VALUES ('', 'Ret IIBB Cba Contribuyente 2,50%', '0', '0', '1', '0', '4', '20');
INSERT INTO `iprove`.`category_retain` (`idCategoryRetain`, `categoryRetain`, `gain`, `suss`, `ib`, `vat`, `idJurisdictions`, `indicadorRet`) VALUES ('', 'Ret IIBB Cba Contribuyente 2,75%', '0', '0', '1', '0', '4', '21');
INSERT INTO `iprove`.`category_retain` (`idCategoryRetain`, `categoryRetain`, `gain`, `suss`, `ib`, `vat`, `idJurisdictions`, `indicadorRet`) VALUES ('', 'Ret IIBB Cba Contribuyente 3%', '0', '0', '1', '0', '4', '22');
INSERT INTO `iprove`.`category_retain` (`idCategoryRetain`, `categoryRetain`, `gain`, `suss`, `ib`, `vat`, `idJurisdictions`, `indicadorRet`) VALUES ('', 'Ret IIBB Cba Contribuyente 3,25%', '0', '0', '1', '0', '4', '23');
INSERT INTO `iprove`.`category_retain` (`idCategoryRetain`, `categoryRetain`, `gain`, `suss`, `ib`, `vat`, `idJurisdictions`, `indicadorRet`) VALUES ('', 'Ret IIBB Cba Contribuyente 3,50%', '0', '0', '1', '0', '4', '24');
INSERT INTO `iprove`.`category_retain` (`idCategoryRetain`, `categoryRetain`, `gain`, `suss`, `ib`, `vat`, `idJurisdictions`, `indicadorRet`) VALUES ('', 'Ret IIBB Cba Contribuyente 3,75%', '0', '0', '1', '0', '4', '25');
INSERT INTO `iprove`.`category_retain` (`idCategoryRetain`, `categoryRetain`, `gain`, `suss`, `ib`, `vat`, `idJurisdictions`, `indicadorRet`) VALUES ('', 'Ret IIBB Cba Contribuyente 4%', '0', '0', '1', '0', '4', '26');
INSERT INTO `iprove`.`category_retain` (`idCategoryRetain`, `categoryRetain`, `gain`, `suss`, `ib`, `vat`, `idJurisdictions`, `indicadorRet`) VALUES ('', 'Ret IIBB Cba Contribuyente 4,25%', '0', '0', '1', '0', '4', '27');
INSERT INTO `iprove`.`category_retain` (`idCategoryRetain`, `categoryRetain`, `gain`, `suss`, `ib`, `vat`, `idJurisdictions`, `indicadorRet`) VALUES ('', 'Ret IIBB Cba Contribuyente 4,50%', '0', '0', '1', '0', '4', '28');
INSERT INTO `iprove`.`category_retain` (`idCategoryRetain`, `categoryRetain`, `gain`, `suss`, `ib`, `vat`, `idJurisdictions`, `indicadorRet`) VALUES ('', 'Ret IIBB Cba Contribuyente 4,75%', '0', '0', '1', '0', '4', '29');
INSERT INTO `iprove`.`category_retain` (`idCategoryRetain`, `categoryRetain`, `gain`, `suss`, `ib`, `vat`, `idJurisdictions`, `indicadorRet`) VALUES ('', 'Ret IIBB Cba Contribuyente 5%', '0', '0', '1', '0', '4', '30');



---IR
INSERT INTO `iprove`.`category_retain` (`idCategoryRetain`, `categoryRetain`, `gain`, `suss`, `ib`, `vat`, `idJurisdictions`, `indicadorRet`) VALUES ('', 'Ret IIBB R.Negro Sujeto Local 1%', '0', '0', '1', '0', '22', '01');
INSERT INTO `iprove`.`category_retain` (`idCategoryRetain`, `categoryRetain`, `gain`, `suss`, `ib`, `vat`, `idJurisdictions`, `indicadorRet`) VALUES ('', 'Ret IIBB R.Negro Sujeto CM1%', '0', '0', '1', '0', '22', '02');
INSERT INTO `iprove`.`category_retain` (`idCategoryRetain`, `categoryRetain`, `gain`, `suss`, `ib`, `vat`, `idJurisdictions`, `indicadorRet`) VALUES ('', 'Ret IIBB R.Negro Sujeto No insc en RN 5%', '0', '0', '1', '0', '22', '03');
INSERT INTO `iprove`.`category_retain` (`idCategoryRetain`, `categoryRetain`, `gain`, `suss`, `ib`, `vat`, `idJurisdictions`, `indicadorRet`) VALUES ('', 'Ret IIBB R.Negro Sujeto Local 0,5%', '0', '0', '1', '0', '22', '04');



---IS
INSERT INTO `iprove`.`category_retain` (`idCategoryRetain`, `categoryRetain`, `gain`, `suss`, `ib`, `vat`, `idJurisdictions`, `indicadorRet`) VALUES ('', 'Ret IIBB Sta Fe Constribuyente Locales', '0', '0', '1', '0', '13', '01');
INSERT INTO `iprove`.`category_retain` (`idCategoryRetain`, `categoryRetain`, `gain`, `suss`, `ib`, `vat`, `idJurisdictions`, `indicadorRet`) VALUES ('', 'Ret IIBB Sta Fe Constribuyente CM', '0', '0', '1', '0', '13', '02');
INSERT INTO `iprove`.`category_retain` (`idCategoryRetain`, `categoryRetain`, `gain`, `suss`, `ib`, `vat`, `idJurisdictions`, `indicadorRet`) VALUES ('', 'Ret IIBB Sta Fe Act Industrial 0,1%', '0', '0', '1', '0', '13', '03');
INSERT INTO `iprove`.`category_retain` (`idCategoryRetain`, `categoryRetain`, `gain`, `suss`, `ib`, `vat`, `idJurisdictions`, `indicadorRet`) VALUES ('', 'Ret IIBB Sta Fe Act Industrial 0,7%', '0', '0', '1', '0', '13', '04');
INSERT INTO `iprove`.`category_retain` (`idCategoryRetain`, `categoryRetain`, `gain`, `suss`, `ib`, `vat`, `idJurisdictions`, `indicadorRet`) VALUES ('', 'Ret IIBB Sta Fe Act Constr 0,8%', '0', '0', '1', '0', '13', '05');
INSERT INTO `iprove`.`category_retain` (`idCategoryRetain`, `categoryRetain`, `gain`, `suss`, `ib`, `vat`, `idJurisdictions`, `indicadorRet`) VALUES ('', 'Ret IIBB Sta Fe Act Constr 2%', '0', '0', '1', '0', '13', '06');
INSERT INTO `iprove`.`category_retain` (`idCategoryRetain`, `categoryRetain`, `gain`, `suss`, `ib`, `vat`, `idJurisdictions`, `indicadorRet`) VALUES ('', 'Ret IIBB Sta Fe Act Constr 1,5%', '0', '0', '1', '0', '13', '07');
INSERT INTO `iprove`.`category_retain` (`idCategoryRetain`, `categoryRetain`, `gain`, `suss`, `ib`, `vat`, `idJurisdictions`, `indicadorRet`) VALUES ('', 'Ret IIBB Sta Fe Act Constr 3%', '0', '0', '1', '0', '13', '08');
INSERT INTO `iprove`.`category_retain` (`idCategoryRetain`, `categoryRetain`, `gain`, `suss`, `ib`, `vat`, `idJurisdictions`, `indicadorRet`) VALUES ('', 'Ret IIBB Sta Fe Riesgo fiscal 5,4%', '0', '0', '1', '0', '13', '09');
INSERT INTO `iprove`.`category_retain` (`idCategoryRetain`, `categoryRetain`, `gain`, `suss`, `ib`, `vat`, `idJurisdictions`, `indicadorRet`) VALUES ('', 'Ret IIBB Sta Fe Act Médico 2,5%', '0', '0', '1', '0', '13', '10');
INSERT INTO `iprove`.`category_retain` (`idCategoryRetain`, `categoryRetain`, `gain`, `suss`, `ib`, `vat`, `idJurisdictions`, `indicadorRet`) VALUES ('', 'Ret IIBB Sta Fe Otros Contr 3,6%', '0', '0', '1', '0', '13', '11');
INSERT INTO `iprove`.`category_retain` (`idCategoryRetain`, `categoryRetain`, `gain`, `suss`, `ib`, `vat`, `idJurisdictions`, `indicadorRet`) VALUES ('', 'Ret IIBB Sta Fe 4,5%', '0', '0', '1', '0', '13', '12');
INSERT INTO `iprove`.`category_retain` (`idCategoryRetain`, `categoryRetain`, `gain`, `suss`, `ib`, `vat`, `idJurisdictions`, `indicadorRet`) VALUES ('', 'Ret IIBB Sta Fe 3,6% BI 10%', '0', '0', '1', '0', '13', '13');
INSERT INTO `iprove`.`category_retain` (`idCategoryRetain`, `categoryRetain`, `gain`, `suss`, `ib`, `vat`, `idJurisdictions`, `indicadorRet`) VALUES ('', 'Ret IIBB Sta Fe 0,7% BI 50%', '0', '0', '1', '0', '13', '14');
INSERT INTO `iprove`.`category_retain` (`idCategoryRetain`, `categoryRetain`, `gain`, `suss`, `ib`, `vat`, `idJurisdictions`, `indicadorRet`) VALUES ('', 'Ret IIBB Sta Fe 3,6% BI 80%', '0', '0', '1', '0', '13', '15');


---N2
INSERT INTO `iprove`.`category_retain` (`idCategoryRetain`, `categoryRetain`, `gain`, `suss`, `ib`, `vat`, `idJurisdictions`, `indicadorRet`) VALUES ('', 'DGR  276/2017 2% Local', '0', '0', '1', '0', '20', '01');
INSERT INTO `iprove`.`category_retain` (`idCategoryRetain`, `categoryRetain`, `gain`, `suss`, `ib`, `vat`, `idJurisdictions`, `indicadorRet`) VALUES ('', 'DGR  276/2017 1,5% ML NQN', '0', '0', '1', '0', '20', '02');
INSERT INTO `iprove`.`category_retain` (`idCategoryRetain`, `categoryRetain`, `gain`, `suss`, `ib`, `vat`, `idJurisdictions`, `indicadorRet`) VALUES ('', 'DGR  276/2017 1% ML', '0', '0', '1', '0', '20', '03');
INSERT INTO `iprove`.`category_retain` (`idCategoryRetain`, `categoryRetain`, `gain`, `suss`, `ib`, `vat`, `idJurisdictions`, `indicadorRet`) VALUES ('', 'DGR  276/2017 4% NO INSCRIPTO', '0', '0', '1', '0', '20', '04');


---RT
INSERT INTO `iprove`.`category_retain` (`idCategoryRetain`, `categoryRetain`, `gain`, `suss`, `ib`, `vat`, `idJurisdictions`, `indicadorRet`) VALUES ('', 'OM 150/2016 Tasa general 0,525%', '0', '0', '1', '0', '25', '01');
INSERT INTO `iprove`.`category_retain` (`idCategoryRetain`, `categoryRetain`, `gain`, `suss`, `ib`, `vat`, `idJurisdictions`, `indicadorRet`) VALUES ('', 'OM 150/2016 Tasa emp. Constructoras 0,5%', '0', '0', '1', '0', '25', '02');


---TU
INSERT INTO `iprove`.`category_retain` (`idCategoryRetain`, `categoryRetain`, `gain`, `suss`, `ib`, `vat`, `idJurisdictions`, `indicadorRet`) VALUES ('', 'Ret IIBB TUCUMAN 0,00%', '0', '0', '1', '0', '15', '01');
INSERT INTO `iprove`.`category_retain` (`idCategoryRetain`, `categoryRetain`, `gain`, `suss`, `ib`, `vat`, `idJurisdictions`, `indicadorRet`) VALUES ('', 'Ret IIBB TUCUMAN 0,75%', '0', '0', '1', '0', '15', '02');
INSERT INTO `iprove`.`category_retain` (`idCategoryRetain`, `categoryRetain`, `gain`, `suss`, `ib`, `vat`, `idJurisdictions`, `indicadorRet`) VALUES ('', 'Ret IIBB TUCUMAN 1%', '0', '0', '1', '0', '15', '03');
INSERT INTO `iprove`.`category_retain` (`idCategoryRetain`, `categoryRetain`, `gain`, `suss`, `ib`, `vat`, `idJurisdictions`, `indicadorRet`) VALUES ('', 'Ret IIBB TUCUMAN 1,5%', '0', '0', '1', '0', '15', '04');
INSERT INTO `iprove`.`category_retain` (`idCategoryRetain`, `categoryRetain`, `gain`, `suss`, `ib`, `vat`, `idJurisdictions`, `indicadorRet`) VALUES ('', 'Ret IIBB TUCUMAN 1,8%', '0', '0', '1', '0', '15', '05');
INSERT INTO `iprove`.`category_retain` (`idCategoryRetain`, `categoryRetain`, `gain`, `suss`, `ib`, `vat`, `idJurisdictions`, `indicadorRet`) VALUES ('', 'Ret IIBB TUCUMAN 2%', '0', '0', '1', '0', '15', '06');
INSERT INTO `iprove`.`category_retain` (`idCategoryRetain`, `categoryRetain`, `gain`, `suss`, `ib`, `vat`, `idJurisdictions`, `indicadorRet`) VALUES ('', 'Ret IIBB TUCUMAN 2,5%', '0', '0', '1', '0', '15', '07');
INSERT INTO `iprove`.`category_retain` (`idCategoryRetain`, `categoryRetain`, `gain`, `suss`, `ib`, `vat`, `idJurisdictions`, `indicadorRet`) VALUES ('', 'Ret IIBB TUCUMAN 3%', '0', '0', '1', '0', '15', '08');
INSERT INTO `iprove`.`category_retain` (`idCategoryRetain`, `categoryRetain`, `gain`, `suss`, `ib`, `vat`, `idJurisdictions`, `indicadorRet`) VALUES ('', 'Ret IIBB TUCUMAN 3,5%', '0', '0', '1', '0', '15', '09');
INSERT INTO `iprove`.`category_retain` (`idCategoryRetain`, `categoryRetain`, `gain`, `suss`, `ib`, `vat`, `idJurisdictions`, `indicadorRet`) VALUES ('', 'Ret IIBB TUCUMAN 3,75%', '0', '0', '1', '0', '15', '10');
INSERT INTO `iprove`.`category_retain` (`idCategoryRetain`, `categoryRetain`, `gain`, `suss`, `ib`, `vat`, `idJurisdictions`, `indicadorRet`) VALUES ('', 'Ret IIBB TUCUMAN 4,50%', '0', '0', '1', '0', '15', '11');
INSERT INTO `iprove`.`category_retain` (`idCategoryRetain`, `categoryRetain`, `gain`, `suss`, `ib`, `vat`, `idJurisdictions`, `indicadorRet`) VALUES ('', 'Ret IIBB TUCUMAN 4,75%', '0', '0', '1', '0', '15', '12');
INSERT INTO `iprove`.`category_retain` (`idCategoryRetain`, `categoryRetain`, `gain`, `suss`, `ib`, `vat`, `idJurisdictions`, `indicadorRet`) VALUES ('', 'Ret IIBB TUCUMAN 5%', '0', '0', '1', '0', '15', '13');
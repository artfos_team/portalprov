(function () {

    'use strict';

    // Your application goes here.
     var resetForm = function($form) {
	    $form.find('input:text, input:password, input:file, select, textarea').val('');
	    $form.find('input:radio, input:checkbox')
	         .removeAttr('checked').removeAttr('selected');
	}
 
	jQuery.fnResetForm = resetForm;

})();
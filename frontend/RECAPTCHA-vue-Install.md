# Vue-Recaptcha

[![N|Solid](https://ps.w.org/advanced-nocaptcha-recaptcha/assets/icon-128x128.jpg?rev=1146799)](https://nodesource.com/products/nsolid)

### Installation

vue-recaptcha

Install

```sh
$ npm install --save vue-recaptcha
```
### How to Generate Google reCAPTCHA v2 Keys

First, we can start by giving gratitude that Google didn’t can reCAPTCHA v2 right away like they did Google+. Kidding aside if you do not have a Google Account already you will need to create one, specifically for the [Google reCAPTCHA admin](https://www.google.com/recaptcha/admin). Once you have an account created and you’re logged into the Google reCAPTCHA console you will be greeted with the register new site screen. If not, click the plus sign at the top right:

[![N|Solid](https://www.iqcomputing.com/wp-content/uploads/articles/6606/new-recaptcha-site.jpg)](https://nodesource.com/products/nsolid)

After complete the form Click Submit. Then, you will be presented with your reCAPTCHA keys. You will have a Site key, and a Secret key

[![N|Solid](https://www.iqcomputing.com/wp-content/uploads/articles/6606/recaptcha-keys.jpg)](https://nodesource.com/products/nsolid)


### info
https://www.npmjs.com/package/vue-recaptcha
https://www.iqcomputing.com/support/articles/generate-google-recaptcha-v2-keys/

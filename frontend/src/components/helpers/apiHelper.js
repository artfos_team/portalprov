import api from '../../api'

export default {
  getTotal (model, condition = '1') {
    return new Promise((resolve, reject) => {
      api.request('GET', 'api/count/' + model + '?condition=' + condition)
      .then(count => {
        resolve(parseInt(count.data))
      })
      .catch(error => {
        reject(error)
      })
    })
  },
  getQueryForApi (queryInfo) {
    console.log(queryInfo)
    if (!queryInfo) return ''
    let query = '?'
    // page
    query += 'pageSize=' + queryInfo.pageSize
    query += '&page=' + queryInfo.page

    // order
    if (queryInfo.sort && queryInfo.sort.prop) {
      query += '&orderProp=' + queryInfo.sort.prop
      query += '&order='
      if (queryInfo.sort.order === 'ascending') query += 'ASC'
      else if (queryInfo.sort.order === 'descending') query += 'DESC'
      else query += 'null'
    }
    // condition
    if (queryInfo.filters.length > 0) {
      let condition = '&condition=('
      queryInfo.filters.forEach((filter, idFilter) => {
        // Si hay mas de un filtro se pone el AND
        condition += idFilter > 0 ? ') AND (' : ''
        if (Array.isArray(filter.prop)) {
          filter.prop.forEach((prop, idProp) => {
            // Si hay mas de una propiedad en un mismo filtro se pone el OR
            condition += idProp > 0 ? ' OR ' : ''
            condition += this.getCondition(prop, filter.value, filter.type)
          })
        } else {
          condition += this.getCondition(filter.prop, filter.value, filter.type)
        }
      })
      condition += ')'
      query += encodeURI(condition)
    }
    return query
  },
  like (prop, value) {
    return prop + ' like \'%' + value + '%\' '
  },
  dateRange (prop, from, to) {
    return prop + ' >= \'' + from + '\' AND ' + prop + ' <= \'' + to + '\' '
  },
  getCondition (prop, value, type) {
    if (value === null) value = ''
    // Filtro por rango de fechas
    switch (type) {
      case 'dateRange':
        return value ? this.dateRange(prop, value[0], value[1]) : '1'
      case 'multiSelect':
        let cond = value.length > 0 ? '' : '1'
        value.forEach((val, i) => {
          cond += i > 0 ? ' OR ' : ''
          cond += prop + '=' + val
        })
        return cond
      default:
        // Filtro normal
        return this.like(prop, value)
    }
  }
}

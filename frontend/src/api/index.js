import axios from 'axios'
import config from '../config'

export default {
  request (method, uri, data = null, debug = null) {
    debug = null
    if (debug) {
      if (!method) {
        console.error('API function call requires method argument')
        return
      }
      if (!uri) {
        console.error('API function call requires uri argument')
        return
      }
      if (uri === 'api/login') {
        var readyPromise = new Promise(resolve => {
          var dato = {
            username: data.username,
            password: data.password,
            token: '123456789321',
            reqPws: '1',
            user: 'cvalicenti'
          }
          var response = {
            data: dato
          }
          resolve(response)
        })
        return readyPromise
      }
      return
    } else {
      if (!method) {
        console.error('API function call requires method argument')
        return
      }
      if (!uri) {
        console.error('API function call requires uri argument')
        return
      }
      var url = config.serverURI + uri
      var jwt = window.localStorage.getItem('token')
      var headers
      if (jwt) {
        headers = {
          'Content-Type': 'text/plain',
          'X-Authorization': jwt,
          'Accept-Encoding': 'gzip, compress'
        }
      } else {
        headers = {
          'Content-Type': 'text/plain',
          'Accept-Encoding': 'gzip, compress'
        }
      }
      return axios({
        method: method,
        url: url,
        data: data,
        headers: headers
      })
    }
  }
}

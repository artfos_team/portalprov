export default {
  TOGGLE_LOADING (state) {
    state.callingAPI = !state.callingAPI
  },
  TOGGLE_SEARCHING (state) {
    state.searching = (state.searching === '') ? 'loading' : ''
  },
  SET_USER (state, user) {
    state.user = user
  },
  SET_TOKEN (state, token) {
    state.token = token
  },
  SET_BUYORDERS (state, ordenesCompra) {
    state.ordenesCompra = ordenesCompra
  },
  SET_OTHEROPERATIONS (state, otherOperations) {
    state.otherOperations = otherOperations
  },
  SET_MESSAGES (state, messages) {
    state.messages = messages
  },
  SET_PROVMESSAGES (state, provMessages) {
    state.provMessages = provMessages
  },
  SET_RECEIVEDNEW (state, receivedNew) {
    state.receivedNew = receivedNew
  },
  SET_STATUS (state, status) {
    state.status = status
  },
  SET_COUNTSTATUS (state, countstatus) {
    state.countStatus = countstatus
  },
  SET_BUYORDERDOCUMENTTYPE (state, buyOrderDocumentType) {
    state.buyOrderDocumentType = buyOrderDocumentType
  },
  SET_BUYORDERDOCUMENTTYPECODE (state, buyOrderDocumentTypeCode) {
    state.buyOrderDocumentTypeCode = buyOrderDocumentTypeCode
  },
  SET_CONTACTS (state, contacts) {
    state.contacts = contacts
  },
  SET_PROVIDERS (state, providers) {
    state.providers = providers
  },
  SET_COMPULSAS (state, compulsas) {
    state.compulsas = compulsas
  },
  SET_FRONTENDMODULES (state, frontendModules) {
    state.frontendModules = frontendModules
  },
  SET_ROLESHASMODULES (state, rolesHasModules) {
    state.rolesHasModules = rolesHasModules
  }

}

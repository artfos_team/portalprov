import Vue from 'vue'
import Vuex from 'vuex'
// import state from './state'
import actions from './actions'
// import mutations from './mutations'

Vue.use(Vuex)

const getDefaultState = () => {
  return {
    callingAPI: false,
    userReturned: false,
    apiCalls: 0,
    searching: '',
    serverURI: 'http://10.110.1.136:8080',
    user: null,
    token: null,
    ordenesCompra: [],
    otherOperations: [],
    buyOrderDocumentType: [],
    buyOrderDocumentStatus: [],
    frontendModules: [],
    rolesHasModules: [],
    facturas: [],
    messages: [],
    provMessages: [],
    receivedNew: [],
    contacts: [],
    documents: [],
    compulsas: [],
    providers: [],
    countStatus: [],
    companyDataStatus: [],
    services: [],
    countries: [],
    groupCompanies: [],
    news: [],
    provinces: [],
    banks: [],
    bankBranches: [],
    accountTypes: [],
    regimenTypes: [],
    jurisdictions: [],
    retentionRegimes: [],
    retentionRegimesVAT: [],
    retentionCategories: [],
    companyDataStage: [],
    retentionCategoriesVAT: [],
    sussRetentionCategory: [],
    ibRetentionCategory: [],
    allCertification: [],
    invoiceMask: [/[1-9]/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/],
    userInfo: {
      messages: [],
      notifications: [],
      tasks: []
    }
  }
}

const state = getDefaultState()

const mutations = {
  TOGGLE_LOADING (state) {
    state.callingAPI = !state.callingAPI
  },
  START_LOADING (state) {
    state.apiCalls++
  },
  STOP_LOADING (state) {
    state.apiCalls--
  },
  TOGGLE_SEARCHING (state) {
    state.searching = (state.searching === '') ? 'loading' : ''
  },
  SET_USER (state, user) {
    state.user = user
  },
  SET_TOKEN (state, token) {
    state.token = token
  },
  SET_BUYORDERS (state, ordenesCompra) {
    state.ordenesCompra = ordenesCompra
  },
  SET_OTHEROPERATIONS (state, otherOperations) {
    state.otherOperations = otherOperations
  },
  SET_MESSAGES (state, messages) {
    state.messages = messages
  },
  SET_PROVMESSAGES (state, provMessages) {
    state.provMessages = provMessages
  },
  SET_RECEIVEDNEW (state, receivedNew) {
    state.receivedNew = receivedNew
  },
  SET_STATUS (state, status) {
    state.status = status
  },
  SET_COUNTSTATUS (state, countstatus) {
    state.countStatus = countstatus
  },
  SET_BUYORDERDOCUMENTTYPE (state, buyOrderDocumentType) {
    state.buyOrderDocumentType = buyOrderDocumentType
  },
  SET_BUYORDERDOCUMENTTYPECODE (state, buyOrderDocumentTypeCode) {
    state.buyOrderDocumentTypeCode = buyOrderDocumentTypeCode
  },
  SET_BUYORDERDOCUMENTSTATUS (state, buyOrderDocumentStatus) {
    state.buyOrderDocumentStatus = buyOrderDocumentStatus
  },
  SET_CONTACTS (state, contacts) {
    state.contacts = contacts
  },
  CONCAT_PROVIDERS (state, providers) {
    state.providers = state.providers.concat(providers)
  },
  SET_PROVIDERS (state, providers) {
    state.providers = providers
  },
  SET_DOCUMENTS (state, documents) {
    state.documents = documents
  },
  SET_COMPULSAS (state, compulsas) {
    state.compulsas = compulsas
  },
  SET_FRONTENDMODULES (state, frontendModules) {
    state.frontendModules = frontendModules
  },
  SET_ROLESHASMODULES (state, rolesHasModules) {
    state.rolesHasModules = rolesHasModules
  },
  SET_COMPANYDATASTATUS (state, companyDataStatus) {
    state.companyDataStatus = companyDataStatus
  },
  RESET_STATE (state) {
    Object.assign(state, getDefaultState())
  },
  SET_COMPANYDATASTAGE (state, companyDataStage) {
    state.companyDataStage = companyDataStage
  }
}

export default new Vuex.Store({
  state,
  actions,
  mutations
})

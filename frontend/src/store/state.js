export default {
  callingAPI: false,
  searching: '',
  serverURI: 'http://10.110.1.136:8080',
  user: null,
  token: null,
  ordenesCompra: [],
  otherOperations: [],
  buyOrderDocumentType: [],
  frontendModules: [],
  rolesHasModules: [],
  facturas: [],
  messages: [],
  provMessages: [],
  receivedNew: [],
  contacts: [],
  compulsas: [],
  providers: [],
  countStatus: [],
  invoiceMask: [/[1-9]/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/],
  userInfo: {
    messages: [],
    notifications: [],
    tasks: []
  }
}

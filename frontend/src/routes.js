import DashView from './components/Dash.vue'
import LoginView from './components/Login.vue'
import NotFoundView from './components/404.vue'

// Import Views - Dash
import DashboardView from './components/views/Dashboard.vue'
import TablesView from './components/views/Tables.vue'
import TasksView from './components/views/Tasks.vue'
import SettingView from './components/views/Setting.vue'
import AccessView from './components/views/Access.vue'
import ServerView from './components/views/Server.vue'
import ReposView from './components/views/Repos.vue'
import SolicitudView from './components/views/Solicitud.vue'
import IndividualView from './components/views/Individual.vue'
import SearchView from './components/views/Search.vue'
import BillsManagementView from './components/views/BillsManagement.vue'
import ChangePswView from './components/views/ChangePsw.vue'
import CommentsView from './components/views/Comments.vue'
import SelfDataView from './components/views/SelfData.vue'
import CommentDetailView from './components/views/CommentDetail.vue'
import AllCommentsView from './components/views/AllComments.vue'
import ReestablecerView from './components/views/Reestablecer.vue'
import PswReestablecidaView from './components/views/PswReestablecida.vue'
import InvoiceDetailView from './components/views/InvoiceDetail.vue'
import ReportsView from './components/views/Reports.vue'
import FinSolicitudView from './components/views/FinSolicitud.vue'
import AllContactsView from './components/views/AllContacts.vue'
import AllProvidersView from './components/views/AllProviders.vue'
import LicitacionesView from './components/views/Licitaciones.vue'
import ManualsView from './components/views/Manuals.vue'
import NewsView from './components/views/News.vue'
import ContactDetail from './components/views/ContactDetail.vue'
import SubmitDocumentationView from './components/views/SubmitDocumentation.vue'
import AltaDeUsuario from './components/views/AltaDeUsuario.vue'
import NewProviders from './components/views/NewProviders.vue'
import ProviderDetail from './components/views/ProviderDetail.vue'

// Routes
const routes = [
  {
    path: '/login',
    component: LoginView
  },
  {
    path: '/',
    component: DashView,
    children: [
      {
        path: 'dashboard',
        alias: '',
        component: DashboardView,
        name: 'Dashboard',
        meta: {description: ''}
      }, {
        path: 'tables',
        component: TablesView,
        name: 'Tables',
        meta: {description: 'Simple and advance table in CoPilot'}
      }, {
        path: 'tasks',
        component: TasksView,
        name: 'Tasks',
        meta: {description: 'Tasks page in the form of a timeline'}
      }, {
        path: 'setting',
        component: SettingView,
        name: 'Settings',
        meta: {description: 'User settings page'}
      }, {
        path: 'access',
        component: AccessView,
        name: 'Access',
        meta: {description: 'Example of using maps'}
      }, {
        path: 'server',
        component: ServerView,
        name: 'Servers',
        meta: {description: 'List of our servers', requiresAuth: true}
      }, {
        path: 'repos',
        component: ReposView,
        name: 'Repository',
        meta: {description: 'List of popular javascript repos'}
      }, {
        path: 'solicitud',
        component: SolicitudView,
        name: 'Solicitud',
        meta: {description: 'de nuevo usuario del Portal'}
      }, {
        path: 'individual',
        component: IndividualView,
        name: 'Individual',
        meta: {description: 'Consulta individual de Factura'}
      }, {
        path: 'logout',
        component: ReposView,
        name: 'Desconectarse',
        meta: {description: 'Desconectarse de la sesión del Portal'}
      }, {
        path: 'search',
        component: SearchView,
        name: 'Búsqueda',
        meta: {description: 'Consulta por filtro'}
      }, {
        path: 'billsManagement',
        component: BillsManagementView,
        name: 'Consulta de facturas',
        meta: {}
      }, {
        path: 'buyOrders',
        component: SearchView,
        name: 'OC (Orden de Compra)',
        meta: {}
      }, {
        path: 'toBill',
        component: SearchView,
        name: 'A facturar',
        meta: {}
      }, {
        path: 'paymentManagement',
        component: SearchView,
        name: 'Consulta de facturas',
        meta: {}
      }, {
        path: 'changePsw',
        component: ChangePswView,
        name: 'Cambiar clave',
        meta: {description: 'Cambio de contraseña'}
      }, {
        path: 'selfdata',
        component: SelfDataView,
        name: 'Mis datos',
        meta: {description: 'Visualice y actualice sus datos'}
      }, {
        path: 'comments',
        component: CommentsView,
        name: 'Actualizar datos',
        meta: {description: 'Envíe un pedido al administrador'}
      }, {
        path: 'commentDetail',
        component: CommentDetailView,
        name: 'Comentario',
        meta: {description: 'Detalles del comentario'}
      }, {
        path: 'allComments',
        component: AllCommentsView,
        name: 'Mensajes'
      }, {
        path: 'reestablecer',
        component: ReestablecerView,
        name: 'Reestablecer contraseña',
        meta: {description: ''}
      }, {
        path: 'pswreestablecida',
        component: PswReestablecidaView,
        name: 'Clave Reestablecida',
        meta: {description: ''}
      }, {
        path: 'invoicedetail',
        component: InvoiceDetailView,
        name: 'Notificación interna de compra',
        meta: {description: 'Detalles'}
      }, {
        path: 'reports',
        component: ReportsView,
        name: 'Reportes',
        meta: {description: ''}
      }, {
        path: 'finSolicitud',
        component: FinSolicitudView,
        name: 'FinSolicitud',
        meta: {description: 'Finalizar registro'}
      }, {
        path: 'contacts',
        component: AllContactsView,
        name: 'Contactos',
        meta: {description: ''}
      }, {
        path: 'providers',
        component: AllProvidersView,
        name: 'Proveedores',
        meta: {description: ''}
      }, {
        path: 'compulsas',
        component: LicitacionesView,
        name: 'Compulsas',
        meta: {description: ''}
      }, {
        path: 'manuals',
        component: ManualsView,
        name: 'Instructivos',
        meta: {description: ''}
      }, {
        path: 'news',
        component: NewsView,
        name: 'Noticias',
        meta: {description: ''}
      },
      {
        path: 'submitDocumentation',
        component: SubmitDocumentationView,
        name: 'Presentar Documentación',
        meta: {description: ''}
      },
      {
        path: 'contactDetail',
        component: ContactDetail,
        name: 'Detalle del Contacto',
        meta: {description: ''}
      },
      {
        path: 'userActivation',
        component: AltaDeUsuario,
        name: 'Proveedores',
        meta: {description: ''}
      },
      {
        path: 'newProviders',
        component: NewProviders,
        name: 'Alta de proveedor',
        meta: {description: ''}
      },
      {
        path: 'providerDetail/:id',
        component: ProviderDetail,
        name: 'Detalles del proveedor',
        meta: {description: ''}
      },
      {
        path: 'confirmProvider/:id',
        component: AltaDeUsuario,
        name: 'Confirmar proveedor',
        meta: {description: ''}
      },
      {
        path: 'companyData/:id/:step',
        component: AltaDeUsuario,
        name: 'Datos de la Compañia',
        meta: {description: ''}
      }
    ]
  }, {
    // not found handler
    path: '*',
    component: NotFoundView
  }
]

export default routes

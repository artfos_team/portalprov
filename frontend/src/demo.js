import moment from 'moment'

export const servers = [{
  name: 'www01',
  status: 'success',
  icon: 'globe',
  description: 'Web server that runs our sites'
}, {
  name: 'sql01',
  status: 'danger',
  icon: 'database',
  description: 'mySQL server used for reporting'
}, {
  name: 'mongoDB01',
  status: 'info',
  icon: 'file-code-o',
  description: 'Main DB server'
}, {
  name: 'ldap01',
  status: 'success',
  icon: 'key',
  description: 'Authentication server'
}, {
  name: 'mgmt01',
  status: 'success',
  icon: 'home',
  description: 'Management server with all tools'
}, {
  name: 'bkup01',
  status: 'warning',
  icon: 'backward',
  description: 'Backup server'
}]

export const stats = [{
  header: '8390',
  text: 'Visitors'
}, {
  header: '30%',
  text: 'Referrals'
}, {
  header: '70%',
  text: 'Organic'
}]

export const timeline = [{
  id: 1,
  icon: 'fa-book',
  color: 'blue',
  title: 'DOCUMENTACIÓN PRESENTADA',
  time: moment().endOf('day').fromNow(),
  body: 'Esta es una lista de la documentación presentada hasta el momento.',
  buttons: [{
    type: 'primary',
    message: 'Declaración jurada',
    href: 'https://github.com/misterGF/CoPilot',
    target: '_blank'
  }, {
    type: 'primary',
    message: 'Certificación de firmas',
    href: 'https://github.com/misterGF/CoPilot',
    target: '_blank'
  }]
}, {
  id: 10,
  icon: 'fa-folder-open-o',
  color: 'blue',
  title: 'DESCARGA DE DOCUMENTOS',
  time: moment().endOf('day').fromNow(),
  body: 'Descarga de documentos',
  buttons: [{
    type: 'primary',
    message: 'Instructivo de carga',
    href: 'https://github.com/misterGF/CoPilot',
    target: '_blank'
  }]
},
{
  id: 3,
  icon: 'fa-file-sound-o',
  color: 'purple',
  title: 'ALERTAS',
  time: moment('20130620', 'YYYYMMDD').fromNow(),
  body: '<div class="embed-responsive embed-responsive-16by9">ALERTAS </div>'
}]

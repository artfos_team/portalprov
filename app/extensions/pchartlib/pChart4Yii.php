<?php

require_once dirname(__FILE__).DIRECTORY_SEPARATOR.'pChart/pChart.class.php';

class pChart4Yii extends pChart
{

	/* This function write the values of the specified series */
	function barGraphWriteValues($Data,$DataDescription,$Series)
	{
		/* Validate the Data and DataDescription array */
		$this->validateDataDescription("writeValues",$DataDescription);
		$this->validateData("writeValues",$Data);

		$SerieID = 0;
		$SeriesWidth = $this->DivisionWidth / (count($DataDescription["Values"])+1);
		$SerieXOffset = $this->DivisionWidth / 2 - $SeriesWidth / 2;

		if ( !is_array($Series) ) { $Series = array($Series); }

		foreach($Series as $Key => $Serie)
		{
		$ID = 0;
		foreach ( $DataDescription["Description"] as $keyI => $ValueI )
		{ if ( $keyI == $Serie ) { $ColorID = $ID; }; $ID++; }

		$XPos = $this->GArea_X1 + $this->GAreaXOffset - $SerieXOffset + $SeriesWidth * $SerieID;
		$XLast = -1;
		foreach ( $Data as $Key => $Values )
		{
		if ( isset($Data[$Key][$Serie]) && is_numeric($Data[$Key][$Serie]))
		{
		$Value = $Data[$Key][$Serie];
		$YPos = $this->GArea_Y2 - (($Value-$this->VMin) * $this->DivisionRatio);

		$Positions = imagettfbbox($this->FontSize,0,$this->FontName,$Value);
		$Width = $Positions[2] - $Positions[6]; $XOffset = $XPos - ($Width/2)+ ($SeriesWidth/2);
		$Height = $Positions[3] - $Positions[7]; $YOffset = $YPos - 4;

		$C_TextColor =$this->AllocateColor($this->Picture,$this->Palette[$ColorID]["R"],$this->Palette[$ColorID]["G"],$this->Palette[$ColorID]["B"]);
		imagettftext($this->Picture,$this->FontSize,0,$XOffset,$YOffset,$C_TextColor,$this->FontName,$Value);
		}
		$XPos = $XPos + $this->DivisionWidth;
		}
		$SerieID++;
		}
	}

}
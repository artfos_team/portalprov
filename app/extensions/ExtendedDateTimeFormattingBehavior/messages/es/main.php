<?php
return array(
	'just now' => 'ahora',
	'Today, {time}' => 'Hoy, {time}',
	'Yesterday, {time}' => 'Ayer, {time}',

	'{n} second|{n} seconds' => '{n} segundo|{n} segundos',
	'{n} minute|{n} minutes' => '{n} minuto|{n} minutos',
	'{n} hour|{n} hours' => '{n} hora|{n} horas',
	'{n} day|{n} days' => '{n} día|{n} días',
	'{n} month|{n} months' => '{n} mes|{n} meses',
	'{n} year|{n} years' => '{n} año|{n} años',
	'{n} century|{n} centuries' => '{n} siglo|{n} siglos',

	'{first} and {second}' => '{first} y {second}',
	'{interval} ago' => 'hace {interval}',
	'In {interval}' => 'En {interval}',
);


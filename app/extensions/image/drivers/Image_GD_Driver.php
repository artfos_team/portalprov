<?php

/**
 * GD Image Driver.
 *
 * $Id: GD.php 3769 2008-12-15 00:48:56Z zombor $
 *
 * @package    Image
 * @author     Kohana Team
 * @copyright  (c) 2007-2008 Kohana Team
 * @license    http://kohanaphp.com/license.html
 */
class Image_GD_Driver extends Image_Driver {

	// A transparent PNG as a string
	protected static $blank_png;
	protected static $blank_png_width;
	protected static $blank_png_height;

	public function __construct()
	{
		// Make sure that GD2 is available
		if ( ! function_exists('gd_info'))
		throw new CException('image gd requires v2');

		// Get the GD information
		$info = gd_info();

		// Make sure that the GD2 is installed
		if (strpos($info['GD Version'], '2.') === FALSE)
		throw new CException('image gd requires v2');
	}

	public function process($image, $actions, $dir, $file, $render = FALSE)
	{
		Yii::import('ext.image.CArray');
		// Set the "create" function
		switch ($image['type'])
		{
			case IMAGETYPE_JPEG:
				$create = 'imagecreatefromjpeg';
				break;
			case IMAGETYPE_GIF:
				$create = 'imagecreatefromgif';
				break;
			case IMAGETYPE_PNG:
				$create = 'imagecreatefrompng';
				break;
			case IMAGETYPE_BMP:
				$create = 'ImageCreateFromBMP';
				break;
		}

		// Set the "save" function
		switch (strtolower(substr(strrchr($file, '.'), 1)))
		{
			case 'jpg':
			case 'bmp':
			case 'jpeg':
				$save = 'imagejpeg';
				break;
			case 'gif':
				$save = 'imagegif';
				break;
			case 'png':
				$save = 'imagepng';
				break;
		}

		// Make sure the image type is supported for import
		if (empty($create) OR ! function_exists($create))
		throw new CException('image type not allowed');

		// Make sure the image type is supported for saving
		if (empty($save) OR ! function_exists($save))
		throw new CException('image type not allowed');

		// Load the image
		$this->image = $image;

		// Create the GD image resource
		if($image['type'] == 'bmp') {
			$this->tmp_image = $this->ImageCreateFromBMP($image['file']);
		} else {
			$this->tmp_image = $create($image['file']);
		}

		// Get the quality setting from the actions
		$quality = CArray::remove('quality', $actions);

		if ($status = $this->execute($actions))
		{
			// Prevent the alpha from being lost
			imagealphablending($this->tmp_image, TRUE);
			imagesavealpha($this->tmp_image, TRUE);

			switch ($save)
			{
				case 'imagejpeg':
					// Default the quality to 95
					($quality === NULL) and $quality = 95;
					break;
				case 'imagegif':
					// Remove the quality setting, GIF doesn't use it
					unset($quality);
					break;
				case 'imagepng':
					// Always use a compression level of 9 for PNGs. This does not
					// affect quality, it only increases the level of compression!
					$quality = 9;
					break;
			}

			if ($render === FALSE)
			{
				// Set the status to the save return value, saving with the quality requested
				$status = isset($quality) ? $save($this->tmp_image, $dir.$file, $quality) : $save($this->tmp_image, $dir.$file);
			}
			else
			{
				// Output the image directly to the browser
				switch ($save)
				{
					case 'imagejpeg':
						header('Content-Type: image/jpeg');
						break;
					case 'imagegif':
						header('Content-Type: image/gif');
						break;
					case 'imagepng':
						header('Content-Type: image/png');
						break;
				}

				$status = isset($quality) ? $save($this->tmp_image, NULL, $quality) : $save($this->tmp_image);
			}

			// Destroy the temporary image
			imagedestroy($this->tmp_image);
		}

		return $status;
	}

	public function flip($direction)
	{
		// Get the current width and height
		$width = imagesx($this->tmp_image);
		$height = imagesy($this->tmp_image);

		// Create the flipped image
		$flipped = $this->imagecreatetransparent($width, $height);

		if ($direction === ImageExt::HORIZONTAL)
		{
			for ($x = 0; $x < $width; $x++)
			{
				$status = imagecopy($flipped, $this->tmp_image, $x, 0, $width - $x - 1, 0, 1, $height);
			}
		}
		elseif ($direction === ImageExt::VERTICAL)
		{
			for ($y = 0; $y < $height; $y++)
			{
				$status = imagecopy($flipped, $this->tmp_image, 0, $y, 0, $height - $y - 1, $width, 1);
			}
		}
		else
		{
			// Do nothing
			return TRUE;
		}

		if ($status === TRUE)
		{
			// Swap the new image for the old one
			imagedestroy($this->tmp_image);
			$this->tmp_image = $flipped;
		}

		return $status;
	}

	public function crop($properties)
	{
		// Sanitize the cropping settings
		$this->sanitize_geometry($properties);

		// Get the current width and height
		$width = imagesx($this->tmp_image);
		$height = imagesy($this->tmp_image);

		// Create the temporary image to copy to
		$img = $this->imagecreatetransparent($properties['width'], $properties['height']);

		// Execute the crop
		if ($status = imagecopyresampled($img, $this->tmp_image, 0, 0, $properties['left'], $properties['top'], $width, $height, $width, $height))
		{
			// Swap the new image for the old one
			imagedestroy($this->tmp_image);
			$this->tmp_image = $img;
		}

		return $status;
	}

	public function resize($properties)
	{
		// Get the current width and height
		$width = imagesx($this->tmp_image);
		$height = imagesy($this->tmp_image);

		if (substr($properties['width'], -1) === '%')
		{
			// Recalculate the percentage to a pixel size
			$properties['width'] = round($width * (substr($properties['width'], 0, -1) / 100));
		}

		if (substr($properties['height'], -1) === '%')
		{
			// Recalculate the percentage to a pixel size
			$properties['height'] = round($height * (substr($properties['height'], 0, -1) / 100));
		}

		// Recalculate the width and height, if they are missing
		empty($properties['width'])  and $properties['width']  = round($width * $properties['height'] / $height);
		empty($properties['height']) and $properties['height'] = round($height * $properties['width'] / $width);

		if ($properties['master'] === ImageExt::AUTO)
		{
			// Change an automatic master dim to the correct type
			$properties['master'] = (($width / $properties['width']) > ($height / $properties['height'])) ? ImageExt::WIDTH : ImageExt::HEIGHT;
		}

		if (empty($properties['height']) OR $properties['master'] === ImageExt::WIDTH)
		{
			// Recalculate the height based on the width
			$properties['height'] = round($height * $properties['width'] / $width);
		}

		if (empty($properties['width']) OR $properties['master'] === ImageExt::HEIGHT)
		{
			// Recalculate the width based on the height
			$properties['width'] = round($width * $properties['height'] / $height);
		}

		// Test if we can do a resize without resampling to speed up the final resize
		if ($properties['width'] > $width / 2 AND $properties['height'] > $height / 2)
		{
			// Presize width and height
			$pre_width = $width;
			$pre_height = $height;

			// The maximum reduction is 10% greater than the final size
			$max_reduction_width  = round($properties['width']  * 1.1);
			$max_reduction_height = round($properties['height'] * 1.1);

			// Reduce the size using an O(2n) algorithm, until it reaches the maximum reduction
			while ($pre_width / 2 > $max_reduction_width AND $pre_height / 2 > $max_reduction_height)
			{
				$pre_width /= 2;
				$pre_height /= 2;
			}

			// Create the temporary image to copy to
			$img = $this->imagecreatetransparent($pre_width, $pre_height);

			if ($status = imagecopyresized($img, $this->tmp_image, 0, 0, 0, 0, $pre_width, $pre_height, $width, $height))
			{
				// Swap the new image for the old one
				imagedestroy($this->tmp_image);
				$this->tmp_image = $img;
			}

			// Set the width and height to the presize
			$width  = $pre_width;
			$height = $pre_height;
		}

		// Create the temporary image to copy to
		$img = $this->imagecreatetransparent($properties['width'], $properties['height']);

		// Execute the resize
		if ($status = imagecopyresampled($img, $this->tmp_image, 0, 0, 0, 0, $properties['width'], $properties['height'], $width, $height))
		{
			// Swap the new image for the old one
			imagedestroy($this->tmp_image);
			$this->tmp_image = $img;
		}

		return $status;
	}
	
	public function shrink($properties) {
		$width = imagesx($this->tmp_image);
		$height = imagesy($this->tmp_image);
		if($width > $properties['width'] || $height > $properties['height']) {
			$properties['master'] = 2; 
			return $this->resize($properties);
		}
		return $this->tmp_image;
	}

	public function rotate($amount)
	{
		// Use current image to rotate
		$img = $this->tmp_image;

		// White, with an alpha of 0
		$transparent = imagecolorallocatealpha($img, 255, 255, 255, 127);

		// Rotate, setting the transparent color
		$img = imagerotate($img, 360 - $amount, $transparent, -1);

		// Fill the background with the transparent "color"
		imagecolortransparent($img, $transparent);

		// Merge the images
		if ($status = imagecopymerge($this->tmp_image, $img, 0, 0, 0, 0, imagesx($this->tmp_image), imagesy($this->tmp_image), 100))
		{
			// Prevent the alpha from being lost
			imagealphablending($img, TRUE);
			imagesavealpha($img, TRUE);

			// Swap the new image for the old one
			imagedestroy($this->tmp_image);
			$this->tmp_image = $img;
		}

		return $status;
	}

	public function sharpen($amount)
	{
		// Make sure that the sharpening function is available
		if ( ! function_exists('imageconvolution'))
		throw new CException('image unsupported method');

		// Amount should be in the range of 18-10
		$amount = round(abs(-18 + ($amount * 0.08)), 2);

		// Gaussian blur matrix
		$matrix = array
		(
		array(-1,   -1,    -1),
		array(-1, $amount, -1),
		array(-1,   -1,    -1),
		);

		// Perform the sharpen
		return imageconvolution($this->tmp_image, $matrix, $amount - 8, 0);
	}

	public function watermark($properties) {
		$img = $this->tmp_image;
		switch ($properties['image']->type) {
			case IMAGETYPE_JPEG:
				$create = 'imagecreatefromjpeg';
				break;
			case IMAGETYPE_GIF:
				$create = 'imagecreatefromgif';
				break;
			case IMAGETYPE_PNG:
				$create = 'imagecreatefrompng';
				break;
		}
		$watermark = $create($properties['image']->file);
		imageAlphaBlending($img, true);

		$w = imagesx($img);
		$h = imagesy($img);
		$nw = $w/2 - $properties['image']->width/2;
		if($nw < 0) {
			$nw = 0;
			$ratio = $w / $properties['image']->width;
		 	$height = $properties['image']->height * $ratio;
		 	$properties['image']->height = $height;
		 	$watermark = $this->smart_resize_image($properties['image']->file,$w,$height,true,'file',false);
		}
		$nh = $h/2 - $properties['image']->height/2;
		// Make the background transparent
		imagecopy($img, $watermark, $nw, $nh, 0, 0, $properties['image']->width, $properties['image']->height);
		$this->tmp_image = $img;
		return true;
	}

	public function filledZoom($properties) {
		$img = $this->tmp_image;
		
		$w = imagesx($this->tmp_image);
		$h = imagesy($this->tmp_image);
		$borderSize = $properties['borderSize'];
		/** VE SI TIENE QUE CAMBIR EL TAMAÑO */
		//si hay borde el tam;ano a achicar es 4 pixles mas chico
		$sW = (!is_null($properties['border'])?($properties['width']-($borderSize*2)):($properties['width']));
		$sH = (!is_null($properties['border'])?($properties['height']-($borderSize*2)):($properties['height']));
		//si la imagen es mas grande, la achica
		if($w>$sW || $h > $sH) {
			$this->resize(array('width'=>$sW,'height'=>$sH,'master'=>2));
		}
		$w = imagesx($this->tmp_image);
		$h = imagesy($this->tmp_image);
		
		//cra una imagen de tamaño total
		$newImg = imagecreatetruecolor( $properties['width'], $properties['height']);
		//calcula los colores del backgron y capaz del borde del borde
		$blue = $properties['background']%256;
		$properties['background'] = floor($properties['background']/256);
		$green = $properties['background']%256;
		$properties['background'] = floor($properties['background']/256);
		$red = $properties['background']%256;
		$background = imagecolorallocate( $newImg, $red, $green, $blue);
		if(!is_null($properties['border'])) {
			$blue = $properties['border']%256;
			$properties['border'] = floor($properties['border']/256);
			$green = $properties['border']%256;
			$properties['border'] = floor($properties['border']/256);
			$red = $properties['border']%256;
			$border = imagecolorallocate( $newImg, $red, $green, $blue);
		}
		
		$dst_x = ($properties['width']-$w)/2;
		$dst_y = ($properties['height']-$h)/2;
		$src_x = 0;
		$src_y = 0;
		$src_w = $properties['width'];
		$src_h = $properties['height'];
		imagecopymerge($newImg, $this->tmp_image, $dst_x,$dst_y, $src_x, $src_y, $src_w, $src_h, 100);
		
		
		if($h<$src_h) {
			if(is_null($properties['border'])) {
				imagefilledrectangle($newImg,0,0,$src_w,$dst_y,$background);
				imagefilledrectangle($newImg,0,$src_h-$dst_y,$src_w,$src_h,$background);
			} else {
				imagefilledrectangle($newImg,$borderSize,$borderSize,$src_w-$borderSize,$dst_y,$background);
				imagefilledrectangle($newImg,$borderSize,$src_h-$dst_y,$src_w-$borderSize,$src_h-$borderSize,$background);
				imagefilledrectangle($newImg,0,0,$src_w,$borderSize,$border);
				imagefilledrectangle($newImg,0,$src_h-$borderSize,$src_w,$src_h,$border);
			}
		}
		if($w<$src_w) {
			if(is_null($properties['border'])) {
				imagefilledrectangle($newImg,0,0,$dst_x,$src_w,$background);
				imagefilledrectangle($newImg,$src_w-$dst_x,0,$src_w,$src_h,$background);
			} else {
				imagefilledrectangle($newImg,$borderSize,$borderSize,$dst_x,$src_h-$borderSize,$background);
				imagefilledrectangle($newImg,$src_w-$dst_x,$borderSize,$src_w-$borderSize,$src_h-$borderSize,$background);
				imagefilledrectangle($newImg,0,0,$borderSize,$src_h,$border);
				imagefilledrectangle($newImg,$src_w-$borderSize,0,$src_w,$src_h,$border);
			}
		}
		$this->tmp_image = $newImg;
		return $newImg;
	}
	
	public function filledShrink($properties) {
		$width = imagesx($this->tmp_image);
		$height = imagesy($this->tmp_image);
		if($width > $properties['width'] || $height > $properties['height']) {
			return $this->filledZoom($properties);
		}
		return $this->tmp_image;
	}
	
	public function zoomAndCrop($properties) {
		$actualW = imagesx($this->tmp_image);
		$actualH = imagesy($this->tmp_image);
		$askWidth = $properties['width'];
		$askHeight = $properties['height'];
		//primero ve si hay que agrandar la imagen
		if($actualW < $askWidth) {
			$this->resize(array('width'=>$askWidth, 'height'=>1, 'master'=>ImageExt::WIDTH));
		}
		$actualW = imagesx($this->tmp_image);
		$actualH = imagesy($this->tmp_image);
		if($actualH < $askHeight) {
			$this->resize(array('width'=>1, 'height'=>$askHeight, 'master'=>ImageExt::HEIGHT));
		}
		$actualW = imagesx($this->tmp_image);
		$actualH = imagesy($this->tmp_image);
		//ahora veo cual de los dos el el de menro achique
		//cada ratio debe ser <= 1 por lo hehco arriba		
		if($actualW/$askWidth < $actualH/$askHeight) {
			$this->resize(array('width'=>$askWidth,'height'=>$askHeight,'master'=>ImageExt::WIDTH));
		} else {
			$this->resize(array('width'=>$askWidth,'height'=>$askHeight,'master'=>ImageExt::HEIGHT));
		}
		$this->crop(array('width'=>$askWidth,'height'=>$askHeight,'left'=>'center','top'=>'center'));
		if(isset($properties['border']) && !is_null($properties['border'])) {
			$this->filledZoom(array('width'=>$askWidth,'height'=>$askHeight,'border'=>$properties['border'],'borderSize'=>$properties['borderSize']));
		}
		return true;
	}

	function smart_resize_image( $file, $width = 0, $height = 0, $proportional = false, $output = 'file', $delete_original = true, $use_linux_commands = false )
	{
		if ( $height <= 0 && $width <= 0 ) {
			return false;
		}
		$info = getimagesize($file);
		$image = '';
		$final_width = 0;
		$final_height = 0;
		list($width_old, $height_old) = $info;
		if ($proportional) {
			if ($width == 0) $factor = $height/$height_old;
			elseif ($height == 0) $factor = $width/$width_old;
			else $factor = min ( $width / $width_old, $height / $height_old);

			$final_width = round ($width_old * $factor);
			$final_height = round ($height_old * $factor);

		}
		else {
			$final_width = ( $width <= 0 ) ? $width_old : $width;
			$final_height = ( $height <= 0 ) ? $height_old : $height;
		}
		switch ( $info[2] ) {
			case IMAGETYPE_GIF:
				$image = imagecreatefromgif($file);
				break;
			case IMAGETYPE_JPEG:
				$image = imagecreatefromjpeg($file);
				break;
			case IMAGETYPE_PNG:
				$image = imagecreatefrompng($file);
				break;
			default:
				return false;
		}
		$image_resized = imagecreatetruecolor( $final_width, $final_height );
		if ( ($info[2] == IMAGETYPE_GIF) || ($info[2] == IMAGETYPE_PNG) ) {
			$trnprt_indx = imagecolortransparent($image);
			// If we have a specific transparent color
			if ($trnprt_indx >= 0) {
				// Get the original image's transparent color's RGB values
				$trnprt_color    = imagecolorsforindex($image, $trnprt_indx);
				// Allocate the same color in the new image resource
				$trnprt_indx    = imagecolorallocatealpha($image_resized, $trnprt_color['red'], $trnprt_color['green'], $trnprt_color['blue'],127);
				// Completely fill the background of the new image with allocated color.
				imagefill($image_resized, 0, 0, $trnprt_indx);
				// Set the background color for new image to transparent
				imagecolortransparent($image_resized, $trnprt_indx);
			}
			// Always make a transparent background color for PNGs that don't have one allocated already
			elseif ($info[2] == IMAGETYPE_PNG) {
				// Turn off transparency blending (temporarily)
				imagealphablending($image_resized, false);
				// Create a new transparent color for image
				$color = imagecolorallocatealpha($image_resized, 0, 0, 0, 127);
				// Completely fill the background of the new image with allocated color.
				imagefill($image_resized, 0, 0, $color);
				// Restore transparency blending
				imagesavealpha($image_resized, true);
			}
		}
		imagecopyresampled($image_resized, $image, 0, 0, 0, 0, $final_width, $final_height, $width_old, $height_old);
		if ( $delete_original ) {
			if ( $use_linux_commands )
			exec('rm '.$file);
			else
			@unlink($file);
		}
		switch ( strtolower($output) ) {
			case 'browser':
				$mime = image_type_to_mime_type($info[2]);
				header("Content-type: $mime");
				$output = NULL;
				break;
			case 'file':
				$output = $file;
				break;
			case 'return':
				return $image_resized;
				break;
			default:
				break;
		}
		switch ( $info[2] ) {
			case IMAGETYPE_GIF:
			case IMAGETYPE_JPEG:
			case IMAGETYPE_PNG:
				return $image_resized;
				break;
			default:
				return false;
		}
	}

	protected function properties()
	{
		return array(imagesx($this->tmp_image), imagesy($this->tmp_image));
	}

	/**
	 * Returns an image with a transparent background. Used for rotating to
	 * prevent unfilled backgrounds.
	 *
	 * @param   integer  image width
	 * @param   integer  image height
	 * @return  resource
	 */
	protected function imagecreatetransparent($width, $height)
	{
		if (self::$blank_png === NULL)
		{
			// Decode the blank PNG if it has not been done already
			self::$blank_png = imagecreatefromstring(base64_decode
			(
				'iVBORw0KGgoAAAANSUhEUgAAACgAAAAoCAYAAACM/rhtAAAABGdBTUEAAK/INwWK6QAAABl0RVh0U29'.
				'mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAADqSURBVHjaYvz//z/DYAYAAcTEMMgBQAANegcCBN'.
				'CgdyBAAA16BwIE0KB3IEAADXoHAgTQoHcgQAANegcCBNCgdyBAAA16BwIE0KB3IEAADXoHAgTQoHcgQ'.
				'AANegcCBNCgdyBAAA16BwIE0KB3IEAADXoHAgTQoHcgQAANegcCBNCgdyBAAA16BwIE0KB3IEAADXoH'.
				'AgTQoHcgQAANegcCBNCgdyBAAA16BwIE0KB3IEAADXoHAgTQoHcgQAANegcCBNCgdyBAAA16BwIE0KB'.
				'3IEAADXoHAgTQoHcgQAANegcCBNCgdyBAgAEAMpcDTTQWJVEAAAAASUVORK5CYII='
				));

				// Set the blank PNG width and height
				self::$blank_png_width = imagesx(self::$blank_png);
				self::$blank_png_height = imagesy(self::$blank_png);
		}

		$img = imagecreatetruecolor($width, $height);

		// Resize the blank image
		imagecopyresized($img, self::$blank_png, 0, 0, 0, 0, $width, $height, self::$blank_png_width, self::$blank_png_height);

		// Prevent the alpha from being lost
		imagealphablending($img, FALSE);
		imagesavealpha($img, TRUE);

		return $img;
	}
} // End Image GD Driver

	function ImageCreateFromBMP($filename)
	{
	 //Ouverture du fichier en mode binaire
	   if (! $f1 = fopen($filename,"rb")) return FALSE;
	
	 //1 : Chargement des ent�tes FICHIER
	   $FILE = unpack("vfile_type/Vfile_size/Vreserved/Vbitmap_offset", fread($f1,14));
	   if ($FILE['file_type'] != 19778) return FALSE;
	
	 //2 : Chargement des ent�tes BMP
	   $BMP = unpack('Vheader_size/Vwidth/Vheight/vplanes/vbits_per_pixel'.
	                 '/Vcompression/Vsize_bitmap/Vhoriz_resolution'.
	                 '/Vvert_resolution/Vcolors_used/Vcolors_important', fread($f1,40));
	   $BMP['colors'] = pow(2,$BMP['bits_per_pixel']);
	   if ($BMP['size_bitmap'] == 0) $BMP['size_bitmap'] = $FILE['file_size'] - $FILE['bitmap_offset'];
	   $BMP['bytes_per_pixel'] = $BMP['bits_per_pixel']/8;
	   $BMP['bytes_per_pixel2'] = ceil($BMP['bytes_per_pixel']);
	   $BMP['decal'] = ($BMP['width']*$BMP['bytes_per_pixel']/4);
	   $BMP['decal'] -= floor($BMP['width']*$BMP['bytes_per_pixel']/4);
	   $BMP['decal'] = 4-(4*$BMP['decal']);
	   if ($BMP['decal'] == 4) $BMP['decal'] = 0;
	
	 //3 : Chargement des couleurs de la palette
	   $PALETTE = array();
	   if ($BMP['colors'] < 16777216)
	   {
	    $PALETTE = unpack('V'.$BMP['colors'], fread($f1,$BMP['colors']*4));
	   }
	
	 //4 : Cr�ation de l'image
	   $IMG = fread($f1,$BMP['size_bitmap']);
	   $VIDE = chr(0);
	
	   $res = imagecreatetruecolor($BMP['width'],$BMP['height']);
	   $P = 0;
	   $Y = $BMP['height']-1;
	   while ($Y >= 0)
	   {
	    $X=0;
	    while ($X < $BMP['width'])
	    {
	     if ($BMP['bits_per_pixel'] == 24)
	        $COLOR = unpack("V",substr($IMG,$P,3).$VIDE);
	     elseif ($BMP['bits_per_pixel'] == 16)
	     { 
	        $COLOR = unpack("n",substr($IMG,$P,2));
	        $COLOR[1] = $PALETTE[$COLOR[1]+1];
	     }
	     elseif ($BMP['bits_per_pixel'] == 8)
	     { 
	        $COLOR = unpack("n",$VIDE.substr($IMG,$P,1));
	        $COLOR[1] = $PALETTE[$COLOR[1]+1];
	     }
	     elseif ($BMP['bits_per_pixel'] == 4)
	     {
	        $COLOR = unpack("n",$VIDE.substr($IMG,floor($P),1));
	        if (($P*2)%2 == 0) $COLOR[1] = ($COLOR[1] >> 4) ; else $COLOR[1] = ($COLOR[1] & 0x0F);
	        $COLOR[1] = $PALETTE[$COLOR[1]+1];
	     }
	     elseif ($BMP['bits_per_pixel'] == 1)
	     {
	        $COLOR = unpack("n",$VIDE.substr($IMG,floor($P),1));
	        if     (($P*8)%8 == 0) $COLOR[1] =  $COLOR[1]        >>7;
	        elseif (($P*8)%8 == 1) $COLOR[1] = ($COLOR[1] & 0x40)>>6;
	        elseif (($P*8)%8 == 2) $COLOR[1] = ($COLOR[1] & 0x20)>>5;
	        elseif (($P*8)%8 == 3) $COLOR[1] = ($COLOR[1] & 0x10)>>4;
	        elseif (($P*8)%8 == 4) $COLOR[1] = ($COLOR[1] & 0x8)>>3;
	        elseif (($P*8)%8 == 5) $COLOR[1] = ($COLOR[1] & 0x4)>>2;
	        elseif (($P*8)%8 == 6) $COLOR[1] = ($COLOR[1] & 0x2)>>1;
	        elseif (($P*8)%8 == 7) $COLOR[1] = ($COLOR[1] & 0x1);
	        $COLOR[1] = $PALETTE[$COLOR[1]+1];
	     }
	     else
	        return FALSE;
	     imagesetpixel($res,$X,$Y,$COLOR[1]);
	     $X++;
	     $P += $BMP['bytes_per_pixel'];
	    }
	    $Y--;
	    $P+=$BMP['decal'];
	   }
	
	 //Fermeture du fichier
	   fclose($f1);
	
	 return $res;
	}
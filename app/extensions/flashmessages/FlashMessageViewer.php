<?php

class FlashMessageViewer extends CWidget {

	/**
	 * @var string The name of the component at the application, defaults to stateManager
	 */
	public $flashMessageManagerComponent = 'flashMessage';
	
	/**
	 * @var CWebUser the CWebUser that have the flash messages
	 */
	public $user = null;
	
	/**
	 * @var string The container HTML tag
	 */
	public $containerTag = 'div';
	
	public $containerHtmlOptions = array();
	
	/**
	 * Shows all the messages of the current implementation
	 */
	public function run(){
		$flashMessageManagerComponent = $this->flashMessageManagerComponent;
		$flashMessageManager = Yii::app()->$flashMessageManagerComponent;
		if (!$flashMessageManager instanceof FlashMessageManager){
			throw new CException("The current flashMessageManager is not defined");
		}
		
		if (!isset($this->user)){
			$user = Yii::app()->user;
		}
		
		if (!$user instanceof CWebUser){
			throw new CException ("You must give a valid CWebUser instance to \"user\" property");
		}
		
		$messages = array();
		
		foreach($flashMessageManager->flashes as $flash){
			$aux = $flash->getMessage($user);
			if ($aux !==null){
				$messages[] = $aux;
			}
		}
		
		if (count($messages)>0){
			echo CHtml::openTag('div', array('class'=>'grid_3'));
			echo CHtml::openTag($this->containerTag, $this->containerHtmlOptions);
			foreach($messages as $message){
				echo $message;
			}
			echo CHtml::closeTag($this->containerTag);
			echo CHtml::closeTag('div');
		}
	}
}
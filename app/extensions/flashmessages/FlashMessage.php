<?php

/**
 * @author Sebastián Thierer <sebathi@gmail.com>
 * @version 1.0
 * @license MIT
 */
Yii::import('common.extensions.FlashMessageManager');
/**
 * This component is used by the FlashMessageManager to define how the flash messages
 * should be shown for each flash type
 */
class FlashMessage extends CComponent {
	
	public $name;

	public $header = '';
	
	public $footer = '';
	
	public $htmlTag = 'div';
	
	public $htmlOptions = array();
	
	/**
	 * Verify if the user has a flash message for this component
	 * @param CWebUser $user
	 * @return boolean
	 */
	public function hasMessage($user){
		return  isset($this->name) && 
				$user instanceof CWebUser && 
				$user->hasFlash($this->name);
	}
	
	/**
	 * Gets the parsed HTML message
	 * @param CWebUser $user
	 * @return string
	 */
	public function getMessage($user, $htmlOptions=array()){
		if ($this->hasMessage($user)){
			return  CHtml::tag($this->htmlTag, 
								array_merge($this->htmlOptions, $htmlOptions), 
								$this->header . $user->getFlash($this->name) . $this->footer);
		}
		return null;
	}
	
	/**
	 * 
	 * @param string $user
	 * @param string $message
	 */
	public function setMessage($user, $message){
		$user->setFlash($this->name, $message);
	}
	/**
	 * Gets the standard FlashMessage component for each predefined component
	 * @param string $type
	 * returns FlashManager
	 */
	public static function getStandardFlashMessage($type){
		if (is_string($type)){
			switch($type) {
				case 'error':
					return Yii::createComponent(
						array(
							'class'=>'application.extensions.flashmessages.FlashMessage',
							'name'=>'error',
							'htmlOptions'=>array(
								'class'=>'flash-error'
							)
						)
					);
				case 'notice':
					return Yii::createComponent(
						array(
							'class'=>'application.extensions.flashmessages.FlashMessage',
							'name'=>'notice',
							'htmlOptions'=>array(
								'class'=>'flash-notice'
							)
						)
					);
				case 'success':
					return Yii::createComponent(
						array(
							'class'=>'application.extensions.flashmessages.FlashMessage',
							'name'=>'success',
							'htmlOptions'=>array(
								'class'=>'flash-success'
							)
						)
					);
			}
		}
		throw new CException('The standard FlashMessage "' . $type . '" does not exist. Try with "success", "notice" or "error"');
	}
}
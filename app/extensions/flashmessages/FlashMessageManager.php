<?php

Yii::import('application.extensions.flashmessages.*');

class FlashMessageManager extends CApplicationComponent {
	/**
	 * @var array All the available message types. The default errors are: 'error', 'information' and 'success'
	 */
	public $flashes = array('success', 'notice', 'error');

	public function init(){
		parent::init();
		$flashes = array();
		foreach ($this->flashes as $key=>$value){
			if (is_array($value)){
				
			}elseif(is_string($value)) {
				$flashes[$value] = $flash = FlashMessage::getStandardFlashMessage($value);
			}elseif (is_array($value)){
				if (!isset($value['name'])){
					$value['name'] = $key;
				}
				$flash = Yii::createComponent($value);
				if (!$flash instanceof FlashMessage){
					throw new CException('The flash message "' . $key . '" is not an instance of FlashMessage');
				}
				$flashes[$key]=$flash;
			}else{
				throw new CException ('The flash component received at flashes['.$key.'] is invalid');
			}
		}
		$this->flashes = $flashes;
	}
	
	/**
	 * Add a new flash message for the selected type and user 
	 * @param string $type
	 * @param string $message
	 * @param CWebUser $user
	 */
	public function addMessage($type, $message, $user=null){
		if ($user===null){
			$user = Yii::app()->user;
		}
		if (isset($this->flashes[$type])){
			$this->flashes[$type]->setMessage($user, $message);
		}else{
			throw new CHttpException ("The type \"$type\" does not exist");
		}
	}
	
}
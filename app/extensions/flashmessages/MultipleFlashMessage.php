<?php
class MultipleFlashMessage extends FlashMessage {
	
	/**
	 * Verify if the user has a flash message for this component
	 * @param CWebUser $user
	 * @return boolean
	 */
	public function hasMessage($user){
		return  isset($this->name) && 
				$user instanceof CWebUser && 
				$user->hasFlash($this->name);
	}
	
	/**
	 * Gets the parsed HTML message
	 * @param CWebUser $user
	 * @return string
	 */
	public function getMessage($user, $htmlOptions=array()){
		if ($this->hasMessage($user)){
			$flash = unserialize($user->getFlash($this->name));
			if (is_array($flash)){
				$ret = '';
				foreach ($flash as $flash_keys=>$times){
					// If this not exist, it was erased and it hasn't to be used...
					if ($user->getFlash($flash_keys) && $times >=0){
						$ret.=CHtml::tag($this->htmlTag, 
								array_merge($this->htmlOptions, $htmlOptions), 
								$this->header . $user->getFlash($flash_keys) . $this->footer);
					}
				}
				return $ret;
			}
		}
		return null; // Or throws an exception
	}
	
	private $_generated_flash_array; 
	/**
	 * 
	 * @param string $user
	 * @param string $message
	 */
	public function setMessage($user, $message){
		$newFlash = array();
		if (!isset($this->_generated_flash_array)){
			$this->_generated_flash_array = array();
			if ($this->hasMessage($user)){
				$flash = unserialize($user->getFlash($this->name));
				foreach($flash as $flash_keys=>$times){
					if ($times>0){
						$this->_generated_flash_array[$flash_keys] = $times-1;
					}
				}
			}
		}
		$key = $this->name.'_'.md5($message);
		$user->setFlash($key, $message);
		$this->_generated_flash_array[$key] = 1; // This request and the next one
		$user->set($this->name, serialize($this->_generated_flash_array));
	}
}
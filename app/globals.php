<?php

/**
 * This is the shortcut to DIRECTORY_SEPARATOR
 */
defined('DS') or define('DS',DIRECTORY_SEPARATOR);
 
/**
 * This is the shortcut to Yii::app()
 * @return CWebApplication
 */
function app()
{
    return Yii::app();
}
 
/**
 * This is the shortcut to Yii::app()->clientScript
 * @return CClientScript
 */
function cs()
{
    // You could also call the client script instance via Yii::app()->clientScript
    // But this is faster
    return Yii::app()->getClientScript();
}
 
/**
 * This is the shortcut to Yii::app()->request
 * @return CClientScript
 */
function request()
{
    return Yii::app()->getRequest();
}
 
/**
 * This is the shortcut to Yii::app()->user.
 * @return CWebUser
 */
function user() 
{
    return Yii::app()->getUser();
}
 
/**
 * This is the shortcut to Yii::app()->createUrl()
 */
function url($route,$params=array(),$ampersand='&')
{
    return Yii::app()->createUrl($route,$params,$ampersand);
}
 
/**
 * This is the shortcut to CHtml::encode
 */
function h($text)
{
    return htmlspecialchars($text,ENT_QUOTES,Yii::app()->charset);
}
 
/**
 * This is the shortcut to CHtml::link()
 */
function l($text, $url = '#', $htmlOptions = array()) 
{
    return CHtml::link($text, $url, $htmlOptions);
}
 
/**
 * This is the shortcut to Yii::app()->request->baseUrl
 * If the parameter is given, it will be returned and prefixed with the app baseUrl.
 */
function bu($url=null) 
{
    static $baseUrl;
    if ($baseUrl===null)
        $baseUrl=Yii::app()->getRequest()->getBaseUrl();
    return $url===null ? $baseUrl : $baseUrl.'/'.ltrim($url,'/');
}
 
/**
 * Returns the named application parameter.
 * This is the shortcut to Yii::app()->params[$name].
 */
function param($name) 
{
    return Yii::app()->params[$name];
}

/**
 * Returns the language code assoseated
 * to the $idlanguage  
 */
function getLanguageCode($idlanguage)
{
    $codes = array(1=>'en', 2=>'es');
    return isset($codes[$idlanguage])?$code[$idlanguage]:null;
}

/**
 * Returns the language id assoseated
 * to the $language_code
 */
function getLanguageId($language_code)
{
    $codes = array('en'=>1, 'es'=>1);
    return isset($codes[$language_code])?$code[$language_code]:null;
}

/**
 * Sets a flash message $message of $type type
 * @param string $message
 * @param string $type 
 */
function flashMessage($message, $type='success')
{
	app()->flashMessage->addMessage($type, $message);
}

function convertSqlDateToShowable($date)
{
    list($year, $month, $day) = explode('-', $date);
    return "$month/$day/$year";
}

function addEmpty($list, $emptyText = "None")
{
    $arr = array(''=>$emptyText);
    foreach($list as $key=>$element)
    {
        $arr[$key] = $element;
    }
    return $arr;

}


function yearsArray($from, $to)
{
    $ret = array();
    foreach (range($from, $to) as $year){
        $ret[$year] = $year;
    }
    return $ret;
}

function monthsArray($monthFrom, $monthTo)
{
    $ret = array();
    for ($i=(int)$monthFrom; $i<=$monthTo; $i++)
    {
        $ret[$i] = app()->dateFormatter->format('MMM', mktime(0,0,0, $i, 10, 2000));
    }
    return $ret;
}



/**
 * Escapes the given string using CHtml::encode().
 * @param $text
 * @return string
 */
function e($text)
{
    return CHtml::encode($text);
}
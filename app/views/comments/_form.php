<?php
/* @var $this CommentsController */
/* @var $model Comments */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'comments-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'description'); ?>
		<?php echo $form->textField($model,'description',array('size'=>60,'maxlength'=>500)); ?>
		<?php echo $form->error($model,'description'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'idCommentType'); ?>
		<?php echo $form->textField($model,'idCommentType'); ?>
		<?php echo $form->error($model,'idCommentType'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'dateComments'); ?>
		<?php echo $form->textField($model,'dateComments'); ?>
		<?php echo $form->error($model,'dateComments'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'statusComments'); ?>
		<?php echo $form->textField($model,'statusComments'); ?>
		<?php echo $form->error($model,'statusComments'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'idRefenceTo'); ?>
		<?php echo $form->textField($model,'idRefenceTo'); ?>
		<?php echo $form->error($model,'idRefenceTo'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->
<?php
/* @var $this CommentsController */
/* @var $data Comments */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('idComments')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->idComments), array('view', 'id'=>$data->idComments)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('description')); ?>:</b>
	<?php echo CHtml::encode($data->description); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('idCommentType')); ?>:</b>
	<?php echo CHtml::encode($data->idCommentType); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('dateComments')); ?>:</b>
	<?php echo CHtml::encode($data->dateComments); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('statusComments')); ?>:</b>
	<?php echo CHtml::encode($data->statusComments); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('idRefenceTo')); ?>:</b>
	<?php echo CHtml::encode($data->idRefenceTo); ?>
	<br />


</div>
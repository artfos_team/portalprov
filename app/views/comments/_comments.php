<?php 
	if ($model->idCommentType == 1)
	{
		$this->widget('bootstrap.widgets.TbButtonGroup',array(
			'size'=>'small',
			'htmlOptions'=>array('class'=>'pull-right'),
			'buttons'=>array(
				array('label'=>'Responder', 'type'=>'inverse', 
					'htmlOptions'=>array('
						data-toggle'=>'modal', 
						'data-target'=>'#addCommentsModel', 
						'id'=>'addComments'
					)
				),
			),
		));
	}
?>
<div>
	<?php
		if ($model->idCommentType == 1)
		{
			$attributes = array(
				array(
					'name'=>'idUser',
				),
				array(
					'name'=>'dateComments',
				),
				array(
					'name'=>'description',
				),
				array(
					'name'=>'idCommentType',
				),
				array(
					'name'=>'statusComments',
				),
				array(
					'name'=>'idRefenceTo',
				),
			);
		}
		else
		{
			$attributes = array(
				array(
					'name'=>'idUser',
				),
				array(
					'name'=>'dateComments',
				),
				array(
					'name'=>'description',
				),
				array(
					'name'=>'idCommentType',
				),
				array(
					'name'=>'statusComments',
				),
				array(
					'name'=>'idRefenceTo',
				),
			);
		}

		$this->widget('bootstrap.widgets.TbDetailView', array(
			'id' => 'comments-grid',
			'data' => $model,
			'attributes' => array(
				array(
					'name'=>'idUser',
					'value'=>$model->getUserName(),
				),
				array(
					'name'=>'dateComments',
				),
				array(
					'name'=>'description',
				),
				array(
					'name'=>'idCommentType',
					'value'=>$model->getTypeName(),
				),
				array(
					'name'=>'statusComments',
					'value'=>$model->getStatusName(),
				),
				array(
					'name'=>'idRefenceTo',
					'value'=>$model->getCommentByReference(),
				),
			)
		));
    ?>
</div>

<?php
	$newComments = new Comments();
	$this->beginWidget('bootstrap.widgets.TbModal', array('id' => 'addCommentsModel')); ?>
		<?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
		    'id'=>'addNew',
		    'enableAjaxValidation'=>true,
		)); ?>

		<div class="modal-header">
	        <a class="close" data-toggle="modal" data-target="#addCommentsModel">&times;</a>
	        <h4>Nuevo Comentario</h4>
	    </div>
	 
	    <div class="modal-body" width="80%">
			<?php 
				echo $form->textAreaRow($newComments, 'description', array('rows'=>3, 'cols'=>100)); 
			?>
		</div>
	 
	    <div class="modal-footer">
	    	<?php $this->widget('bootstrap.widgets.TbButton', array(
				'label' => 'Cancelar',
				'url' => '#',
				'htmlOptions' => array('data-dismiss' => 'modal', 'class'=>'btn'),
			)); ?>
			<?php $this->widget('bootstrap.widgets.TbButton', array(
				'buttonType'=>'ajaxSubmit',
				'url'=>array('saveComment', 'comments'=>$model->idComments),
				'label'=>'Enviar',
				'htmlOptions' => array('class'=>'btn btn-primary', 'id'=>uniqid('btn-save-comments-')),
				'ajaxOptions'=>array(
					'dataType'=>'json',
					'success'=>'js: function(d)
					{
						if(d.success){

							$("#addCommentsModel").modal("hide");						
							$.fn.yiiGridView.update("comments-grid");
							$.fnResetForm($("#addNew"));							
						}
						else
						{
							$.each(d, function(key, val) {
			                   var $input = $("#"+key+"_em_");
			                   $input.text(val);                                                    
			                   $input.show();
			                });
						}
					}'
				)
			)); ?>
	    </div> 


	<?php $this->endWidget(); ?>
<?php $this->endWidget(); ?>


<?php
/* @var $this CommentsController */
/* @var $model Comments */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'idComments'); ?>
		<?php echo $form->textField($model,'idComments'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'description'); ?>
		<?php echo $form->textField($model,'description',array('size'=>60,'maxlength'=>500)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'idCommentType'); ?>
		<?php echo $form->textField($model,'idCommentType'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'dateComments'); ?>
		<?php echo $form->textField($model,'dateComments'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'statusComments'); ?>
		<?php echo $form->textField($model,'statusComments'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'idRefenceTo'); ?>
		<?php echo $form->textField($model,'idRefenceTo'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->
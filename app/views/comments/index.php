<div style="text-align:center">
    <h3>Comentarios</h3>
</div>

<div>
<?php //boton para modal
    $this->widget('bootstrap.widgets.TbButtonGroup',array(
                'size'=>'small',
                'htmlOptions'=>array('class'=>'pull-left'),
                'buttons'=>array(
                    array('label'=>'Enviar mensaje', 'type'=>'inverse', 
                        'htmlOptions'=>array('
                            data-toggle'=>'modal', 
                            'data-target'=>'#sendCommentsModel', 
                            'id'=>'sendComments'
                        )
                    ),
                ),
            ));
?>
</div>
<?php //modal
	$newComments = new Comments();
	$this->beginWidget('bootstrap.widgets.TbModal', array('id' => 'sendCommentsModel')); ?>
		<?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
		    'id'=>'addNew',
		    'enableAjaxValidation'=>true,
		)); ?>

		<div class="modal-header">
	        <a class="close" data-toggle="modal" data-target="#sendCommentModal">&times;</a>
	        <h4>Nuevo Comentario</h4>
	    </div>
	 
	    <div class="modal-body">
			<?php 
                echo $form->textAreaRow($newComments, 'description', array('rows'=>3, 'cols'=>100,'class'=>'form form-control')); 
                echo "<br>";
                echo CHtml::label('Destinatario','idRefenceTo');
                echo $form->dropDownList($newComments,'idRefenceTo', CHtml::listData(User::model()->findAll(array("order"=>"email")),'idUser','email'), array('empty'=>'Todos los usuarios'));
			?>
		</div>
	 
	    <div class="modal-footer">
	    	<?php $this->widget('bootstrap.widgets.TbButton', array(
				'label' => 'Cancelar',
				'url' => '#',
				'htmlOptions' => array('data-dismiss' => 'modal', 'class'=>'btn'),
			)); ?>
			<?php $this->widget('bootstrap.widgets.TbButton', array(
				'buttonType'=>'ajaxSubmit',
				'url'=>array('sendComment'),
				'label'=>'Enviar',
				'htmlOptions' => array('class'=>'btn btn-primary', 'id'=>uniqid('btn-send-comments-')),
				'ajaxOptions'=>array(
					'dataType'=>'json',
					'success'=>'js: function(d)
					{
						if(d.success){

							$("#sendCommentsModel").modal("hide");						
							$.fn.yiiGridView.update("comments-grid");
							$.fnResetForm($("#addNew"));							
						}
						else
						{
							$.each(d, function(key, val) {
			                   var $input = $("#"+key+"_em_");
			                   $input.text(val);                                                    
			                   $input.show();
			                });
						}
					}'
				)
			)); ?>
	    </div> 


	<?php $this->endWidget(); ?>
<?php $this->endWidget(); 
//end modal
?>
<div style="width:100%; margin:0 auto;">
    <?php
        $columns = array(
            'idComments'=>array(
                'class' => 'CDataColumn',
                'header' => 'ID',
                'name' => 'idComments',
                'filter'=> ''
            ),

            'idUser'=>array(
                'class' => 'CDataColumn',
                'name' => 'idUser',
                'value'=>'$data->getUserName()',
                'filter' => CHtml::ListData(User::model()->findAll(array('order' => 'name ASC')), 'idUser', 'name')
            ),

            'description'=>array(
                'name' => 'description',
                'value' => 'substr($data->description, 0, 30) . "..."',
            ),

            'dateComments'=>array(
                'name' => 'dateComments',
                'filter'=>$this->widget('zii.widgets.jui.CJuiDatePicker', array(
                    'model'=>$model,
                    'attribute'=>'dateComments',
                    'htmlOptions' => array(
                        'id' => 'date_comments_search',
                    ), 
                    'options' => array(
                        'showAnim'=>'fold',
                        'dateFormat' => 'yy-mm-dd',
                    )
                ), true)
            ),

            'statusComments'=>array(
                
                'class' => 'CDataColumn',
                //'class' => 'ETbEditableColumn',
                'name' => 'statusComments',
                'filter'=>CHtml::listData(CommentsStatus::model()->findAll(array('order'=>'idCommentsStatus ASC')),
                'idCommentsStatus', 'description'),
                'value'=>'$data->getStatusName()',
                /*'editable' => array(
                    'url' => $this->createUrl('changeAttributeComment'),
                    'placement' => 'top',
                    'source' => CHtml::listData(CommentsStatus::model()->findAll(array('order'=>'idCommentsStatus ASC')),
                    'idCommentsStatus', 'description'),
                    'type'      => 'select',
                    'attribute' => 'statusComments',
                    'inputclass' => 'span2',
                    'title' => 'Seleccionar Estado
                )*/
            ),

            'idCommentType'=>array(
                'class' => 'CDataColumn',
                'name' => 'idCommentType',
                'filter'=>CHtml::listData(CommentType::model()->findAll(array('order'=>'idCommentType ASC')),
                'idCommentType', 'description'),
                'value'=>'$data->getTypeName()',
            ),
            'idRefenceTo'=>array(
                'header' => 'Referencia',
                'urlExpression'=>'array("view","comments"=>$data->idRefenceTo)',
                'class'=>'CLinkColumn',
                'labelExpression'=> '$data->getCommentByReference() != "" ? substr($data->getCommentByReference(), 0, 15) . "..." : ""',
                        
            ),
        );

        $this->widget('bootstrap.widgets.TbExtendedGridView', array(
            'id' => 'comments-grid',
            'dataProvider' => $model->search(),
            'filter' => $model,
            'ajaxUrl'=> array('index'),
            'afterAjaxUpdate'=>"function(){jQuery('#date_comments_search').datepicker({'dateFormat': 'yy-mm-dd'})}",
            'fixedHeader' => true,
            'headerOffset' => 40,
            'type' => 'table striped bordered condensed',
            'responsiveTable' => true,
            'columns' => array_merge(
                array_values($columns),
                array(array(
                    'class' => 'bootstrap.widgets.TbButtonColumn',
                    'header'=>'Acciones',
                    'htmlOptions'=>array('style'=>'width:50px; text-align:center;'),
                    'buttons'=>array(
                        'view'=>array(
                            'icon'=>'icon-eye-open',
                            'label'=>Yii::t('app', 'View'),
                            'options'=>array('class'=>'btn btn-mini'),
                            'url'=>'CHtml::normalizeUrl(array("view", "comments"=>$data->primaryKey))',
                        ),
                    ),
                    'template'=>'{view}',
                ))
            ),
        )); 
    ?>
    <?php $this->widget('application.components.PageSizeComponent'); ?>
</div>
<script>
    $( document ).ready(function() {
    jQuery('#date_comments_search').datepicker({'dateFormat': 'yy-mm-dd'});
});
</script>
<style>
.ui-datepicker{
    background-color: #f3f3f3;
    width: 200px;
    color:#000;
    height: 220px;
    display:none;
}



.ui-datepicker  th {
    color:#000;
}

.ui-datepicker td {
    width: 20px;
    height: 25px;
}

.ui-datepicker td a{
    width: 100%;
    height: 110%;
    font-size: 12px;
    text-align: center;
    padding-top: 5px; 
    
}

.ui-state-default {
     background-color: transparent !important;
     border: none !important;
     border-radius: 5px;
}

.ui-state-default:hover {
     background-color: #0081c2 !important;
     color: #FFF;
}

.ui-state-active {
    background-color: #9336d1 !important;
}
</style>

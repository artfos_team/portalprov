<?php /* @var $this Controller */ ?>
<!DOCTYPE html>
    <html lang="en">
    <?php
                $cuadro_de_mandos_visible = true;
                $bandeja_visible = true;
                $consultas_compras_visible = false;
                $consultas_pagos_visible = false;

                if (user()->es_admin)
                    $texto_menu_dashboard = "Bandeja Administrador General";
                elseif (user()->es_proveedor)
                    $texto_menu_dashboard = "Bandeja Proveedor";
                elseif (user()->es_compras)
                    $texto_menu_dashboard = "Bandeja Compras";
                elseif (user()->es_pagos)
                    $texto_menu_dashboard = "Bandeja Pagos";
                else
                    $bandeja_visible = false;

                if (user()->es_proveedor || user()->es_compras || user()->es_pagos || user()->es_admin)
                    $consultas_compras_visible = true;
                if (user()->es_proveedor || user()->es_pagos || user()->es_admin)
                    $consultas_pagos_visible = true;
                
                $this->widget('bootstrap.widgets.TbNavbar', 
                    array(
                        'items'=>array(
                            array(
                                'class'=>'bootstrap.widgets.TbMenu',
                                'items' => array(
                                    array(
                                        'label' => 'Importaciones',
                                        'items' => array(
                                            array(
                                                'label' => 'Calendario de Ejecución',
                                                'url' => array('/schedule/index'),
                                                'visible' =>  user()->es_admin
                                            ),
                                            array(
                                                'label' => 'Configuración Procesos',
                                                'url' => array('/imports/formats'),
                                                'visible' => user()->es_admin
                                            ),
                                            array(
                                                'label' => 'Log de Importaciones',
                                                'url' => array('/imports/index'),
                                                'visible' => user()->es_admin
                                            )
                                        )
                                    ),
                                    array(
                                        'label'=>'Parametría',
                                        'items'=>array(
                                            array(
                                                'label' => 'Bancos', 
                                                'url' => array('/bank/index'),
                                                'visible'=>$this->checkAccess('user','index')
                                            ),
                                            array(
                                                'label' => 'Categoria a Retener', 
                                                'url' => array('/categoryRetain/index'),
                                                'visible'=>$this->checkAccess('user','index')
                                            ),
                                            array(
                                                'label' => 'Empresas Vinculadas', 
                                                'url' => array('/groupCompanies/index'),
                                                'visible'=>$this->checkAccess('user','index')
                                            ),
                                            array(
                                                'label' => 'Noticias', 
                                                'url' => array('/news/index'),
                                                'visible'=>$this->checkAccess('user','index')
                                            ),
                                            array(
                                                'label' => 'Instructivos', 
                                                'url' => array('/manual/index'),
                                                'visible' => user()->es_proveedor || user()->es_admin
                                            ),
                                            array(
                                                'label' => 'Jurisdicciones', 
                                                'url' => array('/jurisdictions/index'),
                                                'visible'=>$this->checkAccess('user','index')
                                            ),
                                            array(
                                                'label' => 'País', 
                                                'url' => array('/country/index'),
                                                'visible'=>$this->checkAccess('user','index')
                                            ),
                                            array(
                                                'label' => 'Provincia', 
                                                'url' => array('/province/index'),
                                                'visible'=>$this->checkAccess('user','index')
                                            ),
                                            array(
                                                'label'=>'Puestos', 
                                                'url'=>array('/job/index'),
                                                'visible'=>$this->checkAccess('user','index')
                                            ),

                                            array(
                                                'label' => 'Régimen de Retención', 
                                                'url' => array('/retentionRegime/index'),
                                                'visible'=>$this->checkAccess('user','index')
                                            ),

                                            array(
                                                'label' => 'Sucursales de Retiro de Cheques', 
                                                'url' => array('/bankBranch/index'),
                                                'visible'=>$this->checkAccess('user','index')
                                            ),
                                            array(
                                                'label' => 'Tipos de cuentas bancarias', 
                                                'url' => array('/bankAccountType/index'),
                                                'visible'=>$this->checkAccess('user','index')
                                            ),
                                            array(
                                                'label' => 'Tipo de Régimen de convenio', 
                                                'url' => array('/agreementRegimeType/index'),
                                                'visible'=>$this->checkAccess('user','index')
                                            ),                                                                                                                                                                                                                                 
                                        ),
                                        'visible' => !user()->isGuest
                                    ),
                                    array(
                                        'label'=>'Configuración Portal',
                                        'items'=>array(
                                            array(
                                                'label'=>'Compañías', 
                                                'url'=>array('/company/index'),
                                                'visible'=>$this->checkAccess('user','index')
                                            ),
                                             /*array(
                                                'label'=>'Parametros SAP', 
                                                'url'=>array('/sapParameters/index'),
                                                'visible'=>$this->checkAccess('user','index')
                                            ),*/
                                             array(
                                                'label'=>'Configuracion SMTP', 
                                                'url'=>array('/emailParameters/index'),
                                                'visible'=>$this->checkAccess('user','index')
                                            ),
                                            array(
                                                'label'=>'Configuracion de Correos Electrónicos', 
                                                'url'=>array('/customEmail/index'),
                                                'visible'=>$this->checkAccess('user','index')
                                            ),
                                            array(
                                                'label'=>'Configuraciones Generales', 
                                                'url'=>array('/configuration/index'),
                                                'visible'=>$this->checkAccess('user','index')
                                            ),   

                                            array(
                                                'label'=>'Parametros del sistema', 
                                                'url'=>array('/parameters/index'),
                                                'visible'=>$this->checkAccess('user','index')
                                            ),
                                            array(
                                                'label'=>'Proveedores', 
                                                'url'=>array('/provider/index'),
                                                'visible'=>$this->checkAccess('user','index')
                                            ),  
                                            array(
                                                'label'=>'Usuarios', 
                                                'url'=>array('/user/index'),
                                                'visible'=>$this->checkAccess('user','index')
                                            ),                                                                                                                                                                                                                                                                 
                                        ),
                                        'visible' => !user()->isGuest
                                    ),
                                    array(
                                        'label'=>'Auditoría',
                                        'items'=>array(
                                            array(
                                                'label'=>'Logs', 
                                                'url'=>array('/logs/index'),
                                                'visible'=>$this->checkAccess('user','index')
                                            )
                                        ),
                                        'visible' => !user()->isGuest
                                    ),
                                    array(
                                        'label'=>'Perfil',
                                        'items'=>array(
                                            array(
                                                'label'=>'Cambiar clave', 
                                                'url'=>array('/profile/changePassword')
                                            )
                                        ),
                                        'visible' => !user()->isGuest
                                    ),
                                    array(
                                        'label' => 'Login', 
                                        'url' => array('/site/login'), 
                                        'visible' => user()->isGuest
                                    ),
                                    array(
                                        'label' => 'Logout (' . user()->name . ')',
                                        'url' => array('/site/logout'),
                                        'visible' => !user()->isGuest
                                    )
                                ),
                            ),
                        )
                    )
                );
            ?>
    </head>
    <div class="container" id="page">
        <?php if (!empty($this->breadcrumbs)): ?>
            <?php 
                $this->widget(
                    'bootstrap.widgets.TbBreadcrumb',
                    array(
                        'links' => $this->breadcrumbs,
                    )
                ); 
            ?>
        <?php endif ?>
        <?php $this->widget('application.extensions.flashmessages.FlashMessageViewer');?>
        <?php echo $content; ?>
    </div>
    <style type="text/css">
        html, body {
            height: 100%;
            width: 100%;
            padding: 0;
            margin: 0;
        }
    </style>
    <div style=''>
    </div>
    <div id="footer">
        <div id="footer_text">
        <span id="footer_span"><strong>Copyright &copy; <?php echo date('Y'); ?>
        <a href="javascript:;"> PowerSuppliers</a>.</strong> powered by PowerSuppliers.</span>
        </div>
    </div>
    </body>
    </html>
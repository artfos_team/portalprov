<?php /* @var $this Controller */ ?>
<!DOCTYPE html>
<html lang="en">
<?php $this->renderPartial('app.views.layouts._head'); ?>
<body class="layout-minimal">

<div class="container">
    <?php echo $content; ?>
</div>
<div id="footer">
<div id="footer_text"><span id="footer_span"><strong>Copyright &copy; <?php echo date('Y'); ?>
        <a href="https://powersuppliers.io" target="_blank">PowerSuppliers.io</a>.</div>
</div>
</body>
</html>

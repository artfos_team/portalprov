<?php /* @var $this Controller */ ?>
<!DOCTYPE html>
<html lang="en">

    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <meta name="language" content="en"/>
        <link rel="icon" href="<?php echo baseUrl('/favicon.ico'); ?>" type="image/x-icon"/>
        <title><?php echo e($this->pageTitle); ?></title>
        <?php css('css/responsive.css'); ?>
        <?php //css('css/main.css'); ?>
        <?php css('css/mainInforme.css'); ?>
        <?php js('js/main.js'); ?>
        <?php //app()->bootstrap->registerAllScripts(); ?>
        <?php //app()->bootstrap->registerYiistrapCss(); ?>
        <?php //app()->livereload->register(); ?>
    </head>

    <body class="layout-informes">
        <header style="display: none;">
            <div class="container">
                <?php echo CHtml::image(baseUrl("/images/logo_informes2.jpg"), "logo", array('style'=>'height: 61px; display: none;')); ?>
            </div>
        </header>

        <section>
            <div class="container" id="page">
                <?php echo $content; ?>
            </div>
        </section>

        <footer style="display: none;">
            <div class="container">
                <?php echo CHtml::image('images/logo_informes_footer.jpg'); ?>
            </div>
        </footer>

    </body>
</html>
<?php
$this->widget('bootstrap.widgets.TbButtonGroup',array(
    'buttons'=>array(
        array('label'=> Yii::t('app', 'Agregar nuevo'), 'type'=>'inverse',
            'url'=>array('newImportSchedule')),
    ),
));

?>

<div style="text-align:center">
    <h3>Schedulers de importacion</h3>
</div>
<?php
/*if($companyGeneralData instanceof ProviderDataExport)
{
    echo '['.CHtml::link('Exportar Proveedores', array('exportarProv')).'] ';
}*/
?>
<div style="width:100%; margin:0 auto;">
    <?php
        $columns = array(
            'idImportSchedule'=>array(
                'class' => 'TbDataColumn',
                'name' => 'idImportSchedule',
                'header' => 'ID Schedule',
                'filter' => false,
                'htmlOptions' => array(
                    'width' => '10%'
                )
            ),
            'idImportFileFormat'=>array(
                'class' => 'TbDataColumn',
                'name' => 'idImportFileFormat',
                'value' => '$data->format->name',
                //'value' => '($data->format->name) ? "Enabled" : "Disabled"'
                //'($data->status) ? "Enabled" : "Disabled"'
            ),
            'time'=>array(
                'class' => 'TbDataColumn',
                'name' => 'time'
            )
        );


        $test = array();
        $test1 = array();
        $test2 = array();
        $test3 = array();

       // $cond = $columns['idImportFileFormat'] = '19';

        $this->widget('bootstrap.widgets.TbExtendedGridView', array(
            'id' => 'invoice-grid',
            'dataProvider' => $model->search(),
            'filter' => $model,
            'ajaxUrl'=>array('index'),
            'afterAjaxUpdate'=>"function(){jQuery('#date_import_search').datepicker({'dateFormat': 'yy-mm-dd'});}",
            'fixedHeader' => true,
            'headerOffset' => 40,
            'type' => 'table striped bordered condensed',
            'columns' => array_merge(array_values($columns),
                $test[] = array( 
                    $test1[] = array(
                        'class' => 'bootstrap.widgets.TbButtonColumn',
                        'header'=>'Acciones',
                        'htmlOptions'=>array('style'=>'width:150px; text-align:center;'),
                        'buttons'=> $test2[] = array(
                            $test3[] = 'Ejecutar'=>array(
                                'label'=>Yii::t('app', 'Ejecutar'),
                                'options'=>array('class'=>'btn btn-mini'),
                                'url'=>'CHtml::normalizeUrl(array("importManually", "id"=>$data->primaryKey))',
                            ),
                            'edit'=>array(
                                'label'=>Yii::t('app', 'Edit'),
                                'options'=>array('class'=>'btn btn-mini'),
                                'url'=>'CHtml::normalizeUrl(array("editImportSchedule", "id"=>$data->primaryKey))',
                            ),
                            'delete'=>array(
                                'options'=>array('class'=>'btn btn-mini btn-delete-row'),
                                'url'=>'app()->controller->createUrl("deleteImportSchedule",array(
                            "id"=>$data->primaryKey, "key"=>uKey("deleteImportSchedule".$data->primaryKey)))'
                            ),
                        ),
                        'template'=>'{Ejecutar} {edit} {delete}',
                    )
                )
            ),
        ));

        
        /*$test4 = array();
        $test4 = "'ok'=>array(
            'label'=>Yii::t('app', 'Import'),
            'options'=>array('class'=>'btn btn-mini')'";
        //array_push($test2, $test3);
        //print_r($columns['idImportFileFormat'] = '19');
        if($columns['idImportFileFormat'] = '19'){
            array_push($test2, $test4);
        }
       print_r($test2);*/
    ?>
    <?php $this->widget('application.components.PageSizeComponent'); ?>
</div>
<style>
.error {
    background-color: #efc499 !important;
}
.noError {
    background-color: #a7e8a0 !important;
}
.ui-datepicker{
    background-color: #f3f3f3;
    width: 200px;
    color:#000;
    height: 220px;
    display:none;
}



.ui-datepicker  th {
    color:#000;
}

.ui-datepicker td {
    width: 20px;
    height: 25px;
}

.ui-datepicker td a{
    width: 100%;
    height: 110%;
    font-size: 12px;
    text-align: center;
    padding-top: 5px; 
    
}

.ui-state-default {
     background-color: transparent !important;
     border: none !important;
     border-radius: 5px;
}

.ui-state-default:hover {
     background-color: #0081c2 !important;
     color: #FFF;
}

.ui-state-active {
    background-color: #9336d1 !important;
}
</style>
<?php $update = isset($update) ? $update : false; ?>
<h4><?php echo ($update) ? Yii::t('app', 'Actualizar schedule') : Yii::t('app', 'Crear schedule'); ?></h4>

<?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
    'id'=>'file-import-schedule-form',
    'htmlOptions'=>array('class'=>'well', 'enctype'=>'multipart/form-data'),
)); ?>

<?php echo CHtml::errorSummary($model);?>

<?php echo CHtml::activeLabelEx($model, 'time'); ?>
<?php
    $this->widget('application.extensions.timepicker.timepicker',array(
        'model'=>$model,
        'name'=>'time',
        'options'=>array(
            'timeOnly' => true,
            'currentText' => 'Ahora',
            'closeText' => 'Cerrar',
            'timeOnlyTitle' => 'Selecciona el horario',
            'hourText' => 'Hora',
            'minuteText' => 'Minutos',
            'timeText' => 'Horario',
            'hourGrid' => 4,
            'timeFormat' => 'hh:mm'
        ),
    ));
?>

<?php echo $form->dropDownListRow(
    $model,
    'idImportFileFormat',
    CHtml::listData(FileFormat::model()->findAll(array('order' => 'name')),'idImportFileFormat','name'),
    array('class'=>'span3', 'empty'=>Yii::t('app','Select an option'), 'required' => true)
); ?>

<div>
    <?php $this->widget('bootstrap.widgets.TbButton', array(
        'label' => 'Cancelar',
        'url' => array('/schedule/index'),
    )); ?>
    <?php $this->widget('bootstrap.widgets.TbButton', array(
        'buttonType'=>'submit', 
        'label'=>'Guardar',
        'htmlOptions' => array('class'=>'btn btn-primary'),
    )); ?>
</div>

<?php $this->endWidget(); ?>

<style type="text/css">
    #ui-datepicker-div {
        height: auto !important;
        display: none;
    }
    .ui-datepicker-trigger{
        margin-bottom: 10px;
        margin-left: 10px;
    }
</style>


<?php $update = isset($update) ? $update : false; ?>
<h4><?php echo ($update) ? Yii::t('app', 'Actualizar estado') : Yii::t('app', 'Crear estado'); ?></h4>

	<?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
	    'id'=>'user-form',
	    'htmlOptions'=>array('class'=>'well', 'enctype'=>'multipart/form-data'),
	)); ?>

    <?php echo CHtml::errorSummary($model);?>
    
    <?php echo CHtml::label('ID','ID'); ?>
    <?php echo $form->textField($model, 'idInvoiceStatus', array()); ?>
    <?php echo CHtml::label('Descripción','Descripción'); ?>
    <?php echo $form->textField($model, 'description', array()); ?>


</br></br>
<div>
    <?php $this->widget('bootstrap.widgets.TbButton', array(
        'label' => 'Cancelar',
        'url' => array('/invoiceStatus/index'),
    )); ?>
    <?php $this->widget('bootstrap.widgets.TbButton', array(
        'buttonType'=>'submit', 
        'label'=>'Guardar',
        'htmlOptions' => array('class'=>'btn btn-primary'),
    )); ?>
</div>

<?php $this->endWidget(); ?>
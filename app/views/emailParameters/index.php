<?php
/*$this->widget('bootstrap.widgets.TbButtonGroup',array(
        'buttons'=>array(
            array('label'=> Yii::t('app', 'Verificar host'), 'type'=>'inverse', 
                'url'=>array('ping')),
            array('label'=> Yii::t('app', 'Cambiar contraseña'), 'type'=>'inverse', 
                'htmlOptions'=>array('
                            data-toggle'=>'modal', 
                            'data-target'=>'#changePsw', 
                            'id'=>'changePswB'
                        )),
            
        ),
            
    ));?>
<?php   
            if($import)
            {
                $this->widget('bootstrap.widgets.TbButton',array('label'=> Yii::t('app', 'Importar facturas (Excel)'), 'type'=>'inverse', 
                'htmlOptions'=>array('
                            data-toggle'=>'modal', 
                            'data-target'=>'#importExcel', 
                            'id'=>'importExcelB'
                        )));
            } 
           */  ?>


<div style="text-align:center">
    <h3>Parametros de email</h3>
</div>

<div style="width:100%; margin:0 auto;">
    <?php
        $columns = array(

            'smtp'=>array(
                'class' => 'ETbEditableColumn',
                'name' => 'smtp',
                'header' => 'SMTP',
                'editable' => array(
                    'url' => $this->createUrl('ChangeAttributeEmailParameters'),
                    'attribute' => 'smtp',
                    'placement' => 'right',
                    'inputclass' => 'span2',
                    'title' => 'Ingresar SMTP'
                ),
            ),
            'user'=>array(
                'class' => 'ETbEditableColumn',
                'name' => 'user',
                'header' => 'Usuario',
                'editable' => array(
                    'url' => $this->createUrl('ChangeAttributeEmailParameters'),
                    'attribute' => 'user',
                    'placement' => 'top',
                    'inputclass' => 'span2',
                    'title' => 'Ingresar Usuario'
                ),
            ),
            'userPsw'=>array(
                'class' => 'ETbEditableColumn',
                'name' => 'userPsw',
                'header' => 'Clave',
                'editable' => array(
                    'url' => $this->createUrl('ChangeAttributeEmailParameters'),
                    'attribute' => 'client',
                    'placement' => 'top',
                    'inputclass' => 'span2',
                    'title' => 'Ingresar Clave'
                ),
            ),
            'fromName'=>array(
                'class' => 'ETbEditableColumn',
                'name' => 'fromName',
                'header' => 'Emisor',
                'editable' => array(
                    'url' => $this->createUrl('ChangeAttributeEmailParameters'),
                    'attribute' => 'fromName',
                    'placement' => 'top',
                    'inputclass' => 'span2',
                    'title' => 'Ingresar Emisor'
                ),
            ),
            'hostForMails'=>array(
                'class' => 'ETbEditableColumn',
                'name' => 'hostForMails',
                'header' => 'Ruta del sitio',
                'editable' => array(
                    'url' => $this->createUrl('ChangeAttributeEmailParameters'),
                    'attribute' => 'hostForMails',
                    'placement' => 'top',
                    'inputclass' => 'span2',
                    'title' => 'Ingresar Host para los emails'
                ),
            ),
            'port'=>array(
                'class' => 'ETbEditableColumn',
                'name' => 'port',
                'header' => 'Puerto',
                'editable' => array(
                    'url' => $this->createUrl('ChangeAttributeEmailParameters'),
                    'attribute' => 'port',
                    'placement' => 'top',
                    'inputclass' => 'span2',
                    'title' => 'Puerto'
                ),
            ),
            'security'=>array(
                'class' => 'ETbEditableColumn',
                'name' => 'security',
                'header' => 'Modo seguridad',
                'editable' => array(
                    'url' => $this->createUrl('ChangeAttributeEmailParameters'),
                    'attribute' => 'security',
                    'placement' => 'top',
                    'inputclass' => 'span2',
                    'title' => 'Modo seguridad'
                ),
            ),
        );

        $this->widget('bootstrap.widgets.TbExtendedGridView', array(
            'id' => 'invoice-grid',
            'dataProvider' => $model->search(),
            'filter' => $model,
            'ajaxUrl'=>array('index'),
            'fixedHeader' => true,
            'headerOffset' => 40,
            'type' => 'table striped bordered condensed',
            'rowCssClassExpression'=>"'invoiceRow'",
            'columns' => array_merge(array_values($columns),
                array( 
                    /*array(
                        'class' => 'bootstrap.widgets.TbButtonColumn',
                        'header'=>'Acciones',
                        'htmlOptions'=>array('style'=>'width:150px; text-align:center;'),
                        'buttons'=>array(
                            'view'=>array(
                                'icon'=>'icon-eye-open',
                                'label'=>Yii::t('app', 'View'),
                                'options'=>array('class'=>'btn btn-mini'),
                                'url'=>'CHtml::normalizeUrl(array("view", "invoiceStatus"=>$data->primaryKey))',
                            ), 
                        ),
                        'template'=>'',
                    )*/
                )
            ),
        )); 
    ?>
    <?php $this->widget('application.components.PageSizeComponent'); ?>
</div>
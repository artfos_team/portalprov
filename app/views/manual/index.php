<?php $this->widget('bootstrap.widgets.TbButtonGroup',array(
        'buttons'=>array(
            array('label'=> Yii::t('app', 'Agregar instructivo'), 'type'=>'inverse', 
                'url'=>array('create')),),
            
    ));?>
<div style="text-align:center">
    <h3>Instructivos</h3>
</div>

<div style="width:100%; margin:0 auto;">
    <?php
        $columns = array(
            'name'=>array(
                'class' => 'ETbEditableColumn',
                'name' => 'name',
                //'value'=> '$data->company',
                'editable' => array(
                    'url' => $this->createUrl('changeAttributeManual'),
                    'placement' => 'right',
                    'inputclass' => 'span2',
                    'title' => 'Insertar Nombre'
                ),
            ),
            
            'description'=>array(
                'class' => 'ETbEditableColumn',
                'name' => 'description',
                'editable' => array(
                    'url' => $this->createUrl('changeAttributeManual'),
                    'placement' => 'right',
                    'inputclass' => 'span2',
                    'title' => 'Insertar descripcion'
                ),
            ),   
       );

        $this->widget('bootstrap.widgets.TbExtendedGridView', array(
            'id' => 'provider-grid',
            'dataProvider' => $model->search(),
            'filter' => $model,
            'ajaxUrl'=> array('index'),
            'fixedHeader' => true,
            'headerOffset' => 40,
            'type' => 'table striped bordered condensed',
            'responsiveTable' => true,
            'columns' => array_merge(
                array_values($columns),
                array(array(
                    'class' => 'bootstrap.widgets.TbButtonColumn',
                    'header'=>'Acciones',
                    'htmlOptions'=>array('style'=>'width:150px; text-align:center;'),
                    'buttons'=>array(
                        'delete'=>array(
                            'icon'=>'icon-trash',
                            'label'=>'Eliminar',
                            //'visible'=> 'app()->rbac->checkAccess("company","viewCalification")',
                            'options'=>array('class'=>'btn btn-mini'),
                            'url'=>'app()->controller->createUrl("delete",array(
                                "id"=>$data->primaryKey, "key"=>uKey("delete".$data->primaryKey)))'
                            //'url'=>'CHtml::normalizeUrl(array("deleteDocument", "document"=>$data->primaryKey))',
                        ),
                    ),
                    
                    'template'=>'{delete}',
                ))
            ),
        )); 
    ?>
    <?php $this->widget('application.components.PageSizeComponent'); ?>
</div>
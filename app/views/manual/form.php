<h4>Crear nuevo instructivo</h4>


	<?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
	    'id'=>'document-form',
	    'htmlOptions'=>array('class'=>'well', 'enctype'=>'multipart/form-data'),
	)); ?>


<?php //CAMPOS
        echo CHtml::label('Tildar si es Instructivo','Descripción') . $form->checkBox($model, 'category');

        echo $form->textFieldRow($model, 'name', array('class'=>'span5', 'placeholder' =>'Nombre...'));

        echo $form->textAreaRow($model, 'description', array('style' => 'resize:none' ,'class'=>'span6', 'placeholder' => 'Descripcion...', 'rows' => 7));

        echo $form->labelEx($model, 'path');
        echo $form->fileField($model, 'path');
    ?>
</br></br>
<div>
    <?php $this->widget('bootstrap.widgets.TbButton', array(
        'label' => 'Cancelar',
        'url' => array('/document/index'),
    )); ?>
    <?php $this->widget('bootstrap.widgets.TbButton', array(
        'buttonType'=>'submit', 
        'label'=>'Guardar',
        'htmlOptions' => array('class'=>'btn btn-primary'),
    )); ?>
</div>

<?php $this->endWidget(); ?>
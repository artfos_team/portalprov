<h4>Agregar Noticia</h4>


	<?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
	    'id'=>'document-form',
	    'htmlOptions'=>array('class'=>'well', 'enctype'=>'multipart/form-data'),
	)); ?>


<?php //CAMPOS
        echo $form->textFieldRow($model, 'headLine', array('class'=>'span5', 'placeholder' =>'Título...'));
        echo $form->textArea($model, 'body', array('class'=>'span9','maxlength'=>5000,'rows' => 20,'placeholder' => Yii::t('app', 'Noticia')));
    ?>
</br></br>
<div>
    <?php $this->widget('bootstrap.widgets.TbButton', array(
        'label' => 'Cancelar',
        'url' => array('/news/index'),
    )); ?>
    <?php $this->widget('bootstrap.widgets.TbButton', array(
        'buttonType'=>'submit', 
        'label'=>'Guardar',
        'htmlOptions' => array('class'=>'btn btn-primary'),
    )); ?>
</div>

<?php $this->endWidget(); ?>
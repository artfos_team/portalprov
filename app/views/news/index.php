<?php $this->widget('bootstrap.widgets.TbButtonGroup',array(
        'buttons'=>array(
            array('label'=> Yii::t('app', 'Add News'), 'type'=>'inverse', 
                'url'=>array('create')),),
            
    ));?>
<div style="text-align:center">
    <h3>Noticias</h3>
</div>

<div style="width:80%; margin:0 auto;">
    <?php
        $columns = array(
            'dateLine'=>array(
                'class' => 'ETbEditableColumn',
                'name' => 'dateLine',
                //'value'=> '$data->company',
                /*'editable' => array(
                    'url' => $this->createUrl('changeAttributeManual'),
                    'placement' => 'right',
                    'inputclass' => 'span2',
                    'title' => 'id'
                ),*/
            ),
            'headLine'=>array(
                'class' => 'ETbEditableColumn',
                'name' => 'headLine',
                //'value'=> '$data->company',
                'editable' => array(
                    'url' => $this->createUrl('changeAttributeManual'),
                    'placement' => 'right',
                    'inputclass' => 'span2',
                    'title' => 'Modificar título'
                ),
            ),
       );

        $this->widget('bootstrap.widgets.TbExtendedGridView', array(
            'id' => 'provider-grid',
            'dataProvider' => $model->search(),
            'filter' => $model,
            'ajaxUrl'=> array('index'),
            'fixedHeader' => true,
            'headerOffset' => 40,
            'type' => 'table striped bordered condensed',
            'responsiveTable' => true,
            'columns' => array_merge(array_values($columns),
                array(
                    array(
                    'class' => 'bootstrap.widgets.TbButtonColumn',
                    'header'=>'Acciones',
                    'htmlOptions'=>array('style'=>'width:150px; text-align:center;'),
                    'buttons'=>array(
                        'edit'=>array(
                            'label'=>Yii::t('app', 'Edit'),
                            'options'=>array('class'=>'btn btn-mini'),
                            'url'=>'CHtml::normalizeUrl(array("EditBodyNews", "id"=>$data->primaryKey))',
                        )
                    ),
                    'template'=>'{edit}',
                    )                    
                )
            ),
        )); 
    ?>

    <?php $this->widget('application.components.PageSizeComponent'); ?>
</div>


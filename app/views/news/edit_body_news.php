<?php $update = isset($update) ? $update : false; ?>

<?php  $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
    'id'=>'email-news',
    'htmlOptions'=>array('class'=>'well', 'enctype'=>'multipart/form-data'),
)); ?>

<?php echo CHtml::errorSummary($model);?>

<?php echo $form->textArea($model, 'body', array('class'=>'span9','maxlength'=>5000,'rows' => 20,'placeholder' => Yii::t('app', 'Noticia'))); ?>


<div>
    <?php $this->widget('bootstrap.widgets.TbButton', array(
        'label' => 'Cancelar',
        'url' => array('/news/index'),
    )); ?>
    <?php $this->widget('bootstrap.widgets.TbButton', array(
        'buttonType'=>'submit', 
        'label'=>'Guardar',
        'htmlOptions' => array('class'=>'btn btn-primary'),
    )); ?>
</div>

<?php $this->endWidget(); ?>

<style type="text/css">
    #ui-datepicker-div {
        height: auto !important;
        display: none;
    }
    .ui-datepicker-trigger{
        margin-bottom: 10px;
        margin-left: 10px;
    }
</style>

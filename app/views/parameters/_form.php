<?php
/* @var $this ParametersController */
/* @var $model Parameters */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'parameters-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'idParameter'); ?>
		<?php echo $form->textField($model,'idParameter',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'idParameter'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'value'); ?>
		<?php echo $form->textField($model,'value'); ?>
		<?php echo $form->error($model,'value'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'stringValue'); ?>
		<?php echo $form->textField($model,'stringValue',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'stringValue'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'statusParameter'); ?>
		<?php echo $form->textField($model,'statusParameter'); ?>
		<?php echo $form->error($model,'statusParameter'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'date'); ?>
		<?php echo $form->textField($model,'date'); ?>
		<?php echo $form->error($model,'date'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'detailParameter'); ?>
		<?php echo $form->textField($model,'detailParameter',array('size'=>60,'maxlength'=>200)); ?>
		<?php echo $form->error($model,'detailParameter'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->
<?php
/* @var $this ParametersController */
/* @var $data Parameters */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('idParameter')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->idParameter), array('view', 'id'=>$data->idParameter)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('value')); ?>:</b>
	<?php echo CHtml::encode($data->value); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('stringValue')); ?>:</b>
	<?php echo CHtml::encode($data->stringValue); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('statusParameter')); ?>:</b>
	<?php echo CHtml::encode($data->statusParameter); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('date')); ?>:</b>
	<?php echo CHtml::encode($data->date); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('detailParameter')); ?>:</b>
	<?php echo CHtml::encode($data->detailParameter); ?>
	<br />


</div>
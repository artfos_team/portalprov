<?php
/* @var $this ParametersController */
/* @var $model Parameters */

$this->breadcrumbs=array(
	'Parameters'=>array('index'),
	$model->idParameter=>array('view','id'=>$model->idParameter),
	'Update',
);

$this->menu=array(
	array('label'=>'List Parameters', 'url'=>array('index')),
	array('label'=>'Create Parameters', 'url'=>array('create')),
	array('label'=>'View Parameters', 'url'=>array('view', 'id'=>$model->idParameter)),
	array('label'=>'Manage Parameters', 'url'=>array('admin')),
);
?>

<h1>Update Parameters <?php echo $model->idParameter; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
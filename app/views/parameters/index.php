<div style="text-align:center">
    <h3>Parametros del portal</h3>
</div>

<div style="width:100%; margin:0 auto;">
    <?php
        $this->widget(
		    'bootstrap.widgets.TbExtendedGridView',
		    array(
		        'id' => 'provider-grid',
	            'dataProvider' => $model->search(),
	            'filter' => $model,
	            'ajaxUrl'=> array('index'),
	            'fixedHeader' => true,
	            'headerOffset' => 40,
	            'type' => 'table striped bordered condensed',
	            'responsiveTable' => true,
		        'template' => "{items}",
		        'columns' => array(
		            array('name' => 'idParameter', 'header' => 'Parametro','filter'=>''),
		            array('name' => 'detailParameter', 'header' => 'Detalle del parametro','filter'=>''),
		            array(
		            	 'class' => 'ETbEditableColumn',
		                'editable' => array(
		                    'url' => $this->createUrl('changeAttributeParameters'),
		                    'placement' => 'top',
		                    'source' => array('0'=> 'Desactivado', '1'=>'Activado'),
		                    'type'      => 'select',
		                    'attribute' => 'value',
		                    'inputclass' => 'span2',
                    		'title' => 'Seleccionar Estado'),
		                'name' => 'value',
		                'header' => 'Valor',
		                'filter'=>''
		            ),
		        )
				)
		);
    ?>
    <?php $this->widget('application.components.PageSizeComponent'); ?>
</div>


<?php
$this->widget('bootstrap.widgets.TbButtonGroup',array(
        'buttons'=>array(
            array('label'=> Yii::t('app', 'Ver errores en detalle'), 'type'=>'inverse', 
                'url'=>array('errors')),
        ),
    ));

?>


<div style="text-align:center">
    <h3>Log de Importaciones</h3>
</div>

<div style="width:100%; margin:0 auto;">
    <?php
        $columns = array(

            'idImport'=>array(
                'class' => 'TbDataColumn',
                'name' => 'idImport',
                'header' => 'ID Importación',
                'filter' => false,
                'htmlOptions' => array(
                    'width' => '10%'
                )
            ),
            'idProcess'=>array(
                'class' => 'TbDataColumn',
                'name' => 'idImport',
                'header' => 'Proceso',
                'filter' => false,
                'htmlOptions' => array(
                    'width' => '10%'
                )
            ),
            'dateImport'=>array(
                'class' => 'TbDataColumn',
                'name' => 'dateImport',
                'header' => 'Fecha',
                'filter'=>$this->widget('zii.widgets.jui.CJuiDatePicker', array(
                    'model'=>$model,
                    'attribute'=>'dateImport',
                    'htmlOptions' => array(
                        'id' => 'date_import_search',
                    ), 
                    'options' => array(
                        'showAnim'=>'fold',
                        'dateFormat' => 'yy-mm-dd',
                    )
                ), true)
            ),
            'totalImport'=>array(
                'class' => 'TbDataColumn',
                'name' => 'totalImport',
                'header' => 'Total ',
            ),
            'createImport'=>array(
                'class' => 'TbDataColumn',
                'name' => 'createImport',
                'header' => 'Registros  nuevos',
            ),
            'updateImport'=>array(
                'class' => 'TbDataColumn',
                'name' => 'updateImport',
                'header' => 'Registros actualizadas',
            ),
            'errorImport'=>array(
                'class' => 'TbDataColumn',
                'name' => 'errorImport',
                'header' => 'Cantidad de errores',
                'filter' => array('noError' => 'Sin errores' , 'error' => 'Con errores'),
                'type' => 'raw',
                'cssClassExpression' => '$data->errorImport > 0 ? "error" : "noError"',
                //'htmlOptions' => array('style' => $data->errorImport == 0 ? 'background-color: #efc499' : 'background-color: #a7e8a0')
            )
        );

        $this->widget('bootstrap.widgets.TbExtendedGridView', array(
            'id' => 'invoice-grid',
            'dataProvider' => $model->search(),
            'filter' => $model,
            'ajaxUrl'=>array('index'),
            'afterAjaxUpdate'=>"function(){jQuery('#date_import_search').datepicker({'dateFormat': 'yy-mm-dd'});}",
            'fixedHeader' => true,
            'headerOffset' => 40,
            'type' => 'table striped bordered condensed',
            'columns' => array_merge(array_values($columns),
                array( 
                    /*array(
                        'class' => 'bootstrap.widgets.TbButtonColumn',
                        'header'=>'Acciones',
                        'htmlOptions'=>array('style'=>'width:150px; text-align:center;'),
                        'buttons'=>array(
                            'view'=>array(
                                'icon'=>'icon-eye-open',
                                'label'=>Yii::t('app', 'View'),
                                'options'=>array('class'=>'btn btn-mini'),
                                'url'=>'CHtml::normalizeUrl(array("view", "invoiceStatus"=>$data->primaryKey))',
                            ), 
                        ),
                        'template'=>'',
                    )*/
                )
            ),
        ));
    ?>
    <?php $this->widget('application.components.PageSizeComponent'); ?>
</div>
<script>
    $( document ).ready(function() {
    jQuery('#date_import_search').datepicker({'dateFormat': 'yy-mm-dd'});
});
</script>
<style>
.error {
    background-color: #efc499 !important;
}
.noError {
    background-color: #a7e8a0 !important;
}
.ui-datepicker{
    background-color: #f3f3f3;
    width: 200px;
    color:#000;
    height: 220px;
    display:none;
}



.ui-datepicker  th {
    color:#000;
}

.ui-datepicker td {
    width: 20px;
    height: 25px;
}

.ui-datepicker td a{
    width: 100%;
    height: 110%;
    font-size: 12px;
    text-align: center;
    padding-top: 5px; 
    
}

.ui-state-default {
     background-color: transparent !important;
     border: none !important;
     border-radius: 5px;
}

.ui-state-default:hover {
     background-color: #0081c2 !important;
     color: #FFF;
}

.ui-state-active {
    background-color: #9336d1 !important;
}
</style>
<?php
$this->widget('bootstrap.widgets.TbButtonGroup',array(
        'buttons'=>array(
            array('label'=> Yii::t('app', 'Ver todas las importaciones'), 'type'=>'inverse', 
                'url'=>array('index')),
        ),
    ));

$this->widget('bootstrap.widgets.TbButtonGroup',array(
    'buttons'=>array(
        array(
            'label'=> Yii::t('app', 'Nuevo formato'),
            'type'=>'success',
            'url'=>array('newformat')
        ),
    ),
));
?>


<div style="text-align:center">
    <h3>Formatos de importaciones</h3>
</div>

<div style="width:100%; margin:0 auto;">
    <?php
        $columns = array(

            'idImportFileFormat'=>array(
                'class' => 'TbDataColumn',
                'name' => 'idImportFileFormat',
                'header' => 'ID Formato',
                'filter' => false,
                'htmlOptions' => array(
                    'width' => '6%'
                )
            ),
            'name'=>array(
                'class' => 'TbDataColumn',
                'name' => 'name',
                'htmlOptions' => array(
                    'width' => '60%'
                )
            ),
            /*'type'=>array(
                'class' => 'TbDataColumn',
                'name' => 'type',
                'value' => 'FileFormat::FORMATS[$data->type]'
            ),*/
        );

        $this->widget('bootstrap.widgets.TbExtendedGridView', array(
            'id' => 'invoice-grid',
            'dataProvider' => $model->search(),
            'filter' => $model,
            'ajaxUrl'=>array('formats'),
            'fixedHeader' => true,
            'headerOffset' => 40,
            'type' => 'table striped bordered condensed',
            'columns' => array_merge(
                array_values($columns),
                array(array(
                    'class' => 'bootstrap.widgets.TbButtonColumn',
                    'header'=>'Acciones',
                    'htmlOptions'=>array('style'=>'width:150px; text-align:center;'),
                    'buttons'=>array(
                        'edit'=>array(
                            'label'=>Yii::t('app', 'Edit'),
                            'options'=>array('class'=>'btn btn-mini'),
                            'url'=>'CHtml::normalizeUrl(array("editFormat", "id"=>$data->primaryKey))',
                        ),
                        'delete'=>array(
                            'options'=>array('class'=>'btn btn-mini btn-delete-row'),
                            'url'=>'app()->controller->createUrl("deleteFormat",array(
                            "id"=>$data->primaryKey, "key"=>uKey("deleteFormat".$data->primaryKey)))'
                        ),

                    ),

                    'template'=>'{edit} {delete}',
                ))
            ),
        ));
    ?>
    <?php $this->widget('application.components.PageSizeComponent'); ?>
</div>
<style>
.error {
    background-color: #efc499 !important;
}
.noError {
    background-color: #a7e8a0 !important;
}
</style>
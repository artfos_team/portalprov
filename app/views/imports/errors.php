<?php
$this->widget('bootstrap.widgets.TbButtonGroup',array(
        'buttons'=>array(
            array('label'=> Yii::t('app', 'Ver todas las importaciones'), 'type'=>'inverse', 
                'url'=>array('index')),
            
        ),
            
    ));?>


<div style="text-align:center">
    <h3>Errores en las importaciones</h3>
</div>

<div style="width:100%; margin:0 auto;">
    <?php
        $columns = array(

            'idImportError'=>array(
                'class' => 'TbDataColumn',
                'name' => 'idImportError',
                'header' => 'ID Error',
                'filter' => false,
                'htmlOptions' => array(
                    'width' => '6%'
                )
            ),
            'idImport'=>array(
                'class' => 'TbDataColumn',
                'name' => 'idImport',
                'header' => 'ID Importación',
                'htmlOptions' => array(
                    'width' => '10%'
                )
            ),
            'description'=>array(
                'class' => 'TbDataColumn',
                'name' => 'description',
                'header' => 'Descripción',
            ),
            
            
        	/*'status'=>array(
                'class' => 'ETbEditableColumn',
                'name' => 'status',
                'filter'=> array('0' => 'Sin arreglar', '1' => 'Arreglado'),
                'value'=>'$data->getStatusDescription()',
                'editable' => array(
                    'url' => $this->createUrl('changeAttributeImportError'),
                    'placement' => 'top',
                    'source' => array('0' => 'Sin arreglar', '1' => 'Arreglado'),
                    'type'      => 'select',
                    'attribute' => 'status',
                    'inputclass' => 'span2',
                    'title' => 'Seleccionar Estado'
                ),
                'htmlOptions' => array(
                    'width' => '15%'
                )
            ),*/
            'dateImport'=>array(
                'header' => 'Fecha',
                'value' => '$data->getImportDate()'
            ),
        );

        $this->widget('bootstrap.widgets.TbExtendedGridView', array(
            'id' => 'invoice-grid',
            'dataProvider' => $model->search(),
            'filter' => $model,
            'ajaxUrl'=>array('errors'),
            'fixedHeader' => true,
            'headerOffset' => 40,
            'type' => 'table striped bordered condensed',
            'columns' => array_merge(array_values($columns),
                array( 
                    /*array(
                        'class' => 'bootstrap.widgets.TbButtonColumn',
                        'header'=>'Acciones',
                        'htmlOptions'=>array('style'=>'width:150px; text-align:center;'),
                        'buttons'=>array(
                            'view'=>array(
                                'icon'=>'icon-eye-open',
                                'label'=>Yii::t('app', 'View'),
                                'options'=>array('class'=>'btn btn-mini'),
                                'url'=>'CHtml::normalizeUrl(array("view", "invoiceStatus"=>$data->primaryKey))',
                            ), 
                        ),
                        'template'=>'',
                    )*/
                )
            ),
        )); 
    ?>
    <?php $this->widget('application.components.PageSizeComponent'); ?>
</div>
<style>
.error {
    background-color: #efc499 !important;
}
.noError {
    background-color: #a7e8a0 !important;
}
</style>
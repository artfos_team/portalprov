
<?php $update = isset($update) ? $update : false; ?>
<h4><?php echo ($update) ? Yii::t('app', 'Actualizar format') : Yii::t('app', 'Crear formato'); ?></h4>

	<?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
	    'id'=>'file-import-format-form',
	    'htmlOptions'=>array('class'=>'well', 'enctype'=>'multipart/form-data'),
	)); ?>

    <?php echo CHtml::errorSummary($model);?>
    
    <?php echo CHtml::activeLabelEx($model, 'name'); ?>
    <?php echo $form->textField($model, 'name', array('required' => true)); ?>

    <?php echo CHtml::activeLabelEx($model, 'url'); ?>
    <?php echo $form->textField($model, 'url', array('required' => true)); ?>

    <?php echo $form->dropDownListRow(
        $model,
        'type',
        FileFormat::FORMATS,
        array('class'=>'span3', 'empty'=>Yii::t('app','Select an option'), 'required' => true)
    ); ?>

    <?php echo CHtml::label('Columnas', 'Columnas', array('required' => true)); ?>
    <?php
        if ($update) {
            $columns = $model->importColumns;
        }
        else {
            $columns = array([]);
        }

        foreach ($columns as $index => $column) {
    ?>
    <div id="columns-list">
        <div class="container-column">
            <span class="number-column"><?php echo $index+1; ?> - </span>
            <?php echo $form->dropDownList(
                $model,
                'columns[]',
                FileColumn::getColumns(),
                array(
                    'class'=>'span3',
                    'empty'=>Yii::t('app','Select an option'),
                    'value' => ($column instanceOf FileColumn ? $column->name : null)
                )
            ); ?>
            &nbsp;&nbsp;&nbsp;<a href="javascript:void(0)" class="remove-column" style="display: none">Quitar columna</a>
        </div>
    </div>
    <?php } ?>

</br></br>
<div>
    <?php $this->widget('bootstrap.widgets.TbButton', array(
        'type' => 'info',
        'label' => 'Agregar columna',
        'url' => 'javascript:addColumn()',
    )); ?>
    <?php $this->widget('bootstrap.widgets.TbButton', array(
        'label' => 'Cancelar',
        'url' => array('/imports/formats'),
    )); ?>
    <?php $this->widget('bootstrap.widgets.TbButton', array(
        'buttonType'=>'submit', 
        'label'=>'Guardar',
        'htmlOptions' => array('class'=>'btn btn-primary'),
    )); ?>
</div>

<?php $this->endWidget(); ?>

<script type="text/javascript">
    var columns = <?php echo count($columns); ?>;

    function addColumn() {
        var newColumn = $('.container-column:nth-child(1)').clone();

        $('#columns-list').append(newColumn);
        columns++;
        newColumn.find('.number-column').text(columns + ' - ');
        newColumn.find('.remove-column').show();

        newColumn.find('a.remove-column').on('click', function() {
            $(this).parent().remove();

            reorderColumns();
        });
    }

    function reorderColumns() {
        $('.number-column').each((index, column) => {
           $(column).text((index+1) + ' - ');
        });
    }

    $(document).ready(function() {
        $('select[name="FileFormat[columns][]"]').each((index, column) => {
            $(column).val($(column).attr('value'));
        });
    });
</script>
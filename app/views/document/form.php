<h4><?php (isset($update) && $update) ? 'Actualizar Documento' : 'Crear nueva documentacion' ?></h4>


	<?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
	    'id'=>'document-form',
	    'htmlOptions'=>array('class'=>'well', 'enctype'=>'multipart/form-data'),
	)); ?>

    <?php echo CHtml::errorSummary($model);?>
    <?php echo $form->textFieldRow($model, 'documentName', array('class'=>'span5', 'placeholder' => Yii::t('app', 'Documento...'))); ?>
    <?php echo $form->dropDownListRow($model, 'required', array('1' => 'Si', '0'=> 'No'), array('class'=>'span5')); ?>
    <?php echo $form->dropDownListRow($model, 'active', array('1' => 'Si', '0'=> 'No'), array('class'=>'span5')); ?>

</br></br>
<div>
    <?php $this->widget('bootstrap.widgets.TbButton', array(
        'label' => 'Cancelar',
        'url' => array('/document/index'),
    )); ?>
    <?php $this->widget('bootstrap.widgets.TbButton', array(
        'buttonType'=>'submit', 
        'label'=>'Guardar',
        'htmlOptions' => array('class'=>'btn btn-primary'),
    )); ?>
</div>

<?php $this->endWidget(); ?>
<?php
/* @var $this SiteController */
/* @var $model LoginForm */
/* @var $form TbActiveForm */

$this->pageTitle = app()->name . ' - '. Yii::t('app', 'Change password');
?>

<div style="width:100%; margin:30px auto;">

	<h4>Cambiar clave</h4>

	<?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
		    'id'=>'addRow',
		    'htmlOptions'=>array('class'=>'well'),
		    'enableAjaxValidation'=>false
		)); ?>


		<?php echo CHtml::errorSummary($model); ?>
		
		<?php echo $form->passwordFieldRow($model, '_oldPassword', array('class'=>'span3')); ?>
		<?php echo $form->passwordFieldRow($model, 'password', array('class'=>'span3')); ?>
		<?php echo $form->passwordFieldRow($model, '_repeatPassword', array('class'=>'span3')); ?>
		
		<div>
			<?php $this->widget('bootstrap.widgets.TbButton', array(
				'buttonType'=>'submit', 
				'label'=>'Cambiar',
				'htmlOptions' => array('class'=>'btn btn-primary')		
			)); ?>
		</div>

	<?php $this->endWidget(); ?>
</div> 

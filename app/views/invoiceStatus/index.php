<?php
$this->widget('bootstrap.widgets.TbButtonGroup',array(
        'buttons'=>array(
            array('label'=> Yii::t('app', 'Crear estado'), 'type'=>'inverse', 
                'url'=>array('create')),),

    ));?>
<div style="text-align:center">
    <h3>Estados de las facturas</h3>
</div>

<div style="width:100%; margin:0 auto;">
    <?php
        $columns = array(

            'idInvoiceStatus'=>array(
                'class' => 'ETbEditableColumn',
                'name' => 'idInvoiceStatus',
                'header' => 'ID',
                'editable' => array(
                    'url' => $this->createUrl('changeAttributeInvoicestatus'),
                    'placement' => 'right',
                    'inputclass' => 'span2',
                    'title' => 'Ingresar ID'
                ),
            ),

            'description'=>array(
                'class' => 'ETbEditableColumn',
                'name' => 'description',
                'header' => 'Descripción',
                'editable' => array(
                    'url' => $this->createUrl('changeAttributeInvoicestatus'),
                    'placement' => 'top',
                    'inputclass' => 'span2',
                    'title' => 'Ingresar Descripción'
                ),
            ),
        );

        $this->widget('bootstrap.widgets.TbExtendedGridView', array(
            'id' => 'invoice-grid',
            'dataProvider' => $model->search(),
            'filter' => $model,
            'ajaxUrl'=>array('index'),
            'fixedHeader' => true,
            'headerOffset' => 40,
            'type' => 'table striped bordered condensed',
            'rowCssClassExpression'=>"'invoiceRow'",
            'responsiveTable' => true,
            'columns' => array_merge(array_values($columns),
                array( 
                    /*array(
                        'class' => 'bootstrap.widgets.TbButtonColumn',
                        'header'=>'Acciones',
                        'htmlOptions'=>array('style'=>'width:150px; text-align:center;'),
                        'buttons'=>array(
                            'view'=>array(
                                'icon'=>'icon-eye-open',
                                'label'=>Yii::t('app', 'View'),
                                'options'=>array('class'=>'btn btn-mini'),
                                'url'=>'CHtml::normalizeUrl(array("view", "invoiceStatus"=>$data->primaryKey))',
                            ), 
                        ),
                        'template'=>'',
                    )*/
                )
            ),
        )); 
    ?>
    <?php $this->widget('application.components.PageSizeComponent'); ?>
</div>
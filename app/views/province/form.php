<h4>Actualizar Provincia</h4>

	<?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
	    'id'=>'province-form',
	    'htmlOptions'=>array('class'=>'well', 'enctype'=>'multipart/form-data'),
	)); ?>

    <?php echo CHtml::errorSummary($model);?>

    <?php echo $form->textFieldRow($model, 'codRegion', array('class'=>'span3', 'placeholder' => Yii::t('app', 'CodeRegion'))); ?>

    <?php echo $form->textFieldRow($model, 'province', array('class'=>'span3', 'placeholder' => Yii::t('app', 'Región'))); ?>
    
    <?php echo $form->dropDownListRow($model, 'idCountry', CHtml::listData(Country::model()->findAll(array('order'=>'country')), 'idCountry', 'country'), 
        array('class'=>'span3', 'empty'=>Yii::t('app','Select an option'))); ?>


    </br>
    </br>


</br></br>
<div>
    <?php $this->widget('bootstrap.widgets.TbButton', array(
        'label' => 'Cancelar',
        'url' => array('index'),
    )); ?>
    <?php $this->widget('bootstrap.widgets.TbButton', array(
        'buttonType'=>'submit', 
        'label'=>'Guardar',
        'htmlOptions' => array('class'=>'btn btn-primary'),
    )); ?>
</div>

<?php $this->endWidget(); ?>
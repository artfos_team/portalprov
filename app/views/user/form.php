<?php $update = isset($update) ? $update : false; ?>
<h4><?php echo ($update) ? Yii::t('app', 'Update user') : Yii::t('app', 'Create user'); ?></h4>

	<?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
	    'id'=>'user-form',
	    'htmlOptions'=>array('class'=>'well', 'enctype'=>'multipart/form-data'),
	)); ?>

    <?php echo CHtml::errorSummary($model);?>
    
    <?php echo $form->textFieldRow($model, 'name', array('class'=>'span5', 'placeholder' => Yii::t('app', 'Alias'))); ?>
    
    <?php echo $form->textFieldRow($model, 'email', array('class'=>'span5', 'placeholder' => Yii::t('app', 'Email'))); ?>

    <?php
    //if(!$update)
    //{
        echo $form->passwordFieldRow($model, 'password', array('class'=>'span5', 'placeholder' => Yii::t('app', 'Password')));

        echo $form->passwordFieldRow($model, '_repeatPassword', array('class'=>'span5', 'placeholder' => 'Repetir contraseña'));
    //}
    ?>
    <?php echo $form->dropDownListRow($model, 'idJob', CHtml::listData(Job::model()->findAll(), 'idJob', 'job'),
        array('class'=>'span5', 'empty'=>'Seleccionar Puesto')); ?>
    <?php //echo $form->dropDownListRow($model, 'idCompany', CHtml::listData(Company::model()->findAll(), 'idCompany', 'company'),
        //array('class'=>'span5', 'empty'=>'Seleccionar empresa')); ?>

	<?php //echo $form->dropDownListRow($model, 'idConsultant', CHtml::listData(Consultant::model()->findAll(), 'idConsultant', 'consultant'),
   //     array('class'=>'span5', 'empty'=>'Seleccionar consultor')); ?>
   
    <?php //echo $form->dropDownListRow($model, 'sex', array(0=>'Femenino', 1=>'Masculino'),
        //array('class'=>'span5', 'empty'=>'Seleccionar sexo')); ?>

    <?php //$model->generatePictureInput(); ?>


</br></br>
<div>
    <?php $this->widget('bootstrap.widgets.TbButton', array(
        'label' => 'Cancelar',
        'url' => array('/user/index'),
    )); ?>
    <?php $this->widget('bootstrap.widgets.TbButton', array(
        'buttonType'=>'submit', 
        'label'=>$update ? 'Actualizar' : 'Crear',
        'htmlOptions' => array('class'=>'btn btn-primary'),
    )); ?>
</div>

<?php $this->endWidget(); ?>
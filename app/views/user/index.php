<div class="buttons">
	<?php
	$params = array_merge($_GET, array('export'=>true));
	$this->widget('bootstrap.widgets.TbButtonGroup',array(
		'buttons'=>array(
			array('label'=> Yii::t('app', 'Create user'), 'type'=>'inverse', 
				'url'=>array('create')),
			array('label'=>Yii::t('app', 'Export'), 'type'=>'inverse',
					'url'=>app()->controller->createUrl('index', $params)),
			)
	));?>
</div>

<div style="text-align:center">
	<h3>Usuarios</h3>
</div>

<div style="width:100%; margin:0 auto;">

	<?php
	$columns = array(
		'username'=>array(
			'class' => 'ETbEditableColumn',
			'name' => 'name',
			'sortable'=>true,
			'editable' => array(
				'url' => $this->createUrl('changeAttributeUser'),
				'placement' => 'right',
				'inputclass' => 'span3',
                'title' => 'Ingresar Alias'
			),
			'filter' => CHtml::ListData(User::model()->findAll(array('order' => 'name ASC')), 'name', 'name')
		),

		'permissions'=>array(
			'header' => 'Permisos',
			'class' => 'TbDataColumn',
			'id' => 'permissions',
			'value' => '$data->getRolesList()'
		),
		
		'email'=>array(
			'class' => 'TbDataColumn',
			'name' => 'email',
			'sortable'=>true,
		),
		'idJob'=>array(
			'class' => 'CDataColumn',
			'name' => 'idJob',
			'value'=>'$data->getJobName()',
			'filter' => CHtml::ListData(Job::model()->findAll(array('order' => 'job ASC')), 'idJob', 'job')
		),

	);

	$this->widget('bootstrap.widgets.TbExtendedGridView', array(
		'id' => 'user-grid',
		'dataProvider' => $model->search(),
		'filter' => $model,
		'ajaxUrl'=>array('index'),
		'fixedHeader' => true,
		'headerOffset' => 40,
		'type' => 'table striped bordered condensed',
		'responsiveTable' => true,
		'rowCssClassExpression'=>"'userRow'",
		'columns' => array_merge(array_values($columns),
			array(array(
				'class' => 'bootstrap.widgets.TbButtonColumn',
				'header'=>'Acciones',
				'htmlOptions'=>array('style'=>'width:150px; text-align:center;'),
				'buttons'=>array(
					'delete'=>array(
						'options'=>array('class'=>'btn btn-mini btn-delete-row'),
						'url'=>'app()->controller->createUrl("delete",array(
							"id"=>$data->primaryKey, "key"=>uKey("deleteUser".$data->primaryKey)))'
					),
					'update'=>array(
						'label'=>Yii::t('app', 'Edit'),
						'options'=>array('class'=>'btn btn-mini'),
						'url'=>'app()->controller->createUrl("update",array(
							"id"=>$data->primaryKey, "key"=>uKey("updateUser".$data->primaryKey)))'
					),
					'rolesAndPerms'=>array(
						'url'=>'CHtml::normalizeUrl(array("userRolesPermissions",
							"idUser"=>$data->primaryKey,
							"key"=>uKey("perms".$data->primaryKey)))',
						'label'=>Yii::t('app', 'Permissions'),
						'options'=>array('class'=>'btn btn-mini open-modal-small btn-key-row'),
					),
				),
				'template'=>'{update} {rolesAndPerms} {delete}',
				),
			)),
		));
	?>
</div>

<?php
cs()->registerScript(__FILE__ . '_smallUserModal', '
		$("body").on("click", ".open-modal-small", function(e){
			var $this = $(this);
			var $modal = $("#smallUserModal");
			$modal.html("<i class=\"icon-spinner icon-spin icon-large\"></i>");
			$modal.modal("show");
			$.ajax($this.attr("href"), {
				"success": function(data) {
					$modal.html(data);
				},

				"error": function(x,s,e)	{
					$modal.html(x.responseText);
				}

			});
			e.preventDefault();
		});
	');

$this->beginWidget('bootstrap.widgets.TbModal', array('id' => 'smallUserModal',
	 'htmlOptions'=>array('class'=> 'modal-add'))); 
$this->endWidget() ;

cs()->registerScript(__FILE__ . '_toggleLinks', '
jQuery("body").on("click", ".togglelink", function(eventObject){
	$this = jQuery(this);
	$.ajax($this.data("togglelinkurl"),
		{
			dataType:"json",
			type: "POST",
			success: function(data){
				$this.html(data.text);
			}
		}
	);
	eventObject.preventDefault();
});');

?>


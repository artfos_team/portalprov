<?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
    'id'=>'addNewRecord',
    'enableAjaxValidation'=>true,
)); ?>
<div class="modal-header">
    <a class="close" data-dismiss="modal">&times;</a>
    <h4>"<?php echo  Yii::t('app', 'Roles for {user}', array('{user}'=>$user->name)); ?>"</h4>
</div>

<div class="modal-body" style="max-height: 310px; overflow: scroll;">
	
	<?php foreach($roles as $role)
	{ ?>
		<div class="row-fluid">
			<div class="span12">
				<?php echo $role->name . '?: ' . CHtml::link($user->hasRole($role->getPrimaryKey())? Yii::t('app', 'Yes') : Yii::t('app', 'No'), '#toggleRole', array(
					'class'=>'togglelink',
					'data-togglelinkurl'=>$this->createUrl('toggleRolesForUser', array('idUser'=>$user->getPrimaryKey(), 'idRole'=>$role->getPrimaryKey())),
				));?>
			</div>
		</div>
	<?php } ?>
	
</div>
<div class="modal-footer">
	<?php echo  CHtml::link('Volver', '#closeUserView', array('data-dismiss'=>"modal", 'class'=>'btn btn-primary'));?>
</div>

<?php 
$this->endWidget(); 

<h4>Actualizar Informes</h4>

	<?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
	    'id'=>'report-form',
	    'htmlOptions'=>array('class'=>'well', 'enctype'=>'multipart/form-data'),
	)); ?>

    <?php echo CHtml::errorSummary($model);?>

    <?php echo $form->textFieldRow($model, 'report', array('class'=>'span5')); ?>

    <?php echo $form->textAreaRow($model, 'description', array('class'=>'span5')); ?>

    </br>
    </br>


</br></br>
<div>
    <?php $this->widget('bootstrap.widgets.TbButton', array(
        'label' => 'Cancelar',
        'url' => array('index'),
    )); ?>
    <?php $this->widget('bootstrap.widgets.TbButton', array(
        'buttonType'=>'submit', 
        'label'=>'Guardar',
        'htmlOptions' => array('class'=>'btn btn-primary'),
    )); ?>
</div>

<?php $this->endWidget(); ?>
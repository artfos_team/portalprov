<?php 
    echo CHtml::button('Volver', array(
        'size'=>'small',
        'name' => 'btnBack',
        'class'=>'pull-right',
        'style' => 'width:100px;',
        'onclick' => "history.go(-1)",
        )
    );
?>

<div style="text-align:center">
	<h3><?php echo 'Factura: '.$model->getInvoiceNumbre(); ?></h3>
</div>

<div style="width:100%; margin:0 auto;">
	<?php $invoice = $this->renderPartial('_invoice', array('model'=>$model), true); ?>
	<?php $this->widget('bootstrap.widgets.TbTabs', array(
		'type' => 'tabs', // 'tabs' or 'pills'
		'tabs' => array(
			array('label' => 'Factura', 'content' => $invoice, 'active' => true),
		),
    )); ?>
</div>




<?php
$this->widget('bootstrap.widgets.TbButtonGroup',array(
        'buttons'=>array(
            array('label'=> Yii::t('app', 'Verificar host'), 'type'=>'inverse', 
                'url'=>array('ping')),
            array('label'=> Yii::t('app', 'Cambiar contraseña'), 'type'=>'inverse', 
                'htmlOptions'=>array('
                            data-toggle'=>'modal', 
                            'data-target'=>'#changePsw', 
                            'id'=>'changePswB'
                        )),
            
        ),
            
    ));?>
<?php   
            if($import)
            {
                $this->widget('bootstrap.widgets.TbButton',array('label'=> Yii::t('app', 'Importar facturas (Excel)'), 'type'=>'inverse', 
                'htmlOptions'=>array('
                            data-toggle'=>'modal', 
                            'data-target'=>'#importExcel', 
                            'id'=>'importExcelB'
                        )));
            } 
             ?>

<?php 
$this->beginWidget('bootstrap.widgets.TbModal', array('id' => 'importExcel')); ?>
        <?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
            'id'=>'addNew',
            'enableAjaxValidation'=>true,
            'action' => 'importExcel',
            'htmlOptions'=>array('enctype'=>'multipart/form-data'),
        )); ?>

        <div class="modal-header">
            <a class="close" data-toggle="modal" data-target="#importExcel">&times;</a>
            <h4>Seleccione el Excel a importar</h4>
        </div>
     
        <div class="modal-body">
            
            <input type="file" name="excelDir" accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel" required="true" /> 

            
        </div>
     
        <div class="modal-footer">
            <?php $this->widget('bootstrap.widgets.TbButton', array(
                'label' => 'Cancelar',
                'url' => '#',
                'htmlOptions' => array('data-dismiss' => 'modal', 'class'=>'btn'),
            )); ?>
            <?php $this->widget('bootstrap.widgets.TbButton', array(
                'buttonType'=>'submit',
                'url'=>array('importExcel'),
                'label'=>'Importar',
                'htmlOptions' => array('class'=>'btn btn-primary', 'id'=>uniqid('btn-import-excel')),
                
            )); ?>
        </div> 


    <?php $this->endWidget(); ?>
<?php $this->endWidget(); ?>

<?php 
$this->beginWidget('bootstrap.widgets.TbModal', array('id' => 'changePsw')); ?>
        <?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
            'id'=>'changePsw',
            'enableAjaxValidation'=>true,
            'action' => 'changePsw',
            'htmlOptions'=>array('enctype'=>'multipart/form-data'),
        )); ?>

        <div class="modal-header">
            <a class="close" data-toggle="modal" data-target="#changePsw">&times;</a>
            <h4>Actualizar contraseña de SAP</h4>
        </div>
     
        <div class="modal-body">
            Contraseña:
            <input type="password" name="psw" required="true" /> 

            
        </div>
     
        <div class="modal-footer">
            <?php $this->widget('bootstrap.widgets.TbButton', array(
                'label' => 'Cancelar',
                'url' => '#',
                'htmlOptions' => array('data-dismiss' => 'modal', 'class'=>'btn'),
            )); ?>
            <?php $this->widget('bootstrap.widgets.TbButton', array(
                'buttonType'=>'submit',
                'url'=>array('changePsw'),
                'label'=>'Cambiar Psw',
                'htmlOptions' => array('class'=>'btn btn-primary', 'id'=>uniqid('btn-import-change-psw')),
                
            )); ?>
        </div> 


    <?php $this->endWidget(); ?>
<?php $this->endWidget(); ?>
   
<div style="text-align:center">
    <h3>Parametros de SAP</h3>
</div>

<div style="width:100%; margin:0 auto;">
    <?php
        $columns = array(

            'ashost'=>array(
                'class' => 'ETbEditableColumn',
                'name' => 'ashost',
                'header' => 'Host',
                'editable' => array(
                    'url' => $this->createUrl('ChangeAttributeSapParameters'),
                    'attribute' => 'ashost',
                    'placement' => 'right',
                    'inputclass' => 'span2',
                    'title' => 'Ingresar Host'
                ),
            ),

            'sysnr'=>array(
                'class' => 'ETbEditableColumn',
                'name' => 'sysnr',
                'header' => 'SYSNR',
                'editable' => array(
                    'url' => $this->createUrl('changeAttributeSapParameters'),
                    'attribute' => 'sysnr',
                    'placement' => 'top',
                    'inputclass' => 'span2',
                    'title' => 'Ingresar SYSNR'
                ),
            ),
            'client'=>array(
                'class' => 'ETbEditableColumn',
                'name' => 'client',
                'header' => 'Cliente',
                'editable' => array(
                    'url' => $this->createUrl('changeAttributeSapParameters'),
                    'attribute' => 'client',
                    'placement' => 'top',
                    'inputclass' => 'span2',
                    'title' => 'Ingresar Cliente'
                ),
            ),
            'lang'=>array(
                'class' => 'ETbEditableColumn',
                'name' => 'lang',
                'header' => 'Lenguaje',
                'editable' => array(
                    'url' => $this->createUrl('changeAttributeSapParameters'),
                    'attribute' => 'lang',
                    'placement' => 'top',
                    'inputclass' => 'span2',
                    'title' => 'Ingresar Lenguaje'
                ),
            ),
            'user'=>array(
                'class' => 'ETbEditableColumn',
                'name' => 'user',
                'header' => 'Usuario',
                'editable' => array(
                    'url' => $this->createUrl('changeAttributeSapParameters'),
                    'attribute' => 'user',
                    'placement' => 'top',
                    'inputclass' => 'span2',
                    'title' => 'Ingresar Usuario'
                ),
            ),
            'function'=>array(
                'class' => 'ETbEditableColumn',
                'name' => 'function',
                'header' => 'Función',
                'editable' => array(
                    'url' => $this->createUrl('changeAttributeSapParameters'),
                    'attribute' => 'function',
                    'placement' => 'left',
                    'inputclass' => 'span2',
                    'title' => 'Insertar Función'
                ),
            ),
        );

        $this->widget('bootstrap.widgets.TbExtendedGridView', array(
            'id' => 'invoice-grid',
            'dataProvider' => $model->search(),
            'filter' => $model,
            'ajaxUrl'=>array('index'),
            'fixedHeader' => true,
            'headerOffset' => 40,
            'type' => 'table striped bordered condensed',
            'rowCssClassExpression'=>"'invoiceRow'",
            'columns' => array_merge(array_values($columns),
                array( 
                    /*array(
                        'class' => 'bootstrap.widgets.TbButtonColumn',
                        'header'=>'Acciones',
                        'htmlOptions'=>array('style'=>'width:150px; text-align:center;'),
                        'buttons'=>array(
                            'view'=>array(
                                'icon'=>'icon-eye-open',
                                'label'=>Yii::t('app', 'View'),
                                'options'=>array('class'=>'btn btn-mini'),
                                'url'=>'CHtml::normalizeUrl(array("view", "invoiceStatus"=>$data->primaryKey))',
                            ), 
                        ),
                        'template'=>'',
                    )*/
                )
            ),
        )); 
    ?>
    <?php $this->widget('application.components.PageSizeComponent'); ?>
</div>
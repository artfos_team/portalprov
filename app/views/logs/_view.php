<?php
/* @var $this LogsController */
/* @var $data Logs */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('idLog')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->idLog), array('view', 'id'=>$data->idLog)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('idUser')); ?>:</b>
	<?php echo CHtml::encode($data->idUser); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('levelLog')); ?>:</b>
	<?php echo CHtml::encode($data->levelLog); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('dateLog')); ?>:</b>
	<?php echo CHtml::encode($data->dateLog); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('commentaryLog')); ?>:</b>
	<?php echo CHtml::encode($data->commentaryLog); ?>
	<br />


</div>
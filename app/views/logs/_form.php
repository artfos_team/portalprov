<?php
/* @var $this LogsController */
/* @var $model Logs */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'logs-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'idUser'); ?>
		<?php echo $form->textField($model,'idUser'); ?>
		<?php echo $form->error($model,'idUser'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'levelLog'); ?>
		<?php echo $form->textField($model,'levelLog'); ?>
		<?php echo $form->error($model,'levelLog'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'dateLog'); ?>
		<?php echo $form->textField($model,'dateLog'); ?>
		<?php echo $form->error($model,'dateLog'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'commentaryLog'); ?>
		<?php echo $form->textField($model,'commentaryLog',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'commentaryLog'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->

<div style="text-align:center">
    <h3>Logs</h3>
</div>

<div style="width:100%; margin:0 auto;">
    <?php
        $columns = array(
            
            'idLog'=>array(
                'htmlOptions' => array('width' => 25),
                'name' => 'idLog',
                'header' => 'ID',
            ),

            'idUser'=>array(
                'class' => 'CDataColumn',
                'name' => 'idUser',
                'value'=>'$data->getUserName()',
                'header' => 'Usuario',
                'filter' => CHtml::ListData(User::model()->findAll(array('order'=>'name ASC')), 'idUser', 'name'),
            ),

            'commentaryLog'=>array(
                'name' => 'commentaryLog',
                'header' => 'Comentario'
            ),
            
            'levelLog'=>array(
                'class' => 'CDataColumn',
                'name' => 'levelLog',
                'value'=>'$data->levelDescription()',
                'header' => 'Nivel',
                'filter' => false
            ),

            'dateLog'=>array(
                'name' => 'dateLog',
                'header' => 'Fecha',
                'sortable'=>true,
                'filter'=>$this->widget('zii.widgets.jui.CJuiDatePicker', array(
                    'model'=>$model,
                    'attribute'=>'dateLog',
                    'htmlOptions' => array(
                        'id' => 'date_log_search',
                    ), 
                    'options' => array(
                        'showAnim'=>'fold',
                        'dateFormat' => 'yy-mm-dd',
                    )
                ), true)
            ),

        );

        $this->widget('bootstrap.widgets.TbExtendedGridView', array(
            'id' => 'log-grid',
            'dataProvider' => $model->search(),
            'filter' => $model,
            'ajaxUrl'=>array('index'),
            'afterAjaxUpdate'=>"function(){jQuery('#date_log_search').datepicker({'dateFormat': 'yy-mm-dd'})}",
            'fixedHeader' => true,
            'headerOffset' => 40,
            'type' => 'table striped bordered condensed',
            'responsiveTable' => true,
            'columns' => array_merge(array_values($columns),
                array( 
                    
                )
            ),
        )); 
    ?>
    <?php $this->widget('application.components.PageSizeComponent'); ?>
</div>
<script>
    $( document ).ready(function() {
    jQuery('#date_logs_search').datepicker({'dateFormat': 'yy-mm-dd'});
});
</script>
<style>
.ui-datepicker{
    background-color: #f3f3f3;
    width: 200px;
    color:#000;
    height: 220px;
    display:none;
}



.ui-datepicker  th {
    color:#000;
}

.ui-datepicker td {
    width: 20px;
    height: 25px;
}

.ui-datepicker td a{
    width: 100%;
    height: 110%;
    font-size: 12px;
    text-align: center;
    padding-top: 5px; 
    
}

.ui-state-default {
     background-color: transparent !important;
     border: none !important;
     border-radius: 5px;
}

.ui-state-default:hover {
     background-color: #0081c2 !important;
     color: #FFF;
}

.ui-state-active {
    background-color: #9336d1 !important;
}
</style>

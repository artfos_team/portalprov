<?php
/* @var $this LogsController */
/* @var $model Logs */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'idLog'); ?>
		<?php echo $form->textField($model,'idLog'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'idUser'); ?>
		<?php echo $form->textField($model,'idUser'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'levelLog'); ?>
		<?php echo $form->textField($model,'levelLog'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'dateLog'); ?>
		<?php echo $form->textField($model,'dateLog'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'commentaryLog'); ?>
		<?php echo $form->textField($model,'commentaryLog',array('size'=>60,'maxlength'=>100)); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->
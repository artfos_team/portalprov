<?php $this->widget('bootstrap.widgets.TbButtonGroup',array(
        'buttons'=>array(
            array('label'=> Yii::t('app', 'Add Category Retain'), 'type'=>'inverse', 
                'url'=>array('create')),),
            
    ));?>
<div style="text-align:center">
    <h3>Categoría a retener</h3>
</div>

<div style="width:100%; margin:0 auto;">
    <?php
        $columns = array(
            'idRetentionRegime'=>array(
                'class' => 'CDataColumn',
                'name' => 'idRetentionRegime',
                'value'=>'$data->getRetentionRegime()',
                'filter' => CHtml::ListData(RetentionRegime::model()->findAll(array('order' => 'retentionRegime ASC')), 'idRetentionRegime', 'retentionRegime')
            ),
            'name'=>array(
                'class' => 'ETbEditableColumn',
                'name' => 'categoryRetain',
                'editable' => array(
                    'url' => $this->createUrl('changeAttributeManual'),
                    'placement' => 'right',
                    'inputclass' => 'span2',
                    'title' => 'Insertar Nombre'
                ),
            ),
            'codeERP'=>array(
                'class' => 'ETbEditableColumn',
                'name' => 'codeERP',
                'editable' => array(
                    'url' => $this->createUrl('changeAttributeManual'),
                    'placement' => 'right',
                    'inputclass' => 'span2',
                    'title' => 'Insertar Código'
                ),
            ),/* 
            'ib'=>array(
                'class' => 'ETbEditableColumn',
                'name' => 'ib',
                'editable' => array(
                    'url' => $this->createUrl('changeAttributeManual'),
                    'placement' => 'right',
                    'inputclass' => 'span2',
                    'title' => 'Insertar'
                ),
            ),  
            'gain'=>array(
                'class' => 'ETbEditableColumn',
                'name' => 'gain',
                'editable' => array(
                    'url' => $this->createUrl('changeAttributeManual'),
                    'placement' => 'right',
                    'inputclass' => 'span2',
                    'title' => 'Insertar'
                ),
            ),
            'suss'=>array(
                'class' => 'ETbEditableColumn',
                'name' => 'suss',
                'editable' => array(
                    'url' => $this->createUrl('changeAttributeManual'),
                    'placement' => 'right',
                    'inputclass' => 'span2',
                    'title' => 'Insertar'
                ),
            ), 
            'vat'=>array(
                'class' => 'ETbEditableColumn',
                'name' => 'vat',
                'editable' => array(
                    'url' => $this->createUrl('changeAttributeManual'),
                    'placement' => 'right',
                    'inputclass' => 'span2',
                    'title' => 'Insertar'
                ),
            ),*/  
            'codeERP'=>array(
                'class' => 'ETbEditableColumn',
                'name' => 'codeERP',
                'editable' => array(
                    'url' => $this->createUrl('changeAttributeManual'),
                    'placement' => 'right',
                    'inputclass' => 'span2',
                    'title' => 'Insertar'
                ),
            ),/*
            'codeERP2'=>array(
                'class' => 'ETbEditableColumn',
                'name' => 'codeERP2',
                'editable' => array(
                    'url' => $this->createUrl('changeAttributeManual'),
                    'placement' => 'right',
                    'inputclass' => 'span2',
                    'title' => 'Insertar'
                ),
            ),*/
            'indicadorRet'=>array(
                'class' => 'ETbEditableColumn',
                'name' => 'indicadorRet',
                'editable' => array(
                    'url' => $this->createUrl('changeAttributeManual'),
                    'placement' => 'right',
                    'inputclass' => 'span2',
                    'title' => 'Insertar'
                ),
            ),
            'idJurisdictions'=>array(
                'class' => 'CDataColumn',
                'name' => 'idJurisdictions',
                'value'=>'$data->getJurisdiction()',
                'filter' => CHtml::ListData(Jurisdictions::model()->findAll(array('order' => 'jurisdictions ASC')), 'idJurisdictions', 'jurisdictions')
            ),
       );

        $this->widget('bootstrap.widgets.TbExtendedGridView', array(
            'id' => 'provider-grid',
            'dataProvider' => $model->search(),
            'filter' => $model,
            'ajaxUrl'=> array('index'),
            'fixedHeader' => true,
            'headerOffset' => 40,
            'type' => 'table striped bordered condensed',
            'responsiveTable' => true,
            'columns' => array_merge(
                array_values($columns),
                array(array(
                    'class' => 'bootstrap.widgets.TbButtonColumn',
                    'header'=>'Acciones',
                    'htmlOptions'=>array('style'=>'width:150px; text-align:center;'),
                    'buttons'=>array(
                        'delete'=>array(
                            'icon'=>'icon-trash',
                            'label'=>'Eliminar',
                            //'visible'=> 'app()->rbac->checkAccess("company","viewCalification")',
                            'options'=>array('class'=>'btn btn-mini'),
                            'url'=>'app()->controller->createUrl("delete",array(
                                "id"=>$data->primaryKey, "key"=>uKey("delete".$data->primaryKey)))'
                            //'url'=>'CHtml::normalizeUrl(array("deleteDocument", "document"=>$data->primaryKey))',
                        ),
                    ),
                    
                    'template'=>'{delete}',
                ))
            ),
        )); 
    ?>

    <?php $this->widget('application.components.PageSizeComponent'); ?>
</div>


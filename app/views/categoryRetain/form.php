<h4>Agregar categoría a retener</h4>


	<?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
	    'id'=>'document-form',
	    'htmlOptions'=>array('class'=>'well', 'enctype'=>'multipart/form-data'),
	)); ?>


<?php //CAMPOS
    echo $form->dropDownListRow($model, 'idRetentionRegime', CHtml::listData(RetentionRegime::model()->findAll(array('order'=>'retentionRegime')), 'idRetentionRegime', 'retentionRegime'), 
    array('class'=>'span5', 'empty'=>Yii::t('app','Select an option'))); 
        echo $form->textFieldRow($model, 'categoryRetain', array('class'=>'span6', 'placeholder' =>'Descripción...'));?>
        <script src="http://code.jquery.com/jquery-latest.min.js" type="text/javascript"></script>
        <script type="text/javascript">
            /*$(document).ready(function(){
                $("#CategoryRetain_ib").hide();
            });*/
            ///
            $(document).ready(function(){
                document.getElementById("gainElementCodeERP").disabled = true;
                document.getElementById("sussElementCodeERP").disabled = true;
                document.getElementById("ivaElementCodeERP").disabled = true;
                document.getElementById("gainElementIndicadorRet").disabled = true;
                document.getElementById("ibElementIndicadorRet").disabled = true;
                document.getElementById("sussElementIndicadorRet").disabled = true;
                document.getElementById("ivaElementIndicadorRet").disabled = true;
                $("#ib").hide();
                $("#gain").hide();
                $("#suss").hide();
                $("#iva").hide();

                $('#CategoryRetain_idRetentionRegime').on('change', function() {
                    // console.log($(this)[0])
                    if ($(this).val() == 11){
                        $("#ib").show();
                        $("#gain").hide();
                        $("#suss").hide();
                        $("#iva").hide();
                        document.getElementById("ibElementIndicadorRet").disabled = false;
                        document.getElementById("gainElementIndicadorRet").disabled = true;
                        document.getElementById("sussElementIndicadorRet").disabled = true;
                        document.getElementById("ivaElementIndicadorRet").disabled = true;
                        document.getElementById("gainElementCodeERP").disabled = true;
                        document.getElementById("sussElementCodeERP").disabled = true;
                        document.getElementById("ivaElementCodeERP").disabled = true;
                    } else if ($(this).val() == 10) {
                        <?php
                            // $test = $model->suss;
                            // echo $test;
                            // $model['suss'] = 1;
                        ?>
                        $("#suss").show();
                        $("#gain").hide();
                        $("#ib").hide();
                        $("#iva").hide();
                        document.getElementById("sussElementCodeERP").disabled = false;
                        document.getElementById("sussElementIndicadorRet").disabled = false;

                        document.getElementById("gainElementIndicadorRet").disabled = true;
                        document.getElementById("ibElementIndicadorRet").disabled = true;
                        document.getElementById("ivaElementIndicadorRet").disabled = true;
                        document.getElementById("gainElementCodeERP").disabled = true;
                        document.getElementById("ivaElementCodeERP").disabled = true;
                    } else if ($(this).val() == 5 || $(this).val() == 6) {
                        $("#iva").show();
                        $("#gain").hide();
                        $("#ib").hide();
                        $("#suss").hide();
                        document.getElementById("ivaElementCodeERP").disabled = false;
                        document.getElementById("ivaElementIndicadorRet").disabled = false;
                        document.getElementById("gainElementCodeERP").disabled = true;
                        document.getElementById("sussElementCodeERP").disabled = true;
                        document.getElementById("gainElementIndicadorRet").disabled = true;
                        document.getElementById("ibElementIndicadorRet").disabled = true;
                        document.getElementById("sussElementIndicadorRet").disabled = true;

                    } else if($(this).val() == '') {
                        document.getElementById("gainElementCodeERP").disabled = true;
                        document.getElementById("sussElementCodeERP").disabled = true;
                        document.getElementById("ivaElementCodeERP").disabled = true;
                        document.getElementById("gainElementIndicadorRet").disabled = true;
                        document.getElementById("ibElementIndicadorRet").disabled = true;
                        document.getElementById("sussElementIndicadorRet").disabled = true;
                        document.getElementById("ivaElementIndicadorRet").disabled = true;
                        $("#ib").hide();
                        $("#gain").hide();
                        $("#suss").hide();
                        $("#iva").hide();
                    } else {
                        $("#gain").show();
                        $("#suss").hide();
                        $("#ib").hide();
                        $("#iva").hide();
                        document.getElementById("gainElementCodeERP").disabled = false;
                        document.getElementById("gainElementIndicadorRet").disabled = false;
                        document.getElementById("sussElementIndicadorRet").disabled = true;
                        document.getElementById("ibElementIndicadorRet").disabled = true;
                        document.getElementById("ivaElementIndicadorRet").disabled = true;
                        document.getElementById("sussElementCodeERP").disabled = true;
                        document.getElementById("ivaElementCodeERP").disabled = true;
                    }
                });
            });
        </script>
        <br>
        <div id="ib">Ingresos Brutos:
        <?php
        /* echo "Si " . $form->radioButton($model, 'ib', array(
            'value'=>1,
            'uncheckValue'=>null
        ));
        echo " No " . $form->radioButton($model, 'ib', array(
            'value'=>0,
            'uncheckValue'=>null
        )); */
        echo "\n" . " . " .  $form->dropDownList($model, 'idJurisdictions', CHtml::listData(Jurisdictions::model()->findAll(
        array('order'=>'jurisdictions')),'idJurisdictions', 'jurisdictions'),
        array('class'=>'span4', 'empty'=>Yii::t('app','Seleccionar una Jurisdicción')));
        echo "\n" . " . " . $form->textField($model, 'indicadorRet', array('class'=>'span3', 'placeholder' =>'Indicador retención...','id'=>'ibElementIndicadorRet'));
        ?>
        </div>
        <!--<br><br>-->
        <div id="gain">Ganancias: 
        <?php
        /* echo "Si " . $form->radioButton($model, 'gain', array(
            'value'=>1,
            'uncheckValue'=>null
        ));
        echo " No " . $form->radioButton($model, 'gain', array(
            'value'=>0,
            'uncheckValue'=>null
        )); */
        echo "\n" . " . " . $form->textField($model, 'codeERP', array('class'=>'span4', 'placeholder' =>'Cod. Categoría retención...','id'=>'gainElementCodeERP'));
        echo "\n" . " . " . $form->textField($model, 'indicadorRet', array('class'=>'span3', 'placeholder' =>'Indicador retención...','id'=>'gainElementIndicadorRet'));
        ?>
        </div>
        <!--<br><br>-->
        <div id="suss">SUSS: 
        <?php
        /*$testA12 = $model->suss;
        echo "Si " . $form->radioButton($model, 'suss', array(
            'value'=>1,
            'uncheckValue'=>null
        ));
        echo " No " . $form->radioButton($model, 'suss', array(
            'value'=>0,
            'uncheckValue'=>null
        ));*/
        echo "\n" . " . " . $form->textField($model, 'codeERP', array('class'=>'span4', 'placeholder' =>'Cod. Categoría retención...','id'=>'sussElementCodeERP'));
        echo "\n" . " . " . $form->textField($model, 'indicadorRet', array('class'=>'span3', 'placeholder' =>'Indicador retención...','id'=>'sussElementIndicadorRet'));
        ?>
        </div>
        <!--<br><br>-->
        <div id="iva">IVA: 
        <?php
        /* echo "Si " . $form->radioButton($model, 'vat', array(
            'value'=>1,
            'uncheckValue'=>null
        ));
        echo " No " . $form->radioButton($model, 'vat', array(
            'value'=>0,
            'uncheckValue'=>null
        )); */
        echo "\n" . " . " . $form->textField($model, 'codeERP', array('class'=>'span4', 'placeholder' =>'Cod. Categoría retención...','id'=>'ivaElementCodeERP'));
        echo "\n" . " . " . $form->textField($model, 'indicadorRet', array('class'=>'span3', 'placeholder' =>'Indicador retención...','id'=>'ivaElementIndicadorRet'));
        ?>
        </div>
<div>
    <?php $this->widget('bootstrap.widgets.TbButton', array(
        'label' => 'Cancelar',
        'url' => array('/categoryRetain/index'),
    )); ?>
    <?php $this->widget('bootstrap.widgets.TbButton', array(
        'buttonType'=>'submit', 
        'label'=>'Guardar',
        'htmlOptions' => array('class'=>'btn btn-primary'),
    )); ?>
</div>

<?php $this->endWidget(); ?>
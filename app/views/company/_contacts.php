<div>
	
	<div class="modal-header">
	    <a class="close" data-dismiss="modal">&times;</a>
	    <h4> <?php echo 'Empresa: '.$company->company;?> </h4>
	</div>

	<div class="modal-body">
		<div class="buttons">
			<?php $this->widget('bootstrap.widgets.TbButtonGroup',array(
				'size'=>'small',
				'htmlOptions'=>array('class'=>'pull-right'),
				'buttons'=>array(
					array('label'=>'Agregar contacto', 'type'=>'inverse', 
						'htmlOptions'=>array('data-toggle'=>'modal', 
							'data-target'=>'#addContactModal'.$randNumb, 'id'=>'addContact'),
						'visible'=>app()->rbac->checkAccess("companyContact", "addContact")),
				),
			));?></br>
		<h4>Contactos</h4>
		</div>

		</br></br>

		<?php $this->widget('bootstrap.widgets.TbExtendedGridView', array(
			'id' => 'contact-grid-'.$randNumb,
			'dataProvider' => $contact->search(),
			'type' => 'striped condensed',
			'responsiveTable' => true,
			'ajaxUrl'=>array('contactView', 'company'=>$company->idCompany, 'randNumb'=>$randNumb),
			'selectableRows'=>0,
			'columns' => array(
				array(
					'class' => 'ETbEditableColumn',
					'name' => 'name',
					'editable' => array(
						'url' => $this->createUrl('companyContact/changeAttributeContact'),
						'placement' => 'right',
						'inputclass' => 'span3',
						'apply'=>app()->rbac->checkAccess('companyContact', 'changeAttributeContact')
					)
				),
				array(
					'class' => 'ETbEditableColumn',
					'name' => 'lastName',
					'editable' => array(
						'url' => $this->createUrl('companyContact/changeAttributeContact'),
						'placement' => 'right',
						'inputclass' => 'span3',
						'apply'=>app()->rbac->checkAccess('companyContact', 'changeAttributeContact')
					)
				),
				array(
					'class' => 'ETbEditableColumn',
					'name' => 'position',
					'editable' => array(
						'url' => $this->createUrl('companyContact/changeAttributeContact'),
						'placement' => 'right',
						'inputclass' => 'span3',
						'apply'=>app()->rbac->checkAccess('companyContact', 'changeAttributeContact')
					)
				),
				array(
					'class' => 'ETbEditableColumn',
					'name' => 'email',
					'editable' => array(
						'url' => $this->createUrl('companyContact/changeAttributeContact'),
						'placement' => 'right',
						'inputclass' => 'span3',
						'apply'=>app()->rbac->checkAccess('companyContact', 'changeAttributeContact')
					)
				),
				array(
					'class' => 'ETbEditableColumn',
					'name' => 'telephone',
					'editable' => array(
						'url' => $this->createUrl('companyContact/changeAttributeContact'),
						'placement' => 'right',
						'inputclass' => 'span3',
						'apply'=>app()->rbac->checkAccess('companyContact', 'changeAttributeContact')
					)
				),
				/*array(
					'class' => 'ETbEditableColumn',
					'name' => 'idCompanyAddress',
					'filter'=> $companyAddressList = CHtml::listData(CompanyAddress::model()->findAll(array(
						'condition'=>'idCompany = :idC', 'params'=>array(':idC'=>$company->idCompany))),
						'idCompanyAddress', 'address'),
					'value'=>'$data->getAddress()',
					'editable' => array(
						'url' => $this->createUrl('companyContact/changeAttributeContact'),
						'type'      => 'select',
						'attribute' => 'idCompanyAddress',
				        'source'    => $companyAddressList,
		        		'placement' => 'left',
		        		'apply'=>app()->rbac->checkAccess('companyContact', 'changeAttributeContact')
					),
				),*/
				array(
					'class' => 'bootstrap.widgets.TbButtonColumn',
					'header'=>'Acciones',
					'headerHtmlOptions'=>array('style'=>'text-align:center; width:50px!important;'),
					'htmlOptions'=>array('style'=>'text-align:center;'),
					'buttons'=>array(
						'delete'=>array(
							'options'=>array('class'=>'btn btn-mini'),
							'visible'=>'app()->rbac->checkAccess("companyContact", "delete")'
						),
					),
					'deleteButtonUrl'=>'app()->controller->createUrl("/companyContact/delete",
						array("id"=>$data->getPrimaryKey(), "key"=>uKey("deleteCompCont".$data->primaryKey)))',
					'template'=>'<div class="btn-group">{delete}</div>',
				),
		))); ?>


		<?php cs()->registerScript(__FILE__ . '_addNewContacdt', '
				$("#addContactModal1' . $randNumb . '").on("show", function() {
					var $this = $(this);
					$("#addNew' . $randNumb . ' input").val("");
					$("#addNew' . $randNumb . ' .help-block").css("display", "none");
				});
			');

		$this->beginWidget('bootstrap.widgets.TbModal', array('id' => 'addContactModal'.$randNumb)); ?>
			<?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
			    'id'=>'addNew' . $randNumb,
			    'enableAjaxValidation'=>true,
			)); ?>

			<div class="modal-header">
		        <a class="close" data-toggle="modal" data-target="#addContactModal<?php echo $randNumb?>">&times;</a>
		        <h4>Nuevo Contacto</h4>
		        <h4><?php echo 'Empresa: '.$company->company;?></h4>
		    </div>
		 
		    <div class="modal-body">
			    <div class="row-fluid">
			    	<div class="span6">
			    	<?php $sexList = array(0=>'Femenino', 1=>'Masculino'); ?>
					<?php echo $form->textFieldRow($newContact, 'name', array('class'=>'span8')); ?>
					<?php echo $form->textFieldRow($newContact, 'lastName', array('class'=>'span8')); ?>
					<?php echo $form->textFieldRow($newContact, 'position', array('class'=>'span8')); ?>
					<?php echo $form->dropDownListRow($newContact, 'sex', $sexList,
	        				array('class'=>'span8', 'empty'=>'Seleccionar sexo')); ?>
					</div>

					<div class="span6">
					<?php echo $form->textFieldRow($newContact, 'email', array('class'=>'span8')); ?>
					<?php echo $form->textFieldRow($newContact, 'telephone', array('class'=>'span8')); ?>
					<?php echo $form->dropDownListRow($newContact, 'idCompanyAddress', 
						CHtml::listData(CompanyAddress::model()->findAll(array('condition'=>'idCompany = :idC', 'params'=>array(':idC'=>$company->primaryKey))), 'idCompanyAddress', 'address'), 
							array('class'=>'span8', 'id'=>'contact-address-dd')); ?>
					</div>
				</div>
			</div>
		 
		    <div class="modal-footer">
		    	<?php $this->widget('bootstrap.widgets.TbButton', array(
					'label' => 'Cancelar',
					'url' => '#',
					'htmlOptions' => array('data-dismiss' => 'modal', 'class'=>'btn'),
				)); ?>
				<?php $this->widget('bootstrap.widgets.TbButton', array(
					'buttonType'=>'ajaxSubmit',
					'url'=>array('contactView', 'company'=>$company->idCompany, 'randNumb'=>$randNumb),
					'label'=>'Guardar',
					'htmlOptions' => array('class'=>'btn btn-primary', 'id'=>uniqid('btn-save-contact-')),
					'ajaxOptions'=>array(
						'dataType'=>'json',
						'success'=>'js: function(d)
						{
							if(d.success)
							{
								$("#addContactModal' . $randNumb . '").modal("hide");						
								$.fn.yiiGridView.update("contact-grid-'.$randNumb.'");
								$.fnResetForm($("#addNew' . $randNumb .'"));	
							}
							else
							{
								$.each(d, function(key, val) {
				                   var $key = $("#"+key+"_em_");
				                   $key.text(val);                                                    
				                   $key.show();
				                });
							}
						}'
				)
				)); ?>
		    </div> 

		<?php $this->endWidget(); ?>
	<?php $this->endWidget(); ?>

</div>

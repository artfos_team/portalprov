
<h4><?php echo 'Calificar compañia'; ?></h4>

	<?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
	    'id'=>'user-form',
	    'htmlOptions'=>array('class'=>'well', 'enctype'=>'multipart/form-data'),
	)); ?>

    <?php echo CHtml::errorSummary($model);?>
    <?php echo "Calificacion"; 
    echo "<br>";?>
    <?php echo $form->dropDownList($model, 'calification', array('Seleccione...',1,2,3,4,5), array('class'=>'span5')); ?>
    
    <?php echo $form->textAreaRow($model, 'reasonCalification', array('class'=>'span5', 'placeholder' => 'Razon:', 'rows' => 5 )); ?>
    <?php //echo $form->dropDownList($model,'calification', array(1,2,3,4,5), array('prompt'=>'Seleccione...')); ?>

</br></br>
<div>
    <?php $this->widget('bootstrap.widgets.TbButton', array(
        'label' => 'Cancelar',
        'url' => array('/company/index'),
    )); ?>
    <?php $this->widget('bootstrap.widgets.TbButton', array(
        'buttonType'=>'submit', 
        'label'=>'Calificar',
        'htmlOptions' => array('class'=>'btn btn-primary'),
    )); ?>
</div>

<?php $this->endWidget(); ?>

<div class="modal-header">
    <a class="close" data-dismiss="modal">&times;</a>
    <h4> <?php echo 'Empresa: '.$company->company;?> </h4>
</div>

<div class="modal-body" style="max-height: 310px; overflow: scroll;">	
	<div class="buttons">	
		<?php $this->widget('bootstrap.widgets.TbButtonGroup',array(
			'size'=>'small',
			'htmlOptions'=>array('class'=>'pull-right'),
			'buttons'=>array(
				array('label'=>'Agregar direccion', 'type'=>'inverse', 
					'htmlOptions'=>array('data-toggle'=>'modal', 
						'data-target'=>'#addAddressModal'.$randNumb, 'id'=>'addAddress'.$randNumb)),
			),
		));?></br>
		
		<h4>Direcciones</h4>	
	</div>

	<?php $this->widget('bootstrap.widgets.TbExtendedGridView', array(
		'id' => 'address-grid-'.$randNumb,
		'dataProvider' => $address->search(),
		'type' => 'striped condensed',
		'responsiveTable' => true,
		'selectableRows'=>0,
		'ajaxUrl'=>array('addressView', 'company'=>$company->idCompany, 'randNumb'=>$randNumb),
		'columns' => array(
			array(
				'class' => 'ETbEditableColumn',
				'name' => 'address',
				'headerHtmlOptions'=>array('style'=>'text-align:right;'),
				'editable' => array(
					'url' => $this->createUrl('companyAddress/changeAttributeAddress'),
					'placement' => 'right',
					'inputclass' => 'span3',
				)
			),
			array(
				'class' => 'ETbEditableColumn',
				'name' => 'zipCode',
				'headerHtmlOptions'=>array('style'=>'text-align:right;'),
				'htmlOptions'=>array('style'=>'width:60px; text-align:right;'),
				'editable' => array(
					'url' => $this->createUrl('companyAddress/changeAttributeAddress'),
					'placement' => 'right',
					'inputclass' => 'span3',
				)
			),
			array(
				'class' => 'ETbEditableColumn',
				'name' => 'idCountry',
				'filter'=> $countryList = CHtml::listData(Country::model()->findAll(array('order'=>'country')), 'idCountry', 'country'),
				'sortable'=>true,						
				'value'=>'$data->getCountry()',				
				'editable' => array(
					'url' => $this->createUrl('companyAddress/changeAttributeAddress'),
					'type'      => 'select',
			        'attribute' => 'idCountry',
			        'source'    => $countryList,
		    		'placement' => 'right',
				)
			),
			array(
				'class' => 'ETbEditableColumn',
				'name' => 'idProvince',
				'filter'=> $provinceList = CHtml::listData(Province::model()->findAll(array('order'=>'province')), 'idProvince', 'province'),
				'sortable'=>true,						
				'value'=>'$data->getProvince()',				
				'editable' => array(
					'url' => $this->createUrl('companyAddress/changeAttributeAddress'),
					'type'      => 'select',
			        'attribute' => 'idProvince',
			        'source'    => $provinceList,
		    		'placement' => 'right',
				)
			),
			array(
				'class' => 'ETbEditableColumn',
				'name' => 'idCity',
				'filter'=> $cityList = CHtml::listData(City::model()->findAll(array('order'=>'city')), 'idCity', 'city'),
				'sortable'=>true,						
				'value'=>'$data->getCity()',				
				'editable' => array(
					'url' => $this->createUrl('companyAddress/changeAttributeAddress'),
					'type'      => 'select',
			        'attribute' => 'idCity',
			        'source'    => $cityList,
		    		'placement' => 'right',
				)
			),
			array(
				'class' => 'bootstrap.widgets.TbToggleColumn',
				'name' => 'defaultLocation',
				'filter'=> $defaultLocationList = array(0=>"No", 1=>"Yes"),
				'sortable'=>true, 
				'toggleAction'=>'/companyAddress/toggle',
				'checkedButtonLabel'=>'Si',
				'uncheckedButtonLabel'=>'No',				
				'htmlOptions'=>array('style'=>'text-align:center;'),
		),
		array(
			'class' => 'bootstrap.widgets.TbButtonColumn',
			'header'=>'Acciones',
			'headerHtmlOptions'=>array('style'=>'text-align:center; width:50px!important;'),
			'htmlOptions'=>array('style'=>'text-align:center;'),
			'buttons'=>array(
				'delete'=>array(
					'options'=>array('class'=>'btn btn-mini'),
				)
			),
			'deleteButtonUrl'=>'app()->controller->createUrl("/companyAddress/delete", array("id"=>$data->getPrimaryKey(),
				"key"=>uKey("deleteCompAddr".$data->primaryKey)))',
			'template'=>'<div class="btn-group">{delete}</div>',
		),

	))); ?>
</div>

<div class="modal-footer">
	<div class="pull-right">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'label' => 'Volver',
			'url' => '#',
			'htmlOptions' => array('data-dismiss' => 'modal', 'class'=>'btn btn-primary'),
		)); ?>
	</div>
</div>	

<?php 
cs()->registerCoreScript('jquery');
cs()->registerScript(__FILE__ . '_addnewAddress', '
			$("#addAddressModal' . $randNumb . '").on("show", function() {
				var $this = $(this);
				$("#addNew' . $randNumb . ' input").val("");
				$("#addNew' . $randNumb . ' .help-block").css("display", "none");
			});
		');

	$this->beginWidget('bootstrap.widgets.TbModal', array('id' => 'addAddressModal'.$randNumb)); ?>
		<?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
		    'id'=>'addNew' . $randNumb,
		    'enableAjaxValidation'=>true,
		)); ?>

		<div class="modal-header">
	        <a class="close" data-toggle="modal" data-target="#addAddressModal<?php echo $randNumb?>">&times;</a>
	        <h4>Nueva Direccion</h4>
	        <h4><?php echo 'Empresa: '.$company->company;?></h4>
	    </div>
	 
	    <div class="modal-body">

			<div class="row-fluid">
			
			<div class="span6">
				<?php echo $form->textFieldRow($newAddress, 'address', array('class'=>'span6')); ?>
				<?php echo $form->textFieldRow($newAddress, 'zipCode', array('class'=>'span6')); ?>
				
				<?php echo $form->dropDownListRow($newAddress, 'defaultLocation', $defaultLocationList); ?>
			</div>
			
			<div class="span6">
				<div id="countryDiv">
					<?php echo $form->dropDownListRow($newAddress, 'idCountry', $countryList, 
						array(
						'ajax'=>array(
						 	'type'=>'POST',
						 	'url'=>array('company/updateProvince', 'model'=>'CompanyAddress'),
						 	'update'=>'#provinceCombo'. $randNumb, 
						),
					'id'=>'countryCombo'. $randNumb, 'empty'=>Yii::t('app','Select an option')
				)); ?>
				</div>

				<div id="provinceDiv">
					<?php echo $form->dropDownListRow($newAddress, 'idProvince', array(), 
						array(
						'ajax'=>
						 	array(
							 	'type'=>'POST',
								'url'=>array('company/updateCity', 'model'=>'CompanyAddress'),
		                       	'update'=>'#cityCombo'. $randNumb,
						),
					'id'=>'provinceCombo'. $randNumb, 'empty'=>Yii::t('app','Select an option')
				)); ?>
				</div>
				<div id="cityDiv">
					<?php echo $form->dropDownListRow($newAddress, 'idCity', array(), array('id'=>'cityCombo'. $randNumb)); ?>
				</div>
			</div>

			</div>
		</div>
	 
	    <div class="modal-footer">
	    	<?php $this->widget('bootstrap.widgets.TbButton', array(
				'label' => 'Cancelar',
				'url' => '#',
				'htmlOptions' => array('data-dismiss' => 'modal', 'class'=>'btn'),
			)); ?>
			<?php $this->widget('bootstrap.widgets.TbButton', array(
				'buttonType'=>'ajaxSubmit',
				'url'=>array('addressView', 'company'=>$company->idCompany, 'randNumb'=>$randNumb),
				'label'=>'Guardar',
				'htmlOptions' => array('class'=>'btn btn-primary', 'id'=>uniqid('btn-save-address')),
				'ajaxOptions'=>array(
					'dataType'=>'json',
					'success'=>'js:function(d)
					{
						if(d.success)
						{
							$("#addAddressModal' . $randNumb . '").modal("hide");						
							$.fn.yiiGridView.update("address-grid-'.$randNumb.'");
							$.fnResetForm($("#addNew' . $randNumb .'"));
						}
						else
						{
							$.each(d, function(key, val) {
			                   var $input = $("#"+key+"_em_");
			                   $input.text(val);                                                    
			                   $input.show();
			                });
						}
					}'
			)
			)); ?>
	    </div> 

	<?php $this->endWidget(); ?>
<?php $this->endWidget(); ?>
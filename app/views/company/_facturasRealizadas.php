<div>
	<?php
	$invoices = Invoice::model()->findAllByAttributes(array('idCompanyProvider' => $model->idCompany));
	$activities = array();
	//$hasActivityTodoAccess = app()->rbac->checkAccess('programManagement', 'showNonCompletedItems');

	/*foreach($invoices as $invoice) {
		foreach($invoice->getRequiredActivities() as $activity) {
			if ($activity->status == ParticipantActivity::STATUS_TODO && !$hasActivityTodoAccess)
				continue;

			$activities[] = $activity;
		}
	}*/
	
	foreach($invoices as $invoice) {
			$invoices0[] = $invoice;
		}

	$this->widget('bootstrap.widgets.TbExtendedGridView', array(
		'dataProvider' => new CArrayDataProvider($invoices0, array('keyField' => 'idCompanyProvider')),
		'type' => 'striped condensed',
		'responsiveTable' => true,
		'columns' => array(
			array(
				'class' => 'CDataColumn',
				'name' => 'Factura nro.',
				'value' => '$data->numberInvoice',
				'sortable'=>true,
			),
			array(
				'class' => 'CDataColumn',
				'name' => 'Monto total',
				'value' => '$data->invoiceTotalAmount',
				'sortable'=>true,
			),
			array(
				'class' => 'CDataColumn',
				'name' => 'Fecha de emision',
				'value' => '$data->dateEmission',
				'sortable'=>true,
			),
			array(
				'class' => 'CDataColumn',
				'name' => 'Fecha de expiracion',
				'value' => '$data->dateExpiration',
				'sortable'=>true,
			),
			array(
				'class' => 'CDataColumn',
				'name' => 'Fecha de pago',
				'value' => '$data->datePayment',
				'sortable'=>true,
			),
			array(
				'class' => 'CDataColumn',
				'name' => 'Estado',
				'value' => '$data->getStatusDescription()',
				'sortable'=>true,
			),
			array(
				'class' => 'CDataColumn',
				'name' => 'Cliente',
				'value' => '$data->getCompanyClientName()',
				'sortable'=>true,
			),


		))); ?>
</div>
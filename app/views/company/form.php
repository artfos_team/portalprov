<h4><?php (isset($update) && $update) ? 'Actualizar Empresa' : 'Crear Empresa' ?></h4>

	<?php
	$requiresPurchaseOrderList = array(0=>"No", 1=>'Si');
	?>

	<?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
	    'id'=>'company-form',
	    'htmlOptions'=>array('class'=>'well', 'enctype'=>'multipart/form-data'),
	)); ?>

    <?php echo CHtml::errorSummary($model);?>
    <?php echo $form->textFieldRow($model, 'company', array('class'=>'span5', 'placeholder' => Yii::t('app', 'Company'))); ?>
    <?php echo $form->textFieldRow($model, 'companyName', array('class'=>'span5', 'placeholder' => Yii::t('app', 'Company Name'))); ?>   
    <?php //echo $form->dropDownListRow($model, 'idCompanyCategory', $companyCategoryList, array('class'=>'span5')); ?>
    <?php echo $form->textFieldRow($model, 'cuit', array('class'=>'span5', 'placeholder' => Yii::t('app', 'RUT'))); ?>
    <?php //echo $form->textAreaRow($model, 'comments', array('class'=>'span5', 'style'=>'resize: none; height: 140px;', 'placeholder' => 'Comentarios')); ?>

    <?php // $model->generatePictureInput(); ?>

</br></br>
<div>
    <?php $this->widget('bootstrap.widgets.TbButton', array(
        'label' => 'Cancelar',
        'url' => array('/company/index'),
    )); ?>
    <?php $this->widget('bootstrap.widgets.TbButton', array(
        'buttonType'=>'submit', 
        'label'=>'Guardar',
        'htmlOptions' => array('class'=>'btn btn-primary'),
    )); ?>
</div>

<?php $this->endWidget(); ?>
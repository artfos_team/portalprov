<?php $this->widget('bootstrap.widgets.TbButtonGroup',array(
        'buttons'=>array(
            array('label'=> Yii::t('app', 'Agregar compañía'), 'type'=>'inverse', 
                'url'=>array('create')),),
            
    ));?>
<div style="text-align:center">
    <h3>Compañías</h3>
</div>

<div style="width:100%; margin:0 auto;">
    <?php
        $columns = array(
            'calification'=>array(
                'class' => 'CDataColumn',
                'name' => 'calification',
                'value'=> '$data->getCalification()',
                'filter' => array('Sin calificar','1','2','3','4','5')
            ),

            'company'=>array(
                'class' => 'ETbEditableColumn',
                'name' => 'company',
                'value'=> '$data->company',
                'editable' => array(
                    'url' => $this->createUrl('changeAttributeCompany'),
                    'placement' => 'right',
                    'inputclass' => 'span2',
                    'title' => 'Insertar Nombre'
                ),
            ),
            

            'companyName'=>array(
                'class' => 'ETbEditableColumn',
                'name' => 'companyName',
                'value'=>'$data->companyName',
                'editable' => array(
                    'url' => $this->createUrl('changeAttributeCompany'),
                    'placement' => 'top',
                    'inputclass' => 'span2',
                    'title' => 'Insertar Razón Social'
                ),
            ),

            'cuit'=>array(
            	'class' => 'ETbEditableColumn',
            	'name' => 'cuit',
            	'value'=>'$data->cuit',
                'editable' => array(
                    'url' => $this->createUrl('changeAttributeCompany'),
                    'placement' => 'top',
                    'inputclass' => 'span2',
                    'title' => 'Ingresar RUT'
                ),
            ),

            /*'corporateMail'=>array(
            	'class' => 'ETbEditableColumn',
            	'name' => 'corporateMail',
            	'value'=>'$data->corporateMail',
                'editable' => array(
                    'url' => $this->createUrl('changeAttributeCompany'),
                    'placement' => 'top',
                    'inputclass' => 'span2',
                    'title' => 'Ingresar mail corporativo'
                ),
            ),*/
       );

        $this->widget('bootstrap.widgets.TbExtendedGridView', array(
            'id' => 'provider-grid',
            'dataProvider' => $model->search(),
            'filter' => $model,
            'ajaxUrl'=> array('index'),
            'fixedHeader' => true,
            'headerOffset' => 40,
            'type' => 'table striped bordered condensed',
            'responsiveTable' => true,
            'columns' => array_merge(
                array_values($columns),
                array(array(
                    'class' => 'bootstrap.widgets.TbButtonColumn',
                    'header'=>'Acciones',
                    'htmlOptions'=>array('style'=>'width:150px; text-align:center;'),
                    'buttons'=>array(
                        'view'=>array(
                            'icon'=>'icon-eye-open',
                            'label'=>Yii::t('app', 'View'),
                            'options'=>array('class'=>'btn btn-mini'),
                            'url'=>'CHtml::normalizeUrl(array("view", "company"=>$data->primaryKey))',
                        ),
                        'calification'=>array(
                            'icon'=>'icon-star',
                            'label'=>'Calificar',
                            'visible'=> 'app()->rbac->checkAccess("company","viewCalification")',
                            'options'=>array('class'=>'btn btn-mini'),
                            'url'=>'CHtml::normalizeUrl(array("viewCalification", "company"=>$data->primaryKey))',
                        ),
                        'document'=>array(
                            'icon'=>'icon-book',
                            'label'=>'Documentacion',
                            //'visible'=> 'app()->rbac->checkAccess("company","viewCalification")',
                            'options'=>array('class'=>'btn btn-mini'),
                            'url'=>'CHtml::normalizeUrl(array("viewDocument", "company"=>$data->primaryKey))',
                        ),

                    ),
                    
                    'template'=>'{document} {calification} {view}',
                ))
            ),
        )); 
    ?>
    <?php $this->widget('application.components.PageSizeComponent'); ?>
</div>
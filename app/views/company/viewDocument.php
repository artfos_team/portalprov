
<h4><?php echo 'Documentacion presentada'; ?></h4>

	<?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
	    'id'=>'user-form',
	    'htmlOptions'=>array('class'=>'well', 'enctype'=>'multipart/form-data'),
	)); ?>

    <?php echo CHtml::errorSummary($model);?>
    <?php echo "<h4>Documentacion:</h4>"; 
    ?>
    <?php 
    $Documents = Document::model()->findAll();
    echo"<br><br><table>";
    foreach ($Documents as $i => $value) {
        echo "<tr>";
        echo "<td>";
        echo $value['documentName'];
        echo "</td>";
        echo "<td>";
        echo CHtml::checkBox('Document'.$value->idDocument, $model->getDocumentProviderStatus($company, $value->idDocument));
        echo "</td>";
        echo "</tr>";
    }
    echo"</table>";
    /*
    for($i = 1; $i <= $model->getDocumentNameCount(); $i++)
    {
        echo "<br><br>";
        echo $model->getDocumentName($i);
        echo " ";
        echo CHtml::checkBox('Document'.$i, $model->getDocumentProviderStatus($company, $i), array('class'=>'span5'));
    }*/
    echo CHtml::hiddenField('Cant', $model->getDocumentNameCount(), array('class'=>'span5'));
    echo $form->hiddenField($model, 'idProvider',array('value'=>$company), array('class'=>'span5'));
    
    ?>


    <?php //echo $form->textAreaRow($model, 'reasonCalification', array('class'=>'span5', 'placeholder' => 'Razon:', 'rows' => 5 )); ?>
    <?php //echo $form->dropDownList($model,'calification', array(1,2,3,4,5), array('prompt'=>'Seleccione...')); ?>

</br></br>
<div>
    <?php $this->widget('bootstrap.widgets.TbButton', array(
        'label' => 'Cancelar',
        'url' => array('/company/index'),
    )); ?>
    <?php $this->widget('bootstrap.widgets.TbButton', array(
        'buttonType'=>'submit', 
        'label'=>'Confirmar',
        'htmlOptions' => array('class'=>'btn btn-primary'),
    )); ?>
</div>

<?php $this->endWidget(); ?>
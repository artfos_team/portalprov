<?php
/* @var $this InvoiceController */
/* @var $model Invoice */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'idInvoice'); ?>
		<?php echo $form->textField($model,'idInvoice'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'idOrder'); ?>
		<?php echo $form->textField($model,'idOrder'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'XMLfilename'); ?>
		<?php echo $form->textField($model,'XMLfilename',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'SetDTE_ID'); ?>
		<?php echo $form->textField($model,'SetDTE_ID',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'RutEmisor'); ?>
		<?php echo $form->textField($model,'RutEmisor',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'RutEnvia'); ?>
		<?php echo $form->textField($model,'RutEnvia',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'RutReceptor'); ?>
		<?php echo $form->textField($model,'RutReceptor',array('size'=>50,'maxlength'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'FchResol'); ?>
		<?php echo $form->textField($model,'FchResol',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'NroResol'); ?>
		<?php echo $form->textField($model,'NroResol',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'TmstFirmaEnv'); ?>
		<?php echo $form->textField($model,'TmstFirmaEnv',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'SubTotDTE_TpoDTE'); ?>
		<?php echo $form->textField($model,'SubTotDTE_TpoDTE',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'SubTotDTE_NroDTE'); ?>
		<?php echo $form->textField($model,'SubTotDTE_NroDTE',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'DTE_IdDoc'); ?>
		<?php echo $form->textField($model,'DTE_IdDoc',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'IdDoc_TipoDTE'); ?>
		<?php echo $form->textField($model,'IdDoc_TipoDTE',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'IdDoc_Folio'); ?>
		<?php echo $form->textField($model,'IdDoc_Folio',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'IdDoc_FchEmis'); ?>
		<?php echo $form->textField($model,'IdDoc_FchEmis',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'IdDoc_FchVenc'); ?>
		<?php echo $form->textField($model,'IdDoc_FchVenc',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'Emisor_RznSoc'); ?>
		<?php echo $form->textField($model,'Emisor_RznSoc',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'Emisor_RUTEmisor'); ?>
		<?php echo $form->textField($model,'Emisor_RUTEmisor',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'Emisor_GiroEmis'); ?>
		<?php echo $form->textField($model,'Emisor_GiroEmis',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'Emisor_Acteco'); ?>
		<?php echo $form->textField($model,'Emisor_Acteco',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'Emisor_DirOrigen'); ?>
		<?php echo $form->textField($model,'Emisor_DirOrigen',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'Emisor_CmnaOrigen'); ?>
		<?php echo $form->textField($model,'Emisor_CmnaOrigen',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'Emisor_CiudadOrigen'); ?>
		<?php echo $form->textField($model,'Emisor_CiudadOrigen',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'Receptor_RUTRecep'); ?>
		<?php echo $form->textField($model,'Receptor_RUTRecep',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'Receptor_RznSocRecep'); ?>
		<?php echo $form->textField($model,'Receptor_RznSocRecep',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'Receptor_GiroRecep'); ?>
		<?php echo $form->textField($model,'Receptor_GiroRecep',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'Receptor_DirRecep'); ?>
		<?php echo $form->textField($model,'Receptor_DirRecep',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'Receptor_CmnaRecep'); ?>
		<?php echo $form->textField($model,'Receptor_CmnaRecep',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'Receptor_CiudadRecep'); ?>
		<?php echo $form->textField($model,'Receptor_CiudadRecep',array('size'=>50,'maxlength'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'Totales_MntExe'); ?>
		<?php echo $form->textField($model,'Totales_MntExe',array('size'=>50,'maxlength'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'Totales_MntTotal'); ?>
		<?php echo $form->textField($model,'Totales_MntTotal',array('size'=>50,'maxlength'=>50)); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->
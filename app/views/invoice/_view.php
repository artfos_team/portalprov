<?php
/* @var $this InvoiceController */
/* @var $data Invoice */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('idInvoice')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->idInvoice), array('view', 'id'=>$data->idInvoice)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('idOrder')); ?>:</b>
	<?php echo CHtml::encode($data->idOrder); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('XMLfilename')); ?>:</b>
	<?php echo CHtml::encode($data->XMLfilename); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('SetDTE_ID')); ?>:</b>
	<?php echo CHtml::encode($data->SetDTE_ID); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('RutEmisor')); ?>:</b>
	<?php echo CHtml::encode($data->RutEmisor); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('RutEnvia')); ?>:</b>
	<?php echo CHtml::encode($data->RutEnvia); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('RutReceptor')); ?>:</b>
	<?php echo CHtml::encode($data->RutReceptor); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('FchResol')); ?>:</b>
	<?php echo CHtml::encode($data->FchResol); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('NroResol')); ?>:</b>
	<?php echo CHtml::encode($data->NroResol); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('TmstFirmaEnv')); ?>:</b>
	<?php echo CHtml::encode($data->TmstFirmaEnv); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('SubTotDTE_TpoDTE')); ?>:</b>
	<?php echo CHtml::encode($data->SubTotDTE_TpoDTE); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('SubTotDTE_NroDTE')); ?>:</b>
	<?php echo CHtml::encode($data->SubTotDTE_NroDTE); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('DTE_IdDoc')); ?>:</b>
	<?php echo CHtml::encode($data->DTE_IdDoc); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('IdDoc_TipoDTE')); ?>:</b>
	<?php echo CHtml::encode($data->IdDoc_TipoDTE); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('IdDoc_Folio')); ?>:</b>
	<?php echo CHtml::encode($data->IdDoc_Folio); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('IdDoc_FchEmis')); ?>:</b>
	<?php echo CHtml::encode($data->IdDoc_FchEmis); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('IdDoc_FchVenc')); ?>:</b>
	<?php echo CHtml::encode($data->IdDoc_FchVenc); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Emisor_RznSoc')); ?>:</b>
	<?php echo CHtml::encode($data->Emisor_RznSoc); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Emisor_RUTEmisor')); ?>:</b>
	<?php echo CHtml::encode($data->Emisor_RUTEmisor); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Emisor_GiroEmis')); ?>:</b>
	<?php echo CHtml::encode($data->Emisor_GiroEmis); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Emisor_Acteco')); ?>:</b>
	<?php echo CHtml::encode($data->Emisor_Acteco); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Emisor_DirOrigen')); ?>:</b>
	<?php echo CHtml::encode($data->Emisor_DirOrigen); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Emisor_CmnaOrigen')); ?>:</b>
	<?php echo CHtml::encode($data->Emisor_CmnaOrigen); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Emisor_CiudadOrigen')); ?>:</b>
	<?php echo CHtml::encode($data->Emisor_CiudadOrigen); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Receptor_RUTRecep')); ?>:</b>
	<?php echo CHtml::encode($data->Receptor_RUTRecep); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Receptor_RznSocRecep')); ?>:</b>
	<?php echo CHtml::encode($data->Receptor_RznSocRecep); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Receptor_GiroRecep')); ?>:</b>
	<?php echo CHtml::encode($data->Receptor_GiroRecep); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Receptor_DirRecep')); ?>:</b>
	<?php echo CHtml::encode($data->Receptor_DirRecep); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Receptor_CmnaRecep')); ?>:</b>
	<?php echo CHtml::encode($data->Receptor_CmnaRecep); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Receptor_CiudadRecep')); ?>:</b>
	<?php echo CHtml::encode($data->Receptor_CiudadRecep); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Totales_MntExe')); ?>:</b>
	<?php echo CHtml::encode($data->Totales_MntExe); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Totales_MntTotal')); ?>:</b>
	<?php echo CHtml::encode($data->Totales_MntTotal); ?>
	<br />

	*/ ?>

</div>
<?php $this->widget('bootstrap.widgets.TbButtonGroup',array(
        'buttons'=>array(
            array('label'=> Yii::t('app', 'Ver facturas'), 'type'=>'inverse', 
                'url'=>array('index')),),
            
    ));
    echo "<br><br>";
    ?>

<div style="text-align:center">
	<h3><?php echo 'Factura: '.$model->getInvoiceNumbre(); ?></h3>
</div>

<div style="width:100%; margin:0 auto;">
	<?php $invoice = $this->renderPartial('_invoice', array('model'=>$model), true); ?>
	<?php $this->widget('bootstrap.widgets.TbTabs', array(
		'type' => 'tabs', // 'tabs' or 'pills'
		'tabs' => array(
			array('label' => 'Factura', 'content' => $invoice, 'active' => true),
		),
    )); ?>
</div>




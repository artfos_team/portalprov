<div style="text-align:center">
    <h3>Facturas</h3>
</div>

<div style="width:100%; margin:0 auto;">
    <?php
        $columns = array(

            'nic'=>array(
                'name' => 'nic',
            ),

            'idCompany'=>array(
                'class' => 'CDataColumn',
                'name' => 'idCompany',
                'filter'=>CHtml::listData(Company::model()->findAll(array('order'=>'company ASC')),
                'idCompany', 'company'),
                'value'=>'$data->getCompanyName()',
            ),

            'Fecha'=>array(
                'name' => 'Fecha',
                'filter'=>$this->widget('zii.widgets.jui.CJuiDatePicker', array(
                    'model'=>$model,
                    'attribute'=>'fecha_liberacion',
                    'options' => array(
                        'showAnim'=>'fold',
                        'dateFormat' => 'yy-mm-dd',
                    )
                ), true),
                'value'=>Yii::app()->dateFormatter->format('dd/M/yyyy',$model->fecha_liberacion)
            ),

            'idStatusInvoice'=>array(
                'class' => 'CDataColumn',
                'name' => 'statusId',
                'header' => 'Estado',
                'value'=>'$data->getStatusDescription()',
                'filter' => CHtml::listData(buyOrderStatus::model()->findAll(),'idBuyOrderStatus','description')
            ),
        );

        $this->widget('bootstrap.widgets.TbExtendedGridView', array(
            'id' => 'invoice-grid',
            'dataProvider' => $model->search(),
            'filter' => $model,
            'ajaxUrl'=>array('index'),
            'afterAjaxUpdate'=>"function(){jQuery('#date_emission_search').datepicker({'dateFormat': 'yy-mm-dd'}); jQuery('#date_expiration_search').datepicker({'dateFormat': 'yy-mm-dd'}); jQuery('#date_payment_search').datepicker({'dateFormat': 'yy-mm-dd'})}",
            'fixedHeader' => true,
            'headerOffset' => 40,
            'type' => 'table striped bordered condensed',
            'rowCssClassExpression'=>"'invoiceRow'",
            'responsiveTable' => true,
            'columns' => array_merge(array_values($columns),
                array( 
                    array(
                        'class' => 'bootstrap.widgets.TbButtonColumn',
                        'header'=>'Acciones',
                        'htmlOptions'=>array('style'=>'width:150px; text-align:center;'),
                        'buttons'=>array(
                            'view'=>array(
                                'icon'=>'icon-eye-open',
                                'label'=>Yii::t('app', 'View'),
                                'options'=>array('class'=>'btn btn-mini'),
                                'url'=>'CHtml::normalizeUrl(array("view", "invoice"=>$data->primaryKey))',
                            ),
                        ),
                        'template'=>'{view}',
                    )
                )
            ),
        )); 
    ?>
    <?php $this->widget('application.components.PageSizeComponent'); ?>
</div>
<script>
    $( document ).ready(function() {
    jQuery('#date_emission_search').datepicker({'dateFormat': 'yy-mm-dd'}); 
    jQuery('#date_expiration_search').datepicker({'dateFormat': 'yy-mm-dd'}); 
    jQuery('#date_payment_search').datepicker({'dateFormat': 'yy-mm-dd'})
});
</script>
<style>
.ui-datepicker{
    background-color: #f3f3f3;
    width: 200px;
    color:#000;
    height: 220px;
    display:none;
}



.ui-datepicker  th {
    color:#000;
}

.ui-datepicker td {
    width: 20px;
    height: 25px;
}

.ui-datepicker td a{
    width: 100%;
    height: 110%;
    font-size: 12px;
    text-align: center;
    padding-top: 5px; 
    
}

.ui-state-default {
     background-color: transparent !important;
     border: none !important;
     border-radius: 5px;
}

.ui-state-default:hover {
     background-color: #0081c2 !important;
     color: #FFF;
}

.ui-state-active {
    background-color: #9336d1 !important;
}
</style>

<?php
/* @var $this InvoiceController */
/* @var $model Invoice */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'invoice-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'idOrder'); ?>
		<?php echo $form->textField($model,'idOrder'); ?>
		<?php echo $form->error($model,'idOrder'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'XMLfilename'); ?>
		<?php echo $form->textField($model,'XMLfilename',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'XMLfilename'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'SetDTE_ID'); ?>
		<?php echo $form->textField($model,'SetDTE_ID',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'SetDTE_ID'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'RutEmisor'); ?>
		<?php echo $form->textField($model,'RutEmisor',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'RutEmisor'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'RutEnvia'); ?>
		<?php echo $form->textField($model,'RutEnvia',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'RutEnvia'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'RutReceptor'); ?>
		<?php echo $form->textField($model,'RutReceptor',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'RutReceptor'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'FchResol'); ?>
		<?php echo $form->textField($model,'FchResol',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'FchResol'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'NroResol'); ?>
		<?php echo $form->textField($model,'NroResol',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'NroResol'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'TmstFirmaEnv'); ?>
		<?php echo $form->textField($model,'TmstFirmaEnv',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'TmstFirmaEnv'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'SubTotDTE_TpoDTE'); ?>
		<?php echo $form->textField($model,'SubTotDTE_TpoDTE',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'SubTotDTE_TpoDTE'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'SubTotDTE_NroDTE'); ?>
		<?php echo $form->textField($model,'SubTotDTE_NroDTE',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'SubTotDTE_NroDTE'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'DTE_IdDoc'); ?>
		<?php echo $form->textField($model,'DTE_IdDoc',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'DTE_IdDoc'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'IdDoc_TipoDTE'); ?>
		<?php echo $form->textField($model,'IdDoc_TipoDTE',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'IdDoc_TipoDTE'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'IdDoc_Folio'); ?>
		<?php echo $form->textField($model,'IdDoc_Folio',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'IdDoc_Folio'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'IdDoc_FchEmis'); ?>
		<?php echo $form->textField($model,'IdDoc_FchEmis',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'IdDoc_FchEmis'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'IdDoc_FchVenc'); ?>
		<?php echo $form->textField($model,'IdDoc_FchVenc',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'IdDoc_FchVenc'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'Emisor_RznSoc'); ?>
		<?php echo $form->textField($model,'Emisor_RznSoc',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'Emisor_RznSoc'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'Emisor_RUTEmisor'); ?>
		<?php echo $form->textField($model,'Emisor_RUTEmisor',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'Emisor_RUTEmisor'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'Emisor_GiroEmis'); ?>
		<?php echo $form->textField($model,'Emisor_GiroEmis',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'Emisor_GiroEmis'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'Emisor_Acteco'); ?>
		<?php echo $form->textField($model,'Emisor_Acteco',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'Emisor_Acteco'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'Emisor_DirOrigen'); ?>
		<?php echo $form->textField($model,'Emisor_DirOrigen',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'Emisor_DirOrigen'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'Emisor_CmnaOrigen'); ?>
		<?php echo $form->textField($model,'Emisor_CmnaOrigen',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'Emisor_CmnaOrigen'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'Emisor_CiudadOrigen'); ?>
		<?php echo $form->textField($model,'Emisor_CiudadOrigen',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'Emisor_CiudadOrigen'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'Receptor_RUTRecep'); ?>
		<?php echo $form->textField($model,'Receptor_RUTRecep',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'Receptor_RUTRecep'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'Receptor_RznSocRecep'); ?>
		<?php echo $form->textField($model,'Receptor_RznSocRecep',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'Receptor_RznSocRecep'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'Receptor_GiroRecep'); ?>
		<?php echo $form->textField($model,'Receptor_GiroRecep',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'Receptor_GiroRecep'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'Receptor_DirRecep'); ?>
		<?php echo $form->textField($model,'Receptor_DirRecep',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'Receptor_DirRecep'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'Receptor_CmnaRecep'); ?>
		<?php echo $form->textField($model,'Receptor_CmnaRecep',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'Receptor_CmnaRecep'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'Receptor_CiudadRecep'); ?>
		<?php echo $form->textField($model,'Receptor_CiudadRecep',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'Receptor_CiudadRecep'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'Totales_MntExe'); ?>
		<?php echo $form->textField($model,'Totales_MntExe',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'Totales_MntExe'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'Totales_MntTotal'); ?>
		<?php echo $form->textField($model,'Totales_MntTotal',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'Totales_MntTotal'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->
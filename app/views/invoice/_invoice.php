<div>
	<?php
		$atributos = array(
						array(
							'name'=>'numberInvoice',
						),
						array(
							'name'=>'idCompanyProvider',
							'value'=>$model->getCompanyProviderName(),
						),
						array(
							'label'=>'RUT',
							'name'=>'idClientProvider',
							'value'=>$model->getCompanyRut(),
						),
						array(
							'name'=>'invoiceTotalAmount',
						),
						array(
							'name'=>'dateExpiration',
							'value'=>Yii::app()->dateFormatter->format('dd/M/yyyy',$model->dateExpiration)
						),
						array(
							'name'=>'datePayment',
							'value'=>Yii::app()->dateFormatter->format('dd/M/yyyy',$model->dateExpiration)
						),
						array(
							'name'=>'idCompanyClient',
							'value'=>$model->getCompanyClientName(),
						),
					);

		$this->widget('bootstrap.widgets.TbDetailView', array(
			'id' => 'invoice-grid',
			'data' => $model,
			'attributes' => $atributos,
		));
    ?>
</div>
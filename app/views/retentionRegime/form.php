<h4>Agregar régimen de retención</h4>


	<?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
	    'id'=>'document-form',
	    'htmlOptions'=>array('class'=>'well', 'enctype'=>'multipart/form-data'),
	)); ?>


<?php //CAMPOS
        echo $form->textFieldRow($model, 'retentionRegime', array('class'=>'span5', 'placeholder' =>'Descripción...'));
        echo $form->textFieldRow($model, 'codeERP', array('class'=>'span5', 'placeholder' =>'Código...'));
        echo $form->textFieldRow($model, 'vat', array('class'=>'span5', 'placeholder' =>'IVA...'));
        echo $form->textFieldRow($model, 'gain', array('class'=>'span5', 'placeholder' =>'Ganancias...'));


    ?>
</br></br>
<div>
    <?php $this->widget('bootstrap.widgets.TbButton', array(
        'label' => 'Cancelar',
        'url' => array('/retentionRegime/index'),
    )); ?>
    <?php $this->widget('bootstrap.widgets.TbButton', array(
        'buttonType'=>'submit', 
        'label'=>'Guardar',
        'htmlOptions' => array('class'=>'btn btn-primary'),
    )); ?>
</div>

<?php $this->endWidget(); ?>
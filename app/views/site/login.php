<?php
/* @var $this SiteController */
/* @var $model LoginForm */
/* @var $form TbActiveForm */

$this->pageTitle = app()->name . ' - Login';
?>
<div class="site-login">

    <h1 id="nombre_app"><?php echo CHtml::image(baseUrl('/images/Logo.png'), 'logo1',array("id"=>"login-logo","style" => "height: 120px;" )); ?></h1>
    <div id="texto_portal">
            <div id="texto_bienvienida_portal" >Bienvenido</div>
            <hr/>
            <div id="texto_descripcion_portal">Al backend administrativo del PowerSuppliers.</div>
        </div>
    <div class="login-form">

        <?php $form = $this->beginWidget(
            'bootstrap.widgets.TbActiveForm',
            array(
                'id' => 'login-form',
            )
        ); ?>

        <fieldset>
            <?php echo $form->textFieldRow(
                $model,
                'username',
                array('block' => true, 'label' => false, 'placeholder' => 'Username')
            ); ?>
            <?php echo $form->passwordFieldRow(
                $model,
                'password',
                array('block' => true, 'label' => false, 'placeholder' => 'Password')
            ); ?>
        </fieldset>

        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit', 
            'label'=>'Login',
            'htmlOptions' => array('class'=>'btn btn-primary'),
        )); ?>    

        <div style="font-size: 10px; font-style: italic; margin-top: 10px">
            <?php //echo  CHtml::link('aquí', array('loginParticipant')); ?>
        </div>        

        <?php $this->endWidget(); ?>

    </div>
</div>

<?php
/* @var $this SiteController */
$this->pageTitle = app()->name; ?>


<style type="text/css">
    html, body {
        height: 100%;
        width: 100%;
        padding: 0;
        margin: 0;
    }

    #texto_portal {
        background-color: rgba(0, 0, 0, 0.58);
        padding: 10px;
        border-color: rgba(0, 0, 0, 0.78); 
    }
</style>


<div style=''>
   
    <?php echo CHtml::image(baseUrl('/images/Fondo1.png'), 'logo',array("id"=>"full-screen-background-image")); ?>
</div>

<div style=''>
   
    <?php echo CHtml::image(baseUrl('/images/fondo.jpg'), 'logo',array("id"=>"full-screen-background-image")); ?>
</div>
<?php

/* @var $this SiteController */
$this->pageTitle = app()->name; 

$texto_titulo_operaciones = "";     
$sub_texto_titulo_operaciones = "";   
if (user()->es_proveedor) {
        $texto_titulo_operaciones = " Como Proveedor";
        $sub_texto_titulo_operaciones = "Empresa: ".user()->getCompanyName();
}        
        
?>
<div style="text-align:center">
    <h4><?php echo 'Resumen de Operaciones '.$texto_titulo_operaciones; ?></h4>
    <h5><?php echo $sub_texto_titulo_operaciones; ?></h5>
</div>
<div id="div_tabla_estados_dashboard">
    <table id="tabla_estados_dashboard" class="items table table-striped table-bordered">
        <thead>
            <tr>
                <th id="orders-by-state_c0">Estado</th><th class="columna_centrada" id="orders-by-state_c1">Cantidad</th>
            </tr>
        </thead>
        <tbody>
            
        </tbody>
    </table>
</div>

                 

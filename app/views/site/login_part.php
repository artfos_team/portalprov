<?php
/* @var $this SiteController */
/* @var $model LoginForm */
/* @var $form TbActiveForm */

$this->pageTitle = app()->name . ' - Login';
?>
<div class="site-login">

    <h1><?php echo app()->name; ?></h1>

    <div class="login-form">

        <?php $form = $this->beginWidget(
            'bootstrap.widgets.TbActiveForm',
            array(
                'id' => 'login-form',
            )
        ); ?>

        <fieldset>
            <?php echo $form->textFieldRow(
                $model,
                'username',
                array('block' => true, 'label' => false, 'placeholder' => 'Username')
            ); ?>
            <?php echo $form->passwordFieldRow(
                $model,
                'password',
                array('block' => true, 'label' => false, 'placeholder' => 'Password')
            ); ?>
        </fieldset>

        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit', 
            'label'=>'Login',
            'htmlOptions' => array('class'=>'btn btn-primary'),
        )); ?>    

        <div style="font-size: 10px; font-style: italic; margin-top: 10px">
            Ingreso miembros regulares <?php echo CHtml::link('aquí', array('login')); ?>
        </div>   
    
        <?php $this->endWidget(); ?>

    </div>
</div>
<?php $this->widget('bootstrap.widgets.TbButtonGroup',array(
        'buttons'=>array(),
            
    ));?>
<div style="text-align:center">
    <h3>Configuracion</h3>
</div>
<div style="width:100%; margin:0 auto;">
    <?php
        $columns = array(
            'description'=>array(
                'class' => 'CDataColumn',
                'name' => 'description',
                'value'=> '$data->description',
            ),

            'attribute'=>array(
                'class' => 'CDataColumn',
                'name' => 'attribute',
                'value'=> '$data->attribute',
                /*
                'editable' => array(
                    'url' => $this->createUrl('changeAttributeCompany'),
                    'placement' => 'right',
                    'inputclass' => 'span2',
                    'title' => 'Insertar Nombre'
                ),*/
            ),
            

            'value'=>array(
                'class' => 'ETbEditableColumn',
                'name' => 'value',
                'value'=>'$data->getShortValue($data->idConfig)',
                'editable' => array(
                    'url' => $this->createUrl('changeAttributeConfiguration'),
                    'placement' => 'top',
                    'inputclass' => 'span2',
                    'title' => 'Insertar valor'
                ),
            ),

       );

        $this->widget('bootstrap.widgets.TbExtendedGridView', array(
            'id' => 'provider-grid',
            'dataProvider' => $model->search(),
            'filter' => $model,
            'ajaxUrl'=> array('index'),
            'fixedHeader' => true,
            'headerOffset' => 40,
            'type' => 'table striped bordered condensed',
            'responsiveTable' => true,
            'columns' => array_merge(
                array_values($columns),
                array(array(
                    'class' => 'bootstrap.widgets.TbButtonColumn',
                    'header'=>'Acciones',
                    'htmlOptions'=>array('style'=>'width:150px; text-align:center;'),
                    'buttons'=>array(
                        'update'=>array(
                            'label'=>Yii::t('app', 'Edit'),
                            'options'=>array('class'=>'btn btn-mini'),
                            'url'=>'app()->controller->createUrl("update",array(
                                "id"=>$data->primaryKey, "key"=>uKey("updateConf".$data->primaryKey)))'
                        ),
                    ),
                    
                    'template'=>'{update}',
                ))
            ),
        )); 
    ?>
    <?php $this->widget('application.components.PageSizeComponent'); ?>
</div>
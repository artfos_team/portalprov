<?php $update = isset($update) ? $update : false; ?>
<h4><?php echo 'Modificar' ?></h4>

	<?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
	    'id'=>'conf-form',
	    'htmlOptions'=>array('class'=>'well', 'enctype'=>'multipart/form-data'),
	)); ?>

    <?php echo CHtml::errorSummary($model);?>
    
    <?php echo $form->textFieldRow($model, 'description', array('class'=>'span5', 'placeholder' => 'Descripcion...')); ?>
    
    <?php echo $form->textAreaRow($model, 'value', array('style' => 'resize:none' ,'class'=>'span8', 'placeholder' => 'Texto...', 'rows' => 10)); ?>



</br></br>
<div>
    <?php $this->widget('bootstrap.widgets.TbButton', array(
        'label' => 'Cancelar',
        'url' => array('/configuration/index'),
    )); ?>
    <?php $this->widget('bootstrap.widgets.TbButton', array(
        'buttonType'=>'submit', 
        'label'=>'Modificar',
        'htmlOptions' => array('class'=>'btn btn-primary'),
    )); ?>
</div>

<?php $this->endWidget(); ?>
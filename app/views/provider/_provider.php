<div>
	<?php
		$atributos = array(
						array(
							'name'=>'idCompany',
							'value'=>$model->getCompanyName(),
						),
						array(
							'name'=>'idUser',
							'value'=>$model->getUserName(),
						),
						array(
							'name'=>'idComment',
							'label' => 'Comentario',
							'value'=>$model->getCommenterDetail(),
						),
						array(
							'name'=>'date',
							'value'=>Yii::app()->dateFormatter->format('dd/M/yyyy',$model->date)
						),
						array(
							'name'=>'idProviderStatus',
							'label' => 'Estado',
							'value'=>$model->getStatusProvider(),
						),
					);

		$this->widget('bootstrap.widgets.TbDetailView', array(
			'id' => 'provider-grid',
			'data' => $model,
			'attributes' => $atributos,
		));
    ?>
</div>
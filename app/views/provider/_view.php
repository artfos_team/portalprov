<?php
/* @var $this ProviderController */
/* @var $data Provider */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('idProvider')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->idProvider), array('view', 'id'=>$data->idProvider)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('idCompany')); ?>:</b>
	<?php echo CHtml::encode($data->idCompany); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('idUser')); ?>:</b>
	<?php echo CHtml::encode($data->idUser); ?>
	<br />

	<b><?php echo CHtml::encode("Comentario"); ?>:</b>
	<?php echo CHtml::encode($data->idComment); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('idProviderStatus')); ?>:</b>
	<?php echo CHtml::encode($data->idProviderStatus); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('date')); ?>:</b>
	<?php echo CHtml::encode($data->date); ?>
	<br />


</div>
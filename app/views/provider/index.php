<?php $this->widget('bootstrap.widgets.TbButtonGroup',array(
        'buttons'=>array(
            array('label'=> Yii::t('app', 'Crear proveedor'), 'type'=>'inverse', 
                'url'=>array('create')),
            ),        
    ));?>


<div style="text-align:center">
    <h3>Proveedores</h3>
</div>

<div style="width:100%; margin:0 auto;">
    <?php
        $columns = array(

            'calification'=>array(
                'class' => 'CDataColumn',
                'name' => 'calification',
                'value'=> '$data->getCalification()',
                'filter' => array('Sin calificar','1','2','3','4','5')
            ),
            'idUser'=>array(
                'class' => 'TbDataColumn',
                'name' => 'idUser',
                'value'=> '$data->getUserName()',
                'filter' => CHtml::ListData(User::model()->findAll(array('order' => 'name ASC')), 'idUser', 'name')
            ),

            'idCompany'=>array(
                'class' => 'CDataColumn',
                'name' => 'idCompany',
                'value'=>'$data->getCompanyName()',
                'filter' => CHtml::ListData(Company::model()->findAll(array('order' => 'company ASC')), 'idCompany', 'company')
            ),


            'date'=>array(
                'name' => 'date',
            ),

            'isAdmin'=>array(
                'class' => 'ETbEditableColumn',
                'name' => 'isAdmin',
                'value'=> '$data->getProviderIsAdmin()',
                'filter' => array('No','Si'),
                'editable' => array(
                    'url' => $this->createUrl('ChangeAttributeProvider'),
                    'placement' => 'top',
                    'source' => array('0'=> 'No', '1'=>'Si'),
                    'type'      => 'select',
                    'attribute' => 'value',
                    'inputclass' => 'span2',
                    'title' => 'Administrador'
                )
            ),

        	'idProviderStatus'=>array(
                'class' => 'ETbEditableColumn',
                'name' => 'idProviderStatus',
                'filter'=>CHtml::listData(ProviderStatus::model()->findAll(array('order'=>'idProviderStatus ASC')),
                'idProviderStatus', 'description'),
                'value'=>'$data->getStatusProvider()',
                'editable' => array(
                    'url' => $this->createUrl('changeAttributeProvider'),
                    'placement' => 'top',
                    'source' => CHtml::listData(ProviderStatus::model()->findAll(array('order'=>'idProviderStatus ASC')),
                    'idProviderStatus', 'description'),
                    'type'      => 'select',
                    'attribute' => 'idProviderStatus',
                    'inputclass' => 'span2',
                    'title' => 'Seleccionar Estado'
                )
            ),
        );

        $this->widget('bootstrap.widgets.TbExtendedGridView', array(
            'id' => 'provider-grid',
            'dataProvider' => $model->search(),
            'filter' => $model,
            'ajaxUrl'=> array('index'),
            'fixedHeader' => true,
            'headerOffset' => 40,
            'type' => 'table striped bordered condensed',
            'columns' => array_merge(
                array_values($columns),
                array(array(
                    'class' => 'bootstrap.widgets.TbButtonColumn',
                    'header'=>'Acciones',
                    'htmlOptions'=>array('style'=>'width:150px; text-align:center;'),
                    'buttons'=>array(
                        'calification'=>array(
                            'icon'=>'icon-star',
                            'label'=>'Calificar',
                            'options'=>array('class'=>'btn btn-mini'),
                            'url'=>'CHtml::normalizeUrl(array("viewCalification", "provider"=>$data->primaryKey))',
                        ),
                        'view'=>array(
                            'icon'=>'icon-eye-open',
                            'label'=>'Ver detalles',
                            'options'=>array('class'=>'btn btn-mini'),
                            'url'=>'CHtml::normalizeUrl(array("view", "provider"=>$data->primaryKey))',
                        ),
                        'sendMail'=>array(
                            'icon'=>'icon-envelope',
                            'label'=>Yii::t('app', 'Enviar Mail'),
                            'options'=>array('class'=>'btn btn-mini send-email','id'=>'send-email'),
                            'url'=>'app()->controller->createUrl("sendMail",array("id"=>$data->idProvider))'
                
                        ),
                        'delete'=>array(
                        'options'=>array('class'=>'btn btn-mini btn-delete-row', 'id'=>'delete'),
                        'visible'=>'$data->providerDeleted == 0',
                        'url'=>'app()->controller->createUrl("delete",array(
                            "id"=>$data->primaryKey))'
                        ),
                    ),
                    'template'=>'{calification} {view} {sendMail} {delete}',
                ))
            ),
        )); 
    ?>
    <?php $this->widget('application.components.PageSizeComponent'); ?>
</div>

<script type="text/javascript">
$("body").on("click", "#send-email", function(e){
        e.preventDefault();
        var $this = $(this);
        
        if(confirm("Esta seguro de enviar el correo?"))
        {
            $(location).attr('href', $this.attr("href"));
        }
    });

</script>        
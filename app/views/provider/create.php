
<?php $update = isset($update) ? $update : false; ?>
<h4><?php echo ($update) ? Yii::t('app', 'Update provider') : Yii::t('app', 'Crear proveedor'); ?></h4>

	<?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
	    'id'=>'user-form',
	    'htmlOptions'=>array('class'=>'well', 'enctype'=>'multipart/form-data'),
	)); ?>

    <?php echo CHtml::errorSummary($model);?>
    
    <?php echo $form->dropDownListRow($model, 'idCompany', CHtml::listData(Company::model()->findAll(), 'idCompany', 'company'),
        array('class'=>'span5', 'empty'=>'Seleccionar empresa')); ?>
    
    <?php echo $form->dropDownListRow($model, 'idUser', CHtml::listData(User::model()->findAll(), 'idUser', 'name'),
        array('class'=>'span5', 'empty'=>'Seleccionar usuario')); ?>
        
    <?php echo $form->dropDownListRow($model, 'idProviderStatus', CHtml::listData(ProviderStatus::model()->findAll(), 'idProviderStatus', 'description'),
        array('class'=>'span5', 'empty'=>'Seleccionar estado')); ?>
     </br>   
    Administrador <?php echo $form->checkBox($model, 'isAdmin', array('value'=>1, 'uncheckValue'=>0)); ?>
    

</br></br>
<div>
    <?php $this->widget('bootstrap.widgets.TbButton', array(
        'label' => 'Cancelar',
        'url' => array('/provider/index'),
    )); ?>
    <?php $this->widget('bootstrap.widgets.TbButton', array(
        'buttonType'=>'submit', 
        'label'=>'Crear',
        'htmlOptions' => array('class'=>'btn btn-primary'),
    )); ?>
</div>

<?php $this->endWidget(); ?>
<?php
/* @var $this ProviderController */
/* @var $model Provider */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'idProvider'); ?>
		<?php echo $form->textField($model,'idProvider'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'idCompany'); ?>
		<?php echo $form->textField($model,'idCompany'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'idUser'); ?>
		<?php echo $form->textField($model,'idUser'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'idComment'); ?>
		<?php echo $form->textField($model,'idComment'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'idProviderStatus'); ?>
		<?php echo $form->textField($model,'idProviderStatus'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'date'); ?>
		<?php echo $form->textField($model,'date'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->
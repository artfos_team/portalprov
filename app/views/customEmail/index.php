<div style="text-align:center">
    <h3>Ajustes de Email</h3>
</div>

<div style="width:80%; margin:0 auto;">
    <?php
        $columns = array(
            'id'=>array(
                'class' => 'ETbEditableColumn',
                'name' => 'idCustomEmail',
                //'value'=> '$data->company',
                /*'editable' => array(
                    'url' => $this->createUrl('changeAttributeManual'),
                    'placement' => 'right',
                    'inputclass' => 'span2',
                    'title' => 'id'
                ),*/
            ),
            'subject'=>array(
                'class' => 'ETbEditableColumn',
                'name' => 'subject',
                //'value'=> '$data->company',
                'editable' => array(
                    'url' => $this->createUrl('changeAttributeManual'),
                    'placement' => 'right',
                    'inputclass' => 'span2',
                    'title' => 'Modificar asunto'
                ),
            ),
       );

        $this->widget('bootstrap.widgets.TbExtendedGridView', array(
            'id' => 'provider-grid',
            'dataProvider' => $model->search(),
            'filter' => $model,
            'ajaxUrl'=> array('index'),
            'fixedHeader' => true,
            'headerOffset' => 40,
            'type' => 'table striped bordered condensed',
            'responsiveTable' => true,
            'columns' => array_merge(array_values($columns),
                array(
                    array(
                    'class' => 'bootstrap.widgets.TbButtonColumn',
                    'header'=>'Acciones',
                    'htmlOptions'=>array('style'=>'width:150px; text-align:center;'),
                    'buttons'=>array(
                        'edit'=>array(
                            'label'=>Yii::t('app', 'Edit'),
                            'options'=>array('class'=>'btn btn-mini'),
                            'url'=>'CHtml::normalizeUrl(array("EditBodyEmail", "id"=>$data->primaryKey))',
                        )
                    ),
                    'template'=>'{edit}',
                    )                    
                )
            ),
        )); 
    ?>

    <?php $this->widget('application.components.PageSizeComponent'); ?>
</div>


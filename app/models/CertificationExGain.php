<?php

/**
 * This is the model class for table "CertificationExGain".
 *
 * The followings are the available columns in table 'Certification':
 * @property integer $idCertificationExGain
 * @property string $number
 * @property integer $cuit
 * @property string $period
 * @property string $percentage
 * @property string $resolution
 * @property string $state
 * @property string $issue
 * @property string $publication
 * @property string $validity
 
 */
class CertificationExGain extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Certification the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'certification_exgain';

	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
            array('number,cuit,period,percentage,resolution,state,issue,publication,validity', 'length', 'max'=>255),		
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'number' => Yii::t('app', 'Numero Certificado Ex Ganancias'),
			'cuit' => Yii::t('app', 'Cuit Certificado Ex Ganancias'),
			'period' => Yii::t('app', 'Periodo Certificado Ex Ganancias'),
			'percentage' => Yii::t('app', 'Porcentage Certificado Ex Ganancias'),
            'resolution' => Yii::t('app', 'Resolucion Certificado Ex Ganancias'),
			'state' => Yii::t('app', 'Estado Certificado Ex Ganancias'),
			'issue' => Yii::t('app', 'Fecha de Emisión Certificado Ex Ganancias'),
			'publication' => Yii::t('app', 'Fecha de Publicacion Certificado Ex Ganancias'),
			'validity' => Yii::t('app', 'Fecha Validacion Certificado Ex Ganancias'),
		);
	}


	/*


	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */


	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('number',$this->number,true);
		$criteria->compare('cuit',$this->cuit,true);
		$criteria->compare('period',$this->period,true);
		$criteria->compare('percentage',$this->percentage,true);
		$criteria->compare('resolution',$this->resolution,true);
		$criteria->compare('state',$this->state,true);
		$criteria->compare('issue',$this->issue,true);
		$criteria->compare('publication',$this->publication,true);
		$criteria->compare('validity',$this->validity,true);
		$sort = new CSort();
		$sort->defaultOrder = 'nombre ASC';

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'sort'=>$sort,
			'pagination'=>array(
				'pageSize'=>app()->controller->getItemsPerPage()
			),
		));
	}
	

}
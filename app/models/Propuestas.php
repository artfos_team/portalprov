<?php

/**
 * This is the model class for table "company".
 *
 * The followings are the available columns in table 'company':
 * @property integer $idRubroCompany
 * @property string $idRubro
 * @property integer $idCompany
 */
class Propuestas extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Propuestas the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'licitaciones_ofertas';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('idOferta, idLicitacion, idProvider', 'numerical', 'integerOnly'=>true),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	
	public function attributeLabels()
	{
		return array(
			'idLicitacion' => Yii::t('app', 'Licitacion'),
			'idProvider' => Yii::t('app', 'Proveedor'),
            'monto' => Yii::t('app', 'Monto'),
            'status' => Yii::t('app', 'Estado'),
		);
	}

	/*
	public function defaultScope()
	{
		return array(
			'condition' => 'status = 1'
		);
	}*/

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */

    public function getList() {

        $sql = "SELECT * FROM propuestas;";
        $command=Yii::app()->db->createCommand($sql);
        return $data=$command->queryAll();
    }



	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('idOferta',$this->idOferta);
		$criteria->compare('idLicitacion',$this->idLicitacion,true);
        $criteria->compare('idProvider',$this->idProvider,true);
        $criteria->compare('status',$this->status,true);


		$sort = new CSort();
		$sort->defaultOrder = 'idOferta ASC';

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'sort'=>$sort,
			'pagination'=>array(
				'pageSize'=>app()->controller->getItemsPerPage()
			),
		));
	}
	
	public function delete()
	{

			$criteria = new CDbCriteria();
			$criteria->condition = 'idPropuesto = :id';
			$criteria->params[':id'] = $this->idDocument;
			CompanyContact::model()->deleteAll($criteria);
			CompanyAddress::model()->deleteAll($criteria);
			return parent::delete();
    }
    
}
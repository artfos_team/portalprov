<?php

/**
 * This is the model class for table "comments".
 *
 * The followings are the available columns in table 'comments':
 * @property integer $idComments
 * @property string $description
 * @property integer $idCommentType
 * @property integer $idUser
 * @property string $dateComments
 * @property integer $statusComments
 * @property integer $idRefenceTo
 *
 * The followings are the available model relations:
 * @property CommentType $messageType0
 * @property Comments $idRefenceTo0
 * @property Comments[] $comments
 * @property User $idUser0
 * @property CommentsStatus $statusComments0
 */
class Messages extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Comments the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'messages';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			//array('dateComments', 'required'),
			array('messageType, idEmisor, status, idReceptor', 'numerical', 'integerOnly'=>true),
			array('message', 'length', 'max'=>255),
			
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			//array('idComments, description, idCommentType, idUser, dateComments, statusComments, idRefenceTo', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
            'messageType0' => array(self::BELONGS_TO, 'MessageType', 'messageType'),
            'status0' => array(self::BELONGS_TO, 'MessageStatus', 'status'),
            
			//'idRefenceTo0' => array(self::BELONGS_TO, 'Comments', 'idRefenceTo'),
			//'comments' => array(self::HAS_MANY, 'Comments', 'idRefenceTo'),
			//'idEmisor0' => array(self::BELONGS_TO, 'User', 'idUser'),
			//'statusComments0' => array(self::BELONGS_TO, 'CommentsStatus', 'statusComments'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'idMessage' => 'Id mensaje',
			'message' => 'Descripción',
			'idEmisor' => 'Emisor',
			'idReceptor' => 'Receptor',
			'date' => 'Fecha',
			'status' => 'Estado',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('idMessage',$this->idComments);
		$criteria->compare('message',$this->description,true);
		$criteria->compare('idMessageType',$this->idCommentType);
		$criteria->compare('idEmisor',$this->idUser);
		$criteria->compare('date',$this->dateComments,true);
		$criteria->compare('status',$this->statusComments);
		$criteria->compare('idReceptor',$this->idRefenceTo);
		$criteria->order = 'date DESC';
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'pagination' => array(
				'pageSize' => app()->controller->getItemsPerPage()
			),
		));
    }
    public function getMessages()
    {
        
    }
/*
	public function getStatusName()
    {
    	return isset ($this->statusComments0) && $this->statusComments0 instanceof CommentsStatus ? $this->statusComments0->description : "N/A";
    }

    public function getUserName()
    {
    	return isset ($this->idUser0) && $this->idUser0 instanceof User ? $this->idUser0->email : "N/A";
    }

    public function getTypeName()
    {
    	return isset ($this->messageType0) && $this->messageType0 instanceof CommentType ? $this->messageType0->description : "N/A";
    }

    public function getCommentByReference()
    {
    	return isset ($this->idRefenceTo) && $this->idRefenceTo0 instanceof Comments && $this->idCommentType == 2 ? $this->idRefenceTo0->description : "";
    }
    */
}
<?php

/**
 * This is the model class for table "sap_parameters".
 *
 * The followings are the available columns in table 'sap_parameters':
 * @property int $idSapParameters
 * @property string $ashost
 * @property string $sysnr
 * @property string $client
 * @property string $lang
 * @property string $user
 * @property string $passwd
 * @property string $function
 */
class SapParameters extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Parameters the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'sap_parameters';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('ashost, user, passwd, function', 'required'),
			array('ashost, user, passwd', 'length', 'max'=>200),
			array('sysnr, client, lang', 'length', 'max'=>10),
			array('ashost, user, sysnr, client, lang, passwd, function', 'safe', 'on'=>'search'),
		);
			// The following rule is used by search().		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'ashost' => 'Host',
			'sysnr' => 'SYSNR',
			'client' => 'Client',
			'lang' => 'Lang',
			'user' => 'Usuario',
			'passwd' => 'Contraseña',
			'function' => 'Función'
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('ashost',$this->ashost);
		$criteria->compare('sysnr',$this->sysnr);
		$criteria->compare('client',$this->client,true);
		$criteria->compare('lang',$this->lang);
		$criteria->compare('user',$this->user,true);
		$criteria->compare('passwd',$this->passwd);
		$criteria->compare('function',$this->function);


		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public function encriptar($cadena){
        $key='';  // Una clave de codificacion, debe usarse la misma para encriptar y desencriptar
        $encrypted = base64_encode(openssl_encrypt($cadena, 'AES-256-CBC', md5($key)));
        $encrypted = str_replace(array('+','/','='),array('-','_','.'),$encrypted); 
        return $encrypted; //Devuelve el string encriptado
    }

    public function desencriptar($cadena){
         $key='';  // Una clave de codificacion, debe usarse la misma para encriptar y desencriptar
         $cadena = str_replace(array('-','_','.'),array('+','/','='),$cadena); 
         $decrypted = rtrim(mcrypt_decrypt(MCRYPT_RIJNDAEL_256, md5($key), base64_decode($cadena), MCRYPT_MODE_CBC, md5(md5($key))), "\0");
         
        return $decrypted;  //Devuelve el string desencriptado
    }
}
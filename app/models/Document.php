<?php

/**
 * This is the model class for table "document".
 *
 * The followings are the available columns in table 'document':
 * @property integer $idDocument
 * @property string $documentName
 * @property integer $required
 * @property integer $active
 * The followings are the available model relations:
 * @property CompanyCategory $idCompanyCategory0
 * @property CompanyAddress[] $companyAddresses
 * @property CompanyContact[] $companyContacts
 * @property Participant[] $participants
 */
class Document extends CActiveRecord
{

	CONST ATTACHMENTPATH = '/upload/document';

	public $_logo;

	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Document the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
     * @return array the behavior configurations (behavior name=>behavior configuration)
     */
    public function behaviors()
    {
        return array_merge(parent::behaviors(), array(
            'PicturableBehavior' => array(
				'class'=>'application.components.behaviors.PicturableBehavior',
				'attributeNames' => array('logo'),
				'dirName' => 'document',
			),
        ));
    }

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'document';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('idDocument, required, active', 'numerical', 'integerOnly'=>true),
			array('documentName', 'length', 'max'=>255),
			//array('logo, _logo, comments', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			//array('idCompany, company, companyName, logo, idCompanyCategory, cuit, requiresPurchaseOrder', 'safe', 'on'=>'search'),
			//array('_logo', 'file', 'types'=>'jpg, gif, png', 'allowEmpty'=>true),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'idDocument' => Yii::t('app', 'Id documento'),
			'documentName' => Yii::t('app', 'Documento'),
			'required' => Yii::t('app', 'Obligatorio'),
            'active' => Yii::t('app', 'Activo'),
		);
	}

	public function scopes() {
		return array(
			'findAllWithOrder' => array('order' => 'documentName ASC'),
		);
	}
	/*
	public function defaultScope()
	{
		return array(
			'condition' => 'active = 0'
		);
	}*/

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */

    public function getList() {

        //return $this->findAll();

        $sql = "SELECT * FROM document;";
        $command=Yii::app()->db->createCommand($sql);
        return $data=$command->queryAll();
    }



	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('idDocument',$this->idDocument);
		$criteria->compare('documentName',$this->documentName,true);
		$criteria->compare('required',$this->required,true);
		$criteria->compare('active',$this->active,true);


		$sort = new CSort();
		$sort->defaultOrder = 'documentName ASC';

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'sort'=>$sort,
			'pagination'=>array(
				'pageSize'=>app()->controller->getItemsPerPage()
			),
		));
	}
	
	/**
	 * Returns the company category, in string
	 * @return string
	 */
	public function getXlsAttributes()
	{
		return array(
			array('attribute'=>'documentName'),
		);
    }
	public function delete()
	{

			$criteria = new CDbCriteria();
			$criteria->condition = 'idDocument = :id';
			$criteria->params[':id'] = $this->idDocument;

			CompanyContact::model()->deleteAll($criteria);
			CompanyAddress::model()->deleteAll($criteria);
			return parent::delete();
    }
    
    public function getActive()
    {
        if($this->active == 1)
        {
            return "Si";
        }else{
            return "No";
        }
    }
    public function getRequired()
    {
        if($this->required == 1)
        {
            return "Si";
        }else{
            return "No";
        }
	}
	public function deleteDocument($id)
	{
		$criteria = "idDocument='$id'";
		/*$sql = "DELETE from provider_document WHERE idDocument='$id'";
		//throw new Exception($sql);
		
        $command=Yii::app()->db->createCommand($sql);
		$data=$command->queryAll();*/
		ProviderDocument::model()->deleteAll($criteria);
		parent::delete();
		return;
	}
}
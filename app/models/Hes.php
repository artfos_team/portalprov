<?php

/**
 * This is the model class for table "hes".
 *
 * The followings are the available columns in table 'hes':
 * @property integer $idHes
 * @property integer $number
 * @property integer $idBuyOrder
 * @property integer $description
 * @property integer $quantity

 */
class Hes extends CActiveRecord
{


	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Hes the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'hes';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('number, idBuyOrder', 'required'),
			array('idHes, number, idBuyOrder', 'numerical', 'integerOnly'=>true),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'idCompany' => Yii::t('app', 'Id Company'),
			'company' => Yii::t('app', 'Name'),
			'companyName' => Yii::t('app', 'Legal Name'),
			'cuit' => Yii::t('app', 'CUIT'),
			'corporateMail' => 'Mail corporativo',
			'calification' => 'Calificacion',
			'reasonCalification' => 'Motivo de calificacion',
		);
	}

	public function scopes() {
		return array(
			'findAllWithOrder' => array('order' => 'hes ASC'),
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */

    public function getList() {

        //return $this->findAll();

        $sql = "SELECT * FROM hes where number <> 0 ;";
        $command=Yii::app()->db->createCommand($sql);
        return $data=$command->queryAll();
    }



	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('idCompany',$this->idCompany);
		$criteria->compare('company',$this->company,true);
		$criteria->compare('companyName',$this->companyName,true);
		$criteria->compare('corporateMail',$this->corporateMail,true);
		$criteria->compare('cuit',$this->cuit,true);
		$criteria->compare('calification',$this->calification,true);
		$criteria->compare('reasonCalification',$this->reasonCalification,true);


		$sort = new CSort();
		$sort->defaultOrder = 'company ASC';

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'sort'=>$sort,
			'pagination'=>array(
				'pageSize'=>app()->controller->getItemsPerPage()
			),
		));
	}
	
	/**
	 * Returns the company category, in string
	 * @return string
	 */ 
        public function getCompanyName()
	{
		return $this->companyName ? $this->companyName : "No";
	}
        
        public function getIdCompany()
	{
		return $this->idCompany ? $this->idCompany : 0;
	}


}
<?php

class BuyOrderDocumentHes extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return BuyOrderDocumentHes the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'buy_order_document_x_hes';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('idBuyOrderDocument, idHes', 'numerical', 'integerOnly'=>true),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'hes' => array(self::BELONGS_TO, 'Hes', 'idHes'),
			'buyOrderDocument' => array(self::BELONGS_TO, 'BuyOrderDocument', 'idBuyOrderDocument')
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
            'idBuyOrder' => Yii::t('app', 'idBuyOrder')
		);
	}

	public function scopes() {
		return array(
			'findAllWithOrder' => array('order' => 'idBuyOrderDocument ASC'),
		);
	}
}
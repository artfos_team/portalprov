<?php

/**
 * This is the model class for table "invoice".
 *
 * The followings are the available columns in table 'invoice':
 * @property integer $idDocumentation
 * @property string $url
 * @property integer $idDocumentationType
 * @property integer $idProvider
 *
 * The followings are the available model relations:
 * @property Provider $provider
 * @property DocumentationType $documentationType
 */
class Documentation extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Invoice the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'documentation';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('idDocumentation, url', 'required'),
			array('idDocumentation, idDocumentationType, idProvider', 'numerical', 'integerOnly'=>true),
			array('url', 'length', 'max'=>200),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('idDocumentation, idDocumentationType, idProvider, url', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'provider' => array(self::BELONGS_TO, 'Provider', 'idProvider'),
			'documentationType' => array(self::BELONGS_TO, 'Documentation', 'idDocumentationType'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'idDocumentation' => 'Id Documentación',
			'url' => 'URL',
			'idDocumentationType' => 'Tipo de documentación',
			'idProvider' => 'Proveedor',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('idDocumentation',$this->idDocumentation);
		$criteria->compare('url',$this->url);
		$criteria->compare('idDocumentationType',$this->idDocumentationType);
		$criteria->compare('idProvider',$this->idProvider);
		
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}
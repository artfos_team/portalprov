<?php

/**
 * This is the model class for table "payment_type".
 *
 * The followings are the available columns in table 'payment_type':
 * @property integer $idDocumentationType
 * @property string $documentationType
 *
 */
class BuyOrderDocumentType extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return DocumentType the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'buy_order_document_type';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('idBuyOrderDocumentationType', 'required'),
			array('idBuyOrderDocumentationType', 'numerical', 'integerOnly'=>true),
			array('description', 'length', 'max'=>255),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			//'programs' => array(self::MANY_MANY, 'Program', 'program_activity(idActivity, idProgram)'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'idDocumentationType' => Yii::t('app', 'Id Tipo de documento'),
			'documentationType' => Yii::t('app', 'Tipo de documento'),
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('idDocumentationType',$this->idDocumentationType);
		$criteria->compare('documentationType',$this->documentationType,true);

		$sort = new CSort();
		$sort->defaultOrder = 'documentationType ASC';
		
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'sort'=>$sort,
			'pagination'=>array(
				'pageSize'=>app()->controller->getItemsPerPage()
			),
		));
	}

	/*public function getActivityType()
	{
		if ($this->activityType == 0) {
			return 'Individual';
		}else{
			return 'Grupal';
		}
	}*/


	/*public function getXlsAttributes()
	{
		return array(
			array('attribute'=>'activity'),
			array('attribute'=>'description'),
			array('attribute'=>'activityType', 'displayFunction'=>'getActivityType', 'params'=>array()),
			array('attribute'=>'activityRequired', 'type'=>'bool'),
		);
	}*/
}
<?php

/**
 * This is the model class for table "document".
 *
 * The followings are the available columns in table 'document':
 * @property integer $idEmail
 * @property string $subject
 * @property string $body
 * @property string $date_created
 * @property string $date_created
 * @property integer $sent
 * The followings are the available model relations:
 * @property EmailReceiver $receivers
 */
class Email extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Email the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'email';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('subject', 'length', 'max'=>200),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'receivers' => array(self::HAS_MANY, 'EmailReceiver', 'idEmail'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'idEmail' => Yii::t('app', 'Id Email'),
			'subject' => Yii::t('app', 'Sujeto'),
			'body' => Yii::t('app', 'Cuerpo'),
            'sent' => Yii::t('app', 'Enviado'),
		);
	}

	public function scopes() {
		return array(
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */

	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('idEmail',$this->idEmail);
		$criteria->compare('subject',$this->subject,true);
		$criteria->compare('body',$this->body,true);
		$criteria->compare('sent',$this->sent);


		$sort = new CSort();

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'sort'=>$sort,
			'pagination'=>array(
				'pageSize'=>app()->controller->getItemsPerPage()
			),
		));
	}
	
	/**
	 * Returns the company category, in string
	 * @return string
	 */
	public function getXlsAttributes()
	{
		return array(
			array('attribute'=>'subject'),
		);
	}
	
	public function addReceiver($email) {
		if (!isset($this->idEmail)) {
			return false;
		}
		$receiver = new EmailReceiver();
		$receiver->email = $email;
		$receiver->idEmail = $this->idEmail;
		return $receiver->save();
	}
    
    public function getSent()
    {
        if($this->sent == 1)
        {
            return "Si";
        }else{
            return "No";
        }
    }
}
<?php

/**
 * This is the model class for table "RetentionRegime".
 *
 * The followings are the available columns in table 'RetentionRegime':
 * @property integer $idRetentionRegime
 * @property string $retentionRegime
 * @property integer $gain
 * @property integer $vat
 * @property string $codeERP
 */
class RetentionRegime extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return RetentionRegime the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'retention_regime';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('idRetentionRegime,gain,vat', 'numerical', 'integerOnly'=>true),
			array('retentionRegime', 'length', 'max'=>50),
			array('codeERP', 'length', 'max'=>2),

		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        //
		return array(
           //'licitacionStatus' => array(self::BELONGS_TO, 'LicitacionesStatus', 'status'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'idRetentionRegime' => Yii::t('app', 'Id régimen de retención'),
			'retentionRegime' => Yii::t('app', 'Régimen de retención'),
			'gain' => Yii::t('app', 'Ganancias'),
			'vat' => Yii::t('app', 'IVA'),
			'codeERP' => Yii::t('app', 'Código'),

		);
	}

	public function scopes() {
		return array(
			'findAllWithOrder' => array('order' => 'retentionRegime ASC'),
		);
	}


	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */

    public function getList() {

        $sql = "SELECT * FROM retentionRegime;";
        $command=Yii::app()->db->createCommand($sql);
        return $data=$command->queryAll();
    }



	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('idRetentionRegime',$this->idRetentionRegime);
		$criteria->compare('retentionRegime',$this->retentionRegime,true);

		$sort = new CSort();
		$sort->defaultOrder = 'retentionRegime ASC';

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'sort'=>$sort,
			'pagination'=>array(
				'pageSize'=>app()->controller->getItemsPerPage()
			),
		));
	}
	
	public function delete()
	{

			$criteria = new CDbCriteria();
			$criteria->condition = 'idRetentionRegime = :id';
			return parent::delete();
    }

}
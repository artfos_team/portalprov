<?php

/**
 * This is the model class for table "company".
 *
 * The followings are the available columns in table 'company':
 * @property integer $idCompany
 * @property string $company
 * @property string $companyName
 * @property string $logo
 * @property integer $idCompanyCategory
 * @property string $cuit
 * @property integer $requiresPurchaseOrder
 * @property string $comments
 * @property integer $calification
 * @property string $reasonCalification
 * The followings are the available model relations:
 * @property CompanyCategory $idCompanyCategory0
 * @property CompanyAddress[] $companyAddresses
 * @property Participant[] $participants
 * @property integer $idCompanyStatus
 * @property integer $idCompanyDataStatus
 * @property integer $signUpStatus
 * @property integer $idStage
 * @property integer $isExport

 */
class Company extends CActiveRecord implements ProviderDataExport
{

	CONST ATTACHMENTPATH = '/upload/company';


	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Company the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'company';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('cuit', 'required'),
			array('cuit','unique'),
			array('calification,idCompanyStatus, idCompanyDataStatus, idStage, isExport', 'numerical', 'integerOnly'=>true),
			array('calification,idCompanyStatus, signUpStatus', 'numerical', 'integerOnly'=>true),
			array('company, companyName, corporateMail', 'length', 'max'=>100),
			array('corporateMail', 'safe'),
			// The following rule is used by seasrch().
			// Please remove those attributes that should not be searched.
			array('idCompany, company, companyName, cuit, corporateMail', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			//'category' => array(self::BELONGS_TO, 'CompanyCategory', 'idCompanyCategory'),
			'status' => array(self::BELONGS_TO, 'ProviderStatus', 'idCompanyStatus'),
			'dataStage' => array(self::BELONGS_TO, 'CompanyStage', 'idStage'),
			'dataStatus' => array(self::BELONGS_TO, 'CompanyDataStatus', 'idCompanyDataStatus'),
			'generalData' => array(self::BELONGS_TO, 'CompanyGeneralData', 'idGeneralData'),
			'paymentData' => array(self::BELONGS_TO, 'CompanyPaymentData', 'idPaymentData'),
			'taxData' => array(self::BELONGS_TO, 'CompanyTaxData', 'idTaxData'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'idCompany' => Yii::t('app', 'Id Company'),
			'company' => Yii::t('app', 'Name'),
			'companyName' => Yii::t('app', 'Legal Name'),
			'cuit' => Yii::t('app', 'CUIT'),
			'corporateMail' => 'Mail corporativo',
			'calification' => 'Calificacion',
			'reasonCalification' => 'Motivo de calificacion',
			'idCompanyStatus'=>'Estado',
			'idCompanyDataStatus'=>'Estado Datos de la empresa',
			'idStage'=>'Etapa',
			'isExport'=>'Exportado'
		);
	}

	public function scopes() {
		return array(
			'findAllWithOrder' => array('order' => 'company ASC'),
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */

    public function getList() {

        //return $this->findAll();

        $sql = "SELECT * FROM company;";
        $command=Yii::app()->db->createCommand($sql);
        return $data=$command->queryAll();
    }



	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('idCompany',$this->idCompany);
		$criteria->compare('company',$this->company,true);
		$criteria->compare('companyName',$this->companyName,true);
		$criteria->compare('corporateMail',$this->corporateMail,true);
		$criteria->compare('cuit',$this->cuit,true);
		$criteria->compare('calification',$this->calification,true);
		$criteria->compare('reasonCalification',$this->reasonCalification,true);
		$criteria->compare('idCompanyStatus',$this->idCompanyStatus,true);
		$criteria->compare('idCompanyDataStatus',$this->idCompanyDataStatus,true);
		$criteria->compare('idStage',$this->idStage,true);
		$criteria->compare('isExport',$this->isExport,true);


		$sort = new CSort();
		$sort->defaultOrder = 'company ASC';

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'sort'=>$sort,
			'pagination'=>array(
				'pageSize'=>app()->controller->getItemsPerPage()
			),
		));
	}



	public function getExportableAttributes()
	{
		$ret = array();
		//$ret['idCountry0']['codCountry'] = 'plain';
		//$ret['idCountry0'] = 'plain';
		$ret['company'] = 'company';
		//DSOC
		$ret['Numero de personal'] = 'numDePersonal';
		$ret['COMP1'] = 'comp';
		$ret['COMP2'] = 'comp';
		$ret['COMP3'] = 'comp';
		$ret['Vias de pago'] = 'viasDePago';
		$ret['Condiciones de pago'] = 'condPago';
		$ret['Grupo de tesoreria'] = 'grupoTesor';
		$ret['Ritmo calc.intereses'] = 'ritmoCalcInt';
		$ret['Limite efectos'] = 'limEfectos';
		$ret['Dias hasta cobro'] = 'diasHastaCobro';
		$ret['Verif. factura doble'] = 'verifFactDoble';
		$ret['Pais de retencion'] = 'paisDeRet';
		$ret['Orden del Cheque'] = 'ordenCheque';
		$ret['COMP4'] = 'comp';
		$ret['Actividad economica pral.segun cod.CIIU'] = 'activEcon';
		$ret['Completion Date Of Inspection'] = 'dateInspect';
		$ret['Offset Percentage'] = 'offsetPercent';
		$ret['Basis points'] = 'basisPoints';
		$ret['empty1'] = 'empty';
		$ret['empty2'] = 'empty';
		$ret['empty3'] = 'empty';
		$ret['empty4'] = 'empty';
		$ret['empty5'] = 'empty';
		//DSOC

		//DCOM
		$ret['Organizacion comprasDCOM'] = 'orgComprasDCOM';
		$ret['compDCOM'] = 'compDCOM';
		$ret['compDCOM1'] = 'compDCOM';
		//$ret['compDCOM2'] = 'compDCOM';
		//$ret['compDCOM3'] = 'compDCOM';
		$ret['Moneda de pedidoDCOM'] = 'monPedidoDCOM';
		$ret['Valor minimo pedidoDCOM'] = 'valMinPedidoDCOM';
		$ret['Condiciones de pagoDCOM'] = 'condPagoDCOM';
		$ret['Verific.fact.base EMDCOM'] = 'verifFBEMDCOM';
		$ret['Grupo de comprasDCOM'] = 'gpComprasDCOM';
		$ret['Autofacturacion0DCOM'] = 'autoFactDCOM';
		$ret['Plazo entrega prev.DCOM'] = 'plEntPrevDCOM';
		$ret['Verif.facturas rel.serviciosDCOM'] = 'verifFactRelServDCOM';
		$ret['Hora de puesta a disposicionDCOM'] = 'horaPuestaDispDCOM';
		$ret['empty6'] = 'emptyDCOM';
		$ret['empty7'] = 'emptyDCOM';
		$ret['empty8'] = 'emptyDCOM';
		$ret['empty9'] = 'emptyDCOM';
		$ret['empty10'] = 'emptyDCOM';
		$ret['empty11'] = 'emptyDCOM';
		$ret['empty12'] = 'emptyDCOM';
		$ret['empty13'] = 'emptyDCOM';
		$ret['empty14'] = 'emptyDCOM';
		$ret['empty15'] = 'emptyDCOM';
		$ret['empty16'] = 'emptyDCOM';
		$ret['empty17'] = 'emptyDCOM';
		//DCOM


		$ret['cuit'] = 'cuit';
		//$ret['namePrincipalContact'] = 'plain';
		return $ret;
	}

	
	/**
	 * Returns the company category, in string
	 * @return string
	 */ 
        public function getCompanyName()
	{
		return $this->companyName ? $this->companyName : "No";
	}
        
        public function getIdCompany()
	{
		return $this->idCompany ? $this->idCompany : 0;
	}

	public function getXlsAttributes()
	{
		return array(
			array('attribute'=>'company'),
			array('attribute'=>'companyName'),
			array('attribute'=>'cuit'),
		);
	}

	public function delete()
	{

			$criteria = new CDbCriteria();
			$criteria->condition = 'idCompany = :id';
			$criteria->params[':id'] = $this->idCompany;

			return parent::delete();

	}
	public function getCalification()
	{
		if($this->calification == 0)
		{
			return "Sin calificar";
		}
		else
		{
			return $this->calification;
		}
	}
	public static function getCompanyDocument($company)
	{
		$sql = "SELECT idProviderDocument,d.idDocument,documentName,idProviderDocumentStatus,date,emisionDate,url FROM provider_document as pd JOIN document as d on pd.idDocument=d.idDocument where idCompany = '$company' ORDER BY idProviderDocument DESC;";
        $command=Yii::app()->db->createCommand($sql);
        return $data=$command->queryAll();
	}
	public function updateCalification($company,$calification,$reason)
	{
		$reason = str_replace("_"," ",$reason);
		$sql = "UPDATE company set calification =".$calification.", reasonCalification='".$reason."' where idCompany =".$company.";";
        $command=Yii::app()->db->createCommand($sql);
		$command->query();
		return true;
	}
	public function updateDocumentation($company,$document)
	{
		
		$tempDocumentArray = explode("_",$document);		
		for($i = 0; $i<count($tempDocumentArray); $i++)
		{
			$documentArray = explode("o",$tempDocumentArray[$i]);
			$model = ProviderDocument::model()->find("idProvider=".$documentArray[0]." and idDocument=".$documentArray[1]);
			if(isset($model))
			{
			$model->idProviderDocumentStatus = $documentArray[2];
			$model->date = date('Y-m-d');
			$model->save();
			}
			else
			{
				$model = new ProviderDocument();
				$model->idProvider = $documentArray[0];
				$model->idDocument = $documentArray[1];
				$model->idProviderDocumentStatus = $documentArray[2];
				$model->date = date('Y-m-d');
				$model->save();
			}
		}
	}
	public function getCompanyOperations($company)
	{
		//$model = new Invoice();
		//$model->findAll('idCompanyProvider ='.$company);
		$sql = "SELECT numberInvoice,invoiceTotalAmount,dateEmission,dateExpiration, datePayment,description,companyName FROM invoice as i JOIN invoicestatus as ist on i.idStatusInvoice=ist.idInvoiceStatus JOIN company as c on c.idCompany=i.idCompanyClient where idCompanyProvider = '$company';";
        $command=Yii::app()->db->createCommand($sql);
        return $data=$command->queryAll();
	}

}
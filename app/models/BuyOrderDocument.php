<?php

/**
 * This is the model class for table "buyOrderDocument".
 *
 * The followings are the available columns in table 'company':
 * @property integer $idRubro
 * @property string $nombre
 * @property integer $status
 */
class BuyOrderDocument extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Licitaciones the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'buy_order_document';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('idBuyOrderDocument, documentType, idBuyOrder, visto', 'numerical', 'integerOnly'=>true),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'buyOrderDocumentHes' => array(self::HAS_MANY, 'BuyOrderDocumentHes', 'idBuyOrderDocument'),
			'buyOrderDocumentMigo' => array(self::HAS_MANY, 'BuyOrderDocumentMigo', 'idBuyOrderDocument'),
			'buyOrder' => array(self::BELONGS_TO, 'BuyOrder', 'idBuyOrder'),
			'buyOrderDocumentStatus' => array(self::BELONGS_TO, 'BuyOrderDocumentStatus', 'idBuyOrderDocumentStatus'),
			'paymentOrder' => array(self::BELONGS_TO, 'PaymentOrder', 'idPaymentOrder'),
			
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
            'idBuyOrderDocument' => Yii::t('app', 'idBuyOrder'),
		);
	}

	public function scopes() {
		return array(
			'findAllWithOrder' => array('order' => 'idBuyOrderDocument ASC'),
		);
	}
	
	public function getHesAssociated() {
		$hes = array();
		foreach($this->buyOrderDocumentHes as $value) {
			if ($value->hes->number != 0 ) {
                array_push($hes, $value->hes);
            }
		}
		return $hes;
	}
	public function getMigoAssociated() {
		$hes = array();
		foreach($this->buyOrderDocumentMigo as $value) {
			if ($value->migo->number != 0 ) {
                array_push($hes, $value->migo);
            }
		}
		return $hes;
	}

	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('idBuyOrder',$this->idBuyOrder);
        $criteria->compare('idFactura',$this->nroFactura,true);    
		$criteria->compare('visto',$this->view,true);


		$sort = new CSort();
		$sort->defaultOrder = 'idBuyOrder ASC';

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'sort'=>$sort,
			'pagination'=>array(
				'pageSize'=>app()->controller->getItemsPerPage()
			),
		));
	}
}
<?php
/**
 * This is the model class for table "company".
 *
 * The followings are the available columns in table 'company':
 * @property integer $idPaymentOrder
 * @property string $pdf
 * @property string $comment
 * @property int view
 * @property string date
 */
class PaymentOrder extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return PaymentOrder the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'payment_order';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('idPaymentOrder, view', 'numerical', 'integerOnly'=>true),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'buyOrderDocuments' => array(self::HAS_MANY, 'BuyOrderDocument', 'idPaymentOrder'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
            'idPaymentOrder' => Yii::t('app', 'idPaymentOrder'),
		);
	}

	public function scopes() {
		return array(
			'findAllWithOrder' => array('order' => 'idPaymentOrder ASC'),
		);
	}
}
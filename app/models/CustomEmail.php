<?php

/**
 * This is the model class for table "CustomEmail".
 *
 * The followings are the available columns in table 'CustomEmail':
 * @property integer $idCustomEmail
 * @property string $subject
 * @property string $body
 */
class CustomEmail extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return CustomEmail the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'custom_email';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('idCustomEmail', 'numerical', 'integerOnly'=>true),
            array('subject, body', 'length', 'max'=>5000),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        //
		return array(
           'licitacionStatus' => array(self::BELONGS_TO, 'LicitacionesStatus', 'status'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'idCustomEmail' => Yii::t('app', 'Id email personalizado'),
            'subject' => Yii::t('app', 'Asunto'),
            'body' => Yii::t('app', 'Cuerpo'),
		);
	}

	public function scopes() {
		return array(
			'findAllWithOrder' => array('order' => 'idCustomEmail ASC'),
		);
	}
	/*
	public function defaultScope()
	{
		return array(
			'condition' => 'status = 1'
		);
	}*/

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */

    public function getList() {

        $sql = "SELECT * FROM idCustomEmail;";
        $command=Yii::app()->db->createCommand($sql);
		return $data=$command->queryAll();
    }



	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('idCustomEmail',$this->idCustomEmail);
        $criteria->compare('subject',$this->subject,true);
        $criteria->compare('body',$this->body,true);

		$sort = new CSort();
		$sort->defaultOrder = 'idCustomEmail ASC';

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'sort'=>$sort,
			'pagination'=>array(
				'pageSize'=>app()->controller->getItemsPerPage()
			),
		));
	}
	
	public function delete()
	{

			$criteria = new CDbCriteria();
			$criteria->condition = 'idCustomEmail = :id';
			//CompanyContact::model()->deleteAll($criteria);
			//CompanyAddress::model()->deleteAll($criteria);
			return parent::delete();
    }

}
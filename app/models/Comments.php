<?php

/**
 * This is the model class for table "comments".
 *
 * The followings are the available columns in table 'comments':
 * @property integer $idComments
 * @property string $description
 * @property integer $idCommentType
 * @property integer $idUser
 * @property string $dateComments
 * @property integer $statusComments
 * @property integer $idRefenceTo
 *
 * The followings are the available model relations:
 * @property CommentType $idCommentType0
 * @property Comments $idRefenceTo0
 * @property Comments[] $comments
 * @property User $idUser0
 * @property CommentsStatus $statusComments0
 */
class Comments extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Comments the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'comments';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			//array('dateComments', 'required'),
			array('idCommentType, idUser, statusComments, idRefenceTo', 'numerical', 'integerOnly'=>true),
			array('description', 'length', 'max'=>500),
			array('dateComments', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('idComments, description, idCommentType, idUser, dateComments, statusComments, idRefenceTo', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'idCommentType0' => array(self::BELONGS_TO, 'CommentType', 'idCommentType'),
			'idRefenceTo0' => array(self::BELONGS_TO, 'Comments', 'idRefenceTo'),
			'comments' => array(self::HAS_MANY, 'Comments', 'idRefenceTo'),
			'idUser0' => array(self::BELONGS_TO, 'User', 'idUser'),
			'statusComments0' => array(self::BELONGS_TO, 'CommentsStatus', 'statusComments'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'idComments' => 'Id Comentario',
			'description' => 'Descripción',
			'idCommentType' => 'Tipo',
			'idUser' => 'Usuario',
			'dateComments' => 'Fecha',
			'statusComments' => 'Estado',
			'idRefenceTo' => 'Referencia',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('idComments',$this->idComments);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('idCommentType',$this->idCommentType);
		$criteria->compare('idUser',$this->idUser);
		$criteria->compare('dateComments',$this->dateComments,true);
		$criteria->compare('statusComments',$this->statusComments);
		$criteria->compare('idRefenceTo',$this->idRefenceTo);
		$criteria->order = 'dateComments DESC';
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'pagination' => array(
				'pageSize' => app()->controller->getItemsPerPage()
			),
		));
	}

	public function getStatusName()
    {
    	return isset ($this->statusComments0) && $this->statusComments0 instanceof CommentsStatus ? $this->statusComments0->description : "N/A";
    }

    public function getUserName()
    {
    	return isset ($this->idUser0) && $this->idUser0 instanceof User ? $this->idUser0->email : "N/A";
    }

    public function getTypeName()
    {
    	return isset ($this->idCommentType0) && $this->idCommentType0 instanceof CommentType ? $this->idCommentType0->description : "N/A";
    }

    public function getCommentByReference()
    {
    	return isset ($this->idRefenceTo) && $this->idRefenceTo0 instanceof Comments && $this->idCommentType == 2 ? $this->idRefenceTo0->description : "";
    }
}
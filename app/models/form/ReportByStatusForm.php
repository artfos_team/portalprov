<?php

/**
 * ReportByStatusForm class.
 * @author  juan
 */
class ReportByStatusForm extends BaseMonthYearFilterForm
{
    /**
     * Id of the company to filter
     * @var int
     */
    public $idCompany;

    /**
     * Programs that are included in the search
     * @var array
     */
    public $programs;

    /**
     * Initialize instance by setting the corresponding programs in the internal variable $programs
     */
    public function init()
    {
        $this->programs = CHtml::listData(Program::model()->findAll(array('order'=>'program')), 'idProgram', 'idProgram');
    }

    /**
     * Declares the validation rules.
     */
    public function rules()
    {
        return array_merge(parent::rules(), array(
            array('idCompany', 'numerical', 'integerOnly'=>true),
            array('programs', 'safe'),
        ));
    }

    public function attributeLabels()
    {
        return array(
            'idCompany' => 'Empresa',
            'programs' => 'Programas',
        );
    }

    /**
     * Gets the data for the report with the filters applied
     * @return array|void
     */
    public function getData()
    {
        $participantJoin = 'INNER JOIN participant p ON pp.idParticipant = p.idParticipant
                AND p.participantDeleted = 0';

        if(isset($this->idCompany) && $this->idCompany > 0) {
            $participantJoin .= ' AND p.idCompany = ' . $this->idCompany;
        }

        $sql = 'SELECT pp.status as programStatus, COUNT(*) as cant FROM participant_program pp ';
        $sql .= $participantJoin;
        $sql .= ' WHERE pp.idProgram IN ('. implode(',', $this->programs) . ') ';
        $sql .= $this->getDateQuery();
        $sql .= ' GROUP BY pp.status ';
        $data = app()->db->createCommand($sql)->queryAll();
        return $data;
    }

    /**
     * Override parent implementation to define custom date filtering SQL
     */
    protected function getDateQuery()
    {
        $sql = '';
        if($this->dateParamsSet()) {
            $sql = ' AND pp.startDate BETWEEN ' . $this->getDateFromTimestamp() . ' AND ' . $this->getDateToTimestamp() . ' ' ;
        }

        return $sql;
    }

}
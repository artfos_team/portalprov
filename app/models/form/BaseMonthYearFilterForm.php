<?php
/**
 * Abstract form class to provide easy searchable by month/year functionalities. Refactored from
 * common functions of Report and Graphs forms
 * @author juan
 */

abstract class BaseMonthYearFilterForm extends CFormModel
{
    /**
     * Dates from
     * @var int
     */
    public $monthFrom;
    public $yearFrom;

    /**
     * Dates to
     * @var int
     */
    public $monthTo;
    public $yearTo;

    /**
     * Months
     * @var array
     */
    static $months = array(1,2,3,4,5,6,7,8,9,10,11,12);

    /**
     * Years
     * @var array
     */
    static $years = array(2012,2013,2014,2015,2016,2017,2018,2019,2020,2021,2022,2023,2024,2025);


    /**
     * Returns the sql for validating the dates for the reports
     * @return string
     */
    abstract protected function getDateQuery();

    public function rules()
    {
        return array(
            array('monthFrom, monthTo, yearFrom, yearTo', 'numerical', 'integerOnly'=>true)
        );
    }

    /**
     * Checks that the date parameters are correctly set
     * @return bool
     */
    protected function dateParamsSet()
    {
        //var_dump($this->monthFrom, $this->yearTo, $this->monthTo, $this->yearFrom);
        return isset($this->monthFrom) && strlen($this->monthFrom) > 0 &&
            isset($this->yearFrom) && strlen($this->yearFrom) > 0 &&
            isset($this->yearTo) && strlen($this->yearTo) > 0 &&
            isset($this->monthTo) && strlen($this->monthTo) > 0;
    }


    /**
     * Return the year for the index retrieved by the form
     * @param $index
     * @return mixed
     * @throws CException
     */
    protected function getYear($index)
    {
        if(isset(self::$years[$index])) {
            return self::$years[$index];
        }

        throw new CException('Invalid date index');
    }

    /**
     * Return the month for the index retrieved by the form
     * @param $index
     * @return mixed
     * @throws CException
     */
    protected function getMonth($index)
    {
        if(isset(self::$months[$index])) {
            return self::$months[$index];
        }

        throw new CException('Invalid date index');
    }

    /**
     * Return the timestamp for the current date from attribute
     * @return int
     * @throws CException
     */
    protected function getDateFromTimestamp()
    {
        return mktime(0,0,0,$this->getMonth($this->monthFrom),1,$this->getYear($this->yearFrom));
    }

    /**
     * Return the timestamp for the current date to attribute
     * @return int
     * @throws CException
     */
    protected function getDateToTimestamp()
    {
        return mktime(0,0,0,$this->getMonth($this->monthTo),1,$this->getYear($this->yearTo));
    }

    /**
     * Returns the available months
     * @return array
     */
    public static function getMonths()
    {
        return self::$months;
    }

    /**
     * Returns the available years
     * @return array
     */
    public static function getYears()
    {
        return self::$years;
    }
}
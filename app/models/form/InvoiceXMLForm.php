<?php

/**
 * LoginForm class.
 * LoginForm is the data structure for keeping
 * user login form data. It is used by the 'login' action of 'SiteController'.
 */
class InvoiceXMLForm extends CFormModel
{

    /**
     * Declares the validation rules.
     * The rules state that username and password are required,
     * and password needs to be authenticated.
     */
    public function rules()
    {
        return array(
                        array('invoice_xml', 'file', 'types'=>'xml', 'allowEmpty'=>true, 'message'=>'XML files'),
                        array('order_pdf', 'file', 'types'=>'pdf', 'allowEmpty'=>true, 'message'=>'PDF files'),

        );
    }

    /**
     * Declares attribute labels.
     */
    public function attributeLabels()
    {
        return array(
            'invoice_xml' => 'Archivo XML de factura',
            'order_pdf' => 'Archivo PDF de la Orden',
        );
    }

   
    
}

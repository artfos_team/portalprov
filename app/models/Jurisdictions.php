<?php

/**
 * This is the model class for table "jurisdictions".
 *
 * The followings are the available columns in table 'jurisdictions':
 * @property integer $idJurisdictions
 * @property string $jurisdictions
 * @property string $codeERP
 */
class Jurisdictions extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Jurisdictions the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'jurisdictions';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('idJurisdictions', 'numerical', 'integerOnly'=>true),
			array('jurisdictions', 'length', 'max'=>50),
			array('codeERP', 'length', 'max'=>2),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        //province
		return array(
           'licitacionStatus' => array(self::BELONGS_TO, 'LicitacionesStatus', 'status'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'idJurisdictions' => Yii::t('app', 'Id jurisdicciones'),
			'jurisdictions' => Yii::t('app', 'Jurisdicciones'),
			'codeERP' => Yii::t('app', 'Código'),

		);
	}

	public function scopes() {
		return array(
			'findAllWithOrder' => array('order' => 'jurisdictions ASC'),
		);
	}
	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */

    public function getList() {

        $sql = "SELECT * FROM jurisdictions;";
        $command=Yii::app()->db->createCommand($sql);
        return $data=$command->queryAll();
    }



	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('idJurisdictions',$this->idJurisdictions);
		$criteria->compare('jurisdictions',$this->jurisdictions,true);
		$criteria->compare('codeERP',$this->codeERP,true);


		$sort = new CSort();
		$sort->defaultOrder = 'jurisdictions ASC';

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'sort'=>$sort,
			'pagination'=>array(
				'pageSize'=>app()->controller->getItemsPerPage()
			),
		));
	}
	
	public function delete()
	{

			$criteria = new CDbCriteria();
			$criteria->condition = 'idJurisdictions = :id';
			return parent::delete();
    }

}
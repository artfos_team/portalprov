<?php

/**
 * This is the model class for table "document".
 *
 * The followings are the available columns in table 'document':
 * @property integer $idEmailReceiver
 * @property string $email
 * @property integer $idEmail
 * The followings are the available model relations:
 * @property Email $emailToSend
 */
class EmailReceiver extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return EmailReceiver the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
     * @return array the behavior configurations (behavior name=>behavior configuration)
     */
    public function behaviors()
    {
        return array_merge(parent::behaviors(), array(
            'PicturableBehavior' => array(
				'class'=>'application.components.behaviors.PicturableBehavior',
				'attributeNames' => array('logo'),
				'dirName' => 'document',
			),
        ));
    }

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'email_receiver';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('idEmail', 'required'),
			array('email', 'length', 'max'=>100),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'emailToSend' => array(self::BELONGS_TO, 'Email', 'idEmail'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'idEmailReceiver' => Yii::t('app', 'idReceptor'),
			'email' => Yii::t('app', 'Correo electrónico'),
			'idEmail' => Yii::t('app', 'Id Email')
		);
	}

	public function scopes() {
		return array(
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */

	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('idEmailReceiver',$this->idEmailReceiver,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('idEmail',$this->idEmail);


		$sort = new CSort();

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'sort'=>$sort,
			'pagination'=>array(
				'pageSize'=>app()->controller->getItemsPerPage()
			),
		));
	}
	
	/**
	 * Returns the company category, in string
	 * @return string
	 */
	public function getXlsAttributes()
	{
		return array(
			array('attribute'=>'subject'),
		);
    }
}
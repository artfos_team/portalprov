<?php

/**
 * This is the model class for table "agreement_regime_type".
 *
 * The followings are the available columns in table 'agreement_regime_type':
 * @property integer $idAgreementRegimeType
 * @property string $agreementRegimeType
 */
class AgreementRegimeType extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return AgreementRegimeType the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'agreement_regime_type';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('idAgreementRegimeType', 'numerical', 'integerOnly'=>true),
            array('agreementRegimeType', 'length', 'max'=>50),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        //agreementRegime
		return array(
           'country' => array(self::BELONGS_TO, 'country', 'idCountry'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'idAgreementRegimeType' => Yii::t('app', 'Id tipo régimen de convenio'),
			'agreementRegimeType' => Yii::t('app', 'Tipo régimen de convenio'),
		);
	}

	public function scopes() {
		return array(
			'findAllWithOrder' => array('order' => 'agreementRegimeType ASC'),
		);
	}
	/*
	public function defaultScope()
	{
		return array(
			'condition' => 'status = 1'
		);
	}*/

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */

    public function getList() {

        $sql = "SELECT * FROM agreement_regime_type;";
        $command=Yii::app()->db->createCommand($sql);
        return $data=$command->queryAll();
    }



	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('idAgreementRegimeType',$this->idAgreementRegimeType);
		$criteria->compare('agreementRegimeType',$this->agreementRegimeType,true);

		$sort = new CSort();
		$sort->defaultOrder = 'agreementRegimeType ASC';

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'sort'=>$sort,
			'pagination'=>array(
				'pageSize'=>app()->controller->getItemsPerPage()
			),
		));
	}
	
	public function delete()
	{

			$criteria = new CDbCriteria();
			$criteria->condition = 'idAgreementRegimeType = :id';
			//CompanyContact::model()->deleteAll($criteria);
			//CompanyAddress::model()->deleteAll($criteria);
			return parent::delete();
	}
	
	public function getShortValue($id)
	{
        $sql = "SELECT value FROM agreement_regime_type where idAgreementRegimeType =". $id;
        $command=Yii::app()->db->createCommand($sql);
		$data=$command->queryAll();
		$data = substr( $data[0]['value'] , 0, 50);
		return $data."...";
	}

}
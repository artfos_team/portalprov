<?php

/**
 * This is the model class for table "provider_document".
 *
 * The followings are the available columns in table 'provider_document':
 * @property integer $idProviderDocument
 * @property string $url
 * @property integer $idCompany
 * @property integer $idDocument
 * @property integer $idProviderDocumentStatus
 * @property integer $emisionDate
 * @property integer $date
 * @property integer $mensajeRechazo
 * The followings are the available model relations:
 * @property CompanyCategory $idCompanyCategory0
 * @property CompanyAddress[] $companyAddresses
 * @property CompanyContact[] $companyContacts
 * @property Participant[] $participants
 */
class ProviderDocument extends CActiveRecord
{

	public $_logo;

	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return ProviderDocument the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
     * @return array the behavior configurations (behavior name=>behavior configuration)
     */

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'provider_document';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('idProviderDocument, idProviderDocumentStatus, idCompany, idDocument', 'numerical', 'integerOnly'=>true),
			array('url, mensajeRechazo, date, emisionDate', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			//array('idCompany, company, companyName, logo, idCompanyCategory, cuit, requiresPurchaseOrder', 'safe', 'on'=>'search'),
			//array('_logo', 'file', 'types'=>'jpg, gif, png', 'allowEmpty'=>true),
		);
	}
	
	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		
		return array(
			'document' => array(self::BELONGS_TO, 'Document', 'idDocument'),
			'status' => array(self::BELONGS_TO, 'ProviderDocumentStatus', 'idProviderDocumentStatus'),
			'company' => array(self::BELONGS_TO, 'Company', 'idCompany')
		);
	}
	
	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'idProviderDocument' => Yii::t('app', 'Id'),
			'idDocument' => Yii::t('app', 'Documento'),
			'idCompany' => Yii::t('app', 'Proveedor'),
            'date' => Yii::t('app', 'fecha'),
		);
	}

	public function scopes() {
		return array(
			'findAllWithOrder' => array('order' => 'idProviderDocument ASC'),
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */

    public function getList() {

        //return $this->findAll();

        $sql = "SELECT * FROM provider_document;";
        $command=Yii::app()->db->createCommand($sql);
        return $data=$command->queryAll();
    }



	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('idProviderDocument',$this->idProviderDocument);
		$criteria->compare('idDocument',$this->idDocument,true);
		$criteria->compare('idCompany',$this->idCompany,true);
		$criteria->compare('date',$this->date,true);
		$criteria->compare('active',$this->active,true);


		$sort = new CSort();
		$sort->defaultOrder = 'idProviderDocument ASC';

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'sort'=>$sort,
			'pagination'=>array(
				'pageSize'=>app()->controller->getItemsPerPage()
			),
		));
	}
	
	/**
	 * Returns the company category, in string
	 * @return string
	 */
	public function delete()
	{
			throw new Exception("funcion delete");
			
			$criteria = new CDbCriteria();
			$criteria->condition = 'idProviderDocument = :id';
			$criteria->params[':id'] = $this->idDocument;

			//CompanyContact::model()->deleteAll($criteria);
			//CompanyAddress::model()->deleteAll($criteria);
			return parent::delete();
    }
    
    public function getActive()
    {
        if($this->active == 1)
        {
            return "Si";
        }else{
            return "No";
        }
	}
	public function getDocumentName($i)
	{
		
        $sql = "SELECT documentName FROM document where idDocument= '$i';";
        $command=Yii::app()->db->createCommand($sql);
		$data=$command->queryColumn();
		return $data[0];
		//return $this->document->documentName;
	}
	public function getDocumentNameCount()
	{
		$sql = "SELECT count(documentName) FROM document";
        $command=Yii::app()->db->createCommand($sql);
		$data=$command->queryColumn();
		return $data[0];
		//throw new Exception($data[0], 1);
		
	}
	public function getDocumentProviderStatus($company, $i)
	{
		$sql = "SELECT status FROM provider_document where idCompany = '$company' and idDocument='$i'";
        $command=Yii::app()->db->createCommand($sql);
		$data=$command->queryColumn();
		if(isset($data[0]) && $data[0] == 1)
		{
			return true;
		}
		else{
			return false;
		}
	}
}
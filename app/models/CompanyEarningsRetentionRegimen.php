<?php

/**
 * This is the model class for table "company_earnings_retention_regimen".
 *
 * The followings are the available columns in table 'company_earnings_retention_regimen':
 * @property integer $idEarningsRetentionRegimen
 * @property integer $idTaxData
 * @property integer $idRetentionRegime
 *
 * The followings are the available model relations:
 * @property CompanyTaxData $idTaxData0
 * @property RetentionRegime $idRetentionRegime0
 */
class CompanyEarningsRetentionRegimen extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'company_earnings_retention_regimen';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('idTaxData, idRetentionRegime', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('idEarningsRetentionRegimen, idTaxData, idRetentionRegime', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'idTaxData0' => array(self::BELONGS_TO, 'CompanyTaxData', 'idTaxData'),
			'idRetentionRegime0' => array(self::BELONGS_TO, 'RetentionRegime', 'idRetentionRegime'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'idEarningsRetentionRegimen' => 'Id Earnings Retention Regimen',
			'idTaxData' => 'Id Tax Data',
			'idRetentionRegime' => 'Id Retention Regime',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('idEarningsRetentionRegimen',$this->idEarningsRetentionRegimen);
		$criteria->compare('idTaxData',$this->idTaxData);
		$criteria->compare('idRetentionRegime',$this->idRetentionRegime);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return CompanyEarningsRetentionRegimen the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}

<?php

/**
 * This is the model class for table "logs".
 *
 * The followings are the available columns in table 'logs':
 * @property integer $idLog
 * @property integer $idUser
 * @property integer $levelLog
 * @property string $dateLog
 * @property string $commentaryLog
 * @property string $source
 *
 * The followings are the available model relations:
 * @property User $idUser0
 */
class Logs extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Logs the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'logs';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('idUser, levelLog', 'numerical', 'integerOnly'=>true),
			array('commentaryLog, source', 'length', 'max'=>100),
			array('dateLog', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('idLog, idUser, levelLog, dateLog, commentaryLog, source', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'idUser0' => array(self::BELONGS_TO, 'User', 'idUser'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'idLog' => 'Id Log',
			'idUser' => 'Id User',
			'levelLog' => 'Level Log',
			'dateLog' => 'Date Log',
			'commentaryLog' => 'Commentary Log',
			'source' => 'Source',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria = new CDbCriteria();

		$criteria->compare('idLog', $this->idLog);
		$criteria->compare('idUser', $this->idUser);
		$criteria->compare('levelLog', $this->levelLog);
		$criteria->compare('dateLog', $this->dateLog,true);
		$criteria->compare('commentaryLog',$this->commentaryLog,true);
		$criteria->compare('source',$this->source,true);
		$criteria->order = 'dateLog DESC';
		return new CActiveDataProvider($this , array(
			'criteria'=>$criteria,
			'pagination' => array(
				'pageSize' => app()->controller->getItemsPerPage()
			),
		));
	}

	public function getUserName()
    {
        return isset ($this->idUser0) && $this->idUser0 instanceof User ? $this->idUser0->name : "N/A";
	}

	public function levelDescription()
	{
		if($this->levelLog==1)
		{
			return 'Diario';
		}
		else if($this->levelLog==2)
		{
			return 'Error SAP';
		}
		else
		{
			return 'Auditoria';
		}
	}

	public static function log($levelLog, $commentaryLog, $source, $idUser = false)
	{
		$newLog = new Logs();
		if(!$idUser)
		{
			$newLog->idUser = user()->idUser;
		}
		else if($idUser>0)
		{
			$newLog->idUser = $idUser;	
		}	
		else if ($idUser == 'null')
		{
		    $newLog->idUser = null;
		}
        $newLog->levelLog = $levelLog;
        $newLog->commentaryLog = $commentaryLog;
        $newLog->source = $source;
		$newLog->save();
	}
}
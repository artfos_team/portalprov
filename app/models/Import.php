<?php

/**
 * This is the model class for table "import".
 *
 * The followings are the available columns in table 'sap_parameters':
 * @property int $idImport
 * @property date $dateImport
 * @property int $totalImport
 * @property int $errorImport
 * @property int $updateImport
 * @property int $createImport
 */
class Import extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Parameters the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'import';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('idImport, totalImport, errorImport, updateImport, createImport', 'numerical', 'integerOnly'=>true),
			array('dateImport','length','max'=>100),
			array('idImport, dateImport, totalImport, errorImport, updateImport, createImport', 'safe', 'on'=>'search'),
		);
			// The following rule is used by search().		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'errors' => array(self::HAS_MANY, 'ImportError', 'idImport'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'idImport' => 'ID',
			'dateImport' => 'Fecha y Hora',
			'totalImport' => 'Cantidad total',
			'errorImport' => 'Cantidad de errores',
			'updateImport' => 'Cantidad de actualizaciones',
			'createImport' => 'Facturas nuevas'
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.
		
		$criteria=new CDbCriteria;

		$criteria->compare('idImport',$this->idImport);
		$criteria->compare('dateImport',$this->dateImport);
		$criteria->compare('totalImport',$this->totalImport);
		$criteria->compare('createImport',$this->createImport);
		$criteria->compare('updateImport',$this->updateImport);

		if($this->errorImport == 'error') {
			$criteria->addCondition('errorImport > 0');
		} else if ($this->errorImport == 'noError') {
			$criteria->addCondition('errorImport = 0');
		} else if ($this->errorImport == '') {
			$criteria->compare('errorImport', $this->errorImport);
		}

		$criteria->order = "idImport DESC";

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}
<?php

/**
 * This is the model class for table "sap_parameters".
 *
 * The followings are the available columns in table 'sap_parameters':
 * @property int $idEmailParameters
 * @property string $smtp
 * @property string $user
 * @property string $userPsw
 * @property string $fromName
 * @property string	$hostForMails
 */
class EmailParameters extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Parameters the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'email_parameters';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('smtp, user, userPsw, fromName, port, security', 'required'),
			array('smtp, user, userPsw', 'length', 'max'=>50),
			array('hostForMails', 'length', 'max'=>200),
			array('smtp, user, userPsw, fromName, port, security', 'safe', 'on'=>'search'),
		);
			// The following rule is used by search().		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'smtp' => 'SMTP',
			'fromName' => 'Emisor',
			'user' => 'Usuario',
			'userPsw' => 'Contraseña',
			'hostForMails' => 'Host para los emails',
			'port' => 'Puerto',
			'security' => 'Modo seguridad'
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('smtp',$this->smtp);
		$criteria->compare('user',$this->user,true);
		$criteria->compare('userPsw',$this->userPsw);
		$criteria->compare('fromName',$this->fromName);
		$criteria->compare('hostForMails',$this->hostForMails);
		$criteria->compare('port',$this->port);
		$criteria->compare('security',$this->security);


		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'pagination'=>array(
				'pageSize'=>app()->controller->getItemsPerPage()
			),
		));
	}

	public function encriptar($cadena){
        $key='';  // Una clave de codificacion, debe usarse la misma para encriptar y desencriptar
        $encrypted = base64_encode(openssl_encrypt($cadena, 'AES-256-CBC', md5($key)));
        $encrypted = str_replace(array('+','/','='),array('-','_','.'),$encrypted); 
        return $encrypted; //Devuelve el string encriptado
    }

    public function desencriptar($cadena){
         $key='';  // Una clave de codificacion, debe usarse la misma para encriptar y desencriptar
         $cadena = str_replace(array('-','_','.'),array('+','/','='),$cadena); 
         $decrypted = rtrim(openssl_decrypt(base64_decode($cadena), 'AES-256-CBC', md5($key), true,$iv ), "\0");
         
        return $decrypted;  //Devuelve el string desencriptado
    }
}
<?php

/**
 * This is the model class for table "sap_parameters".
 *
 * The followings are the available columns in table 'sap_parameters':
 * @property int $idImportError
 * @property int $idImport
 * @property string $description
 * @property int $status
 */
class ImportError extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Parameters the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'import_error';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('idImport, idImportError, status', 'numerical', 'integerOnly'=>true),
			array('description','length', 'max' => 300),
			array('idImport, idImportError, description, status', 'safe', 'on'=>'search'),
		);
			// The following rule is used by search().		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'import' => array(self::BELONGS_TO, 'Import', 'idImport'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'idImportError' => 'ID',
			'idImport' => 'Numero Importación',
			'description' => 'Descripción',
			'status' => 'Estado'
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('idImport',$this->idImport);
		$criteria->compare('idImportError',$this->idImportError);
		$criteria->compare('description', $this->description);
		$criteria->compare('status',$this->status);
		$criteria->order = "idImportError DESC";

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public function getImportDate() {
		return $this->import->dateImport;
	}

	public function getStatusDescription() { 
		return $this->status == 0 ? 'Sin arreglar' : 'Arreglado';
	}
}
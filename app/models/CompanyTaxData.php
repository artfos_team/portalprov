<?php

/**
 * This is the model class for table "company_tax_data".
 *
 * The followings are the available columns in table 'company_tax_data':
 * @property integer $idTaxData
 * @property string $pdfIb
 * @property string $ibType
 * @property integer $idAgreementRegimeType
 * @property string $pdfCM05
 * @property integer $jurisdictionHeadquarters
 * @property string $earningsRetentionType
 * @property integer $retentionRegimen
 * @property integer $idEarningCategoryRetain
 * @property string $earningsExclusionNumber
 * @property string $earningsExclusionFrom
 * @property string $earningsExclusionTo
 * @property string $earningsExclusionPdf
 * @property integer $sussEmployeer
 * @property integer $sussRetentionRegimen
 * @property integer $sussIdCategoryRetain
 * @property string $sussExclusionNumber
 * @property string $sussExclusionFrom
 * @property string $sussExclusionTo
 * @property string $sussExclusionPdf
 * @property integer $ivaRetentionAgent
 * @property string $ivaResolutionPdf
 * @property integer $ivaIdRetentionRegime
 * @property integer $ivaIdCategoryRetain
 * @property string $ivaExclusionNumber
 * @property string $ivaExclusionFrom
 * @property string $ivaExclusionTo
 * @property string $ivaExclusionPdf
 * @property integer $idCompanyDataStatus
 * @property string $observation
 * @property integer $idUser
 * @property string $modifDate
 *
 * The followings are the available model relations:
 * @property Company[] $companies
 * @property CompanyEarningsRetentionRegimen[] $companyEarningsRetentionRegimens
 * @property CompanyJurisdictionData[] $companyJurisdictionDatas
 * @property CompanyPdfExemptions[] $companyPdfExemptions
 * @property AgreementRegimeType $idAgreementRegimeType0
 * @property RetentionRegime $retentionRegimen0
 * @property CategoryRetain $idEarningCategoryRetain0
 * @property CategoryRetain $ivaIdCategoryRetain0
 * @property CategoryRetain $sussIdCategoryRetain0
 * @property RetentionRegime $ivaIdRetentionRegime0
 * @property Jurisdictions $jurisdictionHeadquarters0
 * @property CompanyDataStatus $idCompanyDataStatus0
 * @property User $idUser0
 */
class CompanyTaxData extends CActiveRecord implements ProviderDataExport
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'company_tax_data';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('idAgreementRegimeType, jurisdictionHeadquarters, retentionRegimen, idEarningCategoryRetain, sussEmployeer, sussRetentionRegimen, sussIdCategoryRetain, ivaRetentionAgent, ivaIdRetentionRegime, ivaIdCategoryRetain, idCompanyDataStatus, idUser', 'numerical', 'integerOnly'=>true),
			array('pdfIb, pdfCM05, earningsExclusionPdf, sussExclusionPdf, ivaResolutionPdf, ivaExclusionPdf, observation', 'length', 'max'=>400),
			array('ibType, earningsRetentionType', 'length', 'max'=>20),
			array('earningsExclusionNumber, sussExclusionNumber, ivaExclusionNumber', 'length', 'max'=>30),
			array('earningsExclusionFrom, earningsExclusionTo, sussExclusionFrom, sussExclusionTo, ivaExclusionTo, ivaExclusionFrom', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('idTaxData, pdfIb, ibType, idAgreementRegimeType, pdfCM05, jurisdictionHeadquarters, earningsRetentionType, retentionRegimen, idEarningCategoryRetain, earningsExclusionNumber, earningsExclusionFrom, earningsExclusionTo, earningsExclusionPdf, sussEmployeer, sussRetentionRegimen, sussIdCategoryRetain, sussExclusionNumber, sussExclusionFrom, sussExclusionTo, sussExclusionPdf, ivaRetentionAgent, ivaResolutionPdf, ivaIdRetentionRegime, ivaIdCategoryRetain, ivaExclusionNumber, ivaExclusionFrom, ivaExclusionTo, ivaExclusionPdf, idCompanyDataStatus, observation, idUser', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'companies' => array(self::HAS_MANY, 'Company', 'idTaxData'),
			'companyEarningsRetentionRegimens' => array(self::HAS_MANY, 'CompanyEarningsRetentionRegimen', 'idTaxData'),
			'companyJurisdictionDatas' => array(self::HAS_MANY, 'CompanyJurisdictionData', 'idTaxData'),
			'companyPdfExemptions' => array(self::HAS_MANY, 'CompanyPdfExemptions', 'idTaxData'),
			'idAgreementRegimeType0' => array(self::BELONGS_TO, 'AgreementRegimeType', 'idAgreementRegimeType'),
			'retentionRegimen0' => array(self::BELONGS_TO, 'RetentionRegime', 'retentionRegimen'),
			'idEarningCategoryRetain0' => array(self::BELONGS_TO, 'CategoryRetain', 'idEarningCategoryRetain'),
			'ivaIdCategoryRetain0' => array(self::BELONGS_TO, 'CategoryRetain', 'ivaIdCategoryRetain'),
			'sussIdCategoryRetain0' => array(self::BELONGS_TO, 'CategoryRetain', 'sussIdCategoryRetain'),
			'ivaIdRetentionRegime0' => array(self::BELONGS_TO, 'RetentionRegime', 'ivaIdRetentionRegime'),
			'jurisdictionHeadquarters0' => array(self::BELONGS_TO, 'Jurisdictions', 'jurisdictionHeadquarters'),
			'idCompanyDataStatus0' => array(self::BELONGS_TO, 'CompanyDataStatus', 'idCompanyDataStatus'),
			'idUser0' => array(self::BELONGS_TO, 'User', 'idUser'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'idTaxData' => 'Id Tax Data',
			'pdfIb' => 'Pdf Ib',
			'ibType' => 'Ib Type',
			'idAgreementRegimeType' => 'Id Agreement Regime Type',
			'pdfCM05' => 'Pdf Cm05',
			'jurisdictionHeadquarters' => 'Sede Jurisdiccion',
			'earningsRetentionType' => 'Tipo de retencion de ganancias',
			'retentionRegimen' => 'Id Retention Regime',
			'idEarningCategoryRetain' => 'Id Earning Category Retain',
			'earningsExclusionNumber' => 'Earnings Exclusion Number',
			'earningsExclusionFrom' => 'Earnings Exclusion From',
			'earningsExclusionTo' => 'Earnings Exclusion To',
			'earningsExclusionPdf' => 'Earnings Exclusion Pdf',
			'sussEmployeer' => 'Suss Employeer',
			'sussRetentionRegimen' => 'Suss Retention Regimen',
			'sussIdCategoryRetain' => 'Suss Category Retain',
			'sussExclusionNumber' => 'Suss Exclusion Number',
			'sussExclusionFrom' => 'Suss Exclusion From',
			'sussExclusionTo' => 'Suss Exclusion To',
			'sussExclusionPdf' => 'Suss Exclusion Pdf',
			'ivaRetentionAgent' => 'Iva Retention Agent',
			'ivaResolutionPdf' => 'Iva Resolution Pdf',
			'ivaIdRetentionRegime' => 'Iva Id Retention Regime',
			'ivaIdCategoryRetain' => 'Iva Id Category Retain',
			'ivaExclusionNumber' => 'Iva Exclusion Number',
			'ivaExclusionFrom' => 'Iva Exclusion From',
			'ivaExclusionTo' => 'Iva Exclusion To',
			'ivaExclusionPdf' => 'Iva Exclusion Pdf',
			'idCompanyDataStatus' => 'Id Company Data Status',
			'observation' => 'Observation',
			'idUser' => 'Id User',
			'modifDate' => Yii::t('app', 'Fecha'),
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('idTaxData',$this->idTaxData);
		$criteria->compare('pdfIb',$this->pdfIb,true);
		$criteria->compare('ibType',$this->ibType,true);
		$criteria->compare('idAgreementRegimeType',$this->idAgreementRegimeType);
		$criteria->compare('pdfCM05',$this->pdfCM05,true);
		$criteria->compare('jurisdictionHeadquarters',$this->jurisdictionHeadquarters);
		$criteria->compare('earningsRetentionType',$this->earningsRetentionType,true);
		$criteria->compare('retentionRegimen',$this->retentionRegimen);
		$criteria->compare('idEarningCategoryRetain',$this->idEarningCategoryRetain);
		$criteria->compare('earningsExclusionNumber',$this->earningsExclusionNumber,true);
		$criteria->compare('earningsExclusionFrom',$this->earningsExclusionFrom,true);
		$criteria->compare('earningsExclusionTo',$this->earningsExclusionTo,true);
		$criteria->compare('earningsExclusionPdf',$this->earningsExclusionPdf,true);
		$criteria->compare('sussEmployeer',$this->sussEmployeer);
		$criteria->compare('sussRetentionRegimen',$this->sussRetentionRegimen);
		$criteria->compare('sussIdCategoryRetain',$this->sussIdCategoryRetain);
		$criteria->compare('sussExclusionNumber',$this->sussExclusionNumber,true);
		$criteria->compare('sussExclusionFrom',$this->sussExclusionFrom,true);
		$criteria->compare('sussExclusionTo',$this->sussExclusionTo,true);
		$criteria->compare('sussExclusionPdf',$this->sussExclusionPdf,true);
		$criteria->compare('ivaRetentionAgent',$this->ivaRetentionAgent);
		$criteria->compare('ivaResolutionPdf',$this->ivaResolutionPdf,true);
		$criteria->compare('ivaIdRetentionRegime',$this->ivaIdRetentionRegime);
		$criteria->compare('ivaIdCategoryRetain',$this->ivaIdCategoryRetain);
		$criteria->compare('ivaExclusionNumber',$this->ivaExclusionNumber,true);
		$criteria->compare('ivaExclusionFrom',$this->ivaExclusionFrom,true);
		$criteria->compare('ivaExclusionTo',$this->ivaExclusionTo,true);
		$criteria->compare('ivaExclusionPdf',$this->ivaExclusionPdf,true);
		$criteria->compare('idCompanyDataStatus',$this->idCompanyDataStatus);
		$criteria->compare('observation',$this->observation,true);
		$criteria->compare('idUser',$this->idUser);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public function getExportableAttributes()
	{
		$ret = array();
		//DIMPIB
		$ret['empty1IB'] = 'emptyIB';
		$ret['sociedadIB'] = 'sociedadIB';
		$ret['Tipo de retencionesIB'] = 'retentionTypeIB';
		$ret['Sujeto a retenciónIB'] = 'sujetoRetenIB';
		//$ret['Categoría retenciónIB'] = 'categoryRetainIB';
		$ret['ibType'] = 'categoryRetainIB';
		$ret['Nº identificacion fiscalIB'] = 'nroIdFiscalIB';
		$ret['Indicador de retenciónIB'] = 'indicRetIB';
		$ret['Numero certificado exencionIB'] = 'nroCertifExenIB';
		$ret['Tipo de exencionIB'] = 'tipoExenIB';
		$ret['Fe.inicio exencionIB'] = 'feInicExenIB';
		$ret['Fe.fin exencionIB'] = 'feFinExenIB';
		$ret['empty2IB'] = 'emptyIB';
		$ret['empty3IB'] = 'emptyIB';
		$ret['empty4IB'] = 'emptyIB';
		$ret['empty5IB'] = 'emptyIB';
		$ret['empty6IB'] = 'emptyIB';
		$ret['empty7IB'] = 'emptyIB';
		$ret['empty8IB'] = 'emptyIB';
		$ret['empty9IB'] = 'emptyIB';
		$ret['empty10IB'] = 'emptyIB';
		$ret['empty11IB'] = 'emptyIB';
		$ret['empty12IB'] = 'emptyIB';
		$ret['empty13IB'] = 'emptyIB';
		$ret['empty14IB'] = 'emptyIB';
		$ret['empty15IB'] = 'emptyIB';
		//DIMPIB

		//DIMPRG
		$ret['empty1'] = 'empty';
		$ret['sociedad'] = 'sociedad';
		$ret['Tipo de retenciones'] = 'retentionType';
		$ret['Sujeto a retención'] = 'sujetoReten';
		$ret['Categoría retención'] = 'categoryRetain';
		$ret['Nº identificacion fiscal'] = 'nroIdFiscal';
		$ret['Indicador de retención'] = 'indicRet';
		$ret['earningsExclusionNumber'] = 'exNumber';
		$ret['Tipo de exencion'] = 'tipoExen';
		//$ret['earningsExclusionFrom'] = 'plain';
		$ret['earningsExclusionFrom'] = 'exFrom';
		//$ret['earningsExclusionTo'] = 'plain';
		$ret['earningsExclusionTo'] = 'exTo';
		$ret['empty2'] = 'empty';
		$ret['empty3'] = 'empty';
		$ret['empty4'] = 'empty';
		$ret['empty5'] = 'empty';
		$ret['empty6'] = 'empty';
		$ret['empty7'] = 'empty';
		$ret['empty8'] = 'empty';
		$ret['empty9'] = 'empty';
		$ret['empty10'] = 'empty';
		$ret['empty11'] = 'empty';
		$ret['empty12'] = 'empty';
		$ret['empty13'] = 'empty';
		$ret['empty14'] = 'empty';
		$ret['empty15'] = 'empty';
		$ret['typeIvaCod'] = 'typeIvaCod';
		//DIMPRG

		//DIMPRS
		$ret['empty1RS'] = 'emptyRS';
		$ret['sociedadRS'] = 'sociedadRS';
		$ret['Tipo de retencionesRS'] = 'retentionTypeRS';
		$ret['Sujeto a retenciónRS'] = 'sujetoRetenRS';
		$ret['Categoría retenciónRS'] = 'categoryRetainRS';
		$ret['Nº identificacion fiscalRS'] = 'nroIdFiscalRS';
		$ret['Indicador de retenciónRS'] = 'indicRetRS';
		$ret['sussExclusionNumber'] = 'plainRS';
		$ret['Tipo de exencionRS'] = 'tipoExenRS';
		$ret['sussExclusionFrom'] = 'plainRSexFrom';
		$ret['sussExclusionTo'] = 'plainRSexTo';
		$ret['empty2RS'] = 'emptyRS';
		$ret['empty3RS'] = 'emptyRS';
		$ret['empty4RS'] = 'emptyRS';
		$ret['empty5RS'] = 'emptyRS';
		$ret['empty6RS'] = 'emptyRS';
		$ret['empty7RS'] = 'emptyRS';
		$ret['empty8RS'] = 'emptyRS';
		$ret['empty9RS'] = 'emptyRS';
		$ret['empty10RS'] = 'emptyRS';
		$ret['empty11RS'] = 'emptyRS';
		$ret['empty12RS'] = 'emptyRS';
		$ret['empty13RS'] = 'emptyRS';
		$ret['empty14RS'] = 'emptyRS';
		$ret['empty15RS'] = 'emptyRS';
		$ret['typeIvaCod2'] = 'typeIvaCod2';
		//DIMPRS

		//DIMPIVA
		$ret['empty1IVA'] = 'emptyIVA';
		$ret['sociedadIVA'] = 'sociedadIVA';
		$ret['Tipo de retencionesIVA'] = 'retentionTypeIVA';
		$ret['Sujeto a retenciónIVA'] = 'sujetoRetenIVA';
		$ret['Categoría retenciónIVA'] = 'categoryRetainIVA';
		$ret['Nº identificacion fiscalIVA'] = 'nroIdFiscalIVA';
		$ret['Indicador de retenciónIVA'] = 'indicRetIVA';
		$ret['ivaExclusionNumber'] = 'plainIVA';
		$ret['Tipo de exencionIVA'] = 'tipoExenIVA';
		$ret['ivaExclusionFrom'] = 'plainIVAexFrom';
		$ret['ivaExclusionTo'] = 'plainIVAexTo';
		$ret['empty2IVA'] = 'emptyIVA';
		$ret['empty3IVA'] = 'emptyIVA';
		$ret['empty4IVA'] = 'emptyIVA';
		$ret['empty5IVA'] = 'emptyIVA';
		$ret['empty6IVA'] = 'emptyIVA';
		$ret['empty7IVA'] = 'emptyIVA';
		$ret['empty8IVA'] = 'emptyIVA';
		$ret['empty9IVA'] = 'emptyIVA';
		$ret['empty10IVA'] = 'emptyIVA';
		$ret['empty11IVA'] = 'emptyIVA';
		$ret['empty12IVA'] = 'emptyIVA';
		$ret['empty13IVA'] = 'emptyIVA';
		$ret['empty14IVA'] = 'emptyIVA';
		$ret['empty15IVA'] = 'emptyIVA';
		$ret['typeIvaCod3'] = 'typeIvaCod3';
		//DIMPIVA
		
		return $ret;
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return CompanyTaxData the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}

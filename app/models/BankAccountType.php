<?php

/**
 * This is the model class for table "BankAccountType".
 *
 * The followings are the available columns in table 'BankAccountType':
 * @property integer $idBankAccountType
 * @property string $BankAccountType
 */
class BankAccountType extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return BankAccountType the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'bankAccountType';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('idBankAccountType', 'numerical', 'integerOnly'=>true),
            array('bankAccountType', 'length', 'max'=>200),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        //province
		return array(
           
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'idBankAccountType' => Yii::t('app', 'Id tipo de cuenta'),
			'bankAccountType' => Yii::t('app', 'Tipo de cuenta'),
		);
	}

	public function scopes() {
		return array(
			'findAllWithOrder' => array('order' => 'bankAccountType ASC'),
		);
	}
	/*
	public function defaultScope()
	{
		return array(
			'condition' => 'status = 1'
		);
	}*/

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */

    public function getList() {

        $sql = "SELECT * FROM bankAccountType;";
        $command=Yii::app()->db->createCommand($sql);
        return $data=$command->queryAll();
    }



	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('idBankAccountType',$this->idBankAccountType);
		$criteria->compare('bankAccountType',$this->bankAccountType,true);

		$sort = new CSort();
		$sort->defaultOrder = 'bankAccountType ASC';

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'sort'=>$sort,
			'pagination'=>array(
				'pageSize'=>app()->controller->getItemsPerPage()
			),
		));
	}
	
	public function delete()
	{

			$criteria = new CDbCriteria();
			$criteria->condition = 'idBankAccountType = :id';
			//CompanyContact::model()->deleteAll($criteria);
			//CompanyAddress::model()->deleteAll($criteria);
			return parent::delete();
    }

}
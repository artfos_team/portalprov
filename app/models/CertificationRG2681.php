<?php

/**
 * This is the model class for table "CertificationRG2681".
 *
 * The followings are the available columns in table 'Certification':
 * @property integer $idCertificationRG2681
 * @property string $cuit
 * @property string $type
 * @property string $state
 * @property string $stateDescription
 * @property string $emision
 * @property string $administrationDate
 * @property string $auth  
 * @property string $ddjj
 * @property string $subsection
 * @property string $since
 * @property string $until
 * @property string $number
 * @property string $orderJud 
 * @property string $modificationDate
 * 
 */
class CertificationRG2681 extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return CertificationRG!8 the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'certification_RG2681';

	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('cuit,type,state, stateDescription,emision,administrationDate,auth,ddjj,subsection,since,until,number,orderJud, modificationDate', 'length', 'max'=>255),
					
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'idCertificadoRG2681' => Yii::t('app', 'Numero Certificado '),
			'cuit' => Yii::t('app', 'Cuit '),
			'type' => Yii::t('app', 'tipo'),
			'state' => Yii::t('app', 'Estado'),
			'stateDescription' => Yii::t('app', 'Estado Descripcion'),
            'emision' => Yii::t('app', 'Emision'),
			'auth' => Yii::t('app', 'Autorizacion'),
			'ddjj' => Yii::t('app', 'DDJJ'),
			'subsection' => Yii::t('app', 'Inciso'),
			'since' => Yii::t('app', 'Desde'),
			'until' => Yii::t('app', 'Hasta'),
			'number' => Yii::t('app', 'numero'),
			'orderJud' => Yii::t('app', 'Orden Judicial'),
			'modificationDate' => Yii::t('app', 'Fecha de Modificacion'),

		);
	}


	/*


	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */


	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('cuit',$this->cuit,true);
		$criteria->compare('type',$this->type,true);
		$criteria->compare('state',$this->state,true);
		$criteria->compare('stateDescription',$this->stateDescription,true);
		$criteria->compare('emision',$this->emision,true);
		$criteria->compare('administrationDate',$this->administrationDate,true);
		$criteria->compare('auth',$this->auth,true);
		$criteria->compare('ddjj',$this->ddjj,true);
		$criteria->compare('subsection',$this->subsection,true);
		$criteria->compare('since',$this->since,true);
		$criteria->compare('until',$this->until,true);
		$criteria->compare('number',$this->number,true);
		$criteria->compare('orderJud',$this->OrderJud,true);
		$criteria->compare('modificationDate',$this->modificationDate,true);


		$sort = new CSort();
		$sort->defaultOrder = 'nombre ASC';

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'sort'=>$sort,
			'pagination'=>array(
				'pageSize'=>app()->controller->getItemsPerPage()
			),
		));
	}
	

}
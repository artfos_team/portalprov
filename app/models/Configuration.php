<?php

/**
 * This is the model class for table "company".
 *
 * The followings are the available columns in table 'company':
 * @property integer $idRubro
 * @property string $nombre
 * @property integer $status
 */
class Configuration extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Licitaciones the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'configuration';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('idConfig', 'numerical', 'integerOnly'=>true),
            array('description', 'length', 'max'=>100),
            array('attribute', 'length', 'max'=>20),
			array('value', 'length', 'max'=>15000),
			array('idConfig, description, attribute, value', 'safe', 'on' => 'search')
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'description' => Yii::t('app', 'Descripcion'),
            'attribute' => Yii::t('app', 'Codigo'),
            'value' => Yii::t('app', 'Valor'),

		);
	}

	public function scopes() {
		return array(
			'findAllWithOrder' => array('order' => 'attribute ASC'),
		);
	}
	/*
	public function defaultScope()
	{
		return array(
			'condition' => 'status = 1'
		);
	}*/

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */

    public function getList() {

        $sql = "SELECT * FROM configuration;";
        $command=Yii::app()->db->createCommand($sql);
        return $data=$command->queryAll();
	}
	
	public function getShortValue($id)
	{
        $sql = "SELECT value FROM configuration where idConfig =". $id;
        $command=Yii::app()->db->createCommand($sql);
		$data=$command->queryAll();
		$data = substr( $data[0]['value'] , 0, 50);
		return $data."...";
	}



	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('description',$this->description);
		$criteria->compare('attribute',$this->attribute,true);
		$criteria->compare('value',$this->value,true);


		$sort = new CSort();
		$sort->defaultOrder = 'attribute ASC';

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'sort'=>$sort,
			'pagination'=>array(
				'pageSize'=>app()->controller->getItemsPerPage()
			),
		));
	}
}
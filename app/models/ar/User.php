<?php

/**
 * This is the model class for table "user".
 *
 * The followings are the available columns in table 'user':
 * @property integer $idUser
 * @property string $name
 * @property string $email
 * @property string $salt
 * @property string $password
 * @property string $passwordStrategy
 * @property boolean $requiresNewPassword
 * @property string $lastLoginAt
 * @property string $lastActiveAt
 * @property string $signature
 * @property integer $idCompany
 * @property integer $idConsultant
 * @property boolean $sex
 * @property integer $loginAttempt
 */
class User extends ActiveRecord
{
    CONST ATTACHMENTPATH = '/upload/user';

    public $_signature;

    public $_repeatPassword;
	
    public $_oldPassword;
    public $encriptar;

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return User the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'user';
    }

    /**
     * @return array the behavior configurations (behavior name=>behavior configuration)
     */
    public function behaviors()
    {
        return array_merge(parent::behaviors(), array(
            "PasswordBehavior" => array(
                "class" => "application.components.behaviors.YiiPasswordBehavior",
                "defaultStrategyName" => "bcrypt",
                "strategies" => array(
                    "bcrypt" => array(
                        "class" => "application.components.behaviors.YiiPasswordStrategyBcrypt",
                        "workFactor" => 14
                    )
                ),
            ),
            /*'workflow' => array(
                'class' => 'webroot.vendor.crisu83.yii-workflow.behaviors.WorkflowBehavior',
                'defaultStatus' => self::STATUS_DEFAULT,
                'statuses' => array(
                    self::STATUS_DEFAULT => array(
                        'label' => Yii::t('label', 'Default'),
                        'transitions' => array(self::STATUS_DELETED),
                    ),
                    self::STATUS_DELETED => array(
                        'label' => Yii::t('label', 'Deleted'),
                    ),
                ),
            ),*/
            'PicturableBehavior' => array(
                'class'=>'application.components.behaviors.PicturableBehavior',
                'attributeNames' => array('signature', 'picture'),
                'dirName' => 'user',
            ),
        ));
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array_merge(parent::rules(), array(
            array('name, email', 'required'),
            array('email', 'unique'),
            array('email', 'email'),
            array('requiresNewPassword, idCompany, idConsultant, sex, loginAttempt, idJob', 'numerical', 'integerOnly' => true),
            array('name, salt, password, passwordStrategy', 'length', 'max' => 255),
			array('picture, signature', 'safe'),
            // The following rule is used by search().
            array('idUser, name, lastLoginAt, lastActiveAt', 'safe', 'on' => 'search'),
            array('password, _repeatPassword', 'required', 'on'=>'create'),
            array('_repeatPassword', 'checkPasswordRepeat', 'on'=>'create'),
			array('password, _repeatPassword, _oldPassword', 'required', 'on'=>'changePassword'),
			array('_repeatPassword', 'checkPasswordRepeat', 'on'=>'changePassword'),			
			array('_oldPassword', 'checkCurrentPassword', 'on'=>'changePassword')
        ));
    }

    public function checkPasswordRepeat($attribute)
    {
        if(strcmp($this->_repeatPassword, $this->password) != 0)
        {
            $this->addError('password', 'Las contraseñas ingresadas no coinciden.');
            $this->_repeatPassword = '';
        }
    }
	
	public function checkCurrentPassword($attribute)
	{		
		$myself = self::model()->findByPk($this->getPrimaryKey());
		if(!$myself->verifyPassword($this->$attribute))
		{
			$this->addError($attribute, 'El password actual no es correcta.');			
		}
	}

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array_merge(parent::relations(), array(
                'company' => array(self::BELONGS_TO, 'Company', 'idCompany'),
                'job' => array(self::BELONGS_TO, 'Job', 'idJob'),

	        'consultant' => array(self::BELONGS_TO, 'Consultant', 'idConsultant'),
                'roles' => array(self::HAS_MANY, 'UserHasRoles', 'idUser'),
                'comments' => array(self::HAS_MANY, 'Comments', 'idUser'),
                'messagesSend' => array(self::HAS_MANY, 'Messages', 'idEmisor'),
                'messagesReceived' => array(self::HAS_MANY, 'Messages', 'idReceptor'),
                
        ));
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array_merge(parent::attributeLabels(), array(
            'idUser' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Alias'),
            'lastLoginAt' => Yii::t('app', 'Last login at'),
            'lastActiveAt' => Yii::t('app', 'Last active at'),
            'idCompany' => Yii::t('app', 'Company'),
            'idJob' => Yii::t('app', 'Job'),
			'idConsultant' => Yii::t('app', 'Consultant'),
            'picture' => Yii::t('app', 'Picture'),
            'sex' => Yii::t('app', 'Sex'),
            'loginAttempt' => Yii::t('app', 'loginAttempt'),
            '_oldPassword' => Yii::t('app', 'Old password'), 
            'password' => Yii::t('app', 'Password'), 
            '_repeatPassword' => Yii::t('app', 'Repeat password'), 
        ));
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider.
     */
    public function search()
    {
        $criteria = new CDbCriteria;

        $criteria->compare('t.idUser', $this->idUser);
        $criteria->compare('t.name', $this->name, true);
        $criteria->compare('t.lastLoginAt', $this->lastLoginAt, true);
        $criteria->compare('t.lastActiveAt', $this->lastActiveAt, true);
        $criteria->compare('t.email', $this->email, true);
        $criteria->compare('t.idCompany', $this->idCompany);
        $criteria->compare('t.idConsultant', $this->idConsultant);
		$criteria->compare('t.sex', $this->sex);
		$criteria->compare('t.idJob', $this->idJob);

        $sort = new CSort();
        $sort->defaultOrder = 'name ASC';

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'sort'=>$sort,
        ));
    }

    /**
     * @var array used to cache user roles (RBAC).
     */ 
    private $_roles;

    /**
     * Checks if the current user has the given role (RBAC permissions)
     * @param int $idRole of the role to check.
     */ 
    public function hasRole($idRole)
    {
        if(!isset($this->_roles))
        {
            $this->_roles = array();

            $criteria = new CDbCriteria();
            $criteria->condition = 'idUser = :me';
            $criteria->params[':me'] = $this->getPrimaryKey();

            $uhr = UserHasRoles::model()->findAll($criteria);

            foreach($uhr as $row)
            {
                $this->_roles[] = $row->rolesId; 
            }
        }

        return in_array($idRole, $this->_roles);
    }
	
	/**
	 * Returns all user roles, separated by comma
	 * @return string
	 */
	public function getRolesList()
	{
		$roles = Roles::model()->findAll('id IN (SELECT rolesId FROM user_has_roles  INNER JOIN user on user.idUser = user_has_roles.idUser where user.status = 0 AND  user_has_roles.idUser = :idu)',
				array(':idu'=>$this->primaryKey));
				
		$r = array();
		
		foreach($roles as $role)
		{
			$r[] = $role->name;
		}
		
		return count($r) > 0 ? implode(', ', $r) : "N/A";
	}

    public function getKey($msg='')
    {
        return hash_hmac('sha1', $msg, "!ProveJ?)/QY=F(/H?)(HJ?(80s9df78mvnb)F(#/H_" . $this->getPrimaryKey(). "_()/AGFjA6(?)(&)&%|/(/)(8adfslksdfn*"); //Should be more secure than regular sha1 if code is not compromised
    }

    public function getXlsAttributes()
    {
        return array(
            array('attribute'=>'name'),
            array('attribute'=>'email'),
            array('attribute'=>'lastLoginAt', 'type'=>'date'),
            array('attribute'=>'lastActiveAt', 'type'=>'date'),
        );
    }

    public function getFullName()
    {
        return $this->name;
    }
    
    public function getEmail()
    {
        return $this->email;
    }
    
    public function getIdCompany()
    {
        return $this->company instanceof Company ? $this->company->getIdCompany() : "Empty";
    }
   
    public function getJobName()
    {
        return $this->job instanceof Job ? $this->job->getJob() : "N/A";
    }

    public function getCompanyName()
    {
        return $this->company instanceof Company ? $this->company->getCompanyName() : "Empty";
    }

    public function displaySex()
    {
        $sexList = array(0=>"Femenino" , 1=>"Masculino");
        return $sexList[$this->sex];
    }

    public function updatePsw($id,$password)
    {
        $salt = CPasswordHelper::generateSalt(12);
        $pswHash = crypt($password, $salt);
        $sql="UPDATE user SET password = '".$pswHash."', salt = '".$salt."', requiresNewPassword = 0 WHERE status = 0 AND idUser = ".$id;
        $command = Yii::app()->db->createCommand($sql)->execute();
        if($command)
        {
            return "ok";
        }
        else
        {
            return "Nyet";
        }
    }

    public function getCommentsAnswers() // si el tipo es 3 (general), el idrefenceto es hacia las id de usuarios y no hacia los otros comentarios
    {
        $sql = "SELECT * FROM `comments` WHERE idRefenceTo IN (SELECT idComments from comments where idUser = ". $this->idUser.") OR (idCommentType = 3 AND idRefenceTo IS NULL) OR (idCommentType = 3 AND idRefenceTo = ".$this->idUser.") ORDER BY dateComments DESC LIMIT 10";
        $answers =  Yii::app()->db->createCommand($sql)->queryAll();
        foreach ($answers as $key => $value) {
            if($value{'statusComments'} == 1)
            {
                $sql = "UPDATE `comments` SET statusComments = 2 where idComments = ". $value{'idComments'} . " AND NOT (idCommentType = 3 AND idRefenceTo IS NULL)";
                Yii::app()->db->createCommand($sql)->query();
            }
        }
        return $answers;
    }
    public function getComments()
    {
        $sql = "SELECT * FROM `comments` WHERE idUser = ". $this->idUser;
        $comments =  Yii::app()->db->createCommand($sql)->queryAll();
        return $comments;
    }

    public function requerirPsw($email,$need) {
		$sql = "UPDATE user set requiresNewPassword = $need where status = 0 AND email = '$email'";
		$command=Yii::app()->db->createCommand($sql);
		$command->query();
	}

	public function reestablecerPsw($email)
	{
        $decrypted = $this->desencriptar($email);
        
        $explod = explode('<',$decrypted);
        $email = $explod[0];
        $date = $explod[1];

        $sql = "SELECT idUser,requiresNewPassword from user where status = 0 AND email = '$email'";
        $command=Yii::app()->db->createCommand($sql);
        $command->query();
        $data=$command->queryAll(); 

		if($data[0]{'requiresNewPassword'} == 1)
		{

            $diff = abs(strtotime(date('Y-m-d H:i:s')) - $date);
            if($diff > (6*60*60)) //6 horas
            {
                $this->requerirPsw($email,0);
                return "vencio";
            }
			
			$userEx = explode('@',$email);
			$user = $userEx[0];
			$psw = $this->randomPsw();
			$this->updatePsw($data[0]{'idUser'},$psw );
            $this->requerirPsw($email,2);
			return $psw;
		}else{
			return "false";
		}
    }
    
//test2
    public function sendPswReestablecido($email,$psw){
        //$mail = new Email();
        require_once dirname(__FILE__).DIRECTORY_SEPARATOR. '../../extensions/mailer/phpmailer/class.phpmailer.php';
        //This should be done in your php.ini, but this is how to do it if you don't have access to that
        $mail = new PHPMailer();
        //configuracion fija
        $emailParameters = EmailParameters::model()->findByPk(1);
        $customEmail = CustomEmail::model()->findByPk(17);

        //$mail->subject ="Recordatorio de clave";
        //
        $mail->SetFrom($emailParameters->user);
        $mail->FromName = $emailParameters->fromName;
        $mail->IsHTML (true); 
        $mail->IsSMTP();
        $mail->Host = $emailParameters->smtp;
        $mail->Port = $emailParameters->port;

        $mail->SMTPSecure = $emailParameters->security;
        $mail->CharSet = 'UTF-8';
        $mail->SMTPAuth = false;
        //$mail->Username = $emailParameters->user;
        //$mail->Password = $emailParameters->userPsw;
        $mail->AddAddress($email);


        //


        $mail->subject = $customEmail->subject;
        //$mail->AddCC($participant->consultant->email);
        $date = date("d/m/Y H:i:s");
        $mail->MsgHTML(
            '<div style="font-size: 10.0pt; font-family: \'Verdana\',sans-serif;">	
            <p>Estimado, conforme a su petición, le informamos que su nueva contraseña es <strong>'.$psw.'</strong></p> 
            <p class="m_-3923079614135954232xmsonormal"><span style="font-size: 9.0pt; font-family: \'Verdana\',sans-serif;">Este mensaje fue generado automáticamente por registrarse o realizar alguna acción en nuestro sitio de proveedores, se ha contactado vía mail o por teléfono con ALBANESI. Si usted desconoce alguna acción de las antes mencionadas, descarte este mail. El contenido es propiedad de ALBANESI y garantiza la privacidad y seguridad de su información.</span><u></u><u></u></p>
            <p class="MsoNormal"><img id="m_-3923079614135954232x__x005f_x0000_i1025" style="width: 74px; height: 93px;" src="http://proveedores.albanesi.com.ar:8081/static/img/logok.png" width="160" height="88" /></p>
            <p class="m_-3923079614135954232xmsonormal"><span style="font-size: 9.0pt; font-family: \'Verdana\',sans-serif;">Av. Leandro N. Alem 855&deg;</span><u></u><u></u></p>
            <p class="m_-3923079614135954232xmsonormal"><span style="font-size: 9.0pt; font-family: \'Verdana\',sans-serif;">Buenos Aires, Argentina</span><u></u><u></u></p>
            <p class="m_-3923079614135954232xmsonormal"><span style="font-size: 9.0pt; font-family: \'Verdana\',sans-serif;">+54 (11) 4313-6790</span><u></u><u></u></p>
            <p class="m_-3923079614135954232xmsonormal"><span style="font-size: 10.0pt; font-family: \'Verdana\',sans-serif;"><a href="http://proveedores.albanesi.com.ar:8081/">proveedores.albanesi.com.ar:8081</a></span><u></u><u></u></p>
            <p><span style="font-size: 8.0pt; font-family: \'Verdana\',sans-serif; color: #828282;">Esta comunicaci&oacute;n y cualquier archivo adjunto contiene informaci&oacute;n reservada, confidencial, protegida por ley. Si no es usted el destinatario de la misma, observe que tiene terminantemente prohibido usar, difundir, distribuir o copiar esta comunicaci&oacute;n. Si ha recibido este mensaje por error deber&aacute; avisar inmediatamente de ello a su emisor por tel&eacute;fono o correo electr&oacute;nico y eliminarlo totalmente de su sistema. No se garantiza que las comunicaciones por Internet sean enviadas en el plazo correspondiente o que no contengan errores o virus. El emisor no aceptar&aacute; responsabilidad por errores u omisiones. Las opiniones vertidas son responsabilidad del autor y no son emitidas ni avaladas por Contreras ni ninguna de sus subsidiarias. <br /> <br /> No imprima este correo si no es necesario; todos tenemos un papel en el cuidado del medio ambiente. <br /> Do not print this mail if it is not necessary. <br /> <br /> This communication and any attached file contain information that is legally privileged, confidential or exempt from disclosure. If you are not the intended recipient, please note that any dissemination, distribution, or copying of this communication is strictly prohibited. Anyone who receives this message in error should notify the sender immediately by telephone or by return e-mail and delete this communication entirely from his or her computer. Contreras is neither liable for the proper and complete transmission of the information contained in this communication nor for any delay, error or virus. Any opinions presented are solely those of its author and do not necessarily represent those of Contreras or any of its subsidiary companies.</span></p>
            </div>'
            );
        /*$mail->body =
            '<div style="font-size: 10.0pt; font-family: \'Verdana\',sans-serif;">	
            <p>Estimado, conforme a su petición, le informamos que su nueva contraseña es <strong>'.$psw.'</strong></p> 
            <p class="m_-3923079614135954232xmsonormal"><span style="font-size: 9.0pt; font-family: \'Verdana\',sans-serif;">Este mensaje fue generado automáticamente por registrarse o realizar alguna acción en nuestro sitio de proveedores, se ha contactado vía mail o por teléfono con CONTRERAS. Si usted desconoce alguna acción de las antes mencionadas, descarte este mail. El contenido es propiedad de CONTRERAS y garantiza la privacidad y seguridad de su información.</span><u></u><u></u></p>
            <p class="MsoNormal"><img id="m_-3923079614135954232x__x005f_x0000_i1025" style="width: 74px; height: 93px;" src="https://www.contreras.com.ar/img/contreras-logo.png" width="70" height="88" /></p>
            <p class="m_-3923079614135954232xmsonormal"><span style="font-size: 9.0pt; font-family: \'Verdana\',sans-serif;">San Martin 140, piso 8&deg;</span><u></u><u></u></p>
            <p class="m_-3923079614135954232xmsonormal"><span style="font-size: 9.0pt; font-family: \'Verdana\',sans-serif;">Buenos Aires, Argentina</span><u></u><u></u></p>
            <p class="m_-3923079614135954232xmsonormal"><span style="font-size: 9.0pt; font-family: \'Verdana\',sans-serif;">+54 (11) 4321 9500</span><u></u><u></u></p>
            <p class="m_-3923079614135954232xmsonormal"><span style="font-size: 10.0pt; font-family: \'Verdana\',sans-serif;"><a href="https://urldefense.proofpoint.com/v2/url?u=http-3A__www.contreras.com.ar_&amp;d=DwMFAw&amp;c=0TzQCy9lgR5hSW-bDg5HA76y7nf4lvOzvVop5GM3Y80&amp;r=lDiBsYLvsElGPOzCWcOqOemV6CUYuuTcfC74XGi7CUo&amp;m=8sseLaql5V9k-ag5cgaMdHVb0z4cwyDOoinA8rxc6E4&amp;s=Gco3yqJEEW6RxzCiGpPaywkYCh3zf9evo5sL36znb1Y&amp;e=" target="_blank" rel="noopener" data-saferedirecturl="https://www.google.com/url?q=https://urldefense.proofpoint.com/v2/url?u%3Dhttp-3A__www.contreras.com.ar_%26d%3DDwMFAw%26c%3D0TzQCy9lgR5hSW-bDg5HA76y7nf4lvOzvVop5GM3Y80%26r%3DlDiBsYLvsElGPOzCWcOqOemV6CUYuuTcfC74XGi7CUo%26m%3D8sseLaql5V9k-ag5cgaMdHVb0z4cwyDOoinA8rxc6E4%26s%3DGco3yqJEEW6RxzCiGpPaywkYCh3zf9evo5sL36znb1Y%26e%3D&amp;source=gmail&amp;ust=1569266816868000&amp;usg=AFQjCNGmf2yOrgexecmt8N0-TW0Zb1m49g">www.contreras.com.ar</a></span><u></u><u></u></p>
            <p><span style="font-size: 8.0pt; font-family: \'Verdana\',sans-serif; color: #828282;">Esta comunicaci&oacute;n y cualquier archivo adjunto contiene informaci&oacute;n reservada, confidencial, protegida por ley. Si no es usted el destinatario de la misma, observe que tiene terminantemente prohibido usar, difundir, distribuir o copiar esta comunicaci&oacute;n. Si ha recibido este mensaje por error deber&aacute; avisar inmediatamente de ello a su emisor por tel&eacute;fono o correo electr&oacute;nico y eliminarlo totalmente de su sistema. No se garantiza que las comunicaciones por Internet sean enviadas en el plazo correspondiente o que no contengan errores o virus. El emisor no aceptar&aacute; responsabilidad por errores u omisiones. Las opiniones vertidas son responsabilidad del autor y no son emitidas ni avaladas por Contreras ni ninguna de sus subsidiarias. <br /> <br /> No imprima este correo si no es necesario; todos tenemos un papel en el cuidado del medio ambiente. <br /> Do not print this mail if it is not necessary. <br /> <br /> This communication and any attached file contain information that is legally privileged, confidential or exempt from disclosure. If you are not the intended recipient, please note that any dissemination, distribution, or copying of this communication is strictly prohibited. Anyone who receives this message in error should notify the sender immediately by telephone or by return e-mail and delete this communication entirely from his or her computer. Contreras is neither liable for the proper and complete transmission of the information contained in this communication nor for any delay, error or virus. Any opinions presented are solely those of its author and do not necessarily represent those of Contreras or any of its subsidiary companies.</span></p>
            </div>';*/ //Fin message HTML

        //
        //$mailBody = $customEmail->body;
        //eval("\$sendBody = \"$mailBody\";");
        //$mail->MsgHTML($sendBody);
        //

        //eval("\$sendBody8 = \"$customEmail->body\";");
        //$mail->body = $sendBody8;
        try
		{
            if($mail->Send())
            {
                return true;
            }
            else {
                return false;
            }
		}
		catch (Exception $ex)
		{
			throw new Exception($ex->getMessage());
		}
    }

//
    public function randomPsw()
    {
        $caracteres='ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        $longpalabra=8;
        for($pass='', $n=strlen($caracteres)-1; strlen($pass) < $longpalabra ; ) {
          $x = rand(0,$n);
          $pass.= $caracteres[$x];
        }
        return $pass;
    }

    public static function encriptar($cadena){
        $cadena = $cadena."<".strtotime(date('Y-m-d H:i:s'));
        $key='';  // Una clave de codificacion, debe usarse la misma para encriptar y desencriptar
        $iv = '12345678912346\0';
        //$encrypted = base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_256, md5($key), $cadena, MCRYPT_MODE_CBC, md5(md5($key))));
        $encrypted = base64_encode(openssl_encrypt($cadena, 'AES-256-CBC', md5($key), true, $iv));
        $encrypted = str_replace(array('+','/','='),array('-','_','.'),$encrypted); 
        return $encrypted; //Devuelve el string encriptado
    }

    public function desencriptar($cadena){
         $key='';  // Una clave de codificacion, debe usarse la misma para encriptar y desencriptar
         $iv = '12345678912346\0';
         $cadena = str_replace(array('-','_','.'),array('+','/','='),$cadena);
         
         //$decrypted = rtrim(mcrypt_decrypt(MCRYPT_RIJNDAEL_256, md5($key), base64_decode($cadena), MCRYPT_MODE_CBC, md5(md5($key))), "\0");
         $decrypted = rtrim(openssl_decrypt(base64_decode($cadena), 'AES-256-CBC', md5($key), true,$iv ), "\0");
        return $decrypted;  //Devuelve el string desencriptado
    }

	public function sendMailReestablecer($email){
		$this->requerirPsw($email,1);
        
		require_once dirname(__FILE__).DIRECTORY_SEPARATOR. '../../extensions/mailer/phpmailer/class.phpmailer.php';
		//This should be done in your php.ini, but this is how to do it if you don't have access to that
		$mail = new PHPMailer();
		//configuracion fija
        $emailParameters = EmailParameters::model()->findByPk(1);
        ///         
        $customEmail = CustomEmail::model()->findByPk(2);
        ///
		$mail->SetFrom($emailParameters->user);
		$mail->FromName = $emailParameters->fromName;
		$mail->IsHTML (true); 
		$mail->IsSMTP();
		$mail->Host = $emailParameters->smtp;
		$mail->Port = $emailParameters->port;
		 
		$mail->SMTPSecure = $emailParameters->security;
        $mail->CharSet = 'UTF-8';
        $mail->SMTPAuth = false;
		//$mail->Username = $emailParameters->user;
		//$mail->Password = $emailParameters->userPsw;
        //$mail->Subject = "Reestablecimiento de clave";
        //
        $mail->Subject = $customEmail->subject;
        //
		$mail->AddAddress($email); // Email Destinatario
		//$mail->AddCC($participant->consultant->email);
		$mail->MsgHTML(
			'<div style="font-size: 10.0pt; font-family: \'Verdana\',sans-serif;">

            <p>Estimado, conforme a su petición,usted ha solicitado un reinicio de contraseña. Para validar su identidad ingrese al siguiente	
					<a href="'.$emailParameters->hostForMails.'/?i=1&e='.$this->encriptar($email).'">link</a></p>
	
			</div>'
		);
        ///
        
        //////like this:: .$emailParameters->hostForMails.'/?i=1&e='.$this->encriptar($email).
        
        $emailParameters->hostForMails;
        $emailCrypted = "A";
        $emailCrypted = $this->encriptar($email);

        $mailBody = $customEmail->body;
       
        //eval("\$emailParameters = \"$emailParameters\";");
        //eval("\$sendBody = \"$customEmail->body\";");
       // eval("\$sendBody = \"$mailBody\";");
        //$mail->MsgHTML($sendBody);
        ///


		try
		{
            if($mail->Send())
            {
                return true;
            }
            else {
                return false;
            }
		}
		catch (Exception $ex)
		{
			throw new Exception($ex->getMessage());
		}
	
    }




////////TEST

public function sendMailProvReject($email){
	require_once dirname(__FILE__).DIRECTORY_SEPARATOR. '../../extensions/mailer/phpmailer/class.phpmailer.php';

	$mail = new PHPMailer();
	$emailParameters = EmailParameters::model()->findByPk(1);
	$customEmail = CustomEmail::model()->findByPk(21);
	$mail->SetFrom($emailParameters->user);
	$mail->FromName = $emailParameters->fromName;
	$mail->IsHTML (true); 
	$mail->IsSMTP();
	$mail->Host = $emailParameters->smtp;
	$mail->Port = $emailParameters->port;
	 
    $mail->SMTPSecure = $emailParameters->security;
    //$mail->SMTPSecure = 'ssl';
	$mail->CharSet = 'UTF-8';
	$mail->SMTPAuth = true;
	$mail->Username = $emailParameters->user;
	$mail->Password = $emailParameters->userPsw;
	$mail->Subject = $customEmail->subject;
	$mail->AddAddress($email); // Email Destinatario
	$emailParameters->hostForMails;

	$mailBody = $customEmail->body;

	//eval("\$sendBody = \"$mailBody\";");
	//$mail->MsgHTML($sendBody);
	$mail->MsgHTML($mailBody);

	try
	{
		if($mail->Send())
		{
			return true;
		}
		else {
			return false;
		}
	}
	catch (Exception $ex)
	{
		throw new Exception($ex->getMessage());
	}

}






public function sendMailProvActivation($email){
	require_once dirname(__FILE__).DIRECTORY_SEPARATOR. '../../extensions/mailer/phpmailer/class.phpmailer.php';

	$mail = new PHPMailer();
	$emailParameters = EmailParameters::model()->findByPk(1);
	$customEmail = CustomEmail::model()->findByPk(13);
	$mail->SetFrom($emailParameters->user);
	$mail->FromName = $emailParameters->fromName;
	$mail->IsHTML (true); 
	$mail->IsSMTP();
	$mail->Host = $emailParameters->smtp;
	$mail->Port = $emailParameters->port;
	 
    $mail->SMTPSecure = $emailParameters->security;
    //$mail->SMTPSecure = 'ssl';
	$mail->CharSet = 'UTF-8';
	$mail->SMTPAuth = true;
	$mail->Username = $emailParameters->user;
	$mail->Password = $emailParameters->userPsw;
	$mail->Subject = $customEmail->subject;
	$mail->AddAddress($email); // Email Destinatario
	$emailParameters->hostForMails;

	$mailBody = $customEmail->body;

	//eval("\$sendBody = \"$mailBody\";");
	//$mail->MsgHTML($sendBody);
	$mail->MsgHTML($mailBody);

	try
	{
		if($mail->Send())
		{
			return true;
		}
		else {
			return false;
		}
	}
	catch (Exception $ex)
	{
		throw new Exception($ex->getMessage());
	}

}

	/////TEST°






    public function sendMessages($idEmisor, $idReceptor, $message, $messageType)
    {
        $model = new Messages();
        $model->idEmisor = $idEmisor;
        $model->idReceptor = $idReceptor;
        $model->message = $reason = str_replace("_"," ",$message);
        $model->messageType = $messageType;
        $model->date = date('Y-m-d H:i:s');
        $model->status = 0;
        $model->save();
        return true;
    }
}
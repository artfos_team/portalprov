<?php

/**
 * This is the model class for table "buy_order_document_type_code".
 *
 * The followings are the available columns in table 'buy_order_document_type_code':
 * @property integer $idDocumentationTypeCode
 * @property integer $idDocumentationType
 * @property string $code
 * @property string $description
 */
class BuyOrderDocumentTypeCode extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return DocumentTypeCode the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'buy_order_document_type_code';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('idBuyOrderDocumentationTypeCode', 'required'),
			array('idBuyOrderDocumentationTypeCode,idBuyOrderDocumentationType', 'numerical', 'integerOnly'=>true),
			array('description,code', 'length', 'max'=>255),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'idDocumentationTypeCode' => Yii::t('app', 'Id Tipo de documento'),
			'idDocumentationType' => Yii::t('app', 'Tipo de documento'),
			'code' => Yii::t('app', 'Tipo de documento'),
			'description' => Yii::t('app', 'Tipo de documento'),
		);
	}

}
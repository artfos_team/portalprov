<?php

/**
 * This is the model class for table "import_file_format".
 *
 * The followings are the available columns in table 'import_file_format':
 * @property $idImportFileFormat
 * @property $name
 * @property $url
 * @property $type
 *
 * The followings are the available model relations:
 * @property array $columns
 * @property FileColumn[] $importColumns
 */
class FileFormat extends CActiveRecord
{
	const NIC = array('NC' => 'Importador de NIC');
	const PROV = array('PR' => 'Importador de Proveedores');
	const PO = array('PO' => 'Importador Pedidos de oferta');
	const AI = array('AI' => 'Importador Certificados de Exlusión SUSS');
	const RG = array('RG' => 'Importador Ret Impuesto a las Ganancias' );
	const IV = array('IV' => 'Importador Agente retención IVA' );
	const IG = array('IG' => 'Importador Cert de Exención en el Impuesto a las Ganancias' );
	const EI = array('EI' => 'Importador Cert de Exclusión Ret/Percep del IVA ');

    const FORMATS = [self::NIC, self::PROV, self::PO, self::AI, self::RG, self::IV, self::IG, self::EI];

    /**
     * @var array
     */
    public $columns;

	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return FileFormat the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'import_file_format';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return array(
			array('name, type', 'required'),
			array('name', 'length', 'max'=>50),
            array('url', 'length', 'max'=>255),
            array('type', 'length', 'max'=>2),
			array('idImportFileFormat, name, type, url', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		return array(
			'importColumns' => array(self::HAS_MANY, 'FileColumn', 'idImportFileFormat'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'idImportFileFormat' => Yii::t('app', 'Id Import file format'),
			'name' => Yii::t('app', 'Nombre de formato'),
            'type' => Yii::t('app', 'Tipo de formato'),
            'url' => Yii::t('app', 'URL'),
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		$criteria=new CDbCriteria;

		$criteria->compare('idImportFileFormat',$this->idImportFileFormat);
		$criteria->compare('name',$this->name,true);
        $criteria->compare('type',$this->type,true);
        $criteria->compare('url',$this->url,true);

		$sort = new CSort();
		$sort->defaultOrder = 'name ASC';

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'sort'=>$sort,
			'pagination'=>array(
				'pageSize'=>app()->controller->getItemsPerPage()
			),
		));
	}

	public function afterSave()
    {
        parent::afterSave();

        FileColumn::model()->deleteAll('idImportFileFormat = '. $this->idImportFileFormat);

        foreach($this->columns as $column) {
            $newColumn = new FileColumn();
            $newColumn->idImportFileFormat = $this->idImportFileFormat;
            $newColumn->name = $column;
            $newColumn->save();
        }
    }

    public function beforeDelete()
    {
        FileColumn::model()->deleteAll('idImportFileFormat = '. $this->idImportFileFormat);

        return parent::beforeDelete();
    }
}
<?php

/**
 * This is the model class for table "import_file_column".
 *
 * The followings are the available columns in table 'import_file_column':
 * @property $idImportFileColumn
 * @property $idImportFileFormat
 * @property $name
 *
 * The followings are the available model relations:
 * @property $columns
 */
class FileColumn extends CActiveRecord
{
    const NIC = array('NIC' => 'Nro. de NIC');
    const CUIT = array('CUIT' => 'CUIT');
	const ORDEN_LIBERADA = array('ORDEN_LIBERADA' => 'Orden liberada');
	const HES = array('HES' => 'Nro. de HES');
	const ESTADO = array('ESTADO' => 'ESTADO');
    const FECHA_LIBERACION = array('FECHA_LIBERACION' => 'Fecha de liberación');
    const MONTO = array('MONTO' => 'Monto');
	const EMAIL = array('EMAIL' => 'Mail corporativo');
	const PROVIDER_NAME = array('PROVIDER_NAME' => 'Nombre del proveedor');

	const NUMBERPO = array('NUMBERPO' => 'N° de PO');
	const START_DATE = array('START_DATE' => 'Fecha apertura');
	const END_DATE = array('END_DATE' => 'Fecha cierre');

	//Datos AFIP SUSS
	const TYPE_CERTIFICATION  = array('TYPE_CERTIFICATION' => 'Tipo de Certificado');
	const PERIOD = array('PERIOD' => 'Periodo');
	const NUMBER = array('NUMBER' => 'Numero');
	const PERCENTAGE = array('PERCENTAGE' => 'Porcentaje');
	const RESOLUTION = array('RESOLUTION' => 'Resolucion');
	const STATE = array('STATE' => 'Estado');
	const SINCE = array('SINCE' => 'Desde');
	const UNTIL = array('UNTIL' => 'Hasta');
     
	// Datos AFIP RG
	const NUMBERRG = array('NUMBERRG' => 'Numero RG');
	const CUITRG = array('CUITRG' => 'Cuit RG');
	const PERIODRG = array('PERIODRG' => 'Periodo RG');
	const PERCENTAGERG = array('PERCENTAGERG' => 'Porcentage RG');
	const RESOLUTIONRG = array('RESOLUTIONRG' => 'Resolucion RG');
	const STATERG = array('STATERG' => 'Estado RG');
	const ISSUERG = array('ISSUERG' => 'Emisión RG');
	const PUBLICATIONRG = array('PUBLICATIONRG' => 'Publicacion RG');
	const VALIDITYRG = array('VALIDITYRG' => 'Valido RG');
 
	// Dato IVA
	const CUITRG18 = array('CUITRG' => 'Cuit RG18');
	const NAMERG18 = array('NAMERG18' => 'Name RG18');
	const SINCERG18 = array('SINCERG18' => 'Desde RG18');
	const UNTILRG18 = array('UNTILRG18' => 'Hasta RG18');
	const TYPERG18 = array ('TYPERG18' => 'Tipo RG18');
	const IGNORE = array('IGNORE' => 'Ignorar campo');

	// RG2681
	const CUITRG2681 = array('CUITRG' => 'CUITRG2681');
	const TYPERG2681 = array('TYPE2681' => 'TYPERG2681');
	const STATERG2681 = array('STATERG2681' => 'STATERG2681');
	const STATEDESCRIPTIONRG2681 = array('STATEDESCRIPTIONRG2681' => 'STATEDESCRIPTIONRG2681');
	const EMISIONRG2681 = array('EMISION2681' => 'EMISIONRG2681');
	const ADMINISTRATIONDATERG2681 = array('ADMINISTRATIONDATERG2681' => 'ADMINISTRATIONDATE2681');
	const AUTHRG2681 = array('AUTHG2681' => 'AUTHRG2681');
	const DDJJRG2681 = array('DDJJRG2681' => 'DDJJRG2681');
	const SUBSECTIONRG2681 = array('SUBSECTIONRG2681' => 'SUBSECTION2681');
	const SINCERG2681 = array('SINCERG2681' => 'SINCERG2681');
	const UNTILRG2681 = array('UNTILRG2681' => 'UNTILRG2681');
	const NUMBERRG2681 = array('NUMBERRG2681' => 'NUMBERRG2681');
	const ORDERJUDRG2681 = array('ORDERJUDRG2681' => 'ORDERJUDRG2681');
	const MODIFICATIONDATERG2681 = array('MODIFICATIONDATERG2681' => 'MODIFICATIONDATERG2681');

	//RG17
	const CUITRG17 = array('CUITRG17' => 'CUITRG17');
	const NAMERG17 = array('NAMERG17' => 'NAMERG17');
	const SINCERG17 = array('SINCERG17' => 'SINCERG17');
	const UNTILRG17 = array('UNTILRG17' => 'UNTILRG17');
	const PERCENTAGERG17 = array('PERCENTAGERG17' => 'PERCENTAGERG17');
	const RGRG17 = array('RGRG17' => 'RGRG17');
	const TYPERG17 = array('TYPERG17' => 'TYPERG17');


    public static $COLUMNS;

    public function __construct($scenario = 'insert')
    {
        parent::__construct($scenario);

        self::getColumns();
    }

    public static function getColumns() {

        self::$COLUMNS = array_merge(
            self::NIC,
            self::CUIT,
			self::ORDEN_LIBERADA,
			self::HES,
			self::ESTADO,
            self::FECHA_LIBERACION,
            self::MONTO,
			self::EMAIL,
			self::PROVIDER_NAME,
			self::IGNORE,	
			self::NUMBERPO,
			self::START_DATE,
			self::END_DATE,
			self::TYPE_CERTIFICATION,
			self::PERIOD,
			self::NUMBER,
			self::PERCENTAGE,
			self::RESOLUTION,
			self::STATE,
			self::SINCE,
			self::UNTIL,
			self::NUMBERRG,
			self::CUITRG,
			self::PERIODRG,
			self::PERCENTAGERG,
			self::RESOLUTIONRG,
			self::STATERG,
			self::ISSUERG,
			self::PUBLICATIONRG,
			self::VALIDITYRG,
			self::CUITRG18,
			self::NAMERG18,
			self::SINCERG18,
			self::UNTILRG18,
			self::TYPERG18,
			self::CUITRG2681,
			self::TYPERG2681,
			self::STATERG2681,
			self::STATEDESCRIPTIONRG2681,
			self::EMISIONRG2681,
			self::ADMINISTRATIONDATERG2681,
			self::AUTHRG2681,
			self::DDJJRG2681,
			self::SUBSECTIONRG2681,
			self::SINCERG2681,
			self::UNTILRG2681,
			self::NUMBERRG2681,
			self::ORDERJUDRG2681,
			self::MODIFICATIONDATERG2681, 	
			self::CUITRG17, 	
			self::NAMERG17,  	
			self::SINCERG17,  	
			self::UNTILRG17,  	
			self::PERCENTAGERG17,  	
			self::RGRG17,  	
			self::TYPERG17  	

		);

        return self::$COLUMNS;
    }

    /**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return FileFormat the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'import_file_column';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return array(
			array('idImportFileFormat, name', 'required'),
			array('idImportFileFormat, idImportFileColumn, name', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'idImportFileColumn' => Yii::t('app', 'Id Import file column'),
            'idImportFileFormat' => Yii::t('app', 'Formato'),
			'name' => Yii::t('app', 'Nombre de columna'),
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		$criteria=new CDbCriteria;

		$criteria->compare('idImportFileColumn',$this->idImportFileColumn);
        $criteria->compare('idImportFileFormat',$this->idImportFileFormat);
		$criteria->compare('name',$this->name,true);

		$sort = new CSort();
		$sort->defaultOrder = 'name ASC';

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'sort'=>$sort,
			'pagination'=>array(
				'pageSize'=>app()->controller->getItemsPerPage()
			),
		));
	}
}
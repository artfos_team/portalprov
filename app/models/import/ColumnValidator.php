<?php


class ColumnValidator
{
    private $columnName;

    /**
     * @param $columnName
     * @param $value
     */
    public function validate($columnName, $value) {

        $this->columnName = $columnName;

        switch ($columnName) {
            case FileColumn::CUIT:
                $this->notEmpty($value);
                $this->validateCUITFormat($value);
                break;
            case FileColumn::NRO_INVOICE:
            case FileColumn::FRGKE:
            case FileColumn::PDFFILE:
                $this->notEmpty($value);
                break;
            case FileColumn::DPTYP:
                $this->validateAnticipo($value);
                break;
            case FileColumn::DPPCT:
                $this->notEmpty($value);
                $this->validateNumber($value);
                $this->validatePorcentaje($value);
                break;
            case FileColumn::AEDAT:
                $this->notEmpty($value);
                $this->validateDateTime($value);
                break;
            case FileColumn::DPDAT:
                $this->notEmpty($value);
                $this->validateDateTime($value, true);
                break;
            case FileColumn::MONTO:
                $this->notEmpty($value);
                $this->validateNumber($value);
                break;
        }
    }

    /**
     * @param $cuit
     */
    private function validateCUITFormat($cuit) {

        $digits = array();
        $isValid = true;

        if (strlen($cuit) != 13) {
            $isValid = false;
        }

        for ($i = 0; $i < strlen($cuit); $i++) {
            if ($i == 2 or $i == 11) {
                if ($cuit[$i] != '-') {
                    $isValid = false;
                    break;
                }
            } else {
                if (!ctype_digit($cuit[$i])) {
                    $isValid = false;
                    break;
                }

                if ($i < 12) {
                    $digits[] = $cuit[$i];
                }
            }
        }

        if ($isValid) {
            $acum = 0;

            foreach (array(5, 4, 3, 2, 7, 6, 5, 4, 3, 2) as $i => $multiplicador) {
                $acum += $digits[$i] * $multiplicador;
            }

            $cmp = 11 - ($acum % 11);

            if ($cmp == 11) $cmp = 0;
            if ($cmp == 10) $cmp = 9;

            $isValid = ($cuit[12] == $cmp);
        }

        if (!$isValid) {
            throw new InvalidArgumentException("El cuit '$cuit' es incorrecto.");
        }
    }

    /**
     * @param $value
     */
    private function notEmpty($value) {

        if (!isset($value) || empty($value)) {
            throw new InvalidArgumentException("El campo '$this->columnName' no debe estar vacio.");
        }
    }

    /**
     * @param $value
     */
    private function validateAnticipo($value) {

        if (isset($value) && !empty($value) && $value !== 'M') {
            throw new InvalidArgumentException("El anticipo '$value' is incorrecto.");
        }
    }

    private function validateNumber($value) {

        if (!is_numeric($value)) {
            throw new InvalidArgumentException("El campo '$this->columnName' no es un valor numerico.");
        }
    }

    /**
     * @param $value
     */
    private function validatePorcentaje($value) {

        $valueFloat = floatval($value);

        if ($valueFloat > 100 || $valueFloat < 0) {
            throw new InvalidArgumentException("El porcentaje debe estar comprendido entre 0 y 100.");
        }
    }

    /**
     * @param $value
     * @param bool $validateFutureDate
     */
    private function validateDateTime($value, $validateFutureDate = false) {

        try {
            $date = new DateTime($value);

            if ($validateFutureDate) {
                $isValidDateTime = ($date > (new DateTime()));
            }
            else {
                $isValidDateTime = true;
            }
        }
        catch (Exception $exception) {
            $isValidDateTime = false;
        }

        if ($isValidDateTime) {
            throw new InvalidArgumentException("El campo '$this->columnName' no es una fecha valida.");
        }
    }
}
<?php
Yii::import('application.commands.SchedulerCommand');
/**
 * This is the model class for table "import_schedule".
 *
 * @property $idImportSchedule
 * @property $idImportFileFormat
 * @property $time
 * @property FileFormat $format
 */
class ImportSchedule extends CActiveRecord
{
    /**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return ImportSchedule the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'import_schedule';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return array(
			array('idImportFileFormat, time', 'required'),
            array('time', 'type', 'type'=>'time'),
			array('idImportFileFormat, idImportSchedule, time', 'safe', 'on'=>'search'),
		);
	}

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array(
            'format' => array(self::BELONGS_TO, 'FileFormat', 'idImportFileFormat'),
        );
    }

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'idImportSchedule' => Yii::t('app', 'Id Import schedule'),
            'idImportFileFormat' => Yii::t('app', 'Formato'),
			'time' => Yii::t('app', 'Horario'),
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		$criteria=new CDbCriteria;

		$criteria->compare('idImportSchedule',$this->idImportSchedule);
        $criteria->compare('idImportFileFormat',$this->idImportFileFormat);
		$criteria->compare('time',$this->time,true);

		$sort = new CSort();
		$sort->defaultOrder = 'time ASC';

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'sort'=>$sort,
			'pagination'=>array(
				'pageSize'=>app()->controller->getItemsPerPage()
			),
		));
	}

	protected function afterSave()
    {
        parent::afterSave();

        $this->reConfigureSchedulers();
    }

    protected function afterDelete()
    {
        parent::afterDelete();

        $this->reConfigureSchedulers();
    }

    private function reConfigureSchedulers() {

        $schedulerCommand = new SchedulerCommand('scheduler', new CConsoleCommandRunner());
        $schedulerCommand->init();

        if (!$schedulerCommand->tableExists()) {
            $schedulerCommand->createTable();
        }

		$schedulerCommand->actionRemoveall();
		
		
        $importSchedulers = ImportSchedule::model()->findAll();
        //$today = new DateTime(shell_exec('date "+%y-%m-%d %H:%M:%S"'));
		$today = new DateTime();
		

		// FINALIZADOR DE COMPULSAS
		$schedulerCommand->actionAdd(
			'FinalizarCompulsas',
			$today->format('Y-m-d_H:i:s'),
			null,
			'php /var/www/html/app/yiic.php finishtender check', 	// LINUX
			//'php yiic.php sync --idSchedule='. $importScheduler->idImportSchedule,	// WINDOWS
			false,
			false,
			true
		);
		// ----FINALIZADOR DE COMPULSAS
		// ENVIADOR DE EMAILS
		$schedulerCommand->actionAdd(
			'EnviarEmails',
			$today->format('Y-m-d_H:i:s'),
			null,
			'php /var/www/html/app/yiic.php sendemails send', 	// LINUX
			//'php yiic.php sync --idSchedule='. $importScheduler->idImportSchedule,	// WINDOWS
			false,
			false,
			false,
			false,
			false,
			true
		);
		
		foreach ($importSchedulers as $importScheduler) {
            $arTime = explode(':', $importScheduler->time);
            $today->setTime($arTime[0], $arTime[1]);

            if ($today <= shell_exec('date "+%y-%m-%d %H:%M:%S"')) {
                $today->add(new DateInterval('P1D'));
            }

            $schedulerCommand->actionAdd(
                $importScheduler->format->name . '-' . $importScheduler->time,
                $today->format('Y-m-d_H:i:s'),
                null,
				'php /var/www/html/app/yiic.php sync run --idSchedule='. $importScheduler->idImportSchedule, 	// LINUX
				//'php yiic.php sync --idSchedule='. $importScheduler->idImportSchedule,	// WINDOWS
                false,
                false,
                true
            );
        }
    }
}
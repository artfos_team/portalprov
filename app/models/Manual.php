<?php

/**
 * This is the model class for table "company".
 *
 * The followings are the available columns in table 'company':
 * @property integer $idDocument
 * @property integer $category
 * @property string $documentName
 * @property integer $required
 * @property integer $active
 */
class Manual extends CActiveRecord
{

	CONST ATTACHMENTPATH = '/upload/manual';

	public $_logo;

	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Document the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}


	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'manual';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
            array('status,category', 'numerical', 'integerOnly'=>true),
            array('path', 'file', 'types'=>'pdf', 'safe' => false),
            array('name', 'length', 'max'=>50),
            array('description', 'length', 'max'=>100),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'name' => Yii::t('app', 'Nombre instructivo'),
			'description' => Yii::t('app', 'Descripcion'),
            'path' => Yii::t('app', 'Subir archivo'),
            'category' => Yii::t('app', 'Categoría'),

        );
	}

	public function scopes() {
		return array(
			'findAllWithOrder' => array('order' => 'name ASC'),
		);
	}
	/*
	public function defaultScope()
	{
		return array(
			'condition' => 'active = 0'
		);
	}*/

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */

    public function getList() {

        //return $this->findAll();

        $sql = "SELECT * FROM manual;";
        $command=Yii::app()->db->createCommand($sql);
        return $data=$command->queryAll();
    }



	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('name',$this->name,true);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('path',$this->path,true);
        $criteria->compare('category',$this->path,true);


        $sort = new CSort();
		$sort->defaultOrder = 'name ASC';

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'sort'=>$sort,
			'pagination'=>array(
				'pageSize'=>app()->controller->getItemsPerPage()
			),
		));
	}
	

	public function delete()
	{
			$criteria = new CDbCriteria();
			$criteria->condition = 'idManual = :id';
            $criteria->params[':id'] = $this->idManual;
            
			return parent::delete();
    }
}
<?php

/**
 * This is the model class for table "compradores".
 *
 * The followings are the available columns in table 'compradores':
 * @property integer $idLicitacion
 * @property integer $idLicitacionComprador
 * @property integer $IdUser
 * @property integer $Resolucion
 */
class LicitacionesCompradores extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return LicitacionesStatus the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'licitaciones_compradores';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('idLicitacion, idLicitacionComprador, idUser, resolucion', 'numerical', 'integerOnly'=>true),
			array('idLicitacionComprador', 'unique'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'idLicitacionComprador' => Yii::t('app', 'Id licitacion Comprador'),
			'idLicitacion' => Yii::t('app', 'Id Licitacion'),
			'idLicitacionUser' => Yii::t('app', 'Id Licitacion User'),
			'idLicitacion' => Yii::t('app', 'Id Licitacion'),
            'resolucion' => Yii::t('app', 'Resolucion'),
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */

    public function getList() {

        $sql = "SELECT * FROM licitaciones_compradores;";
        $command=Yii::app()->db->createCommand($sql);
        return $data=$command->queryAll();
    }

	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('idLicitacion',$this->idLicitacion);
		$criteria->compare('idLicitacionComprador',$this->idLicitacionComprador,true);
		$criteria->compare('idLicitacionUser',$this->idLicitacionUser,true);
		$criteria->compare('resolucion',$this->resolucion,true);

		$sort = new CSort();
		$sort->defaultOrder = 'nombre ASC';

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'sort'=>$sort,
			'pagination'=>array(
				'pageSize'=>app()->controller->getItemsPerPage()
			),
		));
	}


}
<?php

/**
 * This is the model class for table "company".
 *
 * The followings are the available columns in table 'company':
 * @property integer $idRubro
 * @property string $nombre
 * @property integer $status
 */
class Rubros extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Rubros the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'rubros';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('idRubro, status', 'numerical', 'integerOnly'=>true),
			array('nombre', 'length', 'max'=>255),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'idRubro' => Yii::t('app', 'Id rubro'),
			'nombre' => Yii::t('app', 'Rubro'),
            'status' => Yii::t('app', 'Activo'),
		);
	}

	public function scopes() {
		return array(
			'findAllWithOrder' => array('order' => 'nombre ASC'),
		);
	}
	/*
	public function defaultScope()
	{
		return array(
			'condition' => 'status = 1'
		);
	}*/

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */

    public function getList() {

        $sql = "SELECT * FROM rubros;";
        $command=Yii::app()->db->createCommand($sql);
        return $data=$command->queryAll();
    }



	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('idRubro',$this->idRubro);
		$criteria->compare('nombre',$this->nombre,true);
		$criteria->compare('status',$this->status,true);


		$sort = new CSort();
		$sort->defaultOrder = 'nombre ASC';

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'sort'=>$sort,
			'pagination'=>array(
				'pageSize'=>app()->controller->getItemsPerPage()
			),
		));
	}
	
	public function delete()
	{

			$criteria = new CDbCriteria();
			$criteria->condition = 'idRubro = :id';
			$criteria->params[':id'] = $this->idDocument;
			CompanyContact::model()->deleteAll($criteria);
			CompanyAddress::model()->deleteAll($criteria);
			return parent::delete();
    }
    
    public function getStatus()
    {
        if($this->status == 1)
        {
            return "Si";
        }else{
            return "No";
        }
    }
}
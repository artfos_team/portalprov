<?php

/**
 * This is the model class for table "company_jurisdiction_data".
 *
 * The followings are the available columns in table 'company_jurisdiction_data':
 * @property integer $idCompanyJurisdictionData
 * @property integer $idJurisdiction
 * @property string $coefficient
 * @property double $aliquot
 * @property integer $certificateNumber
 * @property string $certificateFrom
 * @property string $certificateTo
 * @property string $certificatePdf
 * @property integer $retentionAgentHas
 * @property string $retentionAgentDate
 * @property string $retentionAgentPdf
 * @property string $form1276pdf
 * @property integer $perceptionAgentHas
 * @property string $perceptionAgentDate
 * @property string $perceptionAgentPdf
 * @property integer $idTaxData
 * @property string $modifDate
 *
 * The followings are the available model relations:
 * @property CompanyTaxData $idTaxData0
 * @property Jurisdictions $idJurisdiction0
 */
class CompanyJurisdictionData extends CActiveRecord implements ProviderDataExport
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'company_jurisdiction_data';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('idJurisdiction, certificateNumber, retentionAgentHas, perceptionAgentHas, idTaxData', 'numerical', 'integerOnly'=>true),
			array('aliquot', 'numerical'),
			array('coefficient', 'length', 'max'=>20),
			array('certificatePdf, retentionAgentPdf, form1276pdf, perceptionAgentPdf', 'length', 'max'=>400),
			array('certificateFrom, certificateTo, retentionAgentDate, perceptionAgentDate', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('idCompanyJurisdictionData, idJurisdiction, coefficient, aliquot, certificateNumber, certificateFrom, certificateTo, certificatePdf, retentionAgentHas, retentionAgentDate, retentionAgentPdf, form1276pdf, perceptionAgentHas, perceptionAgentDate, perceptionAgentPdf, idTaxData', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'idTaxData0' => array(self::BELONGS_TO, 'CompanyTaxData', 'idTaxData'),
			'idJurisdiction0' => array(self::BELONGS_TO, 'Jurisdictions', 'idJurisdiction'),
			'jurisdictionIdCategoryRetain0' => array(self::BELONGS_TO, 'CategoryRetain', 'aliquot'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'idCompanyJurisdictionData' => 'Id Company Jurisdiction Data',
			'idJurisdiction' => 'Id Jurisdiction',
			'coefficient' => 'Coefficient',
			'aliquot' => 'Aliquot',
			'certificateNumber' => 'Certificate Number',
			'certificateFrom' => 'Certificate From',
			'certificateTo' => 'Certificate To',
			'certificatePdf' => 'Certificate Pdf',
			'retentionAgentHas' => 'Retention Agent Has',
			'retentionAgentDate' => 'Retention Agent Date',
			'retentionAgentPdf' => 'Retention Agent Pdf',
			'form1276pdf' => 'Form 1276 Pdf',
			'perceptionAgentHas' => 'Perception Agent Has',
			'perceptionAgentDate' => 'Perception Agent Date',
			'perceptionAgentPdf' => 'Perception Agent Pdf',
			'idTaxData' => 'Id Tax Data',
			'modifDate' => Yii::t('app', 'Fecha'),
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('idCompanyJurisdictionData',$this->idCompanyJurisdictionData);
		$criteria->compare('idJurisdiction',$this->idJurisdiction);
		$criteria->compare('coefficient',$this->coefficient);
		$criteria->compare('aliquot',$this->aliquot);
		$criteria->compare('certificateNumber',$this->certificateNumber);
		$criteria->compare('certificateFrom',$this->certificateFrom,true);
		$criteria->compare('certificateTo',$this->certificateTo,true);
		$criteria->compare('certificatePdf',$this->certificatePdf,true);
		$criteria->compare('retentionAgentHas',$this->retentionAgentHas);
		$criteria->compare('retentionAgentDate',$this->retentionAgentDate,true);
		$criteria->compare('retentionAgentPdf',$this->retentionAgentPdf,true);
		$criteria->compare('form1276pdf',$this->form1276pdf,true);
		$criteria->compare('perceptionAgentHas',$this->perceptionAgentHas);
		$criteria->compare('perceptionAgentDate',$this->perceptionAgentDate,true);
		$criteria->compare('perceptionAgentPdf',$this->perceptionAgentPdf,true);
		$criteria->compare('idTaxData',$this->idTaxData);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}


	public function getExportableAttributes()
	{
		$ret = array();
		//DIMPIB
		$ret['Tipo de retencionesIB'] = 'retentionTypeIB_JD';
		$ret['certificateNumber'] = 'cNumberIB';
		$ret['certificateFrom'] = 'cNumberFromIB';
		$ret['certificateTo'] = 'cNumbertoIB';
		$ret['aliquotDesc'] = 'aliquotDesc';
		//DIMPIB
		
		return $ret;
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return CompanyJurisdictionData the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}

<?php

/**
 * This is the model class for table "CertificationRG18".
 *
 * The followings are the available columns in table 'Certification':
 * @property integer $idCertificationRG17
 * @property string $cuit
 * @property integer $name
 * @property string $since
 * @property string $until
 * @property string $percentage
 * @property string $rg
 * @property string $type
 */
class CertificationRG17 extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return CertificationRG!8 the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'certification_RG17';

	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
            array('cuit,name,since,until,percentage,rg,type', 'length', 'max'=>255),		
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'idCertificadoRG17' => Yii::t('app', 'Numero Certificado RG17'),
			'cuit' => Yii::t('app', 'Cuit RG17'),
			'name' => Yii::t('app', 'Razon Social RG17'),
			'since' => Yii::t('app', 'Desde RG17'),
			'until' => Yii::t('app', 'Hasta RG17'),
			'percentage' => Yii::t('app', 'percentage'),
			'rg' => Yii::t('app', 'rg'),
			'type' => Yii::t('app', 'Inciso'),
		);
	}


	/*


	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */


	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;
 
		$criteria->compare('cuit',$this->cuit,true);
		$criteria->compare('name',$this->nae,true);
		$criteria->compare('since',$this->since,true);
		$criteria->compare('until',$this->until,true);
		$criteria->compare('percentage',$this->percentage,true);
		$criteria->compare('rg',$this->rg,true);
		$criteria->compare('type',$this->type,true);
		$sort = new CSort();
		$sort->defaultOrder = 'nombre ASC';

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'sort'=>$sort,
			'pagination'=>array(
				'pageSize'=>app()->controller->getItemsPerPage()
			),
		));
	}
	

}
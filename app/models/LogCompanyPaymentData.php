<?php

/**
 * This is the model class for table "company_payment_data".
 *
 * The followings are the available columns in table 'company_payment_data':
 * @property integer $idPaymentData
 * @property integer $idBankBranch
 * @property integer $idBank
 * @property string $accountNumber
 * @property integer $idBankAccountType
 * @property string $cbu
 * @property string $pdfBank
 * @property integer $idCompanyDataStatus
 * @property string $observation
 * @property integer $idUser
 * @property string $modifDate
 *
 * The followings are the available model relations:
 * @property Company[] $companies
 * @property Bank $idBank0
 * @property BankAccountType $idBankAccountType0
 * @property BankBranch $idBankBranch0
 * @property CompanyDataStatus $idCompanyDataStatus0
 * @property User $idUser0
 */
class LogCompanyPaymentData extends CActiveRecord implements ProviderDataExport
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'log_company_payment_data';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('idBankBranch, idBank, idBankAccountType, idCompanyDataStatus, idUser', 'numerical', 'integerOnly'=>true),
			array('accountNumber', 'length', 'max'=>50),
			array('cbu', 'length', 'max'=>22),
			array('pdfBank', 'length', 'max'=>200),
			array('observation', 'length', 'max'=>400),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('idPaymentData, idBankBranch, idBank, accountNumber, idBankAccountType, cbu, pdfBank, idCompanyDataStatus, observation, idUser', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'companies' => array(self::HAS_MANY, 'Company', 'idPaymentData'),
			'idBank0' => array(self::BELONGS_TO, 'Bank', 'idBank'),
			'idBankAccountType0' => array(self::BELONGS_TO, 'BankAccountType', 'idBankAccountType'),
			'idBankBranch0' => array(self::BELONGS_TO, 'BankBranch', 'idBankBranch'),
			'idCompanyDataStatus0' => array(self::BELONGS_TO, 'CompanyDataStatus', 'idCompanyDataStatus'),
			'idUser0' => array(self::BELONGS_TO, 'User', 'idUser'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'idPaymentData' => 'Id Payment Data',
			'idBankBranch' => 'Id Bank Branch',
			'idBank' => 'Id Bank',
			'accountNumber' => 'Cuenta Bancaria',
			'idBankAccountType' => 'Id Bank Account Type',
			'cbu' => 'CBU',
			'pdfBank' => 'Pdf Bank',
			'idCompanyDataStatus' => 'Id Company Data Status',
			'observation' => 'Observation',
			'idUser' => 'Id User',
			'modifDate' => Yii::t('app', 'Fecha'),
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('idPaymentData',$this->idPaymentData);
		$criteria->compare('idBankBranch',$this->idBankBranch);
		$criteria->compare('idBank',$this->idBank);
		$criteria->compare('accountNumber',$this->accountNumber,true);
		$criteria->compare('idBankAccountType',$this->idBankAccountType);
		$criteria->compare('cbu',$this->cbu,true);
		$criteria->compare('pdfBank',$this->pdfBank,true);
		$criteria->compare('idCompanyDataStatus',$this->idCompanyDataStatus);
		$criteria->compare('observation',$this->observation,true);
		$criteria->compare('idUser',$this->idUser);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public function getExportableAttributes()
	{
		$ret = array();
		$ret['Clave de banco'] = 'bank';
		$ret['accountNumber'] = 'plain';
		$ret['cbu'] = 'plain';
		$ret['Tipo de Cuenta'] = 'idBankAccountType';
		return $ret;
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return LogCompanyPaymentData the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}

<?php

/**
 * This is the model class for table "provider".
 *
 * The followings are the available columns in table 'provider':
 * @property integer $idProvider
 * @property integer $idCompany
 * @property integer $idUser
 * @property integer $idComments
 * @property integer $idProviderStatus
 * @property string $date
 * @property integer $providerDeleted
 * @property integer $calification
 * @property integer $reasonCalification
 * @property integer $isAdmin
 * 
 *
 * The followings are the available model relations:
 * @property Comments $idComments0
 * @property Company $idCompany0
 * @property ProviderStatus $idProviderStatus0
 * @property User $idUser0
 * @property User $idUser0
 */
class Provider extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Provider the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'provider';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('idCompany, idUser,calification, idComments, idProviderStatus, isAdmin', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('idProvider,calification,isAdmin,reasonCalification, idCompany, idUser, idComments, idProviderStatus, date', 'safe', 'on'=>'search'),
		);
	}

	public function defaultScope()
	{
		return array(
			'condition' => 'providerDeleted = 0'
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'idComments0' => array(self::BELONGS_TO, 'Comments', 'idComments'),
			'idCompany0' => array(self::BELONGS_TO, 'Company', 'idCompany'),
			'idProviderStatus0' => array(self::BELONGS_TO, 'ProviderStatus', 'idProviderStatus'),
			'idUser0' => array(self::BELONGS_TO, 'User', 'idUser')
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'idProvider' => 'Proveedor',
			'idCompany' => 'Compañia',
			'idUser' => 'Usuario',
			'idComments' => 'Comentario',
			'idProviderStatus' => 'Estado',
			'date' => 'Fecha',
			'calification' => 'Calificacion',
			'reasonCalification' => 'Razon de calificacion',
			'isAdmin' => 'Administrador',			
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('idProvider',$this->idProvider);
		$criteria->compare('idCompany',$this->idCompany);
		$criteria->compare('idUser',$this->idUser);
		$criteria->compare('idComments',$this->idComments);
		$criteria->compare('idProviderStatus',$this->idProviderStatus);
		$criteria->compare('date',$this->date,true);
		$criteria->compare('calification',$this->calification,true);
		$criteria->compare('reasonCalification',$this->reasonCalification,true);
		$criteria->compare('isAdmin',$this->isAdmin,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public function getUserName()
    {
    	return isset ($this->idUser0) && $this->idUser0 instanceof User ? $this->idUser0->email : "N/A";
    }

    public function getCompanyName()
    {
    	return isset ($this->idCompany0) && $this->idCompany0 instanceof Company ? $this->idCompany0->companyName : "N/A";
	}
	public function getProviderIsAdmin()
	{
		if($this->isAdmin == 0)
		{
			return "No";
		}
		else
		{
			return "Si";
		}
	}
    public function getCommenterDetail()
    {
    	return isset ($this->idComments0) && $this->idComments0 instanceof Comments ? $this->idComments0->description : "N/A";
    }

    public function getStatusProvider()
    {
    	return isset ($this->idProviderStatus0) && $this->idProviderStatus0 instanceof ProviderStatus ? $this->idProviderStatus0->description : "N/A";
	}
	public function getCalification()
	{
		if($this->calification == 0)
		{
			return "Sin calificar";
		}
		else
		{
			return $this->calification;
		}
	}
	public function deleteProvider($id)
	{

		$sql = "UPDATE provider set providerDeleted = 1 where idProvider = '$id';";
		//throw new Exception($sql);
        $command=Yii::app()->db->createCommand($sql);
		$command->query();
		return true;
	}
	public function sendMailLicitacion($codigoPO, $dateEnd) {
			//This should be done in your php.ini, but this is how to do it if you don't have access to that
			$mail = new Email();
			//configuracion fija
			$emailParameters = EmailParameters::model()->findByPk(1);
			$customEmail = CustomEmail::model()->findByPk(19);

			eval("\$sendSubject = \"$customEmail->subject\";");
            $mail->subject = $sendSubject;
			//$mail->subject ="Invitación a Cotizar – Petición de Oferta N° ".$codigoPO;
			
			//$mail->AddCC($participant->consultant->email);
			$date = date("d/m/Y H:i:s");
			/*$mail->body =
				'<div style="font-size: 10.0pt; font-family: \'Verdana\',sans-serif;">
			<p>Lo invitamos a participar de la cotización de precios, para la realización de una oferta técnica y económica (según corresponda su tipología: servicios, materiales, equipos o subcontratos), cuya fecha de finalización es '.$dateEnd.' .
			Para visualizarla, <a href="'.$emailParameters->hostForMails.'" target="_blank">Haga clic aquí</a></p>
					 <p class="m_-3923079614135954232xmsonormal"><span style="font-size: 9.0pt; font-family: \'Verdana\',sans-serif;">Este mensaje fue generado automáticamente por registrarse o realizar alguna acción en nuestro sitio de proveedores, se ha contactado vía mail o por teléfono con CONTRERAS. Si usted desconoce alguna acción de las antes mencionadas, descarte este mail. El contenido es propiedad de CONTRERAS y garantiza la privacidad y seguridad de su información.</span><u></u><u></u></p>
	            <p class="MsoNormal"><img id="m_-3923079614135954232x__x005f_x0000_i1025" style="width: 74px; height: 93px;" src="https://www.contreras.com.ar/img/contreras-logo.png" width="70" height="88" /></p>
                <p class="m_-3923079614135954232xmsonormal"><span style="font-size: 9.0pt; font-family: \'Verdana\',sans-serif;">San Martin 140, piso 8&deg;</span><u></u><u></u></p>
                <p class="m_-3923079614135954232xmsonormal"><span style="font-size: 9.0pt; font-family: \'Verdana\',sans-serif;">Buenos Aires, Argentina</span><u></u><u></u></p>
                <p class="m_-3923079614135954232xmsonormal"><span style="font-size: 9.0pt; font-family: \'Verdana\',sans-serif;">+54 (11) 4321 9500</span><u></u><u></u></p>
                <p class="m_-3923079614135954232xmsonormal"><span style="font-size: 10.0pt; font-family: \'Verdana\',sans-serif;"><a href="https://urldefense.proofpoint.com/v2/url?u=http-3A__www.contreras.com.ar_&amp;d=DwMFAw&amp;c=0TzQCy9lgR5hSW-bDg5HA76y7nf4lvOzvVop5GM3Y80&amp;r=lDiBsYLvsElGPOzCWcOqOemV6CUYuuTcfC74XGi7CUo&amp;m=8sseLaql5V9k-ag5cgaMdHVb0z4cwyDOoinA8rxc6E4&amp;s=Gco3yqJEEW6RxzCiGpPaywkYCh3zf9evo5sL36znb1Y&amp;e=" target="_blank" rel="noopener" data-saferedirecturl="https://www.google.com/url?q=https://urldefense.proofpoint.com/v2/url?u%3Dhttp-3A__www.contreras.com.ar_%26d%3DDwMFAw%26c%3D0TzQCy9lgR5hSW-bDg5HA76y7nf4lvOzvVop5GM3Y80%26r%3DlDiBsYLvsElGPOzCWcOqOemV6CUYuuTcfC74XGi7CUo%26m%3D8sseLaql5V9k-ag5cgaMdHVb0z4cwyDOoinA8rxc6E4%26s%3DGco3yqJEEW6RxzCiGpPaywkYCh3zf9evo5sL36znb1Y%26e%3D&amp;source=gmail&amp;ust=1569266816868000&amp;usg=AFQjCNGmf2yOrgexecmt8N0-TW0Zb1m49g">www.contreras.com.ar</a></span><u></u><u></u></p>
                <p><span style="font-size: 8.0pt; font-family: \'Verdana\',sans-serif; color: #828282;">Esta comunicaci&oacute;n y cualquier archivo adjunto contiene informaci&oacute;n reservada, confidencial, protegida por ley. Si no es usted el destinatario de la misma, observe que tiene terminantemente prohibido usar, difundir, distribuir o copiar esta comunicaci&oacute;n. Si ha recibido este mensaje por error deber&aacute; avisar inmediatamente de ello a su emisor por tel&eacute;fono o correo electr&oacute;nico y eliminarlo totalmente de su sistema. No se garantiza que las comunicaciones por Internet sean enviadas en el plazo correspondiente o que no contengan errores o virus. El emisor no aceptar&aacute; responsabilidad por errores u omisiones. Las opiniones vertidas son responsabilidad del autor y no son emitidas ni avaladas por Contreras ni ninguna de sus subsidiarias. <br /> <br /> No imprima este correo si no es necesario; todos tenemos un papel en el cuidado del medio ambiente. <br /> Do not print this mail if it is not necessary. <br /> <br /> This communication and any attached file contain information that is legally privileged, confidential or exempt from disclosure. If you are not the intended recipient, please note that any dissemination, distribution, or copying of this communication is strictly prohibited. Anyone who receives this message in error should notify the sender immediately by telephone or by return e-mail and delete this communication entirely from his or her computer. Contreras is neither liable for the proper and complete transmission of the information contained in this communication nor for any delay, error or virus. Any opinions presented are solely those of its author and do not necessarily represent those of Contreras or any of its subsidiary companies.</span></p>
                </div>'
			; *///Fin message HTML
			eval("\$sendBody = \"$customEmail->body\";");
			$mail->body = $sendBody;
			

			$saved = $mail->save();
			$mail->addReceiver($this->idUser0->email);
			return $saved;
	}
}
<?php

/**
 * This is the model class for table "categoryRetain".
 *
 * The followings are the available columns in table 'categoryRetain':
 * @property integer $idCategoryRetain
 * @property string $categoryRetain
 * @property integer $gain
 * @property integer $suss
 * @property integer $ib
 * @property integer $codeERP2
 * @property integer $vat
 * @property string $codeERP
 * @property integer $idRetentionRegime
 * @property integer $idJurisdictions
 * @property string $indicadorRet
  * The followings are the available model relations:
 * @property RetentionRegime $idRetentionRegime0
 */
class CategoryRetain extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return CategoryRetain the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'category_retain';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('idCategoryRetain,vat,gain,suss,ib,codeERP2,idRetentionRegime,indicadorRet,idJurisdictions', 'numerical', 'integerOnly'=>true),
			array('idRetentionRegime', 'required'),
			array('categoryRetain', 'length', 'max'=>50),
			array('categoryRetain', 'required'),
			array('codeERP', 'length', 'max'=>2)
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        //province
		return array(
			'idRetentionRegime0' => array(self::BELONGS_TO, 'RetentionRegime', 'idRetentionRegime'),
			'idJurisdictions0' => array(self::BELONGS_TO, 'Jurisdictions', 'idJurisdictions'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'idCategoryRetain' => Yii::t('app', 'Id categoría a retener'),
			'categoryRetain' => Yii::t('app', 'Categoría a retener'),
			'vat' => Yii::t('app', 'IVA'),
			'gain' => Yii::t('app', 'Ganancias'),
			'idJurisdictions' => Yii::t('app', 'Jurisdicción'),
			'suss' => Yii::t('app', 'Suss'),
			'ib' => Yii::t('app', 'Ing. Brutos'),
			'codeERP2' => Yii::t('app', 'CodERP2. Suss[NO se usa]'),
			'codeERP' => Yii::t('app', 'Código Categ.'),
			'idRetentionRegime' => Yii::t('app', 'Regimen de Retencion'),
			'indicadorRet' => Yii::t('app', 'Indicador Retención'),

		);
	}

	public function scopes() {
		return array(
			'findAllWithOrder' => array('order' => 'categoryRetain ASC'),
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */

    public function getList() {

        $sql = "SELECT * FROM categoryRetain;";
        $command=Yii::app()->db->createCommand($sql);
        return $data=$command->queryAll();
    }



	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('idCategoryRetain',$this->idCategoryRetain);
		$criteria->compare('categoryRetain',$this->categoryRetain,true);
		$criteria->compare('gain',$this->gain,true);
		$criteria->compare('idJurisdictions',$this->idJurisdictions,true);
		$criteria->compare('suss',$this->suss,true);
		$criteria->compare('ib',$this->ib,true);
		$criteria->compare('codeERP2',$this->codeERP2,true);
		$criteria->compare('vat',$this->vat,true);
		$criteria->compare('codeERP',$this->codeERP,true);
		$criteria->compare('idRetentionRegime',$this->idRetentionRegime,true);
		$criteria->compare('indicadorRet',$this->indicadorRet,true);

		$sort = new CSort();
		$sort->defaultOrder = 'categoryRetain ASC';

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'sort'=>$sort,
			'pagination'=>array(
				'pageSize'=>app()->controller->getItemsPerPage()
			),
		));
	}
	
	public function getRetentionRegime()
    {
    	return isset ($this->idRetentionRegime0) && $this->idRetentionRegime0 instanceof RetentionRegime ? $this->idRetentionRegime0->retentionRegime : "N/A";
	}
	public function getJurisdiction()
    {
    	return isset ($this->idJurisdictions0) && $this->idJurisdictions0 instanceof Jurisdictions ? $this->idJurisdictions0->jurisdictions : "N/A";
	}

	public function delete()
	{

			$criteria = new CDbCriteria();
			$criteria->condition = 'idCategoryRetain = :id';
			return parent::delete();
    }

}
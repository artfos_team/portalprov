<?php

/**
 * This is the model class for table "company".
 *
 * The followings are the available columns in table 'company':
 * @property integer $idRubro
 * @property string $nombre
 * @property integer $status
 * @property integer $comment

 */
class BuyOrder extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Licitaciones the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'buy_order';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('nic, statusId, idCompany, view', 'numerical', 'integerOnly'=>true),
            array('pdf, fecha_liberacion, importDate', 'length', 'max'=>255),
		);
	}

    public function isValidDate($attribute, $params)
    {
        if(!strtotime($this->$attribute))
        {
            $this->addError($attribute, $attribute . ' was not a valid date');
        }
    }

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'status' => array(self::BELONGS_TO, 'BuyOrderStatus', 'statusId'),
			'company' => array(self::BELONGS_TO, 'Company', 'idCompany'),
			'buyOrderDocuments' => array(self::HAS_MANY, 'BuyOrderDocument', 'idBuyOrder'),
			'hes' => array(self::HAS_MANY, 'Hes', 'idBuyOrder'),
			'migo' => array(self::HAS_MANY, 'Migo', 'idBuyOrder')
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'nic' => Yii::t('app', 'NIC'),
			'fecha_liberacion' => Yii::t('app', 'Fecha de documentacion'),
			'statusId' => Yii::t('app', 'Estado'),
			'view' => Yii::t('app', 'Visto'),
			'idCompany' => Yii::t('app', 'Compañia'),
            'comment' => Yii::t('app', 'Comentario'),


        );
	}

	public function scopes() {
		return array(
			'findAllWithOrder' => array('order' => 'nic ASC'),
		);
	}
	/*
	public function defaultScope()
	{
		return array(
			'condition' => 'status = 1'
		);
	}*/

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */

    public function getList() {

        $sql = "SELECT * FROM buyOrder;";
        $command=Yii::app()->db->createCommand($sql);
        return $data=$command->queryAll();
    }



	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('nic',$this->nic);
		$criteria->compare('fecha_liberacion',$this->fecha_liberacion,true);
		$criteria->compare('view',$this->view,true);
		$criteria->compare('idCompany',$this->idCompany,true);
        $criteria->compare('comment',$this->comment,true);


		$sort = new CSort();
		$sort->defaultOrder = 'nic ASC';

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'sort'=>$sort,
			'pagination'=>array(
				'pageSize'=>app()->controller->getItemsPerPage()
			),
		));
	}
	
	public function delete()
	{

			$criteria = new CDbCriteria();
			$criteria->condition = 'nic = :id';
			$criteria->params[':id'] = $this->idDocument;
			CompanyContact::model()->deleteAll($criteria);
			CompanyAddress::model()->deleteAll($criteria);
			return parent::delete();
	}
	
	public function getCompanyName() {
		return isset($this->company->company) ? $this->company->company : $this->company->cuit;
	}

	public function getStatusDescription() {
		return $this->status instanceof BuyOrderStatus ? $this->status->description : 'N/A';
	}

	public function getDateOfFirstSentDocument() {
		$document = BuyOrderDocument::model()->find('idBuyOrder = '.$this->idBuyOrder.' order by idBuyOrderDocumentStatus, date ASC');
		return is_array($this->buyOrderDocuments) && isset($document) ? $document->date : '-';
	}
}
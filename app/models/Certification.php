<?php

/**
 * This is the model class for table "Certification".
 *
 * The followings are the available columns in table 'Certification':
 * @property integer $idCertification
 * @property string $type_certification
 * @property integer $cuit
 * @property string $number
 * @property string $period
 * @property string $percentage
 * @property string $resolution
 * @property string $state
 * @property string $since
 * @property string $until
 
 */
class Certification extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Certification the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'certification';

	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			//array('idCertification', 'numerical', 'integerOnly'=>true),
            array('type_certification,cuit,number, period,percentage, resolution, state, since, until', 'length', 'max'=>255),		
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'idCertification' => Yii::t('app', 'Id Certificacion'),
			'type_certification' => Yii::t('app', 'Tipo de Certificacion'),
			'cuit' => Yii::t('app', 'cuit'),
			'number' => Yii::t('app', 'Numero'),
            'period' => Yii::t('app', 'Periodo'),
			'percentage' => Yii::t('app', 'Porcentaje'),
			'resolution' => Yii::t('app', 'Resolucion'),
			'status' => Yii::t('app', 'Estado'),
			'since' => Yii::t('app', 'Desde'),
			'until' => Yii::t('app', 'Hasta')

		);
	}


	/*


	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */


	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('idCertification',$this->idCertification,true);
		$criteria->compare('type_certification',$this->type_certification,true);
		$criteria->compare('cuit',$this->cuit,true);
		$criteria->compare('number',$this->number,true);
		$criteria->compare('period',$this->period,true);
		$criteria->compare('percentage',$this->percentage,true);
		$criteria->compare('resolution',$this->resolution,true);
		$criteria->compare('state',$this->state,true);
		$criteria->compare('since',$this->since,true);
		$criteria->compare('until',$this->until,true);


		$sort = new CSort();
		$sort->defaultOrder = 'nombre ASC';

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'sort'=>$sort,
			'pagination'=>array(
				'pageSize'=>app()->controller->getItemsPerPage()
			),
		));
	}
	

}
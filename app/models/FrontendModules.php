<?php

/**
 * This is the model class for table "user_has_roles".
 *
 * The followings are the available columns in table 'user_has_roles':
 * @property integer $idUser
 * @property integer $rolesId
 */
class FrontendModules extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return UserHasRoles the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'frontend_modules';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('idModule, order', 'required'),
			array('idModule, order', 'numerical', 'integerOnly'=>true),
			array('description', 'length', 'max'=>50),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('idModule, description,order', 'safe', 'on'=>'search'),
		);
	}

	public function scopes() {
		return array(
			'findAllWithOrder' => array('order' => 'order ASC'),
		);
	}
	
	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'idModule' => 'Id Rol',
			'rolesId' => 'Roles',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('idModule',$this->idModule);
		$criteria->compare('rolesId',$this->rolesId);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}
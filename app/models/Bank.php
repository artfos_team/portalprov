<?php

/**
 * This is the model class for table "Bank".
 *
 * The followings are the available columns in table 'Bank':
 * @property integer $idBank
 * @property string $Bank
 */
class Bank extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Bank the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'bank';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('idBank', 'numerical', 'integerOnly'=>true),
            array('bank, claveBanco', 'length', 'max'=>200),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        //province
		return array(
           
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'idBank' => Yii::t('app', 'Id Banco'),
			'bank' => Yii::t('app', 'Banco'),
			'claveBanco' => Yii::t('app', 'Clave Banco'),
		);
	}

	public function scopes() {
		return array(
			'findAllWithOrder' => array('order' => 'bank ASC'),
		);
	}
	/*
	public function defaultScope()
	{
		return array(
			'condition' => 'status = 1'
		);
	}*/

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */

    public function getList() {

        $sql = "SELECT * FROM bank;";
        $command=Yii::app()->db->createCommand($sql);
        return $data=$command->queryAll();
    }



	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('idBank',$this->idBank);
		$criteria->compare('bank',$this->bank,true);
		$criteria->compare('claveBanco',$this->claveBanco,true);

		$sort = new CSort();
		$sort->defaultOrder = 'bank ASC';

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'sort'=>$sort,
			'pagination'=>array(
				'pageSize'=>app()->controller->getItemsPerPage()
			),
		));
	}
	
	public function delete()
	{

			$criteria = new CDbCriteria();
			$criteria->condition = 'idBank = :id';
			//CompanyContact::model()->deleteAll($criteria);
			//CompanyAddress::model()->deleteAll($criteria);
			return parent::delete();
    }

}
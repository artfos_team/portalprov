<?php

/**
 * This is the model class for table "company_general_data".
 *
 * The followings are the available columns in table 'company_general_data':
 * @property integer $idGeneralData
 * @property string $address
 * @property string $addressNumber
 * @property integer $idProvince
 * @property integer $idCountry
 * @property string $zipCode
 * @property string $city
 * @property string $phone
 * @property string $paymentEmail
 * @property string $receiptEmail
 * @property string $typeIVA
 * @property string $personType
 * @property string $personSubType
 * @property string $personSubTypeOther
 * @property string $pdfAfip
 * @property integer $idCompanyDataStatus
 * @property string $observation
 * @property integer $idUser
 * @property string $namePrincipalContact
 * @property string $surnamePrincipalContact
 * @property string $phonePrincipalContact
 * @property string $emailPrincipalContact
 * @property string $nameCommercialContact
 * @property string $surnameCommercialContact
 * @property string $phoneCommercialContact
 * @property string $emailCommercialContact
 * @property integer $idGroupCompany
 * @property string $modifDate
 *
 * The followings are the available model relations:
 * @property Company[] $companies
 * @property CompanyDataStatus $idCompanyDataStatus0
 * @property Country $idCountry0
 * @property Province $idProvince0
 * @property User $idUser0
 * @property idGroupCompany $idGroupCompany0
 */
class LogCompanyGeneralData extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'log_company_general_data';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('idProvince, idCountry, idCompanyDataStatus, idUser, idGroupCompany', 'numerical', 'integerOnly'=>true),
			array('address, city, paymentEmail, receiptEmail, namePrincipalContact, nameCommercialContact, surnamePrincipalContact, surnameCommercialContact, phonePrincipalContact, phoneCommercialContact, emailPrincipalContact, emailCommercialContact', 'length', 'max'=>100),
			array('addressNumber, pdfAfip', 'length', 'max'=>200),
			array('zipCode, phone, typeIVA, personType', 'length', 'max'=>20),
			array('personSubType', 'length', 'max'=>10),
			array('personSubTypeOther', 'length', 'max'=>30),
			array('observation', 'length', 'max'=>400),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('idGeneralData, address, addressNumber, idProvince, idCountry, zipCode, city, phone, paymentEmail, receiptEmail, typeIVA, personType, personSubType, personSubTypeOther, pdfAfip, idCompanyDataStatus, observation, idUser, idGroupCompany', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'companies' => array(self::HAS_MANY, 'Company', 'idGeneralData'),
			'idCompanyDataStatus0' => array(self::BELONGS_TO, 'CompanyDataStatus', 'idCompanyDataStatus'),
			'idCountry0' => array(self::BELONGS_TO, 'Country', 'idCountry'),
			'idProvince0' => array(self::BELONGS_TO, 'Province', 'idProvince'),
			'idUser0' => array(self::BELONGS_TO, 'User', 'idUser'),
			'idGroupCompany0' => array(self::BELONGS_TO, 'GroupCompanies', 'idGroupCompany'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'idGeneralData' => 'Id General Data',
			'address' => 'Address',
			'addressNumber' => 'Address Number',
			'idProvince' => 'Id Province',
			'idCountry' => 'Id Country',
			'zipCode' => 'Zip Code',
			'city' => 'City',
			'phone' => 'Phone',
			'paymentEmail' => 'Payment Email',
			'receiptEmail' => 'Receipt Email',
			'typeIVA' => 'Type Iva',
			'personType' => 'Person Type',
			'personSubType' => 'Person Sub Type',
			'personSubTypeOther' => 'Person Sub Type Other',
			'pdfAfip' => 'Pdf Afip',
			'idCompanyDataStatus' => 'Id Company Data Status',
			'observation' => 'Observation',
			'idUser' => 'Id User',
			'namePrincipalContact' => 'Principal Name Contact',
			'nameCommercialContact' => 'Commercial Name Contact',
			'surnamePrincipalContact' => 'Principal Surname Contact',
			'surnameCommercialContact' => 'Commercial Surname Contact',
			'phonePrincipalContact' => 'Principal Phone Contact',
			'phoneCommercialContact' => 'Commercial Phone Contact',
			'emailPrincipalContact' => 'Principal email Contact',
			'emailCommercialContact' => 'Commercial email Contact',
			'idGroupCompany' => 'Id Group Company',
			'modifDate' => Yii::t('app', 'Fecha'),
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('idGeneralData',$this->idGeneralData);
		$criteria->compare('address',$this->address,true);
		$criteria->compare('addressNumber',$this->addressNumber,true);
		$criteria->compare('idProvince',$this->idProvince);
		$criteria->compare('idCountry',$this->idCountry);
		$criteria->compare('zipCode',$this->zipCode,true);
		$criteria->compare('city',$this->city,true);
		$criteria->compare('phone',$this->phone,true);
		$criteria->compare('paymentEmail',$this->paymentEmail,true);
		$criteria->compare('receiptEmail',$this->receiptEmail,true);
		$criteria->compare('typeIVA',$this->typeIVA,true);
		$criteria->compare('personType',$this->personType,true);
		$criteria->compare('personSubType',$this->personSubType,true);
		$criteria->compare('personSubTypeOther',$this->personSubTypeOther,true);
		$criteria->compare('pdfAfip',$this->pdfAfip,true);
		$criteria->compare('idCompanyDataStatus',$this->idCompanyDataStatus);
		$criteria->compare('observation',$this->observation,true);
		$criteria->compare('idUser',$this->idUser);
		$criteria->compare('namePrincipalContact',$this->namePrincipalContact);
		$criteria->compare('nameCommercialContact',$this->nameCommercialContact);
		$criteria->compare('surnamePrincipalContact',$this->surnamePrincipalContact);
		$criteria->compare('surnameCommercialContact',$this->surnameCommercialContact);
		$criteria->compare('phonePrincipalContact',$this->phonePrincipalContact);
		$criteria->compare('phoneCommercialContact',$this->phoneCommercialContact);
		$criteria->compare('emailPrincipalContact',$this->emailPrincipalContact);
		$criteria->compare('emailCommercialContact',$this->emailCommercialContact);
		$criteria->compare('idGroupCompany',$this->idGroupCompany);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return CompanyGeneralData the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}

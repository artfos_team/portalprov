<?php

Yii::import('application.controllers.api.ApiClassFactory');
ob_start('ob_gzhandler');
/**
 * Created by PhpStorm.
 * User: pgallar
 * Date: 11/7/17
 * Time: 14:10
 */
class ApiController extends AdminController
{
	// Members
	/**
	 * Key which has to be in HTTP USERNAME and PASSWORD headers
	 */
	Const APPLICATION_ID = 'IPROVEEDORES';

	/**
	 * Default response format
	 * either 'json' or 'xml'
	 */
	private $format = 'json';

	/**
	 * @var ApiClassFactory
	 */
	private $apiClassFactory;

	public function __construct($id) {

		parent::__construct($id);

		$this->apiClassFactory = new ApiClassFactory();
	}

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array();
	}

 
	public function actionUpdatePsw()
	{
		try {
			if (!isset($_GET['model'])) {
				throw new Exception("No ha sido especificado ningun  modelo");
			}
			$id = $_GET['id'];
			$password = $_GET['psw'];
			$model = $this->apiClassFactory->getApiModel($_GET['model']);
			//echo CJavaScript::jsonEncode(array('data' => $model->getList()));
			$model_type= get_class($model);

			echo $model_type::model()->updatePsw($id,$password);
        	//echo $model_type::model()->findByPk($id)->password;
		}
		catch(Exception $ex) {
			echo CJavaScript::jsonEncode(array('error' => $ex->getMessage()));
		}

		Yii::app()->end();
	}




	public function actionApiRestWeb()
	{
		if(!$this->actionCheckFront())
		{
			return;
		}
 
	   //$_GET['apellido'];
		try {
			if (!isset($_GET['model'])) {
				throw new Exception("No ha sido especificado ningun  modelo");
			}
			$tipo = $_GET['tipo'];
			$parametro1 = $_GET['parametro1'];
			$parametro2 = $_GET['parametro2'];
			$model = $this->apiClassFactory->getApiModel($_GET['model']);
			//echo CJavaScript::jsonEncode(array('data' => $model->getList()));
			$model_type= get_class($model);
			$models = $model_type::model()->ApiRestWeb($tipo, $parametro1, $parametro2);
			$this->_sendResponse(200, CJSON::encode($models,true));
        	
		}
		catch(Exception $ex) {
			echo CJavaScript::jsonEncode(array('error' => $ex->getMessage()));
		}

		Yii::app()->end();
	}

	public function actionsendMailReestablecer()
	{
		if(!$this->actionCheckFront())
		{
			return;
		}
 
	   //$_GET['apellido'];
		try {
			if (!isset($_GET['model'])) {
				throw new Exception("No ha sido especificado ningun  modelo");
			}
			$nombre = $_GET['nombre'];
		
			$model = $this->apiClassFactory->getApiModel($_GET['model']);
			//echo CJavaScript::jsonEncode(array('data' => $model->getList()));
			$model_type= get_class($model);
			$models = $model_type::model()->sendMailReestablecer($nombre);
			$this->_sendResponse(200, CJSON::encode($models,true));
        	
		}
		catch(Exception $ex) {
			echo CJavaScript::jsonEncode(array('error' => $ex->getMessage()));
		}

		Yii::app()->end();
	}

	public function actionCompanyDocument()
	{
		$this->_checkAuth();
		try {
			if (!isset($_GET['model'])) {
				throw new Exception("No ha sido especificado ningun  modelo");
			}
			$company = $_GET['id'];
		
			$model = $this->apiClassFactory->getApiModel($_GET['model']);
			//echo CJavaScript::jsonEncode(array('data' => $model->getList()));
			$model_type= get_class($model);
			$models = $model_type::model()->getCompanyDocument($company);
			$this->_sendResponse(200, CJSON::encode($models,true));
        	
		}
		catch(Exception $ex) {
			echo CJavaScript::jsonEncode(array('error' => $ex->getMessage()));
		}

		Yii::app()->end();
	}

	public function actionCompanyOperations()
	{
		$this->_checkAuth();
		try {
			$company = $_GET['id'];
		
			$operations = BuyOrder::model()->findAll('idCompany = '.$company);

			$this->_sendResponse(200, CJSON::encode($operations,true));
        	
		}
		catch(Exception $ex) {
			echo CJavaScript::jsonEncode(array('error' => $ex->getMessage()));
		}

		Yii::app()->end();
	}
	public function actionAssociatedUsers()
	{
		$this->_checkAuth();
		try {
			$company = $_GET['id'];
		
			$users = User::model()->findAll('idCompany = '.$company);

			$this->_sendResponse(200, CJSON::encode($users,true));
        	
		}
		catch(Exception $ex) {
			echo CJavaScript::jsonEncode(array('error' => $ex->getMessage()));
		}

		Yii::app()->end();
	}
	public function actionCertificationExgain()
	{
		$this->_checkAuth();
		try {
			$company = $_GET['id'];
		
			$users = User::model()->findAll('idCompany = '.$company);

			$this->_sendResponse(200, CJSON::encode($users,true));
        	
		}
		catch(Exception $ex) {
			echo CJavaScript::jsonEncode(array('error' => $ex->getMessage()));
		}

		Yii::app()->end();
	}	
	public function actionSendMailBuyOrderStatus()
	{
		$this->_checkAuth();
		$idBuyOrder = $_GET['id'];
		$buyOrder = BuyOrder::model()->findByPk($idBuyOrder);
		$status = $buyOrder->statusId;
		
		$users = array();

		if ($status == 2 || $status == 6) {
			$providers = Provider::model()->findAll('idCompany = '. $buyOrder->idCompany);
			if(count($providers) <= 0){
				$this->_sendResponse(200, CJSON::encode(array('failure' => 'No se encuentran proveedores asociados a la compañia.')));
			}
			foreach($providers as $index => $provider) {
				$users[$index] = User::model()->findByPk($provider->idUser);
			}
		} elseif($status == 4) {
			$usersWithoutFilter = User::model()->findall();
			if(count($usersWithoutFilter) <= 0){
				$this->_sendResponse(200, CJSON::encode(array('failure' => 'No se encuentran usuarios con rol de Pagos.')));
			}
			foreach ($usersWithoutFilter as $index => $user) {
				if($user->hasRole(2))
				{
					$users[$index] = $user; 
				}
			}
		}elseif($status == 8) {
            $providers = Provider::model()->findAll('idCompany = '. $buyOrder->idCompany);
            if(count($providers) <= 0){
                $this->_sendResponse(200, CJSON::encode(array('failure' => 'No se encuentran proveedores asociados a la compañia.')));
            }
            foreach($providers as $index => $provider) {
                $users[$index] = User::model()->findByPk($provider->idUser);
            }
        }
		if (count($users) == 0) {
			$this->_sendResponse(200, CJSON::encode(array('success' => 'No se encontraron usuarios a quien enviarles el correo electrónico')));
		}

			$mail = new Email();
			//configuracion fija
			$emailParameters = EmailParameters::model()->findByPk(1);
			$customEmail = CustomEmail::model()->findByPk(6);
			
			if($status == 2){
				//$mail->subject ="Notificación Interna de Compra";
				$mail->subject = $customEmail->subject;
				/*$mail->body =
					'<div style="font-size: 10.0pt; font-family: \'Verdana\',sans-serif;">
					<p>Le informamos que se encuentra realizada la Notificación Interna de Compra (NIC) bajo el N°'.$buyOrder->nic.'</p>
					 <p class="m_-3923079614135954232xmsonormal"><span style="font-size: 9.0pt; font-family: \'Verdana\',sans-serif;">Este mensaje fue generado automáticamente por registrarse o realizar alguna acción en nuestro sitio de proveedores, se ha contactado vía mail o por teléfono con CONTRERAS. Si usted desconoce alguna acción de las antes mencionadas, descarte este mail. El contenido es propiedad de CONTRERAS y garantiza la privacidad y seguridad de su información.</span><u></u><u></u></p>
					 <p class="MsoNormal"><img id="m_-3923079614135954232x__x005f_x0000_i1025" style="width: 74px; height: 93px;" src="https://www.contreras.com.ar/img/contreras-logo.png" width="70" height="88" /></p>
                    <p class="m_-3923079614135954232xmsonormal"><span style="font-size: 9.0pt; font-family: \'Verdana\',sans-serif;">San Martin 140, piso 8&deg;</span><u></u><u></u></p>
                    <p class="m_-3923079614135954232xmsonormal"><span style="font-size: 9.0pt; font-family: \'Verdana\',sans-serif;">Buenos Aires, Argentina</span><u></u><u></u></p>
                    <p class="m_-3923079614135954232xmsonormal"><span style="font-size: 9.0pt; font-family: \'Verdana\',sans-serif;">+54 (11) 4321 9500</span><u></u><u></u></p>
                    <p class="m_-3923079614135954232xmsonormal"><span style="font-size: 10.0pt; font-family: \'Verdana\',sans-serif;"><a href="https://urldefense.proofpoint.com/v2/url?u=http-3A__www.contreras.com.ar_&amp;d=DwMFAw&amp;c=0TzQCy9lgR5hSW-bDg5HA76y7nf4lvOzvVop5GM3Y80&amp;r=lDiBsYLvsElGPOzCWcOqOemV6CUYuuTcfC74XGi7CUo&amp;m=8sseLaql5V9k-ag5cgaMdHVb0z4cwyDOoinA8rxc6E4&amp;s=Gco3yqJEEW6RxzCiGpPaywkYCh3zf9evo5sL36znb1Y&amp;e=" target="_blank" rel="noopener" data-saferedirecturl="https://www.google.com/url?q=https://urldefense.proofpoint.com/v2/url?u%3Dhttp-3A__www.contreras.com.ar_%26d%3DDwMFAw%26c%3D0TzQCy9lgR5hSW-bDg5HA76y7nf4lvOzvVop5GM3Y80%26r%3DlDiBsYLvsElGPOzCWcOqOemV6CUYuuTcfC74XGi7CUo%26m%3D8sseLaql5V9k-ag5cgaMdHVb0z4cwyDOoinA8rxc6E4%26s%3DGco3yqJEEW6RxzCiGpPaywkYCh3zf9evo5sL36znb1Y%26e%3D&amp;source=gmail&amp;ust=1569266816868000&amp;usg=AFQjCNGmf2yOrgexecmt8N0-TW0Zb1m49g">www.contreras.com.ar</a></span><u></u><u></u></p>
                    <p><span style="font-size: 8.0pt; font-family: \'Verdana\',sans-serif; color: #828282;">Esta comunicaci&oacute;n y cualquier archivo adjunto contiene informaci&oacute;n reservada, confidencial, protegida por ley. Si no es usted el destinatario de la misma, observe que tiene terminantemente prohibido usar, difundir, distribuir o copiar esta comunicaci&oacute;n. Si ha recibido este mensaje por error deber&aacute; avisar inmediatamente de ello a su emisor por tel&eacute;fono o correo electr&oacute;nico y eliminarlo totalmente de su sistema. No se garantiza que las comunicaciones por Internet sean enviadas en el plazo correspondiente o que no contengan errores o virus. El emisor no aceptar&aacute; responsabilidad por errores u omisiones. Las opiniones vertidas son responsabilidad del autor y no son emitidas ni avaladas por Contreras ni ninguna de sus subsidiarias. <br /> <br /> No imprima este correo si no es necesario; todos tenemos un papel en el cuidado del medio ambiente. <br /> Do not print this mail if it is not necessary. <br /> <br /> This communication and any attached file contain information that is legally privileged, confidential or exempt from disclosure. If you are not the intended recipient, please note that any dissemination, distribution, or copying of this communication is strictly prohibited. Anyone who receives this message in error should notify the sender immediately by telephone or by return e-mail and delete this communication entirely from his or her computer. Contreras is neither liable for the proper and complete transmission of the information contained in this communication nor for any delay, error or virus. Any opinions presented are solely those of its author and do not necessarily represent those of Contreras or any of its subsidiary companies.</span></p>
				
					</div>';*/
				eval("\$sendBody = \"$customEmail->body\";");
				$mail->body = $sendBody;
			}elseif($status == 4){
				$customEmail = CustomEmail::model()->findByPk(7);
				//$mail->subject ="Documento Disponible";
				$mail->subject = $customEmail->subject;
				/*$mail->body =
					'<div style="font-size: 10.0pt; font-family: \'Verdana\',sans-serif;">
					<p>Le informamos que le han remitido documentación vinculada a la Notificación Interna de Compra N° '.$buyOrder->nic.' .</p>
					<p class="m_-3923079614135954232xmsonormal"><span style="font-size: 9.0pt; font-family: \'Verdana\',sans-serif;">Este mensaje fue generado automáticamente por registrarse o realizar alguna acción en nuestro sitio de proveedores, se ha contactado vía mail o por teléfono con CONTRERAS. Si usted desconoce alguna acción de las antes mencionadas, descarte este mail. El contenido es propiedad de CONTRERAS y garantiza la privacidad y seguridad de su información.</span><u></u><u></u></p>
					<p class="MsoNormal"><img id="m_-3923079614135954232x__x005f_x0000_i1025" style="width: 74px; height: 93px;" src="https://www.contreras.com.ar/img/contreras-logo.png" width="70" height="88" /></p>
                    <p class="m_-3923079614135954232xmsonormal"><span style="font-size: 9.0pt; font-family: \'Verdana\',sans-serif;">San Martin 140, piso 8&deg;</span><u></u><u></u></p>
                    <p class="m_-3923079614135954232xmsonormal"><span style="font-size: 9.0pt; font-family: \'Verdana\',sans-serif;">Buenos Aires, Argentina</span><u></u><u></u></p>
                    <p class="m_-3923079614135954232xmsonormal"><span style="font-size: 9.0pt; font-family: \'Verdana\',sans-serif;">+54 (11) 4321 9500</span><u></u><u></u></p>
                    <p class="m_-3923079614135954232xmsonormal"><span style="font-size: 10.0pt; font-family: \'Verdana\',sans-serif;"><a href="https://urldefense.proofpoint.com/v2/url?u=http-3A__www.contreras.com.ar_&amp;d=DwMFAw&amp;c=0TzQCy9lgR5hSW-bDg5HA76y7nf4lvOzvVop5GM3Y80&amp;r=lDiBsYLvsElGPOzCWcOqOemV6CUYuuTcfC74XGi7CUo&amp;m=8sseLaql5V9k-ag5cgaMdHVb0z4cwyDOoinA8rxc6E4&amp;s=Gco3yqJEEW6RxzCiGpPaywkYCh3zf9evo5sL36znb1Y&amp;e=" target="_blank" rel="noopener" data-saferedirecturl="https://www.google.com/url?q=https://urldefense.proofpoint.com/v2/url?u%3Dhttp-3A__www.contreras.com.ar_%26d%3DDwMFAw%26c%3D0TzQCy9lgR5hSW-bDg5HA76y7nf4lvOzvVop5GM3Y80%26r%3DlDiBsYLvsElGPOzCWcOqOemV6CUYuuTcfC74XGi7CUo%26m%3D8sseLaql5V9k-ag5cgaMdHVb0z4cwyDOoinA8rxc6E4%26s%3DGco3yqJEEW6RxzCiGpPaywkYCh3zf9evo5sL36znb1Y%26e%3D&amp;source=gmail&amp;ust=1569266816868000&amp;usg=AFQjCNGmf2yOrgexecmt8N0-TW0Zb1m49g">www.contreras.com.ar</a></span><u></u><u></u></p>
                    <p><span style="font-size: 8.0pt; font-family: \'Verdana\',sans-serif; color: #828282;">Esta comunicaci&oacute;n y cualquier archivo adjunto contiene informaci&oacute;n reservada, confidencial, protegida por ley. Si no es usted el destinatario de la misma, observe que tiene terminantemente prohibido usar, difundir, distribuir o copiar esta comunicaci&oacute;n. Si ha recibido este mensaje por error deber&aacute; avisar inmediatamente de ello a su emisor por tel&eacute;fono o correo electr&oacute;nico y eliminarlo totalmente de su sistema. No se garantiza que las comunicaciones por Internet sean enviadas en el plazo correspondiente o que no contengan errores o virus. El emisor no aceptar&aacute; responsabilidad por errores u omisiones. Las opiniones vertidas son responsabilidad del autor y no son emitidas ni avaladas por Contreras ni ninguna de sus subsidiarias. <br /> <br /> No imprima este correo si no es necesario; todos tenemos un papel en el cuidado del medio ambiente. <br /> Do not print this mail if it is not necessary. <br /> <br /> This communication and any attached file contain information that is legally privileged, confidential or exempt from disclosure. If you are not the intended recipient, please note that any dissemination, distribution, or copying of this communication is strictly prohibited. Anyone who receives this message in error should notify the sender immediately by telephone or by return e-mail and delete this communication entirely from his or her computer. Contreras is neither liable for the proper and complete transmission of the information contained in this communication nor for any delay, error or virus. Any opinions presented are solely those of its author and do not necessarily represent those of Contreras or any of its subsidiary companies.</span></p>   
					</div>
				   ';*/
				eval("\$sendBody1 = \"$customEmail->body\";");
				$mail->body = $sendBody1;
            }elseif($status == 8){
				$customEmail = CustomEmail::model()->findByPk(8);
				//$mail->subject ="Documento Devuelto";
				$mail->subject = $customEmail->subject;
                /*$mail->body =
                    '<div style="font-size: 10.0pt; font-family: \'Verdana\',sans-serif;">
					<p>Le informamos que le han devuelto documentación vinculada a la Notificación Interna de Compra N° '.$buyOrder->nic.' .</p>
					<p class="m_-3923079614135954232xmsonormal"><span style="font-size: 9.0pt; font-family: \'Verdana\',sans-serif;">Este mensaje fue generado automáticamente por registrarse o realizar alguna acción en nuestro sitio de proveedores, se ha contactado vía mail o por teléfono con CONTRERAS. Si usted desconoce alguna acción de las antes mencionadas, descarte este mail. El contenido es propiedad de CONTRERAS y garantiza la privacidad y seguridad de su información.</span><u></u><u></u></p>
					<p class="MsoNormal"><img id="m_-3923079614135954232x__x005f_x0000_i1025" style="width: 74px; height: 93px;" src="https://www.contreras.com.ar/img/contreras-logo.png" width="70" height="88" /></p>
                    <p class="m_-3923079614135954232xmsonormal"><span style="font-size: 9.0pt; font-family: \'Verdana\',sans-serif;">San Martin 140, piso 8&deg;</span><u></u><u></u></p>
                    <p class="m_-3923079614135954232xmsonormal"><span style="font-size: 9.0pt; font-family: \'Verdana\',sans-serif;">Buenos Aires, Argentina</span><u></u><u></u></p>
                    <p class="m_-3923079614135954232xmsonormal"><span style="font-size: 9.0pt; font-family: \'Verdana\',sans-serif;">+54 (11) 4321 9500</span><u></u><u></u></p>
                    <p class="m_-3923079614135954232xmsonormal"><span style="font-size: 10.0pt; font-family: \'Verdana\',sans-serif;"><a href="https://urldefense.proofpoint.com/v2/url?u=http-3A__www.contreras.com.ar_&amp;d=DwMFAw&amp;c=0TzQCy9lgR5hSW-bDg5HA76y7nf4lvOzvVop5GM3Y80&amp;r=lDiBsYLvsElGPOzCWcOqOemV6CUYuuTcfC74XGi7CUo&amp;m=8sseLaql5V9k-ag5cgaMdHVb0z4cwyDOoinA8rxc6E4&amp;s=Gco3yqJEEW6RxzCiGpPaywkYCh3zf9evo5sL36znb1Y&amp;e=" target="_blank" rel="noopener" data-saferedirecturl="https://www.google.com/url?q=https://urldefense.proofpoint.com/v2/url?u%3Dhttp-3A__www.contreras.com.ar_%26d%3DDwMFAw%26c%3D0TzQCy9lgR5hSW-bDg5HA76y7nf4lvOzvVop5GM3Y80%26r%3DlDiBsYLvsElGPOzCWcOqOemV6CUYuuTcfC74XGi7CUo%26m%3D8sseLaql5V9k-ag5cgaMdHVb0z4cwyDOoinA8rxc6E4%26s%3DGco3yqJEEW6RxzCiGpPaywkYCh3zf9evo5sL36znb1Y%26e%3D&amp;source=gmail&amp;ust=1569266816868000&amp;usg=AFQjCNGmf2yOrgexecmt8N0-TW0Zb1m49g">www.contreras.com.ar</a></span><u></u><u></u></p>
                    <p><span style="font-size: 8.0pt; font-family: \'Verdana\',sans-serif; color: #828282;">Esta comunicaci&oacute;n y cualquier archivo adjunto contiene informaci&oacute;n reservada, confidencial, protegida por ley. Si no es usted el destinatario de la misma, observe que tiene terminantemente prohibido usar, difundir, distribuir o copiar esta comunicaci&oacute;n. Si ha recibido este mensaje por error deber&aacute; avisar inmediatamente de ello a su emisor por tel&eacute;fono o correo electr&oacute;nico y eliminarlo totalmente de su sistema. No se garantiza que las comunicaciones por Internet sean enviadas en el plazo correspondiente o que no contengan errores o virus. El emisor no aceptar&aacute; responsabilidad por errores u omisiones. Las opiniones vertidas son responsabilidad del autor y no son emitidas ni avaladas por Contreras ni ninguna de sus subsidiarias. <br /> <br /> No imprima este correo si no es necesario; todos tenemos un papel en el cuidado del medio ambiente. <br /> Do not print this mail if it is not necessary. <br /> <br /> This communication and any attached file contain information that is legally privileged, confidential or exempt from disclosure. If you are not the intended recipient, please note that any dissemination, distribution, or copying of this communication is strictly prohibited. Anyone who receives this message in error should notify the sender immediately by telephone or by return e-mail and delete this communication entirely from his or her computer. Contreras is neither liable for the proper and complete transmission of the information contained in this communication nor for any delay, error or virus. Any opinions presented are solely those of its author and do not necessarily represent those of Contreras or any of its subsidiary companies.</span></p>   
					</div>
				   ';*/
				eval("\$sendBody2 = \"$customEmail->body\";");
				$mail->body = $sendBody2;
			}elseif($status == 6){
				$customEmail = CustomEmail::model()->findByPk(9);
				//$mail->subject ="Orden de pago disponible";
				$mail->subject = $customEmail->subject;
				/*$mail->body ='
					<p> '.$buyOrder->comment.' </p>
					<p>nota: el horario para consultas es lunes y viernes de 15 a 17hs llamando telefónicamente al 4321-9500</p>
					<p class="m_-3923079614135954232xmsonormal"><span style="font-size: 9.0pt; font-family: \'Verdana\',sans-serif;">Este mensaje fue generado automáticamente por registrarse o realizar alguna acción en nuestro sitio de proveedores, se ha contactado vía mail o por teléfono con CONTRERAS. Si usted desconoce alguna acción de las antes mencionadas, descarte este mail. El contenido es propiedad de CONTRERAS y garantiza la privacidad y seguridad de su información.</span><u></u><u></u></p>
					<p class="MsoNormal"><img id="m_-3923079614135954232x__x005f_x0000_i1025" style="width: 74px; height: 93px;" src="https://www.contreras.com.ar/img/contreras-logo.png" width="70" height="88" /></p>
                    <p class="m_-3923079614135954232xmsonormal"><span style="font-size: 9.0pt; font-family: \'Verdana\',sans-serif;">San Martin 140, piso 8&deg;</span><u></u><u></u></p>
                    <p class="m_-3923079614135954232xmsonormal"><span style="font-size: 9.0pt; font-family: \'Verdana\',sans-serif;">Buenos Aires, Argentina</span><u></u><u></u></p>
                    <p class="m_-3923079614135954232xmsonormal"><span style="font-size: 9.0pt; font-family: \'Verdana\',sans-serif;">+54 (11) 4321 9500</span><u></u><u></u></p>
                    <p class="m_-3923079614135954232xmsonormal"><span style="font-size: 10.0pt; font-family: \'Verdana\',sans-serif;"><a href="https://urldefense.proofpoint.com/v2/url?u=http-3A__www.contreras.com.ar_&amp;d=DwMFAw&amp;c=0TzQCy9lgR5hSW-bDg5HA76y7nf4lvOzvVop5GM3Y80&amp;r=lDiBsYLvsElGPOzCWcOqOemV6CUYuuTcfC74XGi7CUo&amp;m=8sseLaql5V9k-ag5cgaMdHVb0z4cwyDOoinA8rxc6E4&amp;s=Gco3yqJEEW6RxzCiGpPaywkYCh3zf9evo5sL36znb1Y&amp;e=" target="_blank" rel="noopener" data-saferedirecturl="https://www.google.com/url?q=https://urldefense.proofpoint.com/v2/url?u%3Dhttp-3A__www.contreras.com.ar_%26d%3DDwMFAw%26c%3D0TzQCy9lgR5hSW-bDg5HA76y7nf4lvOzvVop5GM3Y80%26r%3DlDiBsYLvsElGPOzCWcOqOemV6CUYuuTcfC74XGi7CUo%26m%3D8sseLaql5V9k-ag5cgaMdHVb0z4cwyDOoinA8rxc6E4%26s%3DGco3yqJEEW6RxzCiGpPaywkYCh3zf9evo5sL36znb1Y%26e%3D&amp;source=gmail&amp;ust=1569266816868000&amp;usg=AFQjCNGmf2yOrgexecmt8N0-TW0Zb1m49g">www.contreras.com.ar</a></span><u></u><u></u></p>
                    <p><span style="font-size: 8.0pt; font-family: \'Verdana\',sans-serif; color: #828282;">Esta comunicaci&oacute;n y cualquier archivo adjunto contiene informaci&oacute;n reservada, confidencial, protegida por ley. Si no es usted el destinatario de la misma, observe que tiene terminantemente prohibido usar, difundir, distribuir o copiar esta comunicaci&oacute;n. Si ha recibido este mensaje por error deber&aacute; avisar inmediatamente de ello a su emisor por tel&eacute;fono o correo electr&oacute;nico y eliminarlo totalmente de su sistema. No se garantiza que las comunicaciones por Internet sean enviadas en el plazo correspondiente o que no contengan errores o virus. El emisor no aceptar&aacute; responsabilidad por errores u omisiones. Las opiniones vertidas son responsabilidad del autor y no son emitidas ni avaladas por Contreras ni ninguna de sus subsidiarias. <br /> <br /> No imprima este correo si no es necesario; todos tenemos un papel en el cuidado del medio ambiente. <br /> Do not print this mail if it is not necessary. <br /> <br /> This communication and any attached file contain information that is legally privileged, confidential or exempt from disclosure. If you are not the intended recipient, please note that any dissemination, distribution, or copying of this communication is strictly prohibited. Anyone who receives this message in error should notify the sender immediately by telephone or by return e-mail and delete this communication entirely from his or her computer. Contreras is neither liable for the proper and complete transmission of the information contained in this communication nor for any delay, error or virus. Any opinions presented are solely those of its author and do not necessarily represent those of Contreras or any of its subsidiary companies.</span></p>

					</div>
				   ';*/
				eval("\$sendBody3 = \"$customEmail->body\";");
				$mail->body = $sendBody3;
			}
			//$mail->AddCC($participant->consultant->email);
			
			$date = date("d/m/Y H:i:s");

			if($mail->save())
			{
				foreach($users as $user) {
					$mail->addReceiver($user->email);
				}
				$this->_sendResponse(200, CJSON::encode(array('success' => 'Se ha enviado una notificación al correo electrónico.')));
			} else
			{
				$this->_sendResponse(200, CJSON::encode(array('success' => 'Se ha producido un error inesperado')));
			}
	}
	public function actionSendMailBuyOrderDocumentStatus()
	{
		$this->_checkAuth();
		$idBuyOrderDocument = $_GET['id'];
		$buyOrderDocument = BuyOrderDocument::model()->findByPk($idBuyOrderDocument);
		$status = $buyOrderDocument->idBuyOrderDocumentStatus;
		
		$users = array();

		if ($status == 3 || $status == 4) {
			$providers = Provider::model()->findAll('idCompany = '. $buyOrderDocument->buyOrder->idCompany);
			if(count($providers) <= 0){
				$this->_sendResponse(200, CJSON::encode(array('failure' => 'No se encuentran proveedores asociados a la compañia.')));
			}
			foreach($providers as $index => $provider) {
				$users[$index] = User::model()->findByPk($provider->idUser);
			}
		} elseif($status == 1) {
			$usersWithoutFilter = User::model()->findall();
			if(count($usersWithoutFilter) <= 0){
				$this->_sendResponse(200, CJSON::encode(array('failure' => 'No se encuentran usuarios con rol de Pagos.')));
			}
			foreach ($usersWithoutFilter as $index => $user) {
				if($user->hasRole(2))
				{
					$users[$index] = $user; 
				}
			}
		}
		if (count($users) == 0) {
			$this->_sendResponse(200, CJSON::encode(array('success' => 'No se encontraron usuarios a quien enviarles el correo electrónico')));
		}

			$mail = new Email();
			//configuracion fija
			$emailParameters = EmailParameters::model()->findByPk(1);
			$customEmail = CustomEmail::model()->findByPk(10);

			if($status == 1){
				//$mail->subject ="Documento Disponible";
				$mail->subject = $customEmail->subject;
				/*$mail->body =
					'<div style="font-size: 10.0pt; font-family: \'Verdana\',sans-serif;">
					<p>Le informamos que le han remitido documentación vinculada a la Notificación Interna de Compra N° '.$buyOrderDocument->buyOrder->nic.' .</p>
					<p>Ingrese al siguiente link para previsualizar el documento: <a href="'.$buyOrderDocument->pdf.'">Ver documento </a></p>
					<p class="m_-3923079614135954232xmsonormal"><span style="font-size: 9.0pt; font-family: \'Verdana\',sans-serif;">Este mensaje fue generado automáticamente por registrarse o realizar alguna acción en nuestro sitio de proveedores, se ha contactado vía mail o por teléfono con CONTRERAS. Si usted desconoce alguna acción de las antes mencionadas, descarte este mail. El contenido es propiedad de CONTRERAS y garantiza la privacidad y seguridad de su información.</span><u></u><u></u></p>
					<p class="MsoNormal"><img id="m_-3923079614135954232x__x005f_x0000_i1025" style="width: 74px; height: 93px;" src="https://www.contreras.com.ar/img/contreras-logo.png" width="70" height="88" /></p>
                    <p class="m_-3923079614135954232xmsonormal"><span style="font-size: 9.0pt; font-family: \'Verdana\',sans-serif;">San Martin 140, piso 8&deg;</span><u></u><u></u></p>
                    <p class="m_-3923079614135954232xmsonormal"><span style="font-size: 9.0pt; font-family: \'Verdana\',sans-serif;">Buenos Aires, Argentina</span><u></u><u></u></p>
                    <p class="m_-3923079614135954232xmsonormal"><span style="font-size: 9.0pt; font-family: \'Verdana\',sans-serif;">+54 (11) 4321 9500</span><u></u><u></u></p>
                    <p class="m_-3923079614135954232xmsonormal"><span style="font-size: 10.0pt; font-family: \'Verdana\',sans-serif;"><a href="https://urldefense.proofpoint.com/v2/url?u=http-3A__www.contreras.com.ar_&amp;d=DwMFAw&amp;c=0TzQCy9lgR5hSW-bDg5HA76y7nf4lvOzvVop5GM3Y80&amp;r=lDiBsYLvsElGPOzCWcOqOemV6CUYuuTcfC74XGi7CUo&amp;m=8sseLaql5V9k-ag5cgaMdHVb0z4cwyDOoinA8rxc6E4&amp;s=Gco3yqJEEW6RxzCiGpPaywkYCh3zf9evo5sL36znb1Y&amp;e=" target="_blank" rel="noopener" data-saferedirecturl="https://www.google.com/url?q=https://urldefense.proofpoint.com/v2/url?u%3Dhttp-3A__www.contreras.com.ar_%26d%3DDwMFAw%26c%3D0TzQCy9lgR5hSW-bDg5HA76y7nf4lvOzvVop5GM3Y80%26r%3DlDiBsYLvsElGPOzCWcOqOemV6CUYuuTcfC74XGi7CUo%26m%3D8sseLaql5V9k-ag5cgaMdHVb0z4cwyDOoinA8rxc6E4%26s%3DGco3yqJEEW6RxzCiGpPaywkYCh3zf9evo5sL36znb1Y%26e%3D&amp;source=gmail&amp;ust=1569266816868000&amp;usg=AFQjCNGmf2yOrgexecmt8N0-TW0Zb1m49g">www.contreras.com.ar</a></span><u></u><u></u></p>
                    <p><span style="font-size: 8.0pt; font-family: \'Verdana\',sans-serif; color: #828282;">Esta comunicaci&oacute;n y cualquier archivo adjunto contiene informaci&oacute;n reservada, confidencial, protegida por ley. Si no es usted el destinatario de la misma, observe que tiene terminantemente prohibido usar, difundir, distribuir o copiar esta comunicaci&oacute;n. Si ha recibido este mensaje por error deber&aacute; avisar inmediatamente de ello a su emisor por tel&eacute;fono o correo electr&oacute;nico y eliminarlo totalmente de su sistema. No se garantiza que las comunicaciones por Internet sean enviadas en el plazo correspondiente o que no contengan errores o virus. El emisor no aceptar&aacute; responsabilidad por errores u omisiones. Las opiniones vertidas son responsabilidad del autor y no son emitidas ni avaladas por Contreras ni ninguna de sus subsidiarias. <br /> <br /> No imprima este correo si no es necesario; todos tenemos un papel en el cuidado del medio ambiente. <br /> Do not print this mail if it is not necessary. <br /> <br /> This communication and any attached file contain information that is legally privileged, confidential or exempt from disclosure. If you are not the intended recipient, please note that any dissemination, distribution, or copying of this communication is strictly prohibited. Anyone who receives this message in error should notify the sender immediately by telephone or by return e-mail and delete this communication entirely from his or her computer. Contreras is neither liable for the proper and complete transmission of the information contained in this communication nor for any delay, error or virus. Any opinions presented are solely those of its author and do not necessarily represent those of Contreras or any of its subsidiary companies.</span></p>   
					</div>
				   ';*/
				   eval("\$sendBody4 = \"$customEmail->body\";");
				   $mail->body = $sendBody4;
            }elseif($status == 3){
				$customEmail = CustomEmail::model()->findByPk(11);
				//$mail->subject ="Documento Devuelto";
				$mail->subject = $customEmail->subject;
                /*$mail->body =
                    '<div style="font-size: 10.0pt; font-family: \'Verdana\',sans-serif;">
					<p>Le informamos que le han devuelto documentación vinculada a la Notificación Interna de Compra N° '.$buyOrderDocument->buyOrder->nic.' .</p>
					<p class="m_-3923079614135954232xmsonormal"><span style="font-size: 9.0pt; font-family: \'Verdana\',sans-serif;">Este mensaje fue generado automáticamente por registrarse o realizar alguna acción en nuestro sitio de proveedores, se ha contactado vía mail o por teléfono con CONTRERAS. Si usted desconoce alguna acción de las antes mencionadas, descarte este mail. El contenido es propiedad de CONTRERAS y garantiza la privacidad y seguridad de su información.</span><u></u><u></u></p>
					<p class="MsoNormal"><img id="m_-3923079614135954232x__x005f_x0000_i1025" style="width: 74px; height: 93px;" src="https://www.contreras.com.ar/img/contreras-logo.png" width="70" height="88" /></p>
                    <p class="m_-3923079614135954232xmsonormal"><span style="font-size: 9.0pt; font-family: \'Verdana\',sans-serif;">San Martin 140, piso 8&deg;</span><u></u><u></u></p>
                    <p class="m_-3923079614135954232xmsonormal"><span style="font-size: 9.0pt; font-family: \'Verdana\',sans-serif;">Buenos Aires, Argentina</span><u></u><u></u></p>
                    <p class="m_-3923079614135954232xmsonormal"><span style="font-size: 9.0pt; font-family: \'Verdana\',sans-serif;">+54 (11) 4321 9500</span><u></u><u></u></p>
                    <p class="m_-3923079614135954232xmsonormal"><span style="font-size: 10.0pt; font-family: \'Verdana\',sans-serif;"><a href="https://urldefense.proofpoint.com/v2/url?u=http-3A__www.contreras.com.ar_&amp;d=DwMFAw&amp;c=0TzQCy9lgR5hSW-bDg5HA76y7nf4lvOzvVop5GM3Y80&amp;r=lDiBsYLvsElGPOzCWcOqOemV6CUYuuTcfC74XGi7CUo&amp;m=8sseLaql5V9k-ag5cgaMdHVb0z4cwyDOoinA8rxc6E4&amp;s=Gco3yqJEEW6RxzCiGpPaywkYCh3zf9evo5sL36znb1Y&amp;e=" target="_blank" rel="noopener" data-saferedirecturl="https://www.google.com/url?q=https://urldefense.proofpoint.com/v2/url?u%3Dhttp-3A__www.contreras.com.ar_%26d%3DDwMFAw%26c%3D0TzQCy9lgR5hSW-bDg5HA76y7nf4lvOzvVop5GM3Y80%26r%3DlDiBsYLvsElGPOzCWcOqOemV6CUYuuTcfC74XGi7CUo%26m%3D8sseLaql5V9k-ag5cgaMdHVb0z4cwyDOoinA8rxc6E4%26s%3DGco3yqJEEW6RxzCiGpPaywkYCh3zf9evo5sL36znb1Y%26e%3D&amp;source=gmail&amp;ust=1569266816868000&amp;usg=AFQjCNGmf2yOrgexecmt8N0-TW0Zb1m49g">www.contreras.com.ar</a></span><u></u><u></u></p>
                    <p><span style="font-size: 8.0pt; font-family: \'Verdana\',sans-serif; color: #828282;">Esta comunicaci&oacute;n y cualquier archivo adjunto contiene informaci&oacute;n reservada, confidencial, protegida por ley. Si no es usted el destinatario de la misma, observe que tiene terminantemente prohibido usar, difundir, distribuir o copiar esta comunicaci&oacute;n. Si ha recibido este mensaje por error deber&aacute; avisar inmediatamente de ello a su emisor por tel&eacute;fono o correo electr&oacute;nico y eliminarlo totalmente de su sistema. No se garantiza que las comunicaciones por Internet sean enviadas en el plazo correspondiente o que no contengan errores o virus. El emisor no aceptar&aacute; responsabilidad por errores u omisiones. Las opiniones vertidas son responsabilidad del autor y no son emitidas ni avaladas por Contreras ni ninguna de sus subsidiarias. <br /> <br /> No imprima este correo si no es necesario; todos tenemos un papel en el cuidado del medio ambiente. <br /> Do not print this mail if it is not necessary. <br /> <br /> This communication and any attached file contain information that is legally privileged, confidential or exempt from disclosure. If you are not the intended recipient, please note that any dissemination, distribution, or copying of this communication is strictly prohibited. Anyone who receives this message in error should notify the sender immediately by telephone or by return e-mail and delete this communication entirely from his or her computer. Contreras is neither liable for the proper and complete transmission of the information contained in this communication nor for any delay, error or virus. Any opinions presented are solely those of its author and do not necessarily represent those of Contreras or any of its subsidiary companies.</span></p>   
					</div>
				   ';*/
				   eval("\$sendBody5 = \"$customEmail->body\";");
				   $mail->body = $sendBody5;
			}elseif($status == 4){
				$customEmail = CustomEmail::model()->findByPk(12);
				//$mail->subject ="Orden de pago disponible";
				$mail->subject = $customEmail->subject;				
				/*$mail->body ='
					<p> '.$buyOrderDocument->paymentOrder->comment.' </p>
					<p>nota: el horario para consultas es lunes y viernes de 15 a 17hs llamando telefónicamente al 4321-9500</p>
					<p class="m_-3923079614135954232xmsonormal"><span style="font-size: 9.0pt; font-family: \'Verdana\',sans-serif;">Este mensaje fue generado automáticamente por registrarse o realizar alguna acción en nuestro sitio de proveedores, se ha contactado vía mail o por teléfono con CONTRERAS. Si usted desconoce alguna acción de las antes mencionadas, descarte este mail. El contenido es propiedad de CONTRERAS y garantiza la privacidad y seguridad de su información.</span><u></u><u></u></p>
					<p class="MsoNormal"><img id="m_-3923079614135954232x__x005f_x0000_i1025" style="width: 74px; height: 93px;" src="https://www.contreras.com.ar/img/contreras-logo.png" width="70" height="88" /></p>
                    <p class="m_-3923079614135954232xmsonormal"><span style="font-size: 9.0pt; font-family: \'Verdana\',sans-serif;">San Martin 140, piso 8&deg;</span><u></u><u></u></p>
                    <p class="m_-3923079614135954232xmsonormal"><span style="font-size: 9.0pt; font-family: \'Verdana\',sans-serif;">Buenos Aires, Argentina</span><u></u><u></u></p>
                    <p class="m_-3923079614135954232xmsonormal"><span style="font-size: 9.0pt; font-family: \'Verdana\',sans-serif;">+54 (11) 4321 9500</span><u></u><u></u></p>
                    <p class="m_-3923079614135954232xmsonormal"><span style="font-size: 10.0pt; font-family: \'Verdana\',sans-serif;"><a href="https://urldefense.proofpoint.com/v2/url?u=http-3A__www.contreras.com.ar_&amp;d=DwMFAw&amp;c=0TzQCy9lgR5hSW-bDg5HA76y7nf4lvOzvVop5GM3Y80&amp;r=lDiBsYLvsElGPOzCWcOqOemV6CUYuuTcfC74XGi7CUo&amp;m=8sseLaql5V9k-ag5cgaMdHVb0z4cwyDOoinA8rxc6E4&amp;s=Gco3yqJEEW6RxzCiGpPaywkYCh3zf9evo5sL36znb1Y&amp;e=" target="_blank" rel="noopener" data-saferedirecturl="https://www.google.com/url?q=https://urldefense.proofpoint.com/v2/url?u%3Dhttp-3A__www.contreras.com.ar_%26d%3DDwMFAw%26c%3D0TzQCy9lgR5hSW-bDg5HA76y7nf4lvOzvVop5GM3Y80%26r%3DlDiBsYLvsElGPOzCWcOqOemV6CUYuuTcfC74XGi7CUo%26m%3D8sseLaql5V9k-ag5cgaMdHVb0z4cwyDOoinA8rxc6E4%26s%3DGco3yqJEEW6RxzCiGpPaywkYCh3zf9evo5sL36znb1Y%26e%3D&amp;source=gmail&amp;ust=1569266816868000&amp;usg=AFQjCNGmf2yOrgexecmt8N0-TW0Zb1m49g">www.contreras.com.ar</a></span><u></u><u></u></p>
                    <p><span style="font-size: 8.0pt; font-family: \'Verdana\',sans-serif; color: #828282;">Esta comunicaci&oacute;n y cualquier archivo adjunto contiene informaci&oacute;n reservada, confidencial, protegida por ley. Si no es usted el destinatario de la misma, observe que tiene terminantemente prohibido usar, difundir, distribuir o copiar esta comunicaci&oacute;n. Si ha recibido este mensaje por error deber&aacute; avisar inmediatamente de ello a su emisor por tel&eacute;fono o correo electr&oacute;nico y eliminarlo totalmente de su sistema. No se garantiza que las comunicaciones por Internet sean enviadas en el plazo correspondiente o que no contengan errores o virus. El emisor no aceptar&aacute; responsabilidad por errores u omisiones. Las opiniones vertidas son responsabilidad del autor y no son emitidas ni avaladas por Contreras ni ninguna de sus subsidiarias. <br /> <br /> No imprima este correo si no es necesario; todos tenemos un papel en el cuidado del medio ambiente. <br /> Do not print this mail if it is not necessary. <br /> <br /> This communication and any attached file contain information that is legally privileged, confidential or exempt from disclosure. If you are not the intended recipient, please note that any dissemination, distribution, or copying of this communication is strictly prohibited. Anyone who receives this message in error should notify the sender immediately by telephone or by return e-mail and delete this communication entirely from his or her computer. Contreras is neither liable for the proper and complete transmission of the information contained in this communication nor for any delay, error or virus. Any opinions presented are solely those of its author and do not necessarily represent those of Contreras or any of its subsidiary companies.</span></p>

					</div>
				   ';*/
				   eval("\$sendBody6 = \"$customEmail->body\";");
				   $mail->body = $sendBody6;
			}
			//$mail->AddCC($participant->consultant->email);
			
			$date = date("d/m/Y H:i:s");

			if($mail->save())
			{
				foreach($users as $user) {
					$mail->addReceiver($user->email);
				}
				$this->_sendResponse(200, CJSON::encode(array('success' => 'Se ha enviado una notificación al correo electrónico.')));
			} else
			{
				$this->_sendResponse(200, CJSON::encode(array('success' => 'Se ha producido un error inesperado')));
			}
	}

	public function actionGetUserWithJobs()
	{
		/*$this->_checkAuth();
		if(!$this->actionCheckFront())
		{
			return;
		}*/
		$id = $_GET['id'];
		$model = User::model()->findAll('idJob ='.$id);
		$this->_sendResponse(200, CJSON::encode(array('userJobs' => $model)));
	}

	public function actionSendMailSignUp()
	{
		if($_GET['id'] > 0){
			$criteria = new CDbCriteria();
			//$criteria->addCondition('idCompany = '. $_GET['id']);
			$criteria->addCondition('idUser = '. $_GET['id']);
			$user = User::model()->findByPk($criteria);
			/*
			$users = [];
			foreach ($providers as $index => $provider) {
				$users[$index] = User::model()->findByPk($provider->idUser);
			}
			*/
		}

			$mail = new Email();
			//configuracion fija
			$emailParameters = EmailParameters::model()->findByPk(1);
			$customEmail = CustomEmail::model()->findByPk(21);
			
			$mail->subject = $customEmail->subject;
			
			
			$date = date("d/m/Y H:i:s");
			
			$mail->body = $customEmail->body;

			if($mail->save())
			{
				//foreach($users as $user) {
				$mail->addReceiver($user->email);
				//}
				$this->_sendResponse(200, CJSON::encode(array('success' => 'Se ha enviado una notificación al correo electrónico.')));
			} else
			{
				$this->_sendResponse(200, CJSON::encode(array('success' => 'Se ha producido un error inesperado')));
			}
	}

	// public function actionSendMailUserActivation()
	// {
	// 	$this->_checkAuth();
	// 	if($_GET['id'] > 0){
	// 		$criteria = new CDbCriteria();
	// 		//$criteria->addCondition('idCompany = '. $_GET['id']);
	// 		$criteria->addCondition('idCompany = '. $_GET['id']);
	// 		$criteria->addCondition('isAdmin = 1');
	// 		$provider = Provider::model()->find($criteria);
	// 		$user = User::model()->findByPk($provider->idUser);
	// 		/*
	// 		$users = [];
	// 		foreach ($providers as $index => $provider) {
	// 			$users[$index] = User::model()->findByPk($provider->idUser);
	// 		}
	// 		*/
	// 	}

	// 		$mail = new Email();
	// 		//configuracion fija
	// 		$emailParameters = EmailParameters::model()->findByPk(1);
	// 		$customEmail = CustomEmail::model()->findByPk(13);
			
	// 		$mail->subject = $customEmail->subject;
			
			
	// 		$date = date("d/m/Y H:i:s");
			
	// 		$mail->body = $customEmail->body;

	// 		if($mail->save())
	// 		{
	// 			//foreach($users as $user) {
	// 			$mail->addReceiver($user->email);
	// 			//}
	// 			$this->_sendResponse(200, CJSON::encode(array('success' => 'Se ha enviado una notificación al correo electrónico.')));
	// 		} else
	// 		{
	// 			$this->_sendResponse(200, CJSON::encode(array('success' => 'Se ha producido un error inesperado')));
	// 		}
	// }

	// public function actionSendMailUserReject()
	// {
	// 	$this->_checkAuth();
	// 	if($_GET['id'] > 0){
	// 		$criteria = new CDbCriteria();
	// 		//$criteria->addCondition('idCompany = '. $_GET['id']);
	// 		$criteria->addCondition('idCompany = '. $_GET['id']);
	// 		// $criteria->addCondition('isAdmin = 1');
	// 		$provider = Provider::model()->find($criteria);
	// 		$user = User::model()->findByPk($provider->idUser);
	// 		/*
	// 		$users = [];
	// 		foreach ($providers as $index => $provider) {
	// 			$users[$index] = User::model()->findByPk($provider->idUser);
	// 		}
	// 		*/
	// 	}

	// 		$mail = new Email();
	// 		//configuracion fija
	// 		$emailParameters = EmailParameters::model()->findByPk(1);
	// 		$customEmail = CustomEmail::model()->findByPk(21);
			
	// 		$mail->subject = $customEmail->subject;
			
			
	// 		$date = date("d/m/Y H:i:s");
			
	// 		$mail->body = $customEmail->body;

	// 		if($mail->save())
	// 		{
	// 			//foreach($users as $user) {
	// 			$mail->addReceiver($user->email);
	// 			//}
	// 			$this->_sendResponse(200, CJSON::encode(array('success' => 'Se ha enviado una notificación al correo electrónico.')));
	// 		} else
	// 		{
	// 			$this->_sendResponse(200, CJSON::encode(array('success' => 'Se ha producido un error inesperado')));
	// 		}
	// }
	public function actionSendMailProvider()
	{
		$this->_checkAuth();
		if (!isset($_GET['model'])) {
			throw new Exception("No ha sido especificado ningun  modelo");
		}
		$id = $_GET['id'];
		//Yii::$app->runAction('ProviderController/sendMail', $id);
		$model = Provider::model()->findByPk(CPropertyValue::ensureInteger($id));


		if ($model instanceof Provider) 
		{			
			try
			{
					$usuario = $model->idUser0;
					$userEx = explode('@',$usuario->email);
					$user = $userEx[0];
					$psw = User::model()->randomPsw();
					$token = User::model()->encriptar($user."<".$psw);
					
						
						//This should be done in your php.ini, but this is how to do it if you don't have access to that
						$mail = new Email();
						
					//throw new Exception("Error Processing Request", 1);
					
						//configuracion fija
						$emailParameters = EmailParameters::model()->findByPk(1);
						$customEmail = CustomEmail::model()->findByPk(13);

						//$mail->subject ="¡Bienvenido a CONTRERAS!";
						$mail->subject = $customEmail->subject;
						
						//$mail->AddCC($consultant->email);
						/*$mail->body =
						'
						<div style="font-size: 10.0pt; font-family: \'Verdana\',sans-serif;">
						<p>Hola. A partir de este momento tendrá acceso al sitio de Proveedores de CONTRERAS. Su usuario '.$usuario->name.' ha sido creado con éxito. </p>
						<p>Ingrese al siguiente link para finalizar el registro: <a href="'.$emailParameters->hostForMails.'?finSolicitud=1&t='.$token.'">Portal de Compras</a> <br/></p>
						<p class="m_-3923079614135954232xmsonormal"><span style="font-size: 9.0pt; font-family: \'Verdana\',sans-serif;">Este mensaje fue generado automáticamente por registrarse o realizar alguna acción en nuestro sitio de proveedores, se ha contactado vía mail o por teléfono con CONTRERAS. Si usted desconoce alguna acción de las antes mencionadas, descarte este mail. El contenido es propiedad de CONTRERAS y garantiza la privacidad y seguridad de su información.</span><u></u><u></u></p>
						
						<p class="MsoNormal"><img id="m_-3923079614135954232x__x005f_x0000_i1025" style="width: 74px; height: 93px;" src="https://www.contreras.com.ar/img/contreras-logo.png" width="70" height="88" /></p>
						<p class="m_-3923079614135954232xmsonormal"><span style="font-size: 9.0pt; font-family: \'Verdana\',sans-serif;">San Martin 140, piso 8&deg;</span><u></u><u></u></p>
						<p class="m_-3923079614135954232xmsonormal"><span style="font-size: 9.0pt; font-family: \'Verdana\',sans-serif;">Buenos Aires, Argentina</span><u></u><u></u></p>
						<p class="m_-3923079614135954232xmsonormal"><span style="font-size: 9.0pt; font-family: \'Verdana\',sans-serif;">+54 (11) 4321 9500</span><u></u><u></u></p>
						<p class="m_-3923079614135954232xmsonormal"><span style="font-size: 10.0pt; font-family: \'Verdana\',sans-serif;"><a href="https://urldefense.proofpoint.com/v2/url?u=http-3A__www.contreras.com.ar_&amp;d=DwMFAw&amp;c=0TzQCy9lgR5hSW-bDg5HA76y7nf4lvOzvVop5GM3Y80&amp;r=lDiBsYLvsElGPOzCWcOqOemV6CUYuuTcfC74XGi7CUo&amp;m=8sseLaql5V9k-ag5cgaMdHVb0z4cwyDOoinA8rxc6E4&amp;s=Gco3yqJEEW6RxzCiGpPaywkYCh3zf9evo5sL36znb1Y&amp;e=" target="_blank" rel="noopener" data-saferedirecturl="https://www.google.com/url?q=https://urldefense.proofpoint.com/v2/url?u%3Dhttp-3A__www.contreras.com.ar_%26d%3DDwMFAw%26c%3D0TzQCy9lgR5hSW-bDg5HA76y7nf4lvOzvVop5GM3Y80%26r%3DlDiBsYLvsElGPOzCWcOqOemV6CUYuuTcfC74XGi7CUo%26m%3D8sseLaql5V9k-ag5cgaMdHVb0z4cwyDOoinA8rxc6E4%26s%3DGco3yqJEEW6RxzCiGpPaywkYCh3zf9evo5sL36znb1Y%26e%3D&amp;source=gmail&amp;ust=1569266816868000&amp;usg=AFQjCNGmf2yOrgexecmt8N0-TW0Zb1m49g">www.contreras.com.ar</a></span><u></u><u></u></p>
                            <p><span style="font-size: 8.0pt; font-family: \'Verdana\',sans-serif; color: #828282;">Esta comunicaci&oacute;n y cualquier archivo adjunto contiene informaci&oacute;n reservada, confidencial, protegida por ley. Si no es usted el destinatario de la misma, observe que tiene terminantemente prohibido usar, difundir, distribuir o copiar esta comunicaci&oacute;n. Si ha recibido este mensaje por error deber&aacute; avisar inmediatamente de ello a su emisor por tel&eacute;fono o correo electr&oacute;nico y eliminarlo totalmente de su sistema. No se garantiza que las comunicaciones por Internet sean enviadas en el plazo correspondiente o que no contengan errores o virus. El emisor no aceptar&aacute; responsabilidad por errores u omisiones. Las opiniones vertidas son responsabilidad del autor y no son emitidas ni avaladas por Contreras ni ninguna de sus subsidiarias. <br /> <br /> No imprima este correo si no es necesario; todos tenemos un papel en el cuidado del medio ambiente. <br /> Do not print this mail if it is not necessary. <br /> <br /> This communication and any attached file contain information that is legally privileged, confidential or exempt from disclosure. If you are not the intended recipient, please note that any dissemination, distribution, or copying of this communication is strictly prohibited. Anyone who receives this message in error should notify the sender immediately by telephone or by return e-mail and delete this communication entirely from his or her computer. Contreras is neither liable for the proper and complete transmission of the information contained in this communication nor for any delay, error or virus. Any opinions presented are solely those of its author and do not necessarily represent those of Contreras or any of its subsidiary companies.</span></p>
							</div>';*/ //Fin message HTML
						eval("\$sendBody7 = \"$customEmail->body\";");
						$mail->body = $sendBody7;
							
							
							try
							{
								if($mail->save() && $usuario->updatePsw($usuario->idUser,$psw))
								{
									$mail->addReceiver($usuario->email); // Email Destinatario
									$sql="UPDATE provider SET idProviderStatus = 2 WHERE idProvider = ".$id;
        						$command = Yii::app()->db->createCommand($sql)->execute();
        						if($command)
        						{
									$this->_sendResponse(200, CJSON::encode("se ha enviado el mail correctamente."));
        						}
        						else {
									$this->_sendResponse(403, CJSON::encode("ocurrion un error inesperado"));
        						}			
							}
						}
						catch (Exception $ex)
						{
							throw new Exception($ex);
						}			
					}
			catch (Exception $e)
			{
				throw new Exception($e);
			}
			
			if (!Yii::app()->getRequest()->getIsAjaxRequest())
			{
				app()->flashMessage->addMessage('error', Yii::t('app', 'No se pudo enviar el mail'));
        							
				//$this->redirect(array('index'));
			}
		}
		else
		{
			throw new CHttpException(400, Yii::t('app', 'Your request is invalid.'));
		}
	}

	public function actionUpdateBuyOrderStatus($idBuyOrder, $statusId) {
		$this->_checkAuth();
		$buyOrder = BuyOrder::model()->findByPk($idBuyOrder);
		if($buyOrder && $buyOrder->statusId < $statusId) {
			$buyOrder->statusId = $statusId;
			if($buyOrder->save()){
				return true;
			}
		}
		return false;
	}

	public function actionUpdateStopCompanyStatus() {
		$this->_checkAuth();
		$company = Company::model()->findByPk($_GET['id']);
		$company->idCompanyStatus = 5;
		$company->save();
		$providers = Provider::model()->findAll('idCompany ='.$_GET['id']);
		foreach($providers as $provider){
			$provider->idProviderStatus = 5;
			$provider->save();
		}
		$this->_sendResponse(200, CJSON::encode("true"));
	}

	public function actionUpdateStartCompanyStatus() {
		$this->_checkAuth();
		$company = Company::model()->findByPk($_GET['id']);
		$company->idCompanyStatus = 2;
		$company->save();
		$providers = Provider::model()->findAll('idCompany ='.$_GET['id']);
		foreach($providers as $provider){
			$provider->idProviderStatus = 2;
			$provider->save();
		}
		$this->_sendResponse(200, CJSON::encode("true"));
	}

	public function actionstatusMessages()
	{
		$this->_checkAuth();
		$user = $_GET['id'];
		$mode = $_GET['mode'];
		$c = new CDbCriteria();
		if($mode == 1) { // CLAVA VISTO EL DE COMPRAS
			$c->addCondition('idEmisor = '.$user);
		} else if ($mode == 2) { // CLAVA VISTO EL PROVEEDOR
			$c->addCondition('idReceptor = '.$user);
		}
		$c->addCondition('status = 0');
		$models = Messages::model()->findAll($c);
		foreach($models as $msg) {		
			$msg->status = 1;
			$msg->save();
		}
		$this->_sendResponse(200, CJSON::encode("true"));

	}
	public function actionUpdateDocumentation()
	{
		$this->_checkAuth();
		//throw new Exception($_GET['documentation']);
		try {
			/*
			if (!isset($_GET['model'])) {
				throw new Exception("No ha sido edwwwerewpecificado ningun  modelo");
			}*/
			
			$company = $_GET['id'];
			$documentation = $_GET['documentation'];
			$model = new Company();

			
			$model->updateDocumentation($company,$documentation);
			$this->_sendResponse(200, CJSON::encode("true"));
			/*
			$model = $this->apiClassFactory->getApiModel($_GET['model']);
			//echo CJavaScript::jsonEncode(array('data' => $model->getList()));
			$model_type= get_class($model);
			$models = $model_type::model()->updateDocumentation($company,$documentation);
        	*/
		}
		catch(Exception $ex) {
			echo CJavaScript::jsonEncode(array('error' => $ex->getMessage()));
		}

		Yii::app()->end();
	}
	public function actionUpdateCalification()
	{
		$this->_checkAuth();
		try {
			if (!isset($_GET['model'])) {
				throw new Exception("No ha sido especificado ningun  modelo");
			}
			$company = $_GET['id'];
			$calification = $_GET['calification'];
			$reason = $_GET['reason'];
			$model = $this->apiClassFactory->getApiModel($_GET['model']);
			//echo CJavaScript::jsonEncode(array('data' => $model->getList()));
			$model_type= get_class($model);
			$models = $model_type::model()->updateCalification($company, $calification,$reason);
			$this->_sendResponse(200, CJSON::encode($models,true));
        	
		}
		catch(Exception $ex) {
			echo CJavaScript::jsonEncode(array('error' => $ex->getMessage()));
		}

		Yii::app()->end();
	}
	public function actionGetMessages()
	{
		$this->_checkAuth();
		try {

			$id = $_GET['id'];
			$model = User::model()->findByPk($id);
			$rows = array();
			//$rows['Enviados'] = array();
			//$rows['Recibidos'] = array();
			$c = new CDbCriteria();
			$c->addCondition('idUser = '.$id);
			$roles = UserHasRoles::model()->findAll($c);
			if(count($roles) > 0)
			{
				$providers = Provider::model()->findAll("idUser != " .$id);
				for($i = 0; $i < count($providers); $i++)
				{
					$rows[$i]['name'] = $providers[$i]->idUser0->name;
					$rows[$i]['email'] = $providers[$i]->idUser0->email;
					$rows[$i]['userStatus'] = $providers[$i]->idProviderStatus;
					$rows[$i]['idUser'] = $providers[$i]->idUser0->idUser;
					$rows[$i]['messages'] = [];
					$messages = Messages::model()->findAll("idEmisor =".$providers[$i]->idUser." or idReceptor=".$providers[$i]->idUser . ' ORDER BY status ASC, date ASC');
					for ($j = 0; $j < count($messages); $j++) {
						$rows[$i]['messages'][$j] = $messages[$j]->attributes;
						$sender = User::model()->findByPk($messages[$j]->idEmisor);
						$rows[$i]['messages'][$j]['sender'] = $sender->email;
						if ($messages[$j]->idEmisor == $providers[$i]->idUser0->idUser) {
							$rows[$i]['messages'][$j]['senderType'] = 'Proveedor';
						} else {
							$rows[$i]['messages'][$j]['senderType'] = 'Compras';
						}
					}
				}
				$this->_sendResponse(200, CJSON::encode(array('provMessages' => $rows),true));
			} else {
				$messages = Messages::model()->findAll("idEmisor =".$id." or idReceptor=".$id . ' ORDER BY date ASC');
				for ($j = 0; $j < count($messages); $j++) {
					$rows['messages'][$j] = $messages[$j]->attributes;
					$sender = User::model()->findByPk($messages[$j]->idEmisor);
					$rows['messages'][$j]['sender'] = $sender->email;
					if ($messages[$j]->idEmisor == $id) {
						$rows['messages'][$j]['senderType'] = 'Proveedor';
					} else {
						$rows['messages'][$j]['senderType'] = 'Compras';
					}
				} 
				$this->_sendResponse(200, CJSON::encode($rows,true));
			}
		}
		catch(Exception $ex) {
			echo CJavaScript::jsonEncode(array('error' => $ex->getMessage()));
		}

		Yii::app()->end();
	}
	public function actionSendMessages()
	{
		$this->_checkAuth();
		try {
			if($_SERVER['REQUEST_METHOD'] == "POST") {
				
				$post = Yii::app()->request->getRawBody();
				
				$array = json_decode($post);
				$idEmisor = $array->idEmisor;
				$idReceptor = $array->idReceptor;
				$message = $array->message;
				$messageType = $array->messageType;
				$models = new User();
				$models = $models->SendMessages($idEmisor, $idReceptor, $message, $messageType);
				$this->_sendResponse(200, CJSON::encode($models,true));
			} else {
				$this->_sendResponse(200, '');
			}
			$this->_sendResponse(200, CJSON::encode($models,true));
		}
		catch(Exception $ex) {
			echo CJavaScript::jsonEncode(array('error' => $ex->getMessage()));
		}
		Yii::app()->end();
	}

	public function actionSendSignUpMessage()
	{
		try {
			if($_SERVER['REQUEST_METHOD'] == "POST") {
				
				$post = Yii::app()->request->getRawBody();
				
				$array = json_decode($post);
				$idEmisor = $array->idEmisor;
				$idReceptor = $array->idReceptor;
				$message = $array->message;
				$messageType = $array->messageType;
				$models = new User();
				$models = $models->SendMessages($idEmisor, $idReceptor, $message, $messageType);
				$this->_sendResponse(200, CJSON::encode($models,true));
			} else {
				$this->_sendResponse(200, '');
			}
			$this->_sendResponse(200, CJSON::encode($models,true));
		}
		catch(Exception $ex) {
			echo CJavaScript::jsonEncode(array('error' => $ex->getMessage()));
		}
		Yii::app()->end();
	}

	public function actionSendMassiveMessages()
	{
		$this->_checkAuth();
		try {
			if($_SERVER['REQUEST_METHOD'] == "POST") {
				
				$post = Yii::app()->request->getRawBody();
				
				$array = json_decode($post);
				$idEmisor = $array->idEmisor;
				$idReceptor = [];
				foreach ($array->idReceptor as $index => $id) {
					$idReceptor[$index] = $id;
				}
				$message = $array->message;
				$messageType = $array->messageType;
				$models = new User();
				foreach ($idReceptor as $id) {
					$models->SendMessages($idEmisor, $id, $message, $messageType);
				}
				$this->_sendResponse(200, CJSON::encode($models,true));
			} else {
				$this->_sendResponse(200, '');
			}
			$this->_sendResponse(200, CJSON::encode($models,true));
		}
		catch(Exception $ex) {
			echo CJavaScript::jsonEncode(array('error' => $ex->getMessage()));
		}
		Yii::app()->end();
	}
	public function actionDeleteProvider()
	{
		$this->_checkAuth();
		try {
			if (!isset($_GET['model'])) {
				throw new Exception("No ha sido especificado ningun  modelo");
			}
			$id = $_GET['id'];
			
			$model = $this->apiClassFactory->getApiModel($_GET['model']);
			//echo CJavaScript::jsonEncode(array('data' => $model->getList()));
			$model_type= get_class($model);
			$models = $model_type::model()->deleteProvider($id);
			$this->_sendResponse(200, CJSON::encode($models,true));
        	
		}
		catch(Exception $ex) {
			echo CJavaScript::jsonEncode(array('error' => $ex->getMessage()));
		}

		Yii::app()->end();
	}
	
	public function actionCount()
	{
		$condition = Yii::app()->request->getQuery('condition', '1');
		switch($_GET['model'])
	    {
			case 'company':
				$count = Company::model()->count($condition);
				break;
			case 'buyOrder':
				$count = BuyOrder::model()->count($condition);
				break;
			case 'buyOrderDocument':
				$count = BuyOrderDocument::model()->with('buyOrder', 'buyOrder.company')->count($condition);
				break;
			case 'buyOrderDocumentByCompany':
				$count = BuyOrderDocument::model()->with('buyOrder')->count($condition);
				break;
		}

		$this->_sendResponse(200, CJSON::encode($count));

	}

	public function actionList()
	{
		$skipAuth = false;
		if(!$this->actionCheckFront())
		{
			return;
		}
		
		$pageSize = Yii::app()->request->getQuery('pageSize', 100000000);
		$page = Yii::app()->request->getQuery('page', 1);
		$order = Yii::app()->request->getQuery('order', 'null');
		if($order != 'null') {
			$orderProp = Yii::app()->request->getQuery('orderProp');
		}
		$condition = Yii::app()->request->getQuery('condition', '1');
		$select = Yii::app()->request->getQuery('select', '*');
	    // Get the respective model instance
	    switch($_GET['model'])
	    {
			case 'rubros':
				$models = Rubros::model()->findAll();
				$skipAuth = true;
				break;
	        case 'users':
	            $models = User::model()->findAll();
	            break;
	        case 'city':
	            $models = City::model()->findAll();
	        	break;
	        case 'invoice':
	            $models = Invoice::model()->findAll();
	        	break;
	        case 'comments':
	            $models = Comments::model()->findAll();
				break;
			case 'document':
	            $models = Document::model()->findAll("active=1");
				break;
			case 'provider':
				$models = Provider::model()->findAll();
				break;
			case 'providerDocument':
				$models = ProviderDocument::model()->findAll();
				break;
			case 'country':
				$models = Country::model()->findAll();
				$skipAuth = true;
				break;
			case 'CertificationExGain':
				$models = CertificationExGain::model()->findAll();
				$skipAuth = true;
				break;
			case 'groupCompanies':
				$models = GroupCompanies::model()->findAll();
				$skipAuth = true;
				break;
			case 'news':
				$models = News::model()->findAll();
				$skipAuth = true;
				break;
				case 'buyOrder':
					$models = BuyOrder::model()->findAll(array(
					'select' => $select,
					'limit' => $pageSize,
					'offset' => ($page-1) * $pageSize,
					'order' => $order != 'null' ? $orderProp . ' ' . $order : 'null',
					'condition' => $condition
				));
			break;
			case 'province':
				$models = Province::model()->findAll();
				$skipAuth = true;
			break;
			case 'bank':
				$models = Bank::model()->findAll();
				$skipAuth = true;
				break;
			case 'buyOrderDocument':
				$models = BuyOrderDocument::model()->with('buyOrder')->findAll();
				/*$models = BuyOrderDocument::model()->findAll(array(
					'select' => $select,
					'limit' => $pageSize,
					'offset' => ($page-1) * $pageSize,
					'order' => $order != 'null' ? $orderProp . ' ' . $order : 'null',
					'condition' => $condition
				));*/
				break;
			case 'buyOrderDocumentStatus':
				$models = BuyOrderDocumentStatus::model()->findAll();
				break;
			case 'bankBranch':
				$models = BankBranch::model()->findAll();
				$skipAuth = true;
				break;
			case 'bankAccountType':
				$models = BankAccountType::model()->findAll();
				$skipAuth = true;
				break;
			case 'agreementRegimeType':
				$models = AgreementRegimeType::model()->findAll();
				$skipAuth = true;
				break;
			case 'company':
				
				$models = Company::model()->findAll(array(
					'select' => $select,
					'limit' => $pageSize,
					'offset' => ($page-1) * $pageSize,
					'order' => $order != 'null' ? $orderProp . ' ' . $order : 'null',
					'condition' => $condition
				));
				break;
			case 'retentionRegime':
				$models = RetentionRegime::model()->findAll("gain=1");
				$skipAuth = true;
				break;
			case 'retentionRegimeVAT':
					$models = RetentionRegime::model()->findAll("vat=1");
					$skipAuth = true;
					break;	
			case 'customEmail':
				$models = CustomEmail::model()->findAll();
				$skipAuth = true;
				break;
			case 'categoryRetain':
				$models = CategoryRetain::model()->findAll("gain=1");
				$skipAuth = true;
				break;
			case 'categoryRetainVAT':
					$models = CategoryRetain::model()->findAll("vat=1");
					$skipAuth = true;
					break;
			case 'sussCategoryRetain':
				$models = CategoryRetain::model()->findAll("suss=1");
				$skipAuth = true;
				break;
			case 'ibCategoryRetain':
				$models = CategoryRetain::model()->findAll("ib=1");
				$skipAuth = true;
				break;
			case 'jurisdictions':
				//$models = Jurisdictions::model()->findAll("idJurisdictions=1");
				// $models = Jurisdictions::model()->findAllByAttributes(array('idJurisdictions'=>array('1','2','4','22','13','20','25','15')));
				$models = Jurisdictions::model()->findAll();
				$skipAuth = true;
				break;			
		}

		if(!$skipAuth) {
			$this->_checkAuth();
			// MODELOS CON AUTORIZACION
			switch($_GET['model'])
			{
				case 'companyDataStatus':
					$models = CompanyDataStatus::model()->findAll();
					break;
				case 'companyStage':
					$models = CompanyStage::model()->findAll();
					break;
				case 'users':
					$models = User::model()->findAll();
					break;
				case 'city':
					$models = City::model()->findAll();
					break;
				case 'invoice':
					$models = Invoice::model()->findAll();
					break;
				case 'comments':
					$models = Comments::model()->findAll();
					break;
				case 'document':
					$models = Document::model()->findAll("active=1");
					break;
				case 'provider':
					$models = Provider::model()->findAll();
					break;
				case 'providerDocument':
					$models = ProviderDocument::model()->findAll();
					break;
				case 'providerDocumentStatus':
					$models = ProviderDocumentStatus::model()->findAll();
					break;
				case 'statusProvider':
					$models = ProviderStatus::model()->findAll();
					break;
				case 'buyOrder':
					$models = BuyOrder::model()->findAll();
					break;
				case 'buyOrderStatus':
					$models = BuyOrderStatus::model()->findAll('statusId NOT IN (1,7) ORDER BY orden');
					break;
				case 'buyOrderDocumentStatus':
					$models = BuyOrderDocumentStatus::model()->findAll();
					break;
				case 'buyOrderDocumentType':
					$models = BuyOrderDocumentType::model()->findAll();
					break;
				case 'buyOrderDocumentTypeCode':
					$models = BuyOrderDocumentTypeCode::model()->findAll();
					break;	
				case 'manual':
					$models = Manual::model()->findAll();
					break;
				case 'company':
					$condition = Yii::app()->request->getQuery('condition', '1');
					if(isset($_GET['idUser'])) {
						$c = new CDbCriteria();
						$c->addCondition('idUser = '.$_GET['idUser']);
						$roles = UserHasRoles::model()->findAll($c);
						foreach ($roles as $index => $role) {
							if($role->rolesId == 23){
								$signUpStatus = 0;
							}
							if($role->rolesId == 24){
								$signUpStatus = 1;
							}
							if($role->rolesId == 25){
								$signUpStatus = 2;
							}
						}
					}
					if(isset($signUpStatus)){
						$condition .= ' AND signUpStatus = '.$signUpStatus.' OR idCompanyStatus = 2';
					}
					$models = Company::model()->findAll(array(
						'limit' => $pageSize,
						'offset' => ($page-1) * $pageSize,
						'order' => $order != 'null' ? $orderProp . ' ' . $order : 'null',
						'condition' => $condition
					));
					break;
				case 'licitaciones':
					$models = Licitaciones::model()->findAll();
					break;
				case 'licitacionesStatus':
					$models = LicitacionesStatus::model()->findAll();
					break;
				case 'frontendModules':
					$models = FrontendModules::model()->findAll();
					break;
				case 'rolesHasModules':
					$models = RolesHasModules::model()->findAll();
					break;
				case 'clients':
					$models = '';
					$criteria = new CDbCriteria();
					$criteria->addCondition('t.idCompany IN (SELECT DISTINCT idCompanyClient FROM invoice WHERE 1)');
					$criteria->order = 'company ASC';
					$models = Company::model()->findAll($criteria);
					break;
				
				default:
					// Model not implemented error
					$this->_sendResponse(501, sprintf(
						'Error: Mode <b>list</b> is not implemented for model <b>%s</b>',
						$_GET['model']) );
					Yii::app()->end();
			}
		}
	    // Did we get some results?
	    if(empty($models)) {
	        // No
	        $this->_sendResponse(200, CJSON::encode([]));
	                //sprintf('No items where found for model <b>%s</b>', $_GET['model']) );
	    } else {
	        // Prepare response
	        $rows = array();
	        $roles = '';
	        if(isset($_GET['idUser'])) {
	        	$c = new CDbCriteria();
	    	    $c->addCondition('idUser = '.$_GET['idUser']);
				
				$roles = UserHasRoles::model()->findAll($c);
	        }
	        
			if(count($roles) > 0 && $_GET['model'] == "providers" ) {
				$models = Company::model()->findAll();		
			}

			if(is_array($roles) && count($roles) > 0 && $_GET['model'] == "buyOrderStatus")
			{
				$models = BuyOrderStatus::model()->findAll(array("order" => "orden ASC"));
			}
			
	        foreach($models as $index => $model)
	        {
				$rows[$index] = $model->attributes;
				// SI NO SE FILTRó por UNOS CAMPOS EN ESPECIFICO
				if ($select == '*') {
					if($_GET['model'] == "provider") {
						$company = Company::model()->findByPk($rows[$index]['idCompany']);
						$rows[$index]['companyStatus'] = $company->idCompanyStatus;
						$rows[$index]['status'] = $model->idProviderStatus0->description;
						$rows[$index]['company'] = $model->idCompany0->companyName;
						$rows[$index]['cuit'] = $model->idCompany0->cuit;
						$rows[$index]['user'] = $model->idUser0->email;
						$rows[$index]['name'] = $model->idUser0->name;
						if (isset($model->idComments0))
							$rows[$index]['comment'] = $model->idComments0->description;
						$rows[$index]['active'] = $model->idUser0->password != null;
					}
					if ($_GET['model'] == "manual") {
						$rows[$index]['path'] =	Configuration::model()->find('attribute = "URL-UPL"')->value . $rows[$index]['path']; 
					}
					if($_GET['model'] == "providers" || $_GET['model'] == 'clients')
					{
						$rows[$index]['company'] = $model->getCompanyName();
					}
	
					if ($_GET['model'] == "providerDocument") {
						$rows[$index]['status'] = $model->status->description;
						$rows[$index]['document'] = $model->document->documentName;
					}
	
					if ($_GET['model'] == "company") {
						$rows[$index]['status'] = $model->status->description;
						$rows[$index]['documents'] = Company::getCompanyDocument($model->idCompany);
						$rubrosId = Rubros_company::model()->findAll('idCompany = ' . $model->idCompany);
						foreach($rubrosId as $key => $value) {
							$rows[$index]['rubros'][$key] = Rubros::model()->findByPk($value->idRubro);
						}
					}
				}
				// traer aunque se hayan pedido campos en especifico
				if ($_GET['model'] == "company") {
					$rows[$index]['status'] = $model->status->description;
					//throw new Exception(count($models), 1);
					$rows[$index]['companyDataStatus'] = $model->dataStatus->description;
					$rows[$index]['companyStage'] = $model->dataStage->stage;
					$rows[$index]['documents'] = Company::getCompanyDocument($model->idCompany);
					$rows[$index]['logCompanyGeneralData'] = LogCompanyGeneralData::model()->find('idGeneralData = ' . $model->idGeneralData);
					$rows[$index]['logCompanyPaymentData'] = LogCompanyPaymentData::model()->find('idPaymentData = ' . $model->idPaymentData);
					$rows[$index]['logCompanyTaxData'] = LogCompanyTaxData::model()->find('idTaxData = ' . $model->idTaxData);
					// $rows[$index]['LogCompanyJurisdictionData'] = LogCompanyJurisdictionData::model()->find('idCompanyJurisdictionData = ' . $model->idCompanyJurisdictionData);
					$rows[$index]['certifExgain'] = CertificationExGain::model()->findAll('cuit = ' . $model->cuit);
					$rows[$index]['certifSuss'] = Certification::model()->findAll('cuit = ' . $model->cuit);
					$rows[$index]['certifRg18'] = CertificationRG18::model()->findAll('cuit = ' . $model->cuit);
					$rows[$index]['certifRg17'] = CertificationRG17::model()->findAll('cuit = ' . $model->cuit);
					$rows[$index]['certifRg2681'] = CertificationRG2681::model()->findAll('cuit = ' . $model->cuit);
					if (isset($model->generalData)) {
						$rows[$index]['generalData'] = $model->generalData;
					}
					if (isset($model->paymentData)) {
						$rows[$index]['paymentData'] = $model->paymentData;
					}
					if (isset($model->taxData->attributes)) {
						$rows[$index]['taxData'] = $model->taxData->attributes;
					}
					if (isset($model->taxData->companyPdfExemptions)) {	
						$rows[$index]['taxData']['pdfsExempt'] = $model->taxData->companyPdfExemptions;
					}		
					if (isset($model->taxData->companyJurisdictionDatas)) {	
						$rows[$index]['taxData']['jurisdictions'] = $model->taxData->companyJurisdictionDatas;
					}	
					$rows[$index]['rubros'] = [];
					$rubrosId = Rubros_company::model()->findAll('idCompany = ' . $model->idCompany);
					foreach($rubrosId as $key => $value) {
						$rows[$index]['rubros'][$key] = Rubros::model()->findByPk($value->idRubro);
					}
				}
	        }
	    	
	        // Send the response
	        $this->_sendResponse(200, CJSON::encode($rows));
	    }
	}

	public function actionLogin() {
		if(!$this->actionCheckFront())
		{
			return;
		}
 

		$post = Yii::app()->request->getRawBody();
		$array = json_decode($post);
		$model = new LoginForm();
		$model->username = $array->username;
		$model->password = $array->password;
		//$model->rememberMe = true;

		if ($model->validate())
		{
			$token = $model->login(true);

			if (isset($token)) {
				/* @var $user WebUser */
				$user = user();
				$user->updateLastLoginAt();

				$c = new CDbCriteria();
				$c->addCondition('t.email= "'. $model->username .'"');
				$userM = User::model()->findAll($c); //datos del usuario registrado

				$c = new CDbCriteria();
				$c->addCondition('idUser = '.$userM[0]->idUser);
				// 
				$roles = UserHasRoles::model()->findAll($c);
				if(count($roles) > 0)
				{
					$ignoreProviderLogin = true;
				}else{
					$ignoreProviderLogin = false;
					$user = User::model()->find('email = "'. $model->username .'"');
					$provider = Provider::model()->find('idUser = "'. $user->idUser .'"');
					if($provider->idProviderStatus == 2){
						$user->loginAttempt = 0;
		
						//throw new Exception($user->loginAttempt);
						$user->save();
					}
				}

				$sql = "SELECT `idProviderStatus`, `idProvider`, `isAdmin`, provider.`idCompany` FROM `provider`  INNER JOIN user on user.idUser = provider.idUser where user.status = 0 AND  user.`idUser` = '".$userM[0]->idUser."'";
				$status = Yii::app()->db->createCommand($sql)->queryAll();
				if($ignoreProviderLogin || count($status) == 1)
				{
					if($ignoreProviderLogin || $status[0]{'idProviderStatus'} == 2 )
					{
						$levelLog = 1;
						$source = 'ApiController->actionLogin';
						$commentaryLog = 'Se ha logueado el usuario ';
						Logs::model()->log($levelLog,$commentaryLog,$source,$userM[0]->idUser);
						$row = $userM[0]->attributes;
						$row['roles'] = UserHasRoles::model()->findAll('idUser = '.$userM[0]->idUser);
						$row['job'] = Job::model()->findByPk($userM[0]['idJob']);
						$row['isProvider'] = count($roles) === 0;
						if($row['isProvider']) {
							$row['idProvider'] = $status[0]['idProvider'];
							$row['isAdmin'] = $status[0]['isAdmin'];
							$row['company'] = Company::model()->findByPk($status[0]['idCompany']);
							$criteria = new CDbCriteria();
							$criteria->addCondition('idCompany = '.$status[0]['idCompany']);
							$criteria->addCondition('idProviderDocumentStatus = 2');
							$documentation = ProviderDocument::model()->findAll($criteria);
							foreach ($documentation as $key => $document) {
								$row['documentation'][$key] = Document::model()->findByPk($document->idDocument);
								$row['document'][$key] = $document;
							}
						
						}


						unset($row['password']);
						unset($row['passwordStrategy']);
						unset($row['salt']);
						$this->_sendResponse(200, CJSON::encode(array('token' => $token, 'user' => $row, 'reqPsw' => $userM[0]->requiresNewPassword)));
					}
					else {
						$this->_sendResponse(200, CJSON::encode(array('error' => 'Usuario no activado')));
					}
				}
				else {
					if (count($status) > 1) {
						$this->_sendResponse(200, CJSON::encode(array('error' => 'Multiples contactos registrados con este usuario')));	
					}
					$this->_sendResponse(200, CJSON::encode(array('error' => 'No se encuentra un proveedor asociado a este usuario')));
				}
				
			}
			else {
				$this->_sendResponse(200, CJSON::encode(array('error' => 'Invalid user and password')));
			}

		}
		else {					

			$user = User::model()->find('email = "'. $model->username .'"');
			$user->loginAttempt = $user->loginAttempt + 1;
			$provider = Provider::model()->find('idUser = "'. $user->idUser .'"');
			//var_dump($user->loginAttempt);
			if($user->loginAttempt >= 3){
				$provider = Provider::model()->find('idUser = "'. $user->idUser .'"');
				$provider->idProviderStatus = 5;
				$provider->save();
			}
			$user->save();
			if($user->loginAttempt >= 3 && $provider->idProviderStatus = 5){
				$this->_sendResponse(200, CJSON::encode(array('error' => 'Usuario bloqueado')));
			}

			$this->_sendResponse(200, CJSON::encode(array('error' => 'Usuario y/o Contraseña incorrecta. Vuelve a intentarlo o haz clic en "¿Olvidaste la contraseña?" para restablecerla.')));
		}
	}

	public function actionDocumentRecord()
	{
		$this->_checkAuth();
		try {
			$documentStatus = $_GET['idProviderDocumentStatus'];
		
			$Document = Document::model()->findAll('idProviderDocumentStatus = '.$documentStatus);

			$this->_sendResponse(200, CJSON::encode($Document,true));
			
		}
		catch(Exception $ex) {
			echo CJavaScript::jsonEncode(array('error' => $ex->getMessage()));
		}

		Yii::app()->end();
	}
	
	public function actionGetComments()
	{
		$this->_checkAuth();
		if(!$this->actionCheckFront())
		{
			return;
		}
 

		$idUser = $_GET['idUser'];
		$model = User::model()->findByPk($idUser);
		$comments = $model->getComments();
		$answers = $model->getCommentsAnswers();
		$this->_sendResponse(200, CJSON::encode(array('comments' => $comments, 'answers' => $answers)));
	}

	public function actionReadComment()
	{
		$this->_checkAuth();
		if(!$this->actionCheckFront())
		{
			return;
		}
 

		$idComments = $_GET['idComments'];
		$model = Comments::model()->findByPk($idComments);
		if(!($model->idCommentType == 3 && $model->idRefenceTo == ''))
		{
			$model->statusComments = 3;
			$model->save();
		}
		$this->_sendResponse(200, CJSON::encode(array('response' => 'ok')));
	}

	public function actionGetStatus()
	{
		$this->_checkAuth();
		if(!$this->actionCheckFront())
		{
			return;
		}
 

		$model = Invoicestatus::model()->findAll();
		$this->_sendResponse(200, CJSON::encode(array('status' => $model)));
	}
	
	public function actionAllCertification(){
		$cuit = $_GET['id'];
		$condition = new CDbCriteria();	
		$condition->addCondition('cuit = '. $cuit);

		$certif = CertificationExGain::model()->findAll($condition);
		$certif1 = Certification::model()->findAll($condition);
		$certif2 = CertificationRG18::model()->findAll($condition);
		$certif3 = CertificationRG17::model()->findAll($condition);
		$certif4 = CertificationRG2681::model()->findAll($condition);

		$this->_sendResponse(200, CJSON::encode(array(
		'exgainCertification' => $certif,
		'sussCertification' => $certif1,
		'rg18Certification' => $certif2,
		'rg17Certification' => $certif3,
		'rg2681Certification' => $certif4)));
	}

	public function actionChangePsw()
	{
		// $this->_checkAuth();
		if(!$this->actionCheckFront())
		{
			return;
		}
		$post = Yii::app()->request->getRawBody();
		$array = json_decode($post);
		if(isset($array->token)){
			$decrypted = User::model()->desencriptar($array->token);
			$datosUsuario = explode('<', $decrypted);
			$model = User::model()->find('email like "'.$datosUsuario[0].'%"');
			$actualPsw = $model->password;	
		}else{
			$idUser = $array->idUser;
			$model = User::model()->findByPk($idUser);
			$actualPsw = $array->actualPsw;
		}
		$newPsw = $array->newPsw;
		$newPswRe = $array->newPswRe;
			if(isset($array->token) || password_verify($actualPsw,$model->password))
			{	
				if($newPsw == $newPswRe)
				{		
					if(User::model()->updatePsw($model->idUser,$newPsw))
					{
						$levelLog = 1;
						$source = 'ApiController->actionChangePsw';
						$commentaryLog = 'Se ha cambiado la contraseña del usuario ';
						//Logs::model()->log($levelLog,$commentaryLog,$source,$idUser);
						$this->_sendResponse(200, CJSON::encode(array('success' => 'La clave fue actualizada')));
					}
					else
					{
						$this->_sendResponse(200, CJSON::encode(array('error' => 'No se pudo actualizar la contraseña')));
					}
				}
				else {
					$this->_sendResponse(200, CJSON::encode(array('error' => 'Las contraseñas no coinciden')));
				}
			}
			else {
				$this->_sendResponse(200, CJSON::encode(array('error' => 'Contraseña actual incorrecta')));
			}
		Yii::app()->end();
	}
	
	public function actionEnviarMailMensajeNuevo()
	{
		$this->_checkAuth();
		if ($_GET['id'] > 0) {
			$user = User::model()->findByPk($_GET['id']);
		}
		else {
			$users = User::model()->findall();
			foreach ($users as $key => $user) {
				if($user->hasRole(22))
				{
					$adminMails[$key] = $user->email; 
				}
			}
		}
			//This should be done in your php.ini, but this is how to do it if you don't have access to that
			$mail = new Email();
			//configuracion fija
			$emailParameters = EmailParameters::model()->findByPk(1);
			$customEmail = CustomEmail::model()->findByPk(14);

			//$mail->subject ="Contreras - Nuevo Mensaje";
			$mail->subject = $customEmail->subject;
			//$mail->AddCC($participant->consultant->email);
			$date = date("d/m/Y H:i:s");
			/*$mail->body =
				'<div style="font-size: 10.0pt; font-family: \'Verdana\',sans-serif;">
					<p>Estimado Proveedor. Ha recibido un nuevo  mensaje de contreras para ver la información ingrese al portal de compras.</p>
					<p class="m_-3923079614135954232xmsonormal"><span style="font-size: 9.0pt; font-family: \'Verdana\',sans-serif;">Este mensaje fue generado automáticamente por registrarse o realizar alguna acción en nuestro sitio de proveedores, se ha contactado vía mail o por teléfono con CONTRERAS. Si usted desconoce alguna acción de las antes mencionadas, descarte este mail. El contenido es propiedad de CONTRERAS y garantiza la privacidad y seguridad de su información.</span><u></u><u></u></p>
					<p class="MsoNormal"><img id="m_-3923079614135954232x__x005f_x0000_i1025" style="width: 74px; height: 93px;" src="https://www.contreras.com.ar/img/contreras-logo.png" width="70" height="88" /></p>
                    <p class="m_-3923079614135954232xmsonormal"><span style="font-size: 9.0pt; font-family: \'Verdana\',sans-serif;">San Martin 140, piso 8&deg;</span><u></u><u></u></p>
                    <p class="m_-3923079614135954232xmsonormal"><span style="font-size: 9.0pt; font-family: \'Verdana\',sans-serif;">Buenos Aires, Argentina</span><u></u><u></u></p>
                    <p class="m_-3923079614135954232xmsonormal"><span style="font-size: 9.0pt; font-family: \'Verdana\',sans-serif;">+54 (11) 4321 9500</span><u></u><u></u></p>
                    <p class="m_-3923079614135954232xmsonormal"><span style="font-size: 10.0pt; font-family: \'Verdana\',sans-serif;"><a href="https://urldefense.proofpoint.com/v2/url?u=http-3A__www.contreras.com.ar_&amp;d=DwMFAw&amp;c=0TzQCy9lgR5hSW-bDg5HA76y7nf4lvOzvVop5GM3Y80&amp;r=lDiBsYLvsElGPOzCWcOqOemV6CUYuuTcfC74XGi7CUo&amp;m=8sseLaql5V9k-ag5cgaMdHVb0z4cwyDOoinA8rxc6E4&amp;s=Gco3yqJEEW6RxzCiGpPaywkYCh3zf9evo5sL36znb1Y&amp;e=" target="_blank" rel="noopener" data-saferedirecturl="https://www.google.com/url?q=https://urldefense.proofpoint.com/v2/url?u%3Dhttp-3A__www.contreras.com.ar_%26d%3DDwMFAw%26c%3D0TzQCy9lgR5hSW-bDg5HA76y7nf4lvOzvVop5GM3Y80%26r%3DlDiBsYLvsElGPOzCWcOqOemV6CUYuuTcfC74XGi7CUo%26m%3D8sseLaql5V9k-ag5cgaMdHVb0z4cwyDOoinA8rxc6E4%26s%3DGco3yqJEEW6RxzCiGpPaywkYCh3zf9evo5sL36znb1Y%26e%3D&amp;source=gmail&amp;ust=1569266816868000&amp;usg=AFQjCNGmf2yOrgexecmt8N0-TW0Zb1m49g">www.contreras.com.ar</a></span><u></u><u></u></p>
                    <p><span style="font-size: 8.0pt; font-family: \'Verdana\',sans-serif; color: #828282;">Esta comunicaci&oacute;n y cualquier archivo adjunto contiene informaci&oacute;n reservada, confidencial, protegida por ley. Si no es usted el destinatario de la misma, observe que tiene terminantemente prohibido usar, difundir, distribuir o copiar esta comunicaci&oacute;n. Si ha recibido este mensaje por error deber&aacute; avisar inmediatamente de ello a su emisor por tel&eacute;fono o correo electr&oacute;nico y eliminarlo totalmente de su sistema. No se garantiza que las comunicaciones por Internet sean enviadas en el plazo correspondiente o que no contengan errores o virus. El emisor no aceptar&aacute; responsabilidad por errores u omisiones. Las opiniones vertidas son responsabilidad del autor y no son emitidas ni avaladas por Contreras ni ninguna de sus subsidiarias. <br /> <br /> No imprima este correo si no es necesario; todos tenemos un papel en el cuidado del medio ambiente. <br /> Do not print this mail if it is not necessary. <br /> <br /> This communication and any attached file contain information that is legally privileged, confidential or exempt from disclosure. If you are not the intended recipient, please note that any dissemination, distribution, or copying of this communication is strictly prohibited. Anyone who receives this message in error should notify the sender immediately by telephone or by return e-mail and delete this communication entirely from his or her computer. Contreras is neither liable for the proper and complete transmission of the information contained in this communication nor for any delay, error or virus. Any opinions presented are solely those of its author and do not necessarily represent those of Contreras or any of its subsidiary companies.</span></p>   
				</div>'							
			; *///Fin message HTML
			$mail->body = $customEmail->body;

			if($mail->save())
			{
				if ($_GET['id'] > 0)
					$mail->addReceiver($user->email); // Email Destinatario
				else {
					foreach($adminMails as $admin) {
						$mail->addReceiver($admin);
					}
				}	
				$this->_sendResponse(200, CJSON::encode(array('success' => 'Se ha enviado una notificación al correo electrónico.')));
			} else
			{
				$this->_sendResponse(200, CJSON::encode(array('success' => $admin )));
			}
	}

	public function actionSendMailRejectedDocument()
	{
		$this->_checkAuth();
		if($_GET['id'] > 0){
			$criteria = new CDbCriteria();
			$criteria->addCondition('idCompany = '. $_GET['id']);
			$users = User::model()->findAll($criteria);
		}

			$mail = new Email();
			//configuracion fija
			$emailParameters = EmailParameters::model()->findByPk(1);
			$customEmail = CustomEmail::model()->findByPk(15);
			//$mail->subject ="Documento Rechazado";
			$mail->subject = $customEmail->subject;
			//$mail->AddCC($participant->consultant->email);
			
			$date = date("d/m/Y H:i:s");
			/*$mail->body =
				'<div style="text-align: justify; font-size: 16px;">

                <p>Se ha rechazado un documento</p>

				<p>Saludos</p>
				
					
				</div>'							
			;*/ //Fin message HTML
			$mail->body = $customEmail->body;

			if($mail->save())
			{
				foreach($users as $user) {
					$mail->addReceiver($user->email);
				}
				$this->_sendResponse(200, CJSON::encode(array('success' => 'Se ha enviado una notificación al correo electrónico.')));
			} else
			{
				$this->_sendResponse(200, CJSON::encode(array('success' => 'Se ha producido un error inesperado')));
			}
	}

	public function actionSendMailExtendedDateCompulsa()
	{
		$this->_checkAuth();
		$criteria = new CDbCriteria();
		$criteria->addCondition('idProvider = '. $_GET['id']);
		$provider = Provider::model()->find($criteria);
		$user = User::model()->find( 'idUser ='. $provider->idUser );

		$mail = new Email();
		$emailParameters = EmailParameters::model()->findByPk(1);
		$customEmail = CustomEmail::model()->findByPk(20);
		$mail->subject = $customEmail->subject;
		//$mail->AddCC($participant->consultant->email);
		
		$date = date("d/m/Y H:i:s");
		$mail->body = $customEmail->body;

		if($mail->save())
		{
			$mail->addReceiver($user->email);
			$this->_sendResponse(200, CJSON::encode(array('success' => 'Se ha enviado una notificación al correo electrónico.' . $user->email)));
		} else
		{
			$this->_sendResponse(200, CJSON::encode(array('success' => 'Se ha producido un error inesperado')));
		}
	}

	/*
	public function actionGetLastProviderDocument()
	{
		$documents=ProviderDocument::model()->find(
			array(
				'condition'=>'idDocument="'. $_GET['document'] .'" AND idProviderDocumentStatus="2" AND idCompany="' . $_GET['id'] . '"',
				'order'=>'idProviderDocument DESC'
			));
		$this->_sendResponse(200, CJSON::encode($documents));
	}
	*/

	public function actionSendMailAcceptedDocument()
	{
		$this->_checkAuth();
		if($_GET['id'] > 0){
			$criteria = new CDbCriteria();
			$criteria->addCondition('idCompany = '. $_GET['id']);
			$users = User::model()->findAll($criteria);
		}

			//This should be done in your php.ini, but this is how to do it if you don't have access to that
			$mail = new Email();
			//configuracion fija
			$emailParameters = EmailParameters::model()->findByPk(1);
			$customEmail = CustomEmail::model()->findByPk(16);
			//$mail->subject ="Documento Aceptado";
			$mail->subject = $customEmail->subject;
			
			//$mail->AddCC($participant->consultant->email);
			$date = date("d/m/Y H:i:s");
			/*$mail->body =
				'<div style="text-align: justify; font-size: 16px;">

                <p>Se ha aceptado un documento</p>

				<p>Saludos</p>
				
					
				</div>'							
			;*/ //Fin message HTML
			$mail->body = $customEmail->body;

			if($mail->save())
			{
				foreach($users as $user) {
					$mail->addReceiver($user->email);
				}
				$this->_sendResponse(200, CJSON::encode(array('success' => 'Se ha enviado una notificación al correo electrónico.')));
			} else
			{
				$this->_sendResponse(200, CJSON::encode(array('success' => 'Se ha producido un error inesperado')));
			}
	}

	public function actionReestablecerPsw()
	{
		if(!$this->actionCheckFront())
		{
			return;
		}
 

		$psw = User::model()->reestablecerPsw($_GET['email']);
		$decrypted = User::model()->desencriptar($_GET['email']);
        $explod = explode('<',$decrypted);
        $email = $explod[0];

		if($psw != "false" && $psw != "vencio") 
		{

			$mail = new Email();
			//configuracion fija
			$emailParameters = EmailParameters::model()->findByPk(1);
			$customEmail = CustomEmail::model()->findByPk(17);

			//$mail->subject ="Recordatorio de clave";
			$mail->subject = $customEmail->subject;
			//$mail->AddCC($participant->consultant->email);
			$date = date("d/m/Y H:i:s");
			/*$mail->body =
				'<div style="font-size: 10.0pt; font-family: \'Verdana\',sans-serif;">	
				<p>Estimado, conforme a su petición, le informamos que su nueva contraseña es <strong>'.$psw.'</strong></p> 
				<p class="m_-3923079614135954232xmsonormal"><span style="font-size: 9.0pt; font-family: \'Verdana\',sans-serif;">Este mensaje fue generado automáticamente por registrarse o realizar alguna acción en nuestro sitio de proveedores, se ha contactado vía mail o por teléfono con CONTRERAS. Si usted desconoce alguna acción de las antes mencionadas, descarte este mail. El contenido es propiedad de CONTRERAS y garantiza la privacidad y seguridad de su información.</span><u></u><u></u></p>
				<p class="MsoNormal"><img id="m_-3923079614135954232x__x005f_x0000_i1025" style="width: 74px; height: 93px;" src="https://www.contreras.com.ar/img/contreras-logo.png" width="70" height="88" /></p>
				<p class="m_-3923079614135954232xmsonormal"><span style="font-size: 9.0pt; font-family: \'Verdana\',sans-serif;">San Martin 140, piso 8&deg;</span><u></u><u></u></p>
				<p class="m_-3923079614135954232xmsonormal"><span style="font-size: 9.0pt; font-family: \'Verdana\',sans-serif;">Buenos Aires, Argentina</span><u></u><u></u></p>
				<p class="m_-3923079614135954232xmsonormal"><span style="font-size: 9.0pt; font-family: \'Verdana\',sans-serif;">+54 (11) 4321 9500</span><u></u><u></u></p>
				<p class="m_-3923079614135954232xmsonormal"><span style="font-size: 10.0pt; font-family: \'Verdana\',sans-serif;"><a href="https://urldefense.proofpoint.com/v2/url?u=http-3A__www.contreras.com.ar_&amp;d=DwMFAw&amp;c=0TzQCy9lgR5hSW-bDg5HA76y7nf4lvOzvVop5GM3Y80&amp;r=lDiBsYLvsElGPOzCWcOqOemV6CUYuuTcfC74XGi7CUo&amp;m=8sseLaql5V9k-ag5cgaMdHVb0z4cwyDOoinA8rxc6E4&amp;s=Gco3yqJEEW6RxzCiGpPaywkYCh3zf9evo5sL36znb1Y&amp;e=" target="_blank" rel="noopener" data-saferedirecturl="https://www.google.com/url?q=https://urldefense.proofpoint.com/v2/url?u%3Dhttp-3A__www.contreras.com.ar_%26d%3DDwMFAw%26c%3D0TzQCy9lgR5hSW-bDg5HA76y7nf4lvOzvVop5GM3Y80%26r%3DlDiBsYLvsElGPOzCWcOqOemV6CUYuuTcfC74XGi7CUo%26m%3D8sseLaql5V9k-ag5cgaMdHVb0z4cwyDOoinA8rxc6E4%26s%3DGco3yqJEEW6RxzCiGpPaywkYCh3zf9evo5sL36znb1Y%26e%3D&amp;source=gmail&amp;ust=1569266816868000&amp;usg=AFQjCNGmf2yOrgexecmt8N0-TW0Zb1m49g">www.contreras.com.ar</a></span><u></u><u></u></p>
				<p><span style="font-size: 8.0pt; font-family: \'Verdana\',sans-serif; color: #828282;">Esta comunicaci&oacute;n y cualquier archivo adjunto contiene informaci&oacute;n reservada, confidencial, protegida por ley. Si no es usted el destinatario de la misma, observe que tiene terminantemente prohibido usar, difundir, distribuir o copiar esta comunicaci&oacute;n. Si ha recibido este mensaje por error deber&aacute; avisar inmediatamente de ello a su emisor por tel&eacute;fono o correo electr&oacute;nico y eliminarlo totalmente de su sistema. No se garantiza que las comunicaciones por Internet sean enviadas en el plazo correspondiente o que no contengan errores o virus. El emisor no aceptar&aacute; responsabilidad por errores u omisiones. Las opiniones vertidas son responsabilidad del autor y no son emitidas ni avaladas por Contreras ni ninguna de sus subsidiarias. <br /> <br /> No imprima este correo si no es necesario; todos tenemos un papel en el cuidado del medio ambiente. <br /> Do not print this mail if it is not necessary. <br /> <br /> This communication and any attached file contain information that is legally privileged, confidential or exempt from disclosure. If you are not the intended recipient, please note that any dissemination, distribution, or copying of this communication is strictly prohibited. Anyone who receives this message in error should notify the sender immediately by telephone or by return e-mail and delete this communication entirely from his or her computer. Contreras is neither liable for the proper and complete transmission of the information contained in this communication nor for any delay, error or virus. Any opinions presented are solely those of its author and do not necessarily represent those of Contreras or any of its subsidiary companies.</span></p>
				</div>';*/ //Fin message HTML
			eval("\$sendBody8 = \"$customEmail->body\";");
			$mail->body = $sendBody8;
				

			if($mail->save())
			{
				$mail->addReceiver($email); // Email Destinatario
				$send = User::model()->sendPswReestablecido($email,$psw);
				$this->_sendResponse(200, CJSON::encode(array('success' => 'Se  ha enviado la nueva contraseña a su correo electrónico. Luego podrá cambiarla desde el perfil.')));
			} else
			{
				$this->_sendResponse(200, CJSON::encode(array('success' => 'Se ha producido un error inesperado')));
			}
			
			
		}
		else if ($psw == "vencio")
		{
			$this->_sendResponse(200, CJSON::encode(array('error' => 'Se venció el pedido de contraseña, solicitelo nuevamente')));
		}
		else if ( $psw == "false")
		{
			$this->_sendResponse(200, CJSON::encode(array('error' => 'Este usuario no solicitó reestablecimiento de contraseña')));
		}
		$this->_sendResponse(200, CJSON::encode(array('success' => $psw)));
		Yii::app()->end();
	}
	


///////////////TEST
public function actionSendMailUserReject()
	{
		$this->_checkAuth();
		 	if($_GET['id'] > 0){
		 		$criteria = new CDbCriteria();
		 		$criteria->addCondition('idCompany = '. $_GET['id']);
		 		$provider = Provider::model()->find($criteria);
		 		$user = User::model()->findByPk($provider->idUser);
		 	}
		if(count($user) == 1)
		{
			$send = User::model()->sendMailProvReject($user->email);
			if($send) {
				$this->_sendResponse(200, CJSON::encode(array('success'=>"Se ha enviado el email satisfactoriamente")));
			}
			else {
				$this->_sendResponse(200, CJSON::encode(array('error'=> array('name'=>'No se pudo enviar el mail.' ) ,'Explanation'=> 'Error: mail dont send') ) );
			}
		}
		else {
		$this->_sendResponse(200, CJSON::encode(array('error'=> array('name'=>'No existe el correo electrónico en el sistema.' ) ,'Explanation'=> 'Error: no company')));
		
		}
	}




	public function actionSendMailUserActivation()
	{
		$this->_checkAuth();
		if($_GET['id'] > 0){
			$criteria = new CDbCriteria();
			//$criteria->addCondition('idCompany = '. $_GET['id']);
			$criteria->addCondition('idCompany = '. $_GET['id']);
			$criteria->addCondition('isAdmin = 1');
			$provider = Provider::model()->find($criteria);
			$user = User::model()->findByPk($provider->idUser);
			/*
			$users = [];
			foreach ($providers as $index => $provider) {
				$users[$index] = User::model()->findByPk($provider->idUser);
			}
			*/
		}
		if(count($user) == 1)
		{
			$send = User::model()->sendMailProvActivation($user->email);
			if($send) {
				$this->_sendResponse(200, CJSON::encode(array('success'=>"Se ha enviado el email satisfactoriamente")));
			}
			else {
				$this->_sendResponse(200, CJSON::encode(array('error'=> array('name'=>'No se pudo enviar el mail.' ) ,'Explanation'=> 'Error: mail dont send') ) );
			}
		}
		else {
		$this->_sendResponse(200, CJSON::encode(array('error'=> array('name'=>'No existe el correo electrónico en el sistema.' ) ,'Explanation'=> 'Error: no company')));
		
		}
	}
///////



	public function actionSolicitarReestablecimientoPsw()
	{
		$c = new CDbCriteria();
		$c->addCondition('t.email = "'. $_GET['email'].'"');
		$user = User::model()->findAll($c); 
		if(count($user) == 1)
		{
			$send = User::model()->sendMailReestablecer($_GET['email']);
			if($send) {
				$levelLog = 1;
				$source = 'ApiController->actionSolicitudUsuario';
				$commentaryLog = 'Se ha solicitado un reestablecimiento de contraseña para el usuario '.$_GET['email'];
				Logs::model()->log($levelLog,$commentaryLog,$source, $user[0]{'idUser'});


				$this->_sendResponse(200, CJSON::encode(array('success'=>"Se ha enviado el email satisfactoriamente")));

			}
			else {
				$this->_sendResponse(200, CJSON::encode(array('error'=> array('name'=>'No se pudo enviar el mail.' ) ,'Explanation'=> 'Error: mail dont send') ) );
			}
		}
		else {
		$this->_sendResponse(200, CJSON::encode(array('error'=> array('name'=>'No existe el correo electrónico en el sistema.' ) ,'Explanation'=> 'Error: no company')));
		
		}
	}

	public function actionSolicitudUsuario()  // verifica si los datos son correctos y se cargan en la tabla del admin
	{
		if(!$this->actionCheckFront())
		{
			return;
		}
 

		//parametros por url
	    if(!isset($_GET['rut']))
	        $this->_sendResponse(200, CJSON::encode(array('error'=> array('name'=>'InvalidArgumentError' ) ,'Explanation'=> 'Error: Parameter rut is missing') ) );  	
		$c = new CDbCriteria();
		//$c->addSearchCondition('t.idCompanyProvider', $_GET['idCompanyProvider']);//"t." Alias asignada al 'invoice' 
		//$c->join = "Select cuit from company INNER JOIN invoice ON company.idCompany=invoice.idCompanyProvider";		
		$c->addCondition('t.cuit = '. $_GET['rut']);
		//$c->addCondition('t.company = "'. $_GET['companyName'].'"');
		try {
			$company = Company::model()->findAll($c); 
		}
		catch(Exception $ex) {
				//$this->_sendResponse(200, CJSON::encode(array('error'=> array('name'=>'No se encuentra la compaÃƒÂ±ÃƒÂ­a' ) ,'Explanation'=> 'Error: Company doesnÃ‚Â´t exists') ) );
		}
	   //SI ENCUENTRA LA COMPAÃƒâ€˜ÃƒÂA

		if(count($company) == 1) {

			/*
			$companyName = $company[0]->companyName;
			if(empty($companyName))
			{
				$company[0]->companyName = $_GET['companyName'];
				$company[0]->company = $_GET['companyName'];
				$company[0]->save(false);
			}
			else {
				$c = new CDbCriteria();
				$c->addCondition('t.cuit = '. $_GET['rut']);
				$c->addCondition('t.companyName = "'.$_GET['companyName'].'" or t.company = "'.$_GET['companyName'].'"');
				$company = Company::model()->findAll($c); 
			}*/
		} else {
			$this->_sendResponse(200, CJSON::encode(array('error'=> array('name'=>'No se encuentra la compañía asociada a ese CUIT' ) ,'Explanation'=> 'Error: Company doesn\'t exists') ) );
		}
		

	   if(count($company) === 1)
	   {
			
			if($this->actionCheckParamInvoices()) {
				$c = new CDbCriteria();
				//$c->addSearchCondition('t.idCompanyProvider', $_GET['idCompanyProvider']);//"t." Alias asignada al 'invoice' 
				//$c->join = "Select cuit from company INNER JOIN invoice ON company.idCompany=invoice.idCompanyProvider";		
				$c->addCondition('(t.numberinvoice = "'. $_GET['invoice0'].'" AND t.dateEmission = "'.$_GET['date0'].'") OR (t.numberinvoice = "'. $_GET['invoice1'].'" AND t.dateEmission = "'.$_GET['date1'].'") OR (t.numberinvoice = "'. $_GET['invoice2'].'" AND t.dateEmission = "'.$_GET['date2'].'")');
				
				try {
					$invoices = Invoice::model()->findAll($c); 
				}
				catch(Exception $ex) {
					$this->_sendResponse(200, CJSON::encode(array('error'=> array('name'=>'La compañía no posee facturas vinculadas' ) ,'Explanation'=> 'Error: Company doesn´t exists') ) );
				}
			}
			//SI ENCUENTRA LAS FACTURAS
			if(!$this->actionCheckParamInvoices() || count($invoices) == 3)
			{
				//Si mando a reestablecer la contraseÃƒÂ±a
				/*$sql = "SELECT `idUser` FROM `user` WHERE status = 0 AND `email` = '".$_GET['email']."' ORDER BY idUser DESC";
				$idUser = Yii::app()->db->createCommand($sql)->queryAll();
				*/
				$user = User::model()->find('status = 0 AND email = "'. $_GET['email'].'"');
				//$this->_sendResponse(200, CJSON::encode(array('error'=> array('name'=>$user ) ,'Explanation'=> 'Error: Company doesnÃ‚Â´t exists') ) );
				if(!isset($user))
				{
					//USUARIO
					//Creacion
					/*if($company[0]->corporateMail != $_GET['email']) {
						$this->_sendResponse(200, CJSON::encode(array('error'=> array('name'=>'No coincide con el correo corporativo')) ) );
						}*/
						
						$user = new User();
						$user->name = $_GET['name'];
						$user->email = $_GET['email'];
						$user->idCompany = $company[0]->idCompany;
						$user->save();
						
						$idUser = $user->idUser;

						$message = new Messages();
						$message->idEmisor = 1;
						$message->idReceptor = $idUser;
						$message->message = "¡Bienvenido al portal de Conteras!";
						$message->date = date('Y-m-d H:i:s', time());
						$message->messageType = 1;
						$message->status = 0;
						$message->save();
					} 
					else
					{
						$this->_sendResponse(200, CJSON::encode(array('error'=> array('name'=>'El usuario ingresado se encuentra registrado ' ) ,'Explanation'=> 'Error: Company doesnÃ‚Â´t exists') ) );
				    }

					//COMENTARIO

						
					if(!empty($_GET['reason']))
					{
						$dateComments = date('Y-m-d H:i:s');
						//Carga
						$sql = "INSERT INTO comments (description, idCommentType, idUser, dateComments, statusComments, idRefenceTo) values (:reason, 1, :idUser, :dateComments, 1, NULL)";
						$parameters = array(":reason"=>$_GET['reason'], ':idUser' => $idUser, ':dateComments' => $dateComments);
						Yii::app()->db->createCommand($sql)->execute($parameters);
						//ID
						$sql = "SELECT `idComments` FROM `comments` WHERE `description` = '".$_GET['reason']."' AND idCommentType = 1 AND dateComments = '".$dateComments . "' AND idUser = '".$idUser."'";
						$idComments = Yii::app()->db->createCommand($sql)->queryAll();
						$idCom = $idComments[0]{'idComments'};
					}
					//PROVEEDOR
					else
					{
						$idCom = 0;
					}
					$sql = "INSERT INTO provider (idCompany, idUser, idComments, idProviderStatus, date) values (:idCompany, :idUser, :idComments, 1, :date)";
					$parameters = array(":idCompany"=>$company[0]->idCompany, ':idUser' => $idUser, ':idComments' => $idCom, ':date' => date('Y-m-d H:i:s'));
					Yii::app()->db->createCommand($sql)->execute($parameters);


					//FIN

					$levelLog = 1;
					$source = 'ApiController->actionSolicitudUsuario';
					$commentaryLog = $_GET['email'].' ha solicitado un nuevo usuario para el portal.';
					Logs::model()->log($levelLog,$commentaryLog,$source, $idUser);
					 
					try {
						$this->sendEmailSolicitudAdmins($_GET['email'], $_GET['name'], $_GET['reason'], $_GET['rut'], $company[0]->companyName);
					}
					catch (Exception $ex) {
						$this->_sendResponse(500, CJSON::encode($ex->message));	
					}
 
					$this->_sendResponse(200, CJSON::encode(array('success' => 'Se ha enviado su solicitud, en breve
estaremos procesándola.')));
				
			}
			else
			{
				$this->_sendResponse(200, CJSON::encode(array('error'=> array('name'=>'No se encuentran las facturas' ) ,'Explanation'=> 'Error: Invoices donÃ‚Â´t exist') ) );	
			}
	   }
	   else
	   {
			$this->_sendResponse(200, CJSON::encode(array('error'=> array('name'=>'No se encuentra la compañía con ese nombre' ) ,'Explanation'=> 'Error: Company doesnÃ‚Â´t exists') ) );
	   }
	}

	public function sendEmailSolicitudAdmins($email, $name, $reason, $rut, $companyName)
	{
		try {
			$adminMails = array();
			$users = User::model()->findAll();
			foreach ($users as $key => $user) {
				if($user->hasRole(21))
				{
					$adminMails[$key] = $user->email; 
				}
			}

			if(count($adminMails) >= 1)
			{
				$mail = new Email();
				$emailParameters = EmailParameters::model()->findByPk(1);
				$customEmail = CustomEmail::model()->findByPk(18);

				//$mail->subject = "Solicitud de Activación de Nuevo Usuario";
				$mail->subject = $customEmail->subject;
				//$mail->AddCC($participant->consultant->email);
				$date = date("d/m/Y H:i:s");
				/*$mail->body =
					'<div style="font-size: 10.0pt; font-family: \'Verdana\',sans-serif;">
					   <p>Se ha registrado su solicitud de alta como usuario en nuestro Portal el siguiente proveedor:</p>	
						<li>Nombre y Apllido:'.$name.' </li> 
						<li>CUIT: '.$rut.'</li>
						<li>Nombre de la compañía: '.$companyName.'</li>
						<li>Motivo: '.$reason.'</li>
						<li>Email: '.$email.'</li>
						<br>
						<p>Ingrese al siguiente link para ir al sitio 
							<a href="'.$emailParameters->hostForMails.'">Portal de Provoeedores</a></p> <br/>
						<p class="m_-3923079614135954232xmsonormal"><span style="font-size: 9.0pt; font-family: \'Verdana\',sans-serif;">Este mensaje fue generado automáticamente por registrarse o realizar alguna acción en nuestro sitio de proveedores, se ha contactado vía mail o por teléfono con CONTRERAS. Si usted desconoce alguna acción de las antes mencionadas, descarte este mail. El contenido es propiedad de CONTRERAS y garantiza la privacidad y seguridad de su información.</span><u></u><u></u></p>
						<p class="MsoNormal"><img id="m_-3923079614135954232x__x005f_x0000_i1025" style="width: 74px; height: 93px;" src="https://www.contreras.com.ar/img/contreras-logo.png" width="70" height="88" /></p>
						<p class="m_-3923079614135954232xmsonormal"><span style="font-size: 9.0pt; font-family: \'Verdana\',sans-serif;">San Martin 140, piso 8&deg;</span><u></u><u></u></p>
						<p class="m_-3923079614135954232xmsonormal"><span style="font-size: 9.0pt; font-family: \'Verdana\',sans-serif;">Buenos Aires, Argentina</span><u></u><u></u></p>
						<p class="m_-3923079614135954232xmsonormal"><span style="font-size: 9.0pt; font-family: \'Verdana\',sans-serif;">+54 (11) 4321 9500</span><u></u><u></u></p>
						<p class="m_-3923079614135954232xmsonormal"><span style="font-size: 10.0pt; font-family: \'Verdana\',sans-serif;"><a href="https://urldefense.proofpoint.com/v2/url?u=http-3A__www.contreras.com.ar_&amp;d=DwMFAw&amp;c=0TzQCy9lgR5hSW-bDg5HA76y7nf4lvOzvVop5GM3Y80&amp;r=lDiBsYLvsElGPOzCWcOqOemV6CUYuuTcfC74XGi7CUo&amp;m=8sseLaql5V9k-ag5cgaMdHVb0z4cwyDOoinA8rxc6E4&amp;s=Gco3yqJEEW6RxzCiGpPaywkYCh3zf9evo5sL36znb1Y&amp;e=" target="_blank" rel="noopener" data-saferedirecturl="https://www.google.com/url?q=https://urldefense.proofpoint.com/v2/url?u%3Dhttp-3A__www.contreras.com.ar_%26d%3DDwMFAw%26c%3D0TzQCy9lgR5hSW-bDg5HA76y7nf4lvOzvVop5GM3Y80%26r%3DlDiBsYLvsElGPOzCWcOqOemV6CUYuuTcfC74XGi7CUo%26m%3D8sseLaql5V9k-ag5cgaMdHVb0z4cwyDOoinA8rxc6E4%26s%3DGco3yqJEEW6RxzCiGpPaywkYCh3zf9evo5sL36znb1Y%26e%3D&amp;source=gmail&amp;ust=1569266816868000&amp;usg=AFQjCNGmf2yOrgexecmt8N0-TW0Zb1m49g">www.contreras.com.ar</a></span><u></u><u></u></p>
						<p><span style="font-size: 8.0pt; font-family: \'Verdana\',sans-serif; color: #828282;">Esta comunicaci&oacute;n y cualquier archivo adjunto contiene informaci&oacute;n reservada, confidencial, protegida por ley. Si no es usted el destinatario de la misma, observe que tiene terminantemente prohibido usar, difundir, distribuir o copiar esta comunicaci&oacute;n. Si ha recibido este mensaje por error deber&aacute; avisar inmediatamente de ello a su emisor por tel&eacute;fono o correo electr&oacute;nico y eliminarlo totalmente de su sistema. No se garantiza que las comunicaciones por Internet sean enviadas en el plazo correspondiente o que no contengan errores o virus. El emisor no aceptar&aacute; responsabilidad por errores u omisiones. Las opiniones vertidas son responsabilidad del autor y no son emitidas ni avaladas por Contreras ni ninguna de sus subsidiarias. <br /> <br /> No imprima este correo si no es necesario; todos tenemos un papel en el cuidado del medio ambiente. <br /> Do not print this mail if it is not necessary. <br /> <br /> This communication and any attached file contain information that is legally privileged, confidential or exempt from disclosure. If you are not the intended recipient, please note that any dissemination, distribution, or copying of this communication is strictly prohibited. Anyone who receives this message in error should notify the sender immediately by telephone or by return e-mail and delete this communication entirely from his or her computer. Contreras is neither liable for the proper and complete transmission of the information contained in this communication nor for any delay, error or virus. Any opinions presented are solely those of its author and do not necessarily represent those of Contreras or any of its subsidiary companies.</span></p>
						</div>';*/ //Fin message HTML
				eval("\$sendBody9 = \"$customEmail->body\";");
				$mail->body = $sendBody9;
						
				$mail->save();
				foreach ($adminMails as $key => $value) {
					$mail->addReceiver($value); // Email Destinatario
				}
			}
		}
		catch (Exception $ex) {
			$this->_sendResponse(200, CJSON::encode(array('error'=> var_dump($ex))));	
		}
	}

	public function actionListByAnonymous()
	{
		$this->_checkAuth();
		if(!$this->actionCheckFront())
		{
			return;
		}
 

		$levelLog = 1;
		$source = 'ApiController->actionListByAnonymous';
		$commentaryLog = 'Se ha hecho una búsqueda anónima.';
		Logs::model()->log($levelLog,$commentaryLog,$source, 'null');
 
 


		//parametros por url
	    if(!isset($_GET['rut']))
	        $this->_sendResponse(200, CJSON::encode(array('error'=> array('name'=>'InvalidArgumentError' ) ,'Explanation'=> 'Error: Parameter rut is missing') ) );  
	    if(!isset($_GET['numberInvoice']))
	        $this->_sendResponse(200, CJSON::encode(array('error'=> array('name'=>'InvalidArgumentError' ) ,'Explanation'=> 'Error: Parameter numberInvoice is missing') ) ); 
	    if(!isset($_GET['dateEmission']))
	        $this->_sendResponse(200, CJSON::encode(array('error'=> array('name'=>'InvalidArgumentError' ) ,'Explanation'=> 'Error: Parameter dateEmission is missing') ) ); 
	    if(!isset($_GET['amount']))
	        $this->_sendResponse(200, CJSON::encode(array('error'=> array('name'=>'InvalidArgumentError' ) ,'Explanation'=> 'Error: Parameter invoiceTotalAmount is missing') ) ); 


		$c = new CDbCriteria();
		//$c->addSearchCondition('t.idCompanyProvider', $_GET['idCompanyProvider']);//"t." Alias asignada al 'invoice' 
		
		$c->addCondition('t.numberInvoice = "'. $_GET['numberInvoice'].'"');
		$c->addCondition('t.dateEmission = "'. $_GET['dateEmission'].'"');
		$c->addCondition('t.invoiceTotalAmount = "'. $_GET['amount'].'"');
		$c->with = array('companyProvider');
		$c->addCondition('companyProvider.cuit = "'. $_GET['rut'].'"');	


		try {
 			$model = Invoice::model()->findAll($c); 
// 			$model = Company::model()->findAll($c);       	
		}
		catch(Exception $ex) {
			echo CJavaScript::jsonEncode(array('error' => $ex->getMessage()));
		}

	    if(is_null($model)) {
	        $this->_sendResponse(200, CJSON::encode(array('error'=> array('name'=>'NotFoundError' ) ,'Explanation'=> 'Error: Invoice not found') ) ); 
	    }
	    else {
	    	error_log('cantidad: ' . count($model) );
	    	if (count($model)===1){
	    		$data = array (
	    		 'rut'=> $model[0]->companyProvider->cuit,
	    		 'companyClientName' => $model[0]->companyClient->companyName,
	    		 'numberInvoice'=> $model[0]->numberInvoice,
	    		 'dateEmission'=> $model[0]->dateEmission,
	    		 'datePayment'=> $model[0]->datePayment, 
	    		 'invoiceStatus' => $model[0]->invoiceStatus->description,
	    		 //'resp'=> $_GET['rut'],
	    		);

		        $this->_sendResponse(200, CJSON::encode($data));
	    	} else {
		    	if (count($model)===0){
			        $this->_sendResponse(200, CJSON::encode(array('error'=> array('name'=>'NotFoundError' ) ,'Explanation'=> 'Error: Invoices not found') ) ); 
				}
	    	}
	    }
	}


	public function actionListByFilters() {
		$this->_checkAuth();
		if(!$this->actionCheckFront())
		{
			return;
		}
 

		$levelLog = 1;
		$source = 'ApiController->actionListByFilters';
		$commentaryLog = 'EL usuario hizo una búsqueda con filtros.';
		Logs::model()->log($levelLog,$commentaryLog,$source, $_GET['idUser']);
 				
 				$c = new CDbCriteria();
				$c->addCondition('idUser = '.$_GET['idUser']);
				$roles = UserHasRoles::model()->findAll($c);

				$condition = new CDbCriteria();
				if(count($roles) == 0)
				{
					$condition->addCondition('p.idUser = '. $_GET['idUser']);
					$condition->join = "INNER JOIN provider as p ON t.idCompanyProvider = p.idCompany";
				}
				$condition->order = "numberInvoice ASC";
	

				$invoices = Invoice::model()->findAll($condition);
				if(count($invoices)> 0)
				{
					for($i=0 ; $i < count($invoices) ; $i++) {
						$data[$i] = array (
						'rut'=> $invoices[$i]->companyProvider->cuit,
						'companyProviderName' => $invoices[$i]->companyProvider->companyName,
						'companyClientName' => $invoices[$i]->companyClient->companyName,
						'invoiceTotalAmount' => $invoices[$i]->invoiceTotalAmount,
						'numberInvoice'=> $invoices[$i]->numberInvoice,
						'dateEmission'=> $invoices[$i]->dateEmission,
						'datePayment'=> $invoices[$i]->datePayment, 
						'dateExpiration'=> $invoices[$i]->dateExpiration,
						'invoiceStatus' => $invoices[$i]->invoiceStatus->description,
						//'resp'=> $_GET['rut'],
						);
					}
				}
				else {
					$this->_sendResponse(200, CJSON::encode(array('error'=> array('name'=>'NotFoundError' ) ,'Explanation'=> 'Error: Invoices not found') ) );
				}
 
				$this->_sendResponse(200, CJSON::encode(array('invoices' => $data)));
	}
	
	// Recibe los comentarios de los usuarios en "actualizar datos"
	public function actiongetLicitaciones()
	{
		$this->_checkAuth();

		$id = $_GET['id'];
		$c = new CDbCriteria();
		$c->addCondition('idUser = '.$id);
		$roles = UserHasRoles::model()->findAll($c);
		$licitaciones = array();
		$licHabilitadas = array();
		
		if(count($roles) > 0)
		{
			$model = Licitaciones::model()->findAll('status != 5');
			foreach ($model as $key => $value) {
				$licitaciones[$key] = $value->attributes;
				$licitaciones[$key]['proveedores'] = LicitacionesProveedor::model()->findAll('idLicitacion = '. $value->idLicitacion); 
				$licitaciones[$key]['status'] = LicitacionesStatus::model()->findByPk($value->status);
				$consultas = array();
				$licitaciones[$key]['consultas'] = LicitacionesConsulta::model()->findAll('idLicitacion = ' . $value->idLicitacion);
				foreach ($licitaciones[$key]['consultas'] as $index => $consulta){
					$consultas[$index] = $consulta->attributes;
					$consultas[$index]['name'] = User::model()->find('idUser = '. $consulta->idEmisor)->name;
				}
				$licitaciones[$key]['consultas'] = $consultas;
				$licitaciones[$key]['propuestas'] = LicitacionesOfertas::model()->findall('idLicitacion = '. $value->idLicitacion);			
				$licitaciones[$key]['buyers'] = LicitacionesCompradores::model()->findall('idLicitacion = '. $value->idLicitacion);
				$licitaciones[$key]['documents'] = LicitacionesDocumentos::model()->findall('idLicitacion = '. $value->idLicitacion);
				$licitaciones[$key]['buyersNames'] = [];
				foreach ($licitaciones[$key]['buyers'] as $buyer) {
					array_push ($licitaciones[$key]['buyersNames'], User::model()->find('idUser = '. $buyer->idUser)->name);
				}
				$licitaciones[$key]['supervisorName'] = User::model()->find('idUser = '. $value->supervisor)->name;
				$licitaciones[$key]['offerAmount'] = count($licitaciones[$key]['propuestas']);
			}
			$this->_sendResponse(200, CJSON::encode($licitaciones));
		}else{
			$provider = Provider::model()->find("idUser= ".$id);
			$licHabilitadas = LicitacionesProveedor::model()->findAll('idProvider = '.$provider->idProvider);
			if (is_array($licHabilitadas) || is_object($licHabilitadas)) {
				$key = 0;
				foreach ((array) $licHabilitadas as $value) {
					$licitacion = Licitaciones::model()->find('idLicitacion = '.$value->idLicitacion.' AND status in (2,3)');
					if (isset($licitacion) && is_object($licitacion)) {
						$licitaciones[$key] = $licitacion->attributes;					
						$licitaciones[$key]['status'] = LicitacionesStatus::model()->findByPk($licitaciones[$key]['status']);
						$licitaciones[$key]['visto'] = $value->status;
						$licitaciones[$key]['idLicitacionProvider'] = $value->idLicitacionProvider;
						$oferta = LicitacionesOfertas::model()->find('idProvider = '.$provider->idCompany.' AND idLicitacion = '. $value->idLicitacion);
						$licitaciones[$key]['oferta_tec'] = isset($oferta) ? $oferta->oferta_tec : null;
						$licitaciones[$key]['oferta_eco'] = isset($oferta) ? $oferta->oferta_eco : null;
						$licitaciones[$key]['consultas'] = LicitacionesConsulta::model()->findAll('(idEmisor = ' . $id . ' OR  idReceptor = ' . $id .' OR messageType = 1 OR idMessage in (SELECT replyTo FROM licitaciones_consultas WHERE messageType=1 AND idLicitacion = ' . $value->idLicitacion .')) AND idLicitacion = ' . $value->idLicitacion);
						$licitaciones[$key]['buyers'] = LicitacionesCompradores::model()->findall('idLicitacion = '. $value->idLicitacion);
						$licitaciones[$key]['documents'] = LicitacionesDocumentos::model()->findall('idLicitacion = '. $value->idLicitacion);
						$licitaciones[$key]['buyersNames'] = [];
						foreach ($licitaciones[$key]['buyers'] as $buyer) {
							array_push ($licitaciones[$key]['buyersNames'], User::model()->find('idUser = '. $buyer->idUser)->name);
						}
						$licitaciones[$key]['supervisorName'] = User::model()->find('idUser = '. $licitacion->supervisor)->name;
						$key++;
					}
				}
			}
			$this->_sendResponse(200, CJSON::encode($licitaciones));
		}
	}

	public function actionGuardarProveedoresLicitacion() {
		$this->_checkAuth();

		$post = Yii::app()->request->getRawBody();
		$array = json_decode($post);
		$idLicitacion = $array->idLicitacion;
		LicitacionesProveedor::model()->deleteAllByAttributes(['idLicitacion' => $idLicitacion]);
		$companies = $array->providers;			// PROVEEDOR DEL FRONT = COMPANIA DEL BACK
		if (is_array($companies) && count($companies) > 0) {
			foreach ($companies as $idCompany) {
				$providers = Provider::model()->findAll('idCompany = '. $idCompany);
				foreach ($providers as $prov) {
					try {
						$licProv = new LicitacionesProveedor();
						$licProv->idLicitacion = $idLicitacion;
						$licProv->idProvider = $prov->idProvider;
						$licProv->status = 0;
						$licProv->save();
					}
					catch (Exception $ex) {
						$this->_sendResponse(500, $ex->getMessage());
					}
				}
			}
		}
		$this->_sendResponse(200, "true");
	}

	public function actionDeleteRubrosFromCompany() {
		$this->_checkAuth();
		$idCompany = $_GET['id'];
		Rubros_company::model()->deleteAllByAttributes(['idCompany' => $idCompany]);
		$this->_sendResponse(200, "true");
	}

	public function actionAssociateHesWithDocument() {
		$this->_checkAuth();
		$post = Yii::app()->request->getRawBody();
		$array = json_decode($post);
		
		$idBuyOrderDocument = $array->idBuyOrderDocument;
		$hes = $array->hes;

		foreach ($hes as $key => $value) {
			$buyOrderDocumentHes = new BuyOrderDocumentHes();
			$buyOrderDocumentHes->idBuyOrderDocument = (int)$idBuyOrderDocument;
			$buyOrderDocumentHes->idHes = (int)$value;
			$buyOrderDocumentHes->save();
		}

		$this->_sendResponse(200, "true");
	}
	public function actionAssociateMigoWithDocument() {
		$this->_checkAuth();
		$post = Yii::app()->request->getRawBody();
		$array = json_decode($post);
		
		$idBuyOrderDocument = $array->idBuyOrderDocument;
		$migo = $array->migo;

		foreach ($migo as $key => $value) {
			$buyOrderDocumentMigo = new BuyOrderDocumentMigo();
			$buyOrderDocumentMigo->idBuyOrderDocument = (int)$idBuyOrderDocument;
			$buyOrderDocumentMigo->idMigo = (int)$value;
			$buyOrderDocumentMigo->save();
		}

		$this->_sendResponse(200, "true");
	}
	public function actionInvitarProveedoresLicitacion() {
		$this->_checkAuth();
		$post = Yii::app()->request->getRawBody();
		$array = json_decode($post);
		$idLicitacion = $array->idLicitacion;
		
		LicitacionesProveedor::model()->deleteAllByAttributes(['idLicitacion' => $idLicitacion]);
		$licitacion = Licitaciones::model()->findbyPk($idLicitacion);
		$licitacion->status = 2;
		$licitacion->save();
		$companies = $array->providers;			// PROVEEDOR DEL FRONT = COMPANIA DEL BACK
		if (is_array($companies) && count($companies) > 0) {
			foreach ($companies as $idCompany) {
				$providers = Provider::model()->findAll('idCompany = '. $idCompany);
				foreach ($providers as $prov) {
					try {
						$licProv = new LicitacionesProveedor();
						$licProv->idLicitacion = $idLicitacion;
						$licProv->idProvider = $prov->idProvider;
						$licProv->status = 0;
						$licProv->save();
						$prov->sendMailLicitacion($licitacion->codPO, $licitacion->fecha_fin);
					}
					catch (Exception $ex) {
						$this->_sendResponse(500, $ex->getMessage());
					}
				}
			}
		}
		$this->_sendResponse(200, 'true');
	}

	public function actionCountOfertasConfirmadas()
	{
		$this->_checkAuth();
		if(!$this->actionCheckFront())
		{
			return;
		}
		$idLicitacion = $_GET['id'];
		$sql = "SELECT COUNT(idOferta) FROM licitaciones_ofertas WHERE status = 2 AND idLicitacion = '".$idLicitacion."'";
		$command = Yii::app()->db->createCommand($sql)->queryAll();
		$this->_sendResponse(200, CJSON::encode($command));
	}

	public function actionCountStatus()
	{
		$this->_checkAuth();
		if(!$this->actionCheckFront())
		{
			return;
		}
 

		$idUser = $_GET['idUser'];

		$c = new CDbCriteria();
		$c->addCondition('idUser = '.$_GET['idUser']);
		
		$roles = UserHasRoles::model()->findAll($c);

		$statuses = Invoicestatus::model()->findAll();
		foreach ($statuses as $key => $value) {
			
			if(count($roles) == 0)
			{
				/*$sql = "SELECT (SELECT COUNT(*) FROM invoice where idStatusInvoice = 1 AND idCompanyProvider IN (SELECT idCompany from provider where idUser = ". $idUser .")) as 'a', (SELECT COUNT(*) FROM invoice where idStatusInvoice = 2 AND idCompanyProvider IN (SELECT idCompany from provider where idUser = ". $idUser .")) as 'b', (SELECT COUNT(*) FROM invoice where idStatusInvoice = 3 AND idCompanyProvider IN (SELECT idCompany from provider where idUser = ". $idUser .")) as 'c',(SELECT COUNT(*) FROM invoice where idStatusInvoice = 4 AND idCompanyProvider IN (SELECT idCompany from provider where idUser = ". $idUser .")) as 'd', (SELECT COUNT(*) FROM invoice where idStatusInvoice = 5 AND idCompanyProvider IN (SELECT idCompany from provider where idUser = ". $idUser .")) as 'e', (SELECT COUNT(*) FROM invoice where idStatusInvoice = 6 AND idCompanyProvider IN (SELECT idCompany from provider where idUser = ". $idUser .")) as 'f' FROM `invoice` as i INNER JOIN provider as p ON i.idCompanyProvider = p.idCompany WHERE p.idUser = ".$idUser." LIMIT 1;";*/

				$sql = "SELECT (SELECT COUNT(*) FROM invoice where idStatusInvoice = ".$value->idInvoiceStatus." AND idCompanyProvider IN (SELECT idCompany from provider where idUser = ". $idUser .")) as 'count', '".$value->description."' as 'description', '".$value->idInvoiceStatus."' as 'idStatusInvoice' FROM `invoice` as i INNER JOIN provider as p ON i.idCompanyProvider = p.idCompany WHERE p.idUser = ".$idUser." LIMIT 1;";
			}
			else {
				/*$sql = "SELECT (SELECT COUNT(*) FROM invoice where idStatusInvoice = 1  ) as 'a', (SELECT COUNT(*) FROM invoice where idStatusInvoice = 2  ) as 'b', (SELECT COUNT(*) FROM invoice where idStatusInvoice = 3  ) as 'c',(SELECT COUNT(*) FROM invoice where idStatusInvoice = 4  ) as 'd', (SELECT COUNT(*) FROM invoice where idStatusInvoice = 5  ) as 'e', (SELECT COUNT(*) FROM invoice where idStatusInvoice = 6  ) as 'f' FROM `invoice` as i INNER JOIN provider as p ON i.idCompanyProvider = p.idCompany WHERE p.idUser = ".$idUser." LIMIT 1;";*/
				
				$sql = "SELECT (SELECT COUNT(*) FROM invoice where idStatusInvoice = ".$value->idInvoiceStatus.") as 'count', '".$value->description."' as 'description', '".$value->idInvoiceStatus."' as 'idStatusInvoice' FROM `invoice` WHERE 1 LIMIT 1;";
			}
			$command[$key] = Yii::app()->db->createCommand($sql)->queryAll();
		}	
		
		//$command .= Yii::app()->db->createCommand($sql)->queryAll();
		foreach ($command as $key => $value) {
			$data[$key] = array('count' => $value[0]{'count'}, 'description' => $value[0]{'description'}, 'idStatusInvoice' => $value[0]{'idStatusInvoice'});
		}
		if($data)
		{
			$this->_sendResponse(200, CJSON::encode(array('status' => $data)));
		}


	}

	public function actionCheckParam()
	{
		$idParameter = $_GET['id'];
		$sql = "SELECT value, stringValue from parameters where idParameter = '".$idParameter."'";
		$command = Yii::app()->db->createCommand($sql)->queryAll();
		if($command[0]{'value'} == 0)
		{
			$this->_sendResponse(200, CJSON::encode(array('error' => $command[0]{'stringValue'})));
		}
	}

	public function actionSaveNewProviderRegister()
	{
		$this->_checkAuth();
		$this->actionSaveNewProvider();
	}
	public function actionSaveNewProvider()
	{
		$transaction = Yii::app()->db->beginTransaction();
		try {
			$post = Yii::app()->request->getRawBody();
			$data = json_decode($post);

			$generalData = $data->generalData;
			$paymentData = $data->paymentData;
			$taxData 	 = $data->taxData;
			$user		 = $data->userData;

			// COMPAÑIA
			$newCompany 			 = new Company();
			$newCompany->cuit 		 = $generalData->cuit;
			$newCompany->company	 = $generalData->company;
			$newCompany->companyName = $generalData->companyName;
			$this->trySaveModel($newCompany, $transaction);

			// USER
			$newUser 		= new User();
			$newUser->name	= $user->name;
			$newUser->email	= $user->email;
			$newUser->idCompany = $newCompany->idCompany;
			$newUser->idJob = 5;
			$this->trySaveModel($newUser, $transaction);
			$newUser->updatePsw($newUser->idUser, $user->password);

			// PROVIDER
			$newProvider = new Provider();
			$newProvider->idCompany = $newCompany->idCompany;
			$newProvider->idUser	= $newUser->idUser;
			$newProvider->date 		= date('y-m-d h:i:s');
			$newProvider->idProviderStatus = 2;
			$newProvider->isAdmin = 1;
			$newProvider->aceptoTerminos = 1;
			$this->trySaveModel($newProvider, $transaction);
			
			// GENERAL DATA
			$companyGeneralData = new CompanyGeneralData();
			foreach ($generalData as $attr => $value) {
				if($companyGeneralData->isAttributeSafe($attr))
					$companyGeneralData->$attr = $value;			
				}
			$this->trySaveModel($companyGeneralData, $transaction);

			// rubros
			foreach ($generalData->rubros as $service) {
				$newService = new Rubros_company();
				$newService->idRubro = $service;
				$newService->idCompany = $newCompany->idCompany;
				$this->trySaveModel($newService, $transaction);
			}

			// PAYMENT DATA
			$companyPaymentData = new CompanyPaymentData();
			foreach ($paymentData as $attr => $value) {
				if($companyPaymentData->isAttributeSafe($attr))
					$companyPaymentData->$attr = $value;

			}
			$this->trySaveModel($companyPaymentData, $transaction);

			// TAX DATA
			$companyTaxData = new CompanyTaxData();
			foreach ($taxData as $attr => $value) {
				if($companyTaxData->isAttributeSafe($attr))
					$companyTaxData->$attr = $value;
			}
			$this->trySaveModel($companyTaxData, $transaction);

			// JURISDICCIONES (IIBB)
			foreach ($taxData->jurisdictions as $jurisdiction) {
				$newCompanyJurisdiction = new CompanyJurisdictionData();
				foreach ($jurisdiction as $attr => $value) {
					if($newCompanyJurisdiction->isAttributeSafe($attr))
						$newCompanyJurisdiction->$attr = $value;

				}
				$newCompanyJurisdiction->idTaxData = $companyTaxData->idTaxData;
				$this->trySaveModel($newCompanyJurisdiction, $transaction);
			}

			// PDF DE EXCEPCIONES (IIBB)
			foreach ($taxData->pdfsExempt as $pdfExempt) {
				$newCompanyPdfExemption = new CompanyPdfExemptions();
				$newCompanyPdfExemption->pdfExempt = $pdfExempt;
				$newCompanyPdfExemption->idTaxData = $companyTaxData->idTaxData;
				$this->trySaveModel($newCompanyPdfExemption, $transaction);

			}

			// RETENTION REGIMEN (EARNINGS)
			//foreach ($taxData->retentionRegimen as $retentionRegimen) {
				if ($taxData->retentionRegimen !== ""){
				$newCompanyEarningsRetentionRegimen = new CompanyEarningsRetentionRegimen();
				$newCompanyEarningsRetentionRegimen->idRetentionRegime = $taxData->retentionRegimen;
				$newCompanyEarningsRetentionRegimen->idTaxData = $companyTaxData->idTaxData;
				$this->trySaveModel($newCompanyEarningsRetentionRegimen, $transaction);
				//$this->_sendResponse(500, CJSON::encode([ 'success' => "entro a earn", 'idUser' => $newUser->idUser ]));	
			}		
			//}

			// ASOCIAR LA DATA A LA COMPAÑIA

			$newCompany->idGeneralData = $companyGeneralData->idGeneralData;
			$newCompany->idPaymentData = $companyPaymentData->idPaymentData;
			$newCompany->idTaxData	   = $companyTaxData->idTaxData;
			if ($newCompany->save()) {
				// FIN
				$transaction->commit();
				$this->_sendResponse(200, CJSON::encode([ 'success' => "Información enviada", 'idUser' => $newUser->idUser ]));
			}
		}
		catch (Exception $e) {
			$transaction->rollBack();
			$this->_sendResponse(500, CJSON::encode([ 'error' => $e->getMessage() ]));
			throw $e;
		}

	}

	private function trySaveModel ($model, $transaction) {
		if (!$model->save()) {
			$transaction->rollBack();
			$this->_sendResponse(500, CJSON::encode($model->getErrors()));
		}
	}

	public function actionFindConfiguration()
	{
		$value = $_GET['attribute'];
		$model = Configuration::model()->find("attribute = '".$value."'");
		$this->_sendResponse(200, CJSON::encode($model));
	}

	public function actionCanDeleteCompany()
	{
		$this->_checkAuth();
		$value = $_GET['id'];
		$model1 = User::model()->find("idCompany = '".$value."'");
		$model2 = BuyOrder::model()->find("idCompany = '".$value."'");
		$this->_sendResponse(200, CJSON::encode(!(isset($model1) || isset($model2))));
	}

	public function actionDeleteLicitacionesConsulta()
	{
		$this->_checkAuth();
		$value = $_GET['id'];
		$model = LicitacionesConsulta::model()->find("idMessage = '".$value."'");
		$model2 = LicitacionesConsulta::model()->find("idMessage = '".$model->replyTo."'");
		$model->delete();
		$model2->delete();
		//$this->_sendResponse(200, CJSON::encode(!(isset($model))));
		$this->_sendResponse(200, CJSON::encode((isset($model) || isset($model2))));
	}	

	public function actionCheckParamInvoices()
	{
		$sql = "SELECT value, stringValue from parameters where idParameter = 'SUF-001'";
		$command = Yii::app()->db->createCommand($sql)->queryAll();
		if($command[0]{'value'} == 0)
		{
			return false;
		}
		else
		{
			return true;
		}
	}

	public function actionCheckFront()
	{
		$sql = "SELECT value, stringValue from parameters where idParameter = 'FE-001'";
		$command = Yii::app()->db->createCommand($sql)->queryAll();
		if($command[0]{'value'} == 0)
		{
			return false;
		}
		else
		{
			return true;
		}
	}

	public function actionSendComment()
	{
		$this->_checkAuth();
		if(!$this->actionCheckFront())
		{
			return;
		}
 

		$post = Yii::app()->request->getRawBody();
		$array = json_decode($post);
		$idUser = $_GET['idUser'];
		$comment = $_GET['comment'];		

		$sql = "INSERT INTO comments (description, idCommentType, idUser, dateComments, statusComments, idRefenceTo) values (:description, 1, :idUser, :dateComments, 1, NULL)";
		$parameters = array(":description" => $comment, ':idUser' => $idUser, ':dateComments' => date('Y-m-d H:i:s'));
		$command = Yii::app()->db->createCommand($sql)->execute($parameters);
		
		if($command)
		{
			$levelLog = 1;
			$source = 'ApiController->actionSendComment';
			$commentaryLog = 'El usuario ha enviado un comentario';
			Logs::model()->log($levelLog,$commentaryLog,$source, $_GET['idUser']);
 
			$this->_sendResponse(200, CJSON::encode(array('success' => 'El comentario ha sido enviado')));
		}
		else {
			$this->_sendResponse(200, CJSON::encode(array('error' => 'Error al enviar el comentario')));
		}
	}

	public function actionUploadFile() {
		if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {
			$this->actionPreflight();
			Yii::app()->end();
		}
		try {
			if (($_FILES['file']['name']!="")){
				// Where the file is going to be stored
				if (isset($_POST['folder'])) {
					$target_dir = 'upload/' . $_POST['folder'] . '/';
				} else {
					$target_dir = 'upload/others/';
				}
				$file = $_FILES['file']['name'];
				$path = pathinfo($file);
				if (mime_content_type($_FILES['file']['tmp_name']) !== 'application/pdf') {
					$this->_sendResponse(500, 'Debe ser un archivo de tipo PDF');
				}

				$filename = str_replace(' ', '_', $path['filename']);
				$ext = $path['extension'];
				$temp_name = $_FILES['file']['tmp_name'];
				$path_filename_ext = $target_dir.$filename. '_' . date('Y-m-d_H-i-s') .".".$ext;
				
				$folder_path = Configuration::model()->find('attribute = "URL-UPL"')->value;

				if (file_exists($path_filename_ext)) {
					$this->_sendResponse(500, CJSON::encode('Ya existe un archivo con ese nombre'));
				} else {
					move_uploaded_file($temp_name, $path_filename_ext);
					$this->_sendResponse(200, CJSON::encode($folder_path . $path_filename_ext));
				}		
			}else{
				$this->_sendResponse(200, "0");
			}
		}
		catch (Exception $ex) {
			$this->_sendResponse(200, $ex->getMessage());
		}
	}

	public function actionView()
	{
		$this->_checkAuth();
		if(!$this->actionCheckFront())
		{
			return;
		}
 
		//$this->_checkAuth();
		// Check if id was submitted via GET
	    if(!isset($_GET['id']))
	        $this->_sendResponse(500, 'Error: Parameter <b>id</b> is missing' );
	 
	    switch($_GET['model'])
	    {
			// Find respective model 
			case 'licitacionesDocumentos':
	            $model = LicitacionesDocumentos::model()->findByPk($_GET['id']);
	            break;   
	        case 'invoice':
	            $model = Invoice::model()->findByPk($_GET['id']);
	            break;
	        case 'comments':
	            $model = Comments::model()->findByPk($_GET['id']);
				break;
			case 'buyOrder':
	            $model = buyOrder::model()->findByPk($_GET['id']);
				break;
			case 'company':
				$model = Company::model()->findByPk($_GET['id']);
				break;
			case 'factura':
	            $model = Factura::model()->findByPk($_GET['id']);
				break;
			case 'user':
				$model = User::model()->findByPk($_GET['id']);
				break;
			case 'configuration':
				$model = Configuration::model()->findByPk($_GET['id']);
				break;
			case 'provider':
				$model = Provider::model()->find( 'idUser ='.$_GET['id']);
				break;
	        default:
	            $this->_sendResponse(501, sprintf(
	                'Mode <b>view</b> is not implemented for model <b>%s</b>',
	                $_GET['model']) );
	            Yii::app()->end();
	    }
	    // Did we find the requested model? If not, raise an error
	    if(is_null($model))
	        $this->_sendResponse(404, 'No Item found with id '.$_GET['id']);
	    else
	        $this->_sendResponse(200, CJSON::encode($model));
	}

	public function actioncreateRubro()
	{
		$this->_checkAuth();
		if($_SERVER['REQUEST_METHOD'] == "POST") {
			$post = Yii::app()->request->getRawBody();
			$array = json_decode($post);
			$nombre = $array->nombre;
			$model = new Rubros();
			$model->nombre = $nombre;
			$model->save();
			$this->_sendResponse(200, CJSON::encode($model));
		}else{
			$this->_sendResponse(200, '');
		}
	}	
	public function actioncreateLicitacionesCompradores()
	{
		$this->_checkAuth();
		if($_SERVER['REQUEST_METHOD'] == "POST") {
			$post = Yii::app()->request->getRawBody();
			$array = json_decode($post);
			$idLicitacion = $array->idLicitacion;
			$idUser = $array->idUser;
			$model = new LicitacionesCompradores();
			$model->idLicitacion = $idLicitacion;
			$model->idUser = $idUser;
			$model->save();
			$this->_sendResponse(200, CJSON::encode($model));
		}else{
			$this->_sendResponse(200, '');
		}
	}
    public function actionCreateLicitacion()
	{
		$this->_checkAuth();
		if($_SERVER['REQUEST_METHOD'] == "POST") {
			$post = Yii::app()->request->getRawBody();
			$array = json_decode($post);
			$model = new Licitaciones();
			$model->nombre = $array->nombre;
			$model->descripcion = $array->descripcion;
			$model->fecha_inicio = $array->fecha_inicio;
			$model->fecha_fin = $array->fecha_fin;
			$model->idRubro = $array->idRubro;
			$model->save();
			$this->_sendResponse(200, CJSON::encode($model));
		}else{
			$this->_sendResponse(200, '');
		}
	}

	public function actionGetPropuestas() {
		$this->_checkAuth();
		$id = $_GET['id'];
		$c = new CDbCriteria();
		$c->addCondition('idUser = '.$id);
		$roles = UserHasRoles::model()->findAll($c);
		$models = array();
		$licitaciones = array();

		if(count($roles) > 0)
		{
			$licitaciones = Licitaciones::model()->findAll('fecha_fin <="'.date('Y-m-d H:i:s').'"');
			foreach ($licitaciones as $key => $value) {
				$propuestas = LicitacionesOfertas::model()->findAll('idLicitacion = '.$value->idLicitacion);
				if(is_array($propuestas)) {
					foreach ($propuestas as $key => $propuesta) {
						$provider = Provider::model()->findByPk($propuesta->idProvider);
						$model = $propuesta->attributes;
						$model['provider'] =Company::model()->findByPk($provider->idCompany)->companyName;
						array_push($models, $model);
					}
				}
			}
			$this->_sendResponse(200, CJSON::encode($models));

		}else{
			$provider = Provider::model()->find("idUser= ".$id);

			$propuestas = LicitacionesOfertas::model()->findAll("idProvider = ".$provider->idProvider);
			$this->_sendResponse(200, CJSON::encode($propuestas));
		}
	}

	public function actionGetProviderDocuments() {
		$this->_checkAuth();
		$id = $_GET['id'];
		$criteria = new CDbCriteria();
		$criteria->addCondition('idUser = '.$id);
		$roles = UserHasRoles::model()->findAll($criteria);
		$documents = array();
		$providerDocuments = array();
		
		if(count($roles) > 0)
		{
			$providerDocuments = ProviderDocument::model()->findAll();
		}else{
			$provider = Provider::model()->find($criteria);
			$providerDocuments = ProviderDocument::model()->findAll(array('condition' => 'idCompany= '.$provider->idCompany));
		}
		foreach ($providerDocuments as $index => $document) {
			$documents[$index] = $document->getAttributes();
			$documents[$index]['status'] = $document->status->description;
			$documents[$index]['document'] = $document->document->documentName;
			$documents[$index]['provider'] = $document->document->documentName;
			$documents[$index]['companyName'] = $document->company->companyName;
			$documents[$index]['cuit'] = $document->company->cuit;
		}
		
		$this->_sendResponse(200, CJSON::encode($documents));
	}

	public function actionGetPersonaAfip() {
		$cuit = $_GET['id'];

		$c = new CDbCriteria();	
		$c->addCondition('t.cuit = '. $cuit);
		$company = Company::model()->findAll($c); 

		if(count($company) == 0) {
			$persona = getPersonaAfip::searchByCuit($cuit);
			$this->_sendResponse(200, CJSON::encode($persona));
		} else {
			$this->_sendResponse(200, CJSON::encode('El CUIT ingresado ya se encuentra registrado en el portal') );
		}
	

	}

	public function actionCreate()
	{
		$this->_checkAuth();
		if(!$this->actionCheckFront())
		{
			return;
		}
 
	    switch($_GET['model'])
	    {
	        // Get an instance of the respective model
	        case 'comments':
	            $model = new Comments;                    
				break;
			case 'licitaciones':
				$model = new Licitaciones; 
				$post = Yii::app()->request->getRawBody();
				$array = json_decode($post);      
				
				break;
            case 'company':
                $model = new Company();
                $post = Yii::app()->request->getRawBody();
                //$this->_sendResponse(200,$post);

                $array = json_decode($post);

                break;
			case 'buyOrder':
				$model = new BuyOrder; 
				$post = Yii::app()->request->getRawBody();
				$array = json_decode($post);
				break;
			case 'buyOrderHistory':
				$model = new BuyOrderHistory; 
				$post = Yii::app()->request->getRawBody();
				$array = json_decode($post);
				break;
			case 'factura':
				$model = new Factura; 
				$post = Yii::app()->request->getRawBody();
				$array = json_decode($post);
				break;
			case 'licitacionesDocumentos':
				$model = new LicitacionesDocumentos; 
				$post = Yii::app()->request->getRawBody();
				$array = json_decode($post);	
				break;
			case 'propuestas':
				$post = Yii::app()->request->getRawBody();
				$array = json_decode($post);
				$model = Propuestas::model()->find('idProvider = '. $array->idProvider .' AND idLicitacion = '. $array->idLicitacion);
				if (!$model) {
					$model = new Propuestas(); 
				}
				$model->fecha_carga = date('Y-m-d H:i:s');
				break;
			case 'buyOrderHistory':
				$model = new BuyOrderHistory();
				$post = Yii::app()->request->getRawBody();
				$array = json_decode($post);
				break;
			case 'buyOrderDocument':
				$model = new BuyOrderDocument();
				$post = Yii::app()->request->getRawBody();
				$array = json_decode($post);
				break;
			case 'paymentOrder':
				$model = new PaymentOrder();
				$post = Yii::app()->request->getRawBody();
				$array = json_decode($post);
				break;
			case 'licitacionesConsulta':
				$model = new LicitacionesConsulta();
				$post = Yii::app()->request->getRawBody();
				$array = json_decode($post);
				break;
			case 'provider':
				$model = new Provider();
				$post = Yii::app()->request->getRawBody();
				$array = json_decode($post);
				break;
			case 'providerDocument':
				$model = new ProviderDocument();
				$post = Yii::app()->request->getRawBody();
				$array = json_decode($post);
				break;
			case 'user':
				$model = new User();
				$post = Yii::app()->request->getRawBody();
				$array = json_decode($post);
				break;
			case 'licitacionesCompradores':
					$model = new  LicitacionesCompradores();
					$post = Yii::app()->request->getRawBody();
					$array = json_decode($post);
					break;	
			case 'rubrosCompany':
				$model = new Rubros_company();
				$post = Yii::app()->request->getRawBody();
				$array = json_decode($post);
				break;
			default:	  
	            $this->_sendResponse(501, 
	                sprintf('Mode <b>create</b> is not implemented for model <b>%s</b>',
	                $_GET['model']) );
	                Yii::app()->end();
	    }
		foreach($array as $var=>$value) {
	        // Does the model have this attribute? If not raise an error
	        if($model->hasAttribute($var))
	            $model->$var = $value;
	        else
	            $this->_sendResponse(500, 
	                sprintf('Parameter <b>%s</b> is not allowed for model <b>%s</b>', $var,
	                $_GET['model']) );
		}

	    if($model->save())
	        $this->_sendResponse(200, CJSON::encode($model));
	    else {
			// Errors occurred
			$msg = '';
	        foreach($model->errors as $attribute=>$attr_errors) {
	            foreach($attr_errors as $attr_error)
	                $msg .= $attr_error;
	        }
	        $this->_sendResponse(500, $msg );
	    }
	}

	public function actionGetBuyOrder()
    {
		$this->_checkAuth();

		$id = $_GET['id'];
		
		$pageSize = Yii::app()->request->getQuery('pageSize', 100000000);
		$page = Yii::app()->request->getQuery('page', 1);
		$order = Yii::app()->request->getQuery('order', 'null');
		if($order != 'null') {
			$orderProp = Yii::app()->request->getQuery('orderProp');
		}
		$condition = Yii::app()->request->getQuery('condition', '1');
		$select = Yii::app()->request->getQuery('select', '*');
        
        $models = array();
        $c = new CDbCriteria();
        $c->addCondition('idUser = '.$id);
        $roles = UserHasRoles::model()->findAll($c);
        if(!(count($roles) > 0))
        {
			$provider = Provider::model()->findAll('idUser = '.$id);
			$condition .= ' AND statusId NOT IN (1, 7) AND idCompany='.$provider[0]->idCompany;
		}
		$buyOrders = BuyOrder::model()->findAll(array(
			'select' => $select,
			'limit' => $pageSize,
			'offset' => ($page-1) * $pageSize,
			'order' => $order != 'null' ? $orderProp . ' ' . $order : 'null',
			'condition' => $condition
		));
        foreach ($buyOrders as $key => $value) {
			$models[$key] = $value->attributes;
			$models[$key]['company'] = Company::model()->findByPk($value->idCompany)->companyName;
			$models[$key]['cuit'] = Company::model()->findByPk($value->idCompany)->cuit;
            if($documents = $value->buyOrderDocuments) {
				foreach($documents as $id => $doc) {
					$models[$key]['documents'][$id] = $doc->getAttributes();
					$models[$key]['documents'][$id]['hes'] = $doc->getHesAssociated();
					//$models[$key]['documents'][$id]['migo'] = $doc->getMigoAssociated();
					$models[$key]['documents'][$id]['paymentOrder'] = $doc->paymentOrder;
				}
				$models[$key]['documentDate'] = $value->getDateOfFirstSentDocument();
            } else {
				$models[$key]['documents'] = [];
			}
            if($historial = BuyOrderHistory::model()->findAll(array('condition' => 'idBuyOrder = '.$value->idBuyOrder, 'order' => 'date DESC'))) {
                $models[$key]['historial'] = $historial;
			}
			$models[$key]['hes'] = $value->hes;
        }
        $this->_sendResponse(200, CJSON::encode($models));
	}

	public function actionGetBuyOrderByPk()
    {
		$this->_checkAuth();

		$id = $_GET['id'];
		$c = new CDbCriteria();
		
		$c->addCondition('idBuyOrder = "'. $id .'"');
        
        $models = array();
        $buyOrders = BuyOrder::model()->findAll($c);
        foreach ($buyOrders as $key => $value) {
			$models[$key] = $value->attributes;
			$models[$key]['company'] = Company::model()->findByPk($value->idCompany)->companyName;
			$models[$key]['cuit'] = Company::model()->findByPk($value->idCompany)->cuit;
            if($documents = $value->buyOrderDocuments) {
				foreach($documents as $id => $doc) {
					$models[$key]['documents'][$id] = $doc->getAttributes();
					$models[$key]['documents'][$id]['hes'] = $doc->getHesAssociated();
					//$models[$key]['documents'][$id]['migo'] = $doc->getMigoAssociated();
					$models[$key]['documents'][$id]['paymentOrder'] = $doc->paymentOrder;
				}
				$models[$key]['documentDate'] = $value->getDateOfFirstSentDocument();
            } else {
				$models[$key]['documents'] = [];
			}
            if($historial = BuyOrderHistory::model()->findAll(array('condition' => 'idBuyOrder = '.$value->idBuyOrder, 'order' => 'date DESC'))) {
                $models[$key]['historial'] = $historial;
			}
			$models[$key]['hes'] = $value->hes;
			$models[$key]['migo'] = $value->migo;
        }
        $this->_sendResponse(200, CJSON::encode($models));
	}
	
	public function actionGetBuyOrderDocument()
    {
		$this->_checkAuth();

		$id = $_GET['id'];
		
		$pageSize = Yii::app()->request->getQuery('pageSize', 100000000);
		$page = Yii::app()->request->getQuery('page', 1);
		$order = Yii::app()->request->getQuery('order', 'null');
		if($order != 'null') {
			$orderProp = Yii::app()->request->getQuery('orderProp');
		}
		$condition = Yii::app()->request->getQuery('condition', '1');
		$select = Yii::app()->request->getQuery('select', '*');
        
        $models = array();
        $c = new CDbCriteria();
        $c->addCondition('idUser = '.$id);
        $roles = UserHasRoles::model()->findAll($c);
        if(!(count($roles) > 0))
        {
			$provider = Provider::model()->findAll('idUser = '.$id);
			$condition .= ' AND buyOrder.idCompany='.$provider[0]->idCompany;
		}
		$buyOrderDocuments = BuyOrderDocument::model()->with('buyOrder', 'buyOrder.company')->findAll(array(
			'select' => $select,
			'limit' => $pageSize,
			'offset' => ($page-1) * $pageSize,
			'order' => $order != 'null' ? $orderProp . ' ' . $order : 'null',
			'condition' => $condition
		));
        foreach ($buyOrderDocuments as $key => $value) {
			$models[$key] = $value->attributes;
			$models[$key]['idCompany'] = BuyOrder::model()->findByPk($value->idBuyOrder)->idCompany;
			$models[$key]['nic'] = BuyOrder::model()->findByPk($value->idBuyOrder)->nic;
			$models[$key]['company'] = Company::model()->findByPk($models[$key]['idCompany'])->companyName;
			$models[$key]['cuit'] = Company::model()->findByPk($models[$key]['idCompany'])->cuit;
			//$models[$key]['migo'] = $value->getMigoAssociated();			
			$models[$key]['hes'] = $value->getHesAssociated();
			$models[$key]['paymentOrder'] = $value->paymentOrder;
        }
        $this->_sendResponse(200, CJSON::encode($models));
    }

	public function actionGetOtherOperations()
	{
		$this->_checkAuth();
		$id = $_GET['id'];
		
		$models = array();
		$c = new CDbCriteria();
		$c->addCondition('idUser = '.$id);
		$roles = UserHasRoles::model()->findAll($c);
		if(count($roles) > 0)
		{
			$models = BuyOrderDocument::model()->findAll();
		} else {
			$provider = Provider::model()->findAll('idUser = '.$id);
			$models = BuyOrderDocument::model()->findAll('idBuyOrder IS NULL AND status NOT IN (1, 7) AND idCompany='.$provider[0]->idCompany);	
		}
		$this->_sendResponse(200, CJSON::encode($models));
	}

	public function actionUpdate()
	{
		$this->_checkAuth();

		if(!$this->actionCheckFront())
		{
			return;
		}
 
	    // Parse the PUT parameters. This didn't work: parse_str(file_get_contents('php://input'), $put_vars);
	    $json = Yii::app()->request->getRawBody(); //$GLOBALS['HTTP_RAW_POST_DATA'] is not preferred: http://www.php.net/manual/en/ini.core.php#ini.always-populate-raw-post-data
	    $put_vars = CJSON::decode($json,true);  //true means use associative array
	 
	    switch($_GET['model'])
	    {
	        // Find respective model
	        case 'posts':
	            $model = Post::model()->findByPk($_GET['id']);                    
				break;
			case 'buyOrder':
	            $model = BuyOrder::model()->findByPk($_GET['id']);                    
				break;		
			case 'factura':
	            $model = Factura::model()->findByPk($_GET['id']);                    
				break;
			case 'user':
	            $model = User::model()->findByPk($_GET['id']);                    
				break;	
			case 'company':
	            $model = Company::model()->findByPk($_GET['id']);                    
				break;
			case 'companyJurisdictionData':
				$model = CompanyJurisdictionData::model()->findByPk($_GET['id']);                    
				break;
			case 'buyOrderDocument':
	            $model = BuyOrderDocument::model()->findByPk($_GET['id']);                    
				break;
			case 'licitaciones':
	            $model = Licitaciones::model()->findByPk($_GET['id']);                    
				break;
			case 'licitacionConsulta':
	            $model = LicitacionesConsulta::model()->findByPk($_GET['id']);                    
				break;
			case 'licitacionProvider':
	            $model = LicitacionesProveedor::model()->findByPk($_GET['id']);                    
				break;
			case 'provider':
	            $model = Provider::model()->findByPk($_GET['id']);                    
				break;
			case 'providerDocument':
	            $model = ProviderDocument::model()->findByPk($_GET['id']);                    
				break;
			case 'generalData':
				$model = CompanyGeneralData::model()->findByPk($_GET['id']);                    
				break;
			case 'paymentData':
				$model = CompanyPaymentData::model()->findByPk($_GET['id']);                    
				break;
			case 'taxData':
				$model = CompanyTaxData::model()->findByPk($_GET['id']);                    
				break;
	        default:
	            $this->_sendResponse(501, 
	                sprintf( 'Error: Mode <b>update</b> is not implemented for model <b>%s</b>',
	                $_GET['model']) );
	            Yii::app()->end();
	    }
	    // Did we find the requested model? If not, raise an error
	    if($model === null)
	        $this->_sendResponse(400, 
	                sprintf("Error: Didn't find any model <b>%s</b> with ID <b>%s</b>.",
	                $_GET['model'], $_GET['id']) );
	 
	    // Try to assign PUT parameters to attributes
	    foreach($put_vars as $var=>$value) {
	        // Does model have this attribute? If not, raise an error
	        if($model->hasAttribute($var))
	            $model->$var = $value;
	        /*else {
	            $this->_sendResponse(500, 
	                sprintf('Parameter <b>%s</b> is not allowed for model <b>%s</b>',
	                $var, $_GET['model']) );
	        }*/
	    }
		// Try to save the model
	    if($model->save())
	        $this->_sendResponse(200, CJSON::encode($model));
	    else
	        // prepare the error $msg
	        // see actionCreate
			// ...
			foreach($model->errors as $attribute=>$attr_errors) {
	            foreach($attr_errors as $attr_error)
	                $msg .= $attr_error;
	        }
	        $this->_sendResponse(500, $msg );
	}




	public function actionDelete()
	{
		$this->_checkAuth();
		if(!$this->actionCheckFront())
		{
			return;
		}
 
		
	    switch($_GET['model'])
	    {
	        // Load the respective model
	        case 'posts':
	            $model = Post::model()->findByPk($_GET['id']);                    
				break;
			case 'factura':
	            $model = Factura::model()->findByPk($_GET['id']);                    
	            break;
            case 'company':
                $model = Company::model()->findByPk($_GET['id']);
				break;
			case 'licitacionesCompradores':
					$model = LicitacionesCompradores::model()->findByPk($_GET['id']);
					break;	
			case 'licitacionesConsultas':
                $model = LicitacionesConsulta::model()->findByPk($_GET['id']);
				break;				
			case 'rubrosCompany':
				$model = Rubros::model()->findByPk($_GET['id']);
				break;
	        default:
	            $this->_sendResponse(501, 
	                sprintf('Error: Mode <b>delete</b> is not implemented for model <b>%s</b>',
	                $_GET['model']) );
	            Yii::app()->end();
	    }
	    // Was a model found? If not, raise an error
	    if($model === null)
	        $this->_sendResponse(400, 
	                sprintf("Error: Didn't find any model <b>%s</b> with ID <b>%s</b>.",
	                $_GET['model'], $_GET['id']) );
	 
	    // Delete the model
	    $num = $model->delete();
	    if($num>0)
	        $this->_sendResponse(200, $num);    //this is the only way to work with backbone
	    else
	        $this->_sendResponse(500, 
	                sprintf("Error: Couldn't delete model <b>%s</b> with ID <b>%s</b>.",
	                $_GET['model'], $_GET['id']) );
	}

	public function actionPreflight() {
	        $content_type = 'application/json';
	        $status = 200;
	 
	        // set the status
	        $status_header = 'HTTP/1.1 ' . $status . ' ' . $this->_getStatusCodeMessage($status);
	        header($status_header);
	 
	        header("Access-Control-Allow-Origin: *");
	        header("Access-Control-Allow-Methods: GET, POST, PUT, OPTIONS");
			header("Access-Control-Allow-Headers: X-Authorization, Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");	 
	        //header("Access-Control-Allow-Headers: Authorization");
	        //header('Content-type: ' . $content_type);
	}

	private function _sendResponse($status = 200, $body = '', $content_type = 'application/json')
	{
	    // set the status
	    $status_header = 'HTTP/1.1 ' . $status . ' ' . $this->_getStatusCodeMessage($status);
	    header($status_header);
	    // and the content type
	    header('Content-type: ' . $content_type);
		header("Access-Control-Allow-Origin: *");
		header("Access-Control-Allow-Methods: GET, POST, PUT, OPTIONS");
		header("Access-Control-Allow-Headers: X-Authorization, Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");	 
	        
	    // pages with body are easy
	    if($body != '')
	    {
	        // send the body
	        echo $body;
	    }
	    // we need to create the body if none is passed
	    else
	    {
	        // create some body messages
	        $message = '';
	 
	        // this is purely optional, but makes the pages a little nicer to read
	        // for your users.  Since you won't likely send a lot of different status codes,
	        // this also shouldn't be too ponderous to maintain
	        switch($status)
	        {
	            case 401:
	                $message = 'You must be authorized to view this page.';
	                break;
	            case 404:
	                $message = 'The requested URL ' . $_SERVER['REQUEST_URI'] . ' was not found.';
	                break;
	            case 500:
	                $message = 'The server encountered an error processing your request.';
	                break;
	            case 501:
	                $message = 'The requested method is not implemented.';
	                break;
	        }
	 
	        // servers don't always have a signature turned on 
	        // (this is an apache directive "ServerSignature On")
	        $signature = ($_SERVER['SERVER_SIGNATURE'] == '') ? $_SERVER['SERVER_SOFTWARE'] . ' Server at ' . $_SERVER['SERVER_NAME'] . ' Port ' . $_SERVER['SERVER_PORT'] : $_SERVER['SERVER_SIGNATURE'];
	 
	        // this should be templated in a real-world solution
	        $body = '
	<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
	<html>
	<head>
	    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	    <title>' . $status . ' ' . $this->_getStatusCodeMessage($status) . '</title>
	</head>
	<body>
	    <h1>' . $this->_getStatusCodeMessage($status) . '</h1>
	    <p>' . $message . '</p>
	    <hr />
	    <address>' . $signature . '</address>
	</body>
	</html>';
	 
	        echo $body;
	    }
	    Yii::app()->end();
	}


	private function _getStatusCodeMessage($status)
	{
	    // these could be stored in a .ini file and loaded
	    // via parse_ini_file()... however, this will suffice
	    // for an example
	    $codes = Array(
	        200 => 'OK',
	        400 => 'Bad Request',
	        401 => 'Unauthorized',
	        402 => 'Payment Required',
	        403 => 'Forbidden',
	        404 => 'Not Found',
	        500 => 'Internal Server Error',
	        501 => 'Not Implemented',
	    );
	    return (isset($codes[$status])) ? $codes[$status] : '';
	}	


	private function _checkAuth()
	{
		if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {
			$this->actionPreflight();
			Yii::app()->end();
		}
	    // Check if we have the USERNAME and PASSWORD HTTP headers set?
	    if(!(isset($_SERVER['HTTP_X_AUTHORIZATION']))) {
	        // Error: Unauthorized
	        $this->_sendResponse(401);
		}
		$token = $_SERVER['HTTP_X_AUTHORIZATION'];
		// Find the user
		$user = Yii::app()->user;
		$responseToken = $user->checkLoginToken($token);
		if (isset($responseToken))
			$this->_sendResponse(401, $responseToken['errmsg']);
	}

	public function actionRefreshToken() {
		if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {
			$this->actionPreflight();
			Yii::app()->end();
		}
	    // Check if we have the USERNAME and PASSWORD HTTP headers set?
	    if(!(isset($_SERVER['HTTP_X_AUTHORIZATION']))) {
	        // Error: Unauthorized
	        $this->_sendResponse(401);
		}
		$token = $_SERVER['HTTP_X_AUTHORIZATION'];
		// Find the user
		$user = Yii::app()->user;
		$responseToken = $user->refreshToken($token);
		if (is_array($responseToken))
			$this->_sendResponse(401, CJSON::encode(['error' => $responseToken['errmsg']]));
		
		$this->_sendResponse(200, CJSON::encode(['token' => $responseToken]));
	}
}
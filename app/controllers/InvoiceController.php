<?php

    class InvoiceController extends AdminController
    {
    	/**
    	 * This is the default 'index' action that is invoked
    	 * when an action is not explicitly requested by users.
    	 */
    	public function actionIndex($export=false, $deleted = 0)
    	{
    		$model = new BuyOrder();
    		$model->unsetAttributes();

    		if (isset($_GET['Invoice'])){
    			$model->setAttributes($_GET['Invoice']);
    		}

    		if($export)
    		{
    			Yii::import('application.components.exportXls.ExportXLSHelper');
    			$helper = new ExportXLSHelper();
    			$helper->model = $model;
    			$helper->filename = 'invoices-';
    			$helper->header = 'Invoices al ';
    			$dp = $model->search();
    			$dp->pagination = false;
    			$helper->data = $dp->getData();
    			$helper->attributes = $model->getXlsAttributes();
    			$helper->generateSheet();
    			app()->end();
    		}
            
            $record = new BuyOrder();
    		$XMLform = new InvoiceXMLForm();	

    		if (isset($_POST['Invoice'])) 
            {    
                $record->attributes = $_POST['Invoice'];
                $invoice_xml = CUploadedFile::getInstance($record,'invoice_xml');
                            
                if (count($invoice_xml)>0) 
                {
                    $xml=simplexml_load_file($invoice_xml->tempName);
                    print "<pre>";
                    print_r($xml); 
                    print "</pre>";
                    exit;
                    app()->flashMessage->addMessage('success', "El archivo XML pudo cargarse");
                } 
                else 
                {
                    app()->flashMessage->addMessage('error', "El archivo XML no pudo cargarse");
                }        
                $this->redirect(array('index'));
            }

    		$this->render('index', array(
    			'model' => $model,
    			'record'=>$record,
                'XMLForm'=>$XMLform,
    			'deleted'=>$deleted
    		));
    	}

    	public function actionChangeAttributeInvoice()
    	{
    		Yii::import('bootstrap.widgets.TbEditableSaver');
    		$es = new TbEditableSaver('BuyOrder');
    	    $es->update();
    	}

    	public function actionDelete($id, $key)
    	{
    		$model = BuyOrder::model()->findByPk(CPropertyValue::ensureInteger($id));
    		if (Yii::app()->getRequest()->getIsPostRequest() && $model instanceof Invoice && 
    			strcmp($key, uKey('deletePart'.$model->primaryKey))==0) 
    		{			
    			try
    			{
    				$model->delete();
    			}
    			catch (Exception $e)
    			{
    				$model->participantDeleted = 1;
    				$model->save(false, array('participantDeleted'));
    			}
    			
    			if (!Yii::app()->getRequest()->getIsAjaxRequest())
    			{
    				$this->redirect(array('index'));
    			}
    		}
    		else
    		{
    			throw new CHttpException(400, Yii::t('app', 'Your request is invalid.'));
    		}
    	}
    	
        public function actionCreate(){
            $model = new BuyOrder();

            if(isset($_POST['Invoice'])){
                $model->attributes=$_POST['Invoice'];
                if($model->save()){
                    $this->redirect(array('index'));
                }
            }
            $this->render('form', array('model'=>$model));
        }

    	/**
    	 * Updates a participant by $id
    	 */ 
    	public function actionUpdate($id, $key)
    	{
    		$model = BuyOrder::model()->findByPk(CPropertyValue::ensureInteger($id));

    		if($model instanceof BuyOrder && strcmp($key, uKey('updatePart'.$model->primaryKey))==0)
    		{
    			if(isset($_POST['Invoice']))
    	       	{
    		        $model->attributes=$_POST['Invoice'];
    		        if($model->save())
    		        {
    		            $this->redirect(array('index'));
    		        }
    	        }
    			$this->render('form', array('model'=>$model));
    			return;
    		}
    		
    		throw new CHttpException(404, "Invoice inválido");
    	}

    	/**
    	 * View
    	 * @param int $idInvoice
    	 */
    	public function actionView($invoice)
    	{
    		$model = BuyOrder::model()->findByPk(CPropertyValue::ensureInteger($invoice));

    		if($model instanceof BuyOrder)
    		{
    			$this->render('view', array('model'=>$model, 'invoice'=>$invoice));
    			app()->end();
    		}

    		throw new CHttpException(404, Yii::t('app', 'Invalid invoice'));
    	}

        // on your controller
        // example code for action rendering the relational data
        public function actionRelational()
        {
            // partially rendering "_relational" view
            $this->renderPartial('_relational', array(
                'id' => Yii::app()->getRequest()->getParam('id'),
                'gridDataProvider' => $this->getGridDataProvider(),
                'gridColumns' => $this->getGridColumns()
            ));
        }

    }
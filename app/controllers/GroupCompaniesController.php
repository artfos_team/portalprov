<?php

class GroupCompaniesController extends AdminController
{
	/**
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
	 */
    public function actionIndex(){
		$model = new GroupCompanies();
		$model->unsetAttributes();

		if(isset($_GET['GroupCompanies']))
		{
			$model->attributes = $_GET['GroupCompanies'];
		}

		$this->render('index', array(
			'model' => $model,
		));
    }
    
	public function actionCreate()
	{
		$model = new GroupCompanies();
		
		if(isset($_POST['GroupCompanies']))
       	{
            $model->attributes=$_POST['GroupCompanies'];
	        if($model->save())
	        {
	            $this->redirect(array('index'));
	        }
        }
		$this->render('form', array('model'=>$model));
    }
    
	public function actionChangeAttributeManual()
	{
		Yii::import('bootstrap.widgets.TbEditableSaver');
		$es = new TbEditableSaver('GroupCompanies');
	    $es->update();
    }

    public function actionDelete($id, $key) {
		$model = GroupCompanies::model()->findByPk(CPropertyValue::ensureInteger($id));

		if ($model instanceof GroupCompanies) 
		{			
			try {
				$model->delete($id);
			} catch (Exception $e) {
				throw new Exception($e);
				
				throw new CHttpException(500, Yii::t('app', 'The Group Company is being used and cannot be deleted'));
			}
			if (!Yii::app()->getRequest()->getIsAjaxRequest())
			{
				$this->redirect(array('index'));
			}
		} else{
			throw new CHttpException(400, Yii::t('app', 'Your request is invalid.'));
		}
	}    

}
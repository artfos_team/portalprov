<?php

class JurisdictionsController extends AdminController
{
	/**
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
	 */
    public function actionIndex(){
		$model = new Jurisdictions();
		$model->unsetAttributes();

		if(isset($_GET['jurisdictions']))
		{
			$model->attributes = $_GET['Jurisdictions'];
		}

		$this->render('index', array(
			'model' => $model,
		));
    }
    
	public function actionCreate()
	{
		$model = new Jurisdictions();
		
		if(isset($_POST['Jurisdictions']))
       	{
            $model->attributes=$_POST['Jurisdictions'];
	        if($model->save())
	        {
	            $this->redirect(array('index'));
	        }
        }
		$this->render('form', array('model'=>$model));
    }
    
	public function actionChangeAttributeManual()
	{
		Yii::import('bootstrap.widgets.TbEditableSaver');
		$es = new TbEditableSaver('Jurisdictions');
	    $es->update();
    }

    public function actionDelete($id, $key) {
		$model = Jurisdictions::model()->findByPk(CPropertyValue::ensureInteger($id));

		if ($model instanceof Jurisdictions) 
		{			
			try {
				$model->delete($id);
			} catch (Exception $e) {
				throw new Exception($e);
				
				throw new CHttpException(500, Yii::t('app', 'The jurisdictions is being used and cannot be deleted'));
			}
			if (!Yii::app()->getRequest()->getIsAjaxRequest())
			{
				$this->redirect(array('index'));
			}
		} else{
			throw new CHttpException(400, Yii::t('app', 'Your request is invalid.'));
		}
	}    

}
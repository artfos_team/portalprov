<?php

class BankAccountTypeController extends AdminController
{
	/**
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
	 */
    public function actionIndex(){
		$model = new BankAccountType();
		$model->unsetAttributes();

		if(isset($_GET['BankAccountType']))
		{
			$model->attributes = $_GET['BankAccountType'];
		}

		$this->render('index', array(
			'model' => $model,
		));
    }
    
	public function actionCreate()
	{
		$model = new BankAccountType();
		
		if(isset($_POST['BankAccountType']))
       	{
            $model->attributes=$_POST['BankAccountType'];
	        if($model->save())
	        {
	            $this->redirect(array('index'));
	        }
        }
		$this->render('form', array('model'=>$model));
    }
    
	public function actionChangeAttributeManual()
	{
		Yii::import('bootstrap.widgets.TbEditableSaver');
		$es = new TbEditableSaver('BankAccountType');
	    $es->update();
    }

    public function actionDelete($id, $key) {
		$model = BankAccountType::model()->findByPk(CPropertyValue::ensureInteger($id));

		if ($model instanceof BankAccountType) 
		{			
			try {
				$model->delete($id);
			} catch (Exception $e) {
				throw new Exception($e);
				
				throw new CHttpException(500, Yii::t('app', 'The bankAccountType is being used and cannot be deleted'));
			}
			if (!Yii::app()->getRequest()->getIsAjaxRequest())
			{
				$this->redirect(array('index'));
			}
		} else{
			throw new CHttpException(400, Yii::t('app', 'Your request is invalid.'));
		}
	}    

}
<?php

/**
 * Created by PhpStorm.
 * User: pgallar
 * Date: 13/7/17
 * Time: 8:48
 */
class ApiClassFactory
{
	public function getApiModel($modelName) {

		if (file_exists(Yii::getPathOfAlias('application.models').DIRECTORY_SEPARATOR.$modelName.'.php'))
		{
			return $modelName::model();
		}

		throw new Exception("El modelo indicado no existe ");
	}
}
<?php
/**
 * Created by PhpStorm.
 * User: rominacostas
 * Date: 17/1/18
 * Time: 13:31
 */

class IndividualController extends ApiController
{
	/**
	 * Key which has to be in HTTP USERNAME and PASSWORD headers
	 */
	Const APPLICATION_ID = 'IPROVEEDORES';

	/**
	 * Default response format
	 * either 'json' or 'xml'
	 */
	private $format = 'json';

	public function actionList()
	{
		$this->_sendResponse(200, CJSON::encode(array('individuals' => [])));
	}

	/**
	 * @param string $rut
	 * @param string $typeDoc
	 * @param int $numberInvoice
	 * @param string $dateCreated
	 * @param number $amount
	 */

	 public function actionListByAnonymous()
	 {
		 //parametros por url
		 if(!isset($_GET['rut']))
			 $this->_sendResponse(200, CJSON::encode(array('error'=> array('name'=>'InvalidArgumentError' ) ,'Explanation'=> 'Error: Parameter rut is missing') ) );  
		 if(!isset($_GET['numberInvoice']))
			 $this->_sendResponse(200, CJSON::encode(array('error'=> array('name'=>'InvalidArgumentError' ) ,'Explanation'=> 'Error: Parameter numberInvoice is missing') ) ); 
		 if(!isset($_GET['dateEmission']))
			 $this->_sendResponse(200, CJSON::encode(array('error'=> array('name'=>'InvalidArgumentError' ) ,'Explanation'=> 'Error: Parameter dateEmission is missing') ) ); 
		 if(!isset($_GET['amount']))
			 $this->_sendResponse(200, CJSON::encode(array('error'=> array('name'=>'InvalidArgumentError' ) ,'Explanation'=> 'Error: Parameter invoiceTotalAmount is missing') ) ); 
 
 
		 $c = new CDbCriteria();
		 //$c->addSearchCondition('t.idCompanyProvider', $_GET['idCompanyProvider']);//"t." Alias asignada al 'invoice' 
		 //$c->join = "Select cuit from company INNER JOIN invoice ON company.idCompany=invoice.idCompanyProvider";		
		 $c->addCondition('t.numberInvoice = "'. $_GET['numberInvoice'].'"');
		 $c->addCondition('t.dateEmission = "'. $_GET['dateEmission'].'"');
		 $c->addCondition('t.invoiceTotalAmount = "'. $_GET['amount'].'"');
		 $c->with = array('companyProvider');
		 $c->addCondition('companyProvider.cuit = "'. $_GET['rut'].'"');
 
		 try {
			  $model = Invoice::model()->findAll($c); 
 // 			$model = Company::model()->findAll($c);       	
		 }
		 catch(Exception $ex) {
			 echo CJavaScript::jsonEncode(array('error' => $ex->getMessage()));
		 }
 
		 if(is_null($model)) {
			 $this->_sendResponse(200, CJSON::encode(array('error'=> array('name'=>'NotFoundError' ) ,'Explanation'=> 'Error: Invoice not found') ) ); 
		 }
		 else {
			 error_log('cantidad: ' . count($model) );
			 if (count($model)===1){
				 $data = array (
				  'rut'=> $model[0]->companyProvider->cuit,
				  'companyClientName' => $model[0]->companyClient->companyName,
				  'numberInvoice'=> $model[0]->numberInvoice,
				  'dateEmission'=> $model[0]->dateEmission,
				  'datePayment'=> $model[0]->datePayment, 
				  'invoiceStatus' => $model[0]->invoiceStatus->description,
				  //'resp'=> $_GET['rut'],
				 );
 
				 $this->_sendResponse(200, CJSON::encode($data));
			 } else {
				 if (count($model)===0){
					 $this->_sendResponse(200, CJSON::encode(array('error'=> array('name'=>'NotFoundError' ) ,'Explanation'=> 'Error: Invoices not found') ) ); 
				 }
			 }
		 }
	 }
	 
	/*public function actionListByAnonymous() {

		$rut = $_POST['rut'];
		$typeDoc = $_POST['typeDoc'];
		$numberInvoice = $_POST['numberInvoice'];
		$dateCreated = $_POST['dateCreated'];
		$amount = $_POST['amount'];

		var_dump($_GET);

		if (is_string($rut) &&
			is_string($typeDoc) &&
			ctype_digit($numberInvoice) &&
			is_string($dateCreated) &&
			ctype_digit($amount)) {

			if ($this->validateDate($dateCreated)) {
				$invoices = Invoice::model()->findAllByAttributes(array(
					'numberInvoice' => $numberInvoice,
					'dateEmission' => $dateCreated,
					'invoiceTotalAmount' => $amount
				));

				$invoicesFound = array();

				
				  @var Invoice $invoice
				 
				foreach ($invoices as $invoice) {

					if ($invoice->companyProvider->cuit == $rut) {
						$invoicesFound[] = $invoice;
					}
				}

				$this->_sendResponse(200, CJSON::encode(array('individuals' => $invoicesFound)));
			}
			else {
				$this->_sendResponse(404, CJSON::encode(array('error' => 'Invalid date')));
			}
		}
		else {
			//$this->_sendResponse(404, CJSON::encode(array('error' => 'Invalid data')));
		}
	}*/

	private function validateDate($date, $format = 'Y-m-d')
	{
		$d = DateTime::createFromFormat($format, $date);
		return $d && $d->format($format) == $date;
	}
}
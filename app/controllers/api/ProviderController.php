<?php
/**
 * Created by PhpStorm.
 * User: pgallar
 * Date: 17/1/18
 * Time: 13:31
 */

class ProviderController extends ApiController
{
	/**
	 * Key which has to be in HTTP USERNAME and PASSWORD headers
	 */
	Const APPLICATION_ID = 'IPROVEEDORES';

	public function actionList() {

		$providers = Company::model()->findAllWithOrder()->findAll();

		$this->_sendResponse(200, CJSON::encode(array('providers' => $providers)));
	}

	private function validateDate($date, $format = 'Y-m-d')
	{
		$d = DateTime::createFromFormat($format, $date);
		return $d && $d->format($format) == $date;
	}
}
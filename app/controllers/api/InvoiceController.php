<?php
/**
 * Created by PhpStorm.
 * User: pgallar
 * Date: 17/1/18
 * Time: 13:31
 */

class InvoiceController extends ApiController
{
	/**
	 * Key which has to be in HTTP USERNAME and PASSWORD headers
	 */
	Const APPLICATION_ID = 'IPROVEEDORES';

	/**
	 * @param string $tipoFecha
	 * @param string $desde
	 * @param string $hasta
	 * @param string $estado
	 * @param string $cliente
	 * @param string $referencia
	 */
	public function actionListByFilters() {

		$tipoFecha = $_POST['tipoFecha'];
		$desde = $_POST['desde'];
		$hasta = $_POST['hasta'];
		$estado = $_POST['estado'];
		$cliente = $_POST['cliente'];
		$referencia = $_POST['referencia'];

		if (is_string($tipoFecha) &&
			is_string($desde) &&
			is_string($hasta) &&
			is_string($estado) &&
			is_string($cliente) &&
			is_string($referencia)) {

			if ($this->validateDate($desde) && $this->validateDate($hasta)) {

				$condition = 'idStatusInvoice IN '. ($status == '0' ? '(1, 2, 3, 4, 5, 6)' : "({$status})");

				if ($tipoFecha == 'emision') {
					$dateFilter = 'dateEmission';
				}
				else {
					$dateFilter = 'datePayment';
				}

				if (!empty($desde) && !empty($hasta)) {
					$condition .= " AND {$dateFilter} BETWEEN '{$desde}' AND '{$hasta}'";
				}
				elseif (!empty($hasta)) {
					$condition .= " AND {$dateFilter} <= '{$hasta}'";
				}
				elseif (!empty($desde)) {
					$condition .= " AND {$dateFilter} >= '{$desde}'";
				}

				if (!empty($cliente)) {
					$condition .= " AND idProvider = {$cliente}";
				}

				if (!empty($referencia)) {
					$condition .= " AND numberInvoice = {$referencia}";
				}

				$invoices = Invoice::model()->findAll($condition);

				$this->_sendResponse(200, CJSON::encode(array('invoices' => $invoices)));
			}
			else {
				$this->_sendResponse(404, CJSON::encode(array('error' => 'Invalid dates')));
			}
		}
		else {
			//$this->_sendResponse(404, CJSON::encode(array('error' => 'Invalid data')));
		}
	}

	private function validateDate($date, $format = 'Y-m-d')
	{
		$d = DateTime::createFromFormat($format, $date);
		return $d && $d->format($format) == $date;
	}
}
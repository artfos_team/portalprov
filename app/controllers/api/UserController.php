<?php
/**
 * Created by PhpStorm.
 * User: rominacostas
 * Date: 26/1/18
 * Time: 9:57
 */

class UserController extends ApiController
{

	public function actionLogin() {

		$model = new LoginForm();
		$model->username = Yii::app()->request->getParam('username', null);
		$model->password = Yii::app()->request->getParam('password', null);
		$model->rememberMe = true;

		if ($model->validate())
		{
			$token = $model->login(true);

			if (isset($token)) {
				/* @var $user WebUser */
				$user = user();
				$user->updateLastLoginAt();

				$this->_sendResponse(200, CJSON::encode(array('token' => $token)));
			}
			else {
				$this->_sendResponse(200, CJSON::encode(array('error' => 'Invalid user and password')));
			}
		}
		else {
			$this->_sendResponse(200, CJSON::encode(array('error' => $model->getError('password'))));
		}
	}
}
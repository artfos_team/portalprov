<?php

require_once dirname(__FILE__) . '/../../vendor/autoload.php';

Yii::import('app.models.import.FileFormat');
Yii::import('app.models.import.FileColumn');

class ImportsController extends AdminController
{

    /**
     * This is the default 'index' action that is invoked
     * when an action is not explicitly requested by users.
     */
    public function actionIndex()
    {
        $model = new Import();
        $model->unsetAttributes();

        if (isset($_GET['Import'])) {
            $model->setAttributes($_GET['Import']);
        }

        $this->render('index', array(
            'model' => $model,
        ));
    }

    public function actionErrors()
    {
        $model = new ImportError();
        $model->unsetAttributes();

        if (isset($_GET['ImportError'])) {
            $model->setAttributes($_GET['ImportError']);
        }

        $this->render('errors', array(
            'model' => $model,
        ));
    }

    public function actionChangeAttributeImportError()
    {
        Yii::import('bootstrap.widgets.TbEditableSaver');
        $es = new TbEditableSaver('ImportError');
        $es->update();
    }

    public function actionCreate()
    {
        $model = new EmailParameters();

        if (isset($_POST['EmailParameters'])) {
            $model->attributes = $_POST['EmailParameters'];
            if ($model->save()) {
                $this->redirect(array('index'));
            }
        }
        $this->render('create', array('model' => $model));
    }

    /**
     * Updates a participant by $id
     */
    public function actionUpdate($id, $key)
    {
        $model = EmailParameters::model()->findByPk(CPropertyValue::ensureInteger($id));

        if ($model instanceof EmailParameters && strcmp($key, uKey('updatePart' . $model->primaryKey)) == 0) {
            if (isset($_POST['EmailParameters'])) {
                $model->attributes = $_POST['EmailParameters'];
                if ($model->save()) {
                    $this->redirect(array('index'));
                }
            }
            $this->render('form', array('model' => $model));
            return;
        }

        throw new CHttpException(404, "Invoicestatus inválido");
    }

    /**
     * View
     * @param int $idInvoice
     */
    public function actionView($invoiceStatus)
    {
        $model = SapParameters::model()->findByPk(CPropertyValue::ensureInteger($invoiceStatus));

        if ($model instanceof SapParameters) {
            $this->render('view', array('model' => $model, 'SapParameters' => $invoice));
            app()->end();
        }

        throw new CHttpException(404, Yii::t('app', 'Invalid invoice'));
    }

    // on your controller
    // example code for action rendering the relational data
    public function actionRelational()
    {
        // partially rendering "_relational" view
        $this->renderPartial('_relational', array(
            'id' => Yii::app()->getRequest()->getParam('id'),
            'gridDataProvider' => $this->getGridDataProvider(),
            'gridColumns' => $this->getGridColumns()
        ));
    }

    public function actionPing()
    {
        $models = SapParameters::model()->findAll();
        $host = $models[0]->ashost;

        $comando = "ping -c 1 " . $host;
        $output = shell_exec($comando);

        if (strpos($output, '1 received')) {
            app()->flashMessage->addMessage('success', Yii::t('app', 'Hay conexión con el servidor'));
            $this->redirect(array('index'));
        } else {
            app()->flashMessage->addMessage('error', Yii::t('app', 'No hay conexión con el servidor'));
            $this->redirect(array('index'));
        }
    }


    public function actionChangePsw()
    {
        $models = SapParameters::model()->findAll();

        if (isset($_POST['psw'])) {
            $models[0]->passwd = $models[0]->encriptar($_POST['psw']);
        }


        if ($models[0]->save()) {
            app()->flashMessage->addMessage('success', Yii::t('app', 'Se actualizo la contraseña correctamente'));
            $this->redirect(array('index'));
        } else {
            app()->flashMessage->addMessage('error', Yii::t('app', 'No se pudo actualizar la contraseña'));
            $this->redirect(array('index'));
        }
    }

    // FORMATS
    public function actionFormats()
    {
        $model = new FileFormat();
        $model->unsetAttributes();

        if (isset($_GET['FileFormat'])) {
            $model->setAttributes($_GET['FileFormat']);
        }

        $this->render('formats', array(
            'model' => $model,
        ));
    }

    public function actionNewformat()
    {
        $model = new FileFormat();
        $this->displayFormatForm($model);
    }

    public function actionEditFormat($id)
    {
        if (isset($id)) {
            $model = FileFormat::model()->findByPk($id);

            if (!isset($model)) {
                throw new Exception('Formato no encontrado por id: '. $id);
            }

            $this->displayFormatForm($model, true);
        }
        else {
            throw new Exception('Id formato invalido: '. $id);
        }
    }

    public function actionDeleteFormat($id)
    {
        if (isset($id)) {
            $model = FileFormat::model()->findByPk($id);

            if (isset($model)) {
                $model->delete();
            }
        }
    }

    /**
     * @param FileFormat $model
     * @param boolean $update
     */
    private function displayFormatForm($model, $update = false) {

        if (isset($_POST['FileFormat'])) {
            $model->attributes = $_POST['FileFormat'];

            if (!isset($_POST['FileFormat']['columns']) || empty($_POST['FileFormat']['columns']) || empty($_POST['FileFormat']['columns'][0])) {
                $model->addError('idFileFormat', 'Se deben agregar columnas al formato');
            } else {
                $model->columns = $_POST['FileFormat']['columns'];

                if ($model->save()) {
                    Yii::app()->flashMessage->addMessage('success', Yii::t('app', 'Formato guardado correctamente'));
                    $this->redirect(array('formats'));
                }
            }
        }

        $this->render('newformat', array('model' => $model, 'update' => $update));
    }
}
        
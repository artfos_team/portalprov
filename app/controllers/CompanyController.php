<?php

class CompanyController extends AdminController
{
	/**
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
	 */
	public function actionIndex($export=false){
		$model = new Company();
		$model->unsetAttributes();

		if(isset($_GET['Company']))
		{
			$model->attributes = $_GET['Company'];
		}

		if($export)
		{
			Yii::import('application.components.exportXls.ExportXLSHelper');
			$helper = new ExportXLSHelper();
			$helper->model = $model;
			$helper->filename = 'empresas-';
			$helper->header = 'Empresas al ';
			$dp = $model->search();
			$dp->pagination = false;
			$helper->data = $dp->getData();
			$helper->attributes = $model->getXlsAttributes();
			$helper->generateSheet();
			app()->end();
		}

		$this->render('index', array(
			'model' => $model,
		));
	}

	public function actionChangeAttributeCompany()
	{
		Yii::import('bootstrap.widgets.TbEditableSaver');
		$es = new TbEditableSaver('Company');
	    $es->update();
	}

	public function actionDelete($id, $key) {
		$model = Company::model()->findByPk(CPropertyValue::ensureInteger($id));

		if (Yii::app()->getRequest()->getIsPostRequest() && $model instanceof Company &&
			strcmp($key, uKey('deleteComp'.$model->primaryKey)) == 0) 
		{			
			try {
				$model->delete();
			} catch (Exception $e) {
				throw new CHttpException(500, Yii::t('app', 'The company is being used and cannot be deleted'));
			}
			if (!Yii::app()->getRequest()->getIsAjaxRequest())
			{
				$this->redirect(array('index'));
			}
		} else{
			throw new CHttpException(400, Yii::t('app', 'Your request is invalid.'));
		}
	}

	/**
	 * Creates a new Company
	 */ 
	public function actionCreate()
	{
		$model = new Company();
		
		if(isset($_POST['Company']))
       	{
	        $model->attributes=$_POST['Company'];
	        $model->_logo=CUploadedFile::getInstance($model,'_logo');
	        if($model->save())
	        {
	            $this->redirect(array('index'));
	        }
        }
		$this->render('form', array('model'=>$model));
	}


	/**
	 * updates a company $id
	 */ 
	public function actionUpdate($id, $key)
	{
		$model = Company::model()->findByPk(CPropertyValue::ensureInteger($id));
		
		if($model instanceof Company && strcmp($key, uKey('updateComp'.$model->primaryKey))==0)
		{
			if(isset($_POST['Company']))
	       	{
		        $model->attributes=$_POST['Company'];
		        if($model->save())
		        {
		            $this->redirect(array('index'));
		        }
	        }
			$this->render('form', array('model'=>$model, 'update'=>true));
			return;
		}
		
		throw new CHttpException(404, "Empresa inválida");
	}


	public function actions()
	{
		return array(
			'toggle' => array(
				'class'=>'bootstrap.actions.TbToggleAction',
				'modelName' => 'Company',
			),
		);
	}


	public function actionViewCalification($company)
	{
		$model = Company::model()->findByPk(CPropertyValue::ensureInteger($company));
		if(isset($_POST['Company']))
		{
			$model->attributes=$_POST['Company'];
			$model->reasonCalification = $_POST['Company']['reasonCalification'];
			if($model->save())
			{
				$this->redirect(array('index'));
			}
		}
		if($model instanceof Company)
		{
			$this->render('viewCalification', array('model'=>$model, 'company'=>$company));
			app()->end();
		}

		throw new CHttpException(404, Yii::t('app', 'Invalid company'));
	}


	/**
	 * Returns the provinces of the current country
	 */
	public function actionUpdateProvince($model = null)
	{
		$data = Province::model()->findAll(array('condition'=>'idCountry=:idCountry', 
			'order'=>'province ASC',
		    'params'=>array(':idCountry'=>(int) $_POST[$model]['idCountry'])));

		$data=CHtml::listData($data,'idProvince','province');
		
		echo CHtml::tag('option', array('value'=>''), Yii::t('app','Select an option'), true);
		foreach($data as $value=>$name)
		{
			echo CHtml::tag('option',
		       array('value'=>$value), CHtml::encode($name), true);
		}
	}

	/**
	 * Returns the cities of the current province
	 */
	public function actionUpdateCity($model = null)
	{
		$data = City::model()->findAll(array('condition'=>'idProvince=:idProvince', 
			'order'=>'city ASC',
		    'params'=>array(':idProvince'=>(int) $_POST[$model]['idProvince'])));

		$data=CHtml::listData($data,'idCity','city');
		
		echo CHtml::tag('option', array('value'=>''), Yii::t('app','Select an option'), true);
		foreach($data as $value=>$name)
		{
			echo CHtml::tag('option',
		       array('value'=>$value), CHtml::encode($name), true);
		}
	}

	/**
	 * View
	 * @param int $idParticipant
	 */
	public function actionView($company)
	{
		$model = Company::model()->findByPk(CPropertyValue::ensureInteger($company));

		if($model instanceof Company)
		{
			
			$this->render('view', array('model'=>$model, 'company'=>$company));
			app()->end();
		}

		throw new CHttpException(404, Yii::t('app', 'Invalid company'));
	}
	public function actionViewDocument($company)
	{
		
		
		$model = new ProviderDocument();
		if(isset($_POST['ProviderDocument']))
		{
			$Documents = Document::model()->findAll();
		
			
			for($i = 0; $i < count($Documents); $i++)
			{
				$model = ProviderDocument::model()->find("idProvider ='".$company."' and idDocument='".$Documents[$i]->idDocument."'");
				if(isset($model))
				{
					if(isset($_POST['Document'.$Documents[$i]->idDocument]) && $_POST['Document'.$Documents[$i]->idDocument] == 1)
					{
						$model->status = $_POST['Document'.$Documents[$i]->idDocument];
					}else{
						$model->status = 0;
					}
				}
				else
				{
					$model = new ProviderDocument();
					$model->idProvider = $_POST['ProviderDocument']['idProvider'];
					$model->date = date('Y-m-d');
					//echo var_dump($_POST);
					$model->idDocument= $Documents[$i]->idDocument;
					if(isset($_POST['Document'.$Documents[$i]->idDocument]) && $_POST['Document'.$Documents[$i]->idDocument] == 1)
					{
						$model->status = $_POST['Document'.$Documents[$i]->idDocument];
					}else{
						$model->status = 0;
					}
				}
				$model->save();
				$model = NULL;
			}
			
			
			$this->redirect(array('index'));
		}
		if($model instanceof ProviderDocument)
		{
			$this->render('viewDocument', array('model'=>$model, 'company'=>$company));
			app()->end();
		}
		
		throw new CHttpException(404, Yii::t('app', 'Invalid company'));
	}
}
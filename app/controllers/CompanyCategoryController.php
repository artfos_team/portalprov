<?php

/**
 * This is the model class for table "company_category".
 *
 * The followings are the available columns in table 'company_category':
 * @property integer $idCompanyCategory
 * @property string $companyCategory
 *
 * The followings are the available model relations:
 * @property Company[] $companies
 */
class CompanyCategory extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return CompanyCategory the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'company_category';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('companyCategory', 'required'),
			array('companyCategory', 'length', 'max'=>255),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('idCompanyCategory, companyCategory', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'companies' => array(self::HAS_MANY, 'Company', 'idCompanyCategory'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'idCompanyCategory' => Yii::t('app', 'Id Company Category'),
			'companyCategory' => Yii::t('app', 'Company Category'),
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('idCompanyCategory',$this->idCompanyCategory);
		$criteria->compare('companyCategory',$this->companyCategory,true);

		$sort = new CSort();
		$sort->defaultOrder = 'companyCategory ASC';

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'sort'=>$sort,
			'pagination'=>array(
				'pageSize'=>app()->controller->getItemsPerPage()
			),
		));
	}

	public function getXlsAttributes()
	{
		return array(
			array('attribute'=>'companyCategory'),
		);
	}
}
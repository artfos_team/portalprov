<?php

class BankBranchController extends AdminController
{
	/**
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
	 */
    public function actionIndex(){
		$model = new BankBranch();
		$model->unsetAttributes();

		if(isset($_GET['BankBranch']))
		{
			$model->attributes = $_GET['BankBranch'];
		}

		$this->render('index', array(
			'model' => $model,
		));
    }
    
	public function actionCreate()
	{
		$model = new BankBranch();
		
		if(isset($_POST['BankBranch']))
       	{
            $model->attributes=$_POST['BankBranch'];
	        if($model->save())
	        {
	            $this->redirect(array('index'));
	        }
        }
		$this->render('form', array('model'=>$model));
    }
    
	public function actionChangeAttributeManual()
	{
		Yii::import('bootstrap.widgets.TbEditableSaver');
		$es = new TbEditableSaver('BankBranch');
	    $es->update();
    }

    public function actionDelete($id, $key) {
		$model = BankBranch::model()->findByPk(CPropertyValue::ensureInteger($id));

		if ($model instanceof BankBranch) 
		{			
			try {
				$model->delete($id);
			} catch (Exception $e) {
				throw new Exception($e);
				
				throw new CHttpException(500, Yii::t('app', 'The bankBranch is being used and cannot be deleted'));
			}
			if (!Yii::app()->getRequest()->getIsAjaxRequest())
			{
				$this->redirect(array('index'));
			}
		} else{
			throw new CHttpException(400, Yii::t('app', 'Your request is invalid.'));
		}
	}    

}
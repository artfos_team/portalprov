<?php

class CommentsController extends Controller
{
	/**
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
	 */
	public function actionIndex($export=false)
	{
		$model = new Comments();
		$model->unsetAttributes();

		if (isset($_GET['Comments'])){
			$model->setAttributes($_GET['Comments']);
		}

        $record = new Comments();

		if (isset($_POST['Comments'])) 
        {    
            $record->attributes = $_POST['Comments'];
            $this->redirect(array('index'));
        }

		$this->render('index', array(
			'model' => $model,
			'record'=>$record,
		));
	}

	/**
	 * View
	 * @param int $comments
	 */
	public function actionView($comments)
	{
		$model = Comments::model()->findByPk(CPropertyValue::ensureInteger($comments));

		if($model instanceof Comments)
		{
			$this->render('view', array('model'=>$model, 'comments'=>$comments));
			app()->end();
		}

		throw new CHttpException(404, Yii::t('app', 'Invalid comments'));
	}

    // on your controller
    // example code for action rendering the relational data
    public function actionRelational()
    {
        // partially rendering "_relational" view
        $this->renderPartial('_relational', array(
            'id' => Yii::app()->getRequest()->getParam('id'),
            'gridDataProvider' => $this->getGridDataProvider(),
            'gridColumns' => $this->getGridColumns()
        ));
    }

    public function actionChangeAttributeComment()
	{
		Yii::import('bootstrap.widgets.TbEditableSaver');
		$es = new TbEditableSaver('comments');
	    $es->update();
	}

	public function actionSaveComment($comments)
	{
		
		$levelLog = 1;
		$commentaryLog = 'Se respondió un comentario';
		$source = 'CommentController->actionSaveComment';

    	$record = new Comments();

		if (isset($_POST['Comments'])) 
        {    
            $record->attributes = $_POST['Comments']; 
        }

    	$newComment = new Comments();
        $newComment->description = $record->description;
        $newComment->idRefenceTo = $comments;
       	$newComment->idCommentType = 2;
       	$newComment->idUser = user()->idUser;
       	$newComment->statusComments = 1;

        if ($newComment->save())
        {     
            Logs::model()->log($levelLog,$commentaryLog,$source);
            echo CJSON::encode(array('success'=>true));
            Yii::app()->end();  
        }

        echo CActiveForm::validate($newComment);
        Yii::app()->end();

        $this->renderPartial('view', 
            array('comments'=>$comments), 
            false, true);
        app()->end();
		
		
	}

	public function actionSendComment()
	{
		
		$levelLog = 1;
		$commentaryLog = 'Se envió un comentario';
		$source = 'CommentController->actionSendComment';

    	$record = new Comments();

		if (isset($_POST['Comments'])) 
        {    
            $record->attributes = $_POST['Comments']; 
        }

    	$newComment = new Comments();
        $newComment->description = $record->description;
        $newComment->idRefenceTo = $record->idRefenceTo;
       	$newComment->idCommentType = 3;
       	$newComment->idUser = user()->idUser;
       	$newComment->statusComments = 1;

        if ($newComment->save())
        {     
            Logs::model()->log($levelLog,$commentaryLog,$source);
            echo CJSON::encode(array('success'=>true));
            Yii::app()->end();  
        }

        echo CActiveForm::validate($newComment);
        Yii::app()->end();

		app()->flashMessage->addMessage('success', Yii::t('app', 'Mensaje enviado con éxito'));
        $this->renderPartial('index', 
            array(), 
            false, true);
        app()->end();
		
		
	}

}

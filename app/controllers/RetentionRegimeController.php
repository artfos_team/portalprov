<?php

class RetentionRegimeController extends AdminController
{
	/**
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
	 */
    public function actionIndex(){
		$model = new RetentionRegime();
		$model->unsetAttributes();

		if(isset($_GET['RetentionRegime']))
		{
			$model->attributes = $_GET['RetentionRegime'];
		}

		$this->render('index', array(
			'model' => $model,
		));
    }
    
	public function actionCreate()
	{
		$model = new RetentionRegime();
		
		if(isset($_POST['RetentionRegime']))
       	{
            $model->attributes=$_POST['RetentionRegime'];
	        if($model->save())
	        {
	            $this->redirect(array('index'));
	        }
        }
		$this->render('form', array('model'=>$model));
    }
    
	public function actionChangeAttributeManual()
	{
		Yii::import('bootstrap.widgets.TbEditableSaver');
		$es = new TbEditableSaver('RetentionRegime');
	    $es->update();
    }

    public function actionDelete($id, $key) {
		$model = RetentionRegime::model()->findByPk(CPropertyValue::ensureInteger($id));

		if ($model instanceof RetentionRegime) 
		{			
			try {
				$model->delete($id);
			} catch (Exception $e) {
				throw new Exception($e);
				
				throw new CHttpException(500, Yii::t('app', 'The retention regime is being used and cannot be deleted'));
			}
			if (!Yii::app()->getRequest()->getIsAjaxRequest())
			{
				$this->redirect(array('index'));
			}
		} else{
			throw new CHttpException(400, Yii::t('app', 'Your request is invalid.'));
		}
	}    

}
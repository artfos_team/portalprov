<?php

class ParametersController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	 public function actionChangeAttributeParameters()
	{
		
		
		$record = new Parameters();

		if (isset($_POST['Parameters'])) 
        {    
            $record->attributes = $_POST['Parameters']; 
        }

        Yii::import('bootstrap.widgets.TbEditableSaver');
		$es = new TbEditableSaver('Parameters');
	    $es->update();

		// $levelLog = 1;
		// $source = 'ParametersController->actionChangeAttributeParameters';
		//$commentaryLog = 'Se cambió el estado del parametro '.$record->idParameter.' a '.$es->value;
		//s $commentaryLog = 'Se cambió el estado del parametro '.$record->idParameter.' a ';
		// Logs::model()->log($levelLog,$commentaryLog,$source);
		//s Logs::model()->log($levelLog,$commentaryLog,$source);
   	}
	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$dataProvider=new CActiveDataProvider('Invoicecomments');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Parameters;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Parameters']))
		{
			$model->attributes=$_POST['Parameters'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->idParameter));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Parameters']))
		{
			$model->attributes=$_POST['Parameters'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->idParameter));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$model = new Parameters();
		$model->unsetAttributes();
		$this->layout = 'main_dashboard';
		$dataProvider=new CActiveDataProvider('Parameters');
		$this->render('index',array(
			'dataProvider'=>$dataProvider, 'model'=>$model,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Parameters('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Parameters']))
			$model->attributes=$_GET['Parameters'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Parameters the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Parameters::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Parameters $model the model to be validated
	 */
	public function performAjaxValidation($model, $formId = 'parameters-form')
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='parameters-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}

	public function actions()
	{
	    return array(
	        'toggle' => array(
	            'class'=>'bootstrap.actions.TbToggleAction',
	            'modelName' => 'Parameters',
	        )
	    );
	}

}


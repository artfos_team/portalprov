<?php

class ConsultantController extends AdminController
{
	/**
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
	 */
	public function actionIndex($export=false, $deleted = 0){
		$model = new Consultant();
		$model->unsetAttributes();

		if (isset($_GET['Consultant'])){
			$model->setAttributes($_GET['Consultant']);
		}

		if($export)
		{
			Yii::import('application.components.exportXls.ExportXLSHelper');
			$helper = new ExportXLSHelper();
			$helper->model = $model;
			$helper->filename = 'consultores-';
			$helper->header = 'Consultores al ';
			$dp = $model->search();
			$dp->pagination = false;
			$helper->data = $dp->getData();
			$helper->attributes = $model->getXlsAttributes();
			$helper->generateSheet();
			app()->end();
		}

		$record = new Consultant();		
		if(isset($_POST['ajax']) && $_POST['ajax']==='addRow')
		{
			echo CActiveForm::validate($record);
			Yii::app()->end();
		}
		elseif (isset($_POST['Consultant']))
		{
			$record->attributes = $_POST['Consultant'];

			if ($record->validate())
			{
				$record->save();
				echo CJSON::encode(array('success'=>true));
				Yii::app()->end();
			}
			echo CActiveForm::validate($record);
			Yii::app()->end();
		}

		$this->render('index', array(
			'model' => $model,
			'record'=>$record,
			'deleted'=>$deleted
		));
	}

	public function actionChangeAttributeConsultant()
	{
		Yii::import('bootstrap.widgets.TbEditableSaver');
		$es = new TbEditableSaver('Consultant');
	    $es->update();
	}

	public function actionDelete($id, $key) {
		$model = Consultant::model()->findByPk(CPropertyValue::ensureInteger($id));

		if (Yii::app()->getRequest()->getIsPostRequest() && $model instanceof Consultant && 
			strcmp(uKey("deleteCons".$model->primaryKey), $key)==0) 
		{			
			try {
				$model->delete();
			} catch (Exception $e) {
				$model->consultantDeleted = 1;
				$model->save(false, array('consultantDeleted'));
			}
			if (!Yii::app()->getRequest()->getIsAjaxRequest())
			{
				$this->redirect(array('index'));
			}
		} else{
			throw new CHttpException(400, Yii::t('app', 'Your request is invalid.'));
		}
	}
	
	/**
	 * Restores a deleted consultant
	 * @param integer $id the id of the consultant to restore
	 * @param string $key the key to ensure the correct instance is being used
	 * @throws CHttpException
	 */
	public function actionRestore($id, $key) {
		$model = Consultant::model()->resetScope()->findByPk($id);

		if($model instanceof Consultant && strcmp($key, uKey('restoreCons'.$id))==0)
		{
			$model->consultantDeleted = 0;
			$model->save(false, array('consultantDeleted'));
			if (!Yii::app()->getRequest()->getIsAjaxRequest())
			{
				$this->redirect(array('index'));
			}
			else
			{
				return;
			}
		}
		
		throw new CHttpException(400, Yii::t('app', 'Your request is invalid.'));
	}
}
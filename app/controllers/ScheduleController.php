<?php

require_once dirname(__FILE__) . '/../../vendor/autoload.php';

Yii::import('app.models.import.FileFormat');
Yii::import('app.models.import.ImportSchedule');
Yii::import('app.extensions.timepicker.EJuiDateTimePicker');

class ScheduleController extends AdminController
{
    public function actionIndex()
    {
        $model = new ImportSchedule();
        $companyGeneralData = new CompanyGeneralData();
        $model->unsetAttributes();
        $companyGeneralData->unsetAttributes();

        if (isset($_GET['ImportSchedule'])) {
            $model->setAttributes($_GET['ImportSchedule']);
        }

        if (isset($_GET['CompanyGeneralData'])) {
            $companyGeneralData->setAttributes($_GET['CompanyGeneralData']);
        }

        $this->render('index', array(
            'model' => $model,
            'companyGeneralData' => $companyGeneralData,
        ));
    }

    public function actionNewImportSchedule()
    {
        $model = new ImportSchedule();
        $this->displayImportScheduleForm($model);
    }

    /**
     * @param $id
     * @throws Exception
     */
    public function actionEditImportSchedule($id)
    {
        if (isset($id)) {
            $model = ImportSchedule::model()->findByPk($id);

            if (!isset($model)) {
                throw new Exception('Schedule no encontrado por id: '. $id);
            }

            $this->displayImportScheduleForm($model, true);
        }
        else {
            throw new Exception('Id schedule invalido: '. $id);
        }
    }

    public function actionDeleteImportSchedule($id)
    {
        if (isset($id)) {
            $model = ImportSchedule::model()->findByPk($id);

            if (isset($model)) {
                $model->delete();
            }
        }
    }

    /**
     * @param ImportSchedule $model
     * @param boolean $update
     */
    private function displayImportScheduleForm($model, $update = false) {

        if (isset($_POST['ImportSchedule'])) {
            $model->attributes = $_POST['ImportSchedule'];

            if ($model->save()) {
                Yii::app()->flashMessage->addMessage('success', Yii::t('app', 'Schedule guardado correctamente'));
                $this->redirect(array('index'));
            }
        }

        $this->render('new_import_schedule', array('model' => $model, 'update' => $update));
    }

    public function actionImportManually($id) {
        if($id == '19'){
            $this->actionExportarProv();
            Yii::app()->flashMessage->addMessage('success', Yii::t('app', 'Exportación realizada correctamente'));
            $this->redirect(array('index'));
        } else {
            if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') {
                $output = exec('cd ../app/ && yiic sync run --idSchedule='.$id); // WINDOWS
            } else {
                $output = shell_exec('php ../app/yiic.php sync run --idSchedule='.$id); //LINUX
                
            }
            //$output = exec('echo %cd%');
            //throw new Exception($output);
            if ($output != NULL) {
                Yii::app()->flashMessage->addMessage('success', Yii::t('app', 'Importación manual realizada correctamente'.$output));
                $this->redirect(array('index'));
            } else {
                Yii::app()->flashMessage->addMessage('error', Yii::t('app', 'Fallo al realizar la importación'));
                $this->redirect(array('index'));
            }
        }
    }

    public function exportCsv($data) {
        // echo "Welcome ", $name, "!";
        $currentDateTime = date('d-m-Y_H_i_s');
        $pathtofile = 'C:/Datos/data.csv';
        //$pathtofile = "C:/Datos/data_", $currentDateTime, ".csv";
        $destination = "C:/Datos/backup/data_". $currentDateTime. ".csv";
        if(file_exists($pathtofile)){
            if(!copy($pathtofile, $destination)) {  
                echo "error en backup";  
            }
        } else {
            $csv_file = 'C:/Datos/data.csv';    
        }
        $csv_file = 'C:/Datos/data.csv';
        if(is_null($data))
        {
            throw new Exception('Indefinido');
        }
        if (!$handle = fopen($csv_file, "w"))
        {
            echo "Cannot open file";
            exit;
        }
        if (fwrite($handle, utf8_decode($data)) === false)
        {
            echo "Cannot write to file";
            exit;
        }
        fclose($handle);
        //echo $_GET['csv'];
        if (file_exists($csv_file) && !is_dir($csv_file))
        {
            header('Content-type: text/csv;');
            header("Cache-Control: maxage=1");
            header("Pragma: public");
            header('Content-Disposition: attachment; filename="data.csv"');
            readfile($csv_file);
            return;
        }
    }


    public function getCompanyModel() {
        /* switch($id) {
            case 'company_general_data': return CompanyGeneralData::model();
            default: throw new CException('Error');
        } */
        return Company::model();
    }
    //public function getCompanyGeneralDataModel($id) //#COMMENT
    public function getCompanyGeneralDataModel() {
        /* switch($id) {
            case 'company_general_data': return CompanyGeneralData::model();
            default: throw new CException('Error');
        } */
        return CompanyGeneralData::model();
    }
    //public function getCompanyGeneralDataForm($id) //#COMMENT
    public function getCompanyForm() {
        /* switch($id) {
            case 'company_general_data': return new CompanyGeneralData();
            default: throw new CException('Error form');
        } */
        return new Company();
    }
    public function getCompanyGeneralDataForm() {
        /* switch($id) {
            case 'company_general_data': return new CompanyGeneralData();
            default: throw new CException('Error form');
        } */
        return new CompanyGeneralData();
    }
    public function getCompanyPaymentDataModel() {
        return CompanyPaymentData::model();
    }
    public function getCompanyPaymentDataForm() {
        return new CompanyPaymentData();
    }
    public function getCompanyTaxDataModel() {
        return CompanyTaxData::model();
    }
    public function getCompanyJurisdictionDataModel() {
        return CompanyJurisdictionData::model();
    }
    public function getCompanyTaxDataForm() {
        return new CompanyTaxData();
    }
    public function getCompanyJurisdictionData() {
        return new CompanyJurisdictionData();
    }
    public function versionConfigExport () {
        //representa la versión del formato del archivo. (Debe comenzar siempre con V)
      }

    public function isExported ($idCompany) {
        $company = $this->getCompanyModel()->findByPk($idCompany);
        $company->isExport = '1';
        /*if($company->idCompanyDataStatus == '4'){
            $company->isExport = '0';
        }*/
        $company->save();
      }

    public function actionExportarProv() {
            $form = $this->getCompanyGeneralDataForm();
            $form1 = $this->getCompanyForm();
            $form2 = $this->getCompanyPaymentDataForm();
            $form3 = $this->getCompanyTaxDataForm();
            $form4 = $this->getCompanyJurisdictionData();
            // $condition = 'isExport = 0';
            $condition = 'isExport IN (0) AND idCompanyDataStatus IN (2)';
            $companies = $this->getCompanyModel()->findAll($condition);
            $companyGD = $this->getCompanyGeneralDataModel()->with('idCountry0')->findAll();
            $companyPD = $this->getCompanyPaymentDataModel()->findAll();
            $companyTD = $this->getCompanyTaxDataModel()->findAll();
            $companyJD = $this->getCompanyJurisdictionDataModel()->findAll();
            if($this->getCompanyGeneralDataModel() instanceof ProviderDataExport)
            {
                $returnFull = array();
                $returnJurisdictionsFull = array();

                for ($i  = 0; $i < count($companies); $i++) {
                    //throw new Exception(count($companyGD));
                    $flagCompanyDataStatus = '';
                    if($companies[$i]['idCompanyDataStatus'] == '4'){
                        $flagCompanyDataStatus = 'MO';
                    } else {
                        $flagCompanyDataStatus = 'AL';
                    }
                    //print_r($companies[$i]['idCompanyDataStatus']);
                    //die;
                    $currentDateTime = date('dmY');
                    $returnCabecera = array();
                    $returnCabecera[] = ['V0.1','PROV',$flagCompanyDataStatus,$currentDateTime,';',';',';',';',';',';',';',';',';',';',';',';',';',';',';',';',';',';',';',';',';',';',';'];
                    $condition = '';

                    ///$companies[$i]->taxData   /// con esta linea accedes mediando la tabla company y el campo idTaxData a su tabla taxData
                    $auxIdGenDataCompany = $companies[$i]['idGeneralData'];
                    $auxIdPayDataCompany = $companies[$i]['idPaymentData'];
                    $auxIdTaxDataCompany = $companies[$i]['idTaxData'];


                    /// DIMPIB'**' (ingresos Brutos) X Jurisdicciones
                    $companyName = $companies[$i];
                    $companyTypeIvaCode = $companyGD[$i];
                    $companyTData = $companyTD[$i];
                    $returnTaxDataIBJur = array();
                    $temp = array();
                    $temp2 = array();
                    for ($j  = 0; $j < count($companyJD); $j++) {
                        $headerTaxDataIBJur = array();
                        $n = '';
                        $companyJurisData = $companyJD[$j];
                        if($companyJD[$j]['idTaxData'] == $auxIdTaxDataCompany){
                            if(!empty($companyJurisData->idJurisdiction0->codeERP)){
                                // $temp[] = "DIMPIB" . $companyJurisData->idJurisdiction0->codeERP;
                                $temp[] = "DIMP";
                                $temp[] = "300";
                                $temp[] = "";
                                $temp[] = $companyName->company;
                                $temp[] = $companyJurisData->idJurisdiction0->codeERP;
                                $temp[] = "X";
                                for ($p  = 0; $p < count($companyTD); $p++) {
                                    if($companyTD[$p]['idTaxData'] == $auxIdTaxDataCompany){
                                        $tempAux = $companyTD[$p];
                                        if ($companyJurisData->idJurisdiction0->codeERP == 'IC') {
                                            if($tempAux['ibType'] == 'Local'){
                                                $temp[] = '1';
                                            } else if ($tempAux['ibType'] == 'Multilateral'){
                                                $temp[] = '2';
                                            } else if ($tempAux['ibType'] == 'Exento'){
                                                $temp[] = '4';
                                            }
                                        } else {
                                            $temp[] = '';
                                        }
                                    }
                                }
                                $temp[] = "";
                                // $temp[] = "03";
                                if(!empty($companyJurisData->jurisdictionIdCategoryRetain0->indicadorRet)){
                                    $temp[] = $companyJurisData->jurisdictionIdCategoryRetain0->indicadorRet;
                                } else {
                                    $temp[] = '';
                                }
                                if(!empty($companyJurisData->certificateNumber)){
                                    $temp[] = $companyJurisData->certificateNumber;
                                    $temp[] = "100";
                                    //$temp[] = $companyJurisData->certificateFrom;
                                    $temp[] = date("dmY", strtotime($companyJurisData->certificateFrom));
                                    $temp[] = date("dmY", strtotime($companyJurisData->certificateTo));
                                    //$temp[] = $companyJurisData->certificateTo;
                                } else {
                                    $temp[] = "";
                                    $temp[] = "";
                                    $temp[] = "";
                                    $temp[] = "";
                                }
                                $temp[] = "";
                                $temp[] = "";
                                $temp[] = "";
                                $temp[] = "";
                                $temp[] = "";
                                $temp[] = "";
                                $temp[] = "";
                                $temp[] = "";
                                $temp[] = "";
                                $temp[] = "";
                                $temp[] = "";
                                $temp[] = "";
                                $temp[] = "";
                                $temp[] = "";
                                for ($d  = 0; $d < count($companyGD); $d++) {
                                    if($companyGD[$d]['idGeneralData'] == $auxIdGenDataCompany){
                                        $tempCompanyTypeIvaCode = $companyGD[$d];
                                        if($tempCompanyTypeIvaCode->typeIVA == '006') {
                                            $temp[] = '06';    
                                        } else {
                                            $temp[] = $tempCompanyTypeIvaCode->typeIVA;
                                        }
                                    }
                                }
                                //$temp[] = $companyTypeIvaCode->typeIVA;
                            }
                        }
                    }
                    $returnTaxDataIBJur = array_chunk($temp, 28, true);

                    /// DIMPIB'**' (ingresos Brutos) X Jurisdicciones


                    /// DGEN
                    $returnGeneralData = array();
                    $headerGeneralData = array();
                    $DGEN = 'DGEN';
                    $headerGeneralData[] = $DGEN;

                    $temp = array();
                    $n = '';
                    $temp[0] = $DGEN;
                    $temp[1] = '300';
                    $temp[2] = 'COMP';
                    
                    $companyAll = $companies[$i]; //asigna unicamente si 'isExported' = 0 

                    //$contador = '0';

                    for ($z  = 0; $z < count($companyGD); $z++) {
                        if($companyGD[$z]['idGeneralData'] == $auxIdGenDataCompany){
                            $company = $companyGD[$z];
                            foreach($form->getExportableAttributes() as $attribute=>$type)
                            {
                                switch ($type) {
                                    case 'plain':
                                        $temp[] = $company->$attribute;
                                        break;
                                    /*case 'plain2':
                                        $temp[] = $company['namePrincipalContact'];
                                        break;*/
                                    case 'rSocial':
                                        foreach($form1->getExportableAttributes() as $attribute1=>$type1){
                                            switch ($type1) {
                                                case 'company':
                                                    $temp[] = $companyAll->$attribute1;
                                                    break;
                                                }
                                        }
                                    break;
                                    case 'plain3':
                                        $temp[] = $company['city'];
                                        break;
                                    case 'codCountry':
                                        $temp[] = $company->idCountry0->codCountry;
                                        break;
                                    case 'codRegion':
                                        $temp[] = $company->idProvince0->codRegion;
                                        break;
                                    case 'Conc.busq.':
                                        foreach($form1->getExportableAttributes() as $attribute1=>$type1){
                                            switch ($type1) {
                                                case 'company':
                                                    $temp[] = $companyAll->$attribute1;
                                                    break;
                                                }
                                        }
                                    break;
                                    case 'senor':
                                        $temp[] = 'Señor';
                                        break;
                                    case 'null':
                                        $temp[] = '0';
                                        break;
                                    case 'digitControl':
                                        $temp[] = '0';
                                        break;
                                    case 'comp':
                                        $temp[] = 'COMP';
                                        break;
                                    case 'gCuentas':
                                        $temp[] = 'ZNAC';
                                        break;
                                    case 'clvIdioma':
                                        $temp[] = 'S';
                                        break;
                                    case 'cuitTest':
                                        //$temp[] = 'S';
                                        //
                                        //if($contador != '1'){
                                            foreach($form1->getExportableAttributes() as $attribute1=>$type1){
                                                switch ($type1) {
                                                    case 'cuit':
                                                        $temp[] = $companyAll->$attribute1;
                                                        break;
                                                    }
                                            }
                                        //}
                                        //$contador = '1';
                                        break;
                                    case 'pf':
                                        $temp[] = 'X';
                                        break;
                                    case 'status':
                                        $temp[] = 'X';
                                        break;
                                    case 'impuesto':
                                        $temp[] = '06';
                                        break;
                                    case 'nif':
                                        $temp[] = '80';
                                        break;
                                }
                                
                            }
                        }
                    }
                    $returnGeneralData[] = $temp;
                    /// DGEN
                    /// ---------------------



                    /// DSOC
                    $returnSocietyData = array();
                    $headerSocietyData = array();
                    $DSOC = 'DSOC';
                    $headerSocietyData[] = $DSOC;
                    $temp = array();
                    $n = '';
                    $temp[0] = $DSOC;
                    $temp[1] = '300';
                    $temp[2] = 'COMP';

                    //$testIdCompany = array();
                    foreach($form1->getExportableAttributes() as $attribute=>$type)
                    {
                        //$testIdCompany[] = $companyAll->$attribute->idCompany; 
                        switch ($type) {
                            case 'company':
                                $temp[] = $companyAll->$attribute;
                                break;
                            case 'comp':
                                $temp[] = 'comp';
                                break;
                            case 'numDePersonal':
                                $temp[] = '0';
                                break;
                            case 'viasDePago':
                                $temp[] = 'ABCDGHJKR';
                                break;
                            case 'condPago':
                                $temp[] = 'Z030';
                                break;
                            case 'grupoTesor':
                                $temp[] = 'A1';
                                break;
                            case 'ritmoCalcInt':
                                $temp[] = '0';
                                break;
                            case 'limEfectos':
                                $temp[] = '0,00';
                                break;
                            case 'diasHastaCobro':
                                $temp[] = '0';
                                break;
                            case 'verifFactDoble':
                                $temp[] = 'X';
                                break;
                            case 'paisDeRet':
                                foreach($form->getExportableAttributes() as $attribute1=>$type1){
                                    switch ($type1) {
                                        case 'codCountry':
                                            $temp[] = $company->idCountry0->codCountry;
                                            break;
                                        }
                                }
                                break;
                            case 'ordenCheque':
                                foreach($form->getExportableAttributes() as $attribute1=>$type1){
                                    switch ($type1) {
                                        case 'plain2':
                                            $temp[] = $company['namePrincipalContact'];
                                            break;
                                        }
                                }
                                break;
                            case 'activEcon':
                                $temp[] = '0';
                                break;
                            case 'dateInspect':
                                $temp[] = '0';
                                break;
                            case 'offsetPercent':
                                $temp[] = '0';
                                break;
                            case 'basisPoints':
                                $temp[] = '0,000';
                                break;
                            case 'empty':
                                $temp[] = ';';
                                break;
                        }
                    }
                    $returnSocietyData[] = $temp;
                    /// DSOC
                    /// ---------------------


                    /// DBAN
                    $returnPaymentData = array();
                    $headerPaymentData = array();
                    $DBAN = 'DBAN';
                    $headerPaymentData[] = $DBAN;


                    $temp = array();
                    $n = '';
                    $temp[0] = $DBAN;
                    $temp[1] = '300';
                    $temp[2] = 'COMP';

                    for ($k  = 0; $k < count($companyGD); $k++) {
                        if($companyGD[$k]['idGeneralData'] == $auxIdGenDataCompany && $companyPD[$k]['idPaymentData'] == $auxIdPayDataCompany){
                            $companyGDTEMP = $companyGD[$k];
                            $company = $companyPD[$k];
                            foreach($form2->getExportableAttributes() as $attribute=>$type)
                            {
                                switch ($type) {
                                    case 'PaisBanco':
                                        foreach($form->getExportableAttributes() as $attribute1=>$type1){
                                            switch ($type1) {
                                                case 'codCountry':
                                                    $temp[] = $companyGDTEMP->idCountry0->codCountry;
                                                    break;
                                                }
                                        }
                                        break;
                                    case 'plain':
                                        $temp[] = $company->$attribute;
                                        break;
                                    case 'bank':
                                        $temp[] = $company->idBank0->claveBanco;
                                        break;
                                    case 'idBankAccountType':
                                        $temp[] = $company->idBankAccountType0->bankAccountType;
                                        break;
                                    case 'empty':
                                        $temp[] = ';';
                                        break;
                                }
                            }
                        }
                    }
                    $returnPaymentData[] = $temp;
                    /// DBAN
                    /// ---------------------
                    

                    /// DCOM
                    $returnPurchasesData = array();
                    $headerPurchasesData = array();
                    $DCOM = 'DCOM';
                    $headerPurchasesData[] = $DCOM;


                    $temp = array();
                    $n = '';
                    $temp[0] = $DCOM;
                    $temp[1] = '300';
                    $temp[2] = 'COMP';
                    foreach($form1->getExportableAttributes() as $attribute=>$type)
                    {
                        switch ($type) {
                            case 'compDCOM':
                                $temp[] = 'comp';
                                break;
                            case 'orgComprasDCOM':
                                $temp[] = 'AL01';
                                break;
                            case 'monPedidoDCOM':
                                $temp[] = 'ARS';
                                break;
                            case 'valMinPedidoDCOM':
                                $temp[] = '0,00';
                                break;
                            case 'condPagoDCOM':
                                $temp[] = 'Z030';
                                break;
                            case 'verifFBEMDCOM':
                                $temp[] = 'X';
                                break;
                            case 'gpComprasDCOM':
                                $temp[] = 'GP1';
                                break;
                            case 'autoFactDCOM':
                                $temp[] = '0';
                                break;
                            case 'plEntPrevDCOM':
                                $temp[] = '0';
                                break;
                            case 'verifFactRelServDCOM':
                                $temp[] = 'X';
                                break;
                            case 'horaPuestaDispDCOM':
                                $temp[] = '0';
                                break;
                            case 'emptyDCOM':
                                $temp[] = ';';
                                break;
                        }
                    }
                    $returnPurchasesData[] = $temp;
                    /// DCOM
                    /// ---------------------


/* //Con Jurisdicción SEDE
                    /// DIMPIB (ingresos Brutos)
                    $returnTaxDataIB = array();
                    $headerTaxDataIB = array();
                    $DIMPIB = 'DIMPIB';
                    $headerTaxDataIB[] = $DIMPIB;


                    $temp = array();
                    $n = '';
                    $temp[0] = $DIMPIB;
                    $temp[1] = '300';
                    $company = $companyTD[$i];
                    $companyJurisData = $companyJD[$i];
                    foreach($form3->getExportableAttributes() as $attribute=>$type)
                    {
                        switch ($type) {
                            case 'emptyIB':
                                $temp[] = ';';
                                break;
                            case 'sociedadIB':
                                    foreach($form1->getExportableAttributes() as $attribute1=>$type1){
                                        switch ($type1) {
                                            case 'company':
                                                $temp[] = $companyAll->$attribute1;
                                                break;
                                            }
                                    }
                                break;
                            // case 'retentionTypeIB':
                            //     foreach($form4->getExportableAttributes() as $attribute1=>$type1){
                            //         switch ($type1) {
                            //             case 'retentionTypeIB_JD':
                            //                 //var_dump($companyJurisData->idJurisdiction0->codeERP);
                            //                 //die;
                            //                 $temp[] = $companyJurisData->idJurisdiction0->codeERP;
                            //                 break;
                            //             }
                            //     }
                            //     break;
                            case 'retentionTypeIB':
                                $temp[] = $company->jurisdictionHeadquarters0->codeERP;
                                break;
                            case 'sujetoRetenIB':
                                $temp[] = 'X';
                                break;
                            case 'categoryRetainIB':
                                if($company['ibType'] == 'Local'){
                                    $temp[] = '01';
                                } else if ($company['ibType'] == 'Multilateral'){
                                    $temp[] = '02';
                                }
                                break;
                            case 'nroIdFiscalIB':
                                $temp[] = ';';
                                break;
                            case 'indicRetIB':
                                $temp[] = '03';
                                break;
                            case 'nroCertifExenIB':
                                foreach($form4->getExportableAttributes() as $attribute1=>$type1){
                                    switch ($type1) {
                                        case 'cNumberIB':
                                            $temp[] = $companyJurisData['certificateNumber'];
                                            break;
                                        }
                                }
                                break;
                            case 'tipoExenIB':
                                $temp[] = '100%';
                                break;
                            case 'feInicExenIB':
                                foreach($form4->getExportableAttributes() as $attribute1=>$type1){
                                    switch ($type1) {
                                        case 'cNumberFromIB':
                                            $temp[] = $companyJurisData['certificateFrom'];
                                            break;
                                        }
                                }
                                break;
                            case 'feFinExenIB':
                                foreach($form4->getExportableAttributes() as $attribute1=>$type1){
                                    switch ($type1) {
                                        case 'cNumbertoIB':
                                            $temp[] = $companyJurisData['certificateTo'];
                                            break;
                                        }
                                }
                                break;
                        }
                    }
                    $returnTaxDataIB[] = $temp;
                    /// DIMPIB
                    /// ---------------------
*/


                    

                    /// DIMPRG (Retención Ganancias)
                    $returnTaxDataRG = array();
                    $headerTaxDataRG = array();
                    $DIMPRG = 'DIMP';
                    $headerTaxDataRG[] = $DIMPRG;


                    $temp = array();
                    $n = '';
                    $temp[0] = $DIMPRG;
                    $temp[1] = '300';


                    for ($e  = 0; $e < count($companyGD); $e++) {
                        if($companyTD[$e]['idTaxData'] == $auxIdTaxDataCompany){
                            $company = $companyTD[$e];
                            $companyGenData = $companyGD[$e];
                            $tempCompanyJurisData = $companyJD[$e];
                            foreach($form3->getExportableAttributes() as $attribute=>$type)
                            {
                                switch ($type) {
                                    case 'empty':
                                        $temp[] = ';';
                                        break;
                                    case 'sociedad':
                                            foreach($form1->getExportableAttributes() as $attribute1=>$type1){
                                                switch ($type1) {
                                                    case 'company':
                                                        $temp[] = $companyAll->$attribute1;
                                                        break;
                                                    }
                                            }
                                        break;
                                    case 'retentionType':
                                        if($company['retentionRegimen'] == ''){
                                            $temp[] = '';
                                        } else {
                                            $temp[] = $company->retentionRegimen0->codeERP;
                                        }
                                        break;
                                    case 'sujetoReten':
                                        $temp[] = 'X';
                                        break;
                                    case 'categoryRetain':
                                        if($company['idEarningCategoryRetain'] == ''){
                                            $temp[] = '';
                                        } else {
                                            $temp[] = $company->idEarningCategoryRetain0->codeERP;
                                        }
                                        break;
                                    case 'nroIdFiscal':
                                        $temp[] = ';';
                                        break;
                                    case 'indicRet':
                                        // $temp[] = '03';
                                        if($company['idEarningCategoryRetain'] == ''){
                                            $temp[] = '';
                                        } else {
                                            $temp[] = $company->idEarningCategoryRetain0->indicadorRet;
                                        }
                                        /*foreach($form4->getExportableAttributes() as $attribute1=>$type1){
                                            switch ($type1) {
                                                case 'aliquotDesc':
                                                    $temp[] = $tempCompanyJurisData->jurisdictionIdCategoryRetain0->indicadorRet;
                                                    break;
                                                }
                                        }*/
                                        break;
                                    case 'exNumber':
                                        if(!empty($company['earningsExclusionNumber'])){
                                            $temp[] = $company['earningsExclusionNumber'];
                                        } else {
                                            $temp[] = '';
                                        }
                                        break;
                                    case 'exFrom':
                                        // $temp[] = $company->$attribute;
                                        if(!empty($company['earningsExclusionNumber'])){
                                            $temp[] = date("dmY", strtotime($company['earningsExclusionFrom']));
                                        } else {
                                            $temp[] = '';
                                        }
                                        break;
                                    case 'exTo':
                                        // $temp[] = $company->$attribute;
                                        if(!empty($company['earningsExclusionNumber'])){
                                            $temp[] = date("dmY", strtotime($company['earningsExclusionTo']));   
                                        } else {
                                            $temp[] = '';
                                        }
                                        break;
                                    case 'tipoExen':
                                        if(!empty($company['earningsExclusionNumber'])){
                                            $temp[] = '100';
                                        } else {
                                            $temp[] = '';
                                        }
                                        break;
                                    case 'typeIvaCod':
                                        foreach($form->getExportableAttributes() as $attribute1=>$type1){
                                            switch ($type1) {
                                                case 'plain4':
                                                    if($companyGenData['typeIVA'] == '006') {
                                                        $temp[] = '06';
                                                    } else {
                                                        $temp[] = $companyGenData['typeIVA'];
                                                    }
                                                    break;
                                                }
                                        }
                                    break;
                                }
                            }
                        }
                    }
                    $returnTaxDataRG[] = $temp;
                    /// DIMPRG
                    /// ---------------------


                    /// DIMPRS
                    $returnTaxDataRS = array();
                    $headerTaxDataRS = array();
                    $DIMPRS = 'DIMP';
                    $headerTaxDataRS[] = $DIMPRS;


                    $temp = array();
                    $n = '';
                    $temp[0] = $DIMPRS;
                    $temp[1] = '300';


                    for ($g  = 0; $g < count($companyGD); $g++) {
                        if($companyTD[$g]['idTaxData'] == $auxIdTaxDataCompany){
                            $company = $companyTD[$g];
                            $companyGenData = $companyGD[$g];
                            foreach($form3->getExportableAttributes() as $attribute=>$type)
                            {
                                switch ($type) {
                                    case 'emptyRS':
                                        $temp[] = ';';
                                        break;
                                    case 'sociedadRS':
                                            foreach($form1->getExportableAttributes() as $attribute1=>$type1){
                                                switch ($type1) {
                                                    case 'company':
                                                        $temp[] = $companyAll->$attribute1;
                                                        break;
                                                    }
                                            }
                                        break;
                                    case 'retentionTypeRS':
                                        if($company['sussIdCategoryRetain'] == ''){
                                            $temp[] = '';
                                        } else {
                                            $temp[] = $company->sussIdCategoryRetain0->codeERP;
                                        }
                                        break;
                                    case 'sujetoRetenRS':
                                        $temp[] = 'X';
                                        break;
                                    case 'categoryRetainRS':
                                        if($company['sussIdCategoryRetain'] == ''){
                                            $temp[] = '';
                                        } else {
                                            // $temp[] = $company->sussIdCategoryRetain0->codeERP2;
                                            $temp[] = '';
                                        }
                                        break;
                                    case 'nroIdFiscalRS':
                                        $temp[] = ';';
                                        break;
                                    case 'indicRetRS':
                                        // $temp[] = '03';
                                        if($company['sussIdCategoryRetain'] == ''){
                                            $temp[] = '';
                                        } else {
                                            $temp[] = $company->sussIdCategoryRetain0->indicadorRet;
                                        }
                                        break;
                                    case 'plainRS':
                                        if(!empty($company['sussExclusionNumber'])){
                                            $temp[] = $company['sussExclusionNumber'];
                                        } else {
                                            $temp[] = '';
                                        }
                                        break;
                                    case 'plainRSexFrom':
                                        // $temp[] = $company->$attribute;
                                        if(!empty($company['sussExclusionNumber'])){
                                            $temp[] = date("dmY", strtotime($company['sussExclusionFrom']));
                                        } else {
                                            $temp[] = '';
                                        }
                                        break;
                                    case 'plainRSexTo':
                                        // $temp[] = $company->$attribute;
                                        if(!empty($company['sussExclusionNumber'])){
                                            $temp[] = date("dmY", strtotime($company['sussExclusionTo']));
                                        } else {
                                            $temp[] = '';
                                        }
                                        break;
                                    case 'tipoExenRS':
                                        if(!empty($company['sussExclusionNumber'])){
                                            $temp[] = '100';
                                        } else {
                                            $temp[] = '';
                                        }
                                        break;
                                    case 'typeIvaCod2':
                                        foreach($form->getExportableAttributes() as $attribute1=>$type1){
                                            switch ($type1) {
                                                case 'plain4':
                                                    if($companyGenData['typeIVA'] == '006') {
                                                        $temp[] = '06';
                                                    } else {
                                                        $temp[] = $companyGenData['typeIVA'];
                                                    }
                                                    break;
                                                }
                                        }
                                    break;
                                }
                            }
                        }
                    }
                    $returnTaxDataRS[] = $temp;
                    /// DIMPRS
                    /// ---------------------

                    /// DIMPIVA
                    $returnTaxDataIVA = array();
                    $headerTaxDataIVA = array();
                    $DIMPIVA = 'DIMP';
                    $headerTaxDataIVA[] = $DIMPIVA;


                    $temp = array();
                    $n = '';
                    $temp[0] = $DIMPIVA;
                    $temp[1] = '300';
                    
                    for ($c  = 0; $c < count($companyGD); $c++) {
                        if($companyTD[$c]['idTaxData'] == $auxIdTaxDataCompany){
                            $company = $companyTD[$c];
                            $company = $companyTD[$c];
                            foreach($form3->getExportableAttributes() as $attribute=>$type)
                            {
                                switch ($type) {
                                    case 'emptyIVA':
                                        $temp[] = ';';
                                        break;
                                    case 'sociedadIVA':
                                            foreach($form1->getExportableAttributes() as $attribute1=>$type1){
                                                switch ($type1) {
                                                    case 'company':
                                                        $temp[] = $companyAll->$attribute1;
                                                        break;
                                                    }
                                            }
                                        break;
                                    case 'retentionTypeIVA':
                                        if($company['ivaIdRetentionRegime'] == ''){
                                            $temp[] = '';
                                        } else {
                                            $temp[] = $company->ivaIdRetentionRegime0->codeERP;
                                        }
                                        break;
                                    case 'sujetoRetenIVA':
                                        $temp[] = 'X';
                                        break;
                                    case 'categoryRetainIVA':
                                        if($company['ivaIdCategoryRetain'] == ''){
                                            $temp[] = '';
                                        } else {
                                            $temp[] = $company->ivaIdCategoryRetain0->codeERP;
                                        }
                                        break;
                                    case 'nroIdFiscalIVA':
                                        $temp[] = ';';
                                        break;
                                    case 'indicRetIVA':
                                        // $temp[] = '03';
                                        if($company['ivaIdCategoryRetain'] == ''){
                                            $temp[] = '';
                                        } else {
                                            $temp[] = $company->ivaIdCategoryRetain0->indicadorRet;
                                        }
                                        break;
                                    case 'plainIVA':
                                        if(!empty($company['earningsExclusionNumber'])){
                                            $temp[] = $company['ivaExclusionNumber'];
                                        } else {
                                            $temp[] = '';
                                        }
                                        break;
                                    case 'plainIVAexFrom':
                                        // $temp[] = $company->$attribute;
                                        if(!empty($company['earningsExclusionNumber'])){
                                            $temp[] = date("dmY", strtotime($company['ivaExclusionFrom']));
                                        } else {
                                            $temp[] = '';
                                        }
                                        break;
                                    case 'plainIVAexTo':
                                        // $temp[] = $company->$attribute;
                                        if(!empty($company['earningsExclusionNumber'])){
                                            $temp[] = date("dmY", strtotime($company['ivaExclusionTo']));
                                        } else {
                                            $temp[] = '';
                                        }
                                        break;
                                    case 'tipoExenIVA':
                                        if(!empty($company['earningsExclusionNumber'])){
                                            $temp[] = '100';
                                        } else {
                                            $temp[] = ''; 
                                        }
                                        break;
                                    case 'typeIvaCod3':
                                        foreach($form->getExportableAttributes() as $attribute1=>$type1){
                                            switch ($type1) {
                                                case 'plain4':
                                                    if($companyGenData['typeIVA'] == '006') {
                                                        $temp[] = '06';
                                                    } else {
                                                        $temp[] = $companyGenData['typeIVA'];
                                                    }
                                                    break;
                                                }
                                        }
                                    break;
                                }
                            }
                        }
                    }
                    $returnTaxDataIVA[] = $temp;
                    /// DIMPIVA
                    /// ---------------------
                    

                    $returnFullCount = array_merge($returnGeneralData,$returnSocietyData,$returnPaymentData,$returnPurchasesData,$returnTaxDataIBJur,$returnTaxDataRG,$returnTaxDataRS,$returnTaxDataIVA);
                    $exportRecordsCount = count($returnFullCount);


                    $returnFin = array();
                    $returnFin[] = ['FIN',$exportRecordsCount,';',';',';',';',';',';',';',';',';',';',';',';',';',';',';',';',';',';',';',';',';',';',';',';',';'];
                    
                    $returnFull = array_merge($returnFull, $returnCabecera, $returnGeneralData,$returnPaymentData,$returnSocietyData,$returnPurchasesData,$returnTaxDataIBJur,$returnTaxDataRG,$returnTaxDataRS,$returnTaxDataIVA,$returnFin);
                    $this->isExported($companies[$i]['idCompany']);

                }



                if(empty($returnFull)){
                    Yii::app()->flashMessage->addMessage('error', Yii::t('app', 'Sin proveedores a exportar'));
                    $this->redirect(array('index'));
                } else {
                    $txt = array();
                    foreach($returnFull as $entrada)
                    {
                        $txt[] = implode(';', str_replace(array('"',"'",";",""), "", $entrada));
                    }
                    $this->exportCsv(implode("\n", $txt));
                    return;
                } 
            }
        throw new CHttpException(404, 'La datos no existem');
    }
}
        
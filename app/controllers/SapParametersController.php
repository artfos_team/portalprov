<?php

    require_once dirname(__FILE__).'/../../vendor/autoload.php';

    class SapParametersController extends AdminController
    {

    	/**
    	 * This is the default 'index' action that is invoked
    	 * when an action is not explicitly requested by users.
    	 */
    	public function actionIndex()
    	{
    		$model = new SapParameters();
    		$model->unsetAttributes();

    		if (isset($_GET['SapParameters'])){
    			$model->setAttributes($_GET['SapParameters']);
    		}
            $parameter = Parameters::model()->findByPk('IFM-001');
            if(isset($parameter) && $parameter->value == 1)
            {
                $import = true;
            }
            else
            {
                $import = false;
            }
    		$this->render('index', array(
    			'model' => $model,'import' => $import,
    		));
    	}



    	public function actionChangeAttributeSapParameters()
    	{
    		Yii::import('bootstrap.widgets.TbEditableSaver');
    		$es = new TbEditableSaver('SapParameters');
    	    $es->update();
    	}

    	/*public function actionDelete($id, $key)
    	{
    		$model = SapParameters::model()->findByPk(CPropertyValue::ensureInteger($id));
    		if (Yii::app()->getRequest()->getIsPostRequest() && $model instanceof SapParameters && 
    			strcmp($key, uKey('deletePart'.$model->primaryKey))==0) 
    		{			
    			try
    			{
    				$model->delete();
    			}
    			catch (Exception $e)
    			{
    				$model->participantDeleted = 1;
    				$model->save(false, array('participantDeleted'));
    			}
    			
    			if (!Yii::app()->getRequest()->getIsAjaxRequest())
    			{
    				$this->redirect(array('index'));
    			}
    		}
    		else
    		{
    			throw new CHttpException(400, Yii::t('app', 'Your request is invalid.'));
    		}
    	}*/
    	
        public function actionCreate(){
            $model = new SapParameters();

            if(isset($_POST['SapParameters'])){
                $model->attributes=$_POST['SapParameters'];
                if($model->save()){
                    $this->redirect(array('index'));
                }
            }
            $this->render('create', array('model'=>$model));
        }

    	/**
    	 * Updates a participant by $id
    	 */ 
    	public function actionUpdate($id, $key)
    	{
    		$model = SapParameters::model()->findByPk(CPropertyValue::ensureInteger($id));

    		if($model instanceof SapParameters && strcmp($key, uKey('updatePart'.$model->primaryKey))==0)
    		{
    			if(isset($_POST['SapParameters' ]))
    	       	{
    		        $model->attributes=$_POST['SapParameters' ];
    		        if($model->save())
    		        {
    		            $this->redirect(array('index'));
    		        }
    	        }
    			$this->render('form', array('model'=>$model));
    			return;
    		}
    		
    		throw new CHttpException(404, "Invoicestatus inválido");
    	}

    	/**
    	 * View
    	 * @param int $idInvoice
    	 */
    	public function actionView($invoiceStatus)
    	{
    		$model = SapParameters::model()->findByPk(CPropertyValue::ensureInteger($invoiceStatus));

    		if($model instanceof SapParameters)
    		{
    			$this->render('view', array('model'=>$model, 'SapParameters' =>$invoice));
    			app()->end();
    		}

    		throw new CHttpException(404, Yii::t('app', 'Invalid invoice'));
    	}

        // on your controller
        // example code for action rendering the relational data
        public function actionRelational()
        {
            // partially rendering "_relational" view
            $this->renderPartial('_relational', array(
                'id' => Yii::app()->getRequest()->getParam('id'),
                'gridDataProvider' => $this->getGridDataProvider(),
                'gridColumns' => $this->getGridColumns()
            ));
        }

        public function actionPing()
        {
            $models = SapParameters::model()->findAll();
            $host = $models[0]->ashost;

            $comando = "ping -c 1 ".$host;
            $output = shell_exec($comando);

            if(strpos($output, '1 received'))
            {
                app()->flashMessage->addMessage('success', Yii::t('app', 'Hay conexión con el servidor'));
                $this->redirect(array('index'));
            }
            else 
            {
                 app()->flashMessage->addMessage('error', Yii::t('app', 'No hay conexión con el servidor'));
                $this->redirect(array('index'));
            }
        }

        public function actionImportExcel()
        {
           /*$reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
            $spreadsheet = $reader->load("C:\Program Files (x86)\Ampps\www\iprove\\testExcel.xlsx");
            $worksheet = $spreadsheet->getActiveSheet();

            echo '<table border=1>' . PHP_EOL;
            foreach ($worksheet->getRowIterator() as $row) {
                echo '<tr>' . PHP_EOL;
                $cellIterator = $row->getCellIterator();
                $cellIterator->setIterateOnlyExistingCells(FALSE); // This loops through all cells,
                                                                   //    even if a cell value is not set.
                                                                   // By default, only cells that have a value
                                                                   //    set will be iterated.
                foreach ($cellIterator as $cell) {

                    echo '<td>' .
                         $cell->getValue() .
                         '</td>' . PHP_EOL;
                }
                echo '</tr>' . PHP_EOL;
            }
            echo '</table>' . PHP_EOL;
        }*/

        $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();

        if(isset($_FILES['excelDir']['tmp_name']))
        {
            $spreadsheet = $reader->load($_FILES['excelDir']['tmp_name']);
        }
        else
        {
            app()->flashMessage->addMessage('error', Yii::t('app', 'No se encontró el archivo'));
            $this->redirect(array('index'));
        }
        $worksheet = $spreadsheet->getActiveSheet();
        echo $highestRow = $worksheet->getHighestRow();

        $config = include '../config/main-environment.php';

        $conn = new mysqli('localhost', $config['components']['db']['username'], $config['components']['db']['password'], 'iproveedores');
        //VENDOR_VAT_NO : 1, XBLNR : 2, BLDAT : 3, GROSS_AMOUNT : 4, STATUS : 5, COMP_CODE: 6, DOCID : 7

    for ($row = 2; $row <= $highestRow; $row++) {
        if($worksheet->getCellByColumnAndRow(1, $row)->getValue() == ''|| $worksheet->getCellByColumnAndRow(2, $row)->getValue() == '' || $worksheet->getCellByColumnAndRow(3, $row)->getValue() == '' || $worksheet->getCellByColumnAndRow(4, $row)->getValue() == '' || $worksheet->getCellByColumnAndRow(5, $row)->getValue() == '' || $worksheet->getCellByColumnAndRow(6, $row)->getValue() == '' || $worksheet->getCellByColumnAndRow(7, $row)->getValue() == '')
        {
            if($worksheet->getCellByColumnAndRow(7, $row)->getValue() != '')
            {
                $commentaryLog = "Error al importar la factura con DOCID = ".$worksheet->getCellByColumnAndRow(7, $row)->getValue().", por falta de datos";
            }
            else if($worksheet->getCellByColumnAndRow(2, $row)->getValue() != '')
            {
                $commentaryLog = "Error al importar la factura número = ".$worksheet->getCellByColumnAndRow(2, $row)->getValue().", por falta de datos";
            }
            else
            {
                $commentaryLog = "Error al importar una factura por falta de datos";
            }
            $sqlLogIn = "INSERT INTO logs (idUser, levelLog, dateLog, commentaryLog, source) VALUES (NULL,3, now(), '".$commentaryLog."', 'Importacion SAP')";  //PONER CAMPOSS S DASD ASDFASDFASDF
            $conn->query($sqlLogIn);
            continue;
        }
        //verificar si existe compaÃ±ina proveedora en la base de datos 
        $sqlCompanySel = "Select idCompany from company where cuit = '".$worksheet->getCellByColumnAndRow(1, $row)->getValue()."'";
        //echo $key . "   $     " .var_dump($value) ."<br>";
        $resultCompany = $conn->query($sqlCompanySel);
        if($resultCompany->num_rows == 0) //AÃ±ade la compaÃ±ia
        {
            $sqlCompanyIn = "INSERT INTO company (cuit) VALUES ('".$worksheet->getCellByColumnAndRow(1, $row)->getValue()."')";
            $conn->query($sqlCompanyIn);
            $resultCompany = $conn->query($sqlCompanySel);
        }
        
        $companyProvider = $resultCompany->fetch_row();

        //Selecciona el id de la compaÃ±ia cliente (, o alterno)
        $sqlClientSel = "Select idCompany from company where cuit = '".$worksheet->getCellByColumnAndRow(6, $row)->getValue()."'"; //rutCliente (ahora es un codigo)
        $resultClient = $conn->query($sqlClientSel);
        if($resultClient->num_rows == 0) //AÃ±ade la compaÃ±ia
        {
            $sqlClientIn = "INSERT INTO company (cuit) VALUES ('".$worksheet->getCellByColumnAndRow(6, $row)->getValue()."')"; //rutCliente (ahora es un codigo)
            $conn->query($sqlClientIn);
            
            $resultClient = $conn->query($sqlClientSel);
            
        }
        $companyClient = $resultClient->fetch_row();

        //Busca si el estado que viene es nuevo, y en ese caso lo agrega
        $sqlStatusSel = "Select idInvoiceStatus from invoicestatus where idInvoiceStatus = ".$worksheet->getCellByColumnAndRow(5, $row)->getValue();
        $resultStatus = $conn->query($sqlStatusSel);
        if($resultStatus->num_rows == 0) //AÃ±ade el estado si no existe
        {
            $sqlStatusIn = "INSERT INTO invoicestatus (idInvoiceStatus) VALUES (".$worksheet->getCellByColumnAndRow(5, $row)->getValue().")";
            $conn->query($sqlStatusIn);
        }
        

        $year = substr($worksheet->getCellByColumnAndRow(3, $row)->getValue(), 0, 4);
        $month = substr($worksheet->getCellByColumnAndRow(3, $row)->getValue(), 4, 2);
        $day = substr($worksheet->getCellByColumnAndRow(3, $row)->getValue(), 6, 2);
        $dateEmission =  $year . "-" .$month."-".$day;

        $sqlInvoiceSel = "Select idInvoice from invoice where numberInvoice = ".$worksheet->getCellByColumnAndRow(2, $row)->getValue();
        $resultInvoice = $conn->query($sqlInvoiceSel);
        if($resultInvoice->num_rows == 1) //AÃ±ade la factura si no existe
        {
            //Si ya existe la factura, actualiza los datos
            $sqlInvoiceUpd = "UPDATE invoice set idStatusInvoice = ".$worksheet->getCellByColumnAndRow(5, $row)->getValue().", idCompanyProvider= ".$companyProvider[0].", dateEmission = '".$dateEmission."', datePayment = '2018-01-01', dateExpiration = '2018-01-01', invoiceTotalAmount = ".$worksheet->getCellByColumnAndRow(4, $row)->getValue().", idCompanyClient = ".$companyClient[0]." WHERE numberInvoice = ".$worksheet->getCellByColumnAndRow(2, $row)->getValue();
            $conn->query($sqlInvoiceUpd);
        }
        else {
            $sqlInvoicesIn =  "INSERT INTO invoice (idInvoice, idStatusInvoice, idCompanyProvider, numberInvoice, dateEmission, datePayment, dateExpiration, invoiceTotalAmount, idCompanyClient) values (".$worksheet->getCellByColumnAndRow(7, $row)->getValue().",".$worksheet->getCellByColumnAndRow(5, $row)->getValue().",".$companyProvider[0].",".$worksheet->getCellByColumnAndRow(2, $row)->getValue().",'".$dateEmission."','2018-01-01','2018-01-01',".$worksheet->getCellByColumnAndRow(4, $row)->getValue().",".$companyClient[0].")";
            $conn->query($sqlInvoicesIn);
            
        }
       
    }

     app()->flashMessage->addMessage('success', Yii::t('app', 'Se importaron las facturas correctamente'));
            $this->redirect(array('index'));
}
    public function actionChangePsw()
    {
        $models = SapParameters::model()->findByPk(1);

        if(isset($_POST['psw']))
        {
            $models->passwd = $models->encriptar($_POST['psw']);
        }
        


        if($models->save())
        {
            app()->flashMessage->addMessage('success', Yii::t('app', 'Se actualizo la contraseña correctamente'));
            $this->redirect(array('index'));
        }
        else 
        {
             app()->flashMessage->addMessage('error', Yii::t('app', $models->passwd));
            $this->redirect(array('index'));
        }
    }
}
        
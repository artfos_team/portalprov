<?php

class CategoryRetainController extends AdminController
{
	/**
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
	 */
    public function actionIndex(){
		$model = new CategoryRetain();
		$model->unsetAttributes();

		if(isset($_GET['CategoryRetain']))
		{
			$model->attributes = $_GET['CategoryRetain'];
		}

		$this->render('index', array(
			'model' => $model,
		));
    }
    
	public function actionCreate()
	{
		$model = new CategoryRetain();
		
		if(isset($_POST['CategoryRetain']))
       	{
			$model->attributes=$_POST['CategoryRetain'];
			$test = $model->idRetentionRegime;
			echo $test;
			if ($model->idRetentionRegime == 10) {
				$model->suss = 1;
				$model->vat = 0;
				$model->gain = 0;
				$model->ib = 0;
			} else if($model->idRetentionRegime == 11) {
				$model->suss = 0;
				$model->vat = 0;
				$model->gain = 0;
				$model->ib = 1;
			} else if($model->idRetentionRegime == 2) {
				$model->suss = 0;
				$model->vat = 0;
				$model->gain = 1;
				$model->ib = 0;
			} else if($model->idRetentionRegime == 2 || $model->idRetentionRegime == 3 || $model->idRetentionRegime == 7 || $model->idRetentionRegime == 8 || $model->idRetentionRegime == 9) {
				$model->suss = 0;
				$model->vat = 0;
				$model->gain = 1;
				$model->ib = 0;
			} else if($model->idRetentionRegime == 5 || $model->idRetentionRegime == 6) {
				$model->suss = 0;
				$model->vat = 1;
				$model->gain = 0;
				$model->ib = 0;
			}
	        if($model->save())
	        {
	            $this->redirect(array('index'));
	        }
        }
		$this->render('form', array('model'=>$model));
    }
    
	public function actionChangeAttributeManual()
	{
		Yii::import('bootstrap.widgets.TbEditableSaver');
		$es = new TbEditableSaver('CategoryRetain');
	    $es->update();
    }

    public function actionDelete($id, $key) {
		$model = CategoryRetain::model()->findByPk(CPropertyValue::ensureInteger($id));

		if ($model instanceof CategoryRetain) 
		{			
			try {
				$model->delete($id);
			} catch (Exception $e) {
				throw new Exception($e);
				
				throw new CHttpException(500, Yii::t('app', 'The category retain is being used and cannot be deleted'));
			}
			if (!Yii::app()->getRequest()->getIsAjaxRequest())
			{
				$this->redirect(array('index'));
			}
		} else{
			throw new CHttpException(400, Yii::t('app', 'Your request is invalid.'));
		}
	}    

}
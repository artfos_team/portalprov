<?php

class AgreementRegimeTypeController extends AdminController
{
	/**
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
	 */
    public function actionIndex(){
		$model = new AgreementRegimeType();
		$model->unsetAttributes();

		if(isset($_GET['AgreementRegimeType']))
		{
			$model->attributes = $_GET['AgreementRegimeType'];
		}

		$this->render('index', array(
			'model' => $model,
		));
    }
    
	public function actionCreate()
	{
		$model = new AgreementRegimeType();
		
		if(isset($_POST['AgreementRegimeType']))
       	{
            $model->attributes=$_POST['AgreementRegimeType'];
	        if($model->save())
	        {
	            $this->redirect(array('index'));
	        }
        }
		$this->render('form', array('model'=>$model));
    }
    
	public function actionChangeAttributeManual()
	{
		Yii::import('bootstrap.widgets.TbEditableSaver');
		$es = new TbEditableSaver('AgreementRegimeType');
	    $es->update();
    }

    public function actionDelete($id, $key) {
		$model = AgreementRegimeType::model()->findByPk(CPropertyValue::ensureInteger($id));

		if ($model instanceof AgreementRegimeType) 
		{			
			try {
				$model->delete($id);
			} catch (Exception $e) {
				throw new Exception($e);
				
				throw new CHttpException(500, Yii::t('app', 'The agreement regime type is being used and cannot be deleted'));
			}
			if (!Yii::app()->getRequest()->getIsAjaxRequest())
			{
				$this->redirect(array('index'));
			}
		} else{
			throw new CHttpException(400, Yii::t('app', 'Your request is invalid.'));
		}
	}    

}
<?php

class CustomEmailController extends AdminController
{
	/**
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
	 */
    public function actionIndex(){
		$model = new CustomEmail();
		$model->unsetAttributes();

		if(isset($_GET['CustomEmail']))
		{
			$model->attributes = $_GET['CustomEmail'];
		}

		$this->render('index', array(
			'model' => $model,
		));
    }
    
	public function actionCreate()
	{
		$model = new CustomEmail();
		
		if(isset($_POST['CustomEmail']))
       	{
            $model->attributes=$_POST['CustomEmail'];
	        if($model->save())
	        {
	            $this->redirect(array('index'));
	        }
        }
		$this->render('form', array('model'=>$model));
    }
    
	public function actionChangeAttributeManual()
	{
		Yii::import('bootstrap.widgets.TbEditableSaver');
		$es = new TbEditableSaver('CustomEmail');
	    $es->update();
    }

    public function actionDelete($id, $key) {
		$model = CustomEmail::model()->findByPk(CPropertyValue::ensureInteger($id));

		if ($model instanceof CustomEmail) 
		{			
			try {
				$model->delete($id);
			} catch (Exception $e) {
				throw new Exception($e);
				
				throw new CHttpException(500, Yii::t('app', 'The custom mail is being used and cannot be deleted'));
			}
			if (!Yii::app()->getRequest()->getIsAjaxRequest())
			{
				$this->redirect(array('index'));
			}
		} else{
			throw new CHttpException(400, Yii::t('app', 'Your request is invalid.'));
		}
	}
	
	private function displayCustomEmail($model, $update = false) {

        if (isset($_POST['CustomEmail'])) {
            $model->attributes = $_POST['CustomEmail'];

            if ($model->save()) {
                Yii::app()->flashMessage->addMessage('success', Yii::t('app', 'Cuerpo de Email guardado correctamente'));
                $this->redirect(array('index'));
            }
        }

        $this->render('edit_body_email', array('model' => $model, 'update' => $update));
    }

	public function actionEditBodyEmail($id)
    {
        if (isset($id)) {
            $model = CustomEmail::model()->findByPk($id);

            if (!isset($model)) {
                throw new Exception('Email no encontrado por su correspondiente id: '. $id);
            }

            $this->displayCustomEmail($model, true);
        }
        else {
            throw new Exception('Id Email invalido: '. $id);
        }
    }
}
<?php

class DocumentController extends AdminController
{
	/**
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
	 */
	public function actionIndex(){
		$model = new Document();
		$model->unsetAttributes();

		if(isset($_GET['Document']))
		{
			$model->attributes = $_GET['Document'];
		}

		$this->render('index', array(
			'model' => $model,
		));
	}

	public function actionChangeAttributeDocument()
	{
		Yii::import('bootstrap.widgets.TbEditableSaver');
		$es = new TbEditableSaver('Document');
	    $es->update();
	}

	public function actionDelete($id, $key) {
		$model = Document::model()->findByPk(CPropertyValue::ensureInteger($id));

		if ($model instanceof Document) 
		{			
			try {
				$model->deleteDocument($id);
			} catch (Exception $e) {
				throw new Exception($e);
				
				throw new CHttpException(500, Yii::t('app', 'The document is being used and cannot be deleted'));
			}
			if (!Yii::app()->getRequest()->getIsAjaxRequest())
			{
				$this->redirect(array('index'));
			}
		} else{
			throw new CHttpException(400, Yii::t('app', 'Your request is invalid.'));
		}
	}

	/**
	 * Creates a new Company
	 */ 
	public function actionCreate()
	{
		$model = new Document();
		
		if(isset($_POST['Document']))
       	{
	        $model->attributes=$_POST['Document'];
	        //$model->_logo=CUploadedFile::getInstance($model,'_logo');
	        if($model->save())
	        {
	            $this->redirect(array('index'));
	        }
        }
		$this->render('form', array('model'=>$model));
	}


	/**
	 * updates a company $id
	 */ 
	public function actionUpdate($id, $key)
	{
		$model = Company::model()->findByPk(CPropertyValue::ensureInteger($id));
		
		if($model instanceof Company && strcmp($key, uKey('updateComp'.$model->primaryKey))==0)
		{
			if(isset($_POST['Company']))
	       	{
		        $model->attributes=$_POST['Company'];
		        if($model->save())
		        {
		            $this->redirect(array('index'));
		        }
	        }
			$this->render('form', array('model'=>$model, 'update'=>true));
			return;
		}
		
		throw new CHttpException(404, "Empresa inválida");
	}


	public function actions()
	{
		return array(
			'toggle' => array(
				'class'=>'bootstrap.actions.TbToggleAction',
				'modelName' => 'Document',
			),
		);
	}

	public function actionContactView($company, $randNumb = null)
	{
		if ($randNumb===null)
		{
			$randNumb = time() . rand(10,99);
		}
		$company = Company::model()->findByPk($company);
		if ($company instanceof Company)
		{
			$contact = new CompanyContact();
			$contact->unsetAttributes();
			$contact->idCompany = $company->idCompany;
			if (isset($_GET['CompanyContact']))
			{
				$contact->setAttributes($_GET['CompanyContact']);
			}			

			$newContact = new CompanyContact();
			if(isset($_POST['ajax']) && $_POST['ajax']==='addNew' . $randNumb)
			{
				$newContact->idCompany = $company->idCompany;
				echo CActiveForm::validate($newContact);
				Yii::app()->end();
			}
			elseif (isset($_POST['CompanyContact']))
			{				
				$newContact->attributes = $_POST['CompanyContact'];
				$newContact->idCompany = $company->idCompany;
				if ($newContact->validate())
				{					
					$newContact->save();
					echo CJSON::encode(array('success'=>true));
					Yii::app()->end();					
				}
				echo CActiveForm::validate($newContact);
				Yii::app()->end();
			}
			
			$this->renderPartial('contactView', 
				array('company'=>$company, 
					'contact'=>$contact, 
					'newContact'=>$newContact,
					'randNumb'=>$randNumb), 
				false, true);
			app()->end();
		}
	}
	public function actionViewCalification($company)
	{
		$model = Company::model()->findByPk(CPropertyValue::ensureInteger($company));
		if(isset($_POST['Company']))
		{
			$model->attributes=$_POST['Company'];
			$model->reasonCalification = $_POST['Company']['reasonCalification'];
			if($model->save())
			{
				$this->redirect(array('index'));
			}
		}
		if($model instanceof Company)
		{
			$this->render('viewCalification', array('model'=>$model, 'company'=>$company));
			app()->end();
		}

		throw new CHttpException(404, Yii::t('app', 'Invalid company'));
	}

	public function actionAddressView($company, $randNumb = null)
	{
		if ($randNumb===null)
		{
			$randNumb = time() . rand(10,99);
		}
		$company = Company::model()->findByPk($company);
		if ($company instanceof Company)
		{
			$address = new CompanyAddress();
			$address->unsetAttributes();
			$address->idCompany = $company->idCompany;
			if (isset($_GET['CompanyAddress']))
			{
				$address->setAttributes($_GET['CompanyAddress']);
			}

			$newAddress = new CompanyAddress();
			if(isset($_POST['ajax']) && $_POST['ajax']==='addNew' . $randNumb)
			{
				$newAddress->idCompany = $company->idCompany;
				echo CActiveForm::validate($newAddress);
				Yii::app()->end();
			}
			elseif (isset($_POST['CompanyAddress']))
			{
				$newAddress->attributes = $_POST['CompanyAddress'];
				$newAddress->idCompany = $company->idCompany;
				if ($newAddress->validate())
				{
					$newAddress->save();
					
					$addresses = CHtml::listData(CompanyAddress::model()->findAll('idCompany = :idc', array(':idc'=>$company->primaryKey)),
							'idCompanyAddress', 'address');      	
			
					$html = '';
					
					foreach($addresses as $key=>$value){
						$html .= CHtml::tag('option',
							   array('value'=>$key),CHtml::encode($value),true);
					}
					
					echo CJSON::encode(array('success'=>true, 'addresses'=>$html));
					Yii::app()->end();
				}
				echo CActiveForm::validate($newAddress);
				Yii::app()->end();
			}

			$this->renderPartial('addressView', 
				array('company'=>$company, 
					'address'=>$address,
					'newAddress'=>$newAddress,
					'randNumb'=>$randNumb), 
				false, true);
			app()->end();
		}
	}
	
	/**
	 * Returns the provinces of the current country
	 */
	public function actionUpdateProvince($model = null)
	{
		$data = Province::model()->findAll(array('condition'=>'idCountry=:idCountry', 
			'order'=>'province ASC',
		    'params'=>array(':idCountry'=>(int) $_POST[$model]['idCountry'])));

		$data=CHtml::listData($data,'idProvince','province');
		
		echo CHtml::tag('option', array('value'=>''), Yii::t('app','Select an option'), true);
		foreach($data as $value=>$name)
		{
			echo CHtml::tag('option',
		       array('value'=>$value), CHtml::encode($name), true);
		}
	}

	/**
	 * Returns the cities of the current province
	 */
	public function actionUpdateCity($model = null)
	{
		$data = City::model()->findAll(array('condition'=>'idProvince=:idProvince', 
			'order'=>'city ASC',
		    'params'=>array(':idProvince'=>(int) $_POST[$model]['idProvince'])));

		$data=CHtml::listData($data,'idCity','city');
		
		echo CHtml::tag('option', array('value'=>''), Yii::t('app','Select an option'), true);
		foreach($data as $value=>$name)
		{
			echo CHtml::tag('option',
		       array('value'=>$value), CHtml::encode($name), true);
		}
	}

	/**
	 * View
	 * @param int $idParticipant
	 */
	public function actionView($company)
	{
		$model = Document::model()->findByPk(CPropertyValue::ensureInteger($company));

		if( $model instanceof Document)
		{
			$this->render('view', array('model'=>$model));
			app()->end();
		}

		throw new CHttpException(404, Yii::t('app', 'Invalid company'));
	}

}
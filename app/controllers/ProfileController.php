<?php

class ProfileController extends AdminController
{
	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'users'=>array('@'),
			),
			array('deny'),
		);
	}

	/**
	 * Allows to change the password for the current user
	 */
    public function actionChangePassword()
	{
		$model = user()->loadModel();
		$model->scenario = 'changePassword';
		$userClass = get_class($model);

		if(is_null($userClass))
		{
			throw new CException('Invalid user class');
		}

		if(isset($_POST[$userClass]))
		{			
			$model->setAttributes($_POST[$userClass]);
			if($model->save())
			{
				app()->flashMessage->addMessage('success', Yii::t('app', 'Password successfully changed'));
				$this->redirect(array('/site/index'));
			}
		}
		
		$model->password = $model->_repeatPassword = $model->_oldPassword = '';
		$this->render('change_password', array('model'=>$model));
	}
}
<?php

class CountryController extends AdminController
{
	/**
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
	 */
    public function actionIndex(){
		$model = new Country();
		$model->unsetAttributes();

		if(isset($_GET['Country']))
		{
			$model->attributes = $_GET['Country'];
		}

		$this->render('index', array(
			'model' => $model,
		));
    }
    
	public function actionCreate()
	{
		$model = new Country();
		
		if(isset($_POST['Country']))
       	{
            $model->attributes=$_POST['Country'];
	        if($model->save())
	        {
	            $this->redirect(array('index'));
	        }
        }
		$this->render('form', array('model'=>$model));
    }
    
	public function actionChangeAttributeManual()
	{
		Yii::import('bootstrap.widgets.TbEditableSaver');
		$es = new TbEditableSaver('Country');
	    $es->update();
    }

    public function actionDelete($id, $key) {
		$model = Country::model()->findByPk(CPropertyValue::ensureInteger($id));

		if ($model instanceof Country) 
		{			
			try {
				$model->delete($id);
			} catch (Exception $e) {
				throw new Exception($e);
				
				throw new CHttpException(500, Yii::t('app', 'The country is being used and cannot be deleted'));
			}
			if (!Yii::app()->getRequest()->getIsAjaxRequest())
			{
				$this->redirect(array('index'));
			}
		} else{
			throw new CHttpException(400, Yii::t('app', 'Your request is invalid.'));
		}
	}    

}
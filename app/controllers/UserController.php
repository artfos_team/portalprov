<?php

class UserController extends AdminController
{
	/**
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
	 */
	public function actionIndex($export=false){
		$model = new User();
		$model->unsetAttributes();

		if(isset($_GET['User']))
		{
			$model->attributes = $_GET['User'];
		}

		if($export)
		{
			Yii::import('application.components.exportXls.ExportXLSHelper');
			$helper = new ExportXLSHelper();
			$helper->model = $model;
			$helper->filename = 'usuarios-';
			$helper->header = 'Usuarios al ';
			$dp = $model->search();
			$dp->pagination = false;
			$helper->data = $dp->getData();
			$helper->attributes = $model->getXlsAttributes();
			$helper->generateSheet();
			app()->end();
		}

		$this->render('index', array(
			'model' => $model,
		));
	}

	public function actionChangeAttributeUser()
	{
		Yii::import('bootstrap.widgets.TbEditableSaver');
		$es = new TbEditableSaver('User');
	    $es->update();
	}

	public function actionDelete($id, $key) {
		$model = User::model()->findByPk(CPropertyValue::ensureInteger($id));

		if (Yii::app()->getRequest()->getIsPostRequest() && $model instanceof User &&
			strcmp($key, uKey("deleteUser".$model->primaryKey))==0) 
		{			
			try {
				$model->delete();
			} catch (Exception $e) {
				throw new CHttpException(500, Yii::t('app', 'The User is being used and cannot be deleted'));
			}
			if (!Yii::app()->getRequest()->getIsAjaxRequest())
			{
				$this->redirect(array('index'));
			}
		} else{
			throw new CHttpException(400, Yii::t('app', 'Your request is invalid.'));
		}
	}

	/**
	 * updates a new user
	 */ 
	public function actionUpdate($id, $key)
	{
		$model = User::model()->findByPk(CPropertyValue::ensureInteger($id));
		
		if($model instanceof User && strcmp(uKey("updateUser".$model->primaryKey), $key)==0)
		{
			if(isset($_POST['User']))
	       	{
		        $model->attributes=$_POST['User'];
		        if($model->save())
		        {
		            $this->redirect(array('index'));
		        }
	        }


			$this->render('form', array('model'=>$model, 'update'=>true));
			return;
		}

		throw new CHttpException(404, Yii::t('app', 'Invalid user'));
	}	

	/**
	 * Creates a new User
	 */ 
	public function actionCreate()
	{
		$model = new User('create');
		
		if(isset($_POST['User']))
       	{
	        $model->attributes=$_POST['User'];
	        $model->_signature=CUploadedFile::getInstance($model,'_signature');
	        
	        if($model->save())
	        {
	            $this->redirect(array('index'));
	        }
        }
		$this->render('form', array('model'=>$model));
	}

	public function actions()
	{
		return array(
			'toggle' => array(
				'class'=>'bootstrap.actions.TbToggleAction',
				'modelName' => 'User',
			),
		);
	}

	/**
	 * Displays details of users and permissions
	 * @param  int $idUser
	 */
	public function actionUserRolesPermissions($idUser, $key)
	{
		$user = User::model()->findByPk($idUser);
		if ($user instanceof User && strcmp($key, uKey("perms".$user->primaryKey))==0)
		{	
			$roles = Roles::model()->findAll('visible = 1');

			$this->renderPartial('userRoles', array(
				'user'=>$user,
				'roles'=>$roles,
			), false, true);
			return;
		}

		throw new CHttpException(404, "Invalid user");
	}

	/**
	 * Check whether a user identified with pk $user has role identified with pk $role
	 * @param int $idUser
	 * @param int $idRole
	 * @return string Json encoded array used to display if user has a role or not via ajax
	 */ 
	public function actionToggleRolesForUser($idUser, $idRole)
	{
		$text = Yii::t('app', 'No');
		$idUser = CPropertyValue::ensureInteger($idUser);
		$idRole = CPropertyValue::ensureInteger($idRole);
		
		$uhr = UserHasRoles::model()->findByPk(array('idUser'=>$idUser, 'rolesId'=>$idRole));
		
		if ($uhr instanceof UserHasRoles)
		{
			//Role already exists. Delete it.
			$uhr->delete();
		}
		else
		{
			//otherwise, create it
			$uhr = new UserHasRoles();
			$uhr->idUser = $idUser;
			$uhr->rolesId = $idRole;
			if($uhr->save())
			{
				$text = Yii::t('app', 'Yes');
			}
		}
		echo CJSON::encode(array('success'=>true, 'text'=>$text));
	}
	
}
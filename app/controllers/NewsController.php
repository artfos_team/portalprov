<?php

class NewsController extends AdminController
{
	/**
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
	 */
    public function actionIndex(){
		$model = new News();
		$model->unsetAttributes();

		if(isset($_GET['ews']))
		{
			$model->attributes = $_GET['News'];
		}

		$this->render('index', array(
			'model' => $model,
		));
    }
    
	public function actionCreate()
	{
		$model = new News();
		
		if(isset($_POST['News']))
       	{
            $model->attributes=$_POST['News'];
	        if($model->save())
	        {
	            $this->redirect(array('index'));
	        }
        }
		$this->render('form', array('model'=>$model));
    }
    
	public function actionChangeAttributeManual()
	{
		Yii::import('bootstrap.widgets.TbEditableSaver');
		$es = new TbEditableSaver('News');
	    $es->update();
    }

    public function actionDelete($id, $key) {
		$model = News::model()->findByPk(CPropertyValue::ensureInteger($id));

		if ($model instanceof News) 
		{			
			try {
				$model->delete($id);
			} catch (Exception $e) {
				throw new Exception($e);
				
				throw new CHttpException(500, Yii::t('app', 'The News is being used and cannot be deleted'));
			}
			if (!Yii::app()->getRequest()->getIsAjaxRequest())
			{
				$this->redirect(array('index'));
			}
		} else{
			throw new CHttpException(400, Yii::t('app', 'Your request is invalid.'));
		}
    }
    
	private function displayNews($model, $update = false) {

        if (isset($_POST['News'])) {
            $model->attributes = $_POST['News'];

            if ($model->save()) {
                Yii::app()->flashMessage->addMessage('success', Yii::t('app', 'Cuerpo de Noticia guardado correctamente'));
                $this->redirect(array('index'));
            }
        }

        $this->render('edit_body_news', array('model' => $model, 'update' => $update));
    }

	public function actionEditBodyNews($id)
    {
        if (isset($id)) {
            $model = News::model()->findByPk($id);

            if (!isset($model)) {
                throw new Exception('Noticia no encontrado por su correspondiente id: '. $id);
            }

            $this->displayNews($model, true);
        }
        else {
            throw new Exception('Id Noticia invalido: '. $id);
        }
    }

}
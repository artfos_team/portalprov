<?php

class BankController extends AdminController
{
	/**
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
	 */
    public function actionIndex(){
		$model = new Bank();
		$model->unsetAttributes();

		if(isset($_GET['Bank']))
		{
			$model->attributes = $_GET['Bank'];
		}

		$this->render('index', array(
			'model' => $model,
		));
    }
    
	public function actionCreate()
	{
		$model = new Bank();
		
		if(isset($_POST['Bank']))
       	{
            $model->attributes=$_POST['Bank'];
	        if($model->save())
	        {
	            $this->redirect(array('index'));
	        }
        }
		$this->render('form', array('model'=>$model));
    }
    
	public function actionChangeAttributeManual()
	{
		Yii::import('bootstrap.widgets.TbEditableSaver');
		$es = new TbEditableSaver('Bank');
	    $es->update();
    }

    public function actionDelete($id, $key) {
		$model = Bank::model()->findByPk(CPropertyValue::ensureInteger($id));

		if ($model instanceof Bank) 
		{			
			try {
				$model->delete($id);
			} catch (Exception $e) {
				throw new Exception($e);
				
				throw new CHttpException(500, Yii::t('app', 'The bank is being used and cannot be deleted'));
			}
			if (!Yii::app()->getRequest()->getIsAjaxRequest())
			{
				$this->redirect(array('index'));
			}
		} else{
			throw new CHttpException(400, Yii::t('app', 'Your request is invalid.'));
		}
	}    

}
<?php

    require_once dirname(__FILE__).'/../../vendor/autoload.php';

    class EmailParametersController extends AdminController
    {

    	/**
    	 * This is the default 'index' action that is invoked
    	 * when an action is not explicitly requested by users.
    	 */
    	public function actionIndex()
    	{
    		$model = new EmailParameters();
    		$model->unsetAttributes();

    		if (isset($_GET['EmailParameters'])){
    			$model->setAttributes($_GET['EmailParameters']);
    		}
            
    		$this->render('index', array(
    			'model' => $model,
    		));
    	}



    	public function actionChangeAttributeEmailParameters()
    	{
    		Yii::import('bootstrap.widgets.TbEditableSaver');
    		$es = new TbEditableSaver('EmailParameters');
    	    $es->update();
    	}

    	/*public function actionDelete($id, $key)
    	{
    		$model = SapParameters::model()->findByPk(CPropertyValue::ensureInteger($id));
    		if (Yii::app()->getRequest()->getIsPostRequest() && $model instanceof SapParameters && 
    			strcmp($key, uKey('deletePart'.$model->primaryKey))==0) 
    		{			
    			try
    			{
    				$model->delete();
    			}
    			catch (Exception $e)
    			{
    				$model->participantDeleted = 1;
    				$model->save(false, array('participantDeleted'));
    			}
    			
    			if (!Yii::app()->getRequest()->getIsAjaxRequest())
    			{
    				$this->redirect(array('index'));
    			}
    		}
    		else
    		{
    			throw new CHttpException(400, Yii::t('app', 'Your request is invalid.'));
    		}
    	}*/
    	
        public function actionCreate(){
            $model = new EmailParameters();

            if(isset($_POST['EmailParameters'])){
                $model->attributes=$_POST['EmailParameters'];
                if($model->save()){
                    $this->redirect(array('index'));
                }
            }
            $this->render('create', array('model'=>$model));
        }

    	/**
    	 * Updates a participant by $id
    	 */ 
    	public function actionUpdate($id, $key)
    	{
    		$model = EmailParameters::model()->findByPk(CPropertyValue::ensureInteger($id));

    		if($model instanceof EmailParameters && strcmp($key, uKey('updatePart'.$model->primaryKey))==0)
    		{
    			if(isset($_POST['EmailParameters' ]))
    	       	{
    		        $model->attributes=$_POST['EmailParameters' ];
    		        if($model->save())
    		        {
    		            $this->redirect(array('index'));
    		        }
    	        }
    			$this->render('form', array('model'=>$model));
    			return;
    		}
    		
    		throw new CHttpException(404, "InvoiceStatus inválido");
    	}

    	/**
    	 * View
    	 * @param int $idInvoice
    	 */
    	public function actionView($invoiceStatus)
    	{
    		$model = SapParameters::model()->findByPk(CPropertyValue::ensureInteger($invoiceStatus));

    		if($model instanceof SapParameters)
    		{
    			$this->render('view', array('model'=>$model, 'SapParameters' =>$invoice));
    			app()->end();
    		}

    		throw new CHttpException(404, Yii::t('app', 'Invalid invoice'));
    	}

        // on your controller
        // example code for action rendering the relational data
        public function actionRelational()
        {
            // partially rendering "_relational" view
            $this->renderPartial('_relational', array(
                'id' => Yii::app()->getRequest()->getParam('id'),
                'gridDataProvider' => $this->getGridDataProvider(),
                'gridColumns' => $this->getGridColumns()
            ));
        }

        public function actionPing()
        {
            $models = SapParameters::model()->findAll();
            $host = $models[0]->ashost;

            $comando = "ping -c 1 ".$host;
            $output = shell_exec($comando);

            if(strpos($output, '1 received'))
            {
                app()->flashMessage->addMessage('success', Yii::t('app', 'Hay conexión con el servidor'));
                $this->redirect(array('index'));
            }
            else 
            {
                 app()->flashMessage->addMessage('error', Yii::t('app', 'No hay conexión con el servidor'));
                $this->redirect(array('index'));
            }
        }

        
    public function actionChangePsw()
    {
        $models = SapParameters::model()->findAll();

        if(isset($_POST['psw']))
        {
            $models[0]->passwd = $models[0]->encriptar($_POST['psw']);
        }
        


        if($models[0]->save())
        {
            app()->flashMessage->addMessage('success', Yii::t('app', 'Se actualizo la contraseña correctamente'));
            $this->redirect(array('index'));
        }
        else 
        {
             app()->flashMessage->addMessage('error', Yii::t('app', 'No se pudo actualizar la contraseña'));
            $this->redirect(array('index'));
        }
    }
}
        
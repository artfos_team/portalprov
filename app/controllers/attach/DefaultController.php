<?php

class DefaultController extends Controller
{
	public function actionDeleteFile($id, $key)
	{
		$item = ItemHasAttachment::model()->findByPk($id);
		
		if($item instanceof ItemHasAttachment && strcmp($item->getKey('delete'), $key)==0)
		{
			$item->delete();
			return;
		}
		throw new CHttpException(404, Yii::t('attach', 'Attachment not found'));
	}

	public function actionSearch(){
		$criteria = new CDbCriteria();

		if(isset($_POST['search_txt'])){
			$q = CPropertyValue::ensureString($_POST['search_txt']);

			if(strlen($q)>0){
				$criteria->condition = 'title like :q';
				$criteria->params[':q'] = '%'.$q.'%';
			}
		}


		$files = ItemHasAttachment::model()->findAll($criteria);

		$html = $this->renderPartial('search_results', array('files'=>$files), true, false);

		echo $html;
	}

}
<?php

    class InvoiceStatusController extends AdminController
    {
    	/**
    	 * This is the default 'index' action that is invoked
    	 * when an action is not explicitly requested by users.
    	 */
    	public function actionIndex($export=false, $deleted = 0)
    	{
    		$model = new Invoicestatus();
    		$model->unsetAttributes();

    		if (isset($_GET['Invoicestatus'])){
    			$model->setAttributes($_GET['Invoicestatus']);
    		}


    		$this->render('index', array(
    			'model' => $model,
    		));
    	}

    	public function actionChangeAttributeInvoicestatus()
    	{
    		Yii::import('bootstrap.widgets.TbEditableSaver');
    		$es = new TbEditableSaver('Invoicestatus');
    	    $es->update();
    	}

    	public function actionDelete($id, $key)
    	{
    		$model = Invoicestatus::model()->findByPk(CPropertyValue::ensureInteger($id));
    		if (Yii::app()->getRequest()->getIsPostRequest() && $model instanceof Invoicestatus && 
    			strcmp($key, uKey('deletePart'.$model->primaryKey))==0) 
    		{			
    			try
    			{
    				$model->delete();
    			}
    			catch (Exception $e)
    			{
    				$model->participantDeleted = 1;
    				$model->save(false, array('participantDeleted'));
    			}
    			
    			if (!Yii::app()->getRequest()->getIsAjaxRequest())
    			{
    				$this->redirect(array('index'));
    			}
    		}
    		else
    		{
    			throw new CHttpException(400, Yii::t('app', 'Your request is invalid.'));
    		}
    	}
    	
        public function actionCreate(){
            $model = new Invoicestatus();

            if(isset($_POST['Invoicestatus'])){
                $model->attributes=$_POST['Invoicestatus'];
                if($model->save()){
                    $this->redirect(array('index'));
                }
            }
            $this->render('create', array('model'=>$model));
        }

    	/**
    	 * Updates a participant by $id
    	 */ 
    	public function actionUpdate($id, $key)
    	{
    		$model = Invoicestatus::model()->findByPk(CPropertyValue::ensureInteger($id));

    		if($model instanceof Invoicestatus && strcmp($key, uKey('updatePart'.$model->primaryKey))==0)
    		{
    			if(isset($_POST['Invoicestatus' ]))
    	       	{
    		        $model->attributes=$_POST['Invoicestatus' ];
    		        if($model->save())
    		        {
    		            $this->redirect(array('index'));
    		        }
    	        }
    			$this->render('form', array('model'=>$model));
    			return;
    		}
    		
    		throw new CHttpException(404, "Invoicestatus inválido");
    	}

    	/**
    	 * View
    	 * @param int $idInvoice
    	 */
    	public function actionView($invoiceStatus)
    	{
    		$model = Invoicestatus::model()->findByPk(CPropertyValue::ensureInteger($invoiceStatus));

    		if($model instanceof Invoicestatus)
    		{
    			$this->render('view', array('model'=>$model, 'Invoicestatus' =>$invoice));
    			app()->end();
    		}

    		throw new CHttpException(404, Yii::t('app', 'Invalid invoice'));
    	}

        // on your controller
        // example code for action rendering the relational data
        public function actionRelational()
        {
            // partially rendering "_relational" view
            $this->renderPartial('_relational', array(
                'id' => Yii::app()->getRequest()->getParam('id'),
                'gridDataProvider' => $this->getGridDataProvider(),
                'gridColumns' => $this->getGridColumns()
            ));
        }

    }
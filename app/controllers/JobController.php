<?php

class JobController extends AdminController
{
	/**
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
	 */
    public function actionIndex(){
		$model = new Job();
		$model->unsetAttributes();

		if(isset($_GET['Job']))
		{
			$model->attributes = $_GET['Job'];
		}

		$this->render('index', array(
			'model' => $model,
		));
    }
    
	public function actionCreate()
	{
		$model = new Job();
		
		if(isset($_POST['Job']))
       	{
            $model->attributes=$_POST['Job'];
	        if($model->save())
	        {
	            $this->redirect(array('index'));
	        }
        }
		$this->render('form', array('model'=>$model));
    }
    
	public function actionChangeAttributeManual()
	{
		Yii::import('bootstrap.widgets.TbEditableSaver');
		$es = new TbEditableSaver('Job');
	    $es->update();
    }

    public function actionDelete($id, $key) {
		$model = Job::model()->findByPk(CPropertyValue::ensureInteger($id));

		if ($model instanceof Job) 
		{			
			try {
				$model->delete($id);
			} catch (Exception $e) {
				throw new Exception($e);
				
				throw new CHttpException(500, Yii::t('app', 'The country is being used and cannot be deleted'));
			}
			if (!Yii::app()->getRequest()->getIsAjaxRequest())
			{
				$this->redirect(array('index'));
			}
		} else{
			throw new CHttpException(400, Yii::t('app', 'Your request is invalid.'));
		}
	}    

}
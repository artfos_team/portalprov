<?php

class ConfigurationController extends AdminController
{
	/**
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
	 */
	public function actionIndex($export=false){
		$model = new Configuration();
		$model->unsetAttributes();

		/*if(isset($_GET['Company']))
		{
			$model->attributes = $_GET['Company'];
        }*/
        
		$this->render('index', array(
			'model' => $model,
		));
	}
	
	public function actionChangeAttributeConfiguration()
	{
		Yii::import('bootstrap.widgets.TbEditableSaver');
		$es = new TbEditableSaver('Configuration');
	    $es->update();
	}
	
/*
	public function actionDelete($id, $key) {
		$model = Company::model()->findByPk(CPropertyValue::ensureInteger($id));

		if (Yii::app()->getRequest()->getIsPostRequest() && $model instanceof Company &&
			strcmp($key, uKey('deleteComp'.$model->primaryKey)) == 0) 
		{			
			try {
				$model->delete();
			} catch (Exception $e) {
				throw new CHttpException(500, Yii::t('app', 'The company is being used and cannot be deleted'));
			}
			if (!Yii::app()->getRequest()->getIsAjaxRequest())
			{
				$this->redirect(array('index'));
			}
		} else{
			throw new CHttpException(400, Yii::t('app', 'Your request is invalid.'));
		}
	}
*/
	/**
	 * Creates a new Company
	 */ 
    /*
	public function actionCreate()
	{
		$model = new Company();
		
		if(isset($_POST['Company']))
       	{
	        $model->attributes=$_POST['Company'];
	        $model->_logo=CUploadedFile::getInstance($model,'_logo');
	        if($model->save())
	        {
	            $this->redirect(array('index'));
	        }
        }
		$this->render('form', array('model'=>$model));
	}
*/

	/**
	 * updates a company $id
	 */ 
	public function actionUpdate($id, $key)
	{
		$model = Configuration::model()->findByPk(CPropertyValue::ensureInteger($id));
		
		if($model instanceof Configuration)
		{
			if(isset($_POST['Configuration']))
	       	{
		        $model->attributes=$_POST['Configuration'];
		        if($model->save())
		        {
		            $this->redirect(array('index'));
		        }
	        }
			$this->render('form', array('model'=>$model, 'update'=>true));
			return;
		}
		
		throw new CHttpException(404, "Elemento inválido");
	}


}
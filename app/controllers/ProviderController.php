<?php

require_once dirname(__FILE__).'/../../vendor/autoload.php';

class ProviderController extends Controller
{
	/**
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
	 */
	public function actionIndex($export=false)
	{
		$model = new Provider();
		$model->unsetAttributes();

		if (isset($_GET['Provider'])){
			$model->setAttributes($_GET['Provider']);
		}

        $record = new Provider();

		if (isset($_POST['Provider'])) 
        {    
            $record->attributes = $_POST['Provider'];
            $this->redirect(array('index'));
        }

		$this->render('index', array(
			'model' => $model,
			'record'=>$record,
		));
	}

	public function actionCreate($export=false)
	{
		$model = new Provider('create');
		
		if(isset($_POST['Provider']))
       	{

	        $model->attributes=$_POST['Provider'];
	      	$model->date = date('Y-m-d H:i:s');
	        if($model->save())
	        {
	            $this->redirect(array('index'));
	        }
		}
		$this->render('create', array('model'=>$model));
	}

	/**
	 * View
	 * @param int $comments
	 */
	public function actionView($provider)
	{
		$model = Provider::model()->findByPk(CPropertyValue::ensureInteger($provider));

		if($model instanceof Provider)
		{
			$this->render('view', array('model'=>$model, 'provider'=>$provider));
			app()->end();
		}

		throw new CHttpException(404, Yii::t('app', 'Invalid provider'));
	}
	public function actionViewCalification($provider)
	{
		$model = Provider::model()->findByPk(CPropertyValue::ensureInteger($provider));
		if(isset($_POST['Provider']))
		{
			$model->attributes=$_POST['Provider'];
			$model->reasonCalification = $_POST['Provider']['reasonCalification'];
			if($model->save())
			{
				$this->redirect(array('index'));
			}
		}
		if($model instanceof Provider)
		{
			$this->render('viewCalification', array('model'=>$model, 'provider'=>$provider));
			app()->end();
		}

		throw new CHttpException(404, Yii::t('app', 'Invalid provider'));
	}

    // on your controller
    // example code for action rendering the relational data
    public function actionRelational()
    {
        // partially rendering "_relational" view
        $this->renderPartial('_relational', array(
            'id' => Yii::app()->getRequest()->getParam('id'),
            'gridDataProvider' => $this->getGridDataProvider(),
            'gridColumns' => $this->getGridColumns()
        ));
    }

    public function actionChangeAttributeProvider()
	{

		$record = new Provider();

		if (isset($_POST['Provider'])) 
        {    
            $record->attributes = $_POST['Provider']; 
        }

        $estado = $record->idProviderStatus;
		$levelLog = 1;
		$source = 'ProviderController->actionChangeAttributeProvider';

		Yii::import('bootstrap.widgets.TbEditableSaver');
		$es = new TbEditableSaver('provider');
	    $es->update();
	    $commentaryLog = 'Se cambió el estado del proveedor a '.$es->model->getStatusProvider();
	    Logs::model()->log($levelLog,$commentaryLog,$source);
	}

	public function actionDelete($id)
	{
		$model = Provider::model()->findByPk(CPropertyValue::ensureInteger($id));
		if ($model instanceof Provider) 
		{			
			try
			{
				$model->delete();
			}
			catch (Exception $e)
			{
				$model->providerDeleted = 1;
				$model->idProviderStatus = 5;
				$model->save(false, array('providerDeleted'));
			}
			
			if (!Yii::app()->getRequest()->getIsAjaxRequest())
			{
				$this->redirect(array('index'));
			}
		}
		else
		{
			throw new CHttpException(400, Yii::t('app', 'Your request is invalid.'));
		}
	}

	public function actionSendMail($id)
	{
		$model = Provider::model()->findByPk(CPropertyValue::ensureInteger($id));


		if ($model instanceof Provider) 
		{			
			try
			{
					$usuario = $model->idUser0;
					$userEx = explode('@',$usuario->email);
					$user = $userEx[0];
					$psw = User::model()->randomPsw();
					$token = User::model()->encriptar($user."<".$psw);
					
					
						//This should be done in your php.ini, but this is how to do it if you don't have access to that
						$mail = new Email();
						

						//configuracion fija
						$emailParameters = EmailParameters::model()->findByPk(1);
						$customEmail = CustomEmail::model()->findByPk(1);


						//$mail->subject ="Bienvenido al sistema";
						//
						$mail->subject = $customEmail->subject;
						//
						
						//$mail->AddCC($consultant->email);
/*						$mail->body =
							'<div style="text-align: justify; font-size: 16px;">
							
								<div style="margin: 8px 0 14px 0; text-align: justify; font-size: 16px;">
									Hola, <br/>
									<br/>
								</div>
							
								<p style="text-align: justify; font-size: 16px;">Nos comunicamos con Ud. Para notificarle la activación del usuario para el proveedor'.$usuario->name.'</p>
								

								<div style="margin: 8px 0 14px 0; text-align: justify; font-size: 16px;">
									Ingrese al siguiente link para finalizar el registro:
									<br>
									<a href="'.$emailParameters->hostForMails.'/finSolicitud?t='.$token.'">Portal de Compras</a> <br/>
									<br/>
								</div>
							<div style="margin: 8px 0 14px 0; text-align: justify; font-size: 16px;">
								Saludos<br/>
								<br/>
							</div>	
							
							</div>'							
								; //Fin message HTML
*/
						eval("\$sendBody = \"$customEmail->body\";");
						$mail->body = $sendBody;

						
						try
						{
							if($mail->save() && $usuario->updatePsw($usuario->idUser,$psw))
							{
								$mail->addReceiver($usuario->email); // Email Destinatario
						
								$sql="UPDATE provider SET idProviderStatus = 2 WHERE idProvider = ".$id;
        						$command = Yii::app()->db->createCommand($sql)->execute();
        						if($command)
        						{
        							app()->flashMessage->addMessage('success', Yii::t('app', 'Mail enviado con éxito'));
        							$this->redirect(array('index'));	
        						}
        						else {
        							app()->flashMessage->addMessage('error', Yii::t('app', 'Se envió el mail, pero no se pudo cambiar al estado activo, cambielo manualmente'));
        							$this->redirect(array('index'));	
        						}			
							}
						}
						catch (Exception $ex)
						{
							throw new Exception($ex);
						}			
					}
			catch (Exception $e)
			{
				throw new Exception($e);
			}
			
			if (!Yii::app()->getRequest()->getIsAjaxRequest())
			{
				app()->flashMessage->addMessage('error', Yii::t('app', 'No se pudo enviar el mail'));
        							
				$this->redirect(array('index'));
			}
		}
		else
		{
			throw new CHttpException(400, Yii::t('app', 'Your request is invalid.'));
		}
	}
	public function actionImportExcel()
    {

        $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
        if(isset($_FILES['excelDir']['tmp_name']))
        {
			$spreadsheet = $reader->load($_FILES['excelDir']['tmp_name']);
        }
        else
        {
            app()->flashMessage->addMessage('error', Yii::t('app', 'No se encontró el archivo'));
            $this->redirect(array('index'));
		}
        $worksheet = $spreadsheet->getActiveSheet();
        echo $highestRow = $worksheet->getHighestRow();
		
		$config = include '/../config/main-environment.php';
		//$config = include 'PageController.php';
        $conn = new mysqli('localhost', $config['components']['db']['username'], $config['components']['db']['password'], 'iprove');
        //VENDOR_VAT_NO : 1, XBLNR : 2, BLDAT : 3, GROSS_AMOUNT : 4, STATUS : 5, COMP_CODE: 6, DOCID : 7

		
    for ($row = 2; $row <= $highestRow; $row++) {		
		if($worksheet->getCellByColumnAndRow(1, $row)->getValue() == ''|| $worksheet->getCellByColumnAndRow(2, $row)->getValue() === '' || $worksheet->getCellByColumnAndRow(3, $row)->getValue() === '' || $worksheet->getCellByColumnAndRow(4, $row)->getValue() === '')
			{
				if($worksheet->getCellByColumnAndRow(3, $row)->getValue() != '')
				{
					$commentaryLog = "Error al importar el usuario con email = ".$worksheet->getCellByColumnAndRow(3, $row)->getValue().", por falta de datos";
				}
				else
				{
					$commentaryLog = "Error al importar usuarios";
				}
				$sqlLogIn = "INSERT INTO logs (idUser, levelLog, dateLog, commentaryLog, source) VALUES (NULL,3, now(), '".$commentaryLog."', 'Importacion SAP')";  //PONER CAMPOSS S DASD ASDFASDFASDF
				$conn->query($sqlLogIn);
				continue;
			}
        //verificar si existe compaÃ±ina en la base de datos 
        $sqlCompanySel = "Select idCompany from company where cuit = ".$worksheet->getCellByColumnAndRow(4, $row)->getValue();

		$resultCompany = $conn->query($sqlCompanySel);
	
        if($resultCompany->num_rows == 0) //AÃ±ade la compaÃ±ia
        {
            $sqlCompanyIn = "INSERT INTO company (cuit) VALUES ('".$worksheet->getCellByColumnAndRow(4, $row)->getValue()."')";
            $conn->query($sqlCompanyIn);
            $resultCompany = $conn->query($sqlCompanySel);
        }
        /*
        $companyProvider = $resultCompany->fetch_row();

		//Selecciona el id de la compaÃ±ia cliente (, o alterno)
		
        $sqlClientSel = "Select idCompany from company where cuit = '".$worksheet->getCellByColumnAndRow(6, $row)->getValue()."'"; //rutCliente (ahora es un codigo)
        $resultClient = $conn->query($sqlClientSel);
        if($resultClient->num_rows == 0) //AÃ±ade la compaÃ±ia
        {
            $sqlClientIn = "INSERT INTO company (cuit) VALUES ('".$worksheet->getCellByColumnAndRow(6, $row)->getValue()."')"; //rutCliente (ahora es un codigo)
            $conn->query($sqlClientIn);
            
            $resultClient = $conn->query($sqlClientSel);
            
        }
		$companyClient = $resultClient->fetch_row();
		

        //Busca si el estado que viene es nuevo, y en ese caso lo agrega
        $sqlStatusSel = "Select idInvoiceStatus from invoicestatus where idInvoiceStatus = ".$worksheet->getCellByColumnAndRow(5, $row)->getValue();
        $resultStatus = $conn->query($sqlStatusSel);
        if($resultStatus->num_rows == 0) //AÃ±ade el estado si no existe
        {
            $sqlStatusIn = "INSERT INTO invoicestatus (idInvoiceStatus) VALUES (".$worksheet->getCellByColumnAndRow(5, $row)->getValue().")";
            $conn->query($sqlStatusIn);
		}
		
        

        $year = substr($worksheet->getCellByColumnAndRow(3, $row)->getValue(), 0, 4);
        $month = substr($worksheet->getCellByColumnAndRow(3, $row)->getValue(), 4, 2);
        $day = substr($worksheet->getCellByColumnAndRow(3, $row)->getValue(), 6, 2);
        $dateEmission =  $year . "-" .$month."-".$day;
		*/

        $sqlUserSel = "Select idUser from user where email = '".$worksheet->getCellByColumnAndRow(3, $row)->getValue()."'";

	
		
		$resultUser = $conn->query($sqlUserSel);

        if($resultUser->num_rows == 1) //AÃ±ade el usuario si no existe
        {	
			
            //Si ya existe el usuario, actualiza los datos
			$sqlUserUpd = "UPDATE user set name = '".$worksheet->getCellByColumnAndRow(1, $row)->getValue()."', status= ".$worksheet->getCellByColumnAndRow(2, $row)->getValue().", idCompany = (select idCompany from company where cuit= ".$worksheet->getCellByColumnAndRow(4, $row)->getValue().") WHERE email = '".$worksheet->getCellByColumnAndRow(3, $row)->getValue()."';";
			$conn->query($sqlUserUpd);
			$sqlProviderUpd .= " UPDATE provider set idCompany = (select idCompany from company where cuit= ".$worksheet->getCellByColumnAndRow(4, $row)->getValue().")";
			$conn->query($sqlProviderUpd);
        }
        else {
            $sqlUserIn =  "INSERT INTO user (name, status, requiresNewPassword, email, idCompany, sex) values ('".$worksheet->getCellByColumnAndRow(1, $row)->getValue()."',".$worksheet->getCellByColumnAndRow(2, $row)->getValue().",0,'".$worksheet->getCellByColumnAndRow(3, $row)->getValue()."',(select idCompany from company where cuit= '".$worksheet->getCellByColumnAndRow(4, $row)->getValue()."'),'0');";
			$conn->query($sqlUserIn);
			$sqlProviderIn = "INSERT INTO provider(idCompany, idUser,idProviderStatus, date) values((select idCompany from company where cuit= ".$worksheet->getCellByColumnAndRow(4, $row)->getValue()."), (Select idUser from user where email='".$worksheet->getCellByColumnAndRow(3, $row)->getValue()."'),1,'".Date('Y-m-d h:i:s')."');";			
			$conn->query($sqlProviderIn);
        }
       
    }

     app()->flashMessage->addMessage('success', Yii::t('app', 'Se importaron los usuarios correctamente'));
            $this->redirect(array('index'));
}
public function actionUpdate($id, $key)
	{
		throw new Exception("Error Processing Request", 1);
		
		$model = Provider::model()->findByPk(CPropertyValue::ensureInteger($id));
		
		if($model instanceof Provider && strcmp(uKey("updateProvider".$model->primaryKey), $key)==0)
		{
			if(isset($_POST['Provider']))
	       	{
		        $model->attributes=$_POST['User'];
		        if($model->save())
		        {
		            $this->redirect(array('index'));
		        }
	        }


			$this->render('form', array('model'=>$model, 'update'=>true));
			return;
		}

		throw new CHttpException(404, Yii::t('app', 'Invalid user'));
	}


}

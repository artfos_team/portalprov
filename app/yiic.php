<?php

require(__DIR__ . '/../vendor/yiisoft/yii/framework/yii.php');
require(__DIR__ . '/../vendor/crisu83/yii-consoletools/helpers/ConfigHelper.php');
require(__DIR__ . '/../vendor/autoload.php');

$env = new \marcovtwout\YiiEnvironment\Environment(null, __DIR__ . '/../app/config');
defined('YII_DEBUG') or define('YII_DEBUG', $env->yiiDebug);
defined('YII_TRACE_LEVEL') or define('YII_TRACE_LEVEL', $env->yiiTraceLevel);

require_once($env->yiiPath);

$config = ConfigHelper::merge(array(
    __DIR__ . '/../app/config/web.php',
    $env->configConsole
));

$app = Yii::createConsoleApplication($env->configConsole);
$app->commandRunner->addCommands(YII_PATH . '/cli/commands');
$app->run();
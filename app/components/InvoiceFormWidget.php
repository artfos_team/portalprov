<?php
/**
 * Display static fields for the Invoice. It does not include
 * file upload forms (so it works with ajax), or buttons which makes
 * the user of the widget responsible for the way that this the form
 * is handled.
 */
class InvoiceFormWidget extends CWidget
{
	public $form;
	public $model;
	public $update=true;

	public function run()
	{
		$this->render('invoice_form', array(
			'form'=>$this->form,
			'update'=>$this->update,
			'model'=>$this->model));
	}
}
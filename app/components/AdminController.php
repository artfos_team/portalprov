<?php
class AdminController extends Controller
{
    /**
     * Filters to be used in the controller filter chain
     * @return array of the filters used
     */
    public function filters()
    {
        return array(
            array(
                'application.filters.RbacFilter',
            )
        );
    }
}
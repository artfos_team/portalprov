<?php
/**
 * Display static fields for the Invoice. It does not include
 * file upload forms (so it works with ajax), or buttons which makes
 * the user of the widget responsible for the way that this the form
 * is handled.
 */
class NCNDViewWidget extends CWidget
{
	public $form;
	public $model;
	public $record;
	public $update=true;

	public function run()
	{
		if ($record)
		$this->render('ncnd_view', array(
			'form'=>$this->form,
			'record'=>$this->record,
			'update'=>$this->update,
			'model'=>$this->model));
	}
}
<?php

class AClientScript extends CClientScript {
	
	/**
	 * Registers a core javascript library.
	 * @param string the core javascript library name
	 * @see renderCoreScript
	 */
	public function registerCoreScript($name)
	{
        if (strcmp($name,'jquery')==0 && (Yii::app()->request->getIsAjaxRequest() || $this->isScriptRegistered('jquery') )) {
            return;
        }
        if (strcmp($name,'jquery.ui')==0 && (Yii::app()->request->getIsAjaxRequest() || $this->isScriptRegistered('jquery.ui') )) {
            return;
        }
        if (app()->request->isAjaxRequest)
        {
        	return;
        }
        parent::registerCoreScript($name);
	}

    public function registerScriptFile($url,$position=null,array $htmlOptions = array())
    {
        if (app()->request->isAjaxRequest)
        {
            return;
        }
        return parent::registerScriptFile($url, $position,$htmlOptions);
    }


}
<?php
/**
 * StatusUpdater updates participant program status to the current day criteria
 */

class StatusUpdater {

    private $toActive = array();
    private $toReinserted = array();
    private $toFinished = array();
    private $updated = 0;

    /**
     * Performs the correct state transition if there is a change of state that should happen today
     * @param bool $dryRun whether to make or not modifications to the database
     * @param bool $debug whether or not to show debug information or not
     * @param bool $html whether to format things with html
     */
    public function check($dryRun = false, $debug = false, $html=false)
    {
        $toUpdate = ParticipantProgram::model()->findAll();

        $dryRun = CPropertyValue::ensureBoolean($dryRun);


        $today = strtotime('today');
        $tomorrow = strtotime('tomorrow');
        $b = new MySQLDateTimeBehaviour();
        $ok = true;

        $transaction = false;
        $rollback = false;

        if(Yii::app()->db->getCurrentTransaction() == null)
        {
            $transaction = Yii::app()->db->beginTransaction();
        }

        foreach($toUpdate as $pprogram)
        {
            $start = $b->parseStringToTS($pprogram->startDate);
            $toSave = array();
            $hasToSave = false;
            //Si se carga una fecha posterior a hoy, cuando llega esa fecha, se debe pasar a activo
            if($pprogram->status == ParticipantProgram::STATUS_TODO && $this->between($start, $today, $tomorrow))
            {
                $pprogram->status = ParticipantProgram::STATUS_ACTIVE;
                $toSave[] = 'status';
                $this->toActive[] = $pprogram->getPrimaryKey();
                $hasToSave = true;
            }

            //Si el participante no se reinserta (es decir, no se carga ninguna fecha de reinserción),
            //tiene que pasar de ACTIVO a FINALIZADO automáticamente cuando pasa la fecha de finalización de programa
            if($pprogram->status == ParticipantProgram::STATUS_ACTIVE && is_null($pprogram->reinsertionDate) && $this->between($b->parseStringToTS($pprogram->endDate), $today, $tomorrow) )
            {
                $pprogram->status = ParticipantProgram::STATUS_FINISHED;
                $toSave[] = 'status';
                $this->toFinished[] = $pprogram->getPrimaryKey();
                $hasToSave = true;
            }

            //Cuando un participante se garantiza (porque se reinserta), por X cantidad de meses.
            //Pasado ese periodo, tiene que cambiar de GARANTIZADO a REINSERTADO automáticamente
            //(yo puedo poner manualmente la fecha de reactivación, pero al finalizar ese periodo tiene que cambiarse a REINSERTADO automáticamente).
            if($pprogram->status == ParticipantProgram::STATUS_GRANTED && $this->between($b->parseStringToTS($pprogram->restartDate), $today, $tomorrow))
            {
                $pprogram->status = ParticipantProgram::STATUS_REINSERTED;
                $pprogram->reinsertionDate = $pprogram->restartDate;
                $toSave[] = 'status';
                $toSave[] = 'reinsertionDate';
                $this->toReinserted[] = $pprogram->getPrimaryKey();
                $hasToSave = true;
            }

            if($hasToSave)
            {
                if($dryRun) {
                    $ok = true;
                }else {
                    $ok = $ok && $pprogram->save(false, $toSave);
                }
                $this->updated++;
                if(!$ok)
                {
                    if($transaction)
                    {
                        $transaction->rollback();
                    }
                    echo "\n ***** ERROR AL GUARDAR - HACIENDO ROLLBACK DE LAS OPERACIONES *****\n\n";
                    return -1;
                }
            }

        }

        if($dryRun)
        {
            if($transaction)
            {
                $transaction->rollback();
            }

            echo $this->getCommandSummary($debug, $html);
            echo "\n\n ***** DRY RUN - NO SE HACEN CAMBIOS EN LA BASE DE DATOS *****\n\n";
            return -2;
        }

        if($transaction)
        {
            $transaction->commit();
        }

        echo $this->getCommandSummary($debug, $html);
    }


    /**
     * Return whether a quantity is between two bounds (closed, open interval)
     * @param $quantity
     * @param $closed
     * @param $open
     * @return bool
     */
    private function between($quantity, $closed, $open)
    {
        return ($quantity >= $closed) && ($quantity < $open);
    }

    /**
     * Displays the command summary
     * @param bool $extended whether to show extended info (debug purposes)
     * @param bool $html whether to show html formatting tags such as brs for newlines
     * @return string
     */
    public function getCommandSummary($extended=false, $html=false)
    {
        $toActive = count($this->toActive);
        $toFinished = count($this->toFinished);
        $toReinserted = count($this->toReinserted);

        $ret = "\n ***** Registros actualizados: {$this->updated} - (NO COMENZADO -> ACTIVO: {$toActive} / ACTIVO -> FINALIZADO: {$toFinished} / GARANTIZADO -> REINSERTADO: {$toReinserted}) *****\n\n";
        if($extended)
        {
            $ret.= " Programas modificados (ids)\n ********************************\n";
            $ret .= ' ACTIVO: ' . implode(",",$this->toActive) ."\n". ' REINSERTADO: '  . implode(",", $this->toReinserted) ."\n" . ' FINALIZADO: ' . implode(",", $this->toFinished) . "\n";
        }


        return $html ? strtr($ret, array("\n"=>"<br/>")) : $ret;
    }
} 
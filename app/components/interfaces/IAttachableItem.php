<?php
/**
 * @author NICO
 *
 */
interface IAttachableItem
{
	/**
	 * Returns the id of the item
	 * @return integer
	 */
	public function getItemId();
	
	/**
	 * Returns the item type identification
	 * @return integer
	 */
	public function getItemTypeId();
	
	/**
	 * Returns the item container identification
	 * @return integer
	 */
	public function getItemContainerId();
	
	/**
	 * Returns the actual item
	 * @param integer $iditem the id of the item
	 * @param integer $iditem_container the id of the item container
	 * @return ILinkerItem
	 *//*
	public function getItem($iditem, $iditem_container);*/
	
	/**
	 * Returns the name that should be used for the type of item
	 * @return string
	 */
	public function getTypeName();
	
}
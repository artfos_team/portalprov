<?php

/**
 * ParticipantIdentity represents the data needed to identity a participant.
 * It contains the authentication method that checks if the provided
 * data can identity the participant.
 */
class ParticipantIdentity extends UserIdentity
{
    /**
     * Authenticates a user.
     * The example implementation makes sure if the username and password
     * are both 'demo'.
     * In practical applications, this should be changed to authenticate
     * against some persistent user identity storage (e.g. database).
     * @return boolean whether authentication succeeds.
     */
    public function authenticate()
    {
        $part = Participant::model()->find('email like :email', array(':email'=>$this->username));
        
        if (!$part instanceof Participant) {
            $this->errorCode = self::ERROR_USERNAME_INVALID;
        } elseif ( !$part->verifyPassword($this->password) ) {
            $this->errorCode = self::ERROR_PASSWORD_INVALID;
        } else {
            $this->errorCode = self::ERROR_NONE;
            user()->setState('idUser', $part->getPrimaryKey());
            user()->setState('idCompany', $part->idCompany);
            user()->setState('idParticipant', $part->getPrimaryKey());
            $this->_id = $part->getPrimaryKey();
        }
        return !$this->errorCode;
    }

}
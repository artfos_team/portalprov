<?php 
$addresses = isset($model->idCompanyAddress) ? CHtml::listData(CompanyAddress::model()->findAll('idCompany IN (SELECT idCompany FROM
    company_address WHERE idCompanyAddress = :id)', array(':id'=>$model->idCompanyAddress)), 'idCompanyAddress', 'address') : array();

$contacts = isset($model->idCompanyContact) ? CHtml::listData(CompanyContact::model()->findAll('idCompany IN (SELECT idCompany FROM
    company_contact WHERE idCompanyContact = :id)', array(':id'=>$model->idCompanyContact)), 'idCompanyContact', 'fullName') : array();

$positions = CHtml::listData(Position::model()->findAll(array('order'=>'position ASC')), 'idPosition', 'position');
?>

<?php echo $form->textFieldRow($model, 'name', array('class'=>'span3', 'placeholder' => Yii::t('app', 'Name'))); ?>
		
<?php echo $form->textFieldRow($model, 'lastName', array('class'=>'span3', 'placeholder' => Yii::t('app', 'Last Name'))); ?>

<?php if(!$update){ ?>
	<?php echo $form->passwordFieldRow($model, 'password', array('class'=>'span3', 'placeholder' => Yii::t('app', 'Password'))); ?>

	<?php echo $form->passwordFieldRow($model, '_repeatPassword', array('class'=>'span3', 'placeholder' => Yii::t('app', 'Repeat Password'))); ?>

    <?php echo $form->dropDownListRow($model, '_idProgram', CHtml::listData(Program::model()->findAll(array('order'=>'program')), 'idProgram', 'programDetailed'),
	    array('class'=>'span3', 'empty'=>Yii::t('app','Select an option'))); ?>
<?php } ?>

<?php echo $form->datepickerRow($model, 'realStartDate',
    array(
        'append'=>'<i class="icon-calendar"></i>',
        'style'=>'width:120px;',
        'options'=>array('autoclose'=>true, 'format' => 'dd/mm/yyyy'),
        'id'=>'real-start-date-row',
    )
); ?>

<?php echo $form->dropDownListRow($model, 'idConsultant', CHtml::listData(Consultant::model()->findAll(array('order'=>'consultant ASC')), 'idConsultant', 'consultant'),
	array('class'=>'span3', 'empty'=>Yii::t('app','Select an option'))); ?>	

<?php echo $form->dropDownListRow($model, 'sex', $sexList = array(0=>"Femenino" , 1=>"Masculino"),
	array('class'=>'span3', 'empty'=>Yii::t('app','Select an option'))); ?>

<?php echo $form->dropDownListRow($model, 'maritalStatus',
	Participant::getMaritalStatuses(),
	array('class'=>'span3', 'empty'=>Yii::t('app','Select an option'))); ?>	

 <?php echo $form->datepickerRow($model, 'birthDate',
        array(
            'append'=>'<i class="icon-calendar"></i>',
            'style'=>'width:120px;',
            'options'=>array('autoclose'=>true, 'format' => 'dd/mm/yyyy'),
            'id'=>'birth-date-row',
        )
    ); ?>

<?php echo $form->dropDownListRow($model, 'idCountry', CHtml::listData(Country::model()->findAll(array('order'=>'country ASC')), 'idCountry', 'country'),
	array('class'=>'span3', 'empty'=>Yii::t('app','Select an option'))); ?>	

<?php echo $form->textFieldRow($model, 'phone', array('class'=>'span3', 'placeholder' => Yii::t('app', 'Phone'))); ?>	

<?php echo $form->textFieldRow($model, 'cellphone', array('class'=>'span3', 'placeholder' => Yii::t('app', 'Cell Phone'))); ?>	

<?php echo $form->textFieldRow($model, 'email', array('class'=>'span3', 'placeholder' => Yii::t('app', 'Email'))); ?>

<?php echo $form->textFieldRow($model, 'children', array('class'=>'span3', 'placeholder' => Yii::t('app', 'Children'))); ?>	

<?php echo $form->textAreaRow($model, 'professionalFormation', array('class'=>'span3', 'placeholder' => Yii::t('app', 'Professional Formation'))); ?>

<?php echo $form->dropDownListRow($model, 'educationalLevel', Participant::getEducationalLevels(),
	array('class'=>'span3', 'empty'=>'Seleccionar nivel educativo')); ?>


<?php echo $form->dropDownListRow($model, 'idCompany', CHtml::listData(Company::model()->findAll(array('order'=>'company')), 'idCompany', 'company'),
	array('class'=>'span3', 'empty'=>Yii::t('app','Select an option'),
	'ajax'=>array(
	 	'type'=>'POST',
	 	'dataType'=>'json',
	 	'url'=>array('participant/updateCompanyData', 'model'=>'Participant'),
	 	'success'=>'function(d){
			$("#company-address").html(d.address);
			$("#company-contact").html(d.contact);
	 	}', 
	))); ?>

<?php echo $form->dropDownListRow($model, 'idCompanyAddress', 
	$addresses,
	array('class'=>'span3', 'empty'=>Yii::t('app','Select an option'), 'id'=>'company-address')); ?>

<?php echo $form->dropDownListRow($model, 'idCompanyContact',
	$contacts,
	array('class'=>'span3', 'empty'=>Yii::t('app','Select an option'), 'id'=>'company-contact')); ?>
	

<?php echo $form->textFieldRow($model, 'lastPosition', array('class'=>'span3', 'placeholder' => Yii::t('app', 'Last Position'))); ?>

<?php echo $form->dropDownListRow($model, 'idPosition',	$positions,
	array('class'=>'span3', 'empty'=>Yii::t('app','Select an option'))); ?>

<?php echo $form->textFieldRow($model, 'rawSalary', array('class'=>'span3', 'placeholder' => Yii::t('app', 'Raw Salary'))); ?>

<?php echo $form->textFieldRow($model, 'area', array('class'=>'span3', 'placeholder' => Yii::t('app', 'Area'))); ?>	

<?php echo $form->textFieldRow($model, 'seniorityCompany', array('class'=>'span3', 'placeholder' => Yii::t('app', 'Seniority Company'))); ?>	

<?php echo $form->textFieldRow($model, 'seniorityPosition', array('class'=>'span3', 'placeholder' => Yii::t('app', 'Seniority Position'))); ?>	

<?php echo $form->textAreaRow($model, 'benefits', array('class'=>'span3', 'placeholder' => Yii::t('app', 'Benefits'))); ?>

<?php echo $form->dropDownListRow($model, 'idExitReason',
    CHtml::listData(ExitReason::model()->findAll(array('order'=>'exitReason ASC')), 'idExitReason', 'exitReason'),
    array('class'=>'span3', 'empty'=>Yii::t('app','Select an option'))); ?>

<?php echo $form->textFieldRow($model, 'newCompany', array('class'=>'span3', 'placeholder' => Yii::t('app', 'New Company'))); ?>	

<?php echo $form->textFieldRow($model, 'newSalary', array('class'=>'span3', 'placeholder' => Yii::t('app', 'New Salary'))); ?>	

<?php echo $form->textFieldRow($model, 'newPosition', array('class'=>'span3', 'placeholder' => Yii::t('app', 'New Position'))); ?>	

<?php echo $form->dropDownListRow($model, 'idNewPosition', $positions,
	array('class'=>'span3', 'empty'=>Yii::t('app','Select an option'))); ?>

<?php echo $form->dropDownListRow($model, 'idReinsertionMode',
	CHtml::listData(ReinsertionMode::model()->findAll(array('order'=>'reinsertionMode ASC')), 'idReinsertionMode', 'reinsertionMode'),
	array('class'=>'span3', 'empty'=>Yii::t('app','Select an option'))); ?>

<?php echo $form->datepickerRow($model, 'reinsertionDate',
        array(
            'append'=>'<i class="icon-calendar"></i>',
            'style'=>'width:120px;',
            'options'=>array('autoclose'=>true, 'format' => 'dd/mm/yyyy'),
            'id'=>'reinsertion-date-row',
        )
    ); ?>

<?php echo $form->textAreaRow($model, 'comments', array('class'=>'span3', 'placeholder' => Yii::t('app', 'Comments'))); ?>

<?php echo $form->textAreaRow($model, 'ccMails', array('class'=>'span3', 'placeholder' => Yii::t('app', 'Mails adicionales (CC)'),
    'hint'=>'Ingresar mails separados por ";"')); ?>
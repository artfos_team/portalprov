    <div>		 
		
<h4>EMISOR</h4>
    <?php
    
    $this->widget('bootstrap.widgets.TbDetailView', array(
        'data' => $model,
        'attributes' => array(
            'RutEmisor',
            'Emisor_RznSoc',
            'Emisor_GiroEmis',
            'Emisor_Acteco',
            'Emisor_DirOrigen',
            'Emisor_CmnaOrigen',
            'Emisor_CiudadOrigen',
)
    ));

    ?>


    <h4>Receptor</h4>

        <?php
    
    $this->widget('bootstrap.widgets.TbDetailView', array(
        'data' => $model,
        'attributes' => array(
            'Receptor_RUTRecep',
            'Receptor_RznSocRecep',
            'Receptor_GiroRecep',
            'Receptor_DirRecep',
            'Receptor_CmnaRecep',
            'Receptor_CiudadRecep',
            'Totales_MntExe',
            'Totales_MntTotal',
)
    ));

    ?>


<div style="width:100%; margin:0 auto;">


	<hr/>

    <div>
    	<h4>Detalle</h4>

    	<?php $this->widget('bootstrap.widgets.TbExtendedGridView', array(
		'id' => 'activities-grid',
		'summaryText'=>'',
		 //'enableSorting'=>true,
         //'enablePagination'=>false,

		'dataProvider' => new CActiveDataProvider('NCNDDetail', array(
                'criteria' => array(
                    'condition' => 'idNCND=:idNCND',
                    'params' => array(
                        ':idNCND' => $model->idNCND),
                ),
        )),
		'type' => 'striped bordered',
		'responsiveTable' => true,
		'columns' => array(
			array(
				'header'=>'#',
				'value'=>'$data->NroLinDet',
			),
			array(
				'header'=>'Descripción',
				'value'=>'$data->NmbItem'
			),
			array(
				'header'=>'Cantidad',
				'value'=>'$data->QtyItem'
			),
			array(
				'header'=>'UnmdItem',
				'value'=>'$data->UnmdItem'
			),
			array(
				'header'=>'Precio',
				'value'=>'$data->PrcItem'
			),
			array(
				'header'=>'Monto',
				'value'=>'$data->MontoItem'
			),
	))); ?>
    </div>
		

    </div>
<div id="page-size-container">
	
	<?php CHtml::beginForm(); ?>
	<div class="form">
		<div class="controls-inline">
			<?php echo CHtml::label(Yii::t('app','Items per page'), 'page-size'); ?>
			<?php echo CHtml::dropDownList('page-size', 'page-size', $sizes,
				array('id'=>'page-size-ddl')); ?>
		</div>
	</div>
	<?php CHtml::endForm(); ?>
</div>
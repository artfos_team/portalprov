<?php
define ("WSDL", "https://awshomo.afip.gov.ar/sr-padron/webservices/personaServiceA4?WSDL");     # The WSDL corresponding to WSAA
define ("URL", "https://awshomo.afip.gov.ar/sr-padron/webservices/personaServiceA4");
define ("CUITREPRESENTADA", 20418343398);
define ("RELATIVEPATH", "../app/components/wsAfip/");

class getPersonaAfip {
    public $token;
    public $sign;
    public $cuitRepresentada;
    public $idPersona;
    
    public static function searchByCuit($cuitBuscado) {
        call:
        if (file_exists(RELATIVEPATH."TA.xml")) {
            $xml = simplexml_load_file(RELATIVEPATH."TA.xml");
        } else {
            shell_exec('php '.RELATIVEPATH.'wsaa-client.php ws_sr_padron_a4');
            goto call;
        }
        
        $searched = new getPersonaAfip();
        $searched->token = $xml->credentials->token;
        $searched->sign = $xml->credentials->sign;
        $searched->cuitRepresentada = CUITREPRESENTADA;
        $searched->idPersona = $cuitBuscado;
        
        $client = new SoapClient(WSDL);
        
        try {
            $results = $client->getPersona($searched);
        }
        catch (SoapFault $ex) {
            if (strpos($ex->faultstring, "El token ha expirado") !== false) {
                shell_exec('php '.RELATIVEPATH.'wsaa-client.php ws_sr_padron_a4');
                goto call;
            }
            return $ex->faultstring;
        }
        
        return $results->personaReturn;
    }
}
?>
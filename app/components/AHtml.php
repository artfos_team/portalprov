<?php

class AHtml
{

	/**
	 * Generates an image tag with absolute path.
	 * @param string $src the image URL
	 * @param string $alt the alternative text display
	 * @param array $htmlOptions additional HTML attributes (see {@link tag}).
	 * @return string the generated image tag
	 */
	public static function absoluteImage($src,$alt='',$htmlOptions=array())
	{
		$src = (strcmp(substr($src,0,4), "http")==0)?($src):(app()->params['baseUrl'].$src);
		return CHtml::image($src,$alt,$htmlOptions);
	}
	
}
<?php
/**
 * Display static fields for the Participant. It does not include
 * file upload forms (so it works with ajax), or buttons which makes
 * the user of the widget responsible for the way that this the form
 * is handled.
 */
class ParticipantFormWidget extends CWidget
{
	public $form;
	public $model;
	public $update=true;

	public function run()
	{
		$this->render('participant_form', array(
			'form'=>$this->form,
			'update'=>$this->update,
			'model'=>$this->model));
	}
}
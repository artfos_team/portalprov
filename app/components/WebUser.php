<?php

Yii::import('vendor.nordsoftware.yii-audit.behaviors.AuditChanger');

/**
 * class WebUser extends CWebUser to add custom behavior
 * @todo  add missing comments
 */
class WebUser extends CWebUser
{
	/**
	 * Variable to hold the current user model
	 * @var User
	 */
	protected $_model;
	//protected $idUser;
	public $es_proveedor = false;
	public $es_pagos = false;
	public $es_admin = false;
	public $es_compras = false;

	const ERR_INVALID_TOKEN = -1;
	const ERR_BAD_TOKEN = -2;
	const ERR_EXPIRED_TOKEN = -3;
	const ERR_INVALID_USER = -4;

	const TOKEN_EXPIRE_SECONDS = 60 * 60;

	/**
	 * Quantity of items per page
	 * @var integer
	 */
	private $_itemsPerPage;

	public function init()
	{
		$token = Yii::app()->request->getParam('token', null);

		if ($token === null) {
			parent::init();
		} else {
			// Check if token is valid.
			$result = $this->checkLoginToken($token);

			if ($result !== null) {
				echo CJSON::encode($result);
				exit;
			}
		}

		$this->attachBehavior('auditChanger', 'AuditChanger');
		$this->updateLastActiveAt();
		if ($this->hasRole(61))
			$this->es_proveedor = true;
		if ($this->hasRole(62))
			$this->es_compras = true;
		if ($this->hasRole(63))
			$this->es_pagos = true;
		if ($this->hasRole(18))
			$this->es_admin = true;
	}

	/**
	 * Loads the user model for the logged in user.
	 * @return User the model
	 */
	public function loadModel()
	{
		if (isset($this->_model)) {
			return $this->_model;
		} else {
			if ($this->isGuest) {
				return null;
			}

			/**
			 * If idParticipant is set then model is Participant not user
			 */
			if (isset($this->idParticipant) && !is_null($this->idParticipant)) {
				return $this->_model = Participant::model()->findByPk($this->idParticipant);
			}
			return $this->_model = User::model()->findByPk($this->idUser);
		}
	}

	/**
	 * Updates the users last active at field.
	 * @return boolean whether the update was successful.
	 */
	public function updateLastActiveAt()
	{
		if (!$this->isGuest) {
			if (($model = $this->loadModel()) !== null) {
				if (isset($this->idParticipant) && !is_null($this->idParticipant)) {
					app()->db->createCommand("UPDATE participant SET lastActiveAt = ? WHERE idParticipant = ?")->execute(array(sqlDateTime(), $model->getPrimaryKey()));
				} else {
					app()->db->createCommand("UPDATE user SET lastActiveAt = ? WHERE idUser = ?")->execute(array(sqlDateTime(), $model->getPrimaryKey()));
				}
				return true;
			}
		}
		return false;
	}

	/**
	 * Updates the users last login at field.
	 * @return boolean whether the update was successful.
	 */
	public function updateLastLoginAt($refreshTok = false)
	{
		if (!$this->isGuest || $refreshTok) {
			if (($model = $this->loadModel()) !== null) {
				$model->lastLoginAt = sqlDateTime();
				return $model->save(true, array('lastLoginAt'));
			}
		}
		return false;
	}

	/**
	 * Returns the PK of the logged user
	 * @return int
	 */
	public function getId()
	{
		if (!$this->isGuest) {
			$this->loadModel();
			return $this->_model->getPrimaryKey();
		}

		return 0;
	}


	//========================= COOKIE FUNCTIONS ===============================

	public function hasCookie($name)
	{
		return isset(Yii::app()->request->cookies[$name]);
	}

	public function getCookie($name)
	{
		return Yii::app()->request->cookies[$name]->value;
	}

	public function setCookie($name, $value)
	{
		$cookie = new CHttpCookie($name, $value);
		Yii::app()->request->cookies[$name] = $cookie;
	}

	public function removeCookie($name)
	{
		unset(Yii::app()->request->cookies[$name]);
	}


	public function hasRole($idRole)
	{
		if (isset($this->idParticipant) && !is_null($this->idParticipant)) {
			return null;
		}
		if (isset($this->_model)) {
			return $this->_model->hasRole($idRole);
		} else
			return 0;
	}

	public function isProveedor()
	{
		if (isset($this->idParticipant) && !is_null($this->idParticipant)) {
			return null;
		}
		if (isset($this->_model)) {
			return $this->_model->hasRole($idRole);
		} else
			return 0;
	}


	public function getCompanyName()
	{
		if (isset($this->idParticipant) && !is_null($this->idParticipant)) {
			return null;
		}
		if (isset($this->_model)) {
			return $this->_model->getCompanyName();
		} else
			return "";
	}


	public function getIdCompany()
	{
		if (isset($this->idParticipant) && !is_null($this->idParticipant)) {
			return null;
		}
		if (isset($this->_model)) {
			return $this->_model->getIdCompany();
		} else
			return 0;
	}

	public function generateToken($refreshTok = false) {

		if (!$this->isGuest || $refreshTok) {
			if (($model = $this->loadModel()) !== null) {
				$token = sha1($model->email . time() . rand(1000, 9999));

				app()->db->createCommand("UPDATE user SET token = ? WHERE idUser = ?")->execute(array($token, $model->getPrimaryKey()));
				return $token;
			}
		}
	}

	public function checkLoginToken($token)
	{
		$result = array();
		$user = UserIdentity::getRemote($token);
		$result['error'] = self::ERR_INVALID_USER;

		if ($user === null) {
			$result['errmsg'] = Yii::t('app', 'Invalid user');
		} else {
			if (strtotime($user->lastLoginAt) < (time() - self::TOKEN_EXPIRE_SECONDS)) {
				$result['error'] = self::ERR_EXPIRED_TOKEN;
				$result['errmsg'] = Yii::t('app', 'Expired token');
			} else {
				//$this->login($user, 0);
				$result = null;
			}
		}

		return $result;
	}

	public function refreshToken($token) {
		$user = UserIdentity::getRemote($token);
		if ($user === null) {
			return $result['errmsg'] = Yii::t('app', 'Invalid user');;
		}
		$this->_model = $user;
		$this->updateLastLoginAt(true);
		return $this->generateToken(true);
	}

}
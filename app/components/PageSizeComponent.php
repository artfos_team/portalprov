<?php

class PageSizeComponent extends CWidget
{

	public $sizes = array(
		10 => 10,
		25 => 25,
		50 => 50,
		100 => 100,
	);
	
	public function run()
	{
		cs()->registerScript('page-size-ddl',  '
			var $pageDdl = $("#page-size-ddl");
			$pageDdl.val('. app()->controller->getItemsPerPage() .');
			$pageDdl.change(function(e){
				e.preventDefault();

				$.ajax({
					url: ' . CJavaScript::encode(app()->controller->createUrl('/page/setPageQty')) . ',
					type: "GET",
					data: {qty: $(this).val()},
					success: function(data){
						window.location = window.location;
					}
				});
			});	
		');

		$this->render('page_size', array('sizes'=>$this->sizes));
	}

}
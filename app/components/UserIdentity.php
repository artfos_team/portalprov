<?php

/**
 * UserIdentity represents the data needed to identity a user.
 * It contains the authentication method that checks if the provided
 * data can identity the user.
 */
class UserIdentity extends CUserIdentity
{
    protected $_id;

    /**
     * Authenticates a user.
     * The example implementation makes sure if the username and password
     * are both 'demo'.
     * In practical applications, this should be changed to authenticate
     * against some persistent user identity storage (e.g. database).
     * @return boolean whether authentication succeeds.
     */
    public function authenticate()
    {
        $user = User::model()->find('email like :email', array(':email'=>$this->username));
        
        if (!$user instanceof User) {
            $this->errorCode = self::ERROR_USERNAME_INVALID;
        } elseif ( !$user->verifyPassword($this->password) ) {
            $this->errorCode = self::ERROR_PASSWORD_INVALID;
        } else {
            $this->errorCode = self::ERROR_NONE;
            user()->setState('idUser', $user->getPrimaryKey());
            user()->setState('idCompany', $user->idCompany);
            $this->_id = $user->getPrimaryKey();
        }
        return !$this->errorCode;
    }

    /**
     * Return id
     * @return int id of current user
     */
    public function getId(){
        return $this->_id;
    }

    public static function getRemote($token) {

	    return User::model()->findByAttributes(array('token' => $token));
    }
}
<?php
Yii::import('bootstrap.widgets.TbEditableColumn');

class ETbEditableColumn extends TbEditableColumn {

    public function init() {
        parent::init();

        // If we have no records ...
        // render some fake data (without displaying it) so the necessary javascript is added for the column
        if(!$this->grid->dataProvider->totalItemCount) {
            ob_start();
            $this->renderDataCellContent(0, $this->grid->dataProvider->model);
            $fakeRow = ob_get_clean();
        }
    }

}
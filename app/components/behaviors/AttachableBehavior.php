<?php
/**
 * This behavior should be used by all models that can have attached files
 * @author NICO
 *
 */
Yii::import('application.components.interfaces.IAttachableItem');
class AttachableBehavior extends CActiveRecordBehavior
{
	public $_files;
	public $files;
	public $attach_ids;
	
	/**
	 * Saves files that are loaded on the $_files instance variable that has to be a CUplodedFile array
	 * @return boolean TRUE if all files could be uploaded
	 */
	protected function saveFiles( )
	{
		$ret = false;
		$item = $this->owner;
		if($item instanceof IAttachableItem)
		{

			$transaction = false;
			if(Yii::app()->db->getCurrentTransaction() == null)
			{
				$transaction = Yii::app()->db->beginTransaction();
			}
			
			$ret = true;
			foreach(CUploadedFile::getInstances($item, '_files') as $upload)
			{
				$attach = new ItemHasAttachment();
				
				$attach->idItem = $item->getItemId();
				$attach->idItemType = $item->getItemTypeId();
				
				if($upload instanceof CUploadedFile)
				{
					$name = 'a_'.time().'_'.rand().'.'.$upload->getExtensionName();
					
					$attach->attachment = $name;
					
					$path = $attach->getFileDirectorySave();
					
					if(!is_dir($path))
					{
						mkdir($path, 0777, true);
					}
					if(!$upload->saveAs($path.'/'.$attach->attachment))
					{
						$ret = false;
						die('no');
						break;	
					}
				}
				
				$attach->title = $upload->getName();
				
				if(!$attach->save())
				{		
					@unlink($attach->getFileDirectorySave().'/'.$attach->attachment);
					$ret = false;
					break;
				}
			}

			//Add files already downloaded (if they exist)
			$classname = get_class($item);
			if(isset($_POST[$classname]) && isset($_POST[$classname]['attach_ids'])){
				$this->attach_ids = $_POST[$classname]['attach_ids'];
			}

			if(isset($this->attach_ids) && strlen($this->attach_ids) > 0){
				$otherAttachs = explode(',', $this->attach_ids);
				$otherAttachsRecords = ItemHasAttachment::model()->findAllByPk($otherAttachs);

				foreach($otherAttachsRecords as $oar){
					$newAttach = new ItemHasAttachment();
					$newAttach->attributes = $oar->attributes;
					$newAttach->idItem = $item->getItemId();
					$newAttach->idItemType = $item->getItemTypeId();
					$newAttach->publicDownload = 0;

					$file = $oar->getFileDirectorySave() . '/' . $oar->attachment;

					$path = $newAttach->getFileDirectorySave();
					$ret = $ret && $newAttach->save();
					if($ret && file_exists($file)){
						if(!is_dir($path))
						{
							mkdir($path, 0777, true);
						}
						copy($file, $path . '/' . $newAttach->attachment);	
					}else{
						$ret = false;
						$item->addError('file', Yii::t('app', 'Error while uploading file'));
					}
				}
			}
			
			if($ret)
			{
				if($transaction !== false)
				{
					$transaction->commit();
				}
			}
			else
			{
				if($transaction !== false)
				{
					$transaction->rollback();				
				}
			}
			
		}
		return $ret;
	}
	
	/**
	 * Generates the widget to be used to upload multiple inputs
	 * @param string $container the container in which to display the files
	 * @param array $htmlOptions the htmltoptions for the container
	 * @param string $elementContainer the tag to be used to display each file
	 * @param array $elementoHtmlOptions the htmloptions for the tag for each file
	 * @param boolean $input whether to show the input to upload new files or not
	 * @param boolean $show whether to display existing files. If both $input and $show are true, the files can be deleted
	 */
	public function generateAttachableInput($container = 'div', $htmlOptions = array(), $elementContainer = 'span', $elementHtmlOptions = array(),
		$input = true, $show = true)
	{
		$item = $this->owner;
		if($input)
		{
			echo CHtml::label(Yii::t('app','Attachment'), '_files');
			Yii::app()->controller->widget('CMultiFileUpload', array(
		    	'model'=>$this->owner,
		     	'attribute'=>'_files',
		  	));

		}

		if($show)
		{
			echo CHtml::openTag($container, array_merge(array('id'=>'files-container'),$htmlOptions));
			foreach($item->files as $file)
		 	{
		 		$elementHtmlOptions['id'] = 'attach_'.$file->idItemHasAttachment;
		 		
		 		echo CHtml::openTag($elementContainer, $elementHtmlOptions);
		 		echo CHtml::link($file->title, $file->getDownloadPath(), array('target'=>'_blank')).'&nbsp';
		 		if($input)
		 		{
			 		echo CHtml::link(Yii::t('attach', 'Delete'), '#',
			 			array(
			 				'id'=>'attach_'.rand().'_'.$file->idItemHasAttachment,
			 				'ajax'=>
						 	array(
							 	'type'=>'POST', //request type
								'url'=>array('/attach/default/deleteFile', 'id'=>$file->idItemHasAttachment, 'key'=>$file->getKey('delete')),
		                        'success'=>'function(d,t,x) {
									$("#attach_'.$file->idItemHasAttachment.'").remove();
		                        }',
						 		'error'=>'function(){
						 			$("#attach_'.$file->idItemHasAttachment.'_notice").html("'.Yii::t('attach', 'Could not delete attachment').'");
						 		}'
							),
							'confirm'=>Yii::t('attach', 'Are you sure to delete the attachment? This cannot be undone')
			 			));
			 		echo CHtml::tag('span', array('id'=>'attach_'.$file->idItemHasAttachment.'_notice'), '&nbsp;');
		 		}
		 		echo CHtml::closeTag($elementContainer);
			}
			echo CHtml::closeTag($container);
		}


	}
	
	/**
	 * Sets the instance $files variable with current files
	 * (non-PHPdoc)
	 * @see CActiveRecordBehavior::afterFind()
	 */
	public function afterFind( $event )
	{
		$item = $this->owner;
		$ret = array();
		if( $item instanceof IAttachableItem )
		{
			$item->files = ItemHasAttachment::model()->findAll('idItem = :idi and idItemType = :idit',
				array(':idi'=>$item->getItemId(), ':idit'=>$item->getItemTypeId()));			
		}
	}


}
<?php
/**
 * This behavior should be used by all models that can have simple files attached
 * @author juan
 */
class SimpleAttachBehavior extends CActiveRecordBehavior
{
	public $_uploads;
	public $uploads;

	public $attributeNames = null;
	public $dirName = null;
	public $dirCode = null;
	
	public $possibleExtensions = array();
	public $parentFolderName = 'files';
	public $fileNamePrefix = 'file_';

	protected $_maxSize = 1887437; // 1.8 Mb

	/**
	 * Saves files that are loaded on the $_picture instance variable that has to be a CUplodedFile array
	 * @return boolean TRUE if all files could be uploaded
	 */
	public function beforeValidate( $event )
	{
		if(!$this->validateParams()){
			return false;
		}

		$ret = false;
		$item = $this->owner;
		
		$ret = true;
		foreach($this->_uploads as $attr=>$uploadAttr){

			$upload = CUploadedFile::getInstance($item, $uploadAttr);
			
			if($upload instanceof CUploadedFile && $upload->size > 0)
			{
				$ext = strtolower($upload->getExtensionName());
				$name = is_null($this->fileNamePrefix)  ? $upload->name : $this->generateFileName().'.'.$ext;

				$possibleExtensions = $this->possibleExtensions;
				if(array_search($ext, $possibleExtensions)===false)
				{
					$item->addError($uploadAttr, 'La extensión del archivo no es permitida. Solo se permiten ' . implode(',', $possibleExtensions));
					return false;
				}

				if($upload->size > $this->_maxSize)
				{
					$item->addError($uploadAttr, 'El tamaño del archivo no debe superar los ' . $this->_maxSize * 1024 * 1024 .' Mb');
				}

				$item->$attr = $name;
			}
			
			$this->uploads[$attr] = $upload;
			
		}
		
		return parent::beforeValidate($event);
	}
	

	public function afterSave($event){
		$item = $this->owner;
		$path = $this->getDir(true);
							
		if(!is_dir($path))
		{
			mkdir($path, 0777, true);
		}
		
		if(count($this->uploads)>0)
		{
			foreach($this->uploads as $attr=>$upload)
			{
				if($upload instanceof CUploadedFile && $upload->size > 0){
					
					$this->deleteAttach($attr);
					if(!$upload->saveAs($path.'/'. $item->$attr))
					{
						throw new CException('Error while saving file');	
					}
				}
			}
		}
		
		return parent::afterSave($event);
	}

	/**
	 * Generates the widget to be used to upload multiple inputs
	 */
	public function generateInput($container='div', $containerHtmlOptions=array(), $labelHtmlOptions=null, $inputHtmlOptions=null)
	{	
		if(!$this->validateParams()){
			return false;
		}
		$item = $this->owner;

		foreach($this->_uploads as $attr=>$uploadAttr){
			echo CHtml::openTag($container, $containerHtmlOptions);

			echo CHtml::activeLabelEx($item, $uploadAttr, $labelHtmlOptions);
			echo CHtml::activeFileField($item, $uploadAttr, $inputHtmlOptions);
			
			echo CHtml::closeTag($container);
		}

	}
	
	/**
	 * Returns the directory whether the category file could be found
	 * @param string $path whether to get the full path
	 * @return string
	 */
	public function getDir($path = false)
	{
		$item = $this->owner;
		
		$prefix = str_pad($item->getPrimaryKey(), 12, '0', STR_PAD_LEFT);
		$directory = '/upload/' . $this->parentFolderName .'/'.$this->dirName.'/'.substr($prefix, 0, 3).'/'.substr($prefix, 3, 3).'/'.substr($prefix, 6, 3);
		$directory .= '/' . $item->getPrimaryKey();

		if(!is_dir(Yii::getPathOfAlias('webroot').$directory))
			mkdir(Yii::getPathOfAlias('webroot').$directory, 0777, true);
		
		
		return ($path ? Yii::getPathOfAlias('webroot') : baseUrl()).  $directory;
	}
	
	/**
	 * Deletes a file from the disk and all resizes of it
	 * @param string $file
	 * @param boolean delete container
	 * @return boolean
	 */
	public function deleteAttachs($dir=null, $deleteContainer = false){
		if(is_null($dir)){
			$dir = $this->getDir(true);
		}

		if(is_dir($dir))
		{
			if($dirHandler = opendir($dir))
			{
				while (false !== ($file = readdir($dirHandler)))
			 	{
			        @unlink($dir.'/'.$file);
			    }
			}
		}
	}

	/**
	 * Deletes a file from the disk and all resizes of it
	 * @param string $file
	 * @param boolean delete container
	 * @return boolean
	 */
	public function deleteAttach($attr, $dir=null, $deleteContainer = false){
		if(is_null($dir)){
			$dir = $this->getDir(true);
		}

		$item = $this->getOwner();
		$file = $item->$attr;
		
		if(is_dir($dir) && !(is_null($attr)) && file_exists($dir . '/' . $file))
		{
		    @unlink($dir.'/'.$file);
		    $item->$attr = null;
		    $item->save(false, array($attr));
		}
	}

	/**
	 * Renders the file of the user, if any
	 * @param string $attr attribute to display
	 */
	public function renderFile($attr, $container='div', $containerHtmlOptions=array())
	{
		if(!$this->validateParams()){
			return null;
		}

		$item = $this->owner;
		$file = $item->$attr;

		if(!is_null($file))
		{
			echo CHtml::openTag($container, $containerHtmlOptions);
			echo CHtml::link($file, $this->getDownloadPath($attr));
			echo CHtml::closeTag($container);
		}
	}

	public function renderFiles($outerContainer = 'ul', $outerContainerHtmlOptions = array(), $container='li', $containerHtmlOptions=array())
	{
		echo CHtml::openTag($outerContainer, $outerContainerHtmlOptions);
		foreach($this->attributeNames as $attr)
		{
			$this->renderFile($attr, $container, $containerHtmlOptions);
		}
		echo CHtml::closeTag($outerContainer);
	}


	public function getFilePath($attr)
	{
		$item = $this->getOwner();
		return $this->getDir(true) . '/' . $item->$attr;
	}

	public function getDownloadPath($attr)
	{
		$item = $this->getOwner();
		return $this->getDir() . '/' . $item->$attr;
	}

	public function generateFileName()
	{
		$pk = $this->getOwner() != null ? $this->getOwner()->getPrimaryKey() : '-';
		return uniqid($this->fileNamePrefix .  $pk . '_' ) .  time().'_'.rand(0, 100);
	}

	protected function validateParams(){
		if( !is_null($this->dirName) && !is_null($this->attributeNames) && is_array($this->attributeNames) )
		{
			foreach($this->attributeNames as $attr)
			{
				$this->_uploads[$attr] = '_'.$attr;
				$this->uploads[$attr] = null;
			}

			return true;
		}

		return false;
	}

}
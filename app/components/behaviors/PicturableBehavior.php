<?php
/**
 * This behavior should be used by all models that can have pictures
 * @author juan
 */
class PicturableBehavior extends CActiveRecordBehavior
{
	public $_uploads;
	public $uploads;

	public $attributeNames = null;
	public $dirName = null;
	

	/*public function init()
	{
		$ret = parent::init();
		$this->validateParams();
		return $ret && $this->validateParams();
	}*/

	/**
	 * Saves files that are loaded on the $_picture instance variable that has to be a CUplodedFile array
	 * @return boolean TRUE if all files could be uploaded
	 */
	public function beforeSave( $event )
	{
		if(!$this->validateParams()){
			return false;
		}

		$ret = false;
		$item = $this->owner;
		
		$ret = true;
		foreach($this->_uploads as $attr=>$uploadAttr){

			$upload = CUploadedFile::getInstance($item, $uploadAttr);
			
			if($upload instanceof CUploadedFile && $upload->size > 0)
			{
				$ext = strtolower($upload->getExtensionName());
				$name = uniqid('pic_').'_'.time().'_'.rand().'.'.$ext;

				$possibleExtensions = array('jpg', 'jpeg', 'png', 'gif');
				if(array_search($ext, $possibleExtensions)===false)
				{
					$item->addError($uploadAttr, 'La extensión del archivo no es permitida. Solo se permiten jpg, jpeg, png y gif');
					return false;
				}

				$item->$attr = $name;
			}
			
			$this->uploads[$attr] = $upload;
		}
		
		return parent::beforeSave($event);
	}
	

	public function afterSave($event){
		$item = $this->owner;
		$path = $this->getDir(true);
							
		if(!is_dir($path))
		{
			mkdir($path, 0777, true);
		}
		foreach($this->uploads as $attr=>$upload)
		{
			if($upload instanceof CUploadedFile && $upload->size > 0)
			{						
				//var_dump($attr);
				$this->deleteFile($attr);				
				if($upload->saveAs($path.'/'. $item->$attr) === false)
				{	
					die();
					throw new CException('Error while saving image');	
				}				
			}
		}
		
		return parent::afterSave($event);
	}

	/**
	 * Generates the widget to be used to upload multiple inputs
	 */
	public function generatePictureInput()
	{	
		if(!$this->validateParams()){
			return false;
		}
		$item = $this->owner;

		foreach($this->_uploads as $attr=>$uploadAttr){
			echo CHtml::activeLabelEx($item, $uploadAttr);
			echo CHtml::activeFileField($item, $uploadAttr);

			if(!is_null($item->$attr))
			{
				echo CHtml::tag('div', array(), $this->renderPicture(50,50, $attr));
				echo CHtml::tag('p', array(), Yii::t('app', 'If you upload a new image, the current will be deleted'));
			}
		}

	}
	
	/**
	 * Returns the directory whether the category file could be found
	 * @param string $path whether to get the full path
	 * @return string
	 */
	public function getDir($path = false)
	{
		$item = $this->owner;
		
		$prefix = str_pad($item->getPrimaryKey(), 12, '0', STR_PAD_LEFT);
		$directory = '/upload/pictures/'.$this->dirName.'/'.substr($prefix, 0, 3).'/'.substr($prefix, 3, 3).'/'.substr($prefix, 6, 3);
		$directory .= '/' . $item->getPrimaryKey();

		if(!is_dir(Yii::getPathOfAlias('webroot').$directory))
			mkdir(Yii::getPathOfAlias('webroot').$directory, 0777, true);
		
		
		return rtrim(($path ? Yii::getPathOfAlias('webroot') : baseUrl()), '/').  $directory;
	}
	
	/**
	 * Deletes a file from the disk and all resizes of it
	 * @param string $file
	 * @param boolean delete container
	 * @return boolean
	 */
	public function deleteFiles($dir=null, $deleteContainer = false){
		if(is_null($dir)){
			$dir = $this->getDir(true);
		}

		if(is_dir($dir))
		{
			if($dirHandler = opendir($dir))
			{
				while (false !== ($file = readdir($dirHandler)))
			 	{
			        @unlink($dir.'/'.$file);
			    }
			}
		}
	}

	/**
	 * Deletes a file from the disk and all resizes of it
	 * @param string $file
	 * @param boolean delete container
	 * @return boolean
	 */
	public function deleteFile($attr, $dir=null, $deleteContainer = false){
		if(is_null($dir)){
			$dir = $this->getDir(true);
		}

		if(is_dir($dir) && !(is_null($attr)) && file_exists($dir . '/' . $attr))
		{
			if($dirHandler = opendir($dir))
			{
			    $item = $this->getOwner();
			    $file = $item->$attr;
			    @unlink($dir.'/'.$file);
			    $item->$attr = null;
			    $item->save(false, array($attr));
			}
		}
	}

	/**
	 * Renders the picture of the user, if any
	 * @param integer $width the width of the logo
	 * @param integer $height the height of the logo
	 * @param boolean $render whether to render or return the image
	 * @param integer $master the master for the resize
	 */
	public function renderPicture($width, $height, $attr, $onlySrc = false, $zoomAndCrop = false, $master = null)
	{
		if(!$this->validateParams()){
			return null;
		}

		$item = $this->owner;
		$picture = $item->$attr;

		$src = null;
		$zm = ($zoomAndCrop) ? 'z_' : '' ;
			
		if(!is_null($picture))
		{
			if(isset($picture) && strlen($picture)>0)
			{
				$src = $this->getDir(true).'/Rw_'.$width.'h_'.$height.'_'.$zm . $picture;
				if(!file_exists($src))
				{
					$original = $this->getDir(true).'/'.$picture;
					if(!file_exists($original))
					{
						$picture = null;
						return null;
					}
					Yii::import('ext.image.ImageExt');
					$img = new ImageExt($original);
					if($zoomAndCrop){
						$img->zoomAndCrop($width, $height);
					}else{
						$img->resize($width, $height, $master);
					}
					$img->save($src);
				}
				$src = $this->getDir().'/Rw_'.$width.'h_'.$height.'_'.$zm.$picture;
			}
			
			if($onlySrc)
			{
				return $src;
			}
			
			if($src !== null){
				return CHtml::image($src, $picture);
			}
		}
	}

	private function validateParams(){
		if( !is_null($this->dirName) && !is_null($this->attributeNames) && is_array($this->attributeNames) )
		{
			foreach($this->attributeNames as $attr)
			{
				$this->_uploads[$attr] = '_'.$attr;
				$this->uploads[$attr] = null;
			}

			return true;
		}

		return false;
	}

}
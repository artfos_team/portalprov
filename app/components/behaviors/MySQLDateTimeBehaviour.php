<?php

/**
 * 
 * MySQLDateTimeBehaviour will treat all the attributes as a mysql DATETIME data type.
 * MySQLDateTimeBehaviour will display each attribute in the corresponding
 * format base on the acutal application language.
 * Each attribute will be save as a MYSQL DATETIME format and it will be show
 * as the languageData format.
 * 
 *  public function behaviors() {
 *		return array(
 *			'date'=>array(
 *				'class'=>'path.to.MySQLDateTimeBehaviour',
 *				'attributes'=>array('birthdate'),
 *				'validatorScenary'=>'register,create,edit'
 *			)
 *		);
 *	}
 *
 * The {@link attributes} is the list of all the ActiveRecord attributes
 * that should be treated a MySQL DATETIME format. It must be an array.
 * 
 * If the {@link addDateValidator} is true, it will add a date rule validator
 * to each attribute on {@link attributes} array. The scenary will be apply
 * to all the {@link validatorScenary} string
 * 
 * @author ngleich
 * 
 */
class MySQLDateTimeBehaviour extends CActiveRecordBehavior {
	
	/**
	 * 
	 * An array of all the attributes of the
	 * Active Record that should be treated as date format
	 * @var array 
	 */
	public $attributes = array();
	
	/**
	 * A boolean that indicate if it should add
	 * and date validator rule to the model.
	 * It will add something like
	 * array({@link attributes}, 'date','on'=>{@link validatorScenary}, 
	 * 	'format'=>$this->dateFormat[$this->actualLanguage]),
	 * @var boolean
	 */
	public $addDateValidator = true;
	
	/**
	 *
	 * if {@link addDateValidator} is true, this will
	 * be all the scenaries that the rule shoud apply
	 * @var string
	 */
	public $validatorScenary = 'create';
	
	/**
	 * The language to use, if false it use
	 * Yii::app()->language
	 * @var string
	 */
	public $actualLanguage = false;
	
	/**
	 * The date formats in javascript type
	 */
	public $dateFormat = array(
		'es'=>'dd/MM/yyyy',
		'en'=>'MM/dd/yyyy',
	);

	/**
	 * Array of scenarios where to ignore behavior
	 * @var array
	 */
	public $excludeBehaviorOnScenario = array();

	/**
	 * The mysql field type, it can be datetime for DATETIME data type
	 * int for INT data type (the dates must be after 1970!)
	 * @var string
	 */
	public $mysql_field_type = 'datetime';
	
	public function attach($owner) {
		parent::attach($owner);
		if($owner instanceof CActiveRecord)
		{
			if($this->addDateValidator && Yii::app() instanceof CWebApplication)
			{
				$validators = $owner->getValidatorList();
				if(strcmp($this->mysql_field_type, 'int')===0)
				{
					foreach($validators as $validator)
					{
						if($validator instanceof CNumberValidator)
						{
							$validator->attributes = array_diff($validator->attributes, $this->attributes);
						}
					}
				}
				$validators->add(CValidator::createValidator('date', $owner, implode(",", $this->attributes), 
					array(
						'format' => $this->getLanguageData(),
					)));
			}
			return true;
		}
		throw new Exception('Owner must be a CActiveRecord class');
	}
	
	/**
	* Responds to {@link CModel::onBeforeSave} event.
	* Sets the values of the creation or modified attributes as configured
	*
	* @param CModelEvent $event event parameter
	*/
	public function beforeSave($event) {
		$owner = $this->getOwner();
		$currentScenario = $owner->getScenario();
		
		if(!in_array($currentScenario, $this->excludeBehaviorOnScenario))
		{
			foreach($this->attributes as $attribute)
			{
				if(!is_null($owner->$attribute) && strlen($owner->$attribute)>0)
				{
					$date = CDateTimeParser::parse($owner->$attribute, 
								$this->getLanguageData());

					if(strcmp($this->mysql_field_type, 'datetime')==0)
					{
						$owner->$attribute = Yii::app()->getDateFormatter()->format('yyyy-MM-dd', $date);//MYSQL DATETIME
					}
					elseif(strcmp($this->mysql_field_type, 'int')==0)
					{
						$owner->$attribute = $date; //timestamp
					}
				}
				else
				{
					$owner->$attribute = null;
				}
			}
		}
	}
	
	public function getAttributeTimestamp($attribute)
	{
		$owner = $this->getOwner();
		return CDateTimeParser::parse($owner->$attribute, $this->getLanguageData());
	}
	/**
	 * Responds to {@link CActiveRecord::onAfterFind} event.
	 * Overrides this method if you want to handle the corresponding event of the {@link CBehavior::owner owner}.
	 * @param CEvent $event event parameter
	 */
	public function afterFind($event) {
		$owner = $this->getOwner();
		foreach($this->attributes as $attribute)
		{
			if(!is_null($owner->$attribute))
			{
				if(strcmp($this->mysql_field_type, 'datetime')==0)
				{
					$owner->$attribute = Yii::app()->getDateFormatter()->format($this->getLanguageData(), CDateTimeParser::parse($owner->$attribute, 'yyyy-MM-dd'));
				}
				elseif(strcmp($this->mysql_field_type, 'int')==0)
				{
					$owner->$attribute = Yii::app()->getDateFormatter()->format($this->getLanguageData(), $owner->$attribute);
				}
			}
		}
	}

	/**
	 * Parse attr (string) to unixtimestamp
	 */ 
	public function parseStringToTS($str)
	{
		return CDateTimeParser::parse($str, $this->getLanguageData());
	}
	
	/**
	 * Format timestamp to a valid date according to language.
	 * @param int $attr
	 */ 
	public static function formatTimestamp($attr)
	{
		$model = new MySQLDateTimeBehaviour();
		return Yii::app()->getDateFormatter()->format($model->getLanguageData(), $attr);
	}
	
	protected function getLanguageData()
	{
		if($this->actualLanguage!==false)
		{
			return $this->dateFormat[$this->actualLanguage];
		}
		else
		{
			return $this->dateFormat[Yii::app()->language];
		}
		
		//$ret = Yii::app()->getDateFormat();
		//return $ret;
	}
	
	
	public static function getPhpLanguageFormat($language = false)
	{
		$data = array(
			'es'=>'dd/mm/yy',
			'en'=>'mm/dd/yy'
		);
		if($language !== false)
		{
			return $data[$language];
		}
		else
		{
			return $data[Yii::app()->language];
		}
	}
	
	
}
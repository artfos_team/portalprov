<?php
require_once __DIR__ . '/../../../vendor/phpnode/yiipassword/src/YiiPassword/Behavior.php';

class YiiPasswordBehavior extends YiiPassword\Behavior
{
    public $usernameAttribute = "email";
}
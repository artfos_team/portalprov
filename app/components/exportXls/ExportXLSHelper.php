<?php

class ExportXLSHelper
{

	public $attributes;

	public $filename;

	public $header;

	public $appendDateOnHeader = true;

	public $appendDateOnFilename = true;

	public $data;

	public $nullValue = '';

	public $model;

	public function generateSheet()
	{
		//Initialize the xls file to export
		$date = Yii::app()->dateFormatter->format('yyyy-MM-dd', time());
		require_once(Yii::getPathOfAlias( 'application.components.exportXls' ) . '/export-xls.class.php');

		$filename = $this->filename;
		$filename .= $this->appendDateOnFilename ? $date . '_' . rand(0, 10000) : '';
		$filename .= '.xls';

		$sheet = new ExportXLS($filename);
				
		$line = array();
		if(isset($this->header) && strlen($this->header) > 0)
		{
			$header = $this->header;
			$header .= $this->appendDateOnHeader ? $date : '';
			$sheet->addRow(array($header));
		}

		foreach($this->attributes as $attr)
		{
			$line[] = isset($attr['label']) ? $attr['label'] : $this->model->getAttributeLabel($attr['attribute']);
		}
		$sheet->addRow($line);
		
		foreach($this->data as $row){
			$line = array();
		
			foreach($this->attributes as $attr)
			{
				$line[] = $this->renderAttribute($row, $attr);
			}

			$sheet->addRow($line);		
		}
		
		app()->request->sendFile($filename, $sheet->returnSheet(), NULL, true);
		return true;	

	}


	private function renderLabel($attr)
	{
		if(isset($attr['label']))
		{
			return $attr['label'];
		}

		return $this->model->getAttributeLabel($attr['attribute']);
	}

	private function renderAttribute($rowModel, $attr)
	{
		$modelAttr = $attr['attribute'];
		

		//Handle relations
		if(isset($attr['relation']))
		{
			$relName = $attr['relation'];

			if(!is_null($relName))
			{
				$dot = strpos($relName, '.');
				if($dot != false)
				{
					$relArray = explode(".", $relName);
					$rowModelRelated = $rowModel;
					foreach($relArray as $r)
					{
						$rowModelRelated = $rowModelRelated->$r;
					}

					return $rowModelRelated->$modelAttr;
				}

				if(isset($rowModel->$relName) && isset($rowModel->$relName->$modelAttr))
				{
                    //Call custom display function
                    if(isset($attr['displayFunction']))
                    {
                        $fName = $attr['displayFunction'];
                        $params = isset($attr['params']) ? $attr['params'] : array();
                        return call_user_func(array($rowModel->$relName,$fName), $params);
                    }
                    return $rowModel->$relName->$modelAttr;
				}
			}

			return $this->nullValue;
		}


        //Execute custom functions
        if(isset($attr['displayFunction']))
        {
            $fName = $attr['displayFunction'];

            $params = isset($attr['params']) ? $attr['params'] : array();
            return call_user_func(array($rowModel,$fName), $params);
        }

		if(isset($attr['type']))
		{
			switch ($attr['type']) {
				case 'bool':
					return $rowModel->$modelAttr ? Yii::t('app', 'Yes') : Yii::t('app', 'No');
					break;
				
				case 'date':
					return app()->dateFormatter->format("dd/MM/yyyy", $rowModel->$modelAttr);
					break;
			}
		}

		return $rowModel->$modelAttr;
	}

}
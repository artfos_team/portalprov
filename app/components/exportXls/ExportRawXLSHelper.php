<?php

class ExportRawXLSHelper
{

	public $filename;

	public $header;

	public $appendDateOnHeader = true;

	public $appendDateOnFilename = true;

	public $data;

	public function generateSheet()
	{
		//Initialize the xls file to export
		$date = Yii::app()->dateFormatter->format('yyyy-MM-dd', time());
		require_once(Yii::getPathOfAlias( 'application.components.exportXls' ) . '/export-xls.class.php');

		$filename = $this->filename;
		$filename .= $this->appendDateOnFilename ? $date . '_' . rand(0, 10000) : '';
		$filename .= '.xls';

		$sheet = new ExportXLS($filename);
				
		if(isset($this->header) && strlen($this->header) > 0)
		{
			$header = $this->header;
			$header .= $this->appendDateOnHeader ? $date : '';
			$sheet->addRow(array($header));
		}

		foreach($this->data as $row=>$rowData){
            $titleRow = null;
            if($row == 0){
                $titleRow = array();
            }
			$line = array();
		
			foreach($rowData as $name=>$value)
			{
                if($row == 0){
                    $titleRow[] = $name;
                }
				$line[] = $value;
			}

            if($row == 0){
                $sheet->addRow($titleRow);
            }
			$sheet->addRow($line);
		}
		
		app()->request->sendFile($filename, $sheet->returnSheet(), NULL, true);
		return true;
	}
}
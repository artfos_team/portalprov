<?php

// shared application configuration
return array(
    'yiiPath' => __DIR__ . '/../../vendor/yiisoft/yii/framework/yii.php',
    'yiicPath' => __DIR__ . '/../../vendor/yiisoft/yii/framework/yiic.php',
    'yiitPath' => __DIR__ . '/../../vendor/yiisoft/yii/framework/yiit.php',

    // Set YII_DEBUG and YII_TRACE_LEVEL flags
    'yiiDebug' => true,
    'yiiTraceLevel' => 0,

    'configWeb' => array(
        'basePath' => realpath(__DIR__ . '/..'),
        // application name
        'name' => 'IProveedores',
        // application language
        'language' => 'es',
        // path aliases
        'aliases' => array(
            'app' => 'application',
            'vendor' => realpath(__DIR__ . '/../../vendor'),
        ),
        // components to preload
        'preload' => array('log', 'bootstrap'),
        // paths to import
        'import' => array(
            'application.helpers.*',
            'application.models.*',
            'application.models.ar.*',
            'application.models.form.*',
            'application.models.report.*',
            'application.components.*',
            'application.components.interfaces.*',
            'application.components.wsAfip.*',
            'vendor.crisu83.yiistrap.helpers.TbHtml',
            'vendor.nordsoftware.yii-emailer.models.*',
        ),
        // application components
        'components' => array(
            'urlManager'=>array(
                'urlFormat'=>'path',
                'showScriptName'=>true,
                'rules'=>array(
                    'gii'=>'gii',
                    'gii/<controller:\w+>'=>'gii/<controller>',
                    'gii/<controller:\w+>/<action:\w+>'=>'gii/<controller>/<action>',
                    // REST patterns
                    

                    
                    array('api/saveNewProvider', 'pattern'=>'api/<action:saveNewProvider>', 'verb'=>'POST'),
                    array('api/allCertification', 'pattern'=>'api/<action:allCertification>/<id:\w+>', 'verb'=>'GET'),
                    array('api/saveNewProviderRegister', 'pattern'=>'api/<action:saveNewProviderRegister>', 'verb'=>'POST'),
                    array('api/preflight', 'pattern'=>'api/*', 'verb'=>'OPTIONS'),
                    array('api/refreshToken', 'pattern'=>'api/<action:refreshModel>', 'verb'=>'GET'),
                    array('api/getPersonaAfip', 'pattern'=>'api/<action:getPersonaAfip>/<id:\w+>', 'verb'=>'GET'),
                    array('api/delete', 'pattern'=>'api/delete/<model:\w+>/<id:\d+>', 'verb'=>'POST'),
                    array('api/login', 'pattern'=>'api/<action:login>', 'verb'=>'POST'),
                    array('api/createrubro','pattern'=>'api/<action:createrubro>', 'verb'=>'POST'),
                    array('api/createlicitacion','pattern'=>'api/<action:createlicitacion>', 'verb'=>'POST'),
                    array('api/sendMessages','pattern'=>'api/<action:sendMessages>', 'verb'=>'POST'),
                    array('api/guardarProveedoresLicitacion','pattern'=>'api/<action:GuardarProveedoresLicitacion>', 'verb'=>'POST'),
                    array('api/invitarProveedoresLicitacion','pattern'=>'api/<action:InvitarProveedoresLicitacion>', 'verb'=>'POST'),
                    array('api/uploadFile','pattern'=>'api/<action:uploadFile>', 'verb'=>'POST'),
                    array('api/getPropuestas', 'pattern'=>'api/<action:getPropuestas>/<id:\d+>', 'verb'=>'GET'),
                    array('api/getBuyOrder','pattern'=>'api/<action:getBuyOrder>/<id:\d+>', 'verb'=>'GET'),
                    array('api/getBuyOrderDocument','pattern'=>'api/<action:getBuyOrderDocument>/<id:\d+>', 'verb'=>'GET'),
                    array('api/getOtherOperations','pattern'=>'api/<action:getOtherOperations>/<id:\d+>', 'verb'=>'GET'),
                    //array('api/getLastProviderDocument','pattern'=>'api/<action:getLastProviderDocument>/<document:\d+>/<id:\d+>', 'verb'=>'GET'),
                    
                    array('api/listByAnonymous', 'pattern'=>'api/<action:listByAnonymous>', 'verb'=>'GET'),
                    array('api/listByFilters', 'pattern'=>'api/<action:listByFilters>', 'verb'=>'GET'),
                    array('api/getLicitaciones','pattern'=>'api/<action:getLicitaciones>/<id:\d+>', 'verb'=>'GET'),
                    array('api/solicitudUsuario', 'pattern'=>'api/<action:solicitudUsuario>', 'verb'=>'GET'),
                    array('api/changePsw', 'pattern'=>'api/<action:changePsw>', 'verb'=>'POST'),
                    array('api/sendComment', 'pattern'=>'api/<action:sendComment>', 'verb'=>'GET'),
                    array('api/getComments', 'pattern'=>'api/<action:getComments>', 'verb'=>'GET'),
                    array('api/readComment', 'pattern'=>'api/<action:readComment>', 'verb'=>'GET'),
                    array('api/reestablecerPsw', 'pattern'=>'api/<action:reestablecerPsw>', 'verb'=>'GET'),
                    array('api/countStatus', 'pattern'=>'api/<action:countStatus>', 'verb'=>'GET'),
                    array('api/checkParam', 'pattern'=>'api/<action:checkParam>', 'verb'=>'GET'),
                    array('api/getStatus', 'pattern'=>'api/<action:getStatus>', 'verb'=>'GET'),
                    array('api/solicitarReestablecimientoPsw', 'pattern'=>'api/<action:solicitarReestablecimientoPsw>', 'verb'=>'GET'),
                    array('api/finSolicitud', 'pattern'=>'api/<action:finSolicitud>', 'verb'=>'POST'),
                    array('api/companydocument','pattern'=>'api/<action:companydocument>/<model:\w+>/<id:\d+>', 'verb'=>'GET'),
                    array('api/updateCalification','pattern'=>'api/<action:updateCalification>/<model:\w+>/<id:\d+>/<calification:\d+>/<reason:\w+>', 'verb'=>'GET'),
                    array('api/updateDocumentation','pattern'=>'api/<action:updateDocumentation>/<id:\d+>/<documentation:\w+>', 'verb'=>'GET'),
                    array('api/companyOperations','pattern'=>'api/<action:companyOperations>/<id:\d+>', 'verb'=>'GET'),
                    array('api/DocumentRecord','pattern'=>'api/<action:DocumentRecord>/<id:\d+>', 'verb'=>'GET'),
                    array('api/AssociatedUsers','pattern'=>'api/<action:AssociatedUsers>/<id:\d+>', 'verb'=>'GET'),
                    array('api/sendMailProvider','pattern'=>'api/<action:sendMailProvider>/<model:\w+>/<id:\d+>', 'verb'=>'GET'),
                    array('api/SendMailExtendedDateCompulsa','pattern'=>'api/<action:SendMailExtendedDateCompulsa>/<model:\w+>/<id:\d+>', 'verb'=>'GET'),
                    array('api/statusProvider','pattern'=>'api/<action:statusProvider>/', 'verb'=>'GET'),
                    array('api/deleteProvider','pattern'=>'api/<action:deleteProvider>/<model:\w+>/<id:\d+>', 'verb'=>'GET'),
                    array('api/getMessages','pattern'=>'api/<action:getMessages>/<id:\d+>', 'verb'=>'GET'),
                    array('api/findConfiguration','pattern'=>'api/<action:findConfiguration>/<attribute:\w+>', 'verb'=>'GET'),
                    array('api/deleteRubrosFromCompany','pattern'=>'api/<action:deleteRubrosFromCompany>/<id:\d+>', 'verb'=>'GET'),
                    array('api/DocumentRecord','pattern'=>'api/<action:DocumentRecord>/<id:\d+>', 'verb'=>'GET'),
                    array('api/canDeleteCompany','pattern'=>'api/<action:canDeleteCompany>/<id:\d+>', 'verb'=>'GET'),
                    array('api/enviarMailMensajeNuevo','pattern'=>'api/<action:enviarMailMensajeNuevo>/<id:\d+>', 'verb'=>'GET'),
                    array('api/sendMailSignUp','pattern'=>'api/<action:sendMailSignUp>/<id:\d+>', 'verb'=>'GET'),
                    array('api/sendMailUserActivation','pattern'=>'api/<action:sendMailUserActivation>/<id:\d+>', 'verb'=>'GET'),
                    array('api/sendMailUserReject','pattern'=>'api/<action:sendMailUserReject>/<id:\d+>', 'verb'=>'GET'),
                    array('api/sendMailRejectedDocument','pattern'=>'api/<action:sendMailRejectedDocument>/<id:\d+>', 'verb'=>'GET'),
                    array('api/sendMailAcceptedDocument','pattern'=>'api/<action:sendMailAcceptedDocument>/<id:\d+>', 'verb'=>'GET'),
                    array('api/getProviderDocuments','pattern'=>'api/<action:getProviderDocuments>/<id:\d+>', 'verb'=>'GET'),
                    array('api/sendMailBuyOrderStatus','pattern'=>'api/<action:sendMailBuyOrderStatus>/<id:\d+>', 'verb'=>'GET'),
                    array('api/sendMailBuyOrderDocumentStatus','pattern'=>'api/<action:sendMailBuyOrderDocumentStatus>/<id:\d+>', 'verb'=>'GET'),
                    array('api/deleteLicitacionesConsulta','pattern'=>'api/<action:deleteLicitacionesConsulta>/<id:\d+>', 'verb'=>'GET'),
                    array('api/updateStartCompanyStatus','pattern'=>'api/<action:updateStartCompanyStatus>/<id:\d+>', 'verb'=>'GET'),
                    array('api/updateStopCompanyStatus','pattern'=>'api/<action:updateStopCompanyStatus>/<id:\d+>', 'verb'=>'GET'),
                    array('api/associateHesWithDocument', 'pattern'=>'api/<action:associateHesWithDocument>', 'verb'=>'POST'),
                    
                    array('api/statusMessages','pattern'=>'api/<action:statusMessages>/<id:\d+>/<mode:\d+>', 'verb'=>'GET'),

                    array('api/count', 'pattern'=>'api/count/<model:\w+>', 'verb'=>'GET'),
                    array('api/list', 'pattern'=>'api/generic/<model:\w+>', 'verb'=>'GET'),
                    array('api/view', 'pattern'=>'api/generic/<model:\w+>/<id:\d+>', 'verb'=>'GET'),
                    array('api/update', 'pattern'=>'api/generic/<model:\w+>/<id:\d+>', 'verb'=>'POST'),
                    array('api/create', 'pattern'=>'api/generic/<model:\w+>', 'verb'=>'POST'),

                    // Other controllers
                    '<controller:\w+>/<id:\d+>'=>'<controller>/view',
                    '<controller:\w+>/<action:\w+>/<id:\d+>'=>'<controller>/<action>',
                    '<controller:\w+>/<action:\w+>'=>'<controller>/<action>',
                    'api/<controller:\w+>/<action:\w+>/<id:\d+>'=>'api/<controller>/<action>',
                    'api/<controller:\w+>/<action:\w+>'=>'api/<controller>/<action>',
                    // REST patterns
                    //array('api/list', 'pattern'=>'api/<model:\w+>/<action:list>', 'verb'=>'GET'),///<id:\d+>
                ),
            ),


            'errorHandler' => array(
                'errorAction' => 'site/error',
            ),
            'format' => array(
                'class' => 'vendor.crisu83.yii-formatter.components.Formatter',
                'formatters' => array(),
            ),
            'flashMessage' => array(
                'class' => 'application.extensions.flashmessages.FlashMessageManager',
            ),
            'rbac'=>array(
                'class'=>'application.components.Rbac'
            ),
            // uncomment the following to enable the imagemanager extension
            /*
            'imageManager' => array(
                'class' => 'vendor.crisu83.yii-imagemanager.components.ImageManager',
                'presets' => array(
                ),
            ),
            */
            'log' => array(
                'class' => 'CLogRouter',
                'routes' => array(
                    array(
                        'class' => 'CFileLogRoute',
                        'levels' => 'error, warning',
                    ),
                    // uncomment the following to show log messages on web pages
                    /*
                    array(
                        'class'=>'CWebLogRoute',
                    ),
                    */
                ),
            ),
            'bootstrap'=>array(
                'class'=>'bootstrap.components.Bootstrap', // assuming you extracted bootstrap under extensions
                'responsiveCss'=>true,
                'fontAwesomeCss'=>true,
            ),

            'user'=>array(
                'class' => 'WebUser',
                'authTimeout'=>4000,
            ),
            'mailer' => array(
                'class' => 'application.extensions.mailer.EMailer',
            )

        ),
        // application parameters
        'params' => array(
            'adminEmail' => 'webmaster@example.com',
            'fromEmail'=>'info@portal.com', //Mail used to send confirmation offer
            'smtpAuth' => true,
            'smtpSecure' => 'tls',
            'mailerUsername' => 'info@portal.com',
            'mailerPassword' => '3yFoLmK25I16d8Eyc_Btrw',
            //'mailerHost' => '',
            'mailerHost' => 'smtp.mandrillapp.com',
            'mailerPort' => 587,
            'mailerFrom' => 'info@portal.com',
            'mailerFromName' => 'info@portal.com',
            'notificationEmails' => array('nsavini@flydreamers.com'),
            'phantomCommand' => 'phantomjs ../app/printheaderfooter.js',
            'hostinfo'=>'http://localhost',
            'baseUrl'=>'http://localhost/iproveedores/',
            'enabled' => true,
            'key' => '6LdsJtEUAAAAAKYTuuM3rWVyttE2EuQzwMp_ZwnO',
            'secret' => '6LdsJtEUAAAAADXxOMHs8HHgMH_gieclgOJR8hV5',



            'orderCat_COMPRA_INGRESADA'=> 47,
            'orderCat_COMPRA_GENERADA'=> 48,
            'orderCat_COMPRA_A_FACTURAR'=> 49,
            'orderCat_COMPRA_RECHAZADA'=> 50,
            'orderCat_FACTURA_CARGADA'=> 51,
            'orderCat_FACTURA_CONFIRMADA'=> 52,
            'orderCat_FACTURA_RECIBIDA'=> 53,
            'orderCat_FACTURA_PEND_APROBACION'=> 54,
            'orderCat_CONTABILIZADO'=> 55,
            'orderCat_PEND_PAGO'=> 56,
            'orderCat_PAGO_DISPONIBLE_BANCO'=> 57,



        ),
        'modules'=>array(
            'mailer'=>array(
                'class' => 'application.modules.mailer.MailerModule',
                'templateAliasPath' => array(
                    'participantProgram' => 'mailer.templates.participantProgram',
                    'participantReport' => 'mailer.templates.participantReport',
                    'order' => 'mailer.templates.order',
                ),
            ),

            'gii' => array(
                'class' => 'system.gii.GiiModule',
                'password' => 'yii',
                'ipFilters' => array('127.0.0.1', '10.0.2.2', '192.168.1.*', '172.16.238.1' /* Vagrant */, '::1'/* WAMP */),
                //'generatorPaths' => array(
                //    'bootstrap.gii'
                //),
            ),
        ),
    ),
    'configConsole' => array(
        'basePath' => realpath(__DIR__ . '/..'),
    )
);
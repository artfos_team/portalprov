<?php

Yii::import('application.models.EmailParameters');
Yii::import('application.models.Email');
Yii::import('application.models.EmailReceiver');
require_once dirname(__FILE__).DIRECTORY_SEPARATOR. '../extensions/mailer/phpmailer/class.phpmailer.php';

class SendEmailsCommand extends CConsoleCommand 
{
    protected $mailer;

    private function initialiseMailer() {
        if (!isset($this->mailer)) {
            echo "Genero mailer\n";
            $emailParameters = EmailParameters::model()->findByPk(1);
            $this->mailer = new PHPMailer();
			$this->mailer->SetFrom($emailParameters->user);
			$this->mailer->FromName = $emailParameters->fromName;
            $this->mailer->IsHTML (true);
            $this->mailer->CharSet = 'UTF-8';
			$this->mailer->IsSMTP();
			$this->mailer->Host = $emailParameters->smtp;
			$this->mailer->Port = $emailParameters->port;
			$this->mailer->SMTPSecure = $emailParameters->security;
			$this->mailer->SMTPAuth = true;
			$this->mailer->Username = $emailParameters->user;
            $this->mailer->Password = $emailParameters->userPsw;
            $this->mailer->SMTPKeepAlive = true;
        }
    }

    public function actionSend($dryRun = false, $debug = false)
    {
        try {
            $this->initialiseMailer();
            $this->sendEmails();
            $this->purgeTable();
        } catch (Exception $ex) {
            echo $ex->getMessage();
        }
    }

    public function sendEmails () {
        $emails = Email::model()->findAll("sent = 0");
        foreach ($emails as $key => $email) {
            if ($this->sendEmailToReceivers($email)) {
                $email->sent = '1';
                $email->save();
            }
        }
        if (count($emails) > 0) {
            echo "\nSe han enviado los emails.";
        }
        else {
            echo "\nNo hay emails pendientes de envío";
        }
    }

    public function sendEmailToReceivers($email) {

        $receivers = $email->receivers;

        $this->mailer->Subject = $email->subject;
        $this->mailer->MsgHTML($email->body);
        foreach ($receivers as $receiver) {
            $this->mailer->AddBCC($receiver->email); // Email Destinatario	
        }
        
        if(count($receivers) > 0 && $this->mailer->Send()) {
            $this->mailer->ClearAllRecipients();
            echo "\n Emails enviados \n";
            return true;
        } else {
            $this->mailer->ClearAllRecipients();
            echo "\n Error al enviar los emails \n";
            return false;
        }
    }

    private function purgeTable() {
        EmailReceiver::model()->deleteAll('idEmail in (SELECT idEmail from email where date_created < NOW() - INTERVAL 7 DAY)');
        Email::model()->deleteAll('date_created < NOW() - INTERVAL 7 DAY');
    }
}
<?php

Yii::import('application.models.Licitaciones');
Yii::import('application.models.LicitacionesProveedor');
Yii::import('application.models.UserHasRoles');
Yii::import('application.models.EmailParameters');
Yii::import('application.models.Provider');
Yii::import('application.models.ar.ActiveRecord');
Yii::import('application.models.ar.User');
Yii::import('application.models.Email');
Yii::import('application.models.EmailReceiver');

class FinishTenderCommand extends CConsoleCommand 
{

    public function actionCheck($dryRun = false, $debug = false)
    {
        try {
            $this->finishTenders();
            $this->remindTenders();
        } catch (Exception $ex) {
            echo $ex->getMessage();
        }
    }

    public function remindTenders () {
        $licitaciones = Licitaciones::model()->findAll("status = 2 AND reminder = 0 AND fecha_fin <= NOW() + INTERVAL 3 DAY");
            foreach ($licitaciones as $key => $value) {
                $this->sendEmailReminder($value);
                $value->reminder = '1';
                $value->save();
            }
            if (count($licitaciones) > 0) {
                echo "\nSe ha enviado el recordatorio de " . count($licitaciones) ." compulsas.";
            }
            else {
                echo "\nNo hay compulsas que estén por vencer";
            }
    }

    public function finishTenders () {
        $licitaciones = Licitaciones::model()->findAll("status = 2 AND fecha_fin <= NOW()");
            foreach ($licitaciones as $key => $value) {
                $this->sendMailFinishedTenders($value->codPO);
                $value->status = '3';
                $value->save();
            }
            if (count($licitaciones) > 0) {
                echo "\nSe han finalizado " . count($licitaciones) ." compulsas.";
            }
            else {
                echo "\nNo hay compulsas que necesiten ser finalizadas";
            }
    }

    public function sendMailFinishedTenders($codPO)
    {
        
        $adminMails = $this->getAllUsersFromPurchases();
			//This should be done in your php.ini, but this is how to do it if you don't have access to that
			$mail = new Email();
			//configuracion fija
            $emailParameters = EmailParameters::model()->findByPk(1);
            $customEmail = CustomEmail::model()->findByPk(3);
            eval("\$sendSubject = \"$customEmail->subject\";");
            $mail->subject = $sendSubject;
            //$mail->subject ="Invitación finalizada – Petición de Oferta N° ". $codPO;
			//$mail->AddCC($participant->consultant->email);
			
			$date = date("d/m/Y H:i:s");
			/*$mail->body =
				'<div style="text-align: justify; font-size: 16px;">
                <p>Le informamos que la etapa para presentar una oferta técnica y económica ha finalizado.</p>
                
                <p>Este mensaje fue generado automáticamente por registrarse o realizar alguna acción en nuestro sitio de proveedores, se ha contactado vía mail o por teléfono con CONTRERAS. Si usted desconoce alguna acción de las antes mencionadas, descarte este mail. El contenido es propiedad de CONTRERAS y garantiza la privacidad y seguridad de su información.</p>
				</div>'							
            ;*/ //Fin message HTML
            $mail->body = $customEmail->body;

            $mail->save();
            foreach ($adminMails as $value) {
                $mail->addReceiver($value); // Email Destinatario	
            }
    }

    public function sendEmailReminder($licitacion) {

        $adminMails = $this->getAllUsersFromPurchases();
        $providerMails = $this->getAllProvidersFromTender($licitacion->idLicitacion);

        	$mail = new Email();
			//configuracion fija
            $emailParameters = EmailParameters::model()->findByPk(1);
            $customEmail = CustomEmail::model()->findByPk(4);
            // Para providers
            eval("\$sendSubject = \"$customEmail->subject\";");
            $mail->subject = $sendSubject;
            //$mail->subject ="Recordatorio | Invitación a Cotizar – Petición de Oferta N° ". $licitacion->codPO;
			//$mail->AddCC($participant->consultant->email);
			
			/*$mail->body =
				'<div style="text-align: justify; font-size: 16px;">
                <p>Estimado, le recordamos que en 72hs vence el plazo previsto para la para la realización de una oferta técnica y económica. Por favor, en caso de dudas, contáctese a: compras@contreras.com.ar </p>
           
                <p>Este mensaje fue generado automáticamente por registrarse o realizar alguna acción en nuestro sitio de proveedores, se ha contactado vía mail o por teléfono con CONTRERAS. Si usted desconoce alguna acción de las antes mencionadas, descarte este mail. El contenido es propiedad de CONTRERAS y garantiza la privacidad y seguridad de su información.</p>
				</div>'							
            ; *///Fin message HTML
            $mail->body = $customEmail->body;

			if(count($providerMails) > 0 && $mail->save())
			{
                foreach ($providerMails as $value) {
                    $mail->addReceiver($value); // Email Destinatario	
                }
				// Para providers
                //$mail->subject ="Recordatorio | Invitación a Cotizar – Petición de Oferta N° ". $licitacion->codPO;
                $mail->subject = $sendSubject;
                //$mail->AddCC($participant->consultant->email);
                
                /*$mail->body =
                    '<div style="text-align: justify; font-size: 16px;">
                    <p>Estimado, le recordamos que en 72hs vence el plazo previsto para la para la realización de una oferta técnica y económica.</p>
                  
                    <p>Este mensaje fue generado automáticamente por registrarse o realizar alguna acción en nuestro sitio de proveedores, se ha contactado vía mail o por teléfono con CONTRERAS. Si usted desconoce alguna acción de las antes mencionadas, descarte este mail. El contenido es propiedad de CONTRERAS y garantiza la privacidad y seguridad de su información.</p>
                    </div>'							
                ; *///Fin message HTML
                $mail->body = $customEmail->body;
    
                if(count($adminMails) > 0 && $mail->save()) {
                    foreach ($adminMails as $value) {
                        $mail->addReceiver($value); // Email Destinatario	
                    }
                    echo "\n Mails enviados \n";
                } else {
                    echo "\n Error al enviar los recordatorios \n";
                }
			}
    }

    private function getAllUsersFromPurchases() {
        $adminMails = [];

        $users = User::model()->findAll();
        foreach ($users as $index => $user) {
            if($user->hasRole(18) || $user->hasRole(1))
            {
                $adminMails[$index] = $user->email; 
            }
        }
        return $adminMails;
    }

    /*
    BUSCAR LOS MAILS DE LOS PROVEEDORES TENIENDO LA LICITACION
    */
    private function getAllProvidersFromTender($licitacion) {
        $users = [];
        $licitacionProveedores = LicitacionesProveedor::model()->findAll("idLicitacion = ". $licitacion);
        foreach ($licitacionProveedores as $index => $licitacionProveedor) {
            $proveedor =  Provider::model()->findByPk($licitacionProveedor->idProvider);
            $users[$index] = User::model()->findByPk($proveedor->idUser)->email;
        }
        return $users;
    }
}
<?php

Yii::import('application.models.import.FileFormat');
Yii::import('application.models.import.FileColumn');
Yii::import('application.models.import.ImportSchedule');
Yii::import('application.models.Company');
Yii::import('application.models.Import');
Yii::import('application.models.Hes');
Yii::import('application.models.ImportError');
Yii::import('application.models.BuyOrder');
Yii::import('application.models.BuyOrderHistory');
Yii::import('application.models.BuyOrderStatus');
Yii::import('application.models.Licitaciones');
Yii::import('application.models.EmailParameters');
Yii::import('application.models.Provider');
Yii::import('application.models.Certification');
Yii::import('application.models.CertificationExGain');
Yii::import('application.models.CertificationRG17');
Yii::import('application.models.CertificationRG18');
Yii::import('application.models.CertificationRG2681');
Yii::import('application.models.ar.User');
Yii::import('application.models.ar.ActiveRecord');
Yii::import('application.models.Email');
Yii::import('application.models.EmailReceiver');

class SyncCommand extends CConsoleCommand
{
    /**
     * @param $idSchedule
     *
     * @throws Exception
     */
    public function actionRun($idSchedule)
    {
        $schedule = ImportSchedule::model()->findByPk($idSchedule);
        echo "id". $idSchedule;
        $this->importByFormat($schedule->format); 

    }

    public function actionIndex($idSchedule = 0) {
        $this->actionRun($idSchedule);
    }
    /**
     * @param $format
     * @throws Exception
     */

    private function exportProviders(){

        $lista = array (
            array('aaa', 'bbb', 'ccc', 'dddd'),
            array('123', '456', '789'),
            array('"aaa"', '"bbb"')
        );
        
        $fp = fopen('fichero.csv', 'w');
        
        foreach ($lista as $campos) {
            fputcsv($fp, $campos);
        }
        
        fclose($fp);


    }



    private function importByFormat($format) {

        if (!file_exists($format->url)) {
            echo "No se ha encontrado el archivo en . $format->url\n";
            return;
        }

        if (($csvFile = fopen($format->url, "r")) !== FALSE) {
            $newImport = new Import();
            $newImport->totalImport = 0;
            $newImport->errorImport = 0;
            $newImport->dateImport = (new DateTime())->format('Y-m-d H:i');
            $newImport->save();
             
            if ($format->type == "AI" ){
                Certification::model()->deleteAll('type_certification = :type_certification', array(':type_certification' => 'SUSSS'));
                echo "SE BORRA SUSSS\n";
            } elseif ($format->type == "RG"){
                CertificationExGain::model()->deleteAll();
                echo "SE BORRA Ganancias\n";
            }elseif ($format->type == "IV"){
                CertificationRG18::model()->deleteAll();
                echo "SE BORRA Retencion IVA\n";
            }elseif ($format->type == "IG"){
                CertificationRG2681::model()->deleteAll();
            echo "SE BORRA Retencion IVA Ganancias\n";
            }elseif ($format->type == "EI"){
                CertificationRG17::model()->deleteAll();
            echo "SE BORRA CERTIFICADO IVA \n";
        }

        if ($format->type == "IG"){
           $separation = "\t";
        }else{
            $separation= ";";
        }

            while (($data = fgetcsv($csvFile, 0, $separation)) !== FALSE) {
                try {
                    $newImport->totalImport += 1;
                    echo "format type: . $format->type";
                    switch ($format->type) {
                        case $this->getType(FileFormat::NIC):
                            $this->saveNic($newImport, $format, $data);
                            $this->changeStatusNics($newImport);
                            break;
                        case $this->getType(FileFormat::PROV):
                            $this->saveProvider($newImport, $format, $data);
                            break;
                        case $this->getType(FileFormat::PO):
                            $this->savePO($newImport, $format, $data);
                            break;
                        case $this->getType(FileFormat::AI):
                            echo "save certification \n";
                            $this->saveCertification($newImport, $format, $data);
                            break;
                        case $this->getType(FileFormat::RG):
                            echo "save retencionIVA \n";
                            $this->saveCertificationRG($newImport, $format, $data);
                        break;  
                        case $this->getType(FileFormat::IV):
                            echo "save Agente retencion IVAIV\n  ";
                            var_dump($data); 
                            $this->saveCertificationRG18($newImport, $format, $data);
                        break;
                        case $this->getType(FileFormat::IG):
                            echo "save  IVA Ganancias\n  ";
                            var_dump($data); 
                            $this->saveCertificationRG2681($newImport, $format, $data);
                        break;
                        case $this->getType(FileFormat::EI):
                            echo "Exclusion IVA ";
                            var_dump($data); 
                            $this->saveCertificationRG17($newImport, $format, $data);
                        break;           
                        default:
                            echo "Formato de importación inválido \n";
                    }
                }
                catch(Exception $ex) {
                    echo $ex->getMessage();
                }
            }
            fclose($csvFile);
            $newImport->save();
        }
    }

    private function createImportError($import, $msg) {
        $importError = new ImportError();
        $importError->idImport = (int)$import->idImport;
        $importError->description = $msg;
        $importError->status = 0;
        $importError->save();
        $import->errorImport += 1;
        $import->save();
        throw new Exception($msg);
    }

    /**
     * @param Import $import
     * @param FileFormat $format
     * @param $line
     *
     * @throws Exception
     */


    private function saveCertification($import, $format, $line){
        try {
            $dataCert = array();
            $dataCert['type_certification'] = 'SUSSS' ;
            foreach ($format->importColumns as $index => $column) {
                $dataColumn = $line[$index];
                
                if ($column->name == $this->getType(FileColumn::IGNORE)) {
                    echo "IGNORE\n";
                    continue;
                }
                switch ($column->name) {
                    case $this->getType(FileColumn::CUIT):
                        $dataCert['cuit'] = $dataColumn;
                        echo "cuit:" .$dataCert['cuit'] ."\n";

                        break;
                    case $this->getType(FileColumn::NUMBER):
                        $dataCert['number'] = $dataColumn;
                        echo "number:" .$dataCert['number'] ."\n";

                        break;
                    case $this->getType(FileColumn::PERIOD):
                        $dataCert['period'] = "0";
                        echo "period" .$dataCert['period'] . "\n";
                        break;    

                    case $this->getType(FileColumn::RESOLUTION):
                            $dataCert['resolution'] = $dataColumn;
                            echo "resolution" .$dataCert['resolution'] . "\n";
                            break;   
                    case $this->getType(FileColumn::PERCENTAGE):
                        $dataCert['percentage'] = $dataColumn;
                        echo "percentage" .$dataCert['percentage'] . "\n";
                        break;

                    case $this->getType(FileColumn::STATE):
                        $dataCert['state'] = $dataColumn;
                        echo "state" .$dataCert['state'] . "\n";
                        break;
                    case $this->getType(FileColumn::SINCE):
                        $dataCert['since'] = $dataColumn;
                        echo "since" .$dataCert['since'] . "\n";
                        break;
                    case $this->getType(FileColumn::UNTIL):
                        $dataCert['until'] = trim($dataColumn);
                        echo "until" .trim($dataCert['until']) . "\n";
                        break;
                }
            }


            $cert = new Certification();
            $import->createImport += 1;
            $cert->setAttributes($dataCert);
            var_dump ($dataCert);
            $certSaved = $cert->save();

            if ($certSaved) {
                echo "se guardo.\n";
            } else {
                echo "no se guardo\n";
            }
        }
        catch (Exception $ex) {
            echo $ex->getMessage();
        }
    
    }

         /**
     * @param Import $import
     * @param FileFormat $format
     * @param $line
     *
     * @throws Exception
     */


    private function saveCertificationRG2681($import, $format, $line){
        try {
            $dataCert = array();
            $array_num = count($line);
            foreach ($format->importColumns as $index => $column) {
                if ($array_num <= 3 ){
                    continue;
                } 
                $dataColumn = $line[$index];
                if ($column->name == $this->getType(FileColumn::IGNORE)) {
                    echo "IGNORE\n";
                    continue;
                }
                 
                switch ($column->name) {
                    case $this->getType(FileColumn::CUITRG2681):
                        $dataCert['cuit'] = $dataColumn;
                        echo "cuit:" .$dataCert['cuit'] ."\n";
                        break;
                    case $this->getType(FileColumn::TYPERG2681):
                        $dataCert['type'] = $dataColumn;
                        echo "type:" .$dataCert['type'] ."\n";

                        break;
                        case $this->getType(FileColumn::STATERG2681):
                            $dataCert['state'] = $dataColumn;
                            echo "state" .$dataCert['state'] . "\n";
                            break;   

                        case $this->getType(FileColumn::STATEDESCRIPTIONRG2681):
                                $dataCert['stateDescription'] = $dataColumn;
                                echo "stateDescription" .$dataCert['stateDescription'] . "\n";
                        break;   
            
                        case $this->getType(FileColumn::EMISIONRG2681):
                        $dataCert['emision'] = $dataColumn;
                        echo "emision" .$dataCert['emision'] . "\n";
                        break;    

                    case $this->getType(FileColumn::ADMINISTRATIONDATERG2681):
                            $dataCert['administrationDate'] = $dataColumn;
                            echo "administrationDate" .$dataCert['administrationDate'] . "\n";
                            break; 
                            case $this->getType(FileColumn::AUTHRG2681):
                                $dataCert['auth'] = trim($dataColumn);
                                echo "auth" .$dataCert['auth'] . "\n";
                                break;  
                    case $this->getType(FileColumn::DDJJRG2681):
                        $dataCert['ddjj'] = trim($dataColumn);
                        echo "ddjj" .$dataCert['ddjj'] . "\n";
                        break;
                    case $this->getType(FileColumn::SUBSECTIONRG2681):
                            $dataCert['subsection'] = trim($dataColumn);
                            echo "subsection" .$dataCert['subsection'] . "\n";
                            break;
                    case $this->getType(FileColumn::SINCERG2681):
                                $dataCert['since'] = trim($dataColumn);
                                echo "since" .$dataCert['since'] . "\n";
                    break; 
                    case $this->getType(FileColumn::UNTILRG2681):
                        $dataCert['until'] = trim($dataColumn);
                        echo "until" .$dataCert['until'] . "\n";
                    break;
                    case $this->getType(FileColumn::ORDERJUDRG2681):
                        $dataCert['orderJud'] = trim($dataColumn);
                        echo "orderJud" .$dataCert['orderJud'] . "\n";
                    break;   
                    case $this->getType(FileColumn::NUMBERRG2681):
                        $dataCert['number'] = trim($dataColumn);
                        echo "number" .$dataCert['number'] . "\n";
                    break;   
                    case $this->getType(FileColumn::MODIFICATIONDATERG2681):
                        $dataCert['modificationDate'] = trim($dataColumn);
                        echo "modificationDate" .$dataCert['modificationDate'] . "\n";
                    break;       
                }
            }


            $cert2681 = new CertificationRG2681();
            $import->createImport += 1;
            $cert2681->setAttributes($dataCert);
            var_dump ($dataCert);
            $cert2681Saved = $cert2681->save();

            if ($cert2681Saved) {
                echo "se guardo.\n";
            } else {
                echo "no se guardo\n";
            }
        }
        catch (Exception $ex) {
            echo $ex->getMessage();
        }
    
    }


    private function saveCertificationRG17($import, $format, $line){
        try {
            $dataCert = array();
            $string_version = implode(',', $line);
            $cuit = substr($string_version, 1, 13);
            $name = substr($string_version, 13, 61);
            $since = substr($string_version, 74, 10);
            $until = substr($string_version, 84, 10);
            $percentage = substr($string_version, 105, 3);
            $rg = substr($string_version, 109, 6);
            $type = substr($string_version, 129, 11);

            $dataCert['cuit'] =$cuit;
            $dataCert['name'] = $name;
            $dataCert['since'] = $since;
            $dataCert['until'] = $until;
            $dataCert['percentage'] = $percentage;
            $dataCert['rg'] = $rg;
            $dataCert['type'] = $type;

            $certRG17 = new CertificationRG17();
            $import->createImport += 1;
            $certRG17->setAttributes($dataCert);
            var_dump ($dataCert);
            $certRG17Saved = $certRG17->save();

            if ($certRG17Saved) {
                echo "se guardo.\n";
            } else {
                echo "no se guardo\n";
            }
        }
        catch (Exception $ex) {
            echo $ex->getMessage();
        }
    }

    private function saveCertificationRG18($import, $format, $line){
        try {
            $dataCert = array();
            $array_num = count($line);
            foreach ($format->importColumns as $index => $column) {
                if ($array_num <= 3 ){
                    //Se saltea la cabecera
                    continue;
                }     
                $dataColumn = $line[$index];
                if ($column->name == $this->getType(FileColumn::IGNORE)) {
                    echo "IGNORE\n";
                    continue;
                }
                switch ($column->name) {
                    case $this->getType(FileColumn::CUITRG18):
                        $dataCert['cuit'] = $dataColumn;
                        echo "cuit:" .$dataCert['cuit'] ."\n";
                        break;
                    case $this->getType(FileColumn::NAMERG18):
                        $dataCert['name'] = $dataColumn;
                        echo "name:" .$dataCert['name'] ."\n";

                        break;
                    case $this->getType(FileColumn::SINCERG18):
                        $dataCert['since'] = $dataColumn;
                        echo "since" .$dataCert['since'] . "\n";
                        break;    

                    case $this->getType(FileColumn::UNTILRG18):
                            $dataCert['until'] = $dataColumn;
                            echo "until" .$dataCert['until'] . "\n";
                            break;   
                    case $this->getType(FileColumn::TYPERG18):
                        $dataCert['type'] = trim($dataColumn);
                        echo "type" .$dataCert['type'] . "\n";
                        break;
                }
            }

            $certRG18 = new CertificationRG18();
            $import->createImport += 1;
            $certRG18->setAttributes($dataCert);
            var_dump ($dataCert);
            $certRG18Saved = $certRG18->save();

            if ($certRG18Saved) {
                echo "se guardo.\n";
            } else {
                echo "no se guardo\n";
            }
        }
        catch (Exception $ex) {
            echo $ex->getMessage();
        }
    
    }

       /**
     * @param Import $import
     * @param FileFormat $format
     * @param $line
     *
     * @throws Exception
     */


    private function saveCertificationRG($import, $format, $line){
        try {
            $dataCert = array();

            foreach ($format->importColumns as $index => $column) {
                $dataColumn = $line[$index];
                
                if ($column->name == $this->getType(FileColumn::IGNORE)) {
                    echo "IGNORE\n";
                    continue;
                }
                switch ($column->name) {
                    case $this->getType(FileColumn::NUMBERRG):
                        $dataCert['number'] = $dataColumn;
                        echo "number:" .$dataCert['number'] ."\n";

                        break;
                    case $this->getType(FileColumn::CUITRG):
                        $dataCert['cuit'] = $dataColumn;
                        echo "cuit:" .$dataCert['cuit'] ."\n";

                        break;
                    case $this->getType(FileColumn::PERIODRG):
                        $dataCert['period'] = $dataColumn;
                        echo "period" .$dataCert['period'] . "\n";
                        break;    

                    case $this->getType(FileColumn::PERCENTAGERG):
                            $dataCert['percentage'] = $dataColumn;
                            echo "percentage" .$dataCert['percentage'] . "\n";
                            break;   
                    case $this->getType(FileColumn::RESOLUTIONRG):
                        $dataCert['resolution'] = $dataColumn;
                        echo "resolution" .$dataCert['resolution'] . "\n";
                        break;

                    case $this->getType(FileColumn::STATERG):
                        $dataCert['state'] = $dataColumn;
                        echo "state" .$dataCert['state'] . "\n";
                        break;
                    case $this->getType(FileColumn::ISSUERG):
                        $dataCert['issue'] = $dataColumn;
                        echo "issue" .$dataCert['issue'] . "\n";
                        break;
                    case $this->getType(FileColumn::PUBLICATIONRG):
                        $dataCert['publication'] = trim($dataColumn);
                        echo "publication" .trim($dataCert['publication']) . "\n";
                        break;
                    case $this->getType(FileColumn::VALIDITYRG):
                        $dataCert['validity'] = trim($dataColumn);
                        echo "validity" .trim($dataCert['validity']) . "\n";
                        break;
                }
            }


            $certRG = new CertificationExGain();
            $import->createImport += 1;
            $certRG->setAttributes($dataCert);
            var_dump ($dataCert);
            $certRGSaved = $certRG->save();

            if ($certRGSaved) {
                echo "se guardo.\n";
            } else {
                echo "no se guardo\n";
            }
        }
        catch (Exception $ex) {
            echo $ex->getMessage();
        }
    
    }
    private function saveNic($import, $format, $line) {

        try {
            $dataBuyOrder = array();
            $initalStatus = 1;
            $dataBuyOrder['statusId'] = $initalStatus;
            foreach ($format->importColumns as $index => $column) {
                if ($column->name == $this->getType(FileColumn::IGNORE)) {
                    continue;
                }
                $dataColumn = $line[$index];


                if (((!isset($dataColumn) || $dataColumn === "") && $column->name != $this->getType(FileColumn::HES))) {
                    $msg = "Falta el campo ". $column->name ." en la NIC: ".$dataBuyOrder['nic']."\n";
                    $this->createImportError($import, $msg);
                    return;
                }


                switch ($column->name) {
                    case $this->getType(FileColumn::NIC):
                        $dataBuyOrder['nic'] = (double)$dataColumn;
                        break;
                    case $this->getType(FileColumn::MONTO):
                        $dataBuyOrder['monto'] = floatval($dataColumn);
                        break;
                    case $this->getType(FileColumn::HES):
                        $dataBuyOrder['hes'] = $dataColumn;
                        break;
                    case $this->getType(FileColumn::ESTADO):
                        if (strtoupper($dataColumn) == "LISTO PARA FACTURAR") {
                            $dataBuyOrder['statusId'] = 3;
                        } else {
                            $dataBuyOrder['statusId'] = 2;
                        }
                        break;
                    case $this->getType(FileColumn::CUIT):
                        $company = Company::model()->findByAttributes(array('cuit' => $dataColumn));

                        if (!isset($company)) {
                            $msg = "Compañía no encontrada con CUIT: ".$dataColumn."\n";
                            $this->createImportError($import, $msg);
                        }

                        $dataBuyOrder['idCompany'] = $company->idCompany;
                        break;
                    case $this->getType(FileColumn::FECHA_LIBERACION):
                        $fechaArray = explode('.', $dataColumn);
                        $year = $fechaArray[2];
                        $month = $fechaArray[1];
                        $day = $fechaArray[0];
                        $dateLiberation =  $year . "-" .$month."-".$day;
                        if ($dateLiberation == '0000-00-00' || $dateLiberation == '0--') {
                            $dataBuyOrder['fecha_liberacion'] = null;
                            break;
                        }
                        $dataBuyOrder['fecha_liberacion'] = $dateLiberation;
                        break;
                    case $this->getType(FileColumn::FECHA_VENCIMIENTO):
                        $fechaArray = explode('.', $dataColumn);
                        $year = $fechaArray[2];
                        $month = $fechaArray[1];
                        $day = $fechaArray[0];
                        $dateExpiration =  $year . "-" .$month."-".$day;
                        if ($dateExpiration == '0000-00-00' || $dateExpiration == '0--') {
                            $dataBuyOrder['fecha_vencimiento'] = null;
                            break;
                        }
                        $dataBuyOrder['fecha_vencimiento'] = $dateExpiration;
                        break;
                }
            }

            $buyOrder = BuyOrder::model()->findByAttributes(array(
                'nic' => $dataBuyOrder['nic']
            ));

            
            if (!isset($buyOrder)) {
                if ($dataBuyOrder['statusId'] != $initalStatus) {
                    echo "Documento ignorado.\n";
                    $import->totalImport -= 1;
                    return;
                }
                $buyOrder = new BuyOrder();
                $import->createImport += 1;
            } else {
                if ($buyOrder->statusId == 1 || $dataBuyOrder['statusId'] == 1 || $buyOrder->statusId >= $dataBuyOrder['statusId']) {
                    echo "Documento ignorado.\n";
                    $import->totalImport -= 1;
                    $buyOrder->importDate = $import->dateImport;
                    $buyOrder->save();
                    return;
                } else {
                    if ($dataBuyOrder['statusId'] == 3 && $buyOrder->statusId != $dataBuyOrder['statusId']) {
                        $this->sendMailToProvider($dataBuyOrder);
                    }
                }
                $import->updateImport += 1;
            }
            $buyOrder->importDate = $import->dateImport;
            $buyOrder->setAttributes($dataBuyOrder); 
            $buyOrderSaved = $buyOrder->save();
            
            if ($buyOrderSaved) {
                
                if (isset($dataBuyOrder['hes']) && $dataBuyOrder['hes'] > 0) {

                    $hes = Hes::model()->findByAttributes(array(
                        'number' => $dataBuyOrder['hes']
                    ));
                    
                    if (!isset($hes)) {
                        $hes = new Hes();
                        $hes->idBuyOrder = $buyOrder->idBuyOrder;
                        $hes->number = $dataBuyOrder['hes'];
                        $hes->save();
                    }
                }
                echo "La orden " . $dataBuyOrder['nic'] . " ha sido guardado correctamente.\n";
                $statusDescription = BuyOrderStatus::model()->findByPk($buyOrder->statusId)->description;
                $newHistory = new BuyOrderHistory();
                $newHistory->idBuyOrder = $buyOrder->idBuyOrder;
                $newHistory->detalle = $statusDescription;
                $newHistory->save();
                
            } else {
                echo "La orden " . $dataBuyOrder['nic'] . "no se ha guardado\n";
            }
        }
        catch (Exception $ex) {
            echo $ex->getMessage();
        }
    }

    private function changeStatusNics($import) {
        BuyOrder::model()->updateAll(array('statusId' => 9), "statusId = 3 and importDate < DATE_SUB(NOW(), INTERVAL 1 DAY)");
        //Yii::$app->db->createCommand("UPDATE buy_order SET statusId = 9 W)->execute();
    }


    // PROVEEDORES
    private function saveProvider($import, $format, $line) {
        try {
            $dataProvider = array();
            $dataProvider['cuit'] = "NULL";
            foreach ($format->importColumns as $index => $column) {
                if ($column->name == $this->getType(FileColumn::IGNORE)) {
                    continue;
                }

                $dataColumn = $line[$index];

                if ((!isset($dataColumn) || $dataColumn === '')) {
                    $msg = "Falta el campo ". $column->name ." en el proveedor con cuit: ". $dataProvider['cuit'] ."\n";
                    $this->createImportError($import, $msg);
                    return;
                }

                switch ($column->name) {
                    case $this->getType(FileColumn::CUIT):
                        $dataProvider['cuit'] = $dataColumn;
                        break;
                    /*case $this->getType(FileColumn::EMAIL):
                        $dataProvider['corporateMail'] = $dataColumn;
                        break;*/
                    case $this->getType(FileColumn::PROVIDER_NAME):
                        $dataProvider['company'] = $dataColumn;
                        $dataProvider['companyName'] = $dataColumn;
                        break;
                }
            }

            $provider = Company::model()->findByAttributes(array(
                'cuit' => $dataProvider['cuit']
            ));

            if (!isset($provider)) {
                $provider = new Company();
                $import->createImport += 1;
                $provider->idCompanyStatus = 2;
            } else {
                $import->updateImport += 1;
            }
            $provider->setAttributes($dataProvider);
            $providerSaved = $provider->save();

            if ($providerSaved) {
                echo "El proveedor " . $dataProvider['cuit'] . " ha sido guardado correctamente.\n";
            } else {
                echo "El proveedor " . $dataProvider['cuit'] . "no se ha guardado\n";
            }
        }
        catch (Exception $ex) {
            echo $ex->getMessage();
        }
    }

    // PEDIDO DE OFERTA
    private function savePO($import, $format, $line) {
        try {
            $dataPO = array();
            $dataPO['codPO'] = "NULL";
            $dataPO['status'] = 1;
            foreach ($format->importColumns as $index => $column) {
                if ($column->name == $this->getType(FileColumn::IGNORE)) {
                    continue;
                }
                $dataColumn = $line[$index];

                if ((!isset($dataColumn) || $dataColumn === '')) {
                    $msg = "Falta el campo ". $column->name ." en el PO con numero: ". $dataPO['codPO'] ."\n";
                    $this->createImportError($import, $msg);
                    return;
                }

                switch ($column->name) {
                    case $this->getType(FileColumn::NUMBERPO):
                        $dataPO['nombre'] = 'PO - ' . $dataColumn;
                        $dataPO['codPO'] = $dataColumn;
                        break;
                    case $this->getType(FileColumn::START_DATE):
                        $fechaArray = explode('.', $dataColumn);
                        $year = $fechaArray[2];
                        $month = $fechaArray[1];
                        $day = $fechaArray[0];
                        $startDate =  $year . "-" .$month."-".$day;
                        if ($startDate == '0000-00-00' || $startDate == '0--') {
                            $dataPO['fecha_inicio'] = null;
                            break;
                        }
                        $dataPO['fecha_inicio'] = $startDate;
                        break;
                    case $this->getType(FileColumn::END_DATE):
                        $fechaArray = explode('.', $dataColumn);
                        $year = $fechaArray[2];
                        $month = $fechaArray[1];
                        $day = $fechaArray[0];
                        $endDate =  $year . "-" .$month."-".$day;
                        if ($endDate == '0000-00-00' || $endDate == '0--') {
                            $dataPO['fecha_fin'] = null;
                            break;
                        }
                        $dataPO['fecha_fin'] = $endDate;
                        break;
                }
            }

            $PO = Licitaciones::model()->findByAttributes(array(
                'codPO' => $dataPO['codPO']
            ));

            //echo var_dump($dataBuyOrder);
            if (!isset($PO)) {
                $PO = new Licitaciones();
                $import->createImport += 1;
            } else {
                $import->updateImport += 1;
                $dataPO['status'] = $PO->status;
            }
            $PO->setAttributes($dataPO);
            $POSaved = $PO->save();

            if ($POSaved) {
                echo "El pedido de oferta " . $dataPO['codPO'] . " ha sido guardado correctamente.\n";
            } else {
                var_dump($PO);
                echo "El pedido de oferta " . $dataPO['codPO'] . " no se ha guardado\n";
            }
        }
        catch (Exception $ex) {
            echo $ex->getMessage();
        }
    }

    private function getType($typeArr) {

        return array_keys($typeArr)[0];
    }

    private function sendMailToProvider($buyOrder) {
        $idCompany = BuyOrder::model()->find('nic = '. $buyOrder['nic'])->idCompany;
        $providers = Provider::model()->findAll('idCompany = '.$idCompany);
        if ($idCompany == 0 || !isset($providers) || !is_array($providers) || count($providers) == 0) {
            return;
        }
			$mail = new Email();
			$emailParameters = EmailParameters::model()->findByPk(1);
			$customEmail = CustomEmail::model()->findByPk(5);
            $mail->subject = $customEmail->subject;
			$date = date("d/m/Y H:i:s");
            eval("\$sendBody = \"$customEmail->body\";");
            $mail->body = $sendBody;    
                $saved = $mail->save();
                    foreach ($providers as $key => $value) {
                        $mail->addReceiver($value->idUser0->email);
                    }
                return $saved;
            }
        }
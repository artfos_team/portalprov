<?php

class DateCheckerCommand extends CConsoleCommand 
{
	
    public function actionCheck(){
        $toChange = ParticipantProgram::model()->findAll('restartDate IS NOT NULL AND restartDate <= :time', array(':time'=>time()));
		Yii::import('application.modules.mailer.components.MailerMailCreator');
		$mailer = new MailerMailCreator();
		
		foreach($toChange as $pprogram)
		{
			$toSave = array('restartDate');
			if($pprogram->status == ParticipantProgram::STATUS_GRANTED)
			{
				$pprogram->status = ParticipantProgram::STATUS_FINISHED;
				$pprogram->endDate = date('d/m/Y');
				$toSave[] = 'status';
				$toSave[] = 'endDate';
				
				$subject = 'Fin de garantía';
				
				$message = '
				<h1>Se informa el fin de garantía para '.$pprogram->participant->getFullName().'</h1>';
				
				foreach(Yii::app()->params['notificationEmails'] as $email)
				{
					$res = $mailer->sendEmail(
						$subject,
						$message,
						strip_tags($message),
						$email,
						$email,
						MailerMailCreator::PRIORITY_NOW
					);
				}
			}
			if($pprogram->status == ParticipantProgram::STATUS_GRANTED)
			{
				$subject = 'Fin de suspensión';
				
				$message = '
				<h1>Se informa el fin de suspensión para '.$pprogram->participant->getFullName().'</h1>';
				
				foreach(Yii::app()->params['notificationEmails'] as $email)
				{
					$res = $mailer->sendEmail(
						$subject,
						$message,
						strip_tags($message),
						$email,
						$email,
						MailerMailCreator::PRIORITY_NOW
					);
				}
			}
			
			$pprogram->restartDate = null;
			$pprogram->save(false, $toSave);
		}
    }

    public function actionIndex(){
        echo $this->getHelp();
	}

}
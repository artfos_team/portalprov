<?php

class StatusUpdaterCommand extends CConsoleCommand 
{

    public function actionCheck($dryRun = false, $debug = false)
    {
		$updater = new StatusUpdater();
        $updater->check($dryRun, $debug);
    }


	public function getHelp()
    {
        return <<<EOD
USAGE
  yiic statusupdater

DESCRIPTION
	Updates the Participant Program dates according to several rules (checklist)

EOD;
    }

}
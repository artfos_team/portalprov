<?php
$path = dirname(dirname(__FILE__));
return array(
	'sourcePath'=>$path,
	'languages'=>array('es'),
	'messagePath'=>dirname(__FILE__),
	'exclude'=>array(
				$path,
				'.svn','.gitkeep'
	),
	'fileTypes'=>array('php'),
);
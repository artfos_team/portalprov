<?php

class MailerModule extends CWebModule
{
	public $templateAliasPath = array(
	);
	public $allowAnonymousMail = false;
	public $isSmtp = true;
	public $senderEmail = '';
	public $senderName = '';
	public $smtp = array( );

	public function init()
	{
		// this method is called when the module is being created
		// you may place code here to customize the module or the application
		// import the module-level models and components
		$this->setImport( array(
			'mailer.models.*',
			'mailer.components.*',
		) );

		if( !isset( Yii::app()->params['smtpAuth'] ) )
		{
			Yii::app()->params['smtpAuth'] = false;
		}
		if( !isset( Yii::app()->params['smtpSecure'] ) )
		{
			Yii::app()->params['smtpSecure'] = false;
		}
		
		$this->smtp = array(
			'user' => Yii::app()->params['mailerUsername'],
			'pass' => Yii::app()->params['mailerPassword'],
			'host' => Yii::app()->params['mailerHost'],
			'port' => Yii::app()->params['mailerPort'],
			'smtpAuth' => Yii::app()->params['smtpAuth'],
			'smtpSecure' => Yii::app()->params['smtpSecure'],
		);
		$this->senderEmail = Yii::app()->params['mailerFrom'];
		$this->senderName = Yii::app()->params['mailerFromName'];

	}

	public function beforeControllerAction( $controller, $action )
	{
		if( parent::beforeControllerAction( $controller, $action ) )
		{
			// this method is called before any module controller action is performed
			// you may place customized code here
			return true;
		}
		else return false;
	}

	/**
	 * Wether to allows anonymous users to send information via email
	 * @var boolean
	 */
	public function getViewFile( $type, $view )
	{
		
		if( isset( $this->templateAliasPath[$type] ) )
		{
			$file = Yii::getPathOfAlias( $this->templateAliasPath[$type] . '.' . $view ) . '.php';
			if( file_exists( $file ) )
			{
				return $this->templateAliasPath[$type] . '.' . $view;
			}
		}
		throw new CException( Yii::t( 'mailer', 'The email view file is invalid' ) );
	}

}

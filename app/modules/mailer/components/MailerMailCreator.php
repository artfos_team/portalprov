<?php

Yii::import( 'application.modules.mailer.models.*' );

/**
 * 
 * @author Sebastián Thierer
 * @version 1.0
 *
 */
class MailerMailCreator extends CWidget
{
	const PRIORITY_LOW = 1;
	const PRIORITY_NORMAL = 2;
	const PRIORITY_HIGH = 3;
	const PRIORITY_NOW = 4;

	public function sendEmail( $subject, $message, $plain_text_message,
		$receiver_name, $receiver_email, $priority=self::PRIORITY_LOW, $send_at=null,
		$sender_name=null, $sender_mail=null, $iditem_type = null, $iditem_item = null,
		$hash = null, $attachments = null, $email_reply_to = null )
	{
		if(!is_null($iditem_type) && !is_null($iditem_item)) {
			$aux = MailerMailer::model()->find('iditem_type = :idit AND iditem_item = :idii
				AND read_at IS NULL AND (sent_at > :time OR sent_at IS NULL)', 
			array(':idit'=>$iditem_type, ':idii'=>$iditem_item, ':time'=>time()-86400));
			if($aux instanceof MailerMailer) {
				return true;
			}
		}
		
		$ret = false;
		$mail = new MailerMailer();
		$mail->subject = $subject;
		$mail->body = $plain_text_message;
		$mail->html_body = $message;
		$mail->sent_at = null;
		$mail->send_at = is_null( $send_at ) ? time() : CPropertyValue::ensureInteger( $send_at );
		$mail->created_at = time();
		$mail->priority = $priority;
		$mail->receiver_name = $receiver_name;
		$mail->receiver_mail = $receiver_email;
		$mail->sender_name = is_null( $sender_name ) ? Yii::app()->params['mailerFromName'] : $sender_name;
		$mail->sender_mail = is_null( $sender_mail ) ? Yii::app()->params['mailerFrom'] : $sender_mail;
		$mail->attachments = is_null( $attachments ) ? null : CJSON::encode($attachments);
		$mail->email_reply_to = $email_reply_to;

		//$mail->iditem_type = $iditem_type;
		//$mail->iditem_item = $iditem_item;
		//$mail->unique_hash = $hash;
		$ret = $mail->save();
		if( !$ret )
		{
			echo CHtml::errorSummary( $mail );
		}
		elseif( $ret && $priority == self::PRIORITY_NOW )
		{
			return $mail->sendNow();
		}
		return $ret;
	}

	/**
	 * @var string The subjecto for the rendered email
	 */
	public $subject;

	/**
	 * 
	 * @param string $language
	 * @param string $type
	 * @param string _type $template
	 * @param Object[] $params
	 * @param string $receiver_name
	 * @param string $receiver_email
	 * @param int $priority
	 * @param int $send_at
	 * @param string $sender_name
	 * @param string $sender_mail
	 * @param array $sender_mail
	 * @param string $subject
	 * @param string $email_reply_to
	 */
	public function sendEmailByTemplate(
	$language=null, $type, $template, $params = array( ), $receiver_name,
		$receiver_email, $priority=self::PRIORITY_LOW, $send_at=null,
		$sender_name=null, $sender_mail=null, $attachments = null, $subject =  null, 
		$email_reply_to = null )
	{
		$originalLanguague = Yii::app()->language;
		$result = false;
		if( $language === null )
		{
			$language = Yii::app()->language;
		}

		if( $language != Yii::app()->language )
		{
			$lang = Yii::app()->language;
			Yii::app()->language = $language;
		}
		$message = $this->render( Yii::app()->getModule( 'mailer' )->getViewFile( $type,
					$template ), $params, true );

		if( !isset( $this->subject ) )
		{
			$this->subject = $subject;
		}
		if( !isset( $this->subject ) )
		{
			$this->subject = Yii::app()->name;
		}
		$result = $this->sendEmail( $this->subject, $message, strip_tags( $message ),
				$receiver_name, $receiver_email, $priority, $send_at, $sender_name,
				$sender_mail, null, null, null, $attachments, $email_reply_to );

		Yii::app()->language = $originalLanguague;

		return $result;
	}
	
	public function sendEmailByTemplateWithItem(
	$language=null, $type, $template, $params = array( ), $receiver_name,
		$receiver_email, $priority=self::PRIORITY_LOW, $iditem_type, $iditem_item, 
		$hash=null, $attachments = null)
	{
		$originalLanguague = Yii::app()->language;
		$result = false;
		if( $language === null )
		{
			$language = Yii::app()->language;
		}

		if( $language != Yii::app()->language )
		{
			$lang = Yii::app()->language;
			Yii::app()->language = $language;
		}
		$message = $this->render( Yii::app()->getModule( 'mailer' )->getViewFile( $type,
					$template ), $params, true );

		if( !isset( $this->subject ) )
		{
			$this->subject = Yii::app()->name;
		}
		$result = $this->sendEmail( $this->subject, $message, strip_tags( $message ),
				$receiver_name, $receiver_email, $priority, null, null,
				null, $iditem_type, $iditem_item, $hash, $attachments );

		Yii::app()->language = $originalLanguague;

		return $result;
	}

	/**
	 * Returns the text used in the message of the template selected with params $params
	 * @param  string $type     type of the template
	 * @param  string $template name of the template to use
	 * @param  array $params   params of the template
	 * @return string           text of the email to send
	 */
	public function getMessageTextByTemplate($type, $template, $params)
	{
		return $this->render( Yii::app()->getModule( 'mailer' )->getViewFile( $type,
					$template ), $params, true );
	}

}

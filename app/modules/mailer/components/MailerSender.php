<?php

class MailerSender
{

	/**
	 * gets the module with its configurations
	 * 
	 * @return MailerModule
	 */
	public static function getMailerModule()
	{
		return Yii::app()->getModule( 'mailer' );
	}

	/**
	 * send an email from a MailModel
	 * @param MailModel $mail
	 * @return boolean
	 */
	public static function sendMailByModel( MailerMailer $mail )
	{
		$ret = false;

		$mailer = Yii::createComponent( 'ext.mailer.EMailer' );

		$module = self::getMailerModule();

		$mailer->Host = $module->smtp['host'];

		if( $mailer->Host != 'dont_send_mail' )
		{
			if( $module->isSmtp )
			{
				$mailer->IsSMTP();
			}

			$mailer->Port = $module->smtp['port'];
			$mailer->Username = $module->smtp['user'];
			$mailer->Password = $module->smtp['pass'];
			$mailer->SMTPAuth = $module->smtp['smtpAuth'];
			$mailer->SMTPSecure = $module->smtp['smtpSecure'];
			
			$mailer->IsHTML( true );
			$mailer->CharSet = 'UTF-8';
			$mailer->SetLanguage( 'es' );

			$mailer->Sender = $module->senderEmail;

			$mailer->From = $module->senderEmail;
			$mailer->FromName = $mail->sender_mail;//$module->senderName;

			/*if( $mail->cc != '' )
			{
				$mailer->AddCC( $mail->cc );
			}

			if( $mail->bcc != '' )
			{
				$mailer->AddBCC( $mail->bcc );
			}	*/

			$mailer->AddAddress( $mail->receiver_mail, $mail->receiver_name );
			$mailer->Subject = $mail->subject;

			$mailer->AltBody = $mail->body;
			$mailer->Body = $mail->html_body;

			//Add custom reply to
			if(!is_null($mail->email_reply_to))
			{
				$mailer->AddReplyTo($mail->email_reply_to);
			}
			
			if(isset($mail->attachments))
			{
				$attachments = CJSON::decode($mail->attachments);
				foreach($attachments as $attachment)
				{
                    if(is_array($attachment))
                    {
                        list($file, $fileName) = $attachment;
                        $mailer->addAttachment($file, $fileName);
                    } else {
					    $mailer->addAttachment($attachment);
                    }
				}
			}
			try
			{
				$ret = $mailer->Send();
				return $ret;
			}
			catch( phpmailerException $e )
			{
				throw $e;
			}
			catch( Exception $e )
			{
				throw $e;
			}
		}
		else
		{
			// no email
			$ret = true;
		}
		return $ret;
	}

}
<?php

Yii::import('application.modules.mailer.components.MailerSender');

/**
 * This is the model class for table "mailer_mailer".
 *
 * The followings are the available columns in table 'mailer_mailer':
 * @property integer $idmailer_mailer
 * @property integer $priority
 * @property integer $sent_at
 * @property integer $created_at
 * @property string $subject
 * @property string $body
 * @property string $html_body
 * @property string $receiver_name
 * @property string $receiver_mail
 * @property string $sender_name
 * @property string $sender_mail
 * @property string $read_at
 * @property string $iditem_type
 * @property string $iditem_container
 * @property string $iditem_item
 * @property string $attachments
 * 
 */
class MailerMailer extends CActiveRecord
{
	public $email_reply_to;

	/**
	 * Returns the static model of the specified AR class.
	 * @return MailerMailer the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'mailer_mailer';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('priority, send_at, created_at, receiver_name, receiver_mail', 'required'),
			array('priority, send_at, created_at, sent_at,read_at,iditem_type,iditem_container,iditem_item', 'numerical', 'integerOnly'=>true),
			array('subject, receiver_name, receiver_mail, sender_name, sender_mail', 'length', 'max'=>255),
			array('body, html_body, attachments', 'safe'),
			array('unique_hash', 'safe'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'idDbmailer' => 'Id Dbmailer',
			'priority' => 'Priority',
			'send_at' => 'Send At',
			'created_at' => 'Created At',
			'sent_at' => 'Sent At',
			'subject' => 'Subject',
			'body' => 'Body',
			'html_body' => 'Html Body',
			'receiver_name' => 'Receiver Name',
			'receiver_mail' => 'Receiver Mail',
			'sender_name' => 'Sender Name',
			'sender_mail' => 'Sender Mail',
			'attachments' => 'Attachments'
		);
	}

	/**
	 * @return boolean send is valid or not
	 */
    public function sendNow()
    {
        if ($this->validate() && ($this->sent_at==null || $this->sent_at == 1))
        {
            if (MailerSender::sendMailByModel($this))
            {
                $this->sent_at = time();
                return $this->save(false, array('sent_at'));
            }
        }
        return false;
    }

    public static function getUnsentMails($qty=5)
    {
        return self::model()->notSent()->findAll(array('limit'=>$qty, 'order'=>'priority DESC', 'index'=>'idmailer_mailer'));
    }
    
    public function scopes()
    {
    	return array(
    		'notSent'=>array('condition'=>'sent_at IS NULL AND send_at<=:actualTimeNotSent', 'params'=>array(':actualTimeNotSent'=>time())),
    		'notAvailable' => array('condition'=>'sent_at IS NULL AND send_at>:actualTimeNotSent', 'params'=>array(':actualTimeNotSent'=>time())),
    	);
    }
}
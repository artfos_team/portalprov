<?php

/**
 * This widget let you create a refresh area in the
 *
 * @author sebas
 */
class ClockRefresh extends CWidget {

    public $time = 1000;
    public $url = null;
    public $updateAtStart = false;
    public $containerCssClass = null;
    public $refreshUpdateClock = true;

    public function init(){
        ob_start();
    }
    public function run(){
        $aux = ob_get_clean();
        $id = $this->getId();
        echo "<div id=\"$id\"";
        if ($this->containerCssClass){
            echo ' class="' . $this->containerCssClass . '"';
        }
        echo '>';
        echo $aux;
        echo ('</div>');
        if ($this->url==null){
            throw new CException("ClockRefresh::url | The url can not be null.");
        }
        $cs=Yii::app()->getClientScript();
        $dir = dirname(__FILE__).DIRECTORY_SEPARATOR.'assets';
        $baseUrl = Yii::app()->getAssetManager()->publish($dir);

        $cs->registerCoreScript('jquery');
        $cs->registerScriptFile($baseUrl.'/jquery.timers-1.1.2.js');

        $js = 'function timer_func_'.$id.'(time_value){
                    $(\'#'.$id.'\').oneTime(time_value, "timer_'.$id.'", function() {
                    '.CHtml::ajax(array('url'=>$this->url,
                                        'type'=>'GET',
                                        'update'=>'#'.$id,
                                        'complete'=>'function (XMLHttpRequest, textStatus) {
                                                    timer_func_'.$id.'('.$this->time.');
                                                }
                                        ')).'
                    });
                }
                ';
        if ($this->updateAtStart){
            $js.="timer_func_$id(1);";
        }else{
            $js.="timer_func_$id(".$this->time.");";
        }
        $cs->registerScript($id, $js);
    }

}
?>

<?php

class m110603_140447_create_mailer extends CDbMigration
{
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
		$this->createTable('mailer_mailer', array(
			'idmailer_mailer'=>'pk',
			'priority'=>'INTEGER NOT NULL DEFAULT 0',
			'send_at'=>'INTEGER NULL',
			'sent_at'=>'INTEGER NULL',
			'created_at'=>'INTEGER NOT NULL',
			'subject'=>'VARCHAR(255) NOT NULL',
			'body'=>'TEXT NULL',
			'html_body'=>'TEXT NULL',
			'receiver_name'=>'VARCHAR(255) NOT NULL',
			'receiver_mail'=>'VARCHAR(255) NOT NULL',
			'sender_name'=>'VARCHAR(255) NOT NULL',
			'sender_mail'=>'VARCHAR(255) NOT NULL',
		),'ENGINE=InnoDB DEFAULT CHARSET=utf8');
	}

	public function safeDown()
	{
		$this->dropTable('mailer_mailer');
	}
}
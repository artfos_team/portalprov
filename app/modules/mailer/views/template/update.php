<h2>Update TemplateModel <?php echo $model->iddbmailer_template; ?></h2>

<div class="actionBar">
[<?php echo CHtml::link('New TemplateModel',array('create')); ?>]
[<?php echo CHtml::link('Manage TemplateModel',array('admin')); ?>]
</div>

<?php echo $this->renderPartial('_form', array(
	'model'=>$model,
	'update'=>true,
)); ?>
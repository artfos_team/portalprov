<div class="yiiForm">

<p>
Fields with <span class="required">*</span> are required.
</p>

<?php echo CHtml::beginForm(); ?>

<?php echo CHtml::errorSummary($model); ?>

<div class="simple">
<?php echo CHtml::activeLabelEx($model,'dbmailer_template_code'); ?>
<?php echo CHtml::activeTextField($model,'dbmailer_template_code',array('size'=>60,'maxlength'=>255)); ?>
</div>
<div class="simple">
<?php echo CHtml::activeLabelEx($model,'dbmailer_template_subject'); ?>
<?php echo CHtml::activeTextField($model,'dbmailer_template_subject',array('size'=>60,'maxlength'=>255)); ?>
</div>
<div class="simple">
<?php echo CHtml::activeLabelEx($model,'dbmailer_template_message'); ?>
<?php echo CHtml::activeTextArea($model,'dbmailer_template_message',array('rows'=>6, 'cols'=>50)); ?>
</div>
<div class="simple">
<?php echo CHtml::activeLabelEx($model,'dbmailer_template_htmlmessage'); ?>
<?php echo CHtml::activeTextArea($model,'dbmailer_template_htmlmessage',array('rows'=>6, 'cols'=>50)); ?>
</div>
<div class="simple">
<?php echo CHtml::activeLabelEx($model,'dbmailer_template_version'); ?>
<?php echo CHtml::activeTextField($model,'dbmailer_template_version'); ?>
</div>
<div class="simple">
<?php echo CHtml::activeLabelEx($model,'iddbmailer_language'); ?>
<?php echo CHtml::activeTextField($model,'iddbmailer_language'); ?>
</div>

<div class="action">
<?php echo CHtml::submitButton($update ? 'Save' : 'Create'); ?>
</div>

<?php echo CHtml::endForm(); ?>

</div><!-- yiiForm -->
<h2>View TemplateModel <?php echo $model->iddbmailer_template; ?></h2>

<div class="actionBar">
[<?php echo CHtml::link('New TemplateModel',array('create')); ?>]
[<?php echo CHtml::link('Update TemplateModel',array('update','id'=>$model->iddbmailer_template)); ?>]
[<?php echo CHtml::linkButton('Delete TemplateModel',array('submit'=>array('delete','id'=>$model->iddbmailer_template),'confirm'=>'Are you sure?')); ?>
]
[<?php echo CHtml::link('Manage TemplateModel',array('admin')); ?>]
</div>

<table class="dataGrid">
<tr>
	<th class="label"><?php echo CHtml::encode($model->getAttributeLabel('dbmailer_template_code')); ?>
</th>
    <td><?php echo CHtml::encode($model->dbmailer_template_code); ?>
</td>
</tr>
<tr>
	<th class="label"><?php echo CHtml::encode($model->getAttributeLabel('dbmailer_template_subject')); ?>
</th>
    <td><?php echo CHtml::encode($model->dbmailer_template_subject); ?>
</td>
</tr>
<tr>
	<th class="label"><?php echo CHtml::encode($model->getAttributeLabel('dbmailer_template_message')); ?>
</th>
    <td><?php echo CHtml::encode($model->dbmailer_template_message); ?>
</td>
</tr>
<tr>
	<th class="label"><?php echo CHtml::encode($model->getAttributeLabel('dbmailer_template_htmlmessage')); ?>
</th>
    <td><?php echo CHtml::encode($model->dbmailer_template_htmlmessage); ?>
</td>
</tr>
<tr>
	<th class="label"><?php echo CHtml::encode($model->getAttributeLabel('dbmailer_template_version')); ?>
</th>
    <td><?php echo CHtml::encode($model->dbmailer_template_version); ?>
</td>
</tr>
<tr>
	<th class="label"><?php echo CHtml::encode($model->getAttributeLabel('iddbmailer_language')); ?>
</th>
    <td><?php echo CHtml::encode($model->iddbmailer_language); ?>
</td>
</tr>
</table>

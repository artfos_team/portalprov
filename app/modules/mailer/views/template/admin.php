<h2>Managing TemplateModel</h2>

<div class="actionBar">
[<?php echo CHtml::link('New TemplateModel',array('create')); ?>]
</div>

<table class="dataGrid">
  <tr>
    <th><?php echo $sort->link('iddbmailer_template'); ?></th>
    <th><?php echo $sort->link('dbmailer_template_code'); ?></th>
    <th><?php echo $sort->link('dbmailer_template_subject'); ?></th>
    <th><?php echo $sort->link('dbmailer_template_version'); ?></th>
    <th><?php echo $sort->link('iddbmailer_language'); ?></th>
    <th>Parametros</th>
	<th>Actions</th>
  </tr>
<?php foreach($models as $n=>$model): ?>
  <tr class="<?php echo $n%2?'even':'odd';?>">
    <td><?php echo CHtml::link($model->iddbmailer_template,array('show','id'=>$model->iddbmailer_template)); ?></td>
    <td><?php echo CHtml::encode($model->dbmailer_template_code); ?></td>
    <td><?php echo CHtml::encode($model->dbmailer_template_subject); ?></td>
    <td><?php echo CHtml::encode($model->dbmailer_template_version); ?></td>
    <td><?php echo CHtml::encode($model->iddbmailer_language); ?></td>
    <td><?php $params = $model->calculateParamList();?>
        <?php foreach ($params as $param){?>
            - <?php echo $param; ?> <br/>
        <?php }?>
    <td>
      <?php echo CHtml::beginForm();?>
          <?php echo CHtml::link('Update',array('update','id'=>$model->iddbmailer_template)); ?>
          <?php echo CHtml::linkButton('Delete',array(
              'submit'=>'',
              'params'=>array('command'=>'delete','id'=>$model->iddbmailer_template),
              'confirm'=>"Are you sure to delete #{$model->iddbmailer_template}?")); ?>
          <?php echo CHtml::endForm();?>
	</td>
  </tr>
<?php endforeach; ?>
</table>
<br/>
<?php $this->widget('CLinkPager',array('pages'=>$pages)); ?>
<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of StatController
 *
 * @author sebas
 */
class DefaultController extends Controller
{

	public function actionIndex()
	{
		$this->render( 'index' );
	}

	public function actionSendMails()
	{
		$mailsToSend = MailerMailer::getUnsentMails( 2 );
		$i = 0;
		foreach( $mailsToSend as $mail )
		{
			$mail->sendNow();
			$i++;
		}
		echo 'Date and time: ' . Yii::app()->dateFormatter->formatDateTime( time(), 'medium' ) . '<br/>';
		echo 'Total sent this request: ' . $i . '<br/>';
		echo 'Mails on queue: ' . MailerMailer::model()->notSent()->count() . '<br/>';
		echo 'Mails to send: ' . MailerMailer::model()->notAvailable()->count();
		
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow', 'actions'=>array('generateImage', 'sendMails')),
			array('deny', // deny all users
				'users' => array('*')),
		);
	}
	
	/**
	 * Generates the image for the header and saves the campaign subscriptor as read
	 * @param integer $idlinker_newsletter_campaign_has_subscriptor the id of the reader of the newsletter
	 */
	public function actionGenerateImage($hash){
		$mailer = MailerMailer::model()->findByAttributes(array('unique_hash'=>$hash));
		if($mailer instanceof MailerMailer && $mailer->read_at == null){
			$mailer->read_at = time();
			$mailer->save(false);
		}
		readfile(Yii::getPathOfAlias('webroot').'/images/logo_mail.jpg');
	}

}

?>

<div style="width: 500px;">
	<p><?php echo $userPreface . ' ' . $mainContact; ?></p>
	<p>Adjunto el documento en referencia al Programa <?php echo $program ?> correspondiente a <?php echo $participant; ?>.</p>
	<p>Cualquier consulta no dude en contactarme, estoy a disposición.</p>
	<p>Saludos cordiales,</p>
	<p><?php echo $user; ?></p>
	<p><?php echo AHtml::absoluteImage(Yii::app()->request->baseUrl.'/images/logo-02.png', 'logo', array('height'=>61)); ?></p>
</div>
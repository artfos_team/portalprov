<?php 
$mainContact = '';
$userPreface = 'Estimado/a';
if( isset( $participantReport->participantProgram, 
	$participantReport->participantProgram->participant,
	$participantReport->participantProgram->participant->companyContact) )
{
	$mainContact = $participantReport->participantProgram->participant->companyContact->getFullName();
	$userPreface = $participantReport->participantProgram->participant->companyContact->sex == 0 ? 'Estimada' : 'Estimado' ;
} ?>
<div style="width: 500px;">	
	<p><?php echo $userPreface . ' ' . $mainContact; ?></p>
	<p>Adjunto el documento en referencia al Programa <?php echo $participantReport->participantProgram->program->program ?> correspondiente a <?php echo $participantReport->participantProgram->participant->getFullName(); ?>.</p>
	<p>Cualquier consulta no dude en contactarme, estoy a disposición.</p>
	<p>Saludos cordiales,</p>
	<p><?php echo user()->loadModel()->getFullName(); ?></p>
	<p><?php echo AHtml::absoluteImage(Yii::app()->request->baseUrl.'/images/logo-02.png', 'logo', array('height'=>61)); ?></p>
</div>
<?php
/**
 * This filter implements core RBAC access filtering
 * Mashala
 * @author oleksiy
 */
class RbacFilter extends COutputProcessor {
    public $user_id_field = 'id';

    public function filter($filterChain)
    {
        $this->preFilter($filterChain);
        return parent::filter($filterChain);
    }

    protected function preFilter($filterChain)
    {
        if(Yii::app()->user->isGuest){
            Yii::app()->user->loginRequired();
        }
        $controller = is_null($filterChain->controller->module)?$filterChain->controller->id:$filterChain->controller->module->id.'/'.$filterChain->controller->id;
        $id_field = $this->user_id_field;

		if(Yii::app()->rbac->checkAccess($controller, $filterChain->action->id, Yii::app()->user->$id_field)){
            return true;
        }
        // Yii::app()->rbac->checkAccess($controller, $action, $userid);

        throw new CHttpException(401,Yii::t('yii','You are not authorized to perform this action.'));
    }

    public function processOutput($content)
    {
        echo $content;
    }
    
}
?>

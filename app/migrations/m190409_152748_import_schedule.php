<?php

class m190409_152748_import_schedule extends CDbMigration
{
	public function up()
	{
        $this->createTable('import_schedule', array(
            'idImportSchedule' => 'pk',
            'idImportFileFormat' => 'bigint NOT NULL',
            'time' => 'varchar(5) NOT NULL'
        ));
	}

	public function down()
	{
        $this->dropTable('import_schedule');
	}
}
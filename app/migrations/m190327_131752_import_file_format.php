<?php

class m190327_131752_import_file_format extends CDbMigration
{
	public function up()
	{
        $this->createTable('import_file_format', array(
            'idImportFileFormat' => 'pk',
            'name' => 'varchar(50) NOT NULL',
            'url' => 'varchar(255) NOT NULL',
            'type' => 'varchar(2) NOT NULL'
        ));

        $this->createTable('import_file_column', array(
            'idImportFileColumn' => 'pk',
            'idImportFileFormat' => 'bigint NOT NULL',
            'name' => 'varchar(50) NOT NULL'
        ));
	}

	public function down()
	{
        $this->dropTable('import_file_column');
        $this->dropTable('import_file_format');
	}
}